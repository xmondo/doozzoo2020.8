import { browser, by, element, ProtractorBrowser, ProtractorExpectedConditions } from 'protractor';

describe('home login dialog', () => {
    const EC = ProtractorExpectedConditions;
    beforeEach(() => {
        // browser.ignoreSynchronization = true;
        // browser.sleep(1000);
    });

    afterEach(async () => {
        await browser.sleep(1000);
    });

    it('should check for cookie hint popup', async () => {
        await browser.get('/');
        const cookie = element(by.css('button[name="cookie-hint-button"]'));
        const status = await cookie.isPresent();
        // console.log('cookie status: ', status);
        if(status) {
            cookie.click();
        }
    });

    // cookie hint window should disapear
    it('cookie hint popup has disapeared', async () => {
        await browser.wait(() => EC.prototype.stalenessOf(element(by.css('.cookie-hint'))), 6000);
        await browser.sleep(2000);
    });

    it('should click "login...', async () => {
        // const el = element(by.css('button[name="login"]'));
        await browser.wait(() => EC.prototype.visibilityOf(element(by.css('button[name="login"]'))), 6000);
        element(by.css('button[name="login"]')).click();
        await browser.sleep(1000);
    });

    it('should login as user2', async () => {
        await browser.get('/directlogin?mode=login&u=user2@mypelz.de&p=%23user2');
        await browser.sleep(2000);
    });

    it('should click account menu button in footer and select "reset"', async () => {
        element(by.css('button[name="footerLogout"]')).click();
        await browser.sleep(1000);
        const el = element(by.cssContainingText('.mat-menu-item', 'Reset password'));
        await el.click();
        await browser.sleep(1000);
      });

    it('should see all tabs (4)', async () => {
        await browser.wait(() => EC.prototype.presenceOf(element(by.css('.mat-tab-labels'))), 6000);
        let el = element(by.css('.mat-tab-labels'));
        // await expect(el.getText()).toContain('Login');
        // await expect(el.getText()).toContain('Register');
        await expect(el.getText()).toContain('Reset password');
        await expect(el.getText()).toContain('Verify e-mail');
    });

});
