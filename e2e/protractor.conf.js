// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const { SpecReporter } = require('jasmine-spec-reporter');
// const baseUrl = 'doozzoo.com';
// const baseUrl = 'doozzoo-eve.firebaseapp.com';
// const baseUrl = 'staging.doozzoo.com';
// const baseUrl = 'dev.doozzoo.com';
const baseUrl = 'localhost:4200';
// const baseUrl = '192.168.1.2:4200';

exports.config = {

  params: {
		user: {
			coach: { nickname: 'coach1', username: 'coach1@mypelz.de', password: '#coach1' },
			customer: { nickname: 'user2', username: 'user2@mypelz.de', password: '#user2' },
			customer2: { nickname: 'user3', username: 'user3@mypelz.de', password: '#user3' },
		},
		size: {
			w: 960,
			h: 900,
		},
		position1: {
			t:0,
			l:0
		},
		position2: {
			t:0,
			//l:1024,
			//l:900,
			l:700
    },
    baseUrl: 'https://' + baseUrl +'/',

	},
	// usage:
	// browser.params.user.coach.username -> 'coach1@mypelz.de'
  // browser.params.baseUrl

  specs: [
    // STRIPE tests!!
    // 'app.eduInstitutionList.e2e-spec.ts', // NOT ready
    // 'app.eduLogin.e2e-spec.ts', //

    // ### !!to do tests above!! ###
    
    // 'app.homeCookies.e2e-spec.ts', // + ready

    //'app.coachParticipantSessionChat.e2e-spec.ts', // NOT ready
    //'app.screensharing.e2e-spec.ts', // NOT ready -> manual clicks necessary
    // ### 'app.testdrive.e2e-spec.ts', // remove
    //'app.nickname.e2e-spec.ts', // NOT ready

    //'app.home.e2e-spec.ts', // + ready
    //'app.loginDialog.e2e-spec.ts', // + ready
    //'app.homeLandingContent.e2e-spec.ts', // + ready
    
    //'app.homeRegisterFlow.e2e-spec.ts', // ready
    //'app.studentRegisterFlowInterupted.e2e-spec.ts', // in progress
    //'app.registerFlowCoachInterrupted.e2e-spec.ts', // ready
    //'app.registerFlowCoachStripe.e2e-spec.ts', // ready
    //'app.registeredCoachVoucher.e2e-spec.ts', // ready
    // ### two screens ###
    //'app.coachParticipants.e2e-spec.ts',  // ready
    //'app.coachParticipantsPublish.e2e-spec.ts',  // ready
    //'app.coachParticipantsWaiting.e2e-spec.ts',  // NOT ready
    

    // ### basic ###
    // MUST - check register flow for student
    //'app.inviteUnregisteredStudent.e2e-spec.ts',  // ready
    //'app.interruptedInviteUnregisteredStudent.e2e-spec.ts',  // NOT ready
    //'app.inviteRegisteredStudent.e2e-spec.ts',  // ready
    //'app.inviteRegisteredStudentLoggedIn.e2e-spec.ts',  // ready
    
    //'app.registerFlowGoogle.e2e-spec.ts', // not ready
    //'app.loginResetPassword.e2e-spec.ts', // ready
	],

  // no blanks after comma for multiple!!
  // ng e2e --suite default
  // npx ng e2e --suite smoke
  // ng e2e --suite smoke,invite,two
	suites: {

		smoke: [
      // testdrive test
      // widget test

      // cookies must be first
      
      // npx ng e2e app.homeCookies.e2e-spec.ts, // ready     
      'app.home.e2e-spec.ts', // ready
      'app.loginDialog.e2e-spec.ts', // ready
      
      // 'app.loginResetPassword.e2e-spec.ts', // ready
      // 'app.testdrive.e2e-spec.ts', // ready
      // 'app.homeLandingContent.e2e-spec.ts', // replaced functionality ready 

      //'app.screensharing.e2e-spec.ts', // NOT ready -> manual clicks necessary

    ],

    /*
    two: [
      // 'app.coachParticipantSessionChat.e2e-spec.ts', // NOT ready
      // 'app.coachParticipantsPublish.e2e-spec.ts', // ready
      //'app.coachParticipants.e2e-spec.ts',  // NOT ready
    ],
    */
   
    invite: [
      'app.inviteUnregisteredStudent.e2e-spec.ts',
      'app.inviteRegisteredStudent.e2e-spec.ts',
      'app.inviteRegisteredStudentLoggedIn.e2e-spec.ts',
    ],

    /*
    registration: [
      'app.homeRegisterFlow.e2e-spec.ts', // ready
      'app.registerFlowCoachInterrupted.e2e-spec.ts', // ready 
      'app.registerFlowCoachStripe.e2e-spec.ts', // ready
      'app.registeredCoachVoucher.e2e-spec.ts', // ready
      'app.nickname.e2e-spec.ts', // ready
    ],
    */

    edu: [
      'app.eduLogin.e2e-spec.ts', // ready
    ],
  },

  // ### config ###
  SELENIUM_PROMISE_MANAGER: false,
  allScriptsTimeout: 60000,
  capabilities: {
    'acceptInsecureCerts' : true,

    'browserName': 'chrome',
    'chromeOptions': {

      prefs: {
        // intl: { accept_languages: "de-DE" },
      },
      'args': [
        //'--headless', 
        //'lang=de-DE',
        'auto-select-desktop-capture-source="Entire screen"',
        'use-fake-device-for-media-stream',
        'use-fake-ui-for-media-stream', 
        'disable-popup-blocking',
        //'--incognito',
        
      ],
/*       
      'args': [
        "--headless", 
        "--disable-gpu", 
        "--window-size=1000x1600", // widthxheight
        'use-fake-device-for-media-stream',
        'use-fake-ui-for-media-stream', 
        'disable-popup-blocking',
      ], 
*/
      'binary': process.env.BROWSERBIN
    }
  },
  directConnect: true,
  baseUrl: 'https://' + baseUrl +'/',
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function() {}
  },
  onPrepare() {
    require('ts-node').register({
      project: 'e2e/tsconfig.e2e.json'
    });
    jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: 'none' } }));

		// ### default size ###
		browser.driver.manage().window().setSize(1000, 1000);
    // ### mail ###
    /*
    var MailListener = require("mail-listener2");

    // here goes your email connection configuration
    var mailListener = new MailListener({
        username: "pelz@mypelz.de",
        password: "SIEBEN#meilen?stiefel",
        host: "imap.1und1.de",
        port: 993, // imap port 
        tls: true,
        tlsOptions: { rejectUnauthorized: false },
        mailbox: "Inbox", // mailbox to monitor 
        searchFilter: ["UNSEEN"], //["UNSEEN", "FLAGGED"], // the search filter being used after an IDLE notification has been retrieved 
        markSeen: true, // all fetched email willbe marked as seen and not fetched next time 
        fetchUnreadOnStart: true, // use it only if you want to get all unread email on lib start. Default is `false`, 
        mailParserOptions: {streamAttachments: true}, // options to be passed to mailParser lib. 
        attachments: false, // download attachments as they are encountered to the project directory 
        attachmentOptions: { directory: "attachments/" } // specify a download directory for attachments 
    });

    mailListener.start();

    mailListener.on("error", function(err){
      console.log('mailListener error: ', err);
		});

    mailListener.on("server:connected", function(){
        console.log("Mail listener initialized");
    });

    mailListener.on("mail", function(mail, seqno, attributes){
      // do something with mail object including attachments
      // console.log("emailParsed", mail);
      // mail processing code goes here
		});

    global.mailListener = mailListener;
    */
	},

	onCleanUp: function () {
	    // mailListener.stop();
  }
  
};
