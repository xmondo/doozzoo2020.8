import { browser, by, element, ProtractorBrowser, ProtractorExpectedConditions } from 'protractor';

// catches unhandled, so that they do not spoil terminal
process.on('unhandledRejection', (err) => {
    console.log('unhandledRejection', err);
});

describe('coach participants waiting', () => {
    const EC = ProtractorExpectedConditions;

    // To create a new browser with url as the first one
    const secondBrowser = browser.forkNewDriverInstance(true);

    browser.driver.executeScript(function() {
        return {
            width: window.screen.availWidth,
            height: window.screen.availHeight
        };
    }).then(function(result: any) {
        // browser.driver.manage().window().setSize(result.width/2,result.height/4*3);
        // browser.driver.manage().window().setPosition(0, 0);
        console.log(result);

        if (result.width / 2 > 960) {
            // set first browser size & position
            browser.manage().window().setSize(result.width / 2, result.height / 4 * 3); // w, h
            browser.manage().window().setPosition(0, 0); // left, top

            secondBrowser.manage().window().setSize(result.width / 2, result.height / 4 * 3); // w, h
            secondBrowser.manage().window().setPosition(result.width / 2, 0); // left, top
        } else {
            // set first browser size & position
            browser.manage().window().setSize(960, result.height / 4 * 3); // w, h
            browser.manage().window().setPosition(0, 0); // left, top

            secondBrowser.manage().window().setSize(960, result.height / 4 * 3); // w, h
            // if(result.width/2 )
            // secondBrowser.manage().window().setPosition(result.width - (960 * 0.75),0); // left, top
            secondBrowser.manage().window().setPosition(result.width - 960, 0);
        }
    });

    // clone element var to 2nd instance
    const element = browser.element;
    const element2 = secondBrowser.element;

    beforeEach( async () => {
        // browser.ignoreSynchronization = true;
        // TODO: this does the trick to accept clicks on submit etc.
        // await browser.waitForAngularEnabled(true);
    });

    afterEach( async () => {
        await browser.sleep(1000);
        await secondBrowser.sleep(1000);
        /*
        await browser.sleep(2000);
        await secondBrowser.sleep(2000);
        */
    });

    it('should login as coach1 and user2', async () => {
        //await browser.waitForAngularEnabled(true);
        await browser.get('/directlogin?u=coach1@mypelz.de&p=%23coach1');
        await browser.sleep(1000);

        //await secondBrowser.waitForAngularEnabled(true);
        await secondBrowser.get('/directlogin?u=user2@mypelz.de&p=%23user2');
        await secondBrowser.sleep(1000); 
  
    });

    it('should close cookies', async () => {

        // handle cookie
        const cookie = element(by.css('button[name="cookie-hint-button"]'));
        const status = await cookie.isPresent();
        // console.log('cookie status: ', status);
        if(status) {
            cookie.click();
        }

        const cookie2 = element2(by.css('button[name="cookie-hint-button"]'));
        const status2 = await cookie2.isPresent();
        // console.log('cookie status: ', status);
        if(status2) {
            cookie2.click();
        }
    });

    it('should go as user2 to "my coaches"', async () => {
        // await element(by.css('.startTango')).click();
        await element2(by.css('.startTango')).click();
    });

    it('participant should see coach-away-message', async () => {
        // await expect(element2(by.css('.messageNotInClassroom')).isDisplayed).toBeTruthy(); 
        // await expect(element2(by.css('.buttonGoToClassroom')).isEnabled).toBeFalsy();
        await expect(element2(by.cssContainingText('.my-coaches-row', 'coach1FirstName')).element(by.css('.buttonGoToClassroom')).getAttribute('disabled')).toBe('true');
    });

    it('coach should enter session room', async () => {
        await element(by.css('.startTango')).click();
        await browser.sleep(2000);
        await secondBrowser.sleep(2000);
    });

    it('participant should see join-message', async () => {
        // await expect(element2(by.css('.buttonGoToClassroom')).isEnabled).toBeTruthy();  
        // await expect(element2(by.cssContainingText('.my-coaches-row', 'coach1FirstName')).element(by.css('.buttonGoToClassroom')).getAttribute('disabled')).toBe('false');
        await expect(element2(by.cssContainingText('.my-coaches-row', 'coach1FirstName')).element(by.css('.buttonGoToClassroom')).isEnabled).toBeTruthy();
    });

    it('coach should lock room', async () => {
        browser.ignoreSynchronization = true;
        // await expect(element(by.css('.sessionLockButton')).isDisplayed).toBeTruthy();
        // await browser.wait(() => EC.prototype.visibilityOf(element(by.css('.sessionLockButton'))), 6000);
        const lockStatus = await element(by.cssContainingText('.sessionLockButton','lock')).getText();
        console.log(': ', lockStatus);
        if(lockStatus.indexOf('lock_open') !== -1) {
            await element(by.cssContainingText('.sessionLockButton','lock')).click();
        }
        // await expect(element(by.css('.buttonLockToggle .locked')).isDisplayed).toBeTruthy(); 
        // await expect(element(by.css('.sessionLockButton')).getText()).toContain('lock');  
        browser.ignoreSynchronization = false;
    });

    it('participant should show see lock-message', async () => {
        await expect(element2(by.css('.buttonKockKnock')).isDisplayed).toBeTruthy();  
    });

    it('participant should request access', async () => {
        await element2(by.css('.buttonKockKnock')).click();
    });

    it('coach should see request alert and admit', async () => {
        // await expect(element(by.css('.mat-snack-bar-container')).isDisplayed).toBeTruthy();
        await element(by.buttonText('Admit')).click();
    });

    it('participant should see join-message and click join', async () => {
        await expect(element2(by.cssContainingText('.my-coaches-row', 'coach1FirstName')).element(by.css('.buttonGoToClassroom')).isEnabled).toBeTruthy();
        await element2(by.cssContainingText('.my-coaches-row', 'coach1FirstName')).element(by.css('.buttonGoToClassroom')).click();
    });

    it('participant should land in session', async () => {
        await expect(secondBrowser.getCurrentUrl()).toContain('/room/');
    });

    it('coach should kick out participant', async () => {
        await element(by.css('.participantsSidenavButton')).click();
        await element(by.cssContainingText('.listMyStudentsRow', 'user2@mypelz.de')).element(by.css('.toggleAccess')).click();
        await element(by.css('.sidenavRightCloseButton')).click();
    });

    it('participant should see notification and ...', async () => {
        await expect(element2(by.css('.mat-snack-bar-container')).isDisplayed).toBeTruthy();
        // await element2(by.cssContainingText('.mat-simple-snackbar-action button','Admit')).click();
    });

    it('...participant should land on my-coaches', async () => {
        await expect(secondBrowser.getCurrentUrl()).toContain('/mycoaches/');
    });

    it('coach should end session...', async () => {
        await element(by.css('button[name="home"]')).click();
    });

/*

    it('participant should see hi coach row visible', async () => {
        const _el = element2(by.cssContainingText('li', 'coach1FirstName'));
        // const _el = element2(by.css('li.auth0|582c7e4cedb8c73048379aad'));
        // await secondBrowser.wait(() => EC.prototype.visibilityOf(_el), 6000);
        // await expect(_el.getText()).toContain('back in a second!');
        await expect(_el.getText()).toContain('coach1FirstName coach1LastName');

        // await browser.sleep(1000);
        await secondBrowser.sleep(1000);
    });

    it('coach1 should stop lesson, student button should be deactivated', async () => {

        //await browser.sleep(2000);
        //await secondBrowser.sleep(2000);

        // important element2->element<-(!!)
        const _el = element2(by.cssContainingText('li', 'coach1FirstName')).element(by.css('.buttonGoToClassroom'));
        // await browser.wait(() => EC.prototype.visibilityOf(_el), 6000);
        
        console.time('publish');
        _el.click();
        await browser.wait(() => EC.prototype.stalenessOf(element2(by.css('name["waitForPublisherSpinner"]'))), 6000);
        console.log('publisher is publishing...');
        console.timeEnd('publish');

        await browser.sleep(1000);
        await secondBrowser.sleep(1000);
    });

    it('user should go back to mycoaches and then rejoin the session', async () => {

        await secondBrowser.get('/mycoaches');

        await browser.sleep(2000);
        await secondBrowser.sleep(2000);

        const _el = element2(by.cssContainingText('li', 'coach1FirstName')).element(by.css('.buttonGoToClassroom'));
        // await browser.wait(() => EC.prototype.visibilityOf(_el), 6000);
        
        console.time('publish');
        _el.click();
        await browser.wait(() => EC.prototype.stalenessOf(element2(by.css('name["waitForPublisherSpinner"]'))), 6000);
        console.log('publisher is publishing...');
        console.timeEnd('publish');

        await browser.sleep(1000);
        await secondBrowser.sleep(1000);
    });


    it('user should go back to mycoaches and then rejoin the session', async () => {

        await secondBrowser.get('/mycoaches');

        await browser.sleep(2000);
        await secondBrowser.sleep(2000);

        const _el = element2(by.cssContainingText('li', 'coach1FirstName')).element(by.css('.buttonGoToClassroom'));
        console.time('publish');
        _el.click();
        await browser.wait(() => EC.prototype.stalenessOf(element2(by.css('name["waitForPublisherSpinner"]'))), 6000);
        console.log('publisher is publishing...');
        console.timeEnd('publish');

        await browser.sleep(1000);
        await secondBrowser.sleep(1000);
    });

    it('user should go back to mycoaches and then rejoin the session', async () => {

        await secondBrowser.get('/mycoaches');

        await browser.sleep(2000);
        await secondBrowser.sleep(2000);

        const _el = element2(by.cssContainingText('li', 'coach1FirstName')).element(by.css('.buttonGoToClassroom'));
        console.time('publish');
        _el.click();
        await browser.wait(() => EC.prototype.stalenessOf(element2(by.css('name["waitForPublisherSpinner"]'))), 6000);
        console.log('publisher is publishing...');
        console.timeEnd('publish');

        await browser.sleep(1000);
        await secondBrowser.sleep(1000);
    });

    it('user should go back to mycoaches and then rejoin the session', async () => {

        await secondBrowser.get('/mycoaches');

        await browser.sleep(2000);
        await secondBrowser.sleep(2000);

        const _el = element2(by.cssContainingText('li', 'coach1FirstName')).element(by.css('.buttonGoToClassroom'));
        console.time('publish');
        _el.click();
        await browser.wait(() => EC.prototype.stalenessOf(element2(by.css('name["waitForPublisherSpinner"]'))), 6000);
        console.log('publisher is publishing...');
        console.timeEnd('publish');

        await browser.sleep(1000);
        await secondBrowser.sleep(1000);
    });

    it('user should go back to mycoaches and then rejoin the session', async () => {

        await secondBrowser.get('/mycoaches');

        await browser.sleep(2000);
        await secondBrowser.sleep(2000);

        const _el = element2(by.cssContainingText('li', 'coach1FirstName')).element(by.css('.buttonGoToClassroom'));
        console.time('publish');
        _el.click();
        await browser.wait(() => EC.prototype.stalenessOf(element2(by.css('name["waitForPublisherSpinner"]'))), 6000);
        console.log('publisher is publishing...');
        console.timeEnd('publish');

        await browser.sleep(1000);
        await secondBrowser.sleep(1000);
    });
*/

});
