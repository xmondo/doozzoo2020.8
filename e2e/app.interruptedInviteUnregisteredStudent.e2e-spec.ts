import { browser, by, element, ProtractorBrowser, ProtractorExpectedConditions, Config } from 'protractor';

const MailListener = require('mail-listener2');

// here goes your email connection configuration
const mailListener = new MailListener({
  username: 'pelz@mypelz.de',
  password: 'SIEBEN#meilen?stiefel',
  host: 'imap.1und1.de',
  port: 993, // imap port
  tls: true,
  tlsOptions: { rejectUnauthorized: false },
  mailbox: 'Inbox', // mailbox to monitor
  searchFilter: ['UNSEEN'], // ["UNSEEN", "FLAGGED"], // the search filter being used after an IDLE notification has been retrieved
  markSeen: true, // all fetched email willbe marked as seen and not fetched next time
  fetchUnreadOnStart: true, // use it only if you want to get all unread email on lib start. Default is `false`,
  mailParserOptions: {streamAttachments: true}, // options to be passed to mailParser lib.
  attachments: false, // download attachments as they are encountered to the project directory
  attachmentOptions: { directory: 'attachments/' } // specify a download directory for attachments
});

mailListener.start();

mailListener.on('error', function(err) {
  console.log('mailListener error: ', err);
});

mailListener.on('server:connected', function() {
    console.log('Mail listener initialized');
});

mailListener.on('mail', function(mail, seqno, attributes) {
  // do something with mail object including attachments
  // console.log("emailParsed", mail);
  // mail processing code goes here
});

// helper
function getLastEmail() {
  // console.log('now waiting for an email...');
  return new Promise(resolve => {
    mailListener.on('mail', function(mail) {
      resolve(mail);
    });
  });
}

// catches unhandled, so that they do not spoil terminal
process.on('unhandledRejection', (err) => {
    console.log('unhandledRejection', err);
});

// global vars
let inviteUrl = null;
let registerUrl = null;

describe('invite unregistered student interrupted ->', () => {
    const EC = ProtractorExpectedConditions;
    const brwsr = ProtractorBrowser;
    beforeEach( async () => {
        browser.ignoreSynchronization = false;
        // browser.sleep(1000);
        // TODO: this does the trick to accept clicks on submit etc.
        await browser.waitForAngularEnabled(true);
    });

    afterEach( async () => {
        await browser.sleep(3000);
    });

    it('should delete user5 & dismiss cookie popup', async () => {
        await browser.get('/login?mode=logindelete&u=user5@mypelz.de&p=%23user5');
        // await browser.sleep(2000);
        const cookie = element(by.css('button[name="cookie-hint-button"]'));
        const status = await cookie.isPresent();
        if(status) {
            cookie.click();
        }
        await browser.sleep(2000);
    });

    it('should login as coach1', async () => {
        // browser.get('/login?mode=login&u=user5@mypelz.de&p=%23user5');
        await browser.get('/directlogin?u=coach1@mypelz.de&p=%23coach1');
        await browser.sleep(2000);
    });

    it('should go to "mystudents" and click tab 2', async () => {
        await browser.get('/mystudents');
        await browser.waitForAngular();
        await browser.wait(() => EC.prototype.urlContains('mystudents'), 5000);
        await browser.sleep(1000);

        await browser.wait(() => EC.prototype.visibilityOf(element(by.css('.mat-tab-labels'))), 6000);
        await expect(element(by.css('.mat-tab-labels')).isPresent()).toBeTruthy();       
        // click on second tab  
        await element(by.cssContainingText('div.mat-tab-label-content', 'Invite & connect students')).click();
        await browser.sleep(1000);
    });

    it('should create new invite links', async () => {
        const el = element(by.css('#listInviteLinks'));
        await browser.wait(() => EC.prototype.visibilityOf(el), 6000);
        // TODO: fragile: browser wait does not work in a stable way
        await browser.sleep(1000);

        const inviteLinkCount = await element.all(by.css('li span.inviteLink')).count();
        //console.log('inviteLinkCount before: ', inviteLinkCount);

        if (inviteLinkCount < 2 && inviteLinkCount > 0) {
            const _el = element(by.css('[name="createInviteLink"]'));
            await browser.wait(() => EC.prototype.visibilityOf(_el), 5000);
            await _el.click();
            const itemsAfter = await element.all(by.css('li span.inviteLink')).count();
            // list should show entry and have one more then before
            await expect(inviteLinkCount < itemsAfter).toBeTruthy();
        }

        const url = await element.all(by.css('span.inviteLink'))
            .last()
            .getText();
        // console.log('inviteUrl: ', url);
        inviteUrl = url;
    });

    it('coach should log out', async () => {
        await browser.get('/login?mode=logout');
        await browser.sleep(3000);
    });


    it('invitee should call the invite link and see coach1', async () => {
        // console.log(inviteUrl);
        await browser.get(inviteUrl);
        await browser.sleep(2000);

        let el = element(by.css('p.successMessage'));
        await browser.wait(() => EC.prototype.visibilityOf(el), 5000) // case sensitive!!
        const txt = await el.getText();
        await expect(txt.toLowerCase()).toContain('hello, you have been invited by coach');

        await browser.sleep(3000);
    });



    it('invitee should register as user5', async () => {
        // sign in as user2

        await element(by.css('button[name="register"]')).click();

        const user = 'user5@mypelz.de';
        const password = '#user5';

        let el = element(by.css('mat-icon[name="loginDialogCloseButton"]'));
        await browser.wait(() => EC.prototype.presenceOf(el), 6000);
        await browser.sleep(1000);

        await element(by.css('input[placeholder="E-Mail"]')).sendKeys(user);
        browser.sleep(1000);
        await element(by.css('input[placeholder="Please repeat your E-Mail"]')).sendKeys(user);
        browser.sleep(1000);
        await element(by.css('input[placeholder="password"]')).sendKeys(password);
        browser.sleep(1000);
        await element(by.css('input[placeholder="Please repeat your password"]')).sendKeys(password);

        await browser.sleep(1000);
        await element(by.css('button[name="registerSubmit"]')).click();

    });

    it('invitee should see feedback to continue in "My coaches"...', async () => {
        // console.log('TODO: success Message not identified...')
        await browser.sleep(1000);
    });


    it('should check for verification email', async () => {

        await browser
            // .controlFlow()
            .wait(getLastEmail())
            .then( async (email: any) => {
                // console.log('email is here: ', email);
                const user = 'user5@mypelz.de';
                const password = '%23user5'; // #->urlencoded
                const from = 'support@doozzoo.com';

                // console.log('email: ', email.to, emailfrom.address, email.from.name, email.subject, email.text);

                await expect(email.subject).toContain('Verify your email');
                await expect(email.headers.from).toContain(from);
                await expect(email.headers.to).toContain(user);

                // extract registration code from the email message
                const pattern = /(\boobCode=\b[a-zA-Z0-9&=\-_]*)/g;
                const regCode = pattern.exec(email.text);
                // console.log('extracted link: ', pattern, regCode, regCode[0]);
                await browser.sleep(1000);
                // expose link
                registerUrl = browser.params.baseUrl + 'login?mode=verifyEmail&' + regCode[0];

                mailListener.stop();
            });

    });

    it('should open verification link and get confirmation feedback', async () => {

        await browser.get(registerUrl);

        await  browser.sleep(2000);

        // const el = element(by.css('section[name="verifyEmail"]'));
        const el = element(by.css('p.successMessage'));
        await browser.wait(() => EC.prototype.presenceOf(el), 6000);
        await expect(el.getText()).toContain('Your account has been verified!');

        // top of profile form
        // p.warnMessage
        // Hi , please complete your profile to enjoy all doozzoo features. This is mandatory to complete your invite.

        await browser.sleep(2000);

    });

    it('should click "continue registration...', async () => {

        const el = element(by.css('a.verificationContinue'));
        await browser.wait(() => EC.prototype.presenceOf(el), 6000);
        el.click();

        await browser.sleep(2000);

    });


    it('should see Profile form for student role', async () => {
      const el = element(by.css('div[name="profile"]'));
      await browser.wait(() => EC.prototype.presenceOf(el), 6000);
      await browser.sleep(1000);
    });

    const rNum = Math.floor(Math.random() * 20);
    const firstname = `user5FirstName_${rNum}`;

    it('should fill in Profile form for student role, submit', async () => {

      let el = element(by.css('form[name="profileFormCustomer"]'));
      await browser.wait(() => EC.prototype.visibilityOf(el), 6000);

      //await element(by.css('mat-select[name="title"]')).sendKeys('Mr.');
      await element(by.css('input[name="firstname"]')).sendKeys(firstname);
      await element(by.css('input[name="lastname"]')).sendKeys('user5 lastname');
      //await element(by.css('input[placeholder="Nickname"]')).clear();
      //await element(by.css('input[placeholder="Nickname"]')).sendKeys(nickname);
      await element(by.css('mat-checkbox[name="dataApproval"]')).click();
      await browser.sleep(2000);

      el = element(by.css('button[name="profileFormCustomerSubmit"]'));
      await browser.wait(() => EC.prototype.elementToBeClickable(el), 6000);
      await el.click();

      await browser.sleep(1000);

    });

    it('should login as coach1', async () => {
        // browser.get('/login?mode=login&u=user5@mypelz.de&p=%23user5');
        await browser.get('/directlogin?u=coach1@mypelz.de&p=%23coach1');
        await browser.sleep(2000);
    });

    it('should go to "mystudents"', async () => {
        await browser.get('/mystudents');
        await browser.sleep(3000);
        await browser.wait(() => EC.prototype.visibilityOf(element(by.css('#listInviteLinks li'))), 6000);
        expect(element(by.css('#listInviteLinks')).getText()).toContain(firstname);
    });

// end of class
});


