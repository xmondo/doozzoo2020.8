import { browser, by, element, ProtractorBrowser, ProtractorExpectedConditions } from 'protractor';

describe('home landing content', () => {
    const EC = ProtractorExpectedConditions;

    // To create a new browser with url as the first one
    const secondBrowser = browser.forkNewDriverInstance(true);

    browser.driver.executeScript(function() {
        return {
            width: window.screen.availWidth,
            height: window.screen.availHeight
        };
    }).then(function(result: any) {
        // browser.driver.manage().window().setSize(result.width/2,result.height/4*3);
        // browser.driver.manage().window().setPosition(0, 0);
        console.log(result);

        if (result.width / 2 > 960) {
            // set first browser size & position
            browser.manage().window().setSize(result.width / 2, result.height / 4 * 3); // w, h
            browser.manage().window().setPosition(0, 0); // left, top

            secondBrowser.manage().window().setSize(result.width / 2, result.height / 4 * 3); // w, h
            secondBrowser.manage().window().setPosition(result.width / 2, 0); // left, top
        } else {
            // set first browser size & position
            browser.manage().window().setSize(960, result.height / 4 * 3); // w, h
            browser.manage().window().setPosition(0, 0); // left, top

            secondBrowser.manage().window().setSize(960, result.height / 4 * 3); // w, h
            // if(result.width/2 )
            // secondBrowser.manage().window().setPosition(result.width - (960 * 0.75),0); // left, top
            secondBrowser.manage().window().setPosition(result.width - 960, 0);
        }
    });

    // clone element var to 2nd instance
    const element = browser.element;
    const element2 = secondBrowser.element;

    beforeEach( async () => {
        browser.ignoreSynchronization = false;
        // TODO: this does the trick to accept clicks on submit etc.
        await browser.waitForAngularEnabled(true);
    });

    afterEach( async () => {
        await browser.sleep(2000);
        await secondBrowser.sleep(2000);
    });

    it('should show open two browsers', async () => {
        await browser.get('/');
        // await secondBrowser.get('/');
    });

    it('should login as coach1 and user2', async () => {
        await browser.get('/login?mode=login&u=coach1@mypelz.de&p=%23coach1');
        // await secondBrowser.get('/login?mode=login&u=user2@mypelz.de&p=%23user2');

        // coach and user2 goes to home
        await element(by.css('button[name="home"]')).click();
        // await element2(by.css('button[name="home"]')).click();

    });

    it('should show text for coach user', async () => {
        const el = element(by.css('.mat-title.landingContent'));
        expect(el.getText()).toContain('Hey coach mypelz, thanx for logging in!');
        // const _el = element2(by.css('.mat-title.landingContent'));
        // expect(_el.getText()).toContain('Hey user2, thanx for logging in!');
    });

    it('should go as coach to room and as user2 to "my coaches"', async () => {
        await element(by.css('.startTango')).click();
        // await element2(by.css('.startTango')).click();
    });

    /*
    it('should see coach line visible, but not clickable', async () => {
        const _el = element2(by.cssContainingText('li', 'coach1FirstName'));
        await browser.wait(() => EC.prototype.visibilityOf(_el), 6000);
        await expect(_el.getText()).toContain('I\'m back in a second!');
    });
*/

    it('coach1 should start lesson', async () => {
        // const el = element.all(by.css('.roomControls button'));
        await browser.wait(() => EC.prototype.visibilityOf(element(by.css('button[name="toggleLessonState"]'))), 6000);
        await element(by.css('button[name="toggleLessonState"]'))
            .click()
            .then(result => {
                console.log('###', result);
            });
        // await browser.wait(() => EC.prototype.visibilityOf(el), 6000);
        // await el.click();
        await browser.sleep(2000);
        await secondBrowser.sleep(2000);
    });

    
    /*

    it('user2 should see the meeting room button turn active', async () => {
        const _el = element2(by.cssContainingText('li', 'coach1FirstName'));
        // await browser.wait(() => EC.prototype.visibilityOf(_el), 6000);
        await expect(_el.getText()).not.toContain('I\'m back in a second!');
        await secondBrowser.sleep(2000);
    });

    it('should show text for role coach', async () => {
        const el = element(by.css('.mat-title.landingContent'));
        expect(el.getText()).toContain('Hey coach mypelz, thanx for logging in!');
        await browser.sleep(2000);
    });

    */

});
