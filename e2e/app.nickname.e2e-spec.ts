import { browser, by, element, ProtractorBrowser, ProtractorExpectedConditions } from 'protractor';

describe('nickname', () => {
    const EC = ProtractorExpectedConditions;
    beforeEach( async () => {
        browser.ignoreSynchronization = false;
        // TODO: this does the trick to accept clicks on submit etc.
        await browser.waitForAngularEnabled(true);
    });
  
    afterEach( async () => {
        await browser.sleep(1000);
    });

    it('should dismiss cookie hint popup', async () => {
        await browser.get('/');
        const cookie = element(by.css('button[name="cookie-hint-button"]'));
        const status = await cookie.isPresent();
        if(status) {
            cookie.click();
        }
        // await browser.sleep(1000);
    });

    it('should login as coach1', async () => {
        await browser.get('/login?mode=login&u=coach1@mypelz.de&p=%23coach1');
        await browser.sleep(1000);
        await element(by.css('button[name="home"]')).click();
        await browser.sleep(1000);
    });

    it('should click profile button', async () => {
        // await element(by.css('button[name="profile"]')).click();
        await browser.get('/login');
        await browser.sleep(1000);
    });

    it('should see Profile form', async () => {
        const el = element(by.css('div[name="profile"]'));
        await browser.wait(() => EC.prototype.presenceOf(el), 6000);
        // await browser.sleep(1000);
    });

    let nickname = 'myNick' + Math.floor(Math.random() * 100).toString();
    it('should change nickname', async () => {
        await element(by.css('input[formControlName="nickname"]')).clear();
        await element(by.css('input[formControlName="nickname"]')).sendKeys(nickname);
        const el = element(by.css('button[name="profileFormCoachSubmit"]'));
        await browser.wait(() => EC.prototype.elementToBeClickable(el), 6000);
        el.click();
        await browser.sleep(1000);
    });

    it('should click home button', async () => {
        await browser.get('/');
        // await element(by.css('button[name="home"]')).click();
        await browser.sleep(1000);
    });

    it('should see nickname in text', async () => {
        // const el = element(by.css('.mat-title .landingContent'));
        const el = element(by.css('.moduleContainer'));
        await browser.wait(() => EC.prototype.visibilityOf(el), 6000);
        await expect(el.getText()).toContain(nickname);
        await browser.sleep(1000);
    });

    it('should go as coach to room ', async () => {
        await element(by.css('.startTango')).click();
        await browser.sleep(3000);
    });
/*
    it('should see the nickname in video header', async () => {
        // const el = element(by.css('h1.OT_name .videoUserName'));
        const el = element(by.css('body'));
        // await browser.wait(() => EC.prototype.visibilityOf(el), 6000);
        await expect(el.getText()).toContain(nickname);
        // await browser.sleep(1000);
    });

    it('should click home button', async () => {
        await element(by.css('button[name="home"]')).click();
        // await browser.sleep(1000);
    });

    it('should click profile button', async () => {
        await element(by.css('button[name="profile"]')).click();
        // await browser.sleep(1000);
    });

    it('should see Profile form', async () => {
        const el = element(by.css('div[name="profile"]'));
        await browser.wait(() => EC.prototype.presenceOf(el), 6000);
        // await browser.sleep(1000);
    });

    it('should change nickname', async () => {
        await element(by.css('input[formControlName="nickname"]')).clear();
        await element(by.css('input[formControlName="nickname"]')).sendKeys('coach1');
        const el = element(by.css('button[name="profileFormCoachSubmit"]'));
        await browser.wait(() => EC.prototype.elementToBeClickable(el), 6000);
        el.click();
        // await browser.sleep(1000);
    });

    */

});
