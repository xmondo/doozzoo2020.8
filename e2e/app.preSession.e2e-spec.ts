import { browser, by, element, ProtractorBrowser, ProtractorExpectedConditions, promise } from 'protractor';

// global vars
let inviteUrl = null;

describe('Pre session check ->', () => {
    const EC = ProtractorExpectedConditions;
    const brwsr = ProtractorBrowser;
    beforeEach( async () => {
        browser.ignoreSynchronization = false;
        // browser.sleep(1000);
        // TODO: this does the trick to accept clicks on submit etc.
        await browser.waitForAngularEnabled(true);
    });

    afterEach( async () => {
        await browser.sleep(1000);
    });

    // ...
    it('should login as coach1', async () => {
        await browser.get('/directlogin?u=coach1@mypelz.de&p=%23coach1');
        
        // handle cookie message
        const cookie = element(by.css('button[name="cookie-hint-button"]'));
        const status = await cookie.isPresent();
        // console.log('cookie status: ', status);
        if(status) {
            cookie.click();
        }
        await browser.sleep(2000);
    });

    it('should click on pre session check and land on the page', async () => {
        element(by.css('button[name="preflight"]')).click();
        await browser.sleep(1000); 
        await expect(element(by.css('h2')).getText()).toContain('Pre session check');    
    });

    // headphone mode should be pre selected
    it('should see headphone mode pre selected', async () => {
        const el = element(by.cssContainingText('.mat-radio-button', 'Headphone Mode'));
        await expect(el.getAttribute('class')).toContain('mat-radio-checked'); 
        await expect(element(by.css('mat-checkbox#echoCancellation')).getAttribute('class')).not.toContain('mat-checkbox-checked');       
        await browser.sleep(1000);     
    });

    it('should change selection to speaker mode', async () => {
        const el = element(by.cssContainingText('.mat-radio-button', 'Speaker Mode'));
        el.click();
        await browser.sleep(1000);
        await expect(element(by.css('mat-checkbox#echoCancellation')).getAttribute('class')).toContain('mat-checkbox-checked'); 
    });

    it('should change selection of audio inputs', async () => {
        const el = element(by.cssContainingText('mat-radio-button', 'Fake Audio Input 2'));
        el.click();
        await browser.sleep(1000); 
    });

    it('should see audio bitrate pre selected as 64000', async () => {
        await expect(element(by.css('mat-select')).getText()).toContain('64000');
        element(by.css('mat-select')).click();
        await browser.sleep(1000);
        element(by.cssContainingText('.mat-option-text', '192000')).click();
        await browser.sleep(1000);
        await expect(element(by.css('mat-select')).getText()).toContain('192000');
    });

    it('should reload page by clicking "Save settings"', async () => {
        element(by.css('a[name="save-settings"]')).click();
        await browser.sleep(3000);
        await browser.wait(() => EC.prototype.visibilityOf(element(by.css('mat-checkbox#echoCancellation'))), 6000);
        await browser.sleep(3000);
        // setting should be remembered
        await expect(element(by.css('mat-checkbox#echoCancellation')).getAttribute('class')).toContain('mat-checkbox-checked');
        await expect(element(by.css('mat-checkbox#noiseSuppression')).getAttribute('class')).not.toContain('mat-checkbox-checked');
        await expect(element(by.css('mat-checkbox#autoGainControl')).getAttribute('class')).not.toContain('mat-checkbox-checked');

        await expect(element(by.cssContainingText('mat-radio-button', 'Fake Audio Input 2')).getAttribute('class')).toContain('mat-radio-checked');

        await expect(element(by.css('mat-select')).getText()).toContain('192000');
    });

});


