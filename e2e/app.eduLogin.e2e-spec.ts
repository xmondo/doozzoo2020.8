import { browser, by, element, ProtractorBrowser, ProtractorExpectedConditions, ElementFinder } from 'protractor';

// catches unhandled, so that they do not spoil terminal
process.on('unhandledRejection', (err) => {
    console.log('unhandledRejection', err);
});

describe('home login dialog', () => {
    // const EC = ProtractorExpectedConditions;
    beforeEach(() => {
        // browser.ignoreSynchronization = true;
        // browser.sleep(1000);
    });

    afterEach(async () => {
        await browser.sleep(1000);
    });

    // ### user role student ###
    it('should login as user2', async () => {
        await browser.get('/directlogin?u=customer5@mypelz.de&p=%23customer5');
        await browser.sleep(1000);
        await element(by.css('button[name="home"]')).click();
        await browser.sleep(2000);
    });

    it('should check for cookie hint popup', async () => {
        const cookie = element(by.css('button[name="cookie-hint-button"]'));
        const status = await cookie.isPresent();
        // console.log('cookie status: ', status);
        if(status) {
            cookie.click();
        }
        await browser.sleep(2000);
    });

/*
    it('should click "login...', async () => {
        await browser.wait(() => element(by.css('button[name="login"]')).isDisplayed(), 6000);
        element(by.css('button[name="login"]')).click();
        await browser.sleep(1000);
    });
  
    it('should fill in register form and submit', async () => {
        const user = 'customer5@mypelz.de';
        const password = '#customer5';
        let el = element(by.css('mat-icon[name="loginDialogCloseButton"]'));
        await browser.wait(() => el.isDisplayed, 6000);
        browser.sleep(1000);
        await element(by.css('input[name="email"]')).sendKeys(user);
        await element(by.css('input[name="password"]')).sendKeys(password);
        browser.sleep(1000);
        await element(by.css('button[name="loginSubmit"]')).click();
    });
*/



    it('should go to EDU landing page', async () => {
        await browser.get('/edu');
        await browser.sleep(3000);
        await browser.waitForAngular();
    });


    it('should see edu login', async () => {
        // browser.ignoreSynchronization = false;
        
        await browser.sleep(1000);
        await expect(element(by.cssContainingText('button[name="toEduBackend"]', 'Access institution backoffice ')).isDisplayed()).toBeTruthy();
        element(by.cssContainingText('button[name="toEduBackend"]', 'Access institution backoffice ')).click();
        await browser.sleep(1000);

    });

    it('should click first backoffice button', async () => {

        element.all(by.css('button[name="edu-backoffice"]')).first().click();
        await browser.sleep(2000);

        await expect(element(by.css('.moduleContainer')).getText()).toContain('Manage institutional accounts');

    });

// end of class
});
