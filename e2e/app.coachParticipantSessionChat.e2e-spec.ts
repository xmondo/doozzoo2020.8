import { browser, by, element, ProtractorBrowser, ProtractorExpectedConditions } from 'protractor';
import { protractor } from 'protractor/built/ptor';

// catches unhandled, so that they do not spoil terminal
process.on('unhandledRejection', (err) => {
    console.log('unhandledRejection', err);
});

describe('coach and participant chat', () => {
    const EC = ProtractorExpectedConditions;

    // To create a new browser with url as the first one
    const secondBrowser = browser.forkNewDriverInstance(true);

    browser.driver.executeScript(function() {
        return {
            width: window.screen.availWidth,
            height: window.screen.availHeight
        };
    }).then(function(result: any) {
        // browser.driver.manage().window().setSize(result.width/2,result.height/4*3);
        // browser.driver.manage().window().setPosition(0, 0);
        console.log(result);

        if (result.width / 2 > 960) {
            // set first browser size & position
            browser.manage().window().setSize(result.width / 2, result.height / 4 * 3); // w, h
            browser.manage().window().setPosition(0, 0); // left, top

            secondBrowser.manage().window().setSize(result.width / 2, result.height / 4 * 3); // w, h
            secondBrowser.manage().window().setPosition(result.width / 2, 0); // left, top
        } else {
            // set first browser size & position
            browser.manage().window().setSize(960, result.height / 4 * 3); // w, h
            browser.manage().window().setPosition(0, 0); // left, top

            secondBrowser.manage().window().setSize(960, result.height / 4 * 3); // w, h
            // if(result.width/2 )
            // secondBrowser.manage().window().setPosition(result.width - (960 * 0.75),0); // left, top
            secondBrowser.manage().window().setPosition(result.width - 960, 0);
        }
    });

    // clone element var to 2nd instance
    const element = browser.element;
    const element2 = secondBrowser.element;

    beforeEach( async () => {
        // browser.ignoreSynchronization = true;
        // TODO: this does the trick to accept clicks on submit etc.
        // await browser.waitForAngularEnabled(true);
    });

    afterEach( async () => {
        await browser.sleep(1000);
        await secondBrowser.sleep(1000);
    });

    it('should show open two browsers', async () => {
        await browser.get('/');
        await secondBrowser.get('/');

        // handle cookie
        const cookie = element(by.css('button[name="cookie-hint-button"]'));
        const status = await cookie.isPresent();
        // console.log('cookie status: ', status);
        if(status) {
            cookie.click();
        }

        const cookie2 = element2(by.css('button[name="cookie-hint-button"]'));
        const status2 = await cookie2.isPresent();
        // console.log('cookie status: ', status);
        if(status2) {
            cookie2.click();
        }
    });

    it('should login as coach1 and user2', async () => {
        await browser.get('/directlogin?u=coach1@mypelz.de&p=%23coach1');
        // await browser.sleep(1000);
        await secondBrowser.get('/directlogin?u=user2@mypelz.de&p=%23user2');
        // await secondBrowser.sleep(1000)

        await browser.sleep(1000);
        await secondBrowser.sleep(1000); 
   
    });

    it('should go as coach to room and as user2 to "my coaches"', async () => {
        await element(by.css('.startTango')).click();
        // await element2(by.css('.startTango')).click();
        await browser.sleep(1000);
    });

    it('should go as coach to room and as user2 to "my coaches"', async () => {
        // const url = await browser.getCurrentUrl();
        browser.ignoreSynchronization = true;
        await browser.wait(() => EC.prototype.urlContains('/room/'), 6000);
        const url = await browser.getCurrentUrl();
        await secondBrowser.get(url);
        await secondBrowser.sleep(1000);
    });

    it('check for chat buttons visible and click them coach', async () => {
        browser.ignoreSynchronization = true;
        await browser.wait(async () => await element(by.css('button[name="chat"]')).isPresent(), 12000);
        await secondBrowser.wait(async () => await element2(by.css('button[name="chat"]')).isPresent(), 12000);
        await element(by.css('button[name="chat"]')).click();
        await expect(await element(by.css('.sidenavContentRight .session-chat')).isPresent()).toBeTruthy();
        await element2(by.css('button[name="chat"]')).click(); 
        await expect(await element2(by.css('.sidenavContentRight .session-chat')).isPresent()).toBeTruthy();
        await browser.sleep(1000);
        await secondBrowser.sleep(1000); 
    });

    it('send chat messages and check', async () => {
        browser.ignoreSynchronization = true;
        await element(by.css('input[name="chat-input"]')).sendKeys('hallo welt user2'); 
        await browser.actions().sendKeys(protractor.Key.ENTER).perform();  
        await element2(by.css('input[name="chat-input"]')).sendKeys('hallo welt coach1'); 
        await secondBrowser.actions().sendKeys(protractor.Key.ENTER).perform();  
        await browser.sleep(1000);
        await secondBrowser.sleep(1000); 
        // await expect(element(by.css('.chat-content')).getText().toContain('')
        await expect(await element(by.css('.chat-content')).getText()).toContain('hallo welt coach1');
        await expect(await element2(by.css('.chat-content')).getText()).toContain('hallo welt user2');
    });
/*
    it('close one pane and open again', async () => {
        browser.ignoreSynchronization = true;
        element2(by.css('.sidenavContentRight button[name="close"]')).click();
        await browser.sleep(1000);
        await secondBrowser.sleep(1000); 
    });

    it('send messages from coach', async () => {
        await element(by.css('input[name="chat-input"]')).sendKeys('hallo welt again user2'); 
        await browser.actions().sendKeys(protractor.Key.ENTER).perform();  
        await browser.sleep(1000);
        await secondBrowser.sleep(1000);
    });

    it('open user 2chat again and see the new messages', async () => {
        secondBrowser.ignoreSynchronization = true;
        await secondBrowser.wait(async () => await element2(by.css('button[name="chat"]')).isPresent(), 8000);
        await element2(by.css('button[name="chat"]')).click();
        await expect(await element2(by.css('.sidenavContentRight .session-chat')).isPresent()).toBeTruthy();
        await expect(await element2(by.css('.chat-content')).getText()).toContain('hallo welt again user2');
        await browser.sleep(1000);
        await secondBrowser.sleep(1000); 
    });
*/
    // check for sidepanes -> .sidenavContentRight .session-chat

    // send text -> input[name="chat-input"] send keys

    // check if text is visible at the counterpart windows -> .chat-content to Contain Text

});
