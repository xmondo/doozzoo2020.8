import { browser, by, element, ProtractorBrowser, ProtractorExpectedConditions } from 'protractor';

describe('home landing content', () => {
    const EC = ProtractorExpectedConditions;
    beforeEach( async () => {
        browser.ignoreSynchronization = false;
        // TODO: this does the trick to accept clicks on submit etc.
        await browser.waitForAngularEnabled(true);
    });
  
    afterEach( async () => {
        await browser.sleep(1000);
    });

    it('should dismiss cookie hint popup', async () => {
        await browser.get('/');
        const cookie = element(by.css('button[name="cookie-hint-button"]'));
        const status = await cookie.isPresent();
        if(status) {
            cookie.click();
        }
        await browser.sleep(1000);
    });

    // ### user role anonymous ###
    it('should show text for anonymous user', async () => {
        // result in array
        const content = await element.all(by.css('.landingContent')).getText();
        console.log(typeof content);
        const strContent = content.toString();
        // content.fin
        expect(strContent.indexOf('To start or join a session, login first!') !== -1).toBeTruthy();
        expect(strContent.indexOf('New user? Please, register first!') !== -1).toBeTruthy();
        expect(strContent.indexOf('Test drive dozzoo - click here') !== -1).toBeTruthy();
    });

    // ### user role student ###
    it('should login as user2', async () => {
        await browser.get('/directlogin?u=user2@mypelz.de&p=%23user2');
        await browser.sleep(1000);
        await element(by.css('button[name="home"]')).click();
        await browser.sleep(2000);
    });

    it('should show text for role student', async () => {
        await browser.wait(() => EC.prototype.visibilityOf(element(by.css('.landingContent'))), 6000);
        expect(element(by.css('.landingContent')).getText()).toContain('Hey user2, thanks for logging in!');
        await browser.sleep(1000);
    });

    it('user2 should log out', async () => {
        await browser.get('/login?mode=logout');
    });

    // ### user role coach ###
    it('should login as coach1', async () => {
        await browser.get('/directlogin?u=coach1@mypelz.de&p=%23coach1');
        await browser.sleep(1000);
    });

    it('should show text for role coach', async () => {
        const el = element(by.css('.mat-title.landingContent'));
        expect(el.getText()).toContain('Hey coach');
        expect(el.getText()).toContain(', thanks for logging in!');
        await browser.sleep(2000);
    });

    it('coach should log out', async () => {
        await browser.get('/login?mode=logout');
    });


    // ### user institutional admin ###
    it('should login as institutional admin', async () => {
        await browser.get('/directlogin?u=customer5@mypelz.de&p=%23customer5');
        await browser.sleep(1000);
    });

    it('should show text for role institutional admin', async () => {
        const el = element(by.css('.mat-title.landingContent'));
        expect(el.getText()).toContain('Please click below to access your institution backoffice');
        await browser.sleep(2000);
    });

    it('institutional admin should log out', async () => {
        await browser.get('/login?mode=logout');
    });

    // ### user institutional coach ###
    it('should login as institutional coach', async () => {
        await browser.get('/directlogin?u=customer1@mypelz.de&p=%23customer1');
        await browser.sleep(2000);
    });

    it('should show text for role institutional coach', async () => {
        const el = element(by.css('.mat-title.landingContent'));
        expect(el.getText()).toContain('Hey coach customer1, thanks for logging in!');
        expect(element(by.css('app-institution-profile')).isPresent()).toBeTruthy();
        await browser.sleep(2000);
    });

    it('institutional coach should log out', async () => {
        await browser.get('/login?mode=logout');
    });

    // ### user institutional student ###
    it('should login as institutional student', async () => {
        await browser.get('/directlogin?u=customer3@mypelz.de&p=%23customer3');
        await browser.sleep(2000);
    });

    it('should show text for role iinstitutional student', async () => {
        const el = element(by.css('.mat-title.landingContent'));
        expect(el.getText()).toContain('Hey customer3, thanks for logging in!');
        expect(element(by.css('app-institution-profile')).isPresent()).toBeTruthy();
        await browser.sleep(2000);
    });

    it('institutional student should log out', async () => {
        await browser.get('/login?mode=logout');
    });

});
