import { browser, by, element, ProtractorBrowser, ProtractorExpectedConditions } from 'protractor';

describe('test drive', () => {
    const EC = ProtractorExpectedConditions;
    beforeEach( async () => {
        browser.ignoreSynchronization = false;
        // TODO: this does the trick to accept clicks on submit etc.
        await browser.waitForAngularEnabled(true);
    });
  
    afterEach( async () => {
        await browser.sleep(1000);
    });

    it('should dismiss cookie hint popup', async () => {
        await browser.get('/');
        const cookie = element(by.css('button[name="cookie-hint-button"]'));
        const status = await cookie.isPresent();
        if(status) {
            cookie.click();
        }
    });

    // issues with routing
    // protractor des not recognize any content
    it('should click button "test drive"', async () => {
        element(by.css('button[name="testdrive"]'))
            .click();
        await browser.sleep(1000); 
        //await browser.wait(EC.prototype.visibilityOf(element(by.css('h2'))), 6000);
        //await expect(element(by.css('h2'))).toContain('Your doozzoo test drive for free');
        await browser.sleep(1000);
    });

    it('should click button "Tango button"', async () => {
        await browser.get('/testdrive');
        await browser.sleep(1000); 
        await browser.wait(() => element(by.css('button[name="tangoButton"]')).isEnabled());
        element(by.css('.startTango')).click();
        await browser.sleep(1000); 
        // const url = await browser.getCurrentUrl();
        // expect(url).toContain('/room/');
    });

    /*
    // finded keinen button
    it('should click home and land on home again', async () => {
        // Wait for spinner to stop
        // await browser.wait(() => EC.prototype.stalenessOf(element(by.css('.waitForPublisherSpinner'))), 6000);
        await browser.sleep(1000);
        await element(by.cssContainingText('button','home')).click();
        browser.ignoreSynchronization = true;
        await browser.sleep(2000);
        
        const url = await browser.getCurrentUrl();
        expect(url).toContain('/');
        await browser.sleep(5000); 
        
    });
    */

// end of class
});
