import { browser, by, element, ProtractorBrowser, ProtractorExpectedConditions, promise } from 'protractor';

describe('test debugger', () => {
    const EC = ProtractorExpectedConditions;
    const brwsr = ProtractorBrowser;
    beforeEach(() => {
        browser.ignoreSynchronization = true;
        // browser.sleep(1000);
        // TODO: this does the trick to accept clicks on submit etc.
        browser.waitForAngularEnabled(true);
    });

    afterEach(() => {
        browser.sleep(1000);
    });

    it('should go to home', async () => {
        // debugger;
        await browser.get('/');
        const el = element(by.css('html > body > app-root > app-home > div > div > span'));
        await el.getText().then(result => console.log(result));
        // debugger;
        await expect(el.getText()).toContain('Please log in!');
        browser.sleep(3000);
    });

});


