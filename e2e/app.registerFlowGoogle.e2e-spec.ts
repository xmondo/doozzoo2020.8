import { browser, by, element, ProtractorBrowser, ProtractorExpectedConditions, promise } from 'protractor';

const MailListener = require('mail-listener2');

// here goes your email connection configuration
const mailListener = new MailListener({
  username: 'pelz@mypelz.de',
  password: 'SIEBEN#meilen?stiefel',
  host: 'imap.1und1.de',
  port: 993, // imap port
  tls: true,
  tlsOptions: { rejectUnauthorized: false },
  mailbox: 'Inbox', // mailbox to monitor
  searchFilter: ['UNSEEN'], // ["UNSEEN", "FLAGGED"], // the search filter being used after an IDLE notification has been retrieved
  markSeen: true, // all fetched email willbe marked as seen and not fetched next time
  fetchUnreadOnStart: true, // use it only if you want to get all unread email on lib start. Default is `false`,
  mailParserOptions: {streamAttachments: true}, // options to be passed to mailParser lib.
  attachments: false, // download attachments as they are encountered to the project directory
  attachmentOptions: { directory: 'attachments/' } // specify a download directory for attachments
});

mailListener.start();

mailListener.on('error', function(err) {
  console.log('mailListener error: ', err);
});

mailListener.on('server:connected', function() {
    console.log('Mail listener initialized');
});

mailListener.on('mail', function(mail, seqno, attributes) {
  // do something with mail object including attachments
  // console.log("emailParsed", mail);
  // mail processing code goes here
});

// helper
function getLastEmail() {
  console.log('now waiting for an email...');
  return new Promise(resolve => {
    mailListener.on('mail', function(mail) {
      resolve(mail);
    });
  });
}

// global vars
let registerUrl = null;

describe('now homeLogin', () => {
  const EC = ProtractorExpectedConditions;
  beforeEach(() => {
    browser.ignoreSynchronization = true;

    // TODO: this does the trick to accept clicks on submit etc.
    browser.waitForAngularEnabled(true);
  });

  afterEach( async () => {
   await  browser.sleep(1000);
  });

  it('should delete user5', async () => {
    await browser.get('/login?mode=logindelete&u=user5@mypelz.de&p=%23user5');
    await browser.sleep(2000);
  });

  it('should click login and open login dialog', async () => {
    await browser.get('/');
    element(by.css('button[name="login"]'))
      .click();
    await browser.sleep(2000);
  });

  it('should click google login', async () => {
    await element(by.css('button[name="loginProviderGoogle"]'))
      .click();
    await browser.sleep(8000);
  });

  

/*
  it('should fill in register form and submit', () => {
    const user = 'user5@mypelz.de';
    const password = '#user5';
    let el = element(by.css('mat-icon[name="loginDialogCloseButton"]'));
    browser.wait(() => EC.prototype.presenceOf(el), 6000);
    browser.sleep(1000);
    element(by.css('input[placeholder="E-Mail"]')).sendKeys(user);
    element(by.css('input[placeholder="password"]')).sendKeys(password);
    browser.sleep(1000);
    element(by.css('button[name="registerSubmit"]')).click();
    browser.sleep(3000);

    el = element(by.css('.successMessage.register'));
    browser.wait(() => EC.prototype.presenceOf(el), 6000);
    // browser.wait(EC.prototype.textToBePresentInElement(el, 'Your account has been created and a verification email was send'), 6000);
    expect(el.getText()).toContain('Your account has been created and a verification email was send');
  });

  it('should check for verification email', function () {

    browser.controlFlow()
      .wait(getLastEmail())
      .then((email: any) => {
        // console.log('email is here: ', email);
        const user = 'user5@mypelz.de';
        const password = '%23user5'; // #->urlencoded
        const from = 'support@doozzoo.com';

        // console.log('email: ', email.to, emailfrom.address, email.from.name, email.subject, email.text);

        expect(email.subject).toContain('Verify your email');
        expect(email.headers.from).toContain(from);
        expect(email.headers.to).toContain(user);

        // extract registration code from the email message
        const pattern = /(\bhttps:\/\/dev.doozzoo.com\/#\/auth\?mode=verifyEmail&oobCode=\b[a-zA-Z0-9&=\-_]*)/g;
        // const pattern = /(\bhttps:\/\/localhost:4200\/#\/auth\?mode=verifyEmail&oobCode=\b[a-zA-Z0-9&=\-_]*)/g;
        // const pattern = browser.params.regexPattern[1]; // the second one
        const regCode = pattern.exec(email.text);
        // console.log('extracted link: ', pattern, regCode, regCode[0]);
        browser.sleep(1000);
        // expose link
        registerUrl = regCode[0];

        mailListener.stop();
      });

    });

    it('should open verification link and get confirmation feedback', () => {

      registerUrl = registerUrl.replace('dev.doozzoo.com/#/auth', 'localhost:4200/login');

      browser.get(registerUrl);

      browser.sleep(2000);

      // const el = element(by.css('section[name="verifyEmail"]'));
      const el = element(by.css('p.successMessage'));
      browser.wait(() => EC.prototype.presenceOf(el), 6000);
      expect(el.getText()).toContain('Your account has been verified!');

      browser.sleep(2000);

    });

    it('should see Profile form for student role', () => {
      const el = element(by.css('div[name="profile"]'));
      browser.wait(() => EC.prototype.presenceOf(el), 6000);
    });


    it('should fill in Profile form for student role, submit', () => {

      let el = element(by.css('form[name="profileFormCustomer"]'));
      browser.wait(() => EC.prototype.visibilityOf(el), 6000);

      element(by.css('mat-select[name="title"]')).sendKeys('Mr.');
      element(by.css('input[name="firstname"]')).sendKeys('user5 firstname');
      element(by.css('input[name="lastname"]')).sendKeys('user5 lastname');
      element(by.css('mat-checkbox[name="dataApproval"]')).click();
      browser.sleep(3000);

      el = element(by.css('button[name="profileFormCustomerSubmit"]'));
      browser.wait(() => EC.prototype.elementToBeClickable(el), 6000);
      el.click();

    });

    // Hi, you are sucessfully registered as "Student".
    it('should see success feedback', () => {

      const el = element(by.css('p.successMessage.student'));
      browser.wait(() => EC.prototype.presenceOf(el), 6000);
      expect(el.getText()).toContain('Hi, you are sucessfully registered as "Student".');

    });
*/
});


