import { browser, by, element, ProtractorBrowser, ProtractorExpectedConditions, promise } from 'protractor';

// catches unhandled, so that they do not spoil terminal
process.on('unhandledRejection', (err) => {
    console.log('unhandledRejection', err);
});

// global vars
let inviteUrl = null;

describe('invite registered student, logged in ->', () => {
    const EC = ProtractorExpectedConditions;
    const brwsr = ProtractorBrowser;
    beforeEach( async () => {
        browser.ignoreSynchronization = false;
        // browser.sleep(1000);
        // TODO: this does the trick to accept clicks on submit etc.
        await browser.waitForAngularEnabled(true);
    });

    afterEach( async () => {
        await browser.sleep(1000);
    });

    // TODO: script does not wait for proper login
    it('should login as coach1', async () => {
        await browser.get('/directlogin?u=coach1@mypelz.de&p=%23coach1');
        
        // handle cookie message
        const cookie = element(by.css('button[name="cookie-hint-button"]'));
        const status = await cookie.isPresent();
        // console.log('cookie status: ', status);
        if(status) {
            cookie.click();
        }
        await browser.sleep(2000);
    });

    it('should go to "mystudents" and click tab 2', async () => {
        await browser.get('/mystudents');
        await browser.waitForAngular();
        await browser.wait(() => EC.prototype.urlContains('mystudents'), 5000);
        await browser.sleep(1000);

        await browser.wait(() => EC.prototype.visibilityOf(element(by.css('.mat-tab-labels'))), 6000);
        await expect(element(by.css('.mat-tab-labels')).isPresent()).toBeTruthy();       
        // click on second tab  
        await element(by.cssContainingText('div.mat-tab-label-content', 'Invite & connect students')).click();
        await browser.sleep(1000);
    });

    it('should create new invite links', async () => {
        const el = element(by.css('#listInviteLinks'));
        await browser.wait(() => EC.prototype.visibilityOf(el), 6000);
        // TODO: fragile: browser wait does not work in a stable way
        await browser.sleep(1000);

        const inviteLinkCount = await element.all(by.css('li span.inviteLink')).count();
        //console.log('inviteLinkCount before: ', inviteLinkCount);

        if (inviteLinkCount < 2 && inviteLinkCount > 0) {
            const _el = element(by.css('[name="createInviteLink"]'));
            await browser.wait(() => EC.prototype.visibilityOf(_el), 5000);
            await _el.click();
            const itemsAfter = await element.all(by.css('li span.inviteLink')).count();
            // list should show entry and have one more then before
            await expect(inviteLinkCount < itemsAfter).toBeTruthy();
        }

        const url = await element.all(by.css('span.inviteLink'))
            .last()
            .getText();
        // console.log('inviteUrl: ', url);
        inviteUrl = url;
    });

    it('coach should log out', async () => {
        await browser.get('/login?mode=logout');
    });

    it('should login as user2', async () => {
        await browser.get('/login?mode=login&u=user2@mypelz.de&p=%23user2');
        await browser.sleep(1000);
    });

    it('invitee should call the invite link and see coach1', async () => {
        // console.log(inviteUrl);
        await browser.get(inviteUrl);
        await browser.sleep(2000);
        const successText = await element(by.css('p.successMessage')).getText();
        expect(successText.toLowerCase()).toContain('visit my coaches to directly join your coach');
    });

});


