import { browser, by, element, ProtractorBrowser, ProtractorExpectedConditions } from 'protractor';

describe('screensharing', () => {
  const EC = ProtractorExpectedConditions;
  beforeEach(() => {
    // browser.ignoreSynchronization = false;
    browser.sleep(1000);
  });

  afterEach( async () => {
    await browser.sleep(1000);
  });

  it('should enter start page and click create room...', async () => {
    // Wait for the publishers to load
    // ### user role coach ###
    await browser.get('/directlogin?u=coach1@mypelz.de&p=%23coach1');
    await browser.sleep(1000);

    const cookie = element(by.css('button[name="cookie-hint-button"]'));
    const status = await cookie.isPresent();
    // console.log('cookie status: ', status);
    if(status) {
        cookie.click();
    }
    await browser.wait(() => element(by.css('button[name="login"]')).isEnabled(), 6000);
    // expect(foo.isEnabled()).toBe(false);
    element(by.css('button[name="tangoButton"]')).click();

    await browser.sleep(2000);
  });

  /*
  it('should land on the room page', async () => {
    // Wait for the publishers to load
    const url = await browser.getCurrentUrl();
    expect(url).toContain('/room');

    await browser.sleep(2000);
  });

  
  it('should see the toolbar spinner', async () => {
    // await browser.wait(() => element(by.css('.mat-spinner[name="waitForPublisherSpinner"]')).isPresent(), 6000);
    // expect(element(by.css('[name="waitForPublisherSpinner"]')).isPresent()).toBe(true);
    await browser.sleep(5000);
  });
  */

  it('should click screenshare button', async () => {
    // browser.ignoreSynchronization = true;
    // await browser.waitForAngular();
    // await browser.wait(() => EC.prototype.stalenessOf(element(by.css('[name="waitForPublisherSpinner"]'))), 12000);
    // await browser.wait(() => EC.prototype.visibilityOf(element(by.css('button[name="startShareScreen"]'))), 12000);
    // await browser.wait(() => element(by.css('button[name="startShareScreen"]')).isPresent(), 12000); 
    //await element(by.css('[name="startShareScreen"]')).click();
    await browser.waitForAngular();
    await element(by.css('.my-share-button')).click();
    
    await browser.sleep(2000);
  });

  it('should see shared screen', async () => {
    await browser.sleep(2000);
    // await browser.wait(() => EC.prototype.visibilityOf(element(by.css('[name="waitForPublisherSpinner"]'))), 12000);
    // await browser.wait(() => element(by.css('button[name="stopShareScreen"]')).isPresent(), 12000); 
    element(by.css('[name="stopShareScreen"]')).click();

    await browser.sleep(10000);
  });

  

});
