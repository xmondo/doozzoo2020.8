import { browser, by, element, ProtractorBrowser, ProtractorExpectedConditions, promise } from 'protractor';

const MailListener = require('mail-listener2');

// here goes your email connection configuration
const mailListener = new MailListener({
  username: 'pelz@mypelz.de',
  password: 'SIEBEN#meilen?stiefel',
  host: 'imap.1und1.de',
  port: 993, // imap port
  tls: true,
  tlsOptions: { rejectUnauthorized: false },
  mailbox: 'Inbox', // mailbox to monitor
  searchFilter: ['UNSEEN'], // ["UNSEEN", "FLAGGED"], // the search filter being used after an IDLE notification has been retrieved
  markSeen: true, // all fetched email willbe marked as seen and not fetched next time
  fetchUnreadOnStart: true, // use it only if you want to get all unread email on lib start. Default is `false`,
  mailParserOptions: {streamAttachments: true}, // options to be passed to mailParser lib.
  attachments: false, // download attachments as they are encountered to the project directory
  attachmentOptions: { directory: 'attachments/' } // specify a download directory for attachments
});

mailListener.start();

mailListener.on('error', function(err) {
  console.log('mailListener error: ', err);
});

mailListener.on('server:connected', function() {
    console.log('Mail listener initialized');
});

mailListener.on('mail', function(mail, seqno, attributes) {
  // do something with mail object including attachments
  // console.log("emailParsed", mail);
  // mail processing code goes here
});

// helper
function getLastEmail() {
  console.log('now waiting for an email...');
  return new Promise(resolve => {
    mailListener.on('mail', function(mail) {
      resolve(mail);
    });
  });
}

// catches unhandled, so that they do not spoil terminal
process.on('unhandledRejection', (err) => {
  console.log('unhandledRejection', err);
});

let registerUrl;

describe('login reset password', () => {
  const EC = ProtractorExpectedConditions;
  beforeEach(() => {
    // browser.ignoreSynchronization = false;
  });

  afterEach( async () => {
    await browser.sleep(1000);
  });

it('should check for cookie hint popup', async () => {
    await browser.get('/directlogin?u=user2@mypelz.de&p=%23user2');
    //await browser.get('/');
    const cookie = element(by.css('button[name="cookie-hint-button"]'));
    const status = await cookie.isPresent();
    // console.log('cookie status: ', status);
    if(status) {
        cookie.click();
    }
    await browser.sleep(2000);
});

// cookie hint window should disapear
it('cookie hint popup has disapeared', async () => {
    await browser.wait(() => EC.prototype.stalenessOf(element(by.css('.cookie-hint'))), 6000);
    await browser.sleep(3000);
});

/*
it('should login as user2', async () => {
    await browser.get('/directlogin?u=user2@mypelz.de&p=%23user2');
    await browser.sleep(2000);
});
*/

it('should click account menu button in footer and select "reset"', async () => {
    element(by.css('button[name="footerLogout"]')).click();
    await browser.sleep(1000);
    const el = element(by.cssContainingText('.mat-menu-item', 'Reset password'));
    await el.click();
    await browser.sleep(1000);
  });


  it('should fill in reset password form', async () => {
    const email = 'user2@mypelz.de';
    // const email = '#user2';
    const el = element(by.css('mat-icon[name="loginDialogCloseButton"]'));
    await browser.wait(() => EC.prototype.presenceOf(el), 6000);
    // browser.sleep(1000);

    await element(by.css('input[name="E-Mail_forgotten"]')).clear();
    await element(by.css('input[name="E-Mail_forgotten"]')).sendKeys(email);
    browser.sleep(1000);
    await element(by.css('input[name="E-Mail_forgotten_repeat"]')).sendKeys(email);
    browser.sleep(1000);
    await element(by.css('button[name="resetPassword"]')).click();
    await browser.sleep(1000);

    // const _el = element(by.css('.successMessage.resetPassword'));
    // await expect(_el.getText()).toContain('Reset E-Mail has been send, please close popup window and check your mailbox!');

    await browser.sleep(1000);
  });

  it('should check for verification email', async () => {

    await browser
        .controlFlow()
        .wait(getLastEmail())
        .then(async (email: any) => {
            // console.log('email is here: ', email);
            const user = 'user2@mypelz.de';
            const password = '%23user2'; // #->urlencoded
            const from = 'support@doozzoo.com';

            // console.log('email: ', email.to, emailfrom.address, email.from.name, email.subject, email.text);

            expect(email.subject).toContain('Reset your password');
            expect(email.headers.from).toContain(from);
            expect(email.headers.to).toContain(user);

            // extract registration code from the email message
            const pattern = /(\boobCode=\b[a-zA-Z0-9&=\-_]*)/g;
            const regCode = pattern.exec(email.text);
            // console.log('extracted link: ', pattern, regCode, regCode[0]);
            await browser.sleep(1000);
            // expose link
            registerUrl = browser.params.baseUrl + 'login?mode=resetPassword&' + regCode[0];

            mailListener.stop();
        });

    });

    it('should open reset link and get final reset form', async () => {

      await browser.get(registerUrl);
      await browser.sleep(1000);

      const el = element(by.css('.mat-title'));
      await expect(el.getText()).toContain('Reset password');

      await browser.sleep(1000);

    });

    it('should fill in final reset password form', async () => {
        const email = 'user2@mypelz.de';
        const password = '#user2'; // #->urlencoded

        await element(by.css('input[name="password"]')).sendKeys(password);
        // browser.sleep(1000);
        await element(by.css('button[name="resetPassword"]')).click();
        await browser.sleep(1000);
    
        const _el = element(by.css('.successMessage'));
        await expect(_el.getText()).toContain('Your password has been updated!');
    
        await browser.sleep(1000);
      });

});


