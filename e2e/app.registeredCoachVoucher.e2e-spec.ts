import { browser, by, element, ProtractorBrowser, ProtractorExpectedConditions, promise } from 'protractor';

// catches unhandled, so that they do not spoil terminal
process.on('unhandledRejection', (err) => {
    console.log('unhandledRejection', err);
});

describe('registered coach voucher ->', () => {
    const EC = ProtractorExpectedConditions;
    const brwsr = ProtractorBrowser;
    beforeEach(async () => {
        browser.ignoreSynchronization = false;
        // browser.sleep(1000);
        // TODO: this does the trick to accept clicks on submit etc.
        await browser.waitForAngularEnabled(true);
    });

    afterEach( async () => {
        await browser.sleep(1000);
    });

    it('should login as coach1', async () => {
        await browser.get('/directlogin?u=coach1@mypelz.de&p=%23coach1');
        await browser.sleep(2000);

        // handle cookie message
        const cookie = element(by.css('button[name="cookie-hint-button"]'));
        const status = await cookie.isPresent();
        // console.log('cookie status: ', status);
        if(status) {
            cookie.click();
        }
        await browser.sleep(2000);
    });

    it('should click Profile button', async () => {
        await browser.wait(() => EC.prototype.presenceOf(element(by.css('button[name="profile"]'))), 8000);
        element(by.css('button[name="profile"]')).click();
        await browser.sleep(1000);
    });

    it('should see Profile form', async () => {
        await browser.wait(() => EC.prototype.presenceOf(element(by.css('div[name="profile"]'))), 8000);
        await browser.sleep(1000);
    });

    it('should see Voucher input form', async () => {
        await browser.wait(() => EC.prototype.presenceOf(element(by.css('div.voucherPane'))), 8000);
        await browser.sleep(1000);
    });

    it('should enter a falsy code and get error', async () => {
        browser.ignoreSynchronization = true;
        await element(by.css('input[name="voucherInput"]')).sendKeys('blahblah');
        await browser.sleep(3000);
        await browser.wait(() => EC.prototype.presenceOf(element(by.css('mat-hint[name="voucherHint"]'))), 8000);
        expect(element(by.css('mat-hint[name="voucherHint"]')).getText()).toContain('Your coupon code is not valid. Please try again...');
        await browser.sleep(1000);
    });

    it('should enter a existing code and get success', async () => {
        browser.ignoreSynchronization = true;
        await element(by.css('input[name="voucherInput"]')).clear();
        await browser.sleep(1000);
        await element(by.css('input[name="voucherInput"]')).sendKeys('edgJHhxe');
        await browser.sleep(3000);
        await browser.wait(() => EC.prototype.presenceOf(element(by.css('.youSave>span[name="voucherName"]'))), 18000);
        expect(element(by.css('.youSave>span[name="voucherName"]')).getText()).toContain('20% off (unlim.)');
        await browser.sleep(1000);
    });

    let newSelection;
    it('should click "change plan" and see subscription form', async () => {
        browser.ignoreSynchronization = true;

        await browser.wait(() => EC.prototype.presenceOf(element(by.css('button[name="changePlan"]'))), 8000);
        await browser.executeScript("arguments[0].scrollIntoView();", 
            element(by.css('button[name="changePlan"]'))
            .getWebElement());
        await element(by.css('button[name="changePlan"]')).click();
        await browser.sleep(1000);

        const el = element(by.css('mat-select[name="package"]'));
        await browser.wait(() => EC.prototype.presenceOf(el), 8000);
        await el.click();
        await browser.sleep(2000);

        const currentSelection = await element(by.css('mat-select[name="package"] .mat-select-value')).getText();
        const optionArr = ['5','10','15','20','25'];
        newSelection = optionArr.find(item => item.indexOf(currentSelection) === -1);
        // console.log('new selection: ', newSelection, currentSelection);

        const matches = await element.all(by.cssContainingText('.mat-option-text', newSelection));
        await matches[0].click();
        await browser.sleep(2000);

        await browser.wait(() => EC.prototype.presenceOf(element(by.css('button[name="choosePlan2"]'))), 8000);
        await element(by.css('button[name="choosePlan2"]')).click();
        await browser.sleep(2000);

        await browser.wait(() => EC.prototype.presenceOf(element(by.css('button[name="subscribeNow"]'))), 8000);
        await element(by.css('button[name="subscribeNow"]')).click();
        await browser.sleep(2000);

        // await browser.wait(async () => EC.prototype.visibilityOf(await element(by.css('div.overlay'))), 18000);
        // expect(element(by.css('div.overlay')).getText()).toContain('Your subscription has been updated!');
        await browser.sleep(2000);
    });

    it('should close overlay and show updated subscription', async () => {
        browser.ignoreSynchronization = true;

        const el = await element(by.css('div.overlay'))
        await browser.wait(async () => EC.prototype.stalenessOf(el), 8000);
        
        await browser.wait(async () => EC.prototype.presenceOf(await element(by.css('span[name="numberOfStudents"]'))), 8000);
        expect(element(by.css('span[name="numberOfStudents"]')).getText()).toContain('Up to ' + newSelection + ' students');
        
        // await browser.sleep(4000);
    });

// end of class
});


