import { browser, by, element, ProtractorBrowser, ProtractorExpectedConditions, ElementFinder } from 'protractor';

// catches unhandled, so that they do not spoil terminal
process.on('unhandledRejection', (err) => {
    console.log('unhandledRejection', err);
});

describe('home login dialog', () => {
    // const EC = ProtractorExpectedConditions;
    beforeEach(() => {
        // browser.ignoreSynchronization = true;
        // browser.sleep(1000);
    });

    afterEach(async () => {
        await browser.sleep(1000);
    });

    // ### user role student ###
    it('should login as user2', async () => {
        await browser.get('/directlogin?u=customer5@mypelz.de&p=%23customer5');
        await browser.sleep(1000);
        await element(by.css('button[name="home"]')).click();
        await browser.sleep(2000);
    });

    it('should check for cookie hint popup', async () => {
        const cookie = element(by.css('button[name="cookie-hint-button"]'));
        const status = await cookie.isPresent();
        // console.log('cookie status: ', status);
        if(status) {
            cookie.click();
        }
        await browser.sleep(2000);
    });

    it('should go to Heinsberg institution page', async () => {
        await browser.get('/edu/rdOJPTDs2SyYLVXolF5y');
        await browser.sleep(3000);
        await browser.waitForAngular();
    });

    it('should see Heinsberg', async () => {
        // browser.ignoreSynchronization = false;      
        await browser.sleep(1000);
        await expect(element.all(by.css('.mat-headline')).first().getText()).toContain('Kreismusikschule Heinsberg');
        await browser.sleep(1000);
    });

    it('should find item row customer1 and see the label coach', async () => {

        const el = element(by.cssContainingText('itemRow', 'customer1@mypelz.de'));
        await expect(el.getText()).toContain('Coach');
        await browser.sleep(5000);

    });

// end of class
});
