import { browser, by, element, ProtractorBrowser, ProtractorExpectedConditions, promise } from 'protractor';

const MailListener = require('mail-listener2');

// here goes your email connection configuration
const mailListener = new MailListener({
  username: 'pelz@mypelz.de',
  password: 'SIEBEN#meilen?stiefel',
  host: 'imap.1und1.de',
  port: 993, // imap port
  tls: true,
  tlsOptions: { rejectUnauthorized: false },
  mailbox: 'Inbox', // mailbox to monitor
  searchFilter: ['UNSEEN'], // ["UNSEEN", "FLAGGED"], // the search filter being used after an IDLE notification has been retrieved
  markSeen: true, // all fetched email willbe marked as seen and not fetched next time
  fetchUnreadOnStart: true, // use it only if you want to get all unread email on lib start. Default is `false`,
  mailParserOptions: {streamAttachments: true}, // options to be passed to mailParser lib.
  attachments: false, // download attachments as they are encountered to the project directory
  attachmentOptions: { directory: 'attachments/' } // specify a download directory for attachments
});

mailListener.start();

mailListener.on('error', function(err) {
  console.log('mailListener error: ', err);
});

mailListener.on('server:connected', function() {
    console.log('Mail listener initialized');
});

mailListener.on('mail', function(mail, seqno, attributes) {
  // do something with mail object including attachments
  // console.log("emailParsed", mail);
  // mail processing code goes here
});

// helper
function getLastEmail() {
  // console.log('now waiting for an email...');
  return new Promise(resolve => {
    mailListener.on('mail', function(mail) {
      resolve(mail);
    });
  });
}

// catches unhandled, so that they do not spoil terminal
process.on('unhandledRejection', (err) => {
  console.log('unhandledRejection', err);
});

// global vars
let registerUrl = null;

describe('register flow coach interrupted', () => {
  const EC = ProtractorExpectedConditions;
  beforeEach(() => {
    browser.ignoreSynchronization = false;
    // browser.sleep(1000);

    // TODO: this does the trick to accept clicks on submit etc.
    browser.waitForAngularEnabled(true);
  });

  afterEach( async () => {
    await browser.sleep(3000);
  });

  it('should delete coach2', async () => {
    await browser.get('/login?mode=logindelete&u=coach2@mypelz.de&p=%23coach2');
    await browser.sleep(4000);
  });

  it('should dismiss cookie hint popup', async () => {
    const cookie = element(by.css('button[name="cookie-hint-button"]'));
    const status = await cookie.isPresent();
    if(status) {
        cookie.click();
    }
  });

  it('should click login and open login dialog', async () => {
    await browser.get('/');
    await element.all(by.css('button[name="register"]'))
      .first()
      .click();
  });

  it('should fill in register form and submit', async () => {
    const user = 'coach2@mypelz.de';
    const password = '#coach2';
    let el = element(by.css('mat-icon[name="loginDialogCloseButton"]'));
    await browser.wait(() => EC.prototype.presenceOf(el), 6000);
    // browser.sleep(1000);
    await element(by.css('input[name="email"]')).sendKeys(user);
    await element(by.css('input[name="emailRepeated"]')).sendKeys(user);
    await element(by.css('input[name="password"]')).sendKeys(password);
    await element(by.css('input[name="passwordRepeated"]')).sendKeys(password);
    browser.sleep(1000);
    await element(by.css('button[name="registerSubmit"]')).click();
  });

  it('should check for verification email', async () => {

    await browser
      // .controlFlow()
      .wait(getLastEmail())
      .then(async (email: any) => {
        // console.log('email is here: ', email);
        const user = 'coach2@mypelz.de';
        const password = '%23coach2'; // #->urlencoded
        const from = 'support@doozzoo.com';

        // console.log('email: ', email.to, emailfrom.address, email.from.name, email.subject, email.text);

        await expect(email.subject).toContain('Verify your email');
        await expect(email.headers.from).toContain(from);
        await expect(email.headers.to).toContain(user);

        // extract registration code from the email message
        const pattern = /(\boobCode=\b[a-zA-Z0-9&=\-_]*)/g;
        const regCode = pattern.exec(email.text);
        // console.log('extracted link: ', pattern, regCode, regCode[0]);
        await browser.sleep(1000);
        // expose link
        registerUrl = browser.params.baseUrl + 'login?mode=verifyEmail&' + regCode[0];

        mailListener.stop();
      });

    });


  it('should open verification link and get confirmation feedback', async () => {

    await browser.get(registerUrl);

    // const el = element(by.css('section[name="verifyEmail"]'));
    const el = element(by.css('p.successMessage'));
    await browser.wait(() => EC.prototype.presenceOf(el), 6000);
    await expect(el.getText()).toContain('Your account has been verified!');
  });

  // interrupt


  it('restart browser and go to hopepage', async () => {
    await browser.restart();
    await browser.sleep(1000);
    
    await browser.get('/');

    await browser.sleep(3000);
  });

  it('should click login and open login dialog', async () => {
    await browser.get('/');
    await element(by.css('button[name="login"]'))
      .click();
  });

  it('should fill in register form and submit', async () => {

    const user = 'coach2@mypelz.de';
    const password = '#coach2';
    let el = element(by.css('mat-icon[name="loginDialogCloseButton"]'));
    await browser.wait(() => EC.prototype.presenceOf(el), 6000);
    // browser.sleep(1000);
    await element(by.css('input[name="email"]')).sendKeys(user);
    await element(by.css('input[name="password"]')).sendKeys(password);
    browser.sleep(1000);
    await element(by.css('button[name="loginSubmit"]')).click();
    
    await browser.sleep(10000);

  });

  it('should see Profile form', async () => {
    const el = element(by.css('div[name="profile"]'));
    await browser.wait(() => EC.prototype.presenceOf(el), 6000);
    await browser.sleep(3000);
  });

/*
  it('should select Profile form for coach role', async () => {
    await element.all(by.css('.mat-radio-label-content')).get(1).click();
    let el = element(by.css('form[value="profileFormCoach"]'));
    await browser.wait(() => EC.prototype.visibilityOf(el), 6000);
    await browser.sleep(1000);
  });

  it('should set a random avatar', async () => {
    await browser.wait(() => EC.prototype.presenceOf(element(by.css('.avatarIcon'))), 6000);
    element(by.css('.avatarIcon')).click();
    await browser.sleep(1000);

    const rNum = Math.floor(Math.random() * 20);
    const imgSelector = `img[src="assets/img/avatars/avataaars_${rNum}.svg"]`;
    element(by.css(imgSelector)).click();
    await browser.sleep(1000);
  });

  it('should fill in Profile form and submit', async () => {

    //element.all(by.css('.mat-radio-label-content')).get(1).click();
    //let el = element(by.css('form[value="profileFormCoach"]'));
    //browser.wait(() => EC.prototype.visibilityOf(el), 6000);

    await element(by.css('input[formControlName="firstname"]')).sendKeys('coach2 first name');
    await element(by.css('input[formControlName="lastname"]')).sendKeys('coach2 lastname');
    await element(by.css('input[formControlName="nickname"]')).sendKeys('coach2nick');

    await element(by.css('input[formControlName="address_street1"]')).sendKeys('Baker Street');
    await element(by.css('input[formControlName="zipcode"]')).sendKeys('456789');
    await element(by.css('input[formControlName="city"]')).sendKeys('Antwerp');

    await element(by.css('input[formControlName="birthday"]')).sendKeys('23.04.1956');
    await element(by.css('input[formControlName="phone"]')).sendKeys('+49 241 888999');

    await element(by.css('mat-select[formControlName="country"]')).click().then(() => {
        element(by.cssContainingText('.mat-option-text', 'Belgium')).click();
    });

    await browser.executeScript("arguments[0].scrollIntoView();", element(by.css('mat-checkbox[formControlName="dataApproval"]')).getWebElement());
    await element(by.css('mat-checkbox[formControlName="dataApproval"]')).click();
    // await browser.sleep(1000);

    const el = element(by.css('button[name="profileFormCoachSubmit"]'));
    browser.wait(() => EC.prototype.elementToBeClickable(el), 6000);
    el.click();
  });

  // Hi, you are sucessfully registered as "Coach".
  it('should see success feedback', async () => {
    const el = element(by.css('p.successMessage.coachRequestPending'));
    await browser.wait(() => EC.prototype.presenceOf(el), 6000);
    await expect(el.getText()).toContain('Please complete your subscription below');
    await browser.sleep(4000);
  });

  it('should see a loading hint', async () => {
    const status = await browser.wait(async () => await element(by.css('.waitForSubscriptionModule')).isPresent(), 6000);
    // console.log('.waitForSubscriptionModule: ', status);
    if(status){
      await browser.executeScript("arguments[0].scrollIntoView();", element(by.css('.waitForSubscriptionModule')).getWebElement());
      expect(element(by.css('.waitForSubscriptionModule')).isPresent()).toBeTruthy();
    }  
  });
*/

// end of class
});


