import { browser, by, element, ProtractorBrowser, ProtractorExpectedConditions, promise } from 'protractor';

const MailListener = require('mail-listener2');

// here goes your email connection configuration
const mailListener = new MailListener({
    username: 'pelz@mypelz.de',
    password: 'SIEBEN#meilen?stiefel',
    host: 'imap.1und1.de',
    port: 993, // imap port
    tls: true,
    tlsOptions: { rejectUnauthorized: false },
    mailbox: 'Inbox', // mailbox to monitor
    searchFilter: ['UNSEEN'], // ["UNSEEN", "FLAGGED"], // the search filter being used after an IDLE notification has been retrieved
    markSeen: true, // all fetched email willbe marked as seen and not fetched next time
    fetchUnreadOnStart: true, // use it only if you want to get all unread email on lib start. Default is `false`,
    mailParserOptions: { streamAttachments: true }, // options to be passed to mailParser lib.
    attachments: false, // download attachments as they are encountered to the project directory
    attachmentOptions: { directory: 'attachments/' } // specify a download directory for attachments
});

mailListener.start();

mailListener.on('error', function (err) {
    console.log('mailListener error: ', err);
});

mailListener.on('server:connected', function () {
    console.log('Mail listener initialized');
});

mailListener.on('mail', function (mail, seqno, attributes) {
    // do something with mail object including attachments
    // console.log("emailParsed", mail);
    // mail processing code goes here
});

// helper
function getLastEmail() {
    // console.log('now waiting for an email...');
    return new Promise(resolve => {
        mailListener.on('mail', function (mail) {
            resolve(mail);
        });
    });
}

// catches unhandled, so that they do not spoil terminal
process.on('unhandledRejection', (err) => {
    console.log('unhandledRejection', err);
});

// global vars
let registerUrl = null;

describe('home register flow - ', () => {
    const EC = ProtractorExpectedConditions;
    beforeEach(() => {
        // browser.ignoreSynchronization = false;
        // browser.sleep(1000);

        // TODO: this does the trick to accept clicks on submit etc.
        browser.waitForAngularEnabled(true);
    });

    afterEach(async () => {
        await browser.sleep(2000);
    });

    it('should delete user5', async () => {
        await browser.get('/login?mode=logindelete&u=user5@mypelz.de&p=%23user5');
        await browser.sleep(3000);
    });

    it('should dismiss cookie hint popup', async () => {
        await browser.get('/');
        const cookie = element(by.css('button[name="cookie-hint-button"]'));
        const status = await cookie.isPresent();
        if (status) {
            cookie.click();
        }
        await browser.sleep(2000);
    });

    it('should click login and open login dialog', async () => {
        await element.all(by.css('button[name="register"]'))
            .first()
            .click();
    });

    it('should fill in register form and submit', async () => {
        const user = 'user5@mypelz.de';
        const password = '#user5';
        let el = element(by.css('mat-icon[name="loginDialogCloseButton"]'));

        await browser.wait(() => EC.prototype.presenceOf(el), 6000);
        browser.sleep(1000);
        await element(by.css('input[name="email"]')).sendKeys(user);
        browser.sleep(1000);
        await element(by.css('input[name="emailRepeated"]')).sendKeys(user);
        browser.sleep(1000);
        await element(by.css('input[name="password"]')).sendKeys(password);
        browser.sleep(1000);
        await element(by.css('input[name="passwordRepeated"]')).sendKeys(password);
        browser.sleep(1000);
        await element(by.css('button[name="registerSubmit"]')).click();

        await browser.sleep(3000);

        // new browser instance
        browser.restart();

    });

    it('should go to root and dismiss cookie hint popup', async () => {
        await browser.get('/');
        const cookie = element(by.css('button[name="cookie-hint-button"]'));
        const status = await cookie.isPresent();
        if (status) {
            cookie.click();
        }
        await browser.sleep(2000);
    });

    it('should click "login...', async () => {
        await browser.wait(() => element(by.css('button[name="login"]')).isDisplayed(), 6000);
        element(by.css('button[name="login"]')).click();
        await browser.sleep(1000);
    });

    it('should fill in register form and submit', async () => {
        const user = 'user5@mypelz.de';
        const password = '#user5';
        let el = element(by.css('mat-icon[name="loginDialogCloseButton"]'));
        await browser.wait(() => el.isDisplayed, 6000);
        browser.sleep(1000);
        await element(by.css('input[name="email"]')).sendKeys(user);
        await element(by.css('input[name="password"]')).sendKeys(password);
        browser.sleep(1000);
        await element(by.css('button[name="loginSubmit"]')).click();

        browser.sleep(1000);
    });

    it('should see error message -> verify first...', async () => {
        browser.waitForAngularEnabled(true);
        
        const user = 'user5@mypelz.de';

        expect(element(by.css('p.errorMessage')).getText()).toContain('To complete your registration');

        browser.sleep(1000);
        await element(by.css('input[name="submitEmail"]')).clear();
        await element(by.css('input[name="submitEmail"]')).sendKeys(user);
        await element(by.css('input[name="submitEmailRepeated"]')).sendKeys(user);
        browser.sleep(1000);
        await element(by.css('button[name="verifySubmit"]')).click();

        console.log('verify form completed...')
        browser.sleep(4000);

    });

    it('should check for verification email', async () => {
        await browser.sleep(3000);
        await browser.controlFlow()
            .wait(getLastEmail())
            .then(async (email: any) => {
            // console.log('email is here: ', email);
            const user = 'user5@mypelz.de';
            const password = '%23user5'; // #->urlencoded
            const from = 'support@doozzoo.com';

            // console.log('email: ', email.to, emailfrom.address, email.from.name, email.subject, email.text);

            await expect(email.subject).toContain('Verify your email');
            await expect(email.headers.from).toContain(from);
            await expect(email.headers.to).toContain(user);

            // extract registration code from the email message
            const pattern = /(\boobCode=\b[a-zA-Z0-9&=\-_]*)/g;
            const regCode = pattern.exec(email.text);
            // console.log('extracted link: ', pattern, regCode, regCode[0]);
            await browser.sleep(1000);
            // expose link
            registerUrl = browser.params.baseUrl + 'login?mode=verifyEmail&' + regCode[0];

            mailListener.stop();
        });
    });

    /*
    
    it('should open verification link and get confirmation feedback', async () => {

        await browser.get(registerUrl);
    
        // const el = element(by.css('section[name="verifyEmail"]'));
        const el = element(by.css('p.successMessage'));
        await browser.wait(() => EC.prototype.presenceOf(el), 6000);
        await expect(el.getText()).toContain('Your account has been verified!');

        var successEn = EC.prototype.textToBePresentInElement(el, 'Hi, you are sucessfully registered');

    });

    it('should click "continue registration...', async () => {
        // element(by.css('a.verificationContinue'));
        // await browser.wait(() => EC.prototype.presenceOf(element(by.css('a.verificationContinue'))), 8000);
        element(by.css('a.verificationContinue')).click();

        await browser.sleep(1000);

    });

    it('should see Profile form for student role', async () => {
    const el = element(by.css('div[name="profile"]'));
    await browser.wait(() => EC.prototype.presenceOf(el), 6000);
    await browser.sleep(3000);
    });

    it('should set a random avatar', async () => {
    await browser.wait(() => EC.prototype.presenceOf(element(by.css('.avatarIcon'))), 6000);
    element(by.css('.avatarIcon')).click();
    await browser.sleep(1000);

    const rNum = Math.floor(Math.random() * 20);
    const imgSelector = `img[src="assets/img/avatars/avataaars_${rNum}.svg"]`;
    element(by.css(imgSelector)).click();
    await browser.sleep(1000);
    });

    it('should fill in Profile form for student role, submit', async () => {

    let el = element(by.css('form[name="profileFormCustomer"]'));
    await browser.wait(() => EC.prototype.visibilityOf(el), 6000);

    // element(by.css('mat-select[name="title"]')).sendKeys('Mr.');
    element(by.css('input[name="firstname"]')).sendKeys('user5 firstname');
    element(by.css('input[name="lastname"]')).sendKeys('user5 lastname');
    element(by.css('mat-checkbox[name="dataApproval"]')).click();
    await browser.sleep(1000);

    el = element(by.css('button[name="profileFormCustomerSubmit"]'));
    browser.executeScript("arguments[0].scrollIntoView();", el.getWebElement());

    await browser.wait(() => EC.prototype.elementToBeClickable(el), 6000);
    await el.click();

    });

    // Hi, you are sucessfully registered as "Student".
    it('should see success feedback', async () => {

    const el = element(by.css('p.successMessage.student'));
    await browser.wait(() => EC.prototype.presenceOf(el), 6000);
    //const option1 = await expect(el.getText()).toContain('Hi, you are sucessfully registered');
    //const option1 = await expect(el.getText()).toContain('Hi, you are sucessfully registered');

    var successEn = EC.prototype.textToBePresentInElement(el, 'Hi, you are sucessfully registered');
    var successDe = EC.prototype.textToBePresentInElement(el, 'Du hast dich erfolgreich als "Schüler" angemeldet');
    // Waits for title to contain either 'Foo' or 'Bar'
    browser.wait(EC.prototype.or(successEn, successDe), 5000);
    });

    it('should go home and check for login status', async () => {
    // await browser.get('/');
    await element(by.css('button[name="home"]')).click();
    });
    
    */

    // end of class
});
