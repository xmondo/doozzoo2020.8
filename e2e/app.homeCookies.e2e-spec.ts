import { browser, by, element, ProtractorBrowser, ProtractorExpectedConditions } from 'protractor';

describe('home cookies', () => {
  const EC = ProtractorExpectedConditions;
  beforeEach(() => {
    // browser.ignoreSynchronization = true;
    // browser.sleep(1000);
  });

  afterEach( async () => {
    await browser.sleep(1000);
  });

  it('should dismiss cookie hint popup', async () => {
    await browser.get('/');
    const cookie = element(by.css('button[name="cookie-hint-button"]'));
    const status = await cookie.isPresent();
    if(status) {
        cookie.click();
    }
  });

  // cookie hint window should disapear
  it('cookie hint popup has disapeared', async () => {
    await browser.wait(() => EC.prototype.stalenessOf(element(by.css('.cookie-hint'))), 6000); 
    await browser.sleep(1000);
  });

});
