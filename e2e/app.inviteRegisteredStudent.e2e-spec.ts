import { browser, by, element, ProtractorBrowser, ProtractorExpectedConditions, promise } from 'protractor';

// catches unhandled, so that they do not spoil terminal
process.on('unhandledRejection', (err) => {
    console.log('unhandledRejection', err);
});

// global vars
let inviteUrl = null;

describe('invite registered student ->', () => {
    const EC = ProtractorExpectedConditions;
    const brwsr = ProtractorBrowser;
    beforeEach(async () => {
        browser.ignoreSynchronization = false;
        // browser.sleep(1000);
        // TODO: this does the trick to accept clicks on submit etc.
        await browser.waitForAngularEnabled(true);
    });

    afterEach( async () => {
        await browser.sleep(1000);
    });

    it('should get an error message due to invalid invite token', async () => {
        await browser.get('/invite/-blablabla___');
        const el = element(by.css('html > body > app-root > app-invite > div > div > div > p.errorMessage'));
        // tslint:disable-next-line:max-line-length
        await browser.wait(EC.prototype.textToBePresentInElement(el, 'Ooops, something went wrong. Your invite link is invalid, please try again.'), 5000);

        // handle cookie message
        const cookie = element(by.css('button[name="cookie-hint-button"]'));
        const status = await cookie.isPresent();
        // console.log('cookie status: ', status);
        if(status) {
            cookie.click();
        }
        await browser.sleep(2000);
    });

    it('should login as coach1', async () => {
        await browser.get('/directlogin?u=coach1@mypelz.de&p=%23coach1');
        await browser.sleep(2000);
    });


    it('should go to "mystudents" and click tab 2', async () => {
        await browser.get('/mystudents');
        await browser.waitForAngular();
        await browser.wait(() => EC.prototype.urlContains('mystudents'), 5000);
        await browser.sleep(1000);

        await browser.wait(() => EC.prototype.visibilityOf(element(by.css('.mat-tab-labels'))), 6000);
        await expect(element(by.css('.mat-tab-labels')).isPresent()).toBeTruthy();       
        // click on second tab  
        await element(by.cssContainingText('div.mat-tab-label-content', 'Invite & connect students')).click();
        await browser.sleep(1000);
    });

    it('should create new invite links', async () => {
        const el = element(by.css('#listInviteLinks'));
        await browser.wait(() => EC.prototype.visibilityOf(el), 6000);
        // TODO: fragile: browser wait does not work in a stable way
        await browser.sleep(1000);

        const inviteLinkCount = await element.all(by.css('li span.inviteLink')).count();
        //console.log('inviteLinkCount before: ', inviteLinkCount);

        if (inviteLinkCount < 2 && inviteLinkCount > 0) {
            const _el = element(by.css('[name="createInviteLink"]'));
            await browser.wait(() => EC.prototype.visibilityOf(_el), 5000);
            await _el.click();
            const itemsAfter = await element.all(by.css('li span.inviteLink')).count();
            // list should show entry and have one more then before
            await expect(inviteLinkCount < itemsAfter).toBeTruthy();
        }

        const url = await element.all(by.css('span.inviteLink'))
            .last()
            .getText();
        // console.log('inviteUrl: ', url);
        inviteUrl = url;
    });

    it('coach should log out', async () => {
        await browser.get('/login?mode=logout');
    });

    it('invitee should call the invite link and see coach1', async () => {
        // console.log(inviteUrl);
        await browser.get(inviteUrl);

        let el = element(by.css('p.successMessage'));

        await browser.wait(
          EC.prototype
            .textToBePresentInElement(
              element(by.css('p.successMessage')), 
              'Hello, you have been invited by coach'
            ), 5000
        );
    });

    it('invitee should log in as user2', async () => {
        // sign in as user2
        element(by.css('button[name="login"]')).click();

        const user = 'user2@mypelz.de';
        const password = '#user2';

        let el = element(by.css('mat-icon[name="loginDialogCloseButton"]'));
        await browser.wait(() => EC.prototype.presenceOf(el), 6000);
        await browser.sleep(1000);

        element(by.css('input[name="email"]')).sendKeys(user);
        element(by.css('input[name="password"]')).sendKeys(password);
        await browser.sleep(1000);
        element(by.css('button[name="loginSubmit"]')).click();
        await browser.sleep(1000);

    });

    /*
    // test simply fails due to -> Async callback was not invoked within timeout specified

    it('invitee should see feedback to continue in "My coaches"...', async () => {
        // sign in as user2
        browser.sleep(4000);
        const el = element(by.css('html > body > app-root > app-invite > div > div > div > div > p.successMessage'));
        // await browser.wait(() => EC.prototype.presenceOf(el), 6000);
        // tslint:disable-next-line:max-line-length
        
        await el.getText()
            .then(result => {
              console.log('result: ', result);
                expect(result.toLowerCase()).toContain('visit my coaches to directly join your coach');
            });

        await browser.sleep(1000);
        
    });
    */

});


