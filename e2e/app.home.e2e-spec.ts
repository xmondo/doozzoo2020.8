import { browser, by, element, ProtractorBrowser, ProtractorExpectedConditions } from 'protractor';

describe('home', () => {
  const EC = ProtractorExpectedConditions;
  beforeEach(() => {
    // browser.ignoreSynchronization = true;
    // browser.sleep(1000);
  });

  afterEach( async () => {
    await browser.sleep(1000);
  });

  it('should check for essential buttons', async () => {
    // Wait for the publishers to load
    await browser.get('/');
    const cookie = element(by.css('button[name="cookie-hint-button"]'));
    const status = await cookie.isPresent();
    // console.log('cookie status: ', status);
    if(status) {
        cookie.click();
    }
    await browser.wait(() => element(by.css('button[name="login"]')).isPresent(), 6000);
    // await browser.wait(() => element(by.css('button[name="preSessionCheck"]')).isPresent(), 6000);
    await browser.wait(() => element(by.css('button[name="login"]')).isPresent(), 6000);
    await browser.wait(() => element(by.css('button[name="register"]')).isPresent(), 6000);
  });

  it('should check header navigation', async () => {
    // Wait for the publishers to load
    const text = await element(by.css('.infoNavigation')).getText();
    expect(text).toContain('blog');
    expect(text).toContain('info');
    expect(text).toContain('features');
    expect(text).toContain('pricing');
    // expect(text).toContain('faq');
  });

  it('should content footer info', async () => {
    // Wait for the publishers to load
    const text = await element(by.css('#footerInfo')).getText();
    expect(text).toContain('Imprint');
    expect(text).toContain('Privacy Policy');
    expect(text).toContain('Terms & Conditions');
  });

  it('should check footer navigation', async () => {
    // Wait for the publishers to load
    const text = await element(by.css('#footer')).getText();
    expect(text).toContain('Pre session check');
    //expect(text).toContain('Media');
    //expect(text).toContain('Apps');
    expect(text).toContain('Login');
    // Language switch
    await browser.wait(() => element(by.css('button[name="languageMenu"]')).isPresent(), 6000);
  });

  it('should click login and open login dialog', async () => {
    await browser.wait(() => EC.prototype.visibilityOf(element(by.css('button[name="login"]'))), 6000);
    element(by.css('button[name="login"]')).click();
    await browser.sleep(1000);
    await element(by.css('mat-icon[name="loginDialogCloseButton"]')).click();
  });

  it('should click register and open register dialog', async () => {
    await element(by.css('button[name="register"]'))
      .click();    
    await browser.sleep(1000);
    await element(by.css('mat-icon[name="loginDialogCloseButton"]')).click();
  });

  it('should click passwordForgotten and open passwordForgotten dialog', async () => {
    await element(by.css('button[name="passwordForgotten"]'))
      .click();    
      await browser.sleep(1000);
      await element(by.css('mat-icon[name="loginDialogCloseButton"]')).click();
  });

  it('should click preflight and land on preflight check page', async () => {
    // Wait for the publishers to load
    await element(by.css('button[name="preflight"]'))
      .click()
    const url = await browser.getCurrentUrl();
    expect(url).toContain('/presession');
  });

  it('should click home and land on home again', async () => {
    // Wait for spinner to stop
    await browser.wait(() => EC.prototype.stalenessOf(element(by.css('.overlay'))), 6000);
    await element(by.css('#footer button[name="home"]'))
    const url = await browser.getCurrentUrl();
    expect(url).toContain('/');
  });

});
