import { browser, by, element, ProtractorBrowser, ProtractorExpectedConditions } from 'protractor';

// catches unhandled, so that they do not spoil terminal
process.on('unhandledRejection', (err) => {
    console.log('unhandledRejection', err);
});
  
describe('coach participants', () => {
    const EC = ProtractorExpectedConditions;

    // To create a new browser with url as the first one
    const secondBrowser = browser.forkNewDriverInstance(true);

    browser.driver.executeScript(function() {
        return {
            width: window.screen.availWidth,
            height: window.screen.availHeight
        };
    }).then(function(result: any) {
        // browser.driver.manage().window().setSize(result.width/2,result.height/4*3);
        // browser.driver.manage().window().setPosition(0, 0);
        console.log(result);

        if (result.width / 2 > 960) {
            // set first browser size & position
            browser.manage().window().setSize(result.width / 2, result.height / 4 * 3); // w, h
            browser.manage().window().setPosition(0, 0); // left, top

            secondBrowser.manage().window().setSize(result.width / 2, result.height / 4 * 3); // w, h
            secondBrowser.manage().window().setPosition(result.width / 2, 0); // left, top
        } else {
            // set first browser size & position
            browser.manage().window().setSize(960, result.height / 4 * 3); // w, h
            browser.manage().window().setPosition(0, 0); // left, top

            secondBrowser.manage().window().setSize(960, result.height / 4 * 3); // w, h
            // if(result.width/2 )
            // secondBrowser.manage().window().setPosition(result.width - (960 * 0.75),0); // left, top
            secondBrowser.manage().window().setPosition(result.width - 960, 0);
        }
    });

    // clone element var to 2nd instance
    const element = browser.element;
    const element2 = secondBrowser.element;

    beforeEach( async () => {
        // browser.ignoreSynchronization = true;
        // TODO: this does the trick to accept clicks on submit etc.
        // await browser.waitForAngularEnabled(true);
    });

    afterEach( async () => {
        await browser.sleep(2000);
        await secondBrowser.sleep(2000);
    });

    it('should login as coach1 and user2', async () => {
        await browser.get('/directlogin?u=coach1@mypelz.de&p=%23coach1');
        await browser.sleep(1000);

        await secondBrowser.get('/directlogin?u=user2@mypelz.de&p=%23user2');
        await secondBrowser.sleep(1000)
    });

    it('should handle cookies', async () => {

        // handle cookie
        const cookie = element(by.css('button[name="cookie-hint-button"]'));
        const status = await cookie.isPresent();
        // console.log('cookie status: ', status);
        if(status) {
            cookie.click();
        }

        const cookie2 = element2(by.css('button[name="cookie-hint-button"]'));
        const status2 = await cookie2.isPresent();
        // console.log('cookie status: ', status);
        if(status2) {
            cookie2.click();
        }
    });

    it('should show text for coach & user', async () => {
        let el = element(by.css('.mat-title.landingContent'));
        expect(el.getText()).toContain('Hey coach coach1, thanks for logging in!');
        await browser.sleep(1000);
        
        el = element2(by.css('.mat-title.landingContent'));
        expect(el.getText()).toContain('Hey user2, thanks for logging in!');
        await secondBrowser.sleep(1000);
    });

    it('should go as coach to room and as user2 to "my coaches"', async () => {
        await element(by.css('.startTango')).click();
        await element2(by.css('.startTango')).click();
    });

    it('should see coach line visible, and clickable', async () => {
        const _el = element2(by.cssContainingText('li', 'coach1FirstName'));
        // const _el = element2(by.css('li.auth0|582c7e4cedb8c73048379aad'));
        // await secondBrowser.wait(() => EC.prototype.visibilityOf(_el), 6000);
        // await expect(_el.getText()).toContain('back in a second!');
        await expect(_el.getText()).toContain('coach1FirstName coach1LastName');

        // await browser.sleep(1000);
        await secondBrowser.sleep(1000);
    });

    it('coach1 should stop lesson, student button should be deactivated', async () => {
        /*
        const el = element(by.css('button[name="home"]'));
        el.getText().then(val => console.log('gettext: ', val));
        await browser.wait(() => EC.prototype.visibilityOf(el), 6000);
        await el.click();
        */

        // await browser.get('/home');
        // element(by.css('#footer>button[name="home"]')).click();

        const _el = element2(by.cssContainingText('li', 'coach1FirstName')).element(by.css('.buttonGoToClassroom'));
        // await expect(_el.getAttribute('disabled')).toBe('true');
        await secondBrowser.wait(() => EC.prototype.elementToBeClickable(_el), 6000);

        await browser.sleep(1000);
        await secondBrowser.sleep(1000);
/*
        // 
        browser.getCurrentUrl().then(function(url) {
            browser.navigate().refresh().catch(function() {
                return browser.switchTo().alert().then(function (alert) {
                    alert.accept();
                    return browser.get(url);
                });
            });

        });

        await browser.sleep(2000);
        await secondBrowser.sleep(2000);

        const _el = element2(by.cssContainingText('li', 'coach1FirstName')).element(by.css('.buttonGoToClassroom'));
        await expect(_el.getAttribute('disabled')).toBe('true');

        

        await browser.sleep(1000);
        await secondBrowser.sleep(1000);
*/

    });

});
