const { setHeadlessWhen } = require('@codeceptjs/configure');

// turn on headless mode when running with HEADLESS=true environment variable
// HEADLESS=true npx codecept run
setHeadlessWhen(process.env.HEADLESS);

const path = require('path');
const fakeVideoFileName = 'fileName.y4m';
var pathToFakeVideoFile =  path.join(__dirname, 'fakeVideo.mp4');

// firebase API keys  
const devKey = 'AIzaSyBHJt1JFPRSuue0ZkbRNEy7oZcpN7KBAqA';
const stageKey = 'AIzaSyBHJt1JFPRSuue0ZkbRNEy7oZcpN7KBAqA';
const prodKey='AIzaSyAklBOk3aEvjfR0KDOK2Ju5GQnlsUCUlnA';

// firebase endpoints
const devEndpoint = "https://doozzoo-dev.firebaseio.com";
const stageEndpoint = "https://doozzoo-dev.firebaseio.com";
const prodEndpoint = "https://doozzoo-eve.firebaseio.com";

const devUrl = 'https://doozzoo.com';

// define height here
const height = 900;

let key;
let endpoint;
let url;
let stripeIban;
let stripeCard;
let stripeVoucher;
let stripeVoucherName;
let profile = process.env.profile || 'default';

let coach1user = 'coach1@mypelz.de';
let coach1pw = '#coach1';
let coach1Id = 'auth0|582c7e4cedb8c73048379aad';
let coach2user = 'coach2@mypelz.de';
let coach2pw = '#coach2';
let user1user = 'user1@mypelz.de';
let user1pw = '#user1';
let user1Id = 'WmYDCkyM3kcTZpRj9ZX63xgy7kx2';
let user2user = 'user2@mypelz.de';
let user2pw = '#user2';
let user2Id = 'OFHEb2PkOOY51gLObPrlsLWlAmS2';
let user5user = 'user5@mypelz.de';
let user5pw = '#user5';

let user3Id = 'uNzOprX2cHhAITb0HEJnMjXX7rB3';
let user4Id = 'meq4dQi9Y7gxufXKd1sqwgOCXwt1';
let user5user = 'user5@mypelz.de';
let user5pw = '#user5';
let stripecoach2user = 'stripecoach2@mypelz.de';
let stripecoach2pw = '#stripecoach2';
let stripecoach2Id = '2X1ZM44k8lXX6t2w1X17fx76DPK2';
let stripecoach2Id = 'AUpCArPXIsdhmyYHMAAzaq96idj2';
let stripecoach2user = 'stripecoach2@mypelz.de';
let stripecoach2pw = '#stripecoach2';
let admin1pw = '#admin1';
let customer1user = 'customer1@mypelz.de';
let customer1pw = '#customer1';
let customer2user = 'customer2@mypelz.de';
let customer2pw = '#customer2';
let customer3user = 'customer3@mypelz.de';
let customer3pw = '#customer3';
let customer5user = 'customer5@mypelz.de';
let customer5pw = '#customer5';

switch(profile) {
  case 'prod':
    key = 'AIzaSyAklBOk3aEvjfR0KDOK2Ju5GQnlsUCUlnA'
    endpoint = 'https://doozzoo-eve.firebaseio.com';
    url = 'https://doozzoo.com';
    stripeIban = 'DE17 3706 0193 1012 6440 23';
    stripeCard = '4546 1783 3290 3019'; // 01/23
    stripeVoucher = '2nN154UK';
    stripeVoucherName = 'TEST100% Intern';
    coach1Id = 'auth0|582c7e4cedb8c73048379aad';
    user1Id = 'auth0|582c4c56edb8c73048379239'
    user2Id = 'auth0|582c7595edb8c7304837994a';
    user3Id = '6w46c0FzUEbhbPdfqLAM9YbA88s2';
    user4Id = 'f3JZ88x0aUMbRGVq4fdCuEJ0g843';
    user5user = user5user;
    user5pw = user5pw;
    stripecoach2Id = 'RRb4APWj0vhzH0kGrehQRqfpTvO2';
    stripecoach2user = 'stripecoach2@mypelz.de';
    stripecoach2pw = '#stripecoach2';
    admin1pw = 'myAdmin111TollesPasswort48'; // ### enter production pw ###
    break;
  case 'stage':
    key = 'AIzaSyAklBOk3aEvjfR0KDOK2Ju5GQnlsUCUlnA'
    endpoint = 'https://doozzoo-eve.firebaseio.com';
    url = 'https://doozzoo.com';
    stripeIban = 'DE17 3706 0193 1012 6440 23'; 
    stripeCard = '4546 1783 3290 3019'; // 01/23
    stripeVoucher = '2nN154UK';
    stripeVoucherName = 'TEST100% Intern';
    break;
  case 'dev':
    key = 'AIzaSyBHJt1JFPRSuue0ZkbRNEy7oZcpN7KBAqA'
    endpoint = 'https://doozzoo-dev.firebaseio.com';
    url = 'https://dev.doozzoo.com';
    stripeIban = 'DE89 3704 0044 0532 0130 00';
    stripeCard = '5555 5555 5555 4444'; 
    stripeVoucher = 'P2joiacT';
    stripeVoucherName = 'HAZE30DAY100%OFF';
    coach1Id = 'auth0|582c7e4cedb8c73048379aad';
    user1Id = 'auth0|582c4c56edb8c73048379239'
    user2Id = 'OFHEb2PkOOY51gLObPrlsLWlAmS2';
    user3Id = 'uNzOprX2cHhAITb0HEJnMjXX7rB3';
    user4Id = 'meq4dQi9Y7gxufXKd1sqwgOCXwt1';
    user5user = user5user;
    user5pw = user5pw;
    stripecoach2user = stripecoach2user;
    stripecoach2pw = stripecoach2pw;
    stripecoach2Id = '2X1ZM44k8lXX6t2w1X17fx76DPK2';
    user4Id = 'meq4dQi9Y7gxufXKd1sqwgOCXwt1'
    stripecoach2Id = 'AUpCArPXIsdhmyYHMAAzaq96idj2';
    stripecoach2user = 'stripecoach2@mypelz.de';
    stripecoach2pw = '#stripecoach2';
    admin1pw = admin1pw;
    break;
  // dev
  default:
    key = 'AIzaSyBHJt1JFPRSuue0ZkbRNEy7oZcpN7KBAqA'
    endpoint = 'https://doozzoo-dev.firebaseio.com';
    url = 'https://localhost:4200'; // 'https://192.168.1.2:4200' //
    stripeIban = 'DE89 3704 0044 0532 0130 00';  
    stripeCard = '5555 5555 5555 4444'; 
    stripeVoucher = 'P2joiacT';
    stripeVoucherName = 'HAZE30DAY100%OFF';
    coach1Id = coach1Id;
    user1Id = user1Id;
    user2Id = user2Id;
    user3Id = user3Id;
    user4Id = user4Id;
    user5user = user5user;
    user5pw = user5pw;
    stripecoach2user = stripecoach2user;
    stripecoach2pw = stripecoach2pw;
    stripecoach2Id = stripecoach2Id;
    stripecoach2user = 'stripecoach2@mypelz.de';
    stripecoach2pw = '#stripecoach2';
    admin1pw = admin1pw;
    // fallback
    profile = 'default';
}

exports.config = {
  tests: './e2e2/*_test.js',
  output: './e2e2/output',
  // env related constants:
  profile: profile,
  key: key,
  endpoint: endpoint,
  url: url,
  stripeIban: stripeIban,
  stripeVoucher: stripeVoucher,
  stripeVoucherName: stripeVoucherName,
  stripeCard: stripeCard,
  coach1user: coach1user,
  coach1pw: coach1pw,
  coach1Id: coach1Id,
  coach2user: coach2user,
  coach2pw: coach2pw,
  user5user: user5user,
  user5pw: user5pw,

  user1user: user1user,
  user1pw: user1pw,
  user1Id: user1Id,
  user2user: user2user,
  user2pw: user2pw,
  user2Id: user2Id,
  user3Id: user3Id,
  user4Id: user4Id,
  user5user: user5user,
  user5pw: user5pw,
  stripecoach2user: stripecoach2user,
  stripecoach2pw: stripecoach2pw,
  stripecoach2Id: stripecoach2Id,
  stripecoach2user: stripecoach2user,
  stripecoach2pw: stripecoach2pw,
  admin1pw: admin1pw,

  customer1user: customer1user,
  customer1pw: customer1pw,
  customer2user: customer2user,
  customer2pw: customer2pw,
  customer3user: customer3user,
  customer3pw: customer3pw,
  customer5user: customer5user,
  customer5pw: customer5pw,

  // helpers
  helpers: {
 
    /*   
    Playwright: {
      url: process.env.profile === 'prod' ? 'https://doozzoo.com' : 'https://localhost:4200', //'https://dev.doozzoo.com', //
      show: true,
      waitForNavigation: 'networkidle0',
      waitForAction: 500,
      windowSize: '900x1024', //'768x900', // hxw or WxH?
      browser: 'firefox', // chromium, firefox, webkit
      ignoreHTTPSErrors: true,
      chromium: {
        acceptInsecureCerts: true,
        ignoreHTTPSErrors: true,
        args: [
          // '--headless', 
          // '--disable-gpu', 
          '--disable-infobars',
          `--window-size=900,${height}`,
          '--use-fake-ui-for-media-stream',
          '--disable-web-security',
          '--use-fake-device-for-media-stream',
          // '--use-file-for-fake-video-capture=' + pathToFakeVideoFile,
          '--allow-file-access-from-files',
          '--allow-running-insecure-content',
          '--disable-features=IsolateOrigins,site-per-process'
        ],
      }
    },
 */  

    Puppeteer: {
      url: url, //
      show: true,
      waitForNavigation: 'networkidle0',
      waitForAction: 500,
      windowSize: `900x${height}`,//'800x700', // width/height
      chrome: {
        // ignoreDefaultArgs: ['--enable-automation','--enable-infobars'],
        ignoreHTTPSErrors: true,
        // see: https://webrtc.org/getting-started/testing
        args: [
          // '--headless', 
          // '--disable-gpu', 
          '--disable-infobars',
          `--window-size=900,${height}`,
          '--disable-web-security',
          '--use-fake-ui-for-media-stream',
          '--use-fake-device-for-media-stream',
          // '--mute-audio', // 
          //'--use-file-for-fake-audio-capture=~/test-assets/testvideo.webm', // ' + pathToFakeAudioFile,
          //'--use-file-for-fake-video-capture=~/test-assets/testvideo.webm', // pathToFakeVideoFile,
          '--allow-file-access-from-files',
          '--allow-running-insecure-content',
          '--disable-features=IsolateOrigins,site-per-process',
          // '--force-device-scale-factor=1.5'
        ],
        //headless: false, // default is true
        //excludeSwitches: 'enable-automation',
        //ignoreDefaultArgs: ['--enable-automation'],
        // userDataDir: "./user_data",
        // defaultViewport: null,
        devtools: false, // show dev windows
      }
    },

    REST: {
      endpoint: process.env.profile === 'prod' ? prodEndpoint : devEndpoint,
      defaultHeaders: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
    },
  },
  include: {
    I: './steps_file.js'
  },
  bootstrap: null,
  mocha: {},
  name: 'app2020.03',
  plugins: {
    retryFailedStep: {
      enabled: true
    },
    screenshotOnFail: {
      enabled: true
    }
  }
}