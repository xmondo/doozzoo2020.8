/// <reference path="../steps.d.ts" />

let config = require('codeceptjs').config.get();
const cookieConsent = require('./helper/cookieConsent');

const MailListener = require('mail-listener2');
const assert = require('assert');
const resetPassword = require('./helper/resetPassword')

// here goes your email connection configuration
const mailListener = new MailListener({
    username: 'pelz@mypelz.de',
    password: 'SIEBEN#meilen?stiefel',
    host: 'imap.1und1.de',
    port: 993, // imap port
    tls: true,
    tlsOptions: { rejectUnauthorized: false },
    mailbox: 'Inbox', // mailbox to monitor
    searchFilter: ['UNSEEN'], // ["UNSEEN", "FLAGGED"], // the search filter being used after an IDLE notification has been retrieved
    markSeen: true, // all fetched email willbe marked as seen and not fetched next time
    fetchUnreadOnStart: true, // use it only if you want to get all unread email on lib start. Default is `false`,
    mailParserOptions: { streamAttachments: true }, // options to be passed to mailParser lib.
    attachments: false, // download attachments as they are encountered to the project directory
    attachmentOptions: { directory: 'attachments/' } // specify a download directory for attachments
});

mailListener.start();

mailListener.on('error', function (err) {
    console.log('mailListener error: ', err);
});

mailListener.on('server:connected', function () {
    console.log('Mail listener initialized');
});

mailListener.on('mail', function (mail, seqno, attributes) {
    // do something with mail object including attachments
    // console.log("emailParsed", mail);
    // mail processing code goes here
});

// helper
function getLastEmail() {
    // console.log('now waiting for an email...');
    return new Promise(resolve => {
        mailListener.on('mail', function (mail) {
            resolve(mail);
        });
    });
}

const resetPw = config.user2pw;

Feature('password forgotten');

Scenario('password forgotten non-logged in Pw1 prepare @registration @login', async ({ I }) => {
    await resetPassword(I, config.user2user, config.user2pw, 'foofoo', config.key);
});

Scenario('password forgotten non-logged in  Pw1 @registration @login', async ({ I }) => {

    I.amOnPage('/');

    cookieConsent(I)

    // click passwordForgotten on home
    I.click({css: 'button[name="passwordForgotten"]'});

    // I should see dialog poping up
    I.waitForVisible({ css: '.mat-dialog-container'}, 6);
    I.see('Please enter the e-mail to which we should send the password reset-link.');

    // I enter my email, that I used for registration
    I.fillField('input[formcontrolname="forgottenEmailCtrl"]', config.user2user);
    // button should still be disabled
    // I.seeAttributesOnElements({ css: 'button[name="resetPassword"]'}, { disabled: true });
    I.seeElement(locate({ css: 'button[name="resetPassword"]'}).withAttr({ disabled: 'true' }));
    // repeat email fiels should get visible
    I.waitForVisible({ css: 'input[formcontrolname="forgottenEmailRepeatedCtrl"]'}, 6);
    I.fillField('input[formcontrolname="forgottenEmailRepeatedCtrl"]', config.user2user);

    I.waitForClickable({ css: 'button[name="resetPassword"]'}, 6);
    I.click({ css: 'button[name="resetPassword"]'});
    // I wait for success feedback
    I.waitForText('Reset e-mail has been send. Check your mailbox and your spam folder as well!', 6);
    
    // wait for email and fetch its content
    const email = await getLastEmail();
    // console.log('email content: ', email.text);

    const from = 'support@doozzoo.com';
    assert.strictEqual(email.subject, 'Reset your password for doozzoo');
    assert.strictEqual(email.headers.from, from);
    assert.strictEqual(email.headers.to, config.user2user);

    // extract registration code from the email message
    const pattern = /(\boobCode=\b[a-zA-Z0-9&=\-_]*)/g;
    const regCode = pattern.exec(email.text);
    // console.log('extracted link: ', pattern, regCode[0]);

    // expose link
    registerUrl = config.url + '/login?mode=resetPassword&' + regCode[0];

    // should continue running for second test
    // mailListener.stop();

    // click registration link
    I.amOnPage(registerUrl);
    I.waitForText('Please enter a new password for', 6);
    // I.seeTextEquals('Reset password', {css: 'span.mat-title'});

    // enter new password 
    // I enter my email, that I used for registration
    I.fillField('input[name="password"]', resetPw);
    //I.fillField('input[name="password"]', 'foofoo');
    I.waitForClickable('button[name="resetPassword"]');
    I.click('button[name="resetPassword"]');
    
    I.waitForText('Your password has been updated!', 6);

    // I login using new password
    I.amOnPage(`/directlogin?u=${config.user2user}&p=${encodeURIComponent(resetPw)}`);
    I.waitForText('Hey user2, thanks for logging in!', 6)

});

/*
Scenario('password forgotten non-logged in  Pw2 @registration @login', async ({ I }) => {

    I.amOnPage('/');

    cookieConsent(I)

    // click passwordForgotten on home
    I.click({css: 'button[name="passwordForgotten"]'});

    // I should see dialog poping up
    I.waitForVisible({ css: '.mat-dialog-container'}, 6);
    I.see('Please enter the e-mail to which we should send the password reset-link.');

    // I enter my email, that I used for registration
    I.fillField('input[formcontrolname="forgottenEmailCtrl"]', user);
    // button should still be disabled
    // I.seeAttributesOnElements({ css: 'button[name="resetPassword"]'}, { disabled: true });
    I.seeElement(locate({ css: 'button[name="resetPassword"]'}).withAttr({ disabled: 'true' }));
    // repeat email fiels should get visible
    I.waitForVisible({ css: 'input[formcontrolname="forgottenEmailRepeatedCtrl"]'}, 6);
    I.fillField('input[formcontrolname="forgottenEmailRepeatedCtrl"]', user);

    I.waitForClickable({ css: 'button[name="resetPassword"]'}, 6);
    I.click({ css: 'button[name="resetPassword"]'});

    // I wait for success feedback
    I.waitForText('Reset e-mail has been send. Check your mailbox and your spam folder as well!', 6);
    
    // wait for email and fetch its content
    const email = await getLastEmail();
    // console.log('email content: ', email.text);

    const from = 'support@doozzoo.com';
    assert.strictEqual(email.subject, 'Reset your password for doozzoo');
    assert.strictEqual(email.headers.from, from);
    assert.strictEqual(email.headers.to, user);

    // extract registration code from the email message
    const pattern = /(\boobCode=\b[a-zA-Z0-9&=\-_]*)/g;
    const regCode = pattern.exec(email.text);
    // console.log('extracted link: ', pattern, regCode, regCode[0]);

    // expose link
    registerUrl = config.url + '/login?mode=resetPassword&' + regCode[0];

    mailListener.stop();

    // click registration link
    I.amOnPage(registerUrl);
    I.waitForText('Please enter a new password for', 6);
    // I.seeTextEquals('Reset password', {css: 'span.mat-title'});

    // enter new password 
    // I enter my email, that I used for registration
    I.fillField('input[name="password"]', password1);
    I.waitForClickable('button[name="resetPassword"]');
    I.click('button[name="resetPassword"]');
    I.waitForText('Your password has been updated!', 6);

    // I login using new password
    I.amOnPage(`/directlogin?u=${user}&p=${password1Url}`);
    I.waitUrlEquals('/');
    I.waitForText('Hey user2, thanks for logging in!', 6)

});
*/
