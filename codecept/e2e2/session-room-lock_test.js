/// <reference path="../steps.d.ts" />

const config = require('codeceptjs').config.get();
const cookieConsent = require('./helper/cookieConsent');
const key = config.key;
const isProd = config.profile === 'prod' ? true : false;

Feature('session lock button');

const coachId = config.coach1Id;

Scenario('prepare session and set lock to "false" @waitingroom @room', (async ({ I }) => { // or Background

    console.log(isProd)

    I.haveRequestHeaders({'Content-Type':'application/x-www-form-urlencoded'});
    // email=coach1%40mypelz.de&password=%23coach1&returnSecureToken=true
    const response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${key}`,"email=coach1%40mypelz.de&password=%23coach1&returnSecureToken=true");
    // auth response
    // console.log('rest data response: ', response.data.displayName, response.data.idToken);
    await I.sendPatchRequest(`/classrooms/${coachId}.json?auth=${response.data.idToken}`, { locked: false, roomId: null });

}));

Scenario('login as coach, enter room and check that lock is acting same in session and participant list @lock @room', ({ I }) => {

    // coach login
    I.amOnPage('/directlogin?u=coach1@mypelz.de&p=%23coach1');
    
    cookieConsent(I)

    I.waitForClickable({css:'button.startTango'}, 12);
    I.click({css:'button.startTango'});
    I.seeInCurrentUrl('/room/');

    I.waitForElement('button[name="sessionLockRoom"]', 12);
    I.seeElement({css: 'button[name="sessionLockRoom"].sessionLockButton.unlocked' });
    I.click({ css: 'button[name="sessionLockRoom"]'});
    I.waitForElement({css: 'button[name="sessionLockRoom"].sessionLockButton.locked' }, 12);

    I.waitForElement({ css: 'button[name="participantsSidenavButton"]'}, 12);
    I.click({ css: 'button[name="participantsSidenavButton"]'});

    I.waitForElement({ css: 'mat-sidenav.sidenavContentRight'});
        
    I.seeElement(locate({css: 'button[name="sessionLockParticipants"].sessionLockButton.locked' })
        .inside({ css: 'mat-sidenav.sidenavContentRight'}));
    I.click(locate({css: 'button[name="sessionLockParticipants"].sessionLockButton.locked' })
        .inside({ css: 'mat-sidenav.sidenavContentRight'}));
    I.waitForElement(locate({css: 'button[name="sessionLockParticipants"].sessionLockButton.unlocked' })
        .inside({ css: 'mat-sidenav.sidenavContentRight'}), 6);

    I.click({css: 'button[name="sidenavRightCloseButton"]'});
    I.waitForElement({css: 'button[name="sessionLockRoom"].sessionLockButton.unlocked' }, 6);

});
