/// <reference path="../steps.d.ts" />

const config = require('codeceptjs').config.get();

/**
 * admin logs in
 * admin creates coach email address
 * admin creates account
 * send out invite
 * invite is created and visible in admin backend list
 */

const assert = require('assert');

const randomEmail = require('./helper/randomEmail');
const roleSelectLabel = 'Coach'; // Coach | Student
const deleteExistingUser = require('./helper/deleteExistingUser');
const login = require('./helper/login');

Feature('edu admin creates coach customer2');

// make sure there is no existing account for customer2
Scenario('prepare coach customer2 creation @edu', async ({ I }) => {
    await deleteExistingUser(I, config.customer2user, config.customer2pw, config.key);
});

Scenario('test user deactivation @edu', async ({ I }) => {

    login(I, config.customer5user, config.customer5pw, 'form');


    I.waitForElement({ css: 'button[name="toEduBackend"]'}, 6);
    I.click({ css: 'button[name="toEduBackend"]'});

    I.click(locate({css: 'button[name="edu-backoffice"]'}).inside(locate('div>span').withText('Monheim')));

    I.scrollPageToBottom();

    // create user     
    I.fillField({ css: 'input[name="email"]'}, config.customer2user);

    I.waitForEnabled({ css: 'input[name="email2"]'}, 6);
    I.fillField({ css: 'input[name="email2"]'}, config.customer2user);

    // option list: native codecept does not work as this are fake material controls
    I.click({ css: 'mat-select[name="role"]' });
    // I.click({ css: '.mat-option-text'});
    I.waitForElement(locate({ css: 'mat-option'}).withText(roleSelectLabel), 6);
    I.click(locate({ css: '.mat-option-text'}).withText(roleSelectLabel));

    I.waitForClickable({ css: 'button[name="createAccount"]'}, 12);
    I.click({ css: 'button[name="createAccount"]'});

    I.waitForElement(locate({ css: '.innerItemRow.freshInvites'}).withText(config.customer2user), 6);
    within(locate({ css: '.innerItemRow.freshInvites'}).withText(config.customer2user),() => {
        I.see('send invite');
        I.click(locate({css: '.invite-button'}).withText('send invite'));
    });

    let password;
    I.waitForElement(locate({ css: '.innerItemRow'}).withText(config.customer2user), 12);
    I.see('invite sent', locate({ css: '.innerItemRow'}).withText(config.customer2user));

    password = await I.grabAttributeFrom(locate({ css: '.password-hint-row' }).inside(locate({ css: '.innerItemRow'}).withText(config.customer2user)), 'title');
    console.log('temp password: ', password);

    session('new coach login', async () => {
        
        login(I, config.customer2user, password, 'form');

        // home logo for edu as indicator for institutional account
        I.waitForElement({ css: '#homeLogo.edu' }, 6);

        // with login in the first time, login is been set to accepted in the institutional backend

    }); 

    I.waitForText('invite accepted', 12, locate({ css: '.innerItemRow'}).withText(config.customer2user));

});

/**
 * If we now try to create this coach account again, there must be a hint that the user already exists
 * -> try to create already existing user @edu
 */


