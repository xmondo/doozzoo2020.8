/// <reference path="../steps.d.ts" />

const config = require('codeceptjs').config.get();
const cookieConsent = require('./helper/cookieConsent');
const key = config.key;

const assert = require('assert');

Feature('toolbar activate');

const coach1user = config.coach1user;
const coach1pw = config.coach1pw;
const coach1Id = config.coach1Id;
const user1user = config.user1user;
const user1pw = config.user1pw;
const user2user = config.user2user;
const user2pw = config.user2pw;

/**
 * assigning access to apps on student side should not generate additional complexity and make things complicated
 * approach:
 * - participant state is persisted in sessionState and hence independent of toolbar component open/close
 * - we have to handle the following cases
 *  - participant array (parr) length = 0 -> nothing to do
 *  - parr.length = 1 OR > 1 -> her we have to check...
 *  - ... ANY toolbarStatus atrributes set (!== null) -> if so, this attribute was once set in the so far running session lifecycle, hence we change NOTHING as this should be the intended status for the participant(s). This should be valid for 1 participant and > 1 participants scenarios
 *  - NO toolbarStatus atrributes set (=== null/undefined) -> if so ...
 *    - ...set toolbarStatus = true for 1 participant
 *    - ...set parr[0].toolbarStatus = true for > 1 participants
 * - if parr.length > 0, and none have toolbarStatus = true, give a hint: "students have no access..."
 * - do not show the hint, if at least one student is activated
 */

/**
 * - coach sees apps button active, can open apps, sees no participant access button, coach closes apps
 * - 1 participant joins, participant sees app button deactivated, coach opens apps (), coach sees participant access button, can open, sees participant activated
 * - coach open/close apps -> same happens at participant
 * - coach deactivates participant access, toolbar is closed on participant side, apps button is disabled
 * - coach activates ...
 * - 2 participants join, only one of them gets the default activation...
 */

const participantListItem = {
    // key: string,
    access: false,
    name: 'foofoo',
    inSession: false,
    isWaiting: false,
    registrationStatus: 'isCoachCustomer',
}

// only coach perspective
Scenario('coach enters room with no participant available @session', async ({ I }) => {
    // coach logs in
    I.amOnPage('/directlogin?u=coach1@mypelz.de&p=%23coach1');

    cookieConsent(I)

    // start session
    I.click({ css: 'button[name="tangoButton"]'});
    I.waitInUrl('/room/', 12);

    // coach should see apps button
    // I.waitForElement({ css: 'button[name="apps"]'}, 12);
    I.waitForEnabled({ css: 'button[name="apps"]'}, 12);
    
    // open apps
    I.click({ css: 'button[name="apps"]'})
    // toolbar pane
    I.waitForElement({ css: '#apps-toolbar'}, 6);
    // activate apps button
    I.dontSee({ css: 'button[name="activateApps"]'});
    // close apps
    I.click({ css: 'button[name="apps"]'})
});

// 1 participant to join
Scenario('coach enters room, 1 participant joins @session', async ({ I }) => {
    // coach logs in
    I.amOnPage('/directlogin?u=coach1@mypelz.de&p=%23coach1');

    cookieConsent(I)

    // start session
    I.click({ css: 'button[name="tangoButton"]'});
    I.waitInUrl('/room/', 12);

    // start second session
    session('user2 joins', () => {
        I.amOnPage('/directlogin?u=user2@mypelz.de&p=%23user2');
        
        // dismiss cookie consent
        cookieConsent(I)
        
        // go to mycoaches
        I.amOnPage('/mycoaches');
        I.waitInUrl('/mycoaches');
        I.waitForElement({ css: '.buttonGoToClassroom'}, 6);
        // I.click({ css: 'button.buttonGoToClassroom'});
        I.click(locate({ css: '.buttonGoToClassroom'})
            .inside(locate({css: '.my-coaches-row'})
            .withText('coach1')));
        I.waitInUrl('/room/', 12);
        // here we go, we are in the session room

        // apps button should be disabled
        // I.waitForElement(locate({ css: 'button[name="apps"]'}).withAttr({disabled: 'true'}), 12)
        
        // apps button should be automatically enabled
        I.waitForEnabled({ css: 'button[name="apps"]'}, 12);

    });

    // coach should see apps button
    I.waitForEnabled({ css: 'button[name="apps"]'}, 12);    
    // open apps
    I.click({ css: 'button[name="apps"]'})
    // toolbar pane
    I.waitForElement({ css: '#apps-toolbar'}, 6);
    // activate apps button
    I.seeElement({ css: 'button[name="activateApps"]'});
    // should see badge with content "1"
    I.seeElement(locate({css: '.mat-badge-content'}).withText('1').inside(locate({css: 'button[name="activateApps"]'})));

    // start second session
    session('user2 joins', () => {
        // toolbar pane opens
        I.waitForElement({ css: '#apps-toolbar'}, 6);
    });
    
    // ### next journey ###
    I.click({ css: 'button[name="activateApps"]'});
    I.waitForElement(locate({css: 'button.mat-menu-item.toolbar-status.enabled'}).withText('user2').inside(locate({css: 'div.mat-menu-panel'})));
    I.click(locate({css: 'button.mat-menu-item.toolbar-status.enabled'}).withText('user2').inside(locate({css: 'div.mat-menu-panel'})));
    I.dontSeeElement(locate({css: '.mat-badge-content'}).withText('1').inside(locate({css: 'button[name="activateApps"]'})));
    
    I.click({ css: 'button[name="activateApps"]'});
    I.waitForElement(locate({css: 'button.mat-menu-item.toolbar-status.disabled'}).withText('user2').inside(locate({css: 'div.mat-menu-panel'})));

    // start second session
    session('user2 joins', () => {
        // toolbar pane closes
        I.waitForInvisible({ css: '#apps-toolbar'}, 6);
    });

    // close apps
    I.click({ css: 'button[name="apps"]'})

});

// multiple participants join...
Scenario('coach enters room, 2 participants joins @session', async ({ I }) => {
    // coach logs in
    I.amOnPage('/directlogin?u=coach1@mypelz.de&p=%23coach1');

    cookieConsent(I)

    // start session
    I.click({ css: 'button[name="tangoButton"]'});
    I.waitInUrl('/room/', 12);

    // start second session
    session('user2 joins', () => {
        const user2pwEncoded = encodeURIComponent(config.user2pw);
        I.amOnPage(`/directlogin?u=${config.user2user}&p=${user2pwEncoded}`);
        
        // dismiss cookie consent
        cookieConsent(I)
        
        // go to mycoaches
        I.amOnPage('/mycoaches');
        I.waitInUrl('/mycoaches');
        I.waitForElement({ css: '.buttonGoToClassroom'}, 12);
        // I.click({ css: 'button.buttonGoToClassroom'});
        I.click(locate({ css: '.buttonGoToClassroom'})
            .inside(locate({css: '.my-coaches-row'})
            .withText('coach1')));
        I.waitInUrl('/room/', 12);
        // here we go, we are in the session room

        // apps button should be disabled
        // I.waitForElement(locate({ css: 'button[name="apps"]'}).withAttr({disabled: 'true'}), 12)
        
        // apps button should be automatically enabled
        I.waitForEnabled({ css: 'button[name="apps"]'}, 12);

    });

    // start second session
    session('user1 joins', () => {
        const user1pwEncoded = encodeURIComponent(config.user1pw);
        I.amOnPage(`/directlogin?u=${config.user1user}&p=${user1pwEncoded}`);
        // dismiss cookie consent
        cookieConsent(I)
        
        // go to mycoaches
        I.amOnPage('/mycoaches');
        I.waitInUrl('/mycoaches');
        I.waitForElement({ css: '.buttonGoToClassroom'}, 6);
        // I.click({ css: 'button.buttonGoToClassroom'});
        I.click(locate({ css: '.buttonGoToClassroom'})
            .inside(locate({css: '.my-coaches-row'})
            .withText('coach1')));
        I.waitInUrl('/room/', 12);
        // here we go, we are in the session room

        // apps button should be disabled
        I.waitForElement(locate({ css: 'button[name="apps"]'}).withAttr({disabled: 'true'}), 12)
        
        // apps button should be automatically enabled
        // I.waitForEnabled({ css: 'button[name="apps"]'}, 12);

    });

    // coach should see apps button
    I.waitForEnabled({ css: 'button[name="apps"]'}, 12);    
    // open apps
    I.click({ css: 'button[name="apps"]'})
    // toolbar pane
    I.waitForElement({ css: '#apps-toolbar'}, 6);
    // activate apps button
    I.seeElement({ css: 'button[name="activateApps"]'});

    // should see badge with content "1"
    I.seeElement(locate({css: '.mat-badge-content'}).withText('1').inside(locate({css: 'button[name="activateApps"]'})));

    // start second session
    session('user2 joins', () => {
        // toolbar pane opens
        I.waitForElement({ css: '#apps-toolbar'}, 6);
    });
    
    // ### next journey ###
    I.click({ css: 'button[name="activateApps"]'});
    I.waitForElement(locate({css: 'button.mat-menu-item.toolbar-status.enabled'}).withText('user2').inside(locate({css: 'div.mat-menu-panel'})));
    I.waitForElement(locate({css: 'button.mat-menu-item.toolbar-status.disabled'}).withText('user1').inside(locate({css: 'div.mat-menu-panel'})));

    // close apps
    I.click({ css: 'button[name="apps"]'})

});




