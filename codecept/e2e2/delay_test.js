/// <reference path="../steps.d.ts" />

let config = require('codeceptjs').config.get();

const assert = require('assert');
const cookieConsent = require('./helper/cookieConsent');

Feature('master delay');
    //.timeout(2000); // set timeout to 5s;

Scenario('go to presession, check initial delay @smoke', async ({ I }) => {
    // home cookie handling
    I.amOnPage('/');
    // dismiss cookie consent
    cookieConsent(I);

    I.waitForClickable({ css: 'button[name="preflight"]'});
    I.click({ css: 'button[name="preflight"]'});

    // dismiss cookie
    I.waitForElement({ css: 'p[name="masterDelay"]'}, 4);
    I.see('0.100', { css: 'p[name="masterDelay"]'});

    I.scrollPageToBottom();

    // dismiss cookie
    I.waitForClickable({ css: 'button[name="calculateDelay"]'}, 16);
    I.click({ css: 'button[name="calculateDelay"]'});
    I.seeInCurrentUrl('/apps/delay');

    I.see('0.000', { css: 'p[name="masterDelay"] > strong'});

    I.click({ css: 'button[name="delayTestStart"]'});

    I.waitForVisible({ css: 'button[name="saveDelayQuit"]'}, 30);
    const calculatedMasterDelay = await I.grabValueFrom({ css: 'p[name="masterDelay"] > strong'});
    I.wait(2);

    I.click({ css: 'button[name="saveDelayQuit"]'});

    I.seeInCurrentUrl('/presession');
    I.waitForInvisible('.overlay', 6);

    I.waitForElement({ css: 'p[name="masterDelay"]'}, 4);
    const storedMasterDelay = await I.grabValueFrom({ css: 'p[name="masterDelay"] > strong'});

    // both values should be the same
    assert.strictEqual(calculatedMasterDelay, storedMasterDelay);

    // I.wait(2);

});
 

