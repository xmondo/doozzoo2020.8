/// <reference path="../steps.d.ts" />

const config = require('codeceptjs').config.get();

/**
 * activate/deactivate account -> find it reflected on FB level
 * customer3@mypelz.de -> role 0, 1GaYBJtvGNccp9umENFbYpqn2o52, #customer3
 * email=customer3%40mypelz.de&password=%23customer3&returnSecureToken=true
 * 
 * customer5 logs into admin account of Heinsberg
 * on the list he selects customer3
 * he deactivates him
 * --> account should be deactivated
 * 
 * he activates him
 * --> account should be activated
 */

const assert = require('assert');

Feature('edu_adminlist');


Before(({ I }) => { // or Background
    I.amOnPage('/');

    // wait
    I.waitForElement({ css: '.cookie-consent-dialog-container'}, 6);
    I.click({ css: 'button[name="accept"]'});
    I.waitForInvisible({ css: '.cookie-consent-dialog-container'}, 6);
    
    I.click({css: 'button.footerLogin'});
    I.fillField('E-Mail', config.customer5user);
    I.fillField('Password', config.customer5pw);
    I.click({css: 'button[name="loginSubmit"]'});

});


Scenario('test user deactivation @edu', async ({ I }) => {

    I.see('Please click below to access your institution backoffice','.mat-title');

    I.see('Access institution backoffice');
    I.click('Access institution backoffice');

    I.click(locate({css: 'button[name="edu-backoffice"]'}).inside(locate('div>span').withText('Monheim')));

    I.see('Manage institutional accounts','p.mat-title');

    I.fillField({css: 'input[name="filterInput"]'}, config.customer3user);

    I.scrollPageToBottom();

    I.click(locate('.toggleAccess').inside(locate('div.innerItemRow').withText(config.customer3user)));

    I.wait(1);

    I.haveRequestHeaders({'Content-Type':'application/x-www-form-urlencoded'});
    // email=coach1%40mypelz.de&password=%23coach1&returnSecureToken=true
    let _response = await I.sendPostRequest('https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyBHJt1JFPRSuue0ZkbRNEy7oZcpN7KBAqA','email=customer3%40mypelz.de&password=%23customer3&returnSecureToken=true');
    // auth response
    // ### console.log('rest data response: ',  _response.data);//response.data.idToken);
    // { error: { code: 400, message: 'USER_DISABLED', errors: [ [Object] ] } }
    // { kind: 'identitytoolkit#VerifyPasswordResponse', localId: '1GaYBJtvGNccp9umENFbYpqn2o52', email: 'customer3@mypelz.de', displayName: 'invite from - Kreismusikschule Heinsberg', idToken:'<token>', registered: true, refreshToken: '<token>', expiresIn: '3600' }
    I.wait(1);

    // console.log('### response1: ', _response.data.error);
    if(_response.data.error) {
        // console.log('### response error: ', _response.data.error.message);
        assert.strictEqual(_response.data.error.message,'USER_DISABLED');

        I.click(locate('.toggleAccess').inside(locate('div.innerItemRow').withText(config.customer3user)));
        I.wait(1);

        I.haveRequestHeaders({'Content-Type':'application/x-www-form-urlencoded'});
        // email=coach1%40mypelz.de&password=%23coach1&returnSecureToken=true
        _response = await I.sendPostRequest('https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyBHJt1JFPRSuue0ZkbRNEy7oZcpN7KBAqA','email=customer3%40mypelz.de&password=%23customer3&returnSecureToken=true');

        assert.strictEqual(_response.data.error, undefined);

    } else {
        // console.log('### response: ', _response.data.email);
        // assert.strictEqual(_response.data.email,config.customer3user);

        // in case of the user beeing enabled, this .data.error is simply undefined
        assert.strictEqual(_response.data.error, undefined);
    }

    // pause()

});


