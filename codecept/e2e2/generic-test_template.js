/// <reference path="../steps.d.ts" />

const config = require('codeceptjs').config.get();
const key = config.key;
const isProd = config.profile === 'prod' ? true : false;

const cookieConsent = require('./helper/cookieConsent');

/**
 * this should set up rooms in now
 * prerequisite is, that these rooms are not existing
 * 1. make sure room is not already there
 * 2. create room
 */

Feature('rest_test');

Scenario('rest test', async (I) => {
    //

    cookieConsent(I)

    I.wait(5);
    /*
    I.haveRequestHeaders({'Content-Type':'application/x-www-form-urlencoded'});
    // email=coach1%40mypelz.de&password=%23coach1&returnSecureToken=true
    const response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${key}`,"email=coach1%40mypelz.de&password=%23coach1&returnSecureToken=true");
    // auth response
    // console.log('rest data response: ', response.data.displayName, response.data.idToken);

    // I.wait(1);

    //I.haveRequestHeaders({'Authorization':'Bearer ' + response.data.idToken + '\''});
    const response2 = await I.sendGetRequest('/opentok.json?auth='+response.data.idToken);
    console.log('rest data response2: ', response2.data);

    I.wait(2);

    const mannheim = {
            mannheim1: {
            owner: 'auth0|582c7e4cedb8c73048379aad',
            permanent: true,
            name: 'Mannheim 1',
            lock: false,
            countdown: 57600, 
        } 
    };

    I.sendPatchRequest('/now.json?auth='+response.data.idToken, { manheim1 : null });
    I.wait(2);
    I.sendPatchRequest('/now.json?auth='+response.data.idToken, mannheim);

    const response3 = await I.sendGetRequest('/opentok.json?auth='+response.data.idToken);
    console.log('rest data response3: ', response3.data);

    I.wait(5);

    I.amOnPage('/room/mannheim1');

    I.wait(2);

    */

});
