/// <reference path="../steps.d.ts" />

let config = require('codeceptjs').config.get();
const assert = require('assert');
const cookieConsent = require('./helper/cookieConsent');

Feature('stripe existing user change payment plan flow');

// firebase API keys
const devKey = 'AIzaSyBHJt1JFPRSuue0ZkbRNEy7oZcpN7KBAqA';
const prodKey= 'AIzaSyAklBOk3aEvjfR0KDOK2Ju5GQnlsUCUlnA';
const key = process.env.profile === 'prod' ? prodKey : devKey;

Scenario('log in as coach2, go to profile and change payment type @registration', async ({ I }) => {

    I.amOnPage('/directlogin?u=coach2@mypelz.de&p=%23coach2');

    cookieConsent(I)

    I.amOnPage('/login');

    // start change payment type
    I.waitForVisible({ css: 'button[name="changePlan"]' }, 12)
    // I.seeElement({ css: 'button[name="changePaymentType"]' }) // button when archiving was already booked
    I.click({ css: 'button[name="changePlan"]' });

    I.scrollPageToBottom();

    // select package Nr. 2
    // option list: native codecept does not work as this are fake material controls
    I.waitForVisible({ css: 'mat-select[name="package"]' }, 10);
    // gut current value
    const valArr = [5,10,15,20,25,30,35,40]; // 7
    const _package = await I.grabTextFrom({ css: 'mat-select[name="package"]' });
    // console.log('package: ', package)
    const idx = valArr.findIndex(item => item === parseInt(_package));
    // console.log('idx: ', idx)
    const newVal = idx < 7 ? valArr[idx + 1] : valArr[idx - 1];

    I.click({ css: 'mat-select[name="package"]' });
    I.click(locate({ css: '.mat-option-text'}).withText(newVal.toString()));

    // select Plan
    I.waitForClickable({ css: '.choosePlan2' }, 4);
    I.click({ css: '.choosePlan2' });
    // subscribe
    I.waitForClickable({ css: 'button[name="subscribeNow"]' }, 10);
    I.click({ css: 'button[name="subscribeNow"]' });
    
    I.wait(2)

    I.waitForText(`Up to ${ newVal } students`, 10, { css: 'span[name="numberOfStudents"]'})

});




