/// <reference path="../steps.d.ts" />

let config = require('codeceptjs').config.get();

let inviteUrl = null;

Feature('registered student invite');

Scenario('user get an error message due to invalid invite token @invite', async ({ I }) => {
    //...
    I.amOnPage('/invite/-blablabla___');
    I.waitForText('Ooops, something went wrong. Your invite link is invalid, please try again.', 6);
});

Scenario('coach1 creates invite and user2 calls it not logged in @invite', async ({ I }) => {

    // login as coach1
    I.amOnPage(`/directlogin?u=${config.coach1user}&p=${encodeURIComponent(config.coach1pw)}`);
    I.amOnPage('/');

    // dismiss cookie consent
    I.waitForElement({ css: '.cookie-consent-dialog-container'}, 6);
    I.click({ css: 'button[name="accept"]'});
    I.waitForInvisible({ css: '.cookie-consent-dialog-container'}, 6);

    // navigate to my students
    within({css: 'div#footer'}, () => {
        I.click({css: 'button[name="myStudents"]'});  
    });

    // select tab
    I.click(locate({ css: 'div.mat-tab-label-content'}).withText('Invite & connect students'));
    
    // create link
    const urls = await I.grabTextFromAll('ul#listInviteLinks li span.inviteLink');
    // console.log('---> ', urls.length, urls);
    urls.length < 2 ? I.click({css: 'button[name="createInviteLink"]'}) : '';

    inviteUrl = await I.grabTextFrom({ css: 'ul#listInviteLinks:first-child span.inviteLink'});
    // console.log('inviteUrl: ', inviteUrl);

    // coach1 logs out
    I.amOnPage('/login?mode=logout');

    // call invite as user2, which is NOT logged in
    I.amOnPage(inviteUrl);

    // click passwordForgotten on home
    I.click({css: 'button[name="login"]'});
    // I should see dialog poping up
    I.waitForVisible({ css: '.mat-dialog-container'}, 6);

    // ### Login form ###
    // I enter my email, that I used for registration
    I.fillField('email', config.user2user);
    I.fillField('password', config.user2pw);

    // I.waitForClickable({ css: 'button[name="loginSubmit"]'}, 6);
    I.click({ css: 'button[name="loginSubmit"]'});

    I.see('You have successfully logged in and connected to your coach');

});
