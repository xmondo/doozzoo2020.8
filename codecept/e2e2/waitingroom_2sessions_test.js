/// <reference path="../steps.d.ts" />

const config = require('codeceptjs').config.get();
const cookieConsent = require('./helper/cookieConsent');
const key = config.key;

Feature('waitingroom 2sessions')
    //.timeout(5000); // set timeout to 5s;

/**
 * preconditions:
 * room unlocked
 * user types: anonymous user, registered user, owner
 */

const rNum = Math.floor(Math.random() * 99);
const name = `John Scofield_${rNum}`;
const coachId = config.coach1Id; //'auth0|582c7e4cedb8c73048379aad';
const roomName = 'pro_mannheim1';
const roomUrl = '/pro/mannheim1';

Scenario('prepare @waitingroom @room', (async ({ I }) => { // or Background
    I.haveRequestHeaders({'Content-Type':'application/x-www-form-urlencoded'});
    // email=coach1%40mypelz.de&password=%23coach1&returnSecureToken=true
    const response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${key}`,`email=coach1%40mypelz.de&password=%23coach1&returnSecureToken=true`);
    // auth response
    // console.log('rest data response: ', response.data.displayName, response.data.idToken);

    // reset data for waitingroom tests
    const mannheim = {
            [roomName]: {
            owner: coachId,
            permanent: true,
            name: 'mannheim1',
            locked: false,
            countdown: 57600,
            myroomId: 'g8o1E2orkWc3WKUZQTra',
            plan: 'premium',
        } 
    }
    I.sendPatchRequest('/now.json?auth='+response.data.idToken, { [roomName] : null });
    I.sendPatchRequest('/now.json?auth='+response.data.idToken, mannheim);

    I.sendPatchRequest(`/classrooms/${coachId}.json?auth=${response.data.idToken}`, { locked: false, roomId: null });
    I.sendPatchRequest(`/classrooms/${coachId}.json?auth=${response.data.idToken}`, { participants: null} );
    I.sendPatchRequest(`/now/${roomName}.json?auth=${response.data.idToken}`, { locked: false} );
    I.wait(1);
}));

Scenario('me as owner would like to join the session and lock it, then a user joins @room @waitingroom', ({ I }) => {

    // check preconditions
    I.amOnPage(roomUrl);
    I.see('Waiting room - mannheim1', 'h2');

    cookieConsent(I)

    // start first test
    I.click({css: 'button[name="login"]'});

    I.fillField('E-Mail', config.coach1user);
    I.fillField('Password', config.coach1pw);
    I.click({css: 'button[name="loginSubmit"]'});
    I.dontSee('Please enter e-mail and password');
    I.click('Join as Session Owner');
    I.seeInCurrentUrl(roomUrl);
    I.dontSeeCurrentUrlEquals(`${roomUrl}/waitingroom`);
    // pause()
    I.waitForElement({ css: 'button.sessionLockButton' }, 6);
    I.click({ css: 'button.sessionLockButton' });
    I.wait(1);

    session('anonymousUser', () => {
        // check preconditions
        I.amOnPage(roomUrl);
        I.see('Waiting room - mannheim1', 'h2');
        I.see('Session is locked','p');

        // dismiss cookie consent
        cookieConsent(I);

        I.fillField('anonymousName', name);
        I.click('Submit');
        // I.see('Click the button below to join the session as ' + name, 'p');
        // I.see('Join session');
        I.seeElement(locate({css:'button[name="joinSessionAnonymous"]'}).withAttr({ disabled: 'true' }));
    });
    // only visible for owner
    I.seeElement('.sessionLockButton');
    I.click('.sessionLockButton');

    session('anonymousUser', () => {
        //I.wait(2);
        I.see('Session is open')
        I.waitForClickable({ css: 'button[name="joinSessionAnonymous"]'}, 10);
        //I.wait(2);
        I.click({ css: 'button[name="joinSessionAnonymous"]'});
        //I.wait(2);
        //I.dontSeeCurrentUrlEquals(`${roomUrl}/waitingroom`); 
        I.waitInUrl('/pro/', 12);
    });

    // snackbar shows name
    I.see('John Scofield');

    I.waitForElement({ css: 'button[name="participantsSidenavButton"]'}, 8);
    I.click({ css: 'button[name="participantsSidenavButton"]'});
    I.see('My students');
    I.see(name);

    I.click(locate('.toggleAccess').inside(locate('li').withText(name)));

    session('anonymousUser', () => {
        // I.wait(2);
        // snackbar message
        I.waitForText('must leave the session', 6)
        I.seeInCurrentUrl('/waitingroom');
        // I.click({ css: 'button[name="joinSessionAnonymous"]'});
        I.wait(2);
    });

});


Scenario('prepare @waitingroom 2nd @room', (async ({ I }) => { // or Background
    I.haveRequestHeaders({'Content-Type':'application/x-www-form-urlencoded'});
    // email=coach1%40mypelz.de&password=%23coach1&returnSecureToken=true
    const response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${key}`,`email=coach1%40mypelz.de&password=%23coach1&returnSecureToken=true`);
    // auth response
    // console.log('rest data response: ', response.data.displayName, response.data.idToken);

    // reset data for waitingroom tests
    const mannheim = {
            [roomName]: {
            owner: coachId,
            permanent: true,
            name: 'mannheim1',
            locked: false,
            countdown: 57600,
            myroomId: 'g8o1E2orkWc3WKUZQTra',
            plan: 'premium',
        } 
    }
    I.sendPatchRequest('/now.json?auth='+response.data.idToken, { [roomName] : null });
    I.sendPatchRequest('/now.json?auth='+response.data.idToken, mannheim);

    I.sendPatchRequest(`/classrooms/${coachId}.json?auth=${response.data.idToken}`, { locked: false, roomId: null });
    I.sendPatchRequest(`/classrooms/${coachId}.json?auth=${response.data.idToken}`, { participants: null} );
    I.sendPatchRequest(`/now/${roomName}.json?auth=${response.data.idToken}`, { locked: false} );
    I.wait(1);
}));

const login = require('./helper/login');
Scenario('me as coach share a session url, then a anonymous user joins via waitingroom @room @waitingroom', async ({ I }) => {
    
    login(I,config.coach1user, config.coach1pw);

    // wait for tango button
    I.waitForClickable({ css: 'button[name="myRoomButton"]'}, 16);
    I.click({ css: 'button[name="myRoomButton"]'});

    I.waitInUrl('/pro/', 6);

    const url = await I.grabCurrentUrl();
    const roomId = url.split('/')[4];
    console.log('roomId', roomId)

    // pause()
    I.waitForElement({ css: 'button.sessionLockButton' }, 16);
    I.click({ css: 'button.sessionLockButton' });
    I.wait(1);

    session('anonymousUser', () => {
        // check preconditions
        
        I.amOnPage(`/pro/${roomId}`);
        I.waitForText(`Waiting room - ${roomId}`, 6, 'h2');
        I.see('Session is locked','p');

        // dismiss cookie consent
        cookieConsent(I);

        I.fillField('anonymousName', name);
        I.click('Submit');
        // I.see('Click the button below to join the session as ' + name, 'p');
        // I.see('Join session');
        I.seeElement(locate({css:'button[name="joinSessionAnonymous"]'}).withAttr({ disabled: 'true' }));
    });

    // only visible for owner
    I.seeElement('.sessionLockButton');
    I.click('.sessionLockButton');

    session('anonymousUser', () => {
        I.see('Session is open')
        I.wait(2);
        I.waitForClickable({ css: 'button[name="joinSessionAnonymous"]'}, 12);
        //I.wait(2);
        I.click({ css: 'button[name="joinSessionAnonymous"]'});
        I.dontSeeInCurrentUrl(`${roomUrl}/waitingroom`); 
    });

    // snackbar shows name
    I.see('John Scofield');

    //I.wait(2);
    I.waitForElement({ css: 'button[name="participantsSidenavButton"]'}, 8);
    I.click({ css: 'button[name="participantsSidenavButton"]'});
    I.see('My students');
    I.see(name);

    I.click(locate('.toggleAccess').inside(locate('li').withText(name)));
    // I.wait(2);

    // I.click('close');

    session('anonymousUser', () => {
        I.wait(2);
        // snackbar message
        // I.see('must leave the session')
        I.waitForText('must leave the session', 12);
        I.seeInCurrentUrl('/waitingroom');
        // I.click({ css: 'button[name="joinSessionAnonymous"]'});
        // I.wait(2);
    });

});
