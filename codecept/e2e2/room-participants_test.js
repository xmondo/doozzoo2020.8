/// <reference path="../steps.d.ts" />

const config = require('codeceptjs').config.get();
const cookieConsent = require('./helper/cookieConsent');
const key = config.key;

const assert = require('assert');

Feature('my-students');

// const studentEmail = 'user2@mypelz.de';
const coachEmail = 'stripecoach2@mypelz.de';
const coachId = config.stripecoach2Id;
const user1Id = config.user1Id;
const user2Id = config.user2Id;
const user3Id = config.user3Id;
const user4Id = config.user4Id;
const coach1Id = config.coach1Id;
const admin1pw = config.admin1pw;

/**
 * Objective: ...
 * Special: if a participant entry is generated, the field registrationStatus should be provided with correct values
 * Special: the field "name" is automatically provided by checking whether the profile has been filled in correctly
 * stripecoach2 -> 2X1ZM44k8lXX6t2w1X17fx76DPK2
 * user1 -> [user1Id]
 * user2 -> OFHEb2PkOOY51gLObPrlsLWlAmS2
 * user3 -> [user1Id]
 * user4 -> [user1Id]
 * user5 -> null
 */
/*
export interface ParticipantList {
    key: string,
    access: boolean,
    name?: string,
    inSession?: boolean,
    isWaiting?: boolean,
    registrationStatus: 'isCoachCustomer',
}
*/
const participantListItem = {
    // key: string,
    access: false,
    name: 'foofoo',
    inSession: false,
    isWaiting: false,
    registrationStatus: 'isCoachCustomer',
}



// I send synthetic data (foofoo...), which should be resolved automatically regarding the names
Scenario('prepare my-students sync classroom 1 @my-students', async ({ I }) => {
    I.haveRequestHeaders({'Content-Type':'application/x-www-form-urlencoded'});
    const response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${key}`,`email=admin1%40mypelz.de&password=${admin1pw}&returnSecureToken=true`);
    // auth response
    //console.log('response: ', response);
    
    // remove any edu assignments from stripcoach2
    const r0 = await I.sendPatchRequest(`/users/${coachId}.json?auth=${response.data.idToken}`, { trigger: null }); 
    console.log('response: ', r0)
    
    // setup the stripe package size correctly
    const r1 = await I.sendPatchRequest(`/stripe_customers/${coachId}/customerData/subscriptions/data/0/items/data/0.json?auth=${response.data.idToken}`, { quantity: 1 }); // = 5 students
    // console.log(r1)
    // 
    await I.sendPatchRequest(`/classrooms/${coachId}.json?auth=${response.data.idToken}`, { locked: true });
    // clear participants in session

    const r2 = await I.sendPatchRequest(`/coachCustomer.json?auth=${response.data.idToken}`, { [coachId] : null } );
    // console.log(r2)

    await I.sendPatchRequest(`/coachCustomer/${coachId}.json?auth=${response.data.idToken}`, { [user1Id]: true });
    await I.sendPatchRequest(`/coachCustomer/${coachId}.json?auth=${response.data.idToken}`, { [user2Id]: true });
    await I.sendPatchRequest(`/coachCustomer/${coachId}.json?auth=${response.data.idToken}`, { [user3Id]: true });
    await I.sendPatchRequest(`/coachCustomer/${coachId}.json?auth=${response.data.idToken}`, { [user4Id]: true });
    await I.sendPatchRequest(`/coachCustomer/${coachId}.json?auth=${response.data.idToken}`, { [coach1Id]: true });

});

Scenario('login as coach stripecoach2 go 2 my students see all deactivated & not in session all @my-students', async ({ I }) => {
    // coach logs in
    I.amOnPage('/directlogin?u=stripecoach2@mypelz.de&p=%23stripecoach2');

    cookieConsent(I)

    // start session
    I.click({ css: 'button[name="tangoButton"]'});
    I.waitInUrl('/room/', 12);

    session('override data', async () => {
        I.haveRequestHeaders({'Content-Type':'application/x-www-form-urlencoded'});
        const response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${key}`,`email=admin1%40mypelz.de&password=${admin1pw}&returnSecureToken=true`);
   
        //const r2 = await I.sendPatchRequest(`/classrooms/${coachId}.json?auth=${response.data.idToken}`, { participants: null });
        //console.log(r2)
        // we rartificially rename one participant and expect it to be corrected immediately: foofoo should not be appear in the list
        participantListItem.name = 'foofoo';
        await I.sendPatchRequest(`/classrooms/${coachId}/participants.json?auth=${response.data.idToken}`, { [user1Id]: participantListItem });

    });

    // as no one has access, the regarding hint should be shown
    I.waitForElement({ css: '.participantsNoAccess'}, 12)
    I.see('No student has access to the session', { css: '.participantsNoAccess'});

    I.waitForElement({ css: 'button[name="participantsSidenavButton"]'}, 12);
    I.click({ css: 'button[name="participantsSidenavButton"]'});

    I.waitForElement({ css: 'mat-sidenav.sidenavContentRight'});
    within('app-participant-list', async () => {
        I.seeNumberOfElements({css: 'ul.listMyStudents>li'}, 5);
        const nrOfLi = await I.grabNumberOfVisibleElements({css: 'ul.listMyStudents>li'});
        console.log(nrOfLi);

        // I see resolved names, so no foofoo
        I.dontSee('foofoo');
    })
    // pause()

});

Scenario('prepare my-students sync classroom 2 @my-students', async ({ I }) => {
    I.haveRequestHeaders({'Content-Type':'application/x-www-form-urlencoded'});
    const response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${key}`,`email=admin1%40mypelz.de&password=${admin1pw}&returnSecureToken=true`);
    // auth response
    // console.log('response: ', response);
    // 
    await I.sendPatchRequest(`/classrooms/${coachId}.json?auth=${response.data.idToken}`, { locked: false });
    // clear participants in session

});

Scenario('login as coach stripecoach2 go to session and see students in 3 different statuses @my-students', async ({ I }) => {
    // coach logs in
    I.amOnPage('/directlogin?u=stripecoach2@mypelz.de&p=%23stripecoach2');

    cookieConsent(I)

    // start session
    I.click({ css: 'button[name="tangoButton"]'});
    I.waitInUrl('/room/', 12);

    session('override data', async () => {
        I.haveRequestHeaders({'Content-Type':'application/x-www-form-urlencoded'});
        const response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${key}`,`email=admin1%40mypelz.de&password=${admin1pw}&returnSecureToken=true`);
   
        I.wait(5)
        await I.sendPatchRequest(`/classrooms/${coachId}.json?auth=${response.data.idToken}`, { participants: null });

        // standard
        participantListItem.isWaiting = true;
        participantListItem.inSession = false;
        participantListItem.access = false;
        await I.sendPatchRequest(`/classrooms/${coachId}/participants.json?auth=${response.data.idToken}`, { [user1Id]: participantListItem });
        participantListItem.isWaiting = false;
        participantListItem.inSession = true;
        participantListItem.access = true;
        await I.sendPatchRequest(`/classrooms/${coachId}/participants.json?auth=${response.data.idToken}`, { [user2Id]: participantListItem });
        participantListItem.isWaiting = false;
        participantListItem.inSession = false;
        participantListItem.access = false;
        await I.sendPatchRequest(`/classrooms/${coachId}/participants.json?auth=${response.data.idToken}`, { [user3Id]: participantListItem });
        participantListItem.isWaiting = false;
        participantListItem.inSession = false;
        participantListItem.access = false;
        await I.sendPatchRequest(`/classrooms/${coachId}/participants.json?auth=${response.data.idToken}`, { [user4Id]: participantListItem });
    });

    I.waitForElement({ css: 'button[name="participantsSidenavButton"]'}, 12);
    I.click({ css: 'button[name="participantsSidenavButton"]'});

    I.waitForElement({ css: 'mat-sidenav.sidenavContentRight'});

    I.seeNumberOfElements({css: 'ul.listMyStudents.isWaiting>li'}, 1);
    I.seeNumberOfElements({css: 'ul.listMyStudents.inSession>li'}, 1);
    I.seeNumberOfElements({css: 'ul.listMyStudents.notInSession>li'}, 2);


    I.click({ css: 'button[name="sidenavRightCloseButton"]'});

    // I close session
    I.waitForElement({ css: '#footer'}, 6);
    within('#footer', () => {
        I.waitForElement({ css: 'button[name="home"]'}, 6);
        I.click({ css: 'button[name="home"]'});
    })

    I.waitForVisible({ css: 'button[name="leaveRoomMenueButton"]' }, 16);
    I.click({ css: 'button[name="leaveRoomMenueButton"]' });

    I.wait(2);

});


/*
// check now in database if "inSession" reset to "false"
Scenario('request...', async (I) => {
    I.haveRequestHeaders({'Content-Type':'application/x-www-form-urlencoded'});
    const response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${key}`,`email=admin1%40mypelz.de&password=${admin1pw}&returnSecureToken=true`);
    // auth response
    // console.log('response: ', response);

    const r2 = await I.sendGetRequest(`/classrooms/${coachId}/participants/${user2Id}/inSession.json?auth=${response.data.idToken}`);
        // auth response
    console.log('response: ', r2.data);

    assert.strictEqual(r2.data, false);

});

*/
