/// <reference path="../steps.d.ts" />

const assert = require('assert');

Feature('archiving_0_flow');
    //.timeout(2000); // set timeout to 5s;

/**
 * preconditions:
 * stripecoach1@mypelz.de exists and has booked the archiving option
 * 
 * 1. coach logs in
 * 2. goes to profile
 * 3. sees booked option
 * 4. cancels option
 * 5. sees confirmation dialog, confirms
 * 6. sees changed button
 * 7. book option 
 * 8. confirms revocation
 * 8. sees confirmation dialog, confirms
 * 9. sees changed button, now option booked again
 */
// firebase API keys
const devKey = 'AIzaSyBHJt1JFPRSuue0ZkbRNEy7oZcpN7KBAqA';
const prodKey= 'AIzaSyAklBOk3aEvjfR0KDOK2Ju5GQnlsUCUlnA';
const key = process.env.profile === 'prod' ? prodKey : devKey;

const coachId = 'TCp7tQqe49UPCr7hYjH4CG2IPiF2';

const cookieConsent = require('./helper/cookieConsent');

Scenario('log in as stripecoach1, go to profile and see archiving options @stripe', ({ I }) => {
    I.amOnPage('/directlogin?u=stripecoach1@mypelz.de&p=%23stripecoach1');
cookieConsent(I)

    // I navigate to profile
    within({css: 'div#footer'},() => {
        I.click({css: 'button[name="profile"]'});  
    });

    within({css: 'div#subscriptionDiv'},() => {
        I.see('Session archiving'); // headline
    });

    I.scrollPageToBottom();

    I.seeElement({ css: 'button.archivingConfirmed' }) // button when archiving was already booked

    // start canceling...
    I.click({ css: 'button.archivingConfirmed' });

    I.see('Confirm your cancelation');
    // I navigate to profile
    within({css: '.mat-dialog-container'},() => {
        I.see('Confirm your cancelation'); // headline
        I.click({css: 'button[name="archiving-dialog-confirm"]'});
    });


    // start booking again
    I.seeElement({ css: 'button.archivingBook' }) // button when archiving was NOT booked

    // pause();
    I.seeElement(locate({css:'mat-checkbox[name="revocationStatusCheckbox"]'}).withAttr({ 'ng-reflect-checked': 'false' }));

    I.click({css:'mat-checkbox[name="revocationStatusCheckbox"]'});
    I.seeElement(locate({css:'.archivingBook'}).withAttr({ 'ng-reflect-disabled': 'false' })); // activates booking button

    I.click({css:'.archivingBook'});
    within({css: '.mat-dialog-container'},() => {
        I.see('Confirm your booking'); // headline
        I.click({css: 'button[name="archiving-dialog-confirm"]'});
    });
    
    // see booked option again
    I.seeElement({ css: 'button.archivingConfirmed' });

});


/**
 * log in
 * start session
 * start archiving
 * open media and remember actual metered pricing
 * stop after 60 sec.
 * open media
 * see lesson recording popping up in list
 * check that price was going up bei 1min * 0.04ct
 */

Scenario('log in as stripecoach1, start session and archiving @stripe', async ({ I }) => {
    I.amOnPage('/directlogin?u=stripecoach1@mypelz.de&p=%23stripecoach1');
cookieConsent(I)    

    I.click({css: '.startTango'});
    I.wait(2);

    I.scrollPageToBottom();

    // I navigate to profile
    within({css: '#footer'}, () => {
        I.waitForElement({css: 'button[name="archiveMenu"]'}, 20);
        I.click({css: 'button[name="archiveMenu"]'});
        I.wait(1);
        // read out current seconds of archived video
        I.click({css: 'button[name="media"]'});
        I.click({css: 'button[name="media"]'});
        I.wait(1);
    });
    
    // pause();

    within({css: 'mat-tab-header'}, () => {
        I.click('My archived sessions');
    });
    I.see('Hours of archived video in this subscription period');
    let seconds = await I.grabAttributeFrom('div.total-duration', 'data-total-duration');
    console.log('seconds so far: ', seconds);
    I.click({css: 'button[name="sidenavRightCloseButton"]'});
    I.wait(1);

    // pause()
    within({css: '#footer'}, () => {
        // open menu ...
        I.click({css: 'button[name="archiveMenu"]'});  
    });

    // pause()
    within({css: 'div.mat-menu-panel'},() => {
        // stop it ...
        I.click({css: 'button.archiveButton'});  
    });
    I.wait(5);

    // check whether the video is visible
     // I navigate to profile
     within({css: '#footer'}, () => {
        // read out current seconds of archived video
        I.click({css: 'button[name="media"]'});
        //I.click({css: 'button[name="media"]'});
    });
    within({css: 'mat-tab-header'}, () => {
        I.click('My archived sessions');
    });
    I.wait(20);
    let secondsAfter = await I.grabAttributeFrom('div.total-duration', 'data-total-duration');
    console.log('seconds so far: ', seconds, secondsAfter);

    // assert.equal(seconds, secondsAfter - 1);

    const filenames = await I.grabAttributeFrom('input.inputFilenameLabel', 'data-filename');
    console.log('filenames: ', filenames[0]);
    const time = filenames[0].split('_')[2].split('.')[0];
    console.log('time: ', time);
    
    I.wait(5);

});




