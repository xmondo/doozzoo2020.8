/// <reference path="../steps.d.ts" />

const config = require('codeceptjs').config.get();
const key = config.key;

/**
 * this should set up rooms in now
 * prerequisite is, that these rooms are not existing
 * 1. make sure room is not already there
 * 2. create room
 */

const MailListener = require('mail-listener2');
const assert = require('assert');

// here goes your email connection configuration
const mailListener = new MailListener({
    username: 'pelz@mypelz.de',
    password: 'SIEBEN#meilen?stiefel',
    host: 'imap.1und1.de',
    port: 993, // imap port
    tls: true,
    tlsOptions: { rejectUnauthorized: false },
    mailbox: 'Inbox', // mailbox to monitor
    searchFilter: ['UNSEEN'], // ["UNSEEN", "FLAGGED"], // the search filter being used after an IDLE notification has been retrieved
    markSeen: true, // all fetched email willbe marked as seen and not fetched next time
    fetchUnreadOnStart: true, // use it only if you want to get all unread email on lib start. Default is `false`,
    mailParserOptions: { streamAttachments: true }, // options to be passed to mailParser lib.
    attachments: false, // download attachments as they are encountered to the project directory
    attachmentOptions: { directory: 'attachments/' } // specify a download directory for attachments
});

mailListener.start();

mailListener.on('error', function (err) {
    console.log('mailListener error: ', err);
});

mailListener.on('server:connected', function () {
    console.log('Mail listener initialized');
});

mailListener.on('mail', function (mail, seqno, attributes) {
    // do something with mail object including attachments
    // console.log("emailParsed", mail);
    // mail processing code goes here
});

// helper
function getLastEmail() {
    // console.log('now waiting for an email...');
    return new Promise(resolve => {
        mailListener.on('mail', function (mail) {
            resolve(mail);
        });
    });
}

Feature('new coach registration premium plan');
const user = 'coach2@mypelz.de';
const password = '#coach2';
const cookieConsent = require('./helper/cookieConsent');

Scenario('register as stripecoach2 for premium plan @registration', async ({ I }) => {
    //...
    //console.log('env prod: ', key);

    // 'should delete coach'
    I.amOnPage(`/login?mode=logindelete&u=${config.stripecoach2user}&p=${encodeURIComponent(config.stripecoach2pw)}`);
    I.wait(2);
    I.amOnPage('/');

    cookieConsent(I)

    // click register on home
    // I.click(locate({ css: 'div.mat-tab-label' }).withText('Register'));
    I.click({css: 'button[name="register"]'});
    I.wait(1);

    I.fillField('input[name="email"]', config.stripecoach2user);
    I.fillField('input[name="emailRepeated"]', config.stripecoach2user);
    I.fillField('input[name="password"]', config.stripecoach2pw);
    I.fillField('input[name="passwordRepeated"]', config.stripecoach2pw);
    I.click({css: 'button[name="registerSubmit"]'});
    I.wait(2);
    
    // wait for email and fetch its content
    const email = await getLastEmail();
    // console.log('email content: ', email.text);

    const from = 'support@doozzoo.com';
    assert.strictEqual(email.subject, 'Verify your email for doozzoo');
    assert.strictEqual(email.headers.from, from);
    assert.strictEqual(email.headers.to, config.stripecoach2user);

    // extract registration code from the email message
    const pattern = /(\boobCode=\b[a-zA-Z0-9&=\-_]*)/g;
    const regCode = pattern.exec(email.text);
    // console.log('extracted link: ', pattern, regCode, regCode[0]);

    // expose link
    registerUrl = config.url + '/login?mode=verifyEmail&' + regCode[0];

    mailListener.stop();

    // ### email double opt in ####
    // pause()

    // click registration link
    I.amOnPage(registerUrl);

    I.waitForText('Your account has been verified', 6);

    // click verification continue
    // !!! will now automatically invoked by filling the form
    // I.waitForClickable({ css: 'button[name="emailVerifiedNext"]' }, 6);
    I.click({css: 'button[name="emailVerifiedNext"]'});

    // ###  Profile form ###

    // wait for Form
    I.waitForVisible({css: 'div[name="profileForm"]'}, 6);

    // select Profile form for coach role
    I.click({css: 'mat-radio-button[value="coach"]'});

    // select avatar
    I.waitForClickable({css: '.avatarIcon'}, 5);
    I.click({css: '.avatarIcon'});

    // select a random avatar image
    const rNum = Math.floor(Math.random() * 20);
    const imgSelector = `img[src="assets/img/avatars/avataaars_${rNum}.svg"]`;
    I.click({css: imgSelector});

    // fill in profile form
    I.fillField('input[formControlName="firstname"]', 'coach2 first name');
    I.fillField('input[formControlName="lastname"]', 'coach2 lastname');
    I.fillField('input[formControlName="nickname"]', 'coach2nick');

    I.fillField('input[formControlName="address_street1"]', 'Baker Street');
    I.fillField('input[formControlName="zipcode"]', '456789');
    I.fillField('input[formControlName="city"]', 'Antwerp');

    I.fillField('input[formControlName="birthday"]', '23.04.1956');
    I.fillField('input[formControlName="phone"]', '+49 241 888999');

    // option list: native codecept does not work as this are fake material controls
    I.click({ css: 'mat-select[formControlName="country"]' });
    // I.click({ css: '.mat-option-text'});
    I.click(locate({ css: '.mat-option-text'}).withText('Cyprus'));

    I.click('mat-checkbox[formControlName="dataApproval"]');
    //I.checkOption({css: 'mat-checkbox[formControlName="dataApproval"]'});

    I.scrollPageToBottom();

    I.waitForClickable({ css: 'button[name="profileFormCoachSubmit"]' }, 16);
    I.click({ css: 'button[name="profileFormCoachSubmit"]' });

    // proceed
    I.click({css: 'button[name="profileVerifiedNext"]'});

    // ###  Select plan ###

    // select "premium"
    I.click({ css: 'button[name="selectPlanPremium"]' });

    I.waitForText('You selected plan Pro Coach Premium', 6);
    I.scrollPageToBottom();

    I.click({ css: 'button[name="confirmPlanPremium"]' });

    I.click({ css: 'button[name="selectPlanNext"]' });

    I.waitForVisible({ css: '#subscriptionDiv' }, 6);

    // ### go back to ensure that the plan still can be changed
    I.click({ css: 'button[name="subscriptionPrevious"]' });
    I.scrollPageToBottom();

    // select "starter"
    I.click({ css: 'button[name="selectPlanStarter"]' });

    I.click({ css: 'button[name="confirmPlanStarter"]' });
    I.scrollPageToBottom();

    I.click({ css: 'button[name="selectPlanNext"]' });

    // success, changing plan works
    I.waitForText('Hi coach2nick, you sucessfully applied as "Coach" at doozzoo, have fun!', 6);

    // ### now go back again and continue with "Premium" 
    I.click({ css: 'button[name="donePrevious"]' });
    I.scrollPageToBottom();

    I.click({ css: 'button[name="selectPlanPremium"]' });

    I.click({ css: 'button[name="confirmPlanPremium"]' });
    I.scrollPageToBottom();

    I.click({ css: 'button[name="selectPlanNext"]' });

    // ### Stripe subscription ###

    // I.waitForVisible({ css: '#subscriptionDiv' }, 6);

    // pause();
    
    I.waitForVisible({ css: 'input[name="voucherInput"]' }, 6);
    I.scrollPageToBottom();
    I.clearField({ css: 'input[name="voucherInput"]' });
    I.fillField({ css: 'input[name="voucherInput"]' }, config.stripeVoucher + '000');
    I.wait(1);
    I.fillField({ css: 'input[name="voucherInput"]' }, config.stripeVoucher);
    // only on dev/localhost with artificial cardNr, zip is necessary
    // I.waitForVisible({ css: 'span[name="voucherName"]' }, 16);
    config.profile === 'dev' || config.profile === 'default' ? console.log(`should show: ${config.stripeVoucherName}`) :  I.waitForText(config.stripeVoucherName, 30, { css: 'span[name="voucherName"]' });
    

    // select package Nr. 2
    // option list: native codecept does not work as this are fake material controls
    I.waitForVisible({ css: 'mat-select[name="package"]' }, 10);
    I.click({ css: 'mat-select[name="package"]' });
    I.click(locate({ css: '.mat-option-text'}).withText('10'));

    // select Plan
    I.click({ css: '.choosePlan1' });
    I.scrollPageToBottom();

    // pause()

    // we must see payment block
    I.waitForVisible({ css: '[name="paymentBlockHeadline"]' }, 6);
    I.waitForClickable({ css: 'mat-radio-button[value="sepa"]'}, 6);
    // switch to sepa...
    I.click({ css: 'mat-radio-button[value="sepa"]' });
    I.seeElement({ css: 'app-sepa' });

    I.fillField({ css: 'input[name="accountholderName"]'}, 'Christoph Pelz'); 

    I.seeElement({ css: 'iframe[title="Secure IBAN input frame"]'})

    within({frame: '[title="Secure IBAN input frame"]'}, async () => {
        const iban = config.stripeIban; // 'DE89 3704 0044 0532 0130 00'; 
        // const iban = 'DE17 3706 0193 1012 6440 23';  
        I.seeElement({ css: 'input[name="iban"]'});
        I.clearField({ css: 'input[name="iban"]'});
        I.fillField({ css: 'input[name="iban"]'}, iban); 
        const _iban = await I.grabValueFrom({ css: 'input[name="iban"]'});
        if(_iban !== iban) {
            console.log('iban is incorrect: ', _iban)
            I.clearField({ css: 'input[name="iban"]'});
            I.fillField({ css: 'input[name="iban"]'}, iban); 
        }
    });

    I.click({ css: 'button[name="sepaSubmit"]' });

    I.wait(1);

    // I should see the subscribe button
    I.waitForClickable({ css: 'button[name="subscribeNow"]'}, 15);

    // pause()

    I.click({ css: 'button[name="subscribeNow"]'});
    I.wait(2)

    // Overlay pops up
    I.waitForVisible({ css: '.overlay'}, 4);
    // Finally close overlay

    // Overlay should be closed
    I.waitForInvisible({ css: '.overlay'}, 10);
    // Finally close overlay

    I.click({ css: 'button[name="subscriptionNext"]' });

    I.waitForText('you sucessfully applied as "Coach" at doozzoo', 6);

    // pause()

});


/**
 * login
 * see roomToken free-...
 * do NOT see mycoaches, mystudents -> within #footer
 * start session
 * do NOT see archiving
 * see sessionTimer, should have 100 min. -> .session-time 100 min.
 * */ 

 Scenario('login as coach2 and start freemium session @registration', async ({ I }) => {
    I.amOnPage(`/directlogin?u=${config.stripecoach2user}&p=${encodeURIComponent(config.stripecoach2pw)}`);
    I.amOnPage('/');
    // dismiss cookie consent
    I.waitForElement({ css: '.cookie-consent-dialog-container'}, 6);
    I.click({ css: 'button[name="accept"]'});
    I.waitForInvisible({ css: '.cookie-consent-dialog-container'}, 6);

    I.seeElement({ css: 'button[name="myCoaches"]'});
    I.seeElement({ css: 'button[name="myStudents"]'});

    I.waitForClickable({ css: 'button[name="myRoomButton"]'}, 8);
    I.see('vip-');
    const roomToken = await I.grabTextFrom({ css: 'span.my-room'})

    I.click({ css: 'button[name="myRoomButton"]'});
    I.waitInUrl(roomToken, 6);

    // not shown for coaches??
    // I.waitForElement({ css: '.session-time'}, 12);

    I.waitForElement({ css: 'button[name="archiveMenu"]'}, 12);
    // I.see('history 90 min.', { css: '.session-time'});
    // I.see('600', { css: '.session-time>span'})

    I.click({ css: 'button[name="home"]'});
    I.waitForVisible({ css: 'button[name="leaveRoomMenueButton"]' }, 16);
    I.click({ css: 'button[name="leaveRoomMenueButton"]' });
    I.wait(2);

    // pause()

});
