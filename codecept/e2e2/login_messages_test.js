/// <reference path="../steps.d.ts" />

let config = require('codeceptjs').config.get();
const cookieConsent = require('./helper/cookieConsent');

/**
 * 
 */

const MailListener = require('mail-listener2');
const assert = require('assert');

// here goes your email connection configuration
const mailListener = new MailListener({
    username: 'pelz@mypelz.de',
    password: 'SIEBEN#meilen?stiefel',
    host: 'imap.1und1.de',
    port: 993, // imap port
    tls: true,
    tlsOptions: { rejectUnauthorized: false },
    mailbox: 'Inbox', // mailbox to monitor
    searchFilter: ['UNSEEN'], // ["UNSEEN", "FLAGGED"], // the search filter being used after an IDLE notification has been retrieved
    markSeen: true, // all fetched email willbe marked as seen and not fetched next time
    fetchUnreadOnStart: true, // use it only if you want to get all unread email on lib start. Default is `false`,
    mailParserOptions: { streamAttachments: true }, // options to be passed to mailParser lib.
    attachments: false, // download attachments as they are encountered to the project directory
    attachmentOptions: { directory: 'attachments/' } // specify a download directory for attachments
});

mailListener.start();

mailListener.on('error', function (err) {
    console.log('mailListener error: ', err);
});

mailListener.on('server:connected', function () {
    console.log('Mail listener initialized');
});

mailListener.on('mail', function (mail, seqno, attributes) {
    // do something with mail object including attachments
    // console.log("emailParsed", mail);
    // mail processing code goes here
});

// helper
function getLastEmail() {
    // console.log('now waiting for an email...');
    return new Promise(resolve => {
        mailListener.on('mail', function (mail) {
            resolve(mail);
        });
    });
}

const user = 'user2@mypelz.de';
//const password = '#user2';
const password1 = '#user2';
const password1Url = '%23user2';
const password2 = 'FooFoo';

Feature('login messages and errors');

Scenario('messages and errors @registration @login', async ({ I }) => {

    I.amOnPage('/');

    cookieConsent(I)

    // I use language switcher
    I.seeElement({css: 'button[name="languageMenu"]'});
    I.click({css: 'button[name="languageMenu"]'});
    I.waitForClickable({css: 'button[role="menuitem"]'}, 3);
    // select DE
    within({css: '.mat-menu-content'},() => {
        I.click('Deutsch'); // headline
    });

    // click passwordForgotten on home
    I.click({css: 'button[name="login"]'});
    // I should see dialog poping up
    I.waitForVisible({ css: '.mat-dialog-container'}, 6);
    I.see('Bitte E-Mail und Passwort eingeben:');

    // ### Login form ###
    // I enter my email, that I used for registration
    I.fillField('email', 'coach1@mypelz.de');
    I.fillField('password', 'blahblah');

    I.waitForClickable({ css: 'button[name="loginSubmit"]'}, 6);
    I.click({ css: 'button[name="loginSubmit"]'});
    // I wait for success feedback
    I.waitForText('Falsches Passwort, bitte erneut versuchen!', 6);

    
    // ### register ###
    // click register on home
    I.click(locate({ css: 'div.mat-tab-label' }).withText('Registrieren'));
    // I.click({css: 'button[name="register"]'});
    I.wait(1);

    I.fillField('input[name="email"]', user);
    I.fillField('input[name="emailRepeated"]', user);
    I.fillField('input[name="password"]', 'foofoo');
    I.fillField('input[name="passwordRepeated"]', 'foofoo');
    I.click({css: 'button[name="registerSubmit"]'});

    I.waitForText('E-Mail bereits verwendet, versuchen Sie es mit einer anderen!');

    // pause()

    I.seeElement(locate({ css: 'div.mat-tab-label' }).withText('Passwort zurücksetzen'));

    // ### reset password ###
    I.click(locate({ css: 'div.mat-tab-label' }).withText('Passwort zurücksetzen'));
    // I.click({css: 'button[name="register"]'});
    I.waitForElement({ css: 'input[name="forgottenEmail"]'}, 6);
    I.fillField('input[name="forgottenEmail"]', 'foo' + user);
    I.fillField('input[name="forgottenEmailRepeated"]', 'foo' + user);

    I.click({css: 'button[name="resetPassword"]'});

    I.waitForText('Benutzer nicht gefunden ! Es gibt keinen Benutzer, der dieser Kennung entspricht.');

});

/* error codes

auth/wrong-password ### covered

auth/email-already-in-use ### covered

auth/user-disabled
    
auth/user-exists-email-not-verified
    
auth/user-not-found ### covered

auth/too-many-requests
    
auth/user-token-expired
    
auth/invalid-action-code // !!! email verification
    
login.generic-error

*/
