/// <reference path="../steps.d.ts" />

const config = require('codeceptjs').config.get();
const cookieConsent = require('./helper/cookieConsent');
const key = config.key;

Feature('waitingroom-simple');
    //.timeout(2000); // set timeout to 5s;

/**
 * preconditions:
 * room unlocked
 * user types: anonymous user, registered user, owner
 * 
 * MISSING: role != 1 for registered joiners
 */

const name = randomName(); // 'Miles Davis';

const coachId = config.coach1Id; //'auth0|582c7e4cedb8c73048379aad';
const roomName = 'pro_mannheim1';
const roomUrl = '/pro/mannheim1';

function randomName() {
    const names = ['H. Kohl', 'w. Neumann', 'F. Kahlo', 'M. Davis', 'J. Coltrane', 'G. Djini' ];
    const idx = Math.floor(names.length * Math.random());
    return names[idx];
}

Before(({ I }) => { // or Background
    // check preconditions
    I.amOnPage(roomUrl);
    I.see('Waiting room - mannheim1', 'h2');

    cookieConsent(I)
});

// ### session OPEN scenarios ### 
Scenario('me as anonymous user with id = "null", would like to join the session @waitingroom @room', ({ I }) => {
    //
    I.see('Session is open','p');
    I.fillField('anonymousName', name);
    I.click('Submit');
    I.dontSee({css:'.anonymousName'});
    I.waitForText('Click the button below to join the session - ' + name, 6);
    // Per default anonymous is added to rooms participant list, but access is false
    // button must be enabled
    I.seeElement({ css: 'button[name="joinSessionAnonymous"]'});
});


Scenario('me as anonymous user can login without email-verify redirect in dialog @waitingroom @room', ({ I }) => {
    //
    I.see('Session is open','p');
    I.fillField('anonymousName', name);
    I.click('Submit');
    I.dontSee({css:'.anonymousName'});

    I.waitForText('Click the button below to join the session - ' + name, 8);
    // Per default anonymous is added to rooms participant list, but access is false
    // button must be disabled
    I.waitForElement({ css: 'button[name="joinSessionAnonymous"]'}, 6);

    // but now I switch to the login for registered
    I.click({ css: 'button[name="footerLogin"]'});
    I.waitForElement({ css: 'mat-dialog-container'}, 6);

    I.waitForText('Please enter e-mail and password:', 6);
});


Scenario('me as registered user would like to join the session @waitingroom @room @foofoo', ({ I }) => {
    //
    I.see('Session is open','p');
    I.click({css: 'button[name="login"]'});

    I.fillField('E-Mail', 'customer5@mypelz.de');
    I.fillField('Password', '#customer5');
    I.click({css: 'button[name="loginSubmit"]'});
    I.wait(1);
    I.dontSee('Please enter e-mail and password');
    I.dontSee({css:'.anonymousName'});
    // pause()
    // I.waitForElement(locate({css:'button[name="joinSessionRegistered"]'}).withAttr({ 'ng-reflect-disabled': 'false' }), 6);
    I.waitForClickable({css:'button[name="joinSessionRegistered"]'}, 6);

});

Scenario('me as owner would like to join the session @waitingroom @room', ({ I }) => {
    //
    I.see('Session is open','p');
    I.click({css: 'button[name="login"]'});
    I.fillField('E-Mail', 'coach1@mypelz.de');
    I.fillField('Password', '#coach1');
    I.click({css: 'button[name="loginSubmit"]'});
    I.dontSee('Please enter e-mail and password');
    I.dontSee({css:'.anonymousName'});
    I.click('Join as Session Owner');
    I.seeInCurrentUrl('/pro/mannheim1');
    I.dontSeeCurrentUrlEquals('/pro/mannheim1/waitingroom');
    // indicator for beeing the owner
    I.seeElement('.sessionLockButton');
    // !!! closing room for next Scenarios... !!!
    I.click('.sessionLockButton');
});

// ### session LOCKED scenarios ###  !!!FAILS
Scenario('me as anonymous user would like to join the (locked) session @waitingroom @room', ({ I }) => {
    //
    I.see('Session is locked','p');
    I.fillField('anonymousName', name);
    I.click('Submit');
    I.dontSee({css:'.anonymousName'});
    I.waitForText('The session host has been informed that you are waiting to join', 6);
    I.seeElement(locate({css:'button[name="joinSessionAnonymous"]'}).withAttr({ disabled: 'true' }));
});

Scenario('me as registered user would like to join the (locked) session @waitingroom @room', ({ I }) => {
    //
    I.see('Session is locked','p');
    I.click({css: 'button[name="login"]'});

    I.fillField('E-Mail', 'user2@mypelz.de');
    I.fillField('Password', '#user2');
    I.click({css: 'button[name="loginSubmit"]'});
    I.dontSee('Please enter e-mail and password');
    I.dontSee({css:'.anonymousName'});

    // I must see the disabled join button as the session is locked
    I.waitForElement(locate({css:'button[name="joinSessionRegistered"]'}).withAttr({ disabled: 'true' }), 6);
});

Scenario('me as owner would like to join the (locked) session @waitingroom @room', ({ I }) => {
    //
    I.see('Session is locked','p');
    I.click({css: 'button[name="login"]'});
    I.fillField('E-Mail', 'coach1@mypelz.de');
    I.fillField('Password', '#coach1');
    I.click({css: 'button[name="loginSubmit"]'});
    I.wait(1);
    I.dontSee('Please enter e-mail and password');
    I.dontSee({css:'.anonymousName'});
    I.click('Join as Session Owner');
    I.waitInUrl('/pro/mannheim1', 6);
    // I.dontSeeCurrentUrlEquals('/room/mannheim1/waitingroom');
    // indicator for beeing the owner
    I.waitForElement('.sessionLockButton', 6);

});


