/// <reference path="../steps.d.ts" />

const config = require('codeceptjs').config.get();
const cookieConsent = require('./helper/cookieConsent');
const key = config.key;

Feature('my-students');

// const studentEmail = 'user2@mypelz.de';
const coachEmail = 'stripecoach2@mypelz.de';
const coachId = config.stripecoach2Id;
const user1Id = config.user1Id;
const user2Id = config.user2Id;
const user3Id = config.user3Id;
const user4Id = config.user4Id;
const coach1Id = config.coach1Id;
const admin1pw = config.admin1pw;

/**
 * Objective: check whether the selection of students in the basic my student package is reflected in the backend.
 * - mystudents page
 * - package size
 * - package max reached
 * - access granted 4 | 5
 * user1 -> WmYDCkyM3kcTZpRj9ZX63xgy7kx2
 * user2 -> OFHEb2PkOOY51gLObPrlsLWlAmS2
 */

// we set the package size
// we submit 5 students into the coach customer section
Scenario('prepare my-students @my-students', async ({ I }) => {
    I.haveRequestHeaders({'Content-Type':'application/x-www-form-urlencoded'});
    const response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${key}`,`email=${config.stripecoach2user}&password=${config.stripecoach2pw}&returnSecureToken=true`);
    // auth response
    // console.log('response: ', response.data);

    // remove any edu assignments from stripcoach2
    const r0 = await I.sendPatchRequest(`/users/${coachId}.json?auth=${response.data.idToken}`, { trigger: null }); 
    console.log('response: ', key, r0.data)

    // setup the stripe package size correctly
    const r1 = await I.sendPatchRequest(`/stripe_customers/${coachId}/customerData/subscriptions/data/0/items/data/0.json?auth=${response.data.idToken}`, { quantity: 1 }); // = 5 students
    // console.log(r1)
    // 
    await I.sendPatchRequest(`/classrooms/${coachId}.json?auth=${response.data.idToken}`, { locked: true });
    // clear participants in session

    const r2 = await I.sendPatchRequest(`/coachCustomer.json?auth=${response.data.idToken}`, { [coachId] : null } );
    // console.log(r2)

    await I.sendPatchRequest(`/coachCustomer/${coachId}.json?auth=${response.data.idToken}`, { [user1Id]: true });
    await I.sendPatchRequest(`/coachCustomer/${coachId}.json?auth=${response.data.idToken}`, { [user2Id]: true });
    await I.sendPatchRequest(`/coachCustomer/${coachId}.json?auth=${response.data.idToken}`, { [user3Id]: true });
    await I.sendPatchRequest(`/coachCustomer/${coachId}.json?auth=${response.data.idToken}`, { [user4Id]: true });
    await I.sendPatchRequest(`/coachCustomer/${coachId}.json?auth=${response.data.idToken}`, { [coach1Id]: true });

});


Scenario('login as coach stripecoach2 and go to my students @my-students', async ({ I }) => {
    // coach logs in
    I.amOnPage(`/directlogin?u=${config.stripecoach2user}&p=${encodeURIComponent(config.stripecoach2pw)}`);

    cookieConsent(I)

    I.click({ css: 'button[name="myStudents"]'});
    I.waitInUrl('/mystudents', 6);

    // go to my students secion
    I.see('My students', 'h2');
    I.see('Manage your connected student accounts');
    
    I.waitForElement({ css: 'app-package-hint' }, 6);
    I.see('Your subscription: Up to 5 Students', 'app-package-hint');

    // pause()

    within({ css: 'ul.listMyStudents'}, async () => {
        I.seeNumberOfElements({css: 'li.student-list-item'}, 5);
        const nrOfLi = await I.grabNumberOfVisibleElements({css: 'li.student-list-item'});
        console.log(nrOfLi);

        for(let i=1; i<=nrOfLi; i++){
            const classes = await I.grabAttributeFrom({ css: `:nth-child(${i})>mat-checkbox.toggleAccess` }, 'class');
            const state = classes.indexOf('test-checked-true') !== -1 ? true : false;
            console.log('state: ', state);
            !state ? I.click(locate({ css: `:nth-child(${i})>mat-checkbox.toggleAccess` })) : '';
        }    
        
    });

    // should see warn message
    I.waitForElement({ css: 'app-package-hint .warnMessage' }, 6);

    within({ css: 'ul.listMyStudents'}, async () => {
        I.click(locate({ css: `:nth-child(2)>mat-checkbox.toggleAccess` }));          
    });

    // should see warn message
    I.waitForInvisible({ css: 'app-package-hint .warnMessage' }, 6);

});

Scenario('change quantity in backend @my-students', (async ({ I }) => { // or Background
    I.haveRequestHeaders({'Content-Type':'application/x-www-form-urlencoded'});
    let response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${key}`,`email=admin1%40mypelz.de&password=${admin1pw}&returnSecureToken=true`);
    // auth response
    response = await I.sendPatchRequest(`/stripe_customers/${coachId}/customerData/subscriptions/data/0/items/data/0.json?auth=${response.data.idToken}`, { quantity: 0 });
    // console.log(response);
}));  

Scenario('check quantity changes in backend @my-students', (async ({ I }) => { // or Background
    // coach logs in
    I.amOnPage(`/directlogin?u=${config.stripecoach2user}&p=${encodeURIComponent(config.stripecoach2pw)}`);

    cookieConsent(I)

    I.click({ css: 'button[name="myStudents"]'});
    I.waitInUrl('/mystudents', 6);
    
    within({ css: 'app-package-hint'}, async () => {
        I.waitForText('No subscription found');
    });

    // coach toggles a single student access value and expects it to NOT to change,
    // because the package is exeeded!!
    I.waitForElement({ css: 'ul.listMyStudents'}, 6);
    I.waitForElement({ css: `ul.listMyStudents:nth-child(2) mat-checkbox.toggleAccess.test-checked-false` }, 6);
    I.click({ css: `ul.listMyStudents:nth-child(2) mat-checkbox.toggleAccess.test-checked-false` });
    I.wait(2)   
    I.waitForElement({ css: `ul.listMyStudents:nth-child(2) mat-checkbox.toggleAccess.test-checked-false` }, 6);   

    // reset data to previous state
    let response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${key}`,`email=admin1%40mypelz.de&password=${admin1pw}&returnSecureToken=true`);
    await I.sendPatchRequest(`/stripe_customers/${coachId}/customerData/subscriptions/data/0/items/data/0.json?auth=${response.data.idToken}`, { quantity: 1 });

    within({ css: 'app-package-hint'}, async () => {
        I.see('Your subscription: Up to 5 Students');
    });

    pause()

}));
