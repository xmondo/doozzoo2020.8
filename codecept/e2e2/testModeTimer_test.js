/// <reference path="../steps.d.ts" />

const config = require('codeceptjs').config.get();
const cookieConsent = require('./helper/cookieConsent');
const key = config.key;
const isProd = config.profile === 'prod' ? true : false;

const participantListItem = {
    // key: string,
    access: true,
    name: 'foofoo',
    inSession: false,
    isWaiting: false,
    registrationStatus: 'isCoachCustomer',
}

const coach1user = 'coach1@mypelz.de';
const coach1pw = '#coach1';
const coach1Id = config.coach1Id;
const user1user = 'user1@mypelz.de';
const user1pw = '#user1';
const user2Id = config.user2Id;

/**
 * this should set up rooms in now
 * prerequisite is, that these rooms are not existing
 * 1. make sure room is not already there
 * 2. create room
 */

Feature('test mode timer in session room');


Scenario('prepare @testtimer @room', (async ({ I }) => { // or Background
    I.haveRequestHeaders({'Content-Type':'application/x-www-form-urlencoded'});
    // email=coach1%40mypelz.de&password=%23coach1&returnSecureToken=true
    const response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${config.key}`,`email=coach1%40mypelz.de&password=${config.coach1pw}&returnSecureToken=true`);
    // auth response
    // console.log('rest data response: ', response.data.idToken);

    I.sendPatchRequest(`/classrooms/${coach1Id}.json?auth=${response.data.idToken}`, { locked: false, roomId: null });
    I.sendPatchRequest(`/classrooms/${coach1Id}/participants/${user2Id}.json?auth=${response.data.idToken}`, { access: true} );

    I.wait(1);
    //const _response = await I.sendGetRequest(`/classrooms/${coachId}.json?auth=${response.data.idToken}`);
    //console.log('rest data response: ', _response.data);
}));

Scenario('I log into a test session, see timer, which terminates after a few seconds @testtimer @room', async ({ I }) => {
    I.amOnPage('/');

    cookieConsent(I)

    I.scrollPageToBottom();

    I.waitForClickable({ css: 'button[name="testdrive"]'}, 12);
    I.click({ css: 'button[name="testdrive"]'});

    I.seeInCurrentUrl('testdrive');

    I.waitForClickable({ css: 'button[name="tangoButton"]'}, 15);
    I.click({ css: 'button[name="tangoButton"]'});

    // I.wait(2);
    // I.seeInCurrentUrl('/room/');
    I.waitInUrl('/room/', 12);
    I.waitForInvisible({ css: '.waitForPublisherSpinner' }, 15);
    I.scrollPageToBottom();

    I.waitForElement(locate({ css: '.trialTimer'}).inside({ css: '#footer'}), 22);

    const url = await I.grabCurrentUrl();
    const roomId = url.split('/')[4];
    console.log('roomId: ', roomId);

    session('patch data', async () => {
        I.haveRequestHeaders({'Content-Type':'application/x-www-form-urlencoded'});
        let response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${key}`,"email=admin1%40mypelz.de&password=%23admin1&returnSecureToken=true");
        // set countdown 
        let response2 = await I.sendPatchRequest(`/now/${roomId}.json?auth=${response.data.idToken}`, { countdown: '10' });  
        console.log(response2.data)  
    });
    
    I.waitForText('Testing time is over', 22);

});


Scenario('I log in as coach1 and do not see timer, when I terminate, I am redirected to home @testtimer @room', async ({ I }) => {

    const coach1pwEncoded = encodeURIComponent(coach1pw);
    I.amOnPage(`/directlogin?u=${coach1user}&p=${coach1pwEncoded}`);

    cookieConsent(I)

    // wait for tango button
    I.waitForClickable({ css: 'button[name="tangoButton"]'}, 6);
    I.click({ css: 'button[name="tangoButton"]'});

    I.waitInUrl('/room/', 6);
    // I must not see the timer component
    I.dontSeeElement({ css: '.trialTimer'});

    I.waitForElement(locate({ css: '#footer button[name="home"]'}), 12);
    I.click(locate({ css: '#footer button[name="home"]'}));
    I.waitForVisible({ css: 'button[name="leaveRoomMenueButton"]' }, 16);

    // without this wait, we are ending up in the waiting room
    I.wait(1)
    I.click({ css: 'button[name="leaveRoomMenueButton"]' });

    // I am back home
    I.waitInUrl('/', 6);
    I.waitForElement({ css: 'button[name="tangoButton"]'}, 12);

});


Scenario('I log in as coach1 and join session as anonymous, I see timer counting down and get redirected to waitingroom @testtimer @room', async ({ I }) => {

    const coach1pwEncoded = encodeURIComponent(coach1pw);
    I.amOnPage(`/directlogin?u=${coach1user}&p=${coach1pwEncoded}`);

    cookieConsent(I)

    // wait for tango button
    I.waitForClickable({ css: 'button[name="tangoButton"]'}, 6);
    I.click({ css: 'button[name="tangoButton"]'});

    I.waitInUrl('/room/', 6);
    
    const url = await I.grabCurrentUrl();
    const roomId = url.split('/')[4];

    session('anonymousUser', () => {
        // check preconditions

        // pause()
        
        I.amOnPage(`/room/${roomId}`);
        I.waitForText(`Waiting room - ${roomId}`, 6, 'h2');

        // dismiss cookie consent
        cookieConsent(I)

        I.fillField('anonymousName', 'FooFoo');
        I.click('Submit');
        I.wait(1)

        // pause()

        I.waitForElement({css:'button[name="joinSessionAnonymous"]'}, 12);
        I.click({css:'button[name="joinSessionAnonymous"]'});

        I.waitInUrl('/room/', 6);
        I.waitForElement({ css: '.trialTimer'}, 6);

        I.waitForElement(locate({ css: '#footer button[name="home"]'}), 12);
        I.click(locate({ css: '#footer button[name="home"]'}));
        I.waitForVisible({ css: 'button[name="leaveRoomMenueButton"]' }, 16);

        // without this wait, we are ending up in the waiting room
        I.wait(1)
        I.click({ css: 'button[name="leaveRoomMenueButton"]' });

        // I am back in the waitingroom
        I.waitInUrl(`/room/${roomId}/waitingroom`, 6);

    });



    I.waitForElement(locate({ css: '#footer button[name="home"]'}), 12);
    I.click(locate({ css: '#footer button[name="home"]'}));
    I.waitForVisible({ css: 'button[name="leaveRoomMenueButton"]' }, 16);

    // without this wait, we are ending up in the waiting room
    I.wait(1)
    I.click({ css: 'button[name="leaveRoomMenueButton"]' });

    // I am back home
    I.waitInUrl('/', 6);
    I.waitForElement({ css: 'button[name="tangoButton"]'}, 12);

});


Scenario('I log in as coach1 and join session as registered user, not connected to coach, I see timer counting down and get redirected to waitingroom @testtimer @room', async ({ I }) => {

    const coach1pwEncoded = encodeURIComponent(coach1pw);
    I.amOnPage(`/directlogin?u=${coach1user}&p=${coach1pwEncoded}`);

    cookieConsent(I)

    // wait for tango button
    I.waitForClickable({ css: 'button[name="tangoButton"]'}, 6);
    I.click({ css: 'button[name="tangoButton"]'});

    I.waitInUrl('/room/', 6);
    
    const url = await I.grabCurrentUrl();
    const roomId = url.split('/')[4];

    session('anonymousUser', () => {
        // check preconditions

        const user1pwEncoded = encodeURIComponent(user1pw);
        I.amOnPage(`/directlogin?u=${user1user}&p=${user1pwEncoded}`);

        // dismiss cookie consent
        cookieConsent(I)
        
        I.amOnPage(`/room/${roomId}/waitingroom`);
        I.waitForText(`Waiting room - ${roomId}`, 6, 'h2');

        I.wait(1)

        I.waitForElement({css:'button[name="joinSessionRegistered"]'}, 12);
        I.click({css:'button[name="joinSessionRegistered"]'});

        I.waitInUrl('/room/', 6);
        I.waitForElement({ css: '.trialTimer'}, 12);

        I.waitForElement(locate({ css: '#footer button[name="home"]'}), 12);
        I.click(locate({ css: '#footer button[name="home"]'}));
        I.waitForVisible({ css: 'button[name="leaveRoomMenueButton"]' }, 16);

        // without this wait, we are ending up in the waiting room
        I.wait(1)
        I.click({ css: 'button[name="leaveRoomMenueButton"]' });

        // I am back in the waitingroom
        I.waitInUrl(`/room/${roomId}/waitingroom`, 6);

    });

    I.waitForElement(locate({ css: '#footer button[name="home"]'}), 12);
    I.click(locate({ css: '#footer button[name="home"]'}));
    I.waitForVisible({ css: 'button[name="leaveRoomMenueButton"]' }, 16);

    // without this wait, we are ending up in the waiting room
    I.wait(1)
    I.click({ css: 'button[name="leaveRoomMenueButton"]' });

    // I am back home
    I.waitInUrl('/', 6);
    I.waitForElement({ css: 'button[name="tangoButton"]'}, 12);

});

Scenario('I log in as coach1 and join session as user2, I see NO timer counting down and get redirected to my coaches @testtimer @room', async ({ I }) => {

    session('patch data for user2', async () => {
        I.haveRequestHeaders({'Content-Type':'application/x-www-form-urlencoded'});
        let response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${key}`,"email=coach1%40mypelz.de&password=%23coach1&returnSecureToken=true");
        // set countdown 
        I.sendPatchRequest(`/classrooms/${coach1Id}/participants/${user2Id}.json?auth=${response.data.idToken}`, { access: true} ); 
    });

    const coach1pwEncoded = encodeURIComponent(coach1pw);
    I.amOnPage(`/directlogin?u=${coach1user}&p=${coach1pwEncoded}`);

    cookieConsent(I)

    // wait for tango button
    I.waitForClickable({ css: 'button[name="tangoButton"]'}, 6);
    I.click({ css: 'button[name="tangoButton"]'});

    I.waitInUrl('/room/', 6);
    
    const url = await I.grabCurrentUrl();
    const roomId = url.split('/')[4];

    session('user2', () => {
        // check preconditions

        const user2pwEncoded = encodeURIComponent(config.user2pw);
        I.amOnPage(`/directlogin?u=${config.user2user}&p=${user2pwEncoded}`);

        // dismiss cookie consent
        cookieConsent(I)

        // wait for tango button
        I.waitForClickable({ css: 'button[name="mycoaches"]'}, 6);
        I.click({ css: 'button[name="mycoaches"]'});

        I.waitInUrl('/mycoaches', 6);

        I.waitForElement(locate('.buttonGoToClassroom').inside(locate('li').withText('coach1')), 10);
        I.click(locate('.buttonGoToClassroom').inside(locate('li').withText('coach1')));

        I.waitInUrl('/room/', 6);
        I.wait(2)
        I.dontSeeElement({ css: '.trialTimer'});

        I.waitForElement(locate({ css: '#footer button[name="home"]'}), 12);
        I.click(locate({ css: '#footer button[name="home"]'}));
        I.waitForVisible({ css: 'button[name="leaveRoomMenueButton"]' }, 16);

        // without this wait, we are ending up in the waiting room
        I.wait(1)
        I.click({ css: 'button[name="leaveRoomMenueButton"]' });

        // I am back in the waitingroom
        I.waitInUrl(`/mycoaches`, 6);

    });

    I.waitForElement(locate({ css: '#footer button[name="home"]'}), 12);
    I.click(locate({ css: '#footer button[name="home"]'}));
    I.waitForVisible({ css: 'button[name="leaveRoomMenueButton"]' }, 16);

    // without this wait, we are ending up in the waiting room
    I.wait(1)
    I.click({ css: 'button[name="leaveRoomMenueButton"]' });

    // I am back home
    I.waitInUrl('/', 6);
    I.waitForElement({ css: 'button[name="tangoButton"]'}, 12);

});


Scenario('I log in as coach1 and join session as user1, I see timer counting down execute redirect to waitingroom after 20 sec @testtimer @room', async ({ I }) => {

    const coach1pwEncoded = encodeURIComponent(coach1pw);
    I.amOnPage(`/directlogin?u=${coach1user}&p=${coach1pwEncoded}`);

    cookieConsent(I)

    // wait for tango button
    I.waitForClickable({ css: 'button[name="tangoButton"]'}, 6);
    I.click({ css: 'button[name="tangoButton"]'});

    I.waitInUrl('/room/', 6);
    
    const url = await I.grabCurrentUrl();
    const roomId = url.split('/')[4];

    session('patch data', async () => {
        I.haveRequestHeaders({'Content-Type':'application/x-www-form-urlencoded'});
        let response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${key}`,"email=coach1%40mypelz.de&password=%23coach1&returnSecureToken=true");
        // set countdown 
        let response2 = await I.sendPatchRequest(`/now/${roomId}.json?auth=${response.data.idToken}`, { countdown: '20' });  
        // console.log(response2)  
    });

    // pause()

    session('user1', () => {
        // check preconditions

        const user1pwEncoded = encodeURIComponent(user1pw);
        I.amOnPage(`/directlogin?u=${user1user}&p=${user1pwEncoded}`);

        // dismiss cookie consent
        cookieConsent(I)
        
        I.amOnPage(`/room/${roomId}/waitingroom`);
        I.waitForText(`Waiting room - ${roomId}`, 6, 'h2');

        I.wait(1)

        I.waitForElement({css:'button[name="joinSessionRegistered"]'}, 12);
        I.click({css:'button[name="joinSessionRegistered"]'});

        I.waitInUrl('/room/', 6);
        I.waitForElement({ css: '.trialTimer'}, 24);

        I.waitInUrl(`/room/${roomId}/waitingroom`, 24);

    });

    I.waitForElement(locate({ css: '#footer button[name="home"]'}), 12);
    I.click(locate({ css: '#footer button[name="home"]'}));
    I.waitForVisible({ css: 'button[name="leaveRoomMenueButton"]' }, 16);

    // without this wait, we are ending up in the waiting room
    I.wait(1)
    I.click({ css: 'button[name="leaveRoomMenueButton"]' });

});

