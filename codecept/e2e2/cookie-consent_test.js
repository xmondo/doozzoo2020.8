/// <reference path="../steps.d.ts" />

//

const assert = require('assert');

const coachEmail = 'customer5@mypelz.de';
const coachPassword = '#customer5';

Feature('Cookie consent test');

/**
 * 1. first visit to site -> cookieConsent must be null
 * 2. consent dialog should pop up
 * 3. accept all cookies
 * 4. dialog should close
 * 5. local storage should contain regarding cookieConsent object
 * 6. navigation to other page should not open dialog any more
 * 7. open dialog via footer button
 * 8. open individual settings
 * 9. set tracking false
 * 10. save individual settings
 * 11. check cookie data
 */

Scenario('go to home and see cookie content dialog', async ({ I }) => {
    I.amOnPage('/');
    I.usePuppeteerTo('get local storage', async ({ page }) => {
        const localStorageRaw = await page.evaluate(() => localStorage.getItem("cookieConsent"));
        // const localStorageData = JSON.parse(localStorageRaw);
        console.log('localStorageRaw: ', localStorageRaw);
        assert.strictEqual(localStorageRaw, null);
    });

    I.waitForElement({ css: '.cookie-consent-dialog-container'}, 6);

    // 1. click standard option
    I.click({ css: 'button[name="accept"]'});

    I.waitForInvisible({ css: '.cookie-consent-dialog-container'}, 6);

    I.usePuppeteerTo('get local storage', async ({ page, browser }) => {
        // await page.setOfflineMode(true);
        const localStorageRaw = await page.evaluate(() => localStorage.getItem("cookieConsent"));
        const localStorageData = JSON.parse(localStorageRaw);
        // console.log('localStorageData: ', localStorageData.trackingCookies)
        assert.strictEqual(localStorageData.trackingCookies, true);
    });

    // should not start popup
    I.amOnPage('/mycoaches');
    I.waitForInvisible({ css: '.cookie-consent-dialog-container'}, 6);

    // 2. open dialog via footer
    I.click({ css: 'button[name="cookie-consent-dialog-button"]'});
    I.waitForVisible({ css: '.cookie-consent-dialog-container'}, 6);
    I.click({ css: '.mat-expansion-panel-header-title'});
    I.click({ css: 'mat-slide-toggle[name="individual-settings"]'});
    I.click({ css: 'button[name="accept-individual-settings"]'});
    I.waitForInvisible({ css: '.cookie-consent-dialog-container'}, 6);

    I.usePuppeteerTo('get local storage', async ({ page, browser }) => {
        const localStorageRaw = await page.evaluate(() => localStorage.getItem("cookieConsent"));
        const localStorageData = JSON.parse(localStorageRaw);
        // console.log('localStorageData: ', localStorageData.trackingCookies)
        assert.strictEqual(localStorageData.trackingCookies, false);
    });

});

Scenario('go to home, accept All cookies, then login as EDU, but tracking cookie remains false', async ({ I }) => {
    I.amOnPage('/');
    I.usePuppeteerTo('get local storage', async ({ page }) => {
        const localStorageRaw = await page.evaluate(() => localStorage.getItem("cookieConsent"));
        console.log('localStorageRaw: ', localStorageRaw);
        assert.strictEqual(localStorageRaw, null);
    });

    I.waitForElement({ css: '.cookie-consent-dialog-container'}, 6);
    // 1. click standard option
    I.click({ css: 'button[name="accept"]'});
    I.waitForInvisible({ css: '.cookie-consent-dialog-container'}, 6);

    // I.amOnPage(`/directlogin?u=${coachEmail}&p=${coachPassword}`);
    // click login on home
    I.click({css: 'button[name="login"]'});
    I.fillField('E-Mail', coachEmail);
    I.fillField('Password', coachPassword);
    I.click({css: 'button[name="loginSubmit"]'});

    I.wait(2);

    // pause()

    I.usePuppeteerTo('get local storage', async ({ page, browser }) => {
        const localStorageRaw = await page.evaluate(() => localStorage.getItem("cookieConsent"));
        const localStorageData = JSON.parse(localStorageRaw);
        // console.log('localStorageData: ', localStorageData.trackingCookies)
        assert.strictEqual(localStorageData.trackingCookies, false);
    });

});    




