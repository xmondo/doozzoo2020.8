/// <reference path="../steps.d.ts" />

const config = require('codeceptjs').config.get();

/**

 */

const assert = require('assert');

const randomEmail = require('./helper/randomEmail');

const userEmail = 'customer5@mypelz.de';
const userPassword = '#customer5';

const newUserEmail = 'f_opelz1@mypelz.de';
const roleSelectLabel = 'Student'; // Coach | Student
const numberOfNewAccounts = 2;


Feature('edu admin creates users');


Scenario('test user deactivation @edu', async ({ I }) => {

    I.amOnPage('/');

    // wait
    I.waitForElement({ css: '.cookie-consent-dialog-container'}, 6);
    I.click({ css: 'button[name="accept"]'});
    I.waitForInvisible({ css: '.cookie-consent-dialog-container'}, 6);
    
    I.click({css: 'button.footerLogin'});
    I.fillField('E-Mail', config.customer5user);
    I.fillField('Password', config.customer5pw);
    I.click({css: 'button[name="loginSubmit"]'});

    I.waitForElement({ css: 'button[name="toEduBackend"]'}, 6);
    I.click({ css: 'button[name="toEduBackend"]'});

    I.click(locate({css: 'button[name="edu-backoffice"]'}).inside(locate('div>span').withText('Monheim')));

    I.scrollPageToBottom();

    //
    let i = 0; 
    while(i < numberOfNewAccounts) {

        const _newUserEmail = randomEmail('@mypelz.de');
        
        I.fillField({ css: 'input[name="email"]'}, _newUserEmail);

        I.waitForEnabled({ css: 'input[name="email2"]'}, 6);
        I.fillField({ css: 'input[name="email2"]'}, _newUserEmail);

            // option list: native codecept does not work as this are fake material controls
        I.click({ css: 'mat-select[name="role"]' });
        // I.click({ css: '.mat-option-text'});
        I.waitForElement(locate({ css: 'mat-option'}).withText(roleSelectLabel), 6);
        I.click(locate({ css: '.mat-option-text'}).withText(roleSelectLabel));

        I.waitForClickable({ css: 'button[name="createAccount"]'}, 12);
        I.click({ css: 'button[name="createAccount"]'});

        i++;
    }

    pause()
    

/*   
    I.waitForElement(locate({css: 'div[name="freshAccountsListRow"]'}).withText(newUserEmail));
    I.click(locate({css: 'button[name="sendInvite"]'}).inside(locate({css: 'div[name="freshAccountsListRow"]'}).withText(newUserEmail)));
    //name="freshAccountsListRow"
*/

    // I.click(locate('.toggleAccess').inside(locate('div.innerItemRow').withText('customer3@mypelz.de')));

});


