/// <reference path="../steps.d.ts" />

const assert = require('assert');
const cookieConsent = require('./helper/cookieConsent');

Feature('Presession test');

Scenario('go to presession check @smoke @constraints', async ({ I }) => {
    I.amOnPage('/');

    // dismiss cookie consent
    cookieConsent(I);

    // I navigate to presession test
    within({css: 'div#footer'},() => {
        I.click({css: 'button[name="preflight"]'});  
    });

    I.see('Pre session check', 'h2');
    I.waitForInvisible('.overlay', 6);

    // ### video settings ###

    // we toggle mirror mode
    I.seeElement(locate({css: '.mat-slide-toggle-content'}).withText('Video mirrored OFF').inside(locate({css: 'mat-slide-toggle'})));
    I.click(locate({css: '.mat-slide-toggle-content'}).withText('Video mirrored OFF').inside(locate({css: 'mat-slide-toggle'})));
    I.waitForInvisible('.overlay', 6);
    I.waitForElement(locate({css: '.mat-slide-toggle-content'}).withText('Video mirrored ON').inside(locate({css: 'mat-slide-toggle'})), 6);

    // we see preference
    I.seeElement(locate({css: '.mat-radio-label-content'})
        .withText('640x480px - 4/3')
        .inside(locate({css: 'mat-radio-button.mat-radio-checked'})));

    // we change to 1280...
    I.click(locate({css: '.mat-radio-label-content'})
        .withText('1280x720px - 16/9'));
    I.waitForInvisible('.overlay', 6);

    I.waitForElement(locate({css: '.mat-radio-label-content'})
        .withText('1280x720px - 16/9')
        .inside(locate({css: 'mat-radio-button.mat-radio-checked'})), 6);

    // headphone mode should be preselected
    I.seeElement(locate({css: '.mat-radio-label-content'})
        .withText('Headphone mode')
        .inside(locate({css: 'mat-radio-button.mat-radio-checked'})));

    I.wait(1)
    // change to Speaker mode
    I.click(locate({css: '.mat-radio-label-content'}).withText('Speaker mode'));
    I.scrollPageToBottom();
    I.waitForInvisible('.overlay', 6);
    I.seeElement(locate({css: '.mat-radio-label-content'}).withText('Speaker mode').inside(locate({css: 'mat-radio-button.mat-radio-checked'})));
    // echo cancelation must now be checked, but the rest not
    I.seeElement({ css: '#echoCancellation mat-pseudo-checkbox.mat-pseudo-checkbox-checked'});
    I.dontSee({ css: '#noiseSuppression mat-pseudo-checkbox.mat-pseudo-checkbox-checked'});
    I.dontSee({ css: '#autoGainControl mat-pseudo-checkbox.mat-pseudo-checkbox-checked'});

    // now I override these presets
    const nSstate = (await I.grabAttributeFrom('#noiseSuppression mat-pseudo-checkbox', 'class')).indexOf('mat-pseudo-checkbox-checked') !== -1;
    const aGstate = (await I.grabAttributeFrom('#autoGainControl mat-pseudo-checkbox', 'class')).indexOf('mat-pseudo-checkbox-checked') !== -1;
    // console.log('aGstate: ', nSstate);

    if(!nSstate) {
        I.click(locate({css: '#noiseSuppression'}));
        I.waitForInvisible('.overlay', 6);
    }
    if(!aGstate) {
        I.click(locate({css: '#autoGainControl'}));
        I.waitForInvisible('.overlay', 6);
    }

    // we check if this was stored properly in localStorage
    I.usePuppeteerTo('get local storage', async ({ page, browser }) => {
        const localStorageRaw = await page.evaluate(() => localStorage.getItem("publisherPrefs"));
        const localStorageData = JSON.parse(localStorageRaw);
        console.log('localStorageData: ', localStorageData);
        assert.strictEqual(localStorageData.echoCancellation, true);
        assert.strictEqual(localStorageData.noiseSuppression, true);
        assert.strictEqual(localStorageData.autoGainControl, true);
    });

    // Change selection of audio inputs
    // cssContainingText('mat-radio-button', 'Fake Audio Input 2'));
    I.click(locate({css: '.mat-radio-button'}).withText('Fake Audio Input 2'));
    I.waitForInvisible('.overlay', 6);

    // Bitrate should be set per default to 64000
    I.waitForElement(locate({ css: '.mat-select-value'}).withText('64000').inside(locate('mat-select')));
    I.click(locate({ css: '.mat-select-value'}).withText('64000').inside(locate('mat-select')));
    I.click(locate({css: '.mat-option-text'}).withText('192000'));
    I.waitForElement(locate({ css: '.mat-select-value'}).withText('192000').inside(locate('mat-select')));
    I.waitForInvisible('.overlay', 6);

    // click to save setting
    // I.click({css: 'a[name="save-settings"]'});
    // I.waitForInvisible('.overlay', 6);

    // I navigate to presession test
    within({css: 'div#footer'},() => {
        I.click({css: 'button[name="preflight"]'});  
    });
    I.waitForInvisible('.overlay', 6);

    // now all checked
    I.seeElement({ css: '#echoCancellation mat-pseudo-checkbox.mat-pseudo-checkbox-checked'});
    I.seeElement({ css: '#noiseSuppression mat-pseudo-checkbox.mat-pseudo-checkbox-checked'});
    I.seeElement({ css: '#autoGainControl mat-pseudo-checkbox.mat-pseudo-checkbox-checked'});

    // read localStorage...

});


