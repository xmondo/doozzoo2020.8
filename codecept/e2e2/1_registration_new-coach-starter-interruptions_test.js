/// <reference path="../steps.d.ts" />

const config = require('codeceptjs').config.get();
const key = config.key;

/**
 * this should set up rooms in now
 * prerequisite is, that these rooms are not existing
 * 1. make sure room is not already there
 * 2. create room
 */

const MailListener = require('mail-listener2');
const assert = require('assert');

// here goes your email connection configuration
const mailListener = new MailListener({
    username: 'pelz@mypelz.de',
    password: 'SIEBEN#meilen?stiefel',
    host: 'imap.1und1.de',
    port: 993, // imap port
    tls: true,
    tlsOptions: { rejectUnauthorized: false },
    mailbox: 'Inbox', // mailbox to monitor
    searchFilter: ['UNSEEN'], // ["UNSEEN", "FLAGGED"], // the search filter being used after an IDLE notification has been retrieved
    markSeen: true, // all fetched email willbe marked as seen and not fetched next time
    fetchUnreadOnStart: true, // use it only if you want to get all unread email on lib start. Default is `false`,
    mailParserOptions: { streamAttachments: true }, // options to be passed to mailParser lib.
    attachments: false, // download attachments as they are encountered to the project directory
    attachmentOptions: { directory: 'attachments/' } // specify a download directory for attachments
});

mailListener.start();

mailListener.on('error', function (err) {
    console.log('mailListener error: ', err);
});

mailListener.on('server:connected', function () {
    console.log('Mail listener initialized');
});

mailListener.on('mail', function (mail, seqno, attributes) {
    // do something with mail object including attachments
    // console.log("emailParsed", mail);
    // mail processing code goes here
});

// helper
function getLastEmail() {
    // console.log('now waiting for an email...');
    return new Promise(resolve => {
        mailListener.on('mail', function (mail) {
            resolve(mail);
        });
    });
}

Feature('new coach registration starter plan interruptions');

Scenario('register as coach2 and select starter plan interruptions @registration', async ({ I }) => {
    //...

    I.amOnPage(`/login?mode=logindelete&u=${config.coach2user}&p=${encodeURIComponent(config.coach2pw)}`);
    I.wait(2);
    I.amOnPage('/');

    // dismiss cookie consent
    I.waitForElement({ css: '.cookie-consent-dialog-container'}, 6);
    I.click({ css: 'button[name="accept"]'});
    I.waitForInvisible({ css: '.cookie-consent-dialog-container'}, 6);

    // click register on home
    // I.click(locate({ css: 'div.mat-tab-label' }).withText('Register'));
    I.click({css: 'button[name="register"]'});
    I.wait(1);

    I.fillField('input[name="email"]', config.coach2user);
    I.fillField('input[name="emailRepeated"]', config.coach2user);
    I.fillField('input[name="password"]', config.coach2pw);
    I.fillField('input[name="passwordRepeated"]', config.coach2pw);
    I.click({css: 'button[name="registerSubmit"]'});
    I.wait(2);
    
    // wait for email and fetch its content
    const email = await getLastEmail();
    // console.log('email content: ', email.text);

    const from = 'support@doozzoo.com';
    assert.strictEqual(email.subject, 'Verify your email for doozzoo');
    assert.strictEqual(email.headers.from, from);
    assert.strictEqual(email.headers.to, config.coach2user);

    // extract registration code from the email message
    const pattern = /(\boobCode=\b[a-zA-Z0-9&=\-_]*)/g;
    const regCode = pattern.exec(email.text);
    // console.log('extracted link: ', pattern, regCode, regCode[0]);

    // expose link
    registerUrl = config.url + '/login?mode=verifyEmail&' + regCode[0];

    mailListener.stop();

    // ### email double opt in ####
    // pause()

    // click registration link
    I.amOnPage(registerUrl);

    I.waitForText('Your account has been verified', 6);

});
// we interrupt here to see, that after login the process will continue here
// we continue
Scenario('coach2 loggs in again and continue registration @registration', async ({ I }) => {

    I.amOnPage('/');

    // dismiss cookie consent
    I.waitForElement({ css: '.cookie-consent-dialog-container'}, 6);
    I.click({ css: 'button[name="accept"]'});
    I.waitForInvisible({ css: '.cookie-consent-dialog-container'}, 6);

    // click passwordForgotten on home
    I.click({css: 'button[name="login"]'});
    // I should see dialog poping up
    I.waitForVisible({ css: '.mat-dialog-container'}, 6);

    // ### Login form ###
    // I enter my email, that I used for registration
    I.fillField('email', config.coach2user);
    I.fillField('password', config.coach2pw);

    I.waitForClickable({ css: 'button[name="loginSubmit"]'}, 6);
    I.click({ css: 'button[name="loginSubmit"]'});

    I.waitForText('Your account has been verified', 6);

    // click verification continue
    I.waitForClickable({ css: 'button[name="emailVerifiedNext"]' }, 6);
    I.click({css: 'button[name="emailVerifiedNext"]'});

    // ###  Profile form ###

    // wait for Form
    I.waitForVisible({css: 'div[name="profileForm"]'}, 6);

    // select Profile form for coach role
    I.click({css: 'mat-radio-button[value="coach"]'});

    // select avatar
    I.waitForClickable({css: '.avatarIcon'}, 5);
    I.click({css: '.avatarIcon'});

    // select a random avatar image
    const rNum = Math.floor(Math.random() * 20);
    const imgSelector = `img[src="assets/img/avatars/avataaars_${rNum}.svg"]`;
    I.click({css: imgSelector});

    // fill in profile form
    I.fillField('input[formControlName="firstname"]', 'coach2 first name');
    I.fillField('input[formControlName="lastname"]', 'coach2 lastname');
    I.fillField('input[formControlName="nickname"]', 'coach2nick');

    I.fillField('input[formControlName="address_street1"]', 'Baker Street');
    I.fillField('input[formControlName="zipcode"]', '456789');
    I.fillField('input[formControlName="city"]', 'Antwerp');

    I.fillField('input[formControlName="birthday"]', '23.04.1956');
    I.fillField('input[formControlName="phone"]', '+49 241 888999');

    // option list: native codecept does not work as this are fake material controls
    I.click({ css: 'mat-select[formControlName="country"]' });
    // I.click({ css: '.mat-option-text'});
    I.click(locate({ css: '.mat-option-text'}).withText('Cyprus'));

    I.click('mat-checkbox[formControlName="dataApproval"]');
    //I.checkOption({css: 'mat-checkbox[formControlName="dataApproval"]'});

    I.scrollPageToBottom();

    I.waitForClickable({ css: 'button[name="profileFormCoachSubmit"]' }, 16);
    I.click({ css: 'button[name="profileFormCoachSubmit"]' });

});
// we interrupt here to see, that after login the process will continue here
// we continue
Scenario('coach2 loggs in again and continue 2nd time @registration', async ({ I }) => {

    I.amOnPage('/');

    // dismiss cookie consent
    I.waitForElement({ css: '.cookie-consent-dialog-container'}, 6);
    I.click({ css: 'button[name="accept"]'});
    I.waitForInvisible({ css: '.cookie-consent-dialog-container'}, 6);

    // click passwordForgotten on home
    I.click({css: 'button[name="login"]'});
    // I should see dialog poping up
    I.waitForVisible({ css: '.mat-dialog-container'}, 6);

    // ### Login form ###
    // I enter my email, that I used for registration
    I.fillField('email', config.coach2user);
    I.fillField('password', config.coach2pw);

    I.waitForClickable({ css: 'button[name="loginSubmit"]'}, 6);
    I.click({ css: 'button[name="loginSubmit"]'});

    I.waitForText('Your account has been verified', 6);

    // click verification continue
    I.waitForClickable({ css: 'button[name="emailVerifiedNext"]' }, 6);
    I.click({css: 'button[name="emailVerifiedNext"]'});

    // ###  Profile form ###

    // wait for Form
    I.waitForVisible({css: 'div[name="profileForm"]'}, 6);

    // proceed
    I.click({css: 'button[name="profileVerifiedNext"]'});

    // ###  Select plan ###

    // select "starter"
    I.click({ css: 'button[name="selectPlanStarter"]' });

    I.waitForText('You selected plan Workshop Room Starter', 6);
    I.scrollPageToBottom();

    I.click({ css: 'button[name="confirmPlanStarter"]' });

    I.waitForText('Your Workshop Room Starter has been set up', 6);


    I.click({ css: 'button[name="selectPlanNext"]' });

    I.waitForText('Hi coach2nick, you sucessfully applied as "Coach" at doozzoo, have fun!', 6);

});
