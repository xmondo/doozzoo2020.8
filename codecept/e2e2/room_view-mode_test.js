/// <reference path="../steps.d.ts" />

const assert = require('assert');
const config = require('codeceptjs').config.get();
const cookieConsent = require('./helper/cookieConsent');
const key = config.key;

const coachId = config.coach1Id; // prod -> auth0|582c7e4cedb8c73048379aad
const userId = config.user2Id; // prod -> auth0|582c7595edb8c7304837994a

Feature('room viewmode');
    //.timeout(2000); // set timeout to 5s;

/**
 * preconditions:
 * coach1@mypelz.de exists and is registered as coach
 * 
 * 1. ...
 */

Scenario('prepare @smoke @room', (async ({ I }) => { // or Background
    I.haveRequestHeaders({'Content-Type':'application/x-www-form-urlencoded'});
    // email=coach1%40mypelz.de&password=%23coach1&returnSecureToken=true
    const response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${key}`,`email=${encodeURIComponent(config.coach1user)}&password=${encodeURIComponent(config.coach1pw)}&returnSecureToken=true`);

    I.sendPatchRequest(`/classrooms/${coachId}.json?auth=${response.data.idToken}`, { locked: false, roomId: null });
    I.sendPatchRequest(`/classrooms/${coachId}/participants/${userId}.json?auth=${response.data.idToken}`, { access: true });

}));

Scenario('log in as coach1 and start session @smoke @room', async ({ I }) => {

    I.amOnPage(`/directlogin?u=${config.coach1user}&p=${encodeURIComponent(config.coach1pw)}`);
    I.scrollPageToBottom();

    cookieConsent(I);

    // wait for tango button
    I.waitForClickable({ css: 'button[name="tangoButton"]'}, 6);
    I.click({ css: 'button[name="tangoButton"]'});

    I.waitInUrl('/room/', 6);

    // here we go, we are in the session room
    I.waitForElement({ css: 'button[name="toggleViewMode"]'}, 16);
    I.seeElement(locate({ css: 'button[name="toggleViewMode"]'}).withAttr({ disabled: 'true' }));
    I.seeElement(locate({ css: 'button[name="toggleViewMode"]'}).withAttr({ title: 'hero' }));

    // start second session
    session('user2', () => {
        I.amOnPage('/directlogin?u=user2@mypelz.de&p=%23user2');
        
        // dismiss cookie consent
        cookieConsent(I);
        
        // go to mycoaches
        I.amOnPage('/mycoaches');
        I.waitInUrl('/mycoaches');
        I.waitForElement({ css: '.buttonGoToClassroom'}, 6);
        // I.click({ css: 'button.buttonGoToClassroom'});
        I.click(locate({ css: '.buttonGoToClassroom'})
            .inside(locate({css: '.my-coaches-row'})
            .withText('coach1')));
        I.waitInUrl('/room/', 12);
        // here we go, we are in the session room
        // I.wait(1);
    });

    // wee should see the hero
    I.seeElement({ css: 'div.videoThumbnail.hero'});
    // wee should see the thumbnails
    I.waitForElement({ css: 'div.videoThumbnail.items'}, 16);
    
    // toggle view button is now enabled
    I.waitForElement({ css: 'button[name="toggleViewMode"]'}, 6);
    I.seeElement(locate({ css: 'button[name="toggleViewMode"]'}).withAttr({ title: 'hero' }));
    // Problem???
    // I.seeElement(locate({ css: 'button[name="toggleViewMode"]'}).withAttr({ 'ng-reflect-disabled': 'false' }));

    // I click to toggle
    I.click({ css: 'button[name="toggleViewMode"]'});
    I.waitForElement(locate({ css: 'button[name="toggleViewMode"]'}).withAttr({ title: 'dual' }), 6);
    I.seeElement(locate({ css: 'button[name="toggleViewMode"]'}).withAttr({ title: 'dual' }));

    // wee should see dual one / two
    I.seeElement({ css: 'div.videoThumbnail.dual.one'});
    I.seeElement({ css: 'div.videoThumbnail.dual.two'});

    // start 3rd session
    session('user2.2', () => {
        I.amOnPage('/directlogin?u=user2@mypelz.de&p=%23user2');
        
        // dismiss cookie consent
        cookieConsent(I);
        
        // go to mycoaches
        I.amOnPage('/mycoaches');
        I.waitInUrl('/mycoaches', 6);
        I.waitForElement({ css: '.buttonGoToClassroom'}, 6);
        // I.click({ css: 'button.buttonGoToClassroom'});
        I.click(locate({ css: '.buttonGoToClassroom'})
            .inside(locate({css: '.my-coaches-row'})
            .withText('coach1')));
        I.waitInUrl('/room/', 6);
        // here we go, we are in the session room
        // I.wait(1);
    });

    // start 4th session
    session('user2.3', () => {
        I.amOnPage('/directlogin?u=user2@mypelz.de&p=%23user2');
        
        // dismiss cookie consent
        cookieConsent(I);
        
        // go to mycoaches
        I.amOnPage('/mycoaches');
        I.waitInUrl('/mycoaches', 6);
        I.waitForElement({ css: '.buttonGoToClassroom'}, 6);
        // I.click({ css: 'button.buttonGoToClassroom'});
        I.click(locate({ css: '.buttonGoToClassroom'})
            .inside(locate({css: '.my-coaches-row'})
            .withText('coach1')));
        I.waitInUrl('/room/', 6);
        // here we go, we are in the session room
        // I.wait(1);
    });

    // start 5th session
    session('user2.4', () => {
        I.amOnPage('/directlogin?u=user2@mypelz.de&p=%23user2');
        
        // dismiss cookie consent
        cookieConsent(I);
        
        // go to mycoaches
        I.amOnPage('/mycoaches');
        I.waitInUrl('/mycoaches', 6);
        I.waitForElement({ css: '.buttonGoToClassroom'}, 6);
        // I.click({ css: 'button.buttonGoToClassroom'});
        I.click(locate({ css: '.buttonGoToClassroom'})
            .inside(locate({css: '.my-coaches-row'})
            .withText('coach1')));
        I.waitInUrl('/room/', 6);
        // here we go, we are in the session room
        // I.wait(1);
    });

    I.scrollPageToBottom();

    I.wait(6);

    I.seeElement(locate({ css: 'button[name="toggleViewMode"]'}).withAttr({ title: 'dual' }));
    // I click to toggle
    // I.click({ css: 'button[name="toggleViewMode"]'});
    I.click(locate({ css: 'button[name="toggleViewMode"]'}).withAttr({ title: 'dual' }));
    I.waitForElement(locate({ css: 'button[name="toggleViewMode"]'}).withAttr({ title: 'multi' }),16);

    // wee should see dual one / two
    I.seeElement({ css: 'div.videoThumbnail.multi.one'});
    I.seeElement({ css: 'div.videoThumbnail.multi.two'});
    I.seeElement({ css: 'div.videoThumbnail.multi.three'});
    I.seeElement({ css: 'div.videoThumbnail.multi.four'});

    I.click({ css: 'button[name="toggleViewMode"]'});
    I.seeElement(locate({ css: 'button[name="toggleViewMode"]'}).withAttr({ title: 'galery' }));

    I.wait(2);

    let countVideoThumbnails = await I.grabNumberOfVisibleElements({ css: 'div.videoThumbnail.galery.items'});

    assert.strictEqual(countVideoThumbnails, 5);

});






