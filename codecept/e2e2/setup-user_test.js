/// <reference path="../steps.d.ts" />

const config = require('codeceptjs').config.get();
const key = config.key;

Feature('my-students EDU');

// const studentEmail = 'user2@mypelz.de';
const userEmail = 'customer5@mypelz.de';
const userPassword = '#customer5';
const studentEmail = 'customer1@mypelz.de';
const studentPassword = '#customer1';
// customer1
const coachId = 'gMA1e2RYJFWLmWXYpNaT0QpRkHu1';
const institutionName = 'Kreismusikschule Heinsberg';


/**
 * Objective: 
 * set up user for standard testing
 * 
 * customer1 -> gMA1e2RYJFWLmWXYpNaT0QpRkHu1 // institutional coach
 * customer3 -> 1GaYBJtvGNccp9umENFbYpqn2o52 // institutional student
 * customer5 -> ViQO10o8Kaa7Ed4E8c8DYCyjGb33 // admin
 * 
 * user1 -> fixed id student user
 * -> WmYDCkyM3kcTZpRj9ZX63xgy7kx2
 * user2 -> temporary student user // for testing of registration with new userIds
 * OFHEb2PkOOY51gLObPrlsLWlAmS2
 * user3 -> fixed id student user
 * uNzOprX2cHhAITb0HEJnMjXX7rB3
 * user4 -> fixed id student user
 * meq4dQi9Y7gxufXKd1sqwgOCXwt1
 * user5 -> temporary user
 * null
 * 
 * coach1 -> fixed id coach user
 * 
 * stripecoach2 -> fixed id coach user for stripe account testing
 * 
 * stripecoach2 -> 2X1ZM44k8lXX6t2w1X17fx76DPK2
 * user1 -> WmYDCkyM3kcTZpRj9ZX63xgy7kx2
 * user2 -> OFHEb2PkOOY51gLObPrlsLWlAmS2
 * user3 -> uNzOprX2cHhAITb0HEJnMjXX7rB3
 * user4 -> meq4dQi9Y7gxufXKd1sqwgOCXwt1
 * user5 -> null
 * 
 * Test-Secenarios
 * 1. log in as admin and clear studentlist of customer1
 * 2. check in customer1 mystudents if no students are visible
 * 3. admin add 2 students for customer1
 * 4. check if those are visible in my students
 * 5. check if those are visible in the session participant list as well
 */

Scenario('clear participant list of customer1 @edu @my-students', async ({ I }) => {
    I.haveRequestHeaders({'Content-Type':'application/x-www-form-urlencoded'});
    const response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${key}`,"email=admin1%40mypelz.de&password=%23admin1&returnSecureToken=true");
    // auth response
    // console.log('response: ', response);

    await I.sendPatchRequest(`/classrooms/${coachId}.json?auth=${response.data.idToken}`, { locked: false });
    await I.sendPatchRequest(`/classrooms/${coachId}.json?auth=${response.data.idToken}`, { participants: null });
    await I.sendPatchRequest(`/coachCustomer.json?auth=${response.data.idToken}`, { [coachId]: null });
});

