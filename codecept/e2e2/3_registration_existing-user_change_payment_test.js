/// <reference path="../steps.d.ts" />

let config = require('codeceptjs').config.get();
const assert = require('assert');
const cookieConsent = require('./helper/cookieConsent');

Feature('stripe existing user change payment option flow');

// firebase API keys
const devKey = 'AIzaSyBHJt1JFPRSuue0ZkbRNEy7oZcpN7KBAqA';
const prodKey= 'AIzaSyAklBOk3aEvjfR0KDOK2Ju5GQnlsUCUlnA';
const key = process.env.profile === 'prod' ? prodKey : devKey;

Scenario('log in as coach2, go to profile and change payment type @registration', async ({ I }) => {

    I.amOnPage('/directlogin?u=coach2@mypelz.de&p=%23coach2');

    cookieConsent(I)
    I.wait(5)

    I.amOnPage('/login');

    // start change payment type
    I.waitForVisible({ css: 'button[name="changePaymentType"]' }, 6)
    // I.seeElement({ css: 'button[name="changePaymentType"]' }) // button when archiving was already booked
    I.click({ css: 'button[name="changePaymentType"]' });

    I.scrollPageToBottom();

    // ccard
    I.seeElement({ css: 'mat-radio-button[value="card"]' });
    I.seeElement({ css: 'app-creditcard' });

    // switch to sepa...
    I.click({ css: 'mat-radio-button[value="sepa"]' });
    I.seeElement({ css: 'app-sepa' });

    I.fillField({ css: 'input[name="accountholderName"]'}, 'Christoph Pelz'); 

    /**
     * IMPORTANT: for this to be able accessing the iFrame content the following pupetteer config flags must be set:
     * '--disable-web-security',
     * '--disable-features=IsolateOrigins,site-per-process'
     */

    I.seeElement({ css: 'iframe[title="Secure IBAN input frame"]'})

    within({frame: '[title="Secure IBAN input frame"]'}, async () => {  
        // const iban = 'DE89370400440532013000';
        // as the value is transformed with spaces, this helps to avoid errors
        const iban = config.stripeIban; //'DE89 3704 0044 0532 0130 00';
        I.clearField({ css: 'input[name="iban"]'});
        I.fillField({ css: 'input[name="iban"]'}, iban); 
        const _iban = await I.grabValueFrom({ css: 'input[name="iban"]'});
        if(_iban !== iban) {
            console.log('iban is incorrect: ', _iban)
            I.clearField({ css: 'input[name="iban"]'});
            I.fillField({ css: 'input[name="iban"]'}, iban); 
        }
    });

    I.click({ css: 'button[name="sepaSubmit"]' });

    I.wait(1);

    // check if payment method has been changed...
    I.waitForClickable({ css: 'button[name="changePaymentType"]' }, 4)
    I.click({ css: 'button[name="changePaymentType"]' });
    I.scrollPageToBottom();

    // div.paymentBlock
    I.waitForText('SEPA Debit', 5);
    I.see('Account holder: Christoph Pelz');

    I.wait(1);

    // now changing to cc
    I.click({ css: 'mat-radio-button[value="card"]' });
    I.seeElement({ css: 'app-creditcard' });

    within({frame: '[title="Secure card payment input frame"]'}, () => {  
        const cardNr = config.stripeCard; // '5555 5555 5555 4444';
        I.seeElement({ css: 'input[name="cardnumber"]'});

        I.clearField({ css: 'input[name="cardnumber"]'});
        I.fillField({ css: 'input[name="cardnumber"]'}, cardNr);
        //I.wait(1);
        I.fillField({ css: 'input[name="exp-date"]'}, '01 / 23');
        //I.wait(1);
        I.fillField({ css: 'input[name="cvc"]'}, '993');  
        // only on dev/localhost with artificial cardNr, zip is necessary

        config.profile === 'dev' || config.profile === 'default' ? I.fillField({ css: 'input[name="postal"]'}, '52074') : '';  

    });

    I.click({ css: 'button[name="cardSubmit"]' });

    I.wait(2);

    // check if payment method has been changed...
    I.waitForClickable({ css: 'button[name="changePaymentType"]' }, 4)
    I.click({ css: 'button[name="changePaymentType"]' });
    I.scrollPageToBottom();

    // div.paymentBlock
    I.waitForText('Your credit card', 10);

});




