const assert = require('assert');

const resetPassword = async function resetPassword(I, user, pw, newPw, apiKey) { 
    // console.log('...', user, pw);
    _response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${apiKey}`,`email=${user}&password=${pw}&returnSecureToken=true`);
    I.wait(1);
    // console.log('signin user: ', _response.data);
    const idToken = _response.data.idToken;
    if(idToken) {
        //console.log('signin userID: ', _response.data.localId);
        _response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:update?key=${apiKey}`,`idToken=${idToken}&password=${newPw}`);
        I.wait(1);
        // console.log('reset: ', _response.data);
        // kind: 'identitytoolkit#DeleteAccountResponse'
        assert.notStrictEqual(_response.data.idToken, undefined);
        return 'user password reset';
    } else {
        //console.log('user does not exist');
        return 'user does not exist';
    }
}
module.exports = resetPassword;