
const deleteExistingUser = async function deleteExistingUser(I, user, pw, apiKey) { 
    // console.log('...', user, pw);
    _response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${apiKey}`,`email=${user}&password=${pw}&returnSecureToken=true`);
    I.wait(1);
    console.log('signin user: ', _response.data);
    const idToken = _response.data.idToken;
    if(idToken) {
        //console.log('signin userID: ', _response.data.localId);
        _response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:delete?key=${apiKey}`,`idToken=${idToken}`);
        I.wait(1);
        //console.log('delete: ', _response.data);
        // kind: 'identitytoolkit#DeleteAccountResponse'
        assert.strictEqual(_response.data.kind, 'identitytoolkit#DeleteAccountResponse');
        return 'user deleted';
    } else {
        //console.log('user does not exist');
        return 'user does not exist';
    }
}
module.exports = deleteExistingUser;