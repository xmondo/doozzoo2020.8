
/**
 * @param {*} I 
 * @param {*} user 
 * @param {*} pw 
 * @param {*} variant -> form | direct
 */
const login = async function login(I, user, pw, variant) { 
    variant = variant ?? 'direct';
    if(variant === 'form') {
        I.amOnPage('/');
        // wait
        I.waitForElement({ css: '.cookie-consent-dialog-container'}, 6);
        I.click({ css: 'button[name="accept"]'});
        I.waitForInvisible({ css: '.cookie-consent-dialog-container'}, 6);

        I.click({css: 'button.footerLogin'});
        I.fillField('E-Mail', user);
        I.fillField('Password', pw);
        I.click({css: 'button[name="loginSubmit"]'});
    } else if (variant === 'direct') {
        I.amOnPage(`/directlogin?u=${user}&p=${encodeURIComponent(pw)}`);
        I.amOnPage('/');
        // dismiss cookie consent
        I.waitForElement({ css: '.cookie-consent-dialog-container'}, 6);
        I.click({ css: 'button[name="accept"]'});
        I.waitForInvisible({ css: '.cookie-consent-dialog-container'}, 6);
    }
}
module.exports = login;