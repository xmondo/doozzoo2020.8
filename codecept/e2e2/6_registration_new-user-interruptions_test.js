/// <reference path="../steps.d.ts" />

let config = require('codeceptjs').config.get();

/**
 * 
 */

const MailListener = require('mail-listener2');
const assert = require('assert');

// here goes your email connection configuration
const mailListener = new MailListener({
    username: 'pelz@mypelz.de',
    password: 'SIEBEN#meilen?stiefel',
    host: 'imap.1und1.de',
    port: 993, // imap port
    tls: true,
    tlsOptions: { rejectUnauthorized: false },
    mailbox: 'Inbox', // mailbox to monitor
    searchFilter: ['UNSEEN'], // ["UNSEEN", "FLAGGED"], // the search filter being used after an IDLE notification has been retrieved
    markSeen: true, // all fetched email willbe marked as seen and not fetched next time
    fetchUnreadOnStart: true, // use it only if you want to get all unread email on lib start. Default is `false`,
    mailParserOptions: { streamAttachments: true }, // options to be passed to mailParser lib.
    attachments: false, // download attachments as they are encountered to the project directory
    attachmentOptions: { directory: 'attachments/' } // specify a download directory for attachments
});

// mailListener.start();

mailListener.on('error', function (err) {
    console.log('mailListener error: ', err);
});

mailListener.on('server:connected', function () {
    console.log('Mail listener initialized');
});

mailListener.on('mail', function (mail, seqno, attributes) {
    // do something with mail object including attachments
    // console.log("emailParsed", mail);
    // mail processing code goes here
});

// helper
function getLastEmail() {
    // console.log('now waiting for an email...');
    return new Promise(resolve => {
        mailListener.on('mail', function (mail) {
            resolve(mail);
        });
    });
}


const user = 'user5@mypelz.de';
const password = '#user5';
const passwordUrl = '%23user5';

const cookieConsent = require('./helper/cookieConsent');

Feature('new user registration interrupted');

Scenario('register as user5 @registration @login', async ({ I }) => {
    //...
    //console.log('env prod: ', key);

    // 'should delete user5'
    I.amOnPage(`/login?mode=logindelete&u=${user}&p=${passwordUrl}`);
    I.wait(2);
    I.amOnPage('/');

    cookieConsent(I)

    // click register on home
    I.click({css: 'button[name="register"]'});
    I.wait(2);

    I.fillField('input[name="email"]', user);
    I.fillField('input[name="emailRepeated"]', user);
    I.fillField('input[name="password"]', password);
    I.fillField('input[name="passwordRepeated"]', password);
    I.click({css: 'button[name="registerSubmit"]'});

});


Scenario('try login as user5 after interruption @registration @login', async ({ I }) => {

    I.amOnPage('/');

    cookieConsent(I)

    // click login on home
    I.click({css: 'button[name="login"]'});
    I.fillField('E-Mail', user);
    I.fillField('Password', password);
    I.click({css: 'button[name="loginSubmit"]'});

    // pause()

    I.wait(2);
    // should be redirected to verify within the login popup
    I.see('To complete your registration');

    // fill in the form and submit
    I.fillField('input[name="email"]', user);
    I.fillField('input[name="emailRepeated"]', user);
    
    I.wait(2);
    
    I.click({css: 'button[name="verifySubmit"]'});
    I.wait(1);
    
    // ??? this throws an error in test...
    // 
    I.waitForText('Too many requests.', 6);
    I.wait(1);
    
    // pause();
    // close the dialog
    I.click({css: 'mat-icon[name="loginDialogCloseButton"]'});
    I.waitForInvisible({ css: '.cdk-overlay-backdrop' })

    

// ### 

    // restart login
    I.amOnPage('/');

    // click login on home
    I.click({css: 'button[name="login"]'});
    I.fillField('E-Mail', user);
    I.fillField('Password', password);
    I.click({css: 'button[name="loginSubmit"]'});

    I.wait(2);
    // should be redirected to verify within the login popup
    I.see('To complete your registration');

    // fill in the form and submit
    I.fillField('input[name="email"]', user);
    I.fillField('input[name="emailRepeated"]', user);

    // pause();

    I.wait(61) // 61 sec works well

    // continue manually
    // pause();

    I.click({css: 'button[name="verifySubmit"]'});
    I.waitForText('We sent you an e-mail.', 80);
    
    console.log('if we get an success message (green box), everything is fine...')
    
    /*
    I.wait(20);
    // only now invoke mail listener
    mailListener.start();
    // wait for email and fetch its content
    const email = await getLastEmail();
    // console.log('email content: ', email.text);

    const from = 'support@doozzoo.com';
    assert.strictEqual(email.subject, 'Verify your email for doozzoo');
    // assert.strictEqual(email.headers.from, from); // throws error
    assert.strictEqual(email.headers.to, user);

    // extract registration code from the email message
    const pattern = /(\boobCode=\b[a-zA-Z0-9&=\-_]*)/g;
    const regCode = pattern.exec(email.text);
    // console.log('extracted link: ', pattern, regCode, regCode[0]);

    // expose link
    registerUrl = config.url + '/login?mode=verifyEmail&' + regCode[0];
    // console.log('registerUrl: ', registerUrl);
    mailListener.stop();

    // click registration link
    I.amOnPage(registerUrl);
    I.wait(2)
    // I.amOnPage(registerUrl);

    I.waitForText('Your account has been verified', 8);
    // I.see('Your account has been verified');

    */

});
