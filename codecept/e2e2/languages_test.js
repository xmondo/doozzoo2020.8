/// <reference path="../steps.d.ts" />

const cookieConsent = require('./helper/cookieConsent');

Feature('languages_switcher');

Scenario('I go to home and switch languages @smoke', async ({ I }) => {
    //

    I.amOnPage('/');

    cookieConsent(I)
    
    // default welcome phrase EN
    I.see('To start or join a session');

    // I use language switcher
    I.seeElement({css: 'button[name="languageMenu"]'});

    I.scrollPageToBottom();

    I.click({css: 'button[name="languageMenu"]'});

    I.waitForClickable({css: 'button[role="menuitem"]'}, 3);

    // DE
    within({css: '.mat-menu-content'},() => {
        I.click('Deutsch'); // headline
    });
    I.see('Um eine Session zu starten');

    // NL
    I.click({css: 'button[name="languageMenu"]'});
    within({css: '.mat-menu-content'},() => {
        I.click('Nederlands'); // headline
    });
    I.see('Een sessie starten of er aan deelnemen');

    // IT
    I.click({css: 'button[name="languageMenu"]'});
    within({css: '.mat-menu-content'},() => {
        I.click('Italiano'); // headline
    });
    I.see('Per iniziare o raggiungere una session');

    // FR
    I.click({css: 'button[name="languageMenu"]'});
    within({css: '.mat-menu-content'},() => {
        I.click('Français'); // headline
    });
    I.see('Pour démarrer ou rejoindre une session');

    // ES
    I.click({css: 'button[name="languageMenu"]'});
    within({css: '.mat-menu-content'},() => {
        I.click('Español'); // headline
    });
    I.see('Para comenzar o unirse a una sesión');

});
