/// <reference path="../steps.d.ts" />

const assert = require('assert');
const config = require('codeceptjs').config.get();
const cookieConsent = require('./helper/cookieConsent');
const key = config.key;

const coachId = config.coach1Id; // prod -> auth0|582c7e4cedb8c73048379aad
const userId = config.user2Id; // prod -> auth0|582c7595edb8c7304837994a

Feature('metronome bug');

Scenario('prepare metronome test', (async ({ I }) => { // or Background
    I.haveRequestHeaders({'Content-Type':'application/x-www-form-urlencoded'});
    // email=coach1%40mypelz.de&password=%23coach1&returnSecureToken=true
    const response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${key}`,`email=coach1%40mypelz.de&password=%23coach1&returnSecureToken=true`);

    I.sendPatchRequest(`/classrooms/${coachId}.json?auth=${response.data.idToken}`, { locked: false, roomId: null });
    I.sendPatchRequest(`/classrooms/${coachId}/participants/${userId}.json?auth=${response.data.idToken}`, { access: true });

}));

Scenario('log in as coach1 and start metronome test session @smoke @room', async ({ I }) => {

    I.amOnPage('/directlogin?u=coach1@mypelz.de&p=%23coach1');
    I.scrollPageToBottom();

    // cookieConsent(I);

    // wait for tango button
    I.waitForClickable({ css: 'button[name="tangoButton"]'}, 6);
    I.click({ css: 'button[name="tangoButton"]'});

    I.waitInUrl('/room/', 6);

    // here we go, we are in the session room
    I.waitForElement({ css: 'button[name="toggleViewMode"]'}, 16);
    I.seeElement(locate({ css: 'button[name="toggleViewMode"]'}).withAttr({ disabled: 'true' }));
    I.seeElement(locate({ css: 'button[name="toggleViewMode"]'}).withAttr({ title: 'hero' }));

    // start second session
    session('user2', () => {
        I.amOnPage('/directlogin?u=user2@mypelz.de&p=%23user2');
        
        // dismiss cookie consent
        // cookieConsent(I);
        
        // go to mycoaches
        I.amOnPage('/mycoaches');
        I.waitInUrl('/mycoaches');

        I.waitForElement({ css: '.buttonGoToClassroom'}, 6);
        // I.click({ css: 'button.buttonGoToClassroom'});
        I.click(locate({ css: '.buttonGoToClassroom'})
            .inside(locate({css: '.my-coaches-row'})
            .withText('coach1')));
        I.waitInUrl('/room/', 12);
        // here we go, we are in the session room
        // I.wait(1);
        // open apps @user2
        I.click({ css: 'button[name="apps"]'});
        I.click({ css: 'button[name="mic-on-off"]'});
    });

    // open apps @coach1
    I.click({ css: 'button[name="apps"]'});
    I.click({ css: 'button[name="mic-on-off"]'});
    // click metronome remote checkbox
    I.waitForElement({ css: 'button[name="remote-rings"]'}, 6);
    I.click({ css: 'button[name="remote-rings"]'});
    // start metronome on coach side
    I.waitForElement({ css: 'button[name="metronome-start"]'}, 6);
    I.click({ css: 'button[name="metronome-start"]'});

    I.wait(6);
    
    // pause()

});






