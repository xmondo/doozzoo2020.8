/// <reference path="../steps.d.ts" />

const config = require('codeceptjs').config.get();
const cookieConsent = require('./helper/cookieConsent');
const key = config.key;

Feature('my-students EDU');

// const coachEmail = 'user2@mypelz.de';
const adminEmail = 'customer5@mypelz.de';
const adminPassword = '#customer5';
const coachEmail = 'customer1@mypelz.de';
const coachPassword = '#customer1';
// customer1
const coachId = 'gMA1e2RYJFWLmWXYpNaT0QpRkHu1'; // customer1 // prod: aMp9SRBTQHTqfJiX08T9d0RDN9W2
const institutionName = 'Kreismusikschule Heinsberg';


/**
 * Objective: 
 * check whether the selection of students in edu backend 
 * is reflected in the coaches "my student" page
 * and reflected in the participants list
 * 
 * customer1 -> gMA1e2RYJFWLmWXYpNaT0QpRkHu1 // institutional coach
 * customer3 -> 1GaYBJtvGNccp9umENFbYpqn2o52 // institutional student
 * customer5 -> ViQO10o8Kaa7Ed4E8c8DYCyjGb33 // admin
 * 
 * Test-Secenarios
 * 1. log in as admin and clear studentlist of customer1
 * 2. check in customer1 mystudents if no students are visible
 * 3. admin add 2 students for customer1
 * 4. check if those are visible in my students
 * 5. check if those are visible in the session participant list as well
 */

Scenario('clear participant list of EDU customer1 @edu @my-students', async ({ I }) => {
    I.haveRequestHeaders({'Content-Type':'application/x-www-form-urlencoded'});
    const response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${key}`,"email=admin1%40mypelz.de&password=%23admin1&returnSecureToken=true");
    // auth response
    // console.log('response: ', response);

    await I.sendPatchRequest(`/classrooms/${coachId}.json?auth=${response.data.idToken}`, { locked: false });
    await I.sendPatchRequest(`/classrooms/${coachId}.json?auth=${response.data.idToken}`, { participants: null });
    await I.sendPatchRequest(`/coachCustomer.json?auth=${response.data.idToken}`, { [coachId]: null });
});

// ### 
Scenario('login into EDU as admin (customer5) and manage students of customer1 @my-students', async ({ I }) => {

    I.amOnPage('/');

    cookieConsent(I)

    I.click({css: 'button.footerLogin'});
    I.fillField('E-Mail', adminEmail);
    I.fillField('Password', adminPassword);
    I.click({css: 'button[name="loginSubmit"]'});
    // I.waitForInvisible({ css: '.cdk-overlay-backdrop'}, 6)

    I.waitForElement({ css: 'button[name="toEduBackend"]'}, 6);
    I.click({ css: 'button[name="toEduBackend"]'});
    I.waitInUrl('/edu', 6);

    I.waitForElement(locate({ css: 'button'})
        .inside(locate({ css: 'div'}).withText(institutionName)));
    I.click(locate({ css: 'button'})
        .inside(locate({ css: 'div'}).withText(institutionName)));

    // production readiness
    // I.waitInUrl('/edu/rdOJPTDs2SyYLVXolF5y', 6);
    I.waitForText(institutionName, 6)

    I.waitForElement({css: 'input[name="filterInput"]'}, 6);
    I.fillField({css: 'input[name="filterInput"]'}, coachEmail);

    // wait is urgently necessary
    I.wait(2)
    const nrOfLi = await I.grabNumberOfVisibleElements(locate({css: 'mat-chip'}).inside(locate({ css: 'div.itemRow .innerItemRow'}).withText('customer1@mypelz.de')))
    console.log(nrOfLi); 

    // admin clears student assignments
    for(let i=1; i<=nrOfLi; i++){
        I.click(locate('mat-chip:nth-child(1) mat-icon.mat-chip-remove')
            .inside(locate({ css: '[name="accountsListRow"]'})
            .withText('customer1@mypelz.de')))
        I.waitForElement(locate({css: 'button'}).withText('Remove'));
        I.click(locate({css: 'button'}).withText('Remove'));
        I.waitForInvisible({ css: 'cdk-overlay-backdrop'});
        I.wait(2) 
    } 

    I.scrollPageToBottom()

    I.seeNumberOfVisibleElements(locate({css: 'mat-chip'})
        .inside(locate({ css: 'div.itemRow .innerItemRow'})
        .withText('customer1@mypelz.de')), 0);

    // open session with "coach" customer1, he should see NO students assigned
    session('customer1', () => {
        I.amOnPage('/');

        // dismiss cookie consent
        I.waitForElement({ css: '.cookie-consent-dialog-container'}, 6);
        I.click({ css: 'button[name="accept"]'});
        I.waitForInvisible({ css: '.cookie-consent-dialog-container'}, 6);

        I.click({css: 'button.footerLogin'});
        I.fillField('E-Mail', coachEmail);
        I.fillField('Password', coachPassword);
        I.click({css: 'button[name="loginSubmit"]'});

        I.waitForElement(locate({css:'button[name="myStudents"]'}), 6);
        I.click(locate({css:'button[name="myStudents"]'}));

        I.waitInUrl('/mystudents');

        I.scrollPageToBottom();
        // student customer3 should NOT see any coach entry in his list
        I.seeNumberOfVisibleElements(locate({ css: '.accountsListRow'}).inside({ css: 'app-create-accounts'}), 0);

    });

    I.scrollPageToBottom();

    // select editing row of customer1 and fill it with two chips
    within(locate({ css: '[name="accountsListRow"]'}).withText('customer1@mypelz.de'), () => {
        I.fillField({css: 'input.mat-chip-input'}, 'cu');
    });

    I.waitForVisible({ css: 'div.mat-autocomplete-panel'}, 6);
    I.click(locate({ css: 'mat-option'}).withText('customer3'));

    within(locate({ css: '[name="accountsListRow"]'}).withText('customer1@mypelz.de'), () => {
        I.clearField({css: 'input.mat-chip-input'});
        I.fillField({css: 'input.mat-chip-input'}, 'cu');
    });

    I.waitForVisible({ css: 'div.mat-autocomplete-panel'}, 6);
    I.click(locate({ css: 'mat-option'}).withText('customer4'));

    // open session with customer3
    session('customer1', () => {
        // I should see 2 student entries
        I.waitForElement(locate({ css: '.innerItemRow'})
            .withText('customer4')
            .inside({ css: 'app-create-accounts'}), 8);
        // I.seeNumberOfVisibleElements(locate({ css: '.innerItemRow'}), 2);

        I.see('customer3@mypelz.de');
        I.see('customer4@mypelz.de');

    });

});

Scenario('coach customer1 sees the connected students customer3/4 in his studentslist @my-students', (async ({ I }) => { // or Background
    I.amOnPage('/');

    cookieConsent(I)

    I.click({css: 'button.footerLogin'});
    I.fillField('E-Mail', coachEmail);
    I.fillField('Password', coachPassword);
    I.click({css: 'button[name="loginSubmit"]'});

    I.waitForElement({ css: 'button[name="tangoButton"]'}, 6);
    I.click({ css: 'button[name="tangoButton"]'});
    I.waitInUrl('/room/', 12);

    I.waitForElement({ css: 'button[name="participantsSidenavButton"]'}, 16);
    I.click({ css: 'button[name="participantsSidenavButton"]'});

    I.waitForElement({ css: 'mat-sidenav.sidenavContentRight'});


    I.seeNumberOfElements(locate({css: 'ul.listMyStudents.notInSession>li'})
        .inside(locate({css: 'app-participant-list'})), 2);
    const nrOfLi = await I.grabNumberOfVisibleElements(locate({css: 'ul.listMyStudents.notInSession>li'})
        .inside(locate({css: 'app-participant-list'})));
    console.log(nrOfLi);

}));
