/// <reference path="../steps.d.ts" />

let config = require('codeceptjs').config.get();

/**
 * //
 */
// firebase API keys
const devKey = 'AIzaSyBHJt1JFPRSuue0ZkbRNEy7oZcpN7KBAqA';
const prodKey='AIzaSyAklBOk3aEvjfR0KDOK2Ju5GQnlsUCUlnA';
const key = process.env.profile === 'prod' ? prodKey : devKey;

const MailListener = require('mail-listener2');
const assert = require('assert');
const { helper } = require('codeceptjs');

// here goes your email connection configuration
const mailListener = new MailListener({
    username: 'pelz@mypelz.de',
    password: 'SIEBEN#meilen?stiefel',
    host: 'imap.1und1.de',
    port: 993, // imap port
    tls: true,
    tlsOptions: { rejectUnauthorized: false },
    mailbox: 'Inbox', // mailbox to monitor
    searchFilter: ['UNSEEN'], // ["UNSEEN", "FLAGGED"], // the search filter being used after an IDLE notification has been retrieved
    markSeen: true, // all fetched email willbe marked as seen and not fetched next time
    fetchUnreadOnStart: true, // use it only if you want to get all unread email on lib start. Default is `false`,
    mailParserOptions: { streamAttachments: true }, // options to be passed to mailParser lib.
    attachments: false, // download attachments as they are encountered to the project directory
    attachmentOptions: { directory: 'attachments/' } // specify a download directory for attachments
});

mailListener.start();

mailListener.on('error', function (err) {
    console.log('mailListener error: ', err);
});

mailListener.on('server:connected', function () {
    console.log('Mail listener initialized');
});

mailListener.on('mail', function (mail, seqno, attributes) {
    // do something with mail object including attachments
    // console.log("emailParsed", mail);
    // mail processing code goes here
});

// helper
function getLastEmail() {
    // console.log('now waiting for an email...');
    return new Promise(resolve => {
        mailListener.on('mail', function (mail) {
            resolve(mail);
        });
    });
}

const user = 'user5@mypelz.de';
const password = '#user5';

const cookieConsent = require('./helper/cookieConsent');

Feature('new student registration e2e');

Scenario('register as user5 @registration', async ({ I }) => {
    //...
    console.log('url: ', config.url);

    // 'should delete user5'
    I.amOnPage(`/login?mode=logindelete&u=${config.user5user}&p=${encodeURIComponent(config.user5pw)}`);
    I.wait(2);
    I.amOnPage('/');

    cookieConsent(I)

    // click register on home
    // I.click(locate({ css: 'div.mat-tab-label' }).withText('Register'));
    I.click({css: 'button[name="register"]'});
    I.wait(1);

    I.fillField('input[name="email"]', config.user5user);
    I.fillField('input[name="emailRepeated"]', config.user5user);
    I.fillField('input[name="password"]', password);
    I.fillField('input[name="passwordRepeated"]', password);
    I.click({css: 'button[name="registerSubmit"]'});
    I.wait(2);
    
    // wait for email and fetch its content
    const email = await getLastEmail();
    // console.log('email content: ', email.text);

    const from = 'support@doozzoo.com';
    assert.strictEqual(email.subject, 'Verify your email for doozzoo');
    assert.strictEqual(email.headers.from, from);
    assert.strictEqual(email.headers.to, config.user5user);

    // extract registration code from the email message
    const pattern = /(\boobCode=\b[a-zA-Z0-9&=\-_]*)/g;
    const regCode = pattern.exec(email.text);
    // console.log('extracted link: ', pattern, regCode, regCode[0]);

    // expose link
    const registerUrl = config.url + '/login?mode=verifyEmail&' + regCode[0];
    const faultyRegisterUrl = config.url + '/login?mode=verifyEmail&oobCode=balhblah';

    mailListener.stop();

    // click registration link
    I.amOnPage(faultyRegisterUrl);

    I.waitForText('The activation code is invalid', 6);

    // click registration link
    I.amOnPage(registerUrl);

    I.waitForText('Your account has been verified', 6);

    I.waitForClickable({ css: 'button[name="emailVerifiedNext"]' }, 6);
    I.click({css: 'button[name="emailVerifiedNext"]'});

    // ### now fill in profile form ###

    // wait for Form
    I.waitForVisible({css: 'div[name="profileForm"]'}, 6);

    // select avatar
    I.waitForClickable({css: '.avatarIcon'}, 5);
    I.click({css: '.avatarIcon'});

    // select a random avatar image
    const rNum = Math.floor(Math.random() * 20);
    const imgSelector = `img[src="assets/img/avatars/avataaars_${rNum}.svg"]`;
    I.click({css: imgSelector});

    // fill in profile form
    I.fillField('input[formControlName="firstname"]', 'user5 first name');
    I.fillField('input[formControlName="lastname"]', 'user5 lastname');
    I.fillField('input[formControlName="nickname"]', 'user5nick');

    I.click('mat-checkbox[formControlName="dataApproval"]');

    I.waitForClickable({ css: 'button[name="profileFormCustomerSubmit"]' }, 6);
    I.click({ css: 'button[name="profileFormCustomerSubmit"]' });

    // invited hint should disapear
    I.waitForInvisible({ css: '.successMessage.invitedStudent'}, 6);
    I.scrollPageToBottom();

    // proceed
    I.click({css: 'button[name="profileVerifiedNext"]'});

    I.see('Congrats, you are now done');

});
