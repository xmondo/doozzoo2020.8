/// <reference path="../steps.d.ts" />

const assert = require('assert');
const config = require('codeceptjs').config.get();
const cookieConsent = require('./helper/cookieConsent');
const key = config.key;
const isProd = config.profile === 'prod' ? true : false;

Feature('constraints session <> presession');

const coachId = config.coach1Id;

Scenario('login as coach, enter room and set constraints then return to presession and check for consitancy  @smoke @constraints', async ({ I }) => {

    // coach login
    I.amOnPage('/directlogin?u=coach1@mypelz.de&p=%23coach1');
    
    // cookieConsent(I);

    I.waitForElement({css:'button.startTango'}, 22);
    I.waitForClickable({css:'button.startTango'}, 6);
    I.click({css:'button.startTango'});
    I.seeInCurrentUrl('/room/');

    I.waitForElement('button[name="settings-sidenav"]', 22);
    I.waitForClickable('button[name="settings-sidenav"]', 22);
    // I.seeElement({css: 'button[name="sessionLockRoom"].sessionLockButton.unlocked' });
    I.click({ css: 'button[name="settings-sidenav"]'});

    // wait for the sidenav popping up
    I.waitForElement('mat-selection-list[name="audio-constraints"]', 22);

    // now I override these constraints
    const eCstate = (await I.grabAttributeFrom('#echoCancellation mat-pseudo-checkbox', 'class')).indexOf('mat-pseudo-checkbox-checked') !== -1;
    const nSstate = (await I.grabAttributeFrom('#noiseSuppression mat-pseudo-checkbox', 'class')).indexOf('mat-pseudo-checkbox-checked') !== -1;
    const aGstate = (await I.grabAttributeFrom('#autoGainControl mat-pseudo-checkbox', 'class')).indexOf('mat-pseudo-checkbox-checked') !== -1;
    // console.log('aGstate: ', nSstate);

    if(!eCstate) {
        I.click(locate({css: '#echoCancellation'}));
    }
    if(!nSstate) {
        I.click(locate({css: '#noiseSuppression'}));
    }
    if(!aGstate) {
        I.click(locate({css: '#autoGainControl'}));
    }

    I.wait(1);

    // we check if this was stored properly in localStorage
    I.usePuppeteerTo('get local storage', async ({ page, browser }) => {
        const localStorageRaw = await page.evaluate(() => localStorage.getItem("publisherPrefs"));
        const localStorageData = JSON.parse(localStorageRaw);
        console.log('localStorageData: ', localStorageData);
        assert.strictEqual(localStorageData.echoCancellation, true);
        assert.strictEqual(localStorageData.noiseSuppression, true);
        assert.strictEqual(localStorageData.autoGainControl, true);
    });

    // we close and go home
    I.waitForElement({ css: '#footer'}, 6);
    within('#footer', () => {
        I.waitForElement({ css: 'button[name="home"]'}, 6);
        I.click({ css: 'button[name="home"]'});
        I.click({ css: 'button[name="home"]'});
    })

    I.waitForVisible({ css: 'button[name="leaveRoomMenueButton"]' }, 16);
    I.click({ css: 'button[name="leaveRoomMenueButton"]' });

    // ### go to presession ###
    // I navigate to presession test
    within({css: 'div#footer'},() => {
        I.click({css: 'button[name="preflight"]'});  
    });

    I.see('Pre session check', 'h2');
    I.waitForInvisible('.overlay', 6);

    I.seeElement(locate({css: '.mat-radio-label-content'}).withText('Speaker mode').inside(locate({css: 'mat-radio-button.mat-radio-checked'})));
    // echo cancelation must now be checked, but the rest not
    I.seeElement({ css: '#echoCancellation mat-pseudo-checkbox.mat-pseudo-checkbox-checked'});
    I.seeElement({ css: '#noiseSuppression mat-pseudo-checkbox.mat-pseudo-checkbox-checked'});
    I.seeElement({ css: '#autoGainControl mat-pseudo-checkbox.mat-pseudo-checkbox-checked'});

    // now I override these constraints
    const _eCstate = (await I.grabAttributeFrom('#echoCancellation mat-pseudo-checkbox', 'class')).indexOf('mat-pseudo-checkbox-checked') !== -1;
    const _nSstate = (await I.grabAttributeFrom('#noiseSuppression mat-pseudo-checkbox', 'class')).indexOf('mat-pseudo-checkbox-checked') !== -1;
    const _aGstate = (await I.grabAttributeFrom('#autoGainControl mat-pseudo-checkbox', 'class')).indexOf('mat-pseudo-checkbox-checked') !== -1;

    if(_eCstate) {
        I.click(locate({css: '#echoCancellation'}));
        I.waitForInvisible('.overlay', 6);
    }
    if(_nSstate) {
        I.click(locate({css: '#noiseSuppression'}));
        I.waitForInvisible('.overlay', 6);
    }
    if(_aGstate) {
        I.click(locate({css: '#autoGainControl'}));
        I.waitForInvisible('.overlay', 6);
    }
    I.wait(1);

    // go again back to the session to check whether 
    I.waitForElement({css:'button.startTango'}, 22);
    I.waitForClickable({css:'button.startTango'}, 6);
    I.click({css:'button.startTango'});
    I.seeInCurrentUrl('/room/');

    I.waitForElement('button[name="settings-sidenav"]', 22);
    I.waitForClickable('button[name="settings-sidenav"]', 22);
    // I.seeElement({css: 'button[name="sessionLockRoom"].sessionLockButton.unlocked' });
    I.click({ css: 'button[name="settings-sidenav"]'});

    // wait for the sidenav popping up
    // I.waitForElement('mat-selection-list[name="audio-constraints"]', 22);

     // now all un-checked
     I.dontSee({ css: '#echoCancellation mat-pseudo-checkbox.mat-pseudo-checkbox-checked'});
     I.dontSee({ css: '#noiseSuppression mat-pseudo-checkbox.mat-pseudo-checkbox-checked'});
     I.dontSee({ css: '#autoGainControl mat-pseudo-checkbox.mat-pseudo-checkbox-checked'});

});
