/// <reference path="../steps.d.ts" />

const config = require('codeceptjs').config.get();
const cookieConsent = require('./helper/cookieConsent');
const key = config.key;

Feature('looper');

Scenario('log in as coach1, go to media and search deep killer @smoke', ({ I }) => {
    I.amOnPage('/directlogin?u=coach1@mypelz.de&p=%23coach1');
    I.amOnPage('/')
    cookieConsent(I)

    I.scrollPageToBottom();

    I.waitForClickable({ css: 'button[name="footer-button-media"]'});
    I.click({ css: 'button[name="footer-button-media"]'});

    I.seeInCurrentUrl('media');

    I.seeElement(locate('button[name="multi-share-item"]').inside(locate('.list-item').withText('01 Deep Killer Jam Track TP.mp3')));
    I.click(locate('button[name="multi-share-item"]').inside(locate('.list-item').withText('01 Deep Killer Jam Track TP.mp3')));
    // I.wait(3);
    I.waitForInvisible({ css: 'mat-progress-bar.localProgressBar'}, 15);

    // header row of player
    I.seeElement(locate({css: 'span.noUserSelect'}).withText('Deep Killer Jam Track'))
    I.seeElement(locate({css: '.transportTime'}).withText('0:00'));

    // press "play", wait 3 sec. and expect that transport time increased 3 sec.
    I.waitForClickable({ css: 'button[name="looper-play-button"]'}, 10);
    I.click({css: 'button[name="looper-play-button"]'});
    I.wait(3);
    I.seeElement(locate({css: '.transportTime'}).withText('0:03'));

    I.click({css: 'button[name="looper-to-start-button"]'});
    I.seeElement(locate({css: '.transportTime'}).withText('0:00'));

    // loop toggle button is disabled
    I.seeElement(locate({css: 'button[name="looper-toggle-loop-button"]'}).withAttr({ disabled: 'true' }));

    // create a region by click and drag
    I.dragSlider('#waveform', 100);
    // after this button must be enabled
    I.wait(1);
    // pause();

    // I.seeElement(locate({css: 'button[name="looper-toggle-loop-button"]'}).withAttr({ 'ng-reflect-disabled': 'false' }));
    // I.wait(3);

    // click loop all button
    within({css: 'section[name="loop"]'},() => {
        I.click({css: 'button[name="looper-loop-all-button"]'});
        I.see('0:00');
        I.see('4:33');
    });

    // switch on music again
    I.click({css: 'button[name="looper-play-button"]'});

    within({css: 'section[name="zoom"]'},() => {
        I.see('1.0x');
        I.click('div[name="looper-zoom-in-icon"]');
        I.see('1.5x');
    });

    within({css: 'section[name="pitch"]'},() => {
        I.see('0st');
        I.click('div[name="looper-pitch-plus-icon"]');
        I.see('+1st');
    });
    I.wait(2);

    within({css: 'section[name="playbackRate"]'},() => {
        I.see('1.00x');
        I.click('div[name="looper-playback-plus-icon"]');
        I.click('div[name="looper-playback-plus-icon"]');
        I.see('1.10x');
    });
    I.wait(2);


});

