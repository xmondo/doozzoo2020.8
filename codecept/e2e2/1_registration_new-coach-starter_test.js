/// <reference path="../steps.d.ts" />

const config = require('codeceptjs').config.get();
const key = config.key;

const MailListener = require('mail-listener2');
const assert = require('assert');

// here goes your email connection configuration
const mailListener = new MailListener({
    username: 'pelz@mypelz.de',
    password: 'SIEBEN#meilen?stiefel',
    host: 'imap.1und1.de',
    port: 993, // imap port
    tls: true,
    tlsOptions: { rejectUnauthorized: false },
    mailbox: 'Inbox', // mailbox to monitor
    searchFilter: ['UNSEEN'], // ["UNSEEN", "FLAGGED"], // the search filter being used after an IDLE notification has been retrieved
    markSeen: true, // all fetched email willbe marked as seen and not fetched next time
    fetchUnreadOnStart: true, // use it only if you want to get all unread email on lib start. Default is `false`,
    mailParserOptions: { streamAttachments: true }, // options to be passed to mailParser lib.
    attachments: false, // download attachments as they are encountered to the project directory
    attachmentOptions: { directory: 'attachments/' } // specify a download directory for attachments
});

mailListener.start();

mailListener.on('error', function (err) {
    console.log('mailListener error: ', err);
});

mailListener.on('server:connected', function () {
    console.log('Mail listener initialized');
});

mailListener.on('mail', function (mail, seqno, attributes) {
    // do something with mail object including attachments
    // console.log("emailParsed", mail);
    // mail processing code goes here
});

// helper
function getLastEmail() {
    // console.log('now waiting for an email...');
    return new Promise(resolve => {
        mailListener.on('mail', function (mail) {
            resolve(mail);
        });
    });
}

const user = 'coach2@mypelz.de';
const password = '#coach2';

Feature('new coach registration starter plan e2e');

/*
Scenario('register as coach2 and select starter plan @registration', async ({ I }) => {
    //...
    //console.log('env prod: ', key);

    // 'should delete user5'
    I.amOnPage(`/login?mode=logindelete&u=${config.coach2user}&p=${encodeURIComponent(config.coach2pw)}`);
    I.wait(2);
    I.amOnPage('/');

    // dismiss cookie consent
    I.waitForElement({ css: '.cookie-consent-dialog-container'}, 6);
    I.click({ css: 'button[name="accept"]'});
    I.waitForInvisible({ css: '.cookie-consent-dialog-container'}, 6);

    // click register on home
    // I.click(locate({ css: 'div.mat-tab-label' }).withText('Register'));
    I.click({css: 'button[name="register"]'});
    I.wait(1);

    I.fillField('input[name="email"]', config.coach2user);
    I.fillField('input[name="emailRepeated"]', config.coach2user);
    I.fillField('input[name="password"]', password);
    I.fillField('input[name="passwordRepeated"]', password);
    I.click({css: 'button[name="registerSubmit"]'});
    I.wait(2);
    
    // wait for email and fetch its content
    const email = await getLastEmail();
    // console.log('email content: ', email.text);

    const from = 'support@doozzoo.com';
    assert.strictEqual(email.subject, 'Verify your email for doozzoo');
    assert.strictEqual(email.headers.from, from);
    assert.strictEqual(email.headers.to, config.coach2user);

    // extract registration code from the email message
    const pattern = /(\boobCode=\b[a-zA-Z0-9&=\-_]*)/g;
    const regCode = pattern.exec(email.text);
    // console.log('extracted link: ', pattern, regCode, regCode[0]);

    // expose link
    registerUrl = config.url + '/login?mode=verifyEmail&' + regCode[0];

    mailListener.stop();

    // ### email double opt in ####
    // pause()

    // click registration link
    I.amOnPage(registerUrl);

    I.waitForText('Your account has been verified', 6);

    // click verification continue
    // I.waitForClickable({ css: 'button[name="emailVerifiedNext"]' }, 6);
    I.click({css: 'button[name="emailVerifiedNext"]'});

    // ###  Profile form ###

    // wait for Form
    I.waitForVisible({css: 'div[name="profileForm"]'}, 6);

    // select Profile form for coach role
    I.click({css: 'mat-radio-button[value="coach"]'});

    // select avatar
    I.waitForClickable({css: '.avatarIcon'}, 6);
    I.click({css: '.avatarIcon'});
    // select a random avatar image
    const rNum = Math.floor(Math.random() * 20);
    const imgSelector = `img[src="assets/img/avatars/avataaars_${rNum}.svg"]`;
    I.click({css: imgSelector});

    // fill in profile form
    I.fillField('input[formControlName="firstname"]', 'coach2 first name');
    I.fillField('input[formControlName="lastname"]', 'coach2 lastname');
    I.fillField('input[formControlName="nickname"]', 'coach2nick');

    I.fillField('input[formControlName="address_street1"]', 'Baker Street');
    I.fillField('input[formControlName="zipcode"]', '456789');
    I.fillField('input[formControlName="city"]', 'Antwerp');

    I.fillField('input[formControlName="birthday"]', '23.04.1956');
    I.fillField('input[formControlName="phone"]', '+49 241 888999');

    // option list: native codecept does not work as this are fake material controls
    I.click({ css: 'mat-select[formControlName="country"]' });
    // I.click({ css: '.mat-option-text'});
    I.click(locate({ css: '.mat-option-text'}).withText('Cyprus'));

    I.click('mat-checkbox[formControlName="dataApproval"]');
    //I.checkOption({css: 'mat-checkbox[formControlName="dataApproval"]'});

    I.scrollPageToBottom();

    I.waitForClickable({ css: 'button[name="profileFormCoachSubmit"]' }, 16);
    I.click({ css: 'button[name="profileFormCoachSubmit"]' });

    // proceed
    I.click({css: 'button[name="profileVerifiedNext"]'});


    // ###  Select plan ###

    // select "starter"
    I.click({ css: 'button[name="selectPlanStarter"]' });


    I.waitForText('You selected plan Workshop Room Starter', 6);
    I.scrollPageToBottom();

    // I.waitForVisible({ css: 'button[name="confirmPlanStarter"]' }, 6);
    I.click({ css: 'button[name="confirmPlanStarter"]' });
    //I.waitForElement(locate({ css: 'button[name="confirmPlanStarter"].disabled' }));
    I.waitForText('Your Workshop Room Starter has been set up', 6);
    // mandatory, otherwise the room will not be created by the FB frontend function
    I.wait(2);

    I.click({ css: 'button[name="selectPlanNext"]' });

    I.waitForText('Hi coach2nick, you sucessfully applied as "Coach" at doozzoo, have fun!', 6);

    //I.amOnPage('/');
    // I.waitForElement({ css: 'button[name="myRoomButton"]'}, 8);
    //I.see('free-', { css: 'button[name="myRoomButton"]'});

});
*/

/**
 * login
 * see roomToken free-...
 * do NOT see mycoaches, mystudents -> within #footer
 * start session
 * do NOT see archiving
 * see sessionTimer, should have 100 min. -> .session-time 100 min.
 * */ 

/*
Scenario('login as coach2 and start freemium session @registration', async ({ I }) => {
    I.amOnPage(`/directlogin?u=${config.coach2user}&p=${encodeURIComponent(config.coach2pw)}`);
    I.amOnPage('/');
    // dismiss cookie consent
    I.waitForElement({ css: '.cookie-consent-dialog-container'}, 6);
    I.click({ css: 'button[name="accept"]'});
    I.waitForInvisible({ css: '.cookie-consent-dialog-container'}, 6);

    I.dontSee({ css: 'button[name="myCoaches"]'});
    I.dontSee({ css: 'button[name="myStudents"]'});

    I.waitForClickable({ css: 'button[name="myRoomButton"]'}, 8);
    const roomToken = await I.grabTextFrom({ css: 'span.my-room'})

    I.click({ css: 'button[name="myRoomButton"]'});
    I.waitInUrl(roomToken, 6);

    I.waitForElement({ css: '.session-time'}, 12);

    I.dontSee({ css: 'button[name="archiveMenu"]'});
    // I.see('history 90 min.', { css: '.session-time'});
    I.see('100', { css: '.session-time>span'});

    // leave room
    I.click({ css: 'button[name="home"]'});
    I.waitForVisible({ css: 'button[name="leaveRoomMenueButton"]' }, 16);
    I.click({ css: 'button[name="leaveRoomMenueButton"]' });
});
*/

/**
 * login
 * start session
 * let one participant join
 * let second participant try, which should not be possible -> session full
 * -> No access. The maximal number of participants has been reached for this session. Please contact your coach.
 * */ 

 Scenario('login as coach2 and start freemium session with one and then with a further participant @registration', async ({ I }) => {
    I.amOnPage(`/directlogin?u=${config.coach2user}&p=${encodeURIComponent(config.coach2pw)}`);
    I.amOnPage('/');
    // dismiss cookie consent
    I.waitForElement({ css: '.cookie-consent-dialog-container'}, 6);
    I.click({ css: 'button[name="accept"]'});
    I.waitForInvisible({ css: '.cookie-consent-dialog-container'}, 6);

    I.waitForClickable({ css: 'button[name="myRoomButton"]'}, 8);
    const roomToken = await I.grabTextFrom({ css: 'span.my-room'})
    I.click({ css: 'button[name="myRoomButton"]'});
    //I.waitInUrl(roomToken, 6);
    I.waitForElement({ css: '.session-time'}, 12);

    session('participant1', async () => {
        I.amOnPage(`/free/${roomToken}`);
        I.waitInUrl(`/free/${roomToken}/waitingroom`, 6);

        // dismiss cookie consent
        I.waitForElement({ css: '.cookie-consent-dialog-container'}, 6);
        I.click({ css: 'button[name="accept"]'});
        I.waitForInvisible({ css: '.cookie-consent-dialog-container'}, 6);

        I.see(`Waiting room - ${roomToken}`);

        const name = 'foofoo';
        I.fillField('anonymousName', name);
        I.click('Submit');
        I.dontSee({css:'.anonymousName'});
        I.waitForText('Click the button below to join the session - ' + name, 6);
        // Per default anonymous is added to rooms participant list, but access is false
        // button must be enabled
        I.waitForClickable({ css: 'button[name="joinSessionAnonymous"]'}, 6);
        I.click({ css: 'button[name="joinSessionAnonymous"]'});

        I.wait(1);
        I.dontSeeInCurrentUrl(`/free/${roomToken}/waitingroom`);
    });

    session('participant2', async () => {
        I.amOnPage(`/free/${roomToken}`);
        I.waitInUrl(`/free/${roomToken}/waitingroom`, 6);

        // dismiss cookie consent
        I.waitForElement({ css: '.cookie-consent-dialog-container'}, 6);
        I.click({ css: 'button[name="accept"]'});
        I.waitForInvisible({ css: '.cookie-consent-dialog-container'}, 6);



        I.see(`Waiting room - ${roomToken}`);

        const name = 'Manuela';
        I.fillField('anonymousName', name);
        I.click('Submit');
        I.dontSee({css:'.anonymousName'});
        I.waitForText('No access. The maximal number of participants has been reached for this session. Please contact your coach.', 6);
    });

    session('participant1', async () => {
        I.click({ css: 'button[name="home"]'});
        I.waitForVisible({ css: 'button[name="leaveRoomMenueButton"]' }, 16);
        I.click({ css: 'button[name="leaveRoomMenueButton"]' });
        I.waitInUrl(`/free/${roomToken}/waitingroom`, 6);
        // I.dontSeeInCurrentUrl(`/free/${roomToken}`);
    });

    session('participant2', async () => {
        const name = 'Manuela';
        I.waitForText('Click the button below to join the session - ' + name, 6);
        // Per default anonymous is added to rooms participant list, but access is false
        // button must be enabled
        I.waitForClickable({ css: 'button[name="joinSessionAnonymous"]'}, 6);
        I.click({ css: 'button[name="joinSessionAnonymous"]'});

        I.wait(1);
        I.dontSeeInCurrentUrl(`/free/${roomToken}/waitingroom`);

        // leave room
        I.click({ css: 'button[name="home"]'});
        I.waitForVisible({ css: 'button[name="leaveRoomMenueButton"]' }, 16);
        I.click({ css: 'button[name="leaveRoomMenueButton"]' });

    });

    I.click({ css: 'button[name="home"]'});
    I.waitForVisible({ css: 'button[name="leaveRoomMenueButton"]' }, 16);
    I.click({ css: 'button[name="leaveRoomMenueButton"]' });
    I.wait(2);

});