/// <reference path="../steps.d.ts" />

let config = require('codeceptjs').config.get();

let inviteUrl = null;

Feature('registered student logged in invite');

Scenario('coach1 creates invite and user2 calls it already logged in @invite', async ({ I }) => {

    // login as coach1
    I.amOnPage(`/directlogin?u=${config.coach1user}&p=${encodeURIComponent(config.coach1pw)}`);
    I.amOnPage('/');

    // dismiss cookie consent
    I.waitForElement({ css: '.cookie-consent-dialog-container'}, 6);
    I.click({ css: 'button[name="accept"]'});
    I.waitForInvisible({ css: '.cookie-consent-dialog-container'}, 6);

    // navigate to my students
    within({css: 'div#footer'}, () => {
        I.click({css: 'button[name="myStudents"]'});  
    });

    // select tab
    I.click(locate({ css: 'div.mat-tab-label-content'}).withText('Invite & connect students'));
    
    // create link
    const urls = await I.grabTextFromAll('ul#listInviteLinks li span.inviteLink');
    // console.log('---> ', urls.length, urls);
    urls.length < 2 ? I.click({css: 'button[name="createInviteLink"]'}) : '';

    inviteUrl = await I.grabTextFrom({ css: 'ul#listInviteLinks:first-child span.inviteLink'});
    // console.log('inviteUrl: ', inviteUrl);

    // coach1 logs out
    I.amOnPage('/login?mode=logout');

    // login as user2
    I.amOnPage(`/directlogin?u=${config.user2user}&p=${encodeURIComponent(config.user2pw)}`);
    I.amOnPage(inviteUrl);

    I.see('You have successfully logged in and connected to your coach');

});

