/// <reference path="../steps.d.ts" />

const config = require('codeceptjs').config.get();
const key = config.key;

Feature('waitingroom-simple');
    //.timeout(2000); // set timeout to 5s;

const coachId = config.coach1Id; //'auth0|582c7e4cedb8c73048379aad';
const roomName = 'pro_mannheim1';

Scenario('prepare @waitingroom @room', (async ({ I }) => { // or Background
    I.haveRequestHeaders({'Content-Type':'application/x-www-form-urlencoded'});
    // email=coach1%40mypelz.de&password=%23coach1&returnSecureToken=true
    const response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${key}`,`email=coach1%40mypelz.de&password=%23coach1&returnSecureToken=true`);
    // auth response
    console.log('rest data response: ', response.data.displayName, response.data.idToken);

    // reset data for waitingroom tests
    const mannheim = {
            [roomName]: {
            owner: coachId,
            permanent: true,
            name: 'mannheim1',
            locked: false,
            countdown: 57600,
            myroomId: 'g8o1E2orkWc3WKUZQTra',
            plan: 'premium',
        } 
    }
    I.sendPatchRequest('/now.json?auth='+response.data.idToken, { [roomName] : null });
    I.sendPatchRequest('/now.json?auth='+response.data.idToken, mannheim);

    I.sendPatchRequest(`/classrooms/${coachId}.json?auth=${response.data.idToken}`, { locked: false, roomId: null });
    I.sendPatchRequest(`/classrooms/${coachId}.json?auth=${response.data.idToken}`, { participants: null} );
    I.sendPatchRequest(`/now/${roomName}.json?auth=${response.data.idToken}`, { locked: false} );
    I.wait(1);
}));

