/// <reference path="../steps.d.ts" />

const config = require('codeceptjs').config.get();

/**
 * invite student as role institutional coach
 * Prerequisites:
 * 1. user does not exist -> neither as doozzoo user, nor as institutional user
 * 2. user does exist as registered user of the institution, not connected with the coach
 * 3. user does exist as registered doozzoo user
 */

const assert = require('assert');
const cookieConsent = require('./helper/cookieConsent');

const coachEmail = config.customer1user;
const coachPassword = config.customer1pw;
const userEmail = config.user5user;
const userPassword = config.user5pw;

Feature('edu_coachlist');

Scenario('prepare  @edu', async ({ I }) => {
    I.amOnPage(`/login?mode=logindelete&u=${config.user5user}&p=${encodeURIComponent(config.user5pw)}`);
    I.wait(1);
    // login as customer5
    I.amOnPage(`/directlogin?u=${config.customer1user}&p=${encodeURIComponent(config.customer1pw)}`);
    
    cookieConsent(I)

    // navigate to my students
    within({css: 'div#footer'}, () => {
        I.click({css: 'button[name="myStudents"]'});  
    });
    // I.wait(2);
    // I.seeInCurrentUrl('/mystudents');
    I.waitUrlEquals('/mystudents',10);

    // check mystudents structure & content
    I.see('My students');
    I.see('Create & connect student accounts');
    I.see('Connect manually');
    I.see('Enter an e-mail address, to create an account');

    I.seeElement(locate({ css: 'h2'}).withText('List of accounts'));
    I.seeElement({ css: 'div[name="accountsListRow"'});

    // check basic accounts
    I.see('customer3@mypelz.de');
    I.see('customer4@mypelz.de'); 
    I.dontSee('user5@mypelz.de'); 

    /*
    // 2. user does exist as registered user of the institution, not connected with the coach
    // disconnect customer3

    I.click(locate({css: 'button[name="appMenuTriggerButton"]'}).inside(locate({ css: '.innerItemRow'}).withText('customer3@mypelz.de')));
    //I.wait(1);
    I.click('Disconnect account');
    //I.wait(1);
    I.click('Remove');
    //I.wait(1);
    I.dontSee('customer3@mypelz.de');

    // fill in new account form
    I.fillField('email', 'customer3@mypelz.de');
    I.fillField('email2', 'customer3@mypelz.de');

    I.waitForClickable({css: 'button[name="createAccount"]'});
    I.click({css: 'button[name="createAccount"]'});
    I.wait(1);

    // success --> connected indicates positiv result
    I.seeElement(locate({css: '.invite-button'}).withText('connected').inside(locate({ css: '.innerItemRow'}).withText('customer3@mypelz.de')));

    */

    // 1. option abolute new user
    // fill in new account form
    I.fillField('email', 'user5@mypelz.de');
    I.fillField('email2', 'user5@mypelz.de');

    // pause()

    I.click({css: 'button[name="createAccount"]'});
    I.wait(1);
    I.waitForClickable(locate({css: '.invite-button'}).withText('send invite'), 5);
    I.click(locate({css: '.invite-button'}).withText('send invite'));

    // pause();

    // I.waitForText('invite sent', 5, { css: '.invite-button'});
    I.waitForText('invite sent', 5);

    const pwRaw = await I.grabTextFrom('.password-hint-row');
    const pw = pwRaw.split(':')[1];
    console.log(pw)

    session('user5-logs-in', () => {
        // check preconditions
        I.amOnPage(`/directlogin?u=user5@mypelz.de&p=${pw}`);
        I.waitForElement('.cookie-hint', 10);
        I.click('cookie-hint-button');
    });

    // I.wait(3);

    I.amOnPage('/mystudents');
    I.wait(1);
    I.seeElement(locate({css: '.invite-button'}).withText('invite accepted').inside(locate({ css: '.innerItemRow'}).withText('user5@mypelz.de')));

    I.wait(5);

});

/*
Scenario('check mystudents structure & content', async (I) => {
    // ...
});
*/

/*
Scenario('test user deactivation', async (I) => {

    I.see('Please click below to access your institution backoffice','.mat-title');

    I.see('Access institution backoffice');
    I.click('Access institution backoffice');

    I.click(locate({css: 'button[name="edu-backoffice"]'}).inside(locate('div').withText('Kreismusikschule Heinsberg')));
    //I.click(locate('.toggleAccess').inside(locate('li').withText(name)));
    I.see('List of accounts','h2');

    I.click(locate('.toggleAccess').inside(locate('div.innerItemRow').withText('customer3@mypelz.de')));

    I.wait(1);

    I.haveRequestHeaders({'Content-Type':'application/x-www-form-urlencoded'});
    // email=coach1%40mypelz.de&password=%23coach1&returnSecureToken=true
    let _response = await I.sendPostRequest('https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyBHJt1JFPRSuue0ZkbRNEy7oZcpN7KBAqA','email=customer3%40mypelz.de&password=%23customer3&returnSecureToken=true');
    // auth response
    // ### console.log('rest data response: ',  _response.data);//response.data.idToken);
    // { error: { code: 400, message: 'USER_DISABLED', errors: [ [Object] ] } }
    // { kind: 'identitytoolkit#VerifyPasswordResponse', localId: '1GaYBJtvGNccp9umENFbYpqn2o52', email: 'customer3@mypelz.de', displayName: 'invite from - Kreismusikschule Heinsberg', idToken:'<token>', registered: true, refreshToken: '<token>', expiresIn: '3600' }

    assert.equal(_response.data.error.message,'USER_DISABLED');

    I.click(locate('.toggleAccess').inside(locate('div.innerItemRow').withText('customer3@mypelz.de')));
    I.wait(1);

    I.haveRequestHeaders({'Content-Type':'application/x-www-form-urlencoded'});
    // email=coach1%40mypelz.de&password=%23coach1&returnSecureToken=true
    _response = await I.sendPostRequest('https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyBHJt1JFPRSuue0ZkbRNEy7oZcpN7KBAqA','email=customer3%40mypelz.de&password=%23customer3&returnSecureToken=true');
    // auth response
    // ### console.log('rest data response: ',  _response.data);//response.data.idToken);
    // { error: { code: 400, message: 'USER_DISABLED', errors: [ [Object] ] } }
    // { kind: 'identitytoolkit#VerifyPasswordResponse', localId: '1GaYBJtvGNccp9umENFbYpqn2o52', email: 'customer3@mypelz.de', displayName: 'invite from - Kreismusikschule Heinsberg', idToken:'<token>', registered: true, refreshToken: '<token>', expiresIn: '3600' }

    assert.equal(_response.data.email,'customer3@mypelz.de');

});

*/


