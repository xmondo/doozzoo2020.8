/// <reference path="../steps.d.ts" />

const cookieConsent = require('./helper/cookieConsent');

Feature('testDrive');

Scenario('visit home page, start test drive and test in room @smoke', ({ I }) => {
    I.amOnPage('/');
cookieConsent(I)

    I.scrollPageToBottom();

    I.waitForClickable({ css: 'button[name="testdrive"]'}, 6);
    I.click({ css: 'button[name="testdrive"]'});

    I.seeInCurrentUrl('testdrive');

    I.waitForClickable({ css: 'button[name="tangoButton"]'}, 15);
    I.click({ css: 'button[name="tangoButton"]'});

    // I.wait(2);
    // I.seeInCurrentUrl('/room/');
    I.waitInUrl('/room/', 6);
    I.waitForInvisible({ css: '.waitForPublisherSpinner' }, 15);
    I.scrollPageToBottom();

    I.waitForElement({ css: '#footer'}, 6);
    within('#footer', () => {
        I.waitForElement({ css: 'button[name="home"]'}, 6);
        I.click({ css: 'button[name="home"]'});
    })

    I.waitForClickable({ css: 'button[name="leaveRoomMenueButton"]' }, 6);
    I.click({ css: 'button[name="leaveRoomMenueButton"]' });
});

Scenario('start test drive and see all relvant functions, including login @smoke', ({ I }) => {
    I.amOnPage('/');
cookieConsent(I)

    I.scrollPageToBottom();

    I.waitForClickable({ css: 'button[name="testdrive"]'}, 6);
    I.click({ css: 'button[name="testdrive"]'});

    I.seeInCurrentUrl('testdrive');

    // check wheterh all relevant pages are visible
    within('#footer', () => {
        I.see('Test drive');
        I.seeElement({ css: 'button[name="preflight"]'});
        I.see('Media');
        I.see('Apps');
        I.see('Login');
    });

    // check if login shows the right tab
    I.click('Login');

    within({ css: 'mat-dialog-container'}, () => {
        I.waitForText('Please enter e-mail and password:')
    });
    
});

