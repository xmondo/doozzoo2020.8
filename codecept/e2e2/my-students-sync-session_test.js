/// <reference path="../steps.d.ts" />

const config = require('codeceptjs').config.get();
const cookieConsent = require('./helper/cookieConsent');
const key = config.key;

Feature('my-students');

// const studentEmail = 'user2@mypelz.de';
const coachEmail = 'stripecoach2@mypelz.de';
const coachId = config.stripecoach2Id;
const user1Id = config.user1Id;
const user2Id = config.user2Id;
const user3Id = config.user3Id;
const user4Id = config.user4Id;
const coach1Id = config.coach1Id;
const admin1pw = config.admin1pw;
const admin1pwEncoded = encodeURIComponent(config.admin1pw);

/**
 * objective
 * stripecoach2 -> 2X1ZM44k8lXX6t2w1X17fx76DPK2
 * user1 -> WmYDCkyM3kcTZpRj9ZX63xgy7kx2
 * user2 -> OFHEb2PkOOY51gLObPrlsLWlAmS2
 */

 
Scenario('prepare my-students sync @my-students', async ({ I }) => {
    I.haveRequestHeaders({'Content-Type':'application/x-www-form-urlencoded'});
    const response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${key}`,`email=admin1%40mypelz.de&password=${admin1pw}&returnSecureToken=true`);
    // auth response
    // console.log('response: ', response);

    // remove any edu assignments from stripcoach2
    const r0 = await I.sendPatchRequest(`/users/${coachId}.json?auth=${response.data.idToken}`, { trigger: null }); 
    console.log('response: ', r0.data)

    // setup the stripe package size correctly
    const r1 = await I.sendPatchRequest(`/stripe_customers/${coachId}/customerData/subscriptions/data/0/items/data/0.json?auth=${response.data.idToken}`, { quantity: 1 }); // = 5 students
    // console.log(r1)
    // 
    await I.sendPatchRequest(`/classrooms/${coachId}.json?auth=${response.data.idToken}`, { locked: true });
    // clear participants in session

    const r2 = await I.sendPatchRequest(`/coachCustomer.json?auth=${response.data.idToken}`, { [coachId] : null } );
    // console.log(r2)

    await I.sendPatchRequest(`/coachCustomer/${coachId}.json?auth=${response.data.idToken}`, { [user1Id]: true });
    await I.sendPatchRequest(`/coachCustomer/${coachId}.json?auth=${response.data.idToken}`, { [user2Id]: true });
    await I.sendPatchRequest(`/coachCustomer/${coachId}.json?auth=${response.data.idToken}`, { [user3Id]: true });
    await I.sendPatchRequest(`/coachCustomer/${coachId}.json?auth=${response.data.idToken}`, { [user4Id]: true });
    await I.sendPatchRequest(`/coachCustomer/${coachId}.json?auth=${response.data.idToken}`, { [coach1Id]: true });
});


Scenario('login as coach stripecoach2 go 2 my students and deactivate all @my-students', async ({ I }) => {
    // coach logs in
    I.amOnPage('/directlogin?u=stripecoach2@mypelz.de&p=%23stripecoach2');

    cookieConsent(I)

    I.click({ css: 'button[name="myStudents"]'});
    I.waitInUrl('/mystudents', 6);

    within({ css: 'ul.listMyStudents'}, async () => {
        I.seeNumberOfElements({css: 'li.student-list-item'}, 5);
        const nrOfLi = await I.grabNumberOfVisibleElements({css: 'li.student-list-item'});
        console.log(nrOfLi);

        //pause()

        for(let i=1; i<=nrOfLi; i++){
            const state = await I.grabAttributeFrom({ css: `:nth-child(${i})>mat-checkbox.toggleAccess` }, 'ng-reflect-checked');
            state === 'true' ? I.click(locate({ css: `:nth-child(${i})>mat-checkbox.toggleAccess` })) : '';
        }    
        
    });

    I.wait(5);

    // start session
    I.click({ css: 'button[name="tangoButton"]'});
    I.waitInUrl('/room/', 12);

    I.waitForElement({ css: 'button[name="participantsSidenavButton"]'}, 12);
    I.click({ css: 'button[name="participantsSidenavButton"]'});

    I.waitForElement({ css: 'mat-sidenav.sidenavContentRight'});
    within('app-participant-list', async () => {
        I.seeNumberOfElements({css: 'ul.listMyStudents>li'}, 0);
        const nrOfLi = await I.grabNumberOfVisibleElements({css: 'ul.listMyStudents>li'});
        console.log(nrOfLi);
    })

});


// in combination with the following scenario
Scenario('login as coach stripecoach2 go to my students and activate 3 @my-students', async ({ I }) => {
    // coach logs in
    I.amOnPage('/directlogin?u=stripecoach2@mypelz.de&p=%23stripecoach2');

    cookieConsent(I)

    I.click({ css: 'button[name="myStudents"]'});
    I.waitInUrl('/mystudents', 6);

    within({ css: 'ul.listMyStudents'}, async () => {
        I.seeNumberOfElements({css: 'li.student-list-item'}, 5);

        for(let i=1; i<=5; i++){
            const state = await I.grabAttributeFrom({ css: `:nth-child(${i})>mat-checkbox.toggleAccess` }, 'ng-reflect-checked');
            state === 'true' ? I.click(locate({ css: `:nth-child(${i})>mat-checkbox.toggleAccess` })) : '';
        }
        // now activate 3 
        for(let i=1; i<=3; i++){
            // const state = await I.grabAttributeFrom({ css: `:nth-child(${i})>mat-checkbox.toggleAccess` }, 'ng-reflect-checked');
            I.click(locate({ css: `:nth-child(${i})>mat-checkbox.toggleAccess` }));
        }    
        
    });

    I.wait(3); // human wait

    // start session
    I.click({ css: 'button[name="tangoButton"]'});
    I.waitInUrl('/room', 12);

    I.waitForElement({ css: 'button[name="participantsSidenavButton"]'}, 12);
    I.click({ css: 'button[name="participantsSidenavButton"]'});

    I.waitForElement({ css: 'mat-sidenav.sidenavContentRight'});
    within('app-participant-list', async () => {
        // pause()
        I.seeNumberOfElements({css: 'ul.listMyStudents>li'}, 3);
        const nrOfLi = await I.grabNumberOfVisibleElements({css: 'ul.listMyStudents>li'});
        console.log(nrOfLi);
    })

    // I.seeElement({css: 'ul.listMyStudents>li'})

});

// in combination with the previous scenario

Scenario('change stripe package quantity in backend @my-students', (async ({ I }) => { // or Background
    I.haveRequestHeaders({'Content-Type':'application/x-www-form-urlencoded'});

    let response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${key}`,"email=admin1%40mypelz.de&password=%23admin1&returnSecureToken=true");
    // auth response
    response = await I.sendPatchRequest(`/stripe_customers/${coachId}/customerData/subscriptions/data/0/items/data/0.json?auth=${response.data.idToken}`, { quantity: 0 });
    // console.log(response);
}));   


Scenario('check stripe package quantity changes in backend and see correct package size hint @my-students', (async ({ I }) => { // or Background
    // coach logs in
    I.amOnPage('/directlogin?u=stripecoach2@mypelz.de&p=%23stripecoach2');

    cookieConsent(I)

    I.click({ css: 'button[name="myStudents"]'});
    I.waitInUrl('/mystudents', 6);
    
    within({ css: 'app-package-hint'}, async () => {
        I.waitForText('No subscription found');
    });

    I.waitForElement({ css: 'ul.listMyStudents'}, 6);

    // as the try to change visibility will trigger the recaclculation of the package
    within({ css: 'ul.listMyStudents'}, async () => {
        I.click(locate({ css: `:nth-child(2)>mat-checkbox.toggleAccess` })); 
        I.wait(2)   
        // I.dontSeeElement(locate({ css: `:nth-child(2)>mat-checkbox.toggleAccess` }).withAttr({'class':''}), 6);      
    });

    // reset data to previous state
    let response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${key}`,"email=admin1%40mypelz.de&password=%23admin1&returnSecureToken=true");
    await I.sendPatchRequest(`/stripe_customers/${coachId}/customerData/subscriptions/data/0/items/data/0.json?auth=${response.data.idToken}`, { quantity: 1 });

    within({ css: 'app-package-hint'}, async () => {
        I.see('Your subscription: Up to 5 Students');
    });

}));


Scenario('prepare my-students sync classroom for 1 @my-students', async ({ I }) => {
    I.haveRequestHeaders({'Content-Type':'application/x-www-form-urlencoded'});
    const response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${key}`,`email=admin1%40mypelz.de&password=${admin1pwEncoded}&returnSecureToken=true`);
    // auth response
    // console.log('response: ', response);
    // set the package quantity to 1 * 5
    await I.sendPatchRequest(`/stripe_customers/${coachId}/customerData/subscriptions/data/0/items/data/0.json?auth=${response.data.idToken}`, { quantity: 1 });
    // clear participants in session
    await I.sendPatchRequest(`/classrooms/${coachId}.json?auth=${response.data.idToken}`, { locked: false });
    await I.sendPatchRequest(`/classrooms/${coachId}.json?auth=${response.data.idToken}`, { participants: null });
    const data = {
        access:true,
        inSession:false,
        isWaiting:false,
        registrationStatus:'isCoachCustomer',
    }
    await I.sendPatchRequest(`/classrooms/${coachId}/participants.json?auth=${response.data.idToken}`, { [user1Id]: data });
    await I.sendPatchRequest(`/coachCustomer/${coachId}.json?auth=${response.data.idToken}`, { [user1Id]: true });
});

Scenario('activate user 1 in mystudents and check in session @my-students', (async ({ I }) => { // or Background
    // coach logs in
    I.amOnPage('/directlogin?u=stripecoach2@mypelz.de&p=%23stripecoach2');

    cookieConsent(I)

    // start session
    I.click({ css: 'button[name="tangoButton"]'});
    I.waitInUrl('/room/', 12);

    I.waitForElement({ css: 'button[name="participantsSidenavButton"]'}, 12);
    I.click({ css: 'button[name="participantsSidenavButton"]'});

    I.waitForElement({ css: 'mat-sidenav.sidenavContentRight'});
    within('app-participant-list', async () => {
        I.seeNumberOfElements({css: 'ul.listMyStudents.notInSession>li'}, 1);
        I.seeElement(locate({css: 'ul.listMyStudents.notInSession>li:nth-child(1)>mat-slide-toggle.toggleAccess'}).withAttr({'ng-reflect-checked':'true'}));
    })

}));

Scenario('prepare my-students sync classroom @my-students', async ({ I }) => {
    I.haveRequestHeaders({'Content-Type':'application/x-www-form-urlencoded'});
    const response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${key}`,`email=admin1%40mypelz.de&password=${admin1pwEncoded}&returnSecureToken=true`);
    // auth response
    // console.log('response: ', response);
    // set the package quantity to 1 * 5
    await I.sendPatchRequest(`/stripe_customers/${coachId}/customerData/subscriptions/data/0/items/data/0.json?auth=${response.data.idToken}`, { quantity: 1 });
    // clear participants in session
    await I.sendPatchRequest(`/classrooms/${coachId}.json?auth=${response.data.idToken}`, { locked: true });
    await I.sendPatchRequest(`/classrooms/${coachId}.json?auth=${response.data.idToken}`, { participants: null });
    await I.sendPatchRequest(`/coachCustomer/${coachId}.json?auth=${response.data.idToken}`, { [user1Id]: false });
    await I.sendPatchRequest(`/coachCustomer/${coachId}.json?auth=${response.data.idToken}`, { [user1Id]: true });
});

// due to lock status the user should not be activated
Scenario('activate before not activated user 1 in mystudents and check in session @my-students', (async ({ I }) => { // or Background
    // coach logs in
    I.amOnPage('/directlogin?u=stripecoach2@mypelz.de&p=%23stripecoach2');

    cookieConsent(I)

    I.wait(2); // human wait

    // start session
    I.click({ css: 'button[name="tangoButton"]'});
    I.waitInUrl('/room/', 12);

    I.waitForElement({ css: 'button[name="participantsSidenavButton"]'}, 12);
    I.click({ css: 'button[name="participantsSidenavButton"]'});

    I.waitForElement({ css: 'mat-sidenav.sidenavContentRight'});
    within('app-participant-list', async () => {
        I.seeNumberOfElements({css: 'ul.listMyStudents.notInSession>li'}, 1);
        //const nrOfLi = await I.grabNumberOfVisibleElements({css: 'ul.listMyStudents>li'});
        //console.log(nrOfLi);
        I.seeElement(locate({css: 'ul.listMyStudents.notInSession>li:nth-child(1)>mat-slide-toggle.toggleAccess'}).withAttr({'ng-reflect-checked':'false'}));
    })

}));

