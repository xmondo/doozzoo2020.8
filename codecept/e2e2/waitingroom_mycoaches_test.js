/// <reference path="../steps.d.ts" />

const config = require('codeceptjs').config.get();
const cookieConsent = require('./helper/cookieConsent');
const key = config.key;
const isProd = config.profile === 'prod' ? true : false;

Feature('waitingroom_mycoaches');

const studentEmail = 'user2@mypelz.de';
const coachEmail = 'coach1@mypelz.de';
const coachName = 'coach1FirstName';
const coachId = config.coach1Id;
const userId = config.user2Id;
const userName = 'user2FirstName user2LastName';

const participantListItem = {
    // key: string,
    access: true,
    name: userName,
    inSession: false,
    isWaiting: false,
    registrationStatus: 'isCoachCustomer',
}

const login = require('./helper/login');

Scenario('prepare session and set lock to "false" @waitingroom @room', (async ({ I }) => { // or Background

    console.log(isProd)

    I.haveRequestHeaders({'Content-Type':'application/x-www-form-urlencoded'});
    // email=coach1%40mypelz.de&password=%23coach1&returnSecureToken=true
    const response = await I.sendPostRequest(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${key}`,"email=coach1%40mypelz.de&password=%23coach1&returnSecureToken=true");
    // auth response
    // console.log('rest data response: ', response.data.displayName, response.data.idToken);

    await I.sendPatchRequest(`/classrooms/${coachId}.json?auth=${response.data.idToken}`, { locked: false, roomId: null });
    await I.sendPatchRequest(`/classrooms/${coachId}/participants.json?auth=${response.data.idToken}`, { [userId]: participantListItem });
}));


Scenario('login as student go to my coaches and want to join the locked session @waitingroom @room', ({ I }) => {
    // student logs in
    login(I, config.user2user, config.user2pw);

    I.see('MY COACHES');
    I.waitForClickable({css:'button.startTango'},6);
    I.click({css:'button.startTango'});
    I.seeInCurrentUrl('/mycoaches');
    I.see('coach1FirstName coach1LastName');

    // coach is NOT in room, so generic message
    I.see('I\'m back in a second!');
    I.waitForElement(locate({css:'.buttonGoToClassroom'}).withAttr({ disabled: 'true' }), 6);

    session('coach', async () => {
        // student logs in
        login(I,config.coach1user, config.coach1pw);

        // wait for tango button
        I.waitForClickable({ css: 'button[name="myRoomButton"]'}, 16);
        I.click({ css: 'button[name="myRoomButton"]'});
    
        I.waitInUrl('/pro/', 6);
    });

    // coach is now in the room, and as its NOT locked, 
    // he sees the button enabled
    // indicated by the according message
    I.waitForElement(locate({css:'.messageInClassroomUnlocked'}), 12);

    session('coach', async () => {
        I.waitForElement('.sessionLockButton', 6);
        // now LOCKING room
        I.click('.sessionLockButton');
    });

    // room is locked now, student should notify coach
    I.waitForText('Your coach is busy', 12);
    // I.waitForText('I am ready to join', 8);
    I.click('I am ready to join');
    // I.wait(2);
    I.waitForText('you can join soon', 8);

    // pause()

    session('coach', () => {
        // snackbar
        I.waitForText('want\'s to join the session', 12)
        I.waitForElement({ css: 'button[name="participantsSidenavButton"]'}, 6);
        I.click({ css: 'button[name="participantsSidenavButton"]'});
        I.waitForText('My students', 6);
        I.see(userName);
        I.click(locate('.toggleAccess').inside(locate('li').withText(userName)));
    });

    // I.wait(2)
    I.waitForElement(locate('.buttonGoToClassroom').inside(locate('li').withText(coachName)), 10);
    I.click(locate('.buttonGoToClassroom').inside(locate('li').withText(coachName)));
    // I.waitForNavigation();
    // I.wait(2)
    // I.seeInCurrentUrl('/pro/');
    I.waitInUrl('/pro/', 6);

    session('coach', () => {

        I.click('.sessionLockButton');

        I.wait(2);

    });


});
