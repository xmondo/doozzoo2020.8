# testing with codecept

npx codeceptjs 

## TODO / Issues
INvite process??!!

## 0. groups
npx codeceptjs run --grep @smoke --steps --profile=dev
npx codeceptjs run --grep @registration --steps --profile=dev
npx codeceptjs run --grep @waitingroom --steps --profile=dev
npx codeceptjs run --grep @testtimer --steps --profile=dev
npx codeceptjs run --grep @my-students --steps --profile=dev
npx codeceptjs run --grep @session --steps --profile=dev
npx codeceptjs run --grep @invite --steps --profile=dev
npx codeceptjs run --grep @constraints --steps --profile=dev
### check if meaningfull
npx codeceptjs run --grep @edu --steps --profile=dev
npx codeceptjs run --grep @stripe --steps --profile=dev
npx codeceptjs run --grep @room --steps --profile=dev

npx codeceptjs run --grep @smoke --steps --profile=prod
npx codeceptjs run --grep @registration --steps --profile=prod
npx codeceptjs run --grep @waitingroom --steps --profile=prod
npx codeceptjs run --grep @testtimer --steps --profile=prod
npx codeceptjs run --grep @my-students --steps --profile=prod
npx codeceptjs run --grep @session --steps --profile=prod
npx codeceptjs run --grep @invite --steps --profile=prod
npx codeceptjs run --grep @constraints --steps --profile=prod
### check if meaningfull
npx codeceptjs run --grep @edu --steps --profile=prod
npx codeceptjs run --grep @stripe --steps --profile=prod
npx codeceptjs run --grep @room --steps --profile=prod

npx codeceptjs run --grep @foofoo

## 1. room
### 

### 1.1 waitingroom
-> prepare first @waitingroom-first
npx codeceptjs run e2e2/waitingroom_prepare-mannheim1_test.js --steps --profile=prod

-> then @waitingroom
npx codeceptjs run --grep @waitingroom --steps --profile=prod

npx codeceptjs run e2e2/waitingroom_simple_test.js --steps --profile=prod
npx codeceptjs run e2e2/waitingroom_2sessions_test.js --steps --profile=prod
npx codeceptjs run e2e2/waitingroom_mycoaches_test.js --steps --profile=prod

## 2. apps
@smoke
### 2.1 looper 
npx codeceptjs run e2e2/looper_test.js --steps --profile=dev --profile=prod
### 2.2 tuner
### 2.3 metronome
### 2.4 recorder
### 2.5 mixer / matrix   

## 3. EDU
@edu
// TODO: errors
npx codeceptjs run e2e2/edu_adminlist_test.js --steps --profile=prod
// TODO: errors
npx codeceptjs run e2e2/edu_coachlist_test.js --steps --profile=prod
###
npx codeceptjs run e2e2/edu_admin-adds-user_test.js --steps --profile=prod

## 4. Language Switcher
@smoke
npx codeceptjs run e2e2/languages_test.js --steps --profile=prod

## 5. archiving
npx codeceptjs run e2e2/archiving_0_flow_test.js --steps --profile=prod

## 6. login 
### 6.1 password forgotten
npx codeceptjs run e2e2/login_password-forgotten_test.js --steps --profile=prod
### 6.2 login messages & errors
npx codeceptjs run e2e2/login_messages_test.js --steps --profile=prod

## 7. Stripe
// ...

## 7. registration 
npx codeceptjs run --grep @registration --steps --profile=prod
### 7.1 coach2 new e2e
npx codeceptjs run e2e2/1_registration_new-coach-starter_test.js --steps --profile=dev --profile=prod
npx codeceptjs run e2e2/1_registration_new-coach-premium_test.js --steps --profile=dev --profile=prod
npx codeceptjs run e2e2/1_registration_new-coach-starter-interruptions_test.js --steps --profile=prod
### 7.2 coach2 changes payment
npx codeceptjs run e2e2/2_registration_existing-user_change-plan_test.js  --steps --profile=prod
### 7.3 coach2 changes payment
npx codeceptjs run e2e2/3_registration_existing-user_change_payment_test.js  --steps --profile=prod
### 7.4 coach2 changes payment
// subscribe archiving
### 7.5 user5 new e2e
npx codeceptjs run e2e2/4_registration_new-student_test.js --steps --profile=dev --profile=prod
### 7.6 interruption
npx codeceptjs run e2e2/6_registration_new-user-interruptions_test.js --steps --profile=prod

## 8. Presession / Session
@smoke
npx codeceptjs run e2e2/1_presession_test.js --steps --profile=prod
npx codeceptjs run e2e2/constraints-session-presession_test.js --steps --profile=prod

## 9. Testdrive / delay
@smoke
npx codeceptjs run e2e2/testDrive_test.js --steps --profile=prod
npx codeceptjs run e2e2/delay_test.js --steps --profile=prod

## 10 EDU
### 
npx codeceptjs run e2e2/my-students-edu_test.js --steps --profile=prod
npx codeceptjs run e2e2/edu_coachlist_test.js --steps --profile=prod
npx codeceptjs run e2e2/edu_adminlist_test.js --steps --profile=prod
npx codeceptjs run e2e2/edu_admin-adds-user_test.js --steps --profile=prod

## 11 Room / Session
###
## 11.1 Room Viewmode
npx codeceptjs run e2e2/room_view-mode_test.js --steps --profile=prod
## 11.2 test timer
npx codeceptjs run e2e2/testModeTimer_test.js --steps --profile=prod
## 11.3 room lock
npx codeceptjs run e2e2/session-room-lock_test.js --steps --profile=prod

## 11.4 toolbar activate
npx codeceptjs run e2e2/session-toolbar-activate_test.js --steps --profile=prod

## 12 my Students
npx codeceptjs run e2e2/my-students_test.js --steps --profile=prod
npx codeceptjs run e2e2/my-students-sync-session_test.js --steps --profile=prod

## 13 participants
npx codeceptjs run e2e2/room-participants_test.js --steps --profile=prod

## 14 cookie consent
npx codeceptjs run e2e2/cookie-consent_test.js --steps --profile=prod

## 15 apps
### 15.1 metronome
npx codeceptjs run e2e2/metronome_test.js --steps --profile=prod

## 16 invite
npx codeceptjs run e2e2/invite_registered-student_test.js --steps --profile=dev
npx codeceptjs run e2e2/invite_registered-student-logged-in_test.js --steps --profile=dev
npx codeceptjs run e2e2/invite_unregistered-student_test.js --steps --profile=dev


//



