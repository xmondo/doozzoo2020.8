export const environment = {
  production: true,
  name: 'prod',
  // eve
  firebase: {
    apiKey: "AIzaSyAklBOk3aEvjfR0KDOK2Ju5GQnlsUCUlnA",
    authDomain: "doozzoo-eve.firebaseapp.com",
    databaseURL: "https://doozzoo-eve.firebaseio.com",
    projectId: "doozzoo-eve",
    storageBucket: "doozzoo-eve.appspot.com",
    messagingSenderId: "1000190963716",
    appId: "1:1000190963716:web:6a5f01d976c4539b"
  },
  opentok: {
    apiKey: '46059892'
  },
  stripePublishableKey: 'pk_live_ogAH7GMHFrzFp1bJgCO2Y1Yg',
  // httpFunctionsUrl: 'https://us-central1-doozzoo-eve.cloudfunctions.net/',
  httpFunctionsUrl: 'https://europe-west1-doozzoo-eve.cloudfunctions.net/',
  tld: 'doozzoo.com',
};
