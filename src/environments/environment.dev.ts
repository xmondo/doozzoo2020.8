export const environment = {
  production: false,
  name: 'dev',
  firebase: {
    apiKey: 'AIzaSyBHJt1JFPRSuue0ZkbRNEy7oZcpN7KBAqA',
    authDomain: 'doozzoo-dev.firebaseapp.com',
    // databaseURL: 'https://doozzoo-dev.firebaseio.com', // RTDB US
    databaseURL: 'https://doozzoo-dev-eu.firebaseio.com',// RTDB EU
    projectId: 'doozzoo-dev',
    storageBucket: 'doozzoo-dev.appspot.com',
    messagingSenderId: '507347482581',
    appId: '1:507347482581:web:31ac24142556a922'
  },
  opentok: {
    apiKey: '46105142'
  },
  stripePublishableKey: 'pk_test_b8fFMnEjFJYLvNlLOik5hc54',
  // httpFunctionsUrl: 'https://us-central1-doozzoo-dev.cloudfunctions.net/',
  httpFunctionsUrl: 'https://europe-west1-doozzoo-dev.cloudfunctions.net/',
  tld: 'dev.doozzoo.com',
};

