export const environment = {
  production: false,
  name: 'stage',
  firebase: {
    apiKey: 'AIzaSyArEC2z2ZHXmhNFdW8qXN_ABLhAV1hF01I',
    authDomain: 'project-6816589442602044124.firebaseapp.com',
    databaseURL: 'https://project-6816589442602044124.firebaseio.com',
    projectId: 'project-6816589442602044124',
    storageBucket: 'project-6816589442602044124.appspot.com',
    messagingSenderId: '1072065802537',
    appId: '1:1072065802537:web:9f7164831ee6ac5c'
  },
  opentok: {
    apiKey: '45743172'
  },
  stripePublishableKey: 'pk_live_ogAH7GMHFrzFp1bJgCO2Y1Yg',
  httpFunctionsUrl: 'https://europe-west1-project-6816589442602044124.cloudfunctions.net/',
  tld: 'stage.doozzoo.com',
};