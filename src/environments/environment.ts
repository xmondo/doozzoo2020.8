// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  name: 'dev',
  firebase: {
    apiKey: 'AIzaSyBHJt1JFPRSuue0ZkbRNEy7oZcpN7KBAqA',
    authDomain: 'doozzoo-dev.firebaseapp.com',
    databaseURL: 'https://doozzoo-dev.firebaseio.com', // RTDB US
    // databaseURL: 'https://doozzoo-dev-eu.firebaseio.com',// RTDB EU
    projectId: 'doozzoo-dev',
    storageBucket: 'doozzoo-dev.appspot.com',
    messagingSenderId: '507347482581'
  },
  opentok: {
    apiKey: '46105142'
  },
  stripePublishableKey: 'pk_test_b8fFMnEjFJYLvNlLOik5hc54',
  // httpFunctionsUrl: 'https://us-central1-doozzoo-dev.cloudfunctions.net/',
  httpFunctionsUrl: 'https://europe-west1-doozzoo-dev.cloudfunctions.net/',
  tld: 'dev.doozzoo.com',
};
