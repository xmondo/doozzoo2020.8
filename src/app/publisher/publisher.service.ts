import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs';
import { AVConstraints } from '../shared/opentok.service';
import { PublisherState, PublisherStoreService } from './publisher-store.service';

@Injectable({
  providedIn: 'root'
})
export class PublisherService {
  subContainer = new Subscription();
  publisherToStorage: Subscription = null;
  constructor(
    private publisherStore: PublisherStoreService,
  ) { }

  saveToLocalStorage() {
    this.publisherToStorage = this.publisherToStorage === null ? this.publisherStore
      .state$
      .subscribe(val => localStorage.setItem('publisherPrefs', JSON.stringify(val))) : null;
    // easier to handle
    this.publisherToStorage !== null ? this.subContainer.add(this.publisherToStorage) : '';
  }

  getFromLocalStorage() {
    const publisherPrefs = JSON.parse(localStorage.getItem('publisherPrefs'));
    return publisherPrefs ?? null;
  }

  doUnsubscribe(){
    this.subContainer.unsubscribe();
  }

  buildConstraints(publisherStorePrefs: PublisherState) {
    let width = 640;
    let ratio = 4 / 3; // calculation!!!
    if (publisherStorePrefs.resolution === '1280x720') {
      width = 1280;
      ratio = 16 / 9;
    } else {
      // nothing, keep defaults
    }

    const constraints: AVConstraints = {
      video: {
        deviceId: publisherStorePrefs.videoSource,
        width: width,
        aspectRatio: { ideal: ratio },
      },
      audio: {
        // deviceId: { exact: this.devices.audioInputSelected },
        // deviceId: { ideal: <any> this.devices.audioInputSelected },
        deviceId: publisherStorePrefs.audioSource,
        echoCancellation: publisherStorePrefs.echoCancellation,
        noiseSuppression: publisherStorePrefs.noiseSuppression,
        autoGainControl: publisherStorePrefs.autoGainControl,
      }
    }
    return constraints;
  }


}
