import { Injectable } from '@angular/core';
import { ComponentStore } from '@ngrx/component-store';
import { Observable } from 'rxjs';
import { AudioService } from '../shared/audio.service';
import { OpentokService } from '../shared/opentok.service';
import { PublisherService } from './publisher.service';

export interface PublisherState {
  audioSource: string | boolean,
  videoSource: string | boolean, 
  audioOutDeviceId: string,
  publishAudio: boolean, // = mic?
  publishVideo: boolean, // = cam?
  fitMode: string, // 'contain', // cover | contain 
  audioBitrate: 64000 | 128000 | 192000, //this.as.getBitrate(), // 64000 | 128000 | 192000
  name: string,
  mirror: boolean, 
  enableStereo: boolean, 
  // TODO: audioprocessing default on or off?
  disableAudioProcessing: boolean, // true,
  echoCancellation: boolean,
  noiseSuppression: boolean,
  autoGainControl: boolean,
  constraints: any,
  resolution: '1280x720' | '640x480' | '320x240', // resolution, // 
  // headphoneMode: boolean, // not stored but dependent on constraints
  sessionConnected: boolean,
  publisherStatus: 'videoElementCreated' | 'destroyed' | 'streamCreated' | null,
}

export interface AudioConstraints {
  deviceId: string,
  echoCancellation: boolean,
  noiseSuppression: boolean,
  autoGainControl: boolean,
}

@Injectable({
  providedIn: 'root',
})
export class PublisherStoreService extends ComponentStore<PublisherState> {
  constructor(
    // ...
    //private ots: OpentokService,
    private as: AudioService,
  ) { 
    super({ 
      audioSource: 'default', // constraints.video.deviceId
      videoSource: 'default',  // constraints.video.deviceId
      audioOutDeviceId: 'default',
      publishAudio: true,
      publishVideo: true,
      fitMode: 'contain', // cover | contain 
      audioBitrate: 64000,
      name: null,
      mirror: false, // ,
      enableStereo: false, 
      // TODO: audioprocessing default on or off?
      disableAudioProcessing: true, 
      echoCancellation: false,
      noiseSuppression: false,
      autoGainControl: false,
      constraints: null,
      resolution: '640x480', 
      // headphoneMode: true,
      sessionConnected: false,
      publisherStatus: null,
    });

    // set prefs
    /*
    this.setMirrorMode(this.ots.getMirrorMode() || false);
    this.setAudioBitrate(this.as.getBitrate() || 64000);
    this.setEnableStereo(this.ots.getStereoMode() || false);
    // this.setConstraints(this.ots.getConstraints());
    // this.setHeadphoneMode(this.as.getHeadphoneMode() || true);
    */

    const storageState = this.getFromLocalStorage();
    storageState ? this.setAllState(storageState) : '';
  }

  // *********** Selects ************ //
  public readonly disableAudioProcessing$: Observable<boolean> = this.select(state => state.disableAudioProcessing);
  public readonly deviceId$: Observable<string | boolean> = this.select(state => state.audioSource);
  public readonly audioSource$: Observable<string | boolean> = this.select(state => state.audioSource);
  public readonly audioOutDeviceId$: Observable<string | boolean> = this.select(state => state.audioOutDeviceId);
  public readonly videoSource$: Observable<string | boolean> = this.select(state => state.videoSource);
  public readonly resolution$: Observable<string> = this.select(state => state.resolution);
  public readonly echoCancellation$: Observable<boolean> = this.select(state => state.echoCancellation);
  public readonly noiseSuppression$: Observable<boolean> = this.select(state => state.noiseSuppression);
  public readonly autoGainControl$: Observable<boolean> = this.select(state => state.autoGainControl);
  // public readonly headphoneMode$: Observable<boolean> = this.select(state => state.headphoneMode);
  public readonly sessionConnected$: Observable<boolean> = this.select(state => state.sessionConnected);
  public readonly publisherStatus$: Observable<PublisherState['publisherStatus']> = this.select(state => state.publisherStatus);

  // ### UPDATER ###
  readonly setAllState = this.updater((state: PublisherState, value: PublisherState) => ({
    ...state,
    ...value, 
  }));
  readonly resetState = this.updater((state: PublisherState) => ({
    ...state,
    audioSource: 'default', 
    videoSource: 'default',  
    audioOutDeviceId: 'default',
    publishAudio: true,
    publishVideo: true,
    fitMode: 'contain', // cover | contain 
    audioBitrate: 64000,
    name: null,
    mirror: false, // ,
    enableStereo: false, 
    // TODO: audioprocessing default on or off?
    disableAudioProcessing: true, 
    echoCancellation: false,
    noiseSuppression: false,
    autoGainControl: false,
    constraints: null,
    resolution: '640x480', 
    // headphoneMode: true,
    sessionConnected: false,
    publisherStatus: null,
  }));
  readonly setAudioSource = this.updater((state: PublisherState, value: PublisherState['audioSource']) => ({
    ...state,
    audioSource: value, 
  }));
  readonly setName = this.updater((state: PublisherState, value: PublisherState['name']) => ({
    ...state,
    name: value, 
  }));
  readonly setVideoSource = this.updater((state: PublisherState, value: PublisherState['videoSource']) => {
    return ({
      ...state,
      videoSource: value, 
    })
  });
  readonly setAudioOutDeviceId = this.updater((state: PublisherState, value: PublisherState['audioOutDeviceId']) => ({
    ...state,
    audioOutDeviceId: value, 
  }));
  readonly setMirrorMode = this.updater((state: PublisherState, value: PublisherState['mirror']) => ({
    ...state,
    mirror: value, 
  }));
  readonly setAudioBitrate = this.updater((state: PublisherState, value: PublisherState['audioBitrate']) => ({
    ...state,
    audioBitrate: value, 
  }));
  readonly setEnableStereo = this.updater((state: PublisherState, value: PublisherState['enableStereo']) => ({
    ...state,
    enableStereo: value, 
  }));
  readonly setEchoCancellation = this.updater((state: PublisherState, value: PublisherState['echoCancellation']) => ({
    ...state,
    echoCancellation: value, 
  }));
  readonly toggleEchoCancellation = this.updater((state: PublisherState) => ({
    ...state,
    echoCancellation: !state.echoCancellation, 
  }));
  readonly setNoiseSuppression = this.updater((state: PublisherState, value: PublisherState['noiseSuppression']) => ({
    ...state,
    noiseSuppression: value, 
  }));
  readonly setAutoGainControl = this.updater((state: PublisherState, value: PublisherState['autoGainControl']) => ({
    ...state,
    autoGainControl: value, 
  }));
  readonly setResolution = this.updater((state: PublisherState, value: PublisherState['autoGainControl']) => ({
    ...state,
    autoGainControl: value, 
  }));
  readonly setConstraints = this.updater((state: PublisherState, value: PublisherState['constraints']) => {
    console.log('#### ', value);
    return ({
      ...state,
      audioSource: value.audio.deviceId,
      videoSource: value.video.deviceId,
      echoCancellation: value.audio.echoCancellation,
      noiseSuppression: value.audio.noiseSuppression,
      autoGainControl: value.audio.autoGainControl,
      constraints: value, 
    })
  });
  // this ONLY sets headphoneMode
  readonly setHeadphoneMode = this.updater((state: PublisherState, value: boolean) => ({
    ...state,
    headphoneMode: value, 
  }));
  // this toggles also the dependent audio constraints
  readonly toggleHeadphoneMode = this.updater((state: PublisherState, value: boolean) => ({
    ...state,
    echoCancellation: value ? false : true,
    noiseSuppression: value ? false : false,
    autoGainControl: value ? false : false,
    headphoneMode: value, 
  }));
  readonly setSessionConnected = this.updater((state: PublisherState, value: PublisherState['sessionConnected']) => ({
    ...state,
    sessionConnected: value, 
  }));
  readonly setPublisherStatus = this.updater((state: PublisherState, value: PublisherState['publisherStatus']) => ({
    ...state,
    publisherStatus: value, 
  }));
  readonly toggleMirror = this.updater((state: PublisherState, value: PublisherState['mirror']) => ({
    ...state,
    mirror: !state.mirror, 
  }));
  readonly toggleResolution = this.updater((state: PublisherState, value: PublisherState['resolution']) => ({
    ...state,
    resolution: state.resolution === '640x480' ? '1280x720' : '640x480', 
  }));
  readonly toggleStereo = this.updater((state: PublisherState, value: PublisherState['enableStereo']) => ({
    ...state,
    enableStereo: !state.enableStereo, 
  }));


  // ### SELECTS ###

  // ViewModel for the component
  public readonly vm$ = this.select(
    this.state$,
    (state) => ({
      state
    })
  );

  // ViewModel for the constraints
  public readonly audioConstraints$ = this.select(
    this.deviceId$,
    this.echoCancellation$,
    this.noiseSuppression$,
    this.autoGainControl$,
    (deviceId, echoCancellation, noiseSuppression, autoGainControl) => ({
      deviceId, echoCancellation, noiseSuppression, autoGainControl
    }),
    {debounce: true}, // 👈 setting this selector to debounce
  );

  // ViewModel for the constraints
  public readonly preSessionCheckChanges$ = this.select(
    this.audioSource$,
    this.videoSource$,
    this.resolution$,
    (audioSource, videoSource, resolution) => ({
      audioSource, videoSource, resolution
    }),
    // {debounce: true}, // 👈 setting this selector to debounce
  );

  // ViewModel for the component
  public readonly headphoneMode$ = this.select(
    this.echoCancellation$,
    this.noiseSuppression$,
    this.autoGainControl$,
    (echoCancellation, noiseSuppression, autoGainControl) => {
      // TODO: should we add noise suppression??
      return echoCancellation === false && autoGainControl === false ? true : false;
    }
  );

  // ViewModel for the component
  public readonly doPublish$ = this.select(
    this.sessionConnected$,
    this.publisherStatus$,
    (sessionConnected, publisherStatus) => {
      // TODO: should we add noise suppression??
      return sessionConnected === true && publisherStatus === 'videoElementCreated' ? true : false;
    },
    // {debounce: true}, // 👈 setting this selector to debounce
  );

  getFromLocalStorage() {
    const publisherPrefs = JSON.parse(localStorage.getItem('publisherPrefs'));
    return publisherPrefs ?? null;
  }

// end of class
}
