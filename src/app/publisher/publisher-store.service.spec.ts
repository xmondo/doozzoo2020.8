import { TestBed } from '@angular/core/testing';

import { PublisherStoreService } from './publisher-store.service';

describe('PublisherStoreService', () => {
  let service: PublisherStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PublisherStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
