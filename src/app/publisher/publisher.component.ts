// tslint:disable-next-line:max-line-length
import { Component, ElementRef, OnDestroy, OnInit, AfterViewInit, ViewChild, Input, Output, EventEmitter, Renderer2 } from '@angular/core';
import { AVConstraints, OpentokService } from '../shared/opentok.service';
import { AudioService } from '../shared/audio.service';
import { filter, map, distinctUntilChanged, switchMap, skip, tap, mergeMap, take, first } from 'rxjs/operators';
import { AuthService } from '../shared/auth.service';
import { combineLatest, from, Observable, of, Subject, Subscription } from 'rxjs';
import { ThumbsManagerService } from '../shared/thumbs-manager.service';
import { AudioConstraints, PublisherState, PublisherStoreService } from './publisher-store.service';
import { PublisherService } from './publisher.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-publisher',
  templateUrl: './publisher.component.html',
  styleUrls: ['./publisher.component.scss'],
})

export class PublisherComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('publisherDiv', { static: true }) publisherDiv: ElementRef;
  @Input() session: any; // OT.Session;
  @Input() userName: string; // from firebase displayName
  @Input() userId: string; 
  @Input() photoURL: string; 
  @Input() isOwner: boolean; 
  @Output() publisherStatus: EventEmitter<boolean> = new EventEmitter(); // true -> is publishing
  @Input() user: any; // from firebase usr$
  @Output() openSidenav: EventEmitter<string> = new EventEmitter();
  publisher: OT.Publisher;
  publishing: boolean;
  publishVideo = true;
  publishAudio = true;
  sessionStatus: any;
  fitModeCover: boolean = false;
  thumbsId: string = null;
  isHero: boolean = true;
  toRight: number = 0;
  layoutGrid: boolean = false;
  // container for video devices
  videoDevicesArr: Array<any>;

  // contains all ...
  thumbsObj: any;
  audioLabel: string;
  videoLabel: string;

  // defines location of video item
  status: any; // class | right

  // for managing bulk unsubscription
  subContainer: Subscription = new Subscription();

  constraints: AVConstraints;
  vm$: Observable<any>;
  triggerPublisher$ = new Subject();

  debug = false;

  constructor(
    public ots: OpentokService,
    public tms: ThumbsManagerService,
    private as: AudioService,
    public renderer: Renderer2,
    private a: AuthService,
    public publisherStore: PublisherStoreService,
    private publisherService: PublisherService,
    private router: Router,
    ) {
    //...
    this.vm$ = this.publisherStore.vm$;

    this.publishing = false;

    // init status to default value
    this.status = {
      class: '',
      right: 0,
    }
  }

  ngOnInit() {

    // { publisher: null, session: null }
    const seSub = this.ots.sessionEmitter
      .pipe(
        
        map(val => {
          // console.log('publisher->sessionEmitter PRE filter: ', val);
          // console.log('publisher->sessionEmitter PRE filter: ', this.session);
          return val;
        }),
        
        // tap(val => console.log('publisher->sessionEmitter PRE filter: ', this.session)),
        filter(val => this.session !== null && this.session !== undefined),
        filter(val => this.session.isConnected()),
        filter((val:any) => val.publisher !== null && val.publisher !== undefined), // && val.sessionCurrentState === 'connected'),
        distinctUntilChanged(),
      )
      .subscribe(val => {
        // console.log('publisher->sessionEmitter post filter: ', val);
        this.publish();
      });
    this.subContainer.add(seSub);

    // IMPORTANT: catches any OT errors and retriggers the publishing process
    // works well in case the issue is caused by saved device ids, which are not working any more
    this.ots.errorsEmitter
      .subscribe(event => {
        console.log(event)
        // tslint:disable-next-line:max-line-length
        if (event.error?.name.toLowerCase().indexOf('overconstrained') !== -1 || event.error?.message.toLowerCase().indexOf('invalid constraint') !== -1) {
          // console.log('***', event);
          this.publisherStore.setAudioSource('default');
          this.publisherStore.setVideoSource('default');
          setTimeout(() => this.triggerPublisher$.next(), 500);
        }
      })

    // init sync of prefs to localStorage
    this.publisherService.saveToLocalStorage();

    // register publisher in parent array
    // and set "hero" to true
    // this.doThumbArrayChange.emit({'type': 'publisher', 'index': 0, 'hero': true });
    // invoke reading constraints from local storage
    // prepares for GUM request in After View
    // this.ots.getDevicesConstraints();

    // filters for trigger by payload 'fiftyFifty'
    const ctsSub = this.ots.controlsEmitter
      .pipe(filter(val => val === 'fiftyFifty'))
      .subscribe( _ => this.layoutGrid = this.layoutGrid ? false : true);
    this.subContainer.add(ctsSub);

    // init status attributes
    // TODO: fallback in case of empty response
    try{
      this.status = this.tms.getThumbsStatus(this.thumbsId);
    } 
    catch(error) {
      console.log(error);
    }

    const deSub = this.ots.deviceEmitter.subscribe(action => {
      if (action.type === 'videoinput') {
        this.publisher.cycleVideo()
          .then(device => {
            // console.log( 'device switched to next video device', device);
            this.publisherStore.setVideoSource(device.deviceId);
          }).catch(error => {
            console.log( 'PublisherComponent->cycleVideo->error: ', error.name, error.message );
          });
      }
    });
    this.subContainer.add(deSub);

    // toggle mic/cam on/off
    const cteSub = this.ots.controlsEmitter.subscribe(payload => {
      switch (payload) {
        case 'cam':
          this.publishVideo = !this.publishVideo;
          this.publisher.publishVideo(this.publishVideo);
        break;
        case 'mic':
          this.publishAudio = !this.publishAudio;
          this.publisher.publishAudio(this.publishAudio);
        break;
      }
    });
    this.subContainer.add(cteSub);
    
  }

  ngOnDestroy() {
    // console.log('destroy publisher')
    try{
      this.publisher ? this.publisher.destroy() : '';
    } catch (error) {
      console.log('publisher->onDestroy->error: ', error);
    }
    this.subContainer.unsubscribe();
  }

  async ngAfterViewInit() {

    // triggeres publisher initiation
    const trpuSub = this.triggerPublisher$
      .pipe(
        switchMap(() => this.publisherStore
          .state$
          .pipe(
            take(1),
            // ### 1. fetches usermedia and... ###
            // creates properties for publisher
            // tap(publisherStorePrefs => console.log('+++ publisherStore: ', publisherStorePrefs)),
            switchMap(publisherStorePrefs => from(this.doGUM(publisherStorePrefs))
              .pipe(
                // tap(publisherProperties => console.log('@@@ initializePublisher: ', publisherProperties)),
                // ### 2. initializes publisher from these properties ###
                map(publisherProperties => this.initializePublisher(publisherProperties)),
              )
            )
          )
        )
      )
      .subscribe();
    this.subContainer.add(trpuSub);
    this.triggerPublisher$.next();
    
    // this.initializePublisher(publisherProperties);
    // ### init event listener ###
    // with every change, calc distance to right
    const theSub = this.tms.thumbsEmitter
      // .pipe(filter(val => val.action === 'isHero'))
      .subscribe(val => {
        this.status = this.tms.getThumbsStatus(this.thumbsId);
        // get thumbsobject
        const thumbsArr = this.tms.thumbsArrayGet();
        this.thumbsObj = thumbsArr.find(item => item.id === this.thumbsId);
      });
    this.subContainer.add(theSub);

    // ### toggle Echo cancellation ###
    const puSub = this.publisherStore
      .audioConstraints$
      .pipe(
        skip(1), // must not fire while initialization
        // distinctUntilChanged(),
        map((audioConstraints: AudioConstraints) => {
          this.toggleAudioMode(audioConstraints);
        })
      )
      .subscribe();
    this.subContainer.add(puSub);

    // ### toggle Echo cancellation ###
    const heaSub = this.publisherStore
      .headphoneMode$
      .pipe(
        skip(1), 
        distinctUntilChanged(),
        map((mode: boolean) => {
          this.ots.setAudioMode(mode);
          this.as.setHeadphoneMode(mode);
        })
      )
      .subscribe();
    this.subContainer.add(heaSub);

  }

   /**
   * fetches user media according to
   * ots contraints and
   * puts them into the 
   * properties to initialize publisher
   */
  async doGUM(publisherStorePrefs) {
      let stream = null;

      // make sure video devices exist, otherwise map it later to "default"
      // OT specific: deviceId "default" will throw and "overconstraint" exception
      // Therefore we must finaly map it to "true" in the OT publisher config
      let audioSource: string | boolean = true;
      let videoSource: string | boolean = true;
      const isAudioSourceAvailable = await this.ots.deviceAvailable(publisherStorePrefs.audioSource);
      const isVideoSourceAvailable = await this.ots.deviceAvailable(publisherStorePrefs.videoSource);
      if (isAudioSourceAvailable) {
        audioSource = publisherStorePrefs.audioSource;
      } else {
        audioSource = true;
        this.publisherStore.setAudioSource('default');
      }
      if (isVideoSourceAvailable) {
        // mapping from "default" to "true" due to OT specific needs
        videoSource = publisherStorePrefs.videoSource.indexOf('default') !== -1 ? true : publisherStorePrefs.videoSource;
      } else {
        videoSource = true;
        this.publisherStore.setVideoSource('default');
      }

      const constraints: AVConstraints = {
        video: {
          deviceId: audioSource,
        },
        audio: {
          deviceId: videoSource,
          echoCancellation: publisherStorePrefs.echoCancellation,
          noiseSuppression: publisherStorePrefs.noiseSuppression,
          autoGainControl: publisherStorePrefs.autoGainControl,
        }
      };
      // console.log('publisher->doGUM constraints: ', constraints);
  
      stream = await navigator.mediaDevices
        .getUserMedia({video: true, audio: true})
        .catch(error => {
          console.log('publisher GUM catch: ', error);
          // user did not allow camera, microphone
          error.name === 'NotAllowedError' ? this.router.navigate(['/camera-access']) : '';
        });
  
      // used only for extracting lables from streams
      const aTrk = await stream.getAudioTracks()[0];
      const vTrk = await stream.getVideoTracks()[0];
      const name = this.parseVideoName(aTrk, vTrk);
      this.publisherStore.setName(name);
      // console.log('AVTracks: ', name, aTrk, vTrk);
  
      // init global audio nodes
      /**
       * This seems to be necessary if a new publisher is created,
       * otherwise the audiotracks are not live any more
       */
      this.as.initGlobalAudioNodes();
      // most important: connect OT relevant stream with custom audio
      const destAudioTrack = this.as.rewireAudioTrack(stream);
  
      // ### enableStereo is missing in types!!! ###
      const props = {
        audioSource: destAudioTrack,
        videoSource: videoSource, 
        insertMode: 'append', // replace || append
        width: '100%',
        height: '100%',
        fitMode: 'contain', // cover | contain 
        audioBitrate: publisherStorePrefs.audioBitrate, // 64000 | 128000 | 192000
        name: name,
        mirror: publisherStorePrefs.mirror,
        enableStereo: publisherStorePrefs.enableStereo,
        // TODO: audioprocessing default on or off?
        disableAudioProcessing: true,
        // audioFallbackEnabled: true,
        resolution: publisherStorePrefs.resolution, // '1280x720', // "1280x720", "640x480", and "320x240"
      };
      /**
       * return properties for initialization of publisher, which contain a fully initialized video and audio track
       */
      // console.log('### ', props);
      return props;
      // END doGUM
    }

  async selectProAudio(status: string = 'standard'){ // simple | standard | proAudio
    const constraints = this.ots.getConstraints();
    let customStream: MediaStream;
    try {
      customStream = await navigator.mediaDevices
       .getUserMedia( { audio: constraints.audio } );      
    } catch(error) {
      console.log('selectProAudio error: ', error);
    }
  
    let audioTrack: MediaStreamTrack;
    if (status === 'proAudio') {
      audioTrack = this.as.rewireAudioTrack(customStream);
    } else {
      audioTrack = customStream.getAudioTracks()[0];
    }
    this.publisher.setAudioSource(audioTrack);    
  }

  /**
   * @param properties 
   */
  initializePublisher(properties) {
    const self = this;
    // console.log('pre init Publisher: ', properties)
    // init OT publisher
    // const publisherEl = this.renderer.selectRootElement('#publisherDiv');
    // console.log('rootEl: ', publisherEl);

    this.publisher = OT.initPublisher(
      // publisherEl,
      this.publisherDiv.nativeElement,
      properties
    );

    // publisher events
    this.publisher.on('videoElementCreated', function(event) {
      // console.log('Publisher video element was created...');
      self.ots.sessionTrigger.publisher = true;
      self.ots.sessionEmitter.next(self.ots.sessionTrigger);
      self.publisherStore.setPublisherStatus('videoElementCreated');
    });

    this.publisher.on('streamCreated', function(event) {
      // console.log('Publisher started streaming: ', event.stream);
      self.thumbsId = event.stream.streamId;
      self.tms.thumbsEmitter.next({ action: 'create', id: event.stream.streamId, event: event }); // event.stream.streamId
      self.renderPublisherElement();
      // console.log('Publisher started streaming...');
      self.publisherStore.setPublisherStatus('streamCreated');
    });

    this.publisher.on('destroyed', function(event: any) {
      // ...
      // self.ots.streamEmitter.emit({ action: 'publisherDestroyed', event: event });
      self.publisherStatus.emit(false);
      self.tms.thumbsEmitter.next({ action: 'delete', id: event.target.streamId, event: event });
      // console.log('Publisher destroyed...', event);
      self.publisherStore.setPublisherStatus('destroyed');
    }); 

    // subscribe to changes of the videoSource
    this.publisherStore
      .videoSource$
      .pipe(
        skip(1),
      )
      .subscribe((deviceId:string) => {
        // console.log('===== setVideoSource: ', deviceId)
        this.publisher.setVideoSource(deviceId)
      });
  }

  // ...if so, publish
  publish() {
    // console.log('session: ', this.session)
    this.session.publish(this.publisher, (err) => {
      if (err) {
        console.log('session.publish error: ', err.name);
        // TODO: covered by OT generic exeption error???
        // this.ots.errorsEmitter.emit(event);
      } else {
        // this should trigger all other functions exposed in the classroom
        this.publishing = true;
        // ...
        console.log('--> Publisher session published');
        // this.ots.streamEmitter.emit({ action: 'publisherCreated', content: this.publisher });
        this.publisherStatus.emit(true);
      }
    });
  }

  // ### helper functions
  toggleVideoView() {
    // OT_fit-mode-contain | OT_fit-mode-cover
    let OT_root = this.publisherDiv.nativeElement.querySelector('.OT_root');
    const state = OT_root.classList.value.indexOf('OT_fit-mode-cover') === -1 ? false : true;
    // console.log('classes', this.publisherDiv.nativeElement.classList.value);

    // console.log('state: ', state);
    if (state) {
      this.fitModeCover = false;
      this.renderer.removeClass(OT_root, 'OT_fit-mode-cover');
      this.renderer.addClass(OT_root, 'OT_fit-mode-contain');
    } else {
      this.fitModeCover = true;
      this.renderer.removeClass(OT_root, 'OT_fit-mode-contain');
      this.renderer.addClass(OT_root, 'OT_fit-mode-cover');
    }
  }

  parseVideoName(aTrk, vTrk) {
   const data = {
     id: this.userId,
     name: this.userName,
     isOwner: this.isOwner,
     avatar: this.photoURL,
     audioLabel: aTrk.label,
     videoLabel: vTrk.label,
   } 
   this.audioLabel = aTrk.label;
   this.videoLabel = vTrk.label;
   return JSON.stringify(data);
  }

  renderPublisherElement() {   
    const avatarEl = this.publisherDiv.nativeElement.querySelector('.publisher .OT_widget-container .OT_video-poster');  
    const url = 'url(' + this.photoURL + ')';
    this.renderer.setStyle(
      avatarEl,
      'background-image',
      url
    );
  }

  /**
   * This function takes care of switching audio constraints of the current audio track
   * This only works consistantly by establishing the complete setup
   * starting with GUM to setting it as Audio source in OT
   * @param audioConstraints 
   */
  async toggleAudioMode(audioConstraints: AudioConstraints) {
    console.log('toggle AudioConstraints: ', audioConstraints);
    // const allConstraints = this.ots.getConstraints();
    // allConstraints.audio = audioConstraints;
    // this.ots.setConstraints(allConstraints);
    this.ots.setAudioConstraints(audioConstraints);

    this.as.initGlobalAudioNodes();
    // console.log('audioConstraints audio', this.audioConstraints.audio);
    const customStream: MediaStream = await navigator.mediaDevices
      .getUserMedia({ audio: audioConstraints });
    const destAudioTrack = this.as.rewireAudioTrack(customStream);
    // TODO: publish false/true necessary?
    this.publisher.publishAudio(false);
    this.publisher.setAudioSource(destAudioTrack); 
    this.publisher.publishAudio(true);
  }

// end of class
}


