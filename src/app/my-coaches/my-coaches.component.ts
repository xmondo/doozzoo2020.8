import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shared/auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { map, mergeMap, filter, defaultIfEmpty, subscribeOn } from 'rxjs/operators';

@Component({
  selector: 'app-my-coaches',
  templateUrl: './my-coaches.component.html',
  styleUrls: ['./my-coaches.component.scss']
})
export class MyCoachesComponent implements OnInit {

  coachesList: any;
  coaches: any;
  userId: any;

  constructor(
    public a: AuthService,
    public db: AngularFireDatabase,
  ) { }

  ngOnInit() {
    /**
     * filter makes sure, that only events pass, 
     * that contain an already resolved user
     */
    this.a.usr$
      .pipe(
        // uid must exist and role of registered user
        filter(val => val.uid && val.role > -1)
      )
      .subscribe(val => {
        // console.log('userId: ', val.uid),
        this.userId = val.uid;
        this.getCoachesArr();
      });
  }

  getCoachesArr() {

   /**
    * orderByChild puts matches for a child containing user to the end of the list
    * equalTo true "somehow" filters out non "null" matches === only those coaches, containing the user
    * true/false value can be retrieved directly in template then
    */

    this.coachesList = this.db
      .list(
        'coachCustomer',
        ref => ref.orderByChild(this.userId).equalTo(true)
      )
      .snapshotChanges()
      // .valueChanges()
      // .stateChanges()
      .pipe(
        // map((val: any) => val.val())
      );
  }

  calc(id) {
    console.log(id);
    return this.db
      .object('coachCustomer/' + id + '/' + this.userId)
      .valueChanges();
  }

}
