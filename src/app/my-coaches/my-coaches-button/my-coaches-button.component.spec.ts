import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyCoachesButtonComponent } from './my-coaches-button.component';

describe('MyCoachesButtonComponent', () => {
  let component: MyCoachesButtonComponent;
  let fixture: ComponentFixture<MyCoachesButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyCoachesButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyCoachesButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
