import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
// import { AuthService } from '../../shared/auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { filter, map, mergeMap } from 'rxjs/operators';
import { Subscription, Observable } from 'rxjs';

@Component({
  selector: 'app-my-coaches-button',
  templateUrl: './my-coaches-button.component.html',
  styleUrls: ['./my-coaches-button.component.scss']
})
export class MyCoachesButtonComponent implements OnInit, OnDestroy {
  @Input() coachId: any;
  @Input() userId: any;
  roomId: string;
  meetingRoom: Observable<any>;
  _roomAccess: any;
  _isWaiting: Observable<boolean>;
  subContainer: Subscription = new Subscription;

  constructor(
    // public a: AuthService,
    public db: AngularFireDatabase,
    public router: Router
  ) {
    // ...
  }

  ngOnInit() {
    // ###
    this.meetingRoom = this.db
      .object('classrooms/' + this.coachId)
      .valueChanges();

    // subscription emits null | true | false
    // subscription emits noRoom | join | wait | anonymous
    // null in case of the participant not beeing listed in classroom
    this._roomAccess = this.meetingRoom
      .pipe(
        // does meeting room exist
        filter((val: any) => val !== null && val !== undefined),
        // has roomId been set?
        map((val: any) => val.roomId ? val : null),
        map((val: any) => {
          // console.log('mycoachesButton->_roomAccess: ', val, this.coachId);
          // no roomId does not exist
          if (!val) {
            return 'noRoom'; // false;
          // does participant exist?
          } else if (val.participants[this.userId]) {
            // we have a roomId, assign it
            // this.roomId = val.roomId;
            this.roomId = this.sessionLinkFromRoomId(val.roomId);
            // is participants access allowed?
            if (val.participants[this.userId].access) {
              return 'join';// true;
            } else {
              if (val.participants[this.userId].isWaiting) {
                return 'isWaiting'; // false;
              } else {
                return 'wait';
              }
              
            }
          // participant does not exist
          } else {
            return 'anonymous';
          }
        })
      );
   
    this._isWaiting = this.meetingRoom
      .pipe(
        map((val: any) => {
          if(val === null || val === undefined) {
            return false;
          }
          if(val.participants[this.userId].isWaiting === null || val.participants[this.userId].isWaiting === undefined) {
            return false;
          } else {
            return val.participants[this.userId].isWaiting;
          }
        }),
      )

  }

  doKnockKnock(state) {
    // console.log('knockKnock...', this.userId);
    this.db
      .object(`classrooms/${this.coachId}/participants/${this.userId}/isWaiting`)
      .set(state);
  }

  sessionLinkFromRoomId(roomId) {
    return '/' + roomId.replace('_', '/');
  }

  ngOnDestroy() {
    this.subContainer.unsubscribe();
  }

}
