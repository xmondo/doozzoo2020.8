import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFirestore } from '@angular/fire/firestore';
import { ComponentStore, tapResponse } from '@ngrx/component-store';
import * as firebase from 'firebase';
import { Observable, EMPTY } from 'rxjs';
import { map, switchMap, tap, catchError, filter, distinctUntilChanged, startWith } from 'rxjs/operators';
import { StripeService } from '../stripe/stripe.service';

export const enum AccountStatus {
  // 'isAnonymous','testDrive', 'isRegistered', 'emailVerified', 'hasProfile', 'planSelected', 'planSubscribed'
  isAnonymous = 'isAnonymous',
  testDrive = 'testDrive',
  isRegistered = 'isRegistered',
  emailVerified = 'emailVerified',
  hasProfile = 'hasProfile',
  planSelected = 'planSelected',
  planSubscribed = 'planSubscribed',
}

export const enum Role {
  undefined = -2,
  anonymous = -1,
  student = 0,
  coach = 1,
  admin = 2,
  edu = 3,
}

export const enum ProfileType {
  student = 'student',
  coach = 'coach',
  edu = 'edu',
}

export const enum Plan {
  starter = 'starter',
  pro = 'pro',
  premium = 'premium',
  edu = 'edu',
}

// make available for other modules as well
export interface AccountState {
  userId: string,
  isAnonymous: boolean,
  email: string,
  emailVerified: boolean,
  role: number,
  //userActions: string,
  photoURL: string,
  plan: Plan,
  profileType: ProfileType, //'student' | 'coach' | 'edu', // does this make sense?? set with profile submit 
  nickName: string,
  betatester: boolean,
  subscribed: boolean,
  lastSignInTime?: number | string,
  creationTime?: number | string,
}

@Injectable({
  providedIn: 'root'
})
export class AccountStoreService extends ComponentStore<AccountState> {
  constructor(
    private afs: AngularFirestore,
    private db: AngularFireDatabase,
    public afa: AngularFireAuth,
    private stripe: StripeService,
  ) { 
    super({ 
      userId: null,
      isAnonymous: true,
      email: null,
      emailVerified: false,
      role: -2,
      //userActions: string,
      photoURL: 'assets/img/avatars/avataaars-default.png',
      plan: null,
      profileType: null,
      // accountStatus: null,
      nickName: null,
      betatester: false,
      subscribed: false,
      lastSignInTime: null,
      creationTime: null,
    });

    /*
    this.afa.authState
      .pipe(
        filter(val => val !== null && val !== undefined),
      )
      .subscribe(user => {
        console.log('-----', user);
        this.setFBUser(user)
      });
    */

    this.afa.auth
      .onAuthStateChanged(user => {
        if(user !== null){
          this.setFBUser(user); // from auth change
          this.getUser(user.uid); // from user
          this.getContactData(user.uid); // from profile
          this.getStripeSubscription(user.uid); // from profile
        } else {
          this.resetFBUser();
          // done in reset FBUser...
          //const role = this.get().isAnonymous ? -1 : this.get().role; //
          //this.setRole(role);
          //console.log('rrr2 ', role)
        }
      });

    // this.stateChanges(this.state$);

  }

  // *********** Updaters *********** //
  readonly setFBUser = this.updater((state: AccountState, value: firebase.User) => {
    return ({
      ...state,
      userId: value.uid, 
      isAnonymous: value.isAnonymous,
      email: value.email,
      emailVerified: value.emailVerified,
      nickName: value.displayName,
      photoURL: value.photoURL || 'assets/img/avatars/avataaars-default.png',
      lastSignInTime: value.metadata?.lastSignInTime,
      creationTime: value.metadata?.creationTime,
    })
  });

  public readonly userId$: Observable<string> = this.select(state => state.userId);
  public readonly nickName$: Observable<string> = this.select(state => state.nickName);
  public readonly role$: Observable<Role> = this.select(state => state.role);
  public readonly plan$: Observable<Plan> = this.select(state => state.plan);
  public readonly profileType$: Observable<ProfileType> = this.select(state => state.profileType);
  private readonly isAnonymous$: Observable<boolean> = this.select(state => state.isAnonymous);
  public readonly emailVerified$: Observable<boolean> = this.select(state => state.emailVerified);
  public readonly subscribed$: Observable<boolean> = this.select(state => state.subscribed);

  private readonly profileTypeObs$ = this.select(
    (state) => {
      if(state.role > -1) {
        if(state.role === 3) {
          return ProfileType.edu;
        } else if(state.role === 1) {
          return ProfileType.coach;
        } else if (state.role === 0) {
          return ProfileType.student;
        }
      } else if(state.profileType !== null) {
        return state.profileType;
      } else {
        return null;
      }  
    }
  );

  // ViewModel for the component
  // to have a pure function, we shoukld in any case return a value
  // TODO: thats not complete due to the null cases
  public readonly accountStatus$ = this.select(
    this.state$, this.profileType$,
    (state, profileType) => {
      if(state.userId === null && state.isAnonymous) {
        return AccountStatus.isAnonymous;
      } else if(state.userId !== null && state.isAnonymous) {
        return AccountStatus.testDrive;
      } else if(!state.emailVerified && state.email !== null) {
        return AccountStatus.isRegistered;
      } if(state.emailVerified && profileType === null) { // state.role < 0) {
        return AccountStatus.emailVerified;
      } else if(state.profileType === 'student' || (state.profileType === 'coach' && state.plan === null)) { // state.role > -1 && state.plan === null) {
        return AccountStatus.hasProfile;
      // only relevant for coaches
      } else if(state.profileType === 'coach') {
        // if starter pack, we need only the pack selection for having a valid subscription at that step
        if(state.plan === 'starter') {
          return AccountStatus.planSubscribed;
        // in the payed plans we need a valid stripe subscription
        } else if ((state.plan === 'pro' || state.plan === 'premium') && !state.subscribed) {
          return AccountStatus.planSelected;
        } else if ((state.plan === 'pro' || state.plan === 'premium') && state.subscribed) {
          return AccountStatus.planSubscribed;
        } else {
          return null;
        }
      } else {
        return null;
      }
    }
  );

  public readonly vm$ = this.select(
    this.state$, this.accountStatus$, this.profileTypeObs$,
    (state, accountStatus, profileType) => ({ state, accountStatus, profileType }),
    {debounce: true}, // 👈 setting this selector to debounce
  );

  // for profile form
  public readonly coachStudent$ = this.select(
    (state) => {
      if(state.role) {
        if(state.role === 0) {
          return ProfileType.student;
        } else if (state.role === 1) {
          return ProfileType.coach;
        }
      } else {

      }
    }
  )

  /* ###### */
  // case for user being null
  // set allways role to -1 in this case
  readonly resetFBUser = this.updater((state: AccountState) => ({
    ...state,
    userId: null, 
    isAnonymous: true,
    email: null,
    emailVerified: false,
    role: -2,
    photoURL: 'assets/img/avatars/avataaars-default.png',
    plan: null,
    profileType: null,
    nickName: null,
    betatester: false,
    subscribed: false,
    lastSignInTime: null,
    creationTime: null,
  }));

  readonly setPlan = this.updater((state: AccountState, value: AccountState['plan']) => ({
    ...state,
    plan: value ?? null,
  }));

  readonly setBetatester = this.updater((state: AccountState, value: AccountState['betatester']) => ({
    ...state,
    betatester: value ?? false,
  }));

  readonly setNickName = this.updater((state: AccountState, value: AccountState['nickName']) => ({
    ...state,
    nickName: value ?? null,
  }));

  readonly setPhotoURL = this.updater((state: AccountState, value: AccountState['photoURL']) => ({
    ...state,
    photoURL: value ?? 'assets/img/avatars/avataaars-default.png',
  }));

  readonly setRole = this.updater((state: AccountState, value: AccountState['role']) => ({
    ...state,
    role: value ?? -2, // ...
  }));

  readonly setEmailVerified = this.updater((state: AccountState, value: AccountState['emailVerified']) => ({
    ...state,
    emailVerified: value, 
  }));

  readonly setProfileType = this.updater((state: AccountState, value: AccountState['profileType']) => ({
    ...state,
    profileType: value ?? null, 
  }));

  readonly setSubscribed = this.updater((state: AccountState, value: AccountState['subscribed']) => ({
    ...state,
    subscribed: value ?? false, 
  }));


// *********** Effects *********** //
/**
 * this checks whether the room is already up and running, e.g. when the coach left for a break...
 */

readonly getUser = this.effect((userId$: Observable<string>) => {
  return userId$
    .pipe(
      switchMap((userId) => this.db
        .object(`users/${userId}`)
        .valueChanges()
        .pipe(
          // map((val:any) => val.map(_val => ({ key: _val.key, ..._val.payload.val() }))),
          // tap(val => console.log('...', val)),
          tap({
            next: (val:any) => {
              // catch the general null case
              if(val !== null && val !== undefined) {
                // catch that there is no role defined yet
                const role = val.role ?? -1;
                this.setRole(role);
                this.setPlan(val.plan);
                this.setBetatester(val.betatester);
                this.setProfileType(val.profileType);
              } else {
                this.setRole(-1);
              }
            },
            error: (e) => console.error(e),
          }),
          /*
          tapResponse(
            (val:any) => {
              this.setRole(val.role);
              this.setPlan(val.plan);
              this.setBetatester(val.betatester);
            },
            (error) => console.log('tapResponse: ', error),
          ),
          */
          // 👇 Handle potential error within inner pipe.
          catchError(() => EMPTY),
        )
      )
    )
});

readonly getContactData = this.effect((userId$: Observable<string>) => {
  return userId$
    .pipe(
      switchMap((userId) => this.db
        .object(`profiles/${userId}/contactData`)
        .valueChanges()
        .pipe(
          // map((val:any) => val.map(_val => ({ key: _val.key, ..._val.payload.val() }))),
          // tap(val => console.log('in contactData: ', val)),
          tap({
            next: (val:any) => {
              // tap(val => console.log('out contactData: ', val))
              // this handles the null case, where we create an artificial nickname
              if(val === null || val === undefined){
                const nick = this.get().email ? this.get().email.split('@')[0] : '';
                nick ? this.setNickName(nick) : '';
              } else {
                this.setNickName(val.nickname);
                this.setPhotoURL(val.avatar);
              }
            },
            error: (e) => console.error(e),
          }),
        )
      )
    )
});

readonly getStripeSubscription = this.effect((userId$: Observable<string>) => {
  return userId$
    .pipe(
      switchMap((userId) => this.db
        .object(`stripe_customers/${userId}/customerData`)
        .valueChanges()
        .pipe(
          // tap(val => console.log('1...', val)),
          filter(val => val !== null && val !== undefined),
          map((val: any) => {
            // console.log('2...', val)
            return val.subscriptions.total_count === 0 ? false : true
          }),
          // startWith(false),
          tap({
            next: (val:any) => {
              this.setSubscribed(val);
            },
            error: (e) => console.error(e),
          }),
          // 👇 Handle potential error within inner pipe.
          catchError(() => EMPTY),
        )
      )
    )
});

// end of class
}

