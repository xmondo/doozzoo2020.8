import {
  async,
  inject,
  ComponentFixture,
  TestBed
} from '@angular/core/testing';

import { AngularFireDatabase, AngularFireDatabaseModule } from '@angular/fire/database';
import { DeltaTango } from '../delta-tango';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from '../auth.service';
import { environment } from '../../../environments/environment';
import { RouterTestingModule } from '@angular/router/testing';
import { FirebaseOptionsToken } from '@angular/fire';
import { Router } from '@angular/router';

import { TangoButtonComponent } from './tango-button.component';

describe('TangoButtonComponent', () => {
  let component: TangoButtonComponent;
  let fixture: ComponentFixture<TangoButtonComponent>;
  let button: HTMLElement;
  const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
  let router: Router;
  let auth: any;

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [ RouterTestingModule, AngularFireDatabaseModule ],
      declarations: [ TangoButtonComponent ],
      providers: [
        { provide: DeltaTango, AngularFireAuth, AngularFireDatabase },
        { provide: FirebaseOptionsToken, useValue: environment.firebase },
        { provide: Router, useValue: routerSpy },
        { provide: AuthService }
      ],
    })
    .compileComponents();

    fixture = TestBed.createComponent(TangoButtonComponent);
    component = fixture.componentInstance;
    button = fixture.nativeElement.querySelector('button');
    fixture.detectChanges();
    auth = TestBed.get(AuthService);

  });

  it('should create an instance', () => {
    expect(component).toBeTruthy();
  });

  it('should display button label "Create room"', () => {
    expect(button.textContent).toContain('Create room');
  });

  it('should go to test room', () => {
    // button.click();
    component.gotoRoom();
    router = TestBed.get(Router);
    // args passed to router.navigateByUrl() spy
    const spy = router.navigateByUrl as jasmine.Spy;
    const navArgs = spy.calls.first().args[0];
    // expecting to navigate to the deltaTango
    const token = component.token;
    expect(navArgs).toContain('/room/' + token,
      'should navigatate to test room');
  });

});
