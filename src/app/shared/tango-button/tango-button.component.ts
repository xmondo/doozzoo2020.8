import { Component, OnInit, AfterViewInit, OnDestroy, ChangeDetectorRef, Input } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { DeltaTango } from '../delta-tango';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { ControllsService } from '../controlls.service';
import { OpentokService } from '../opentok.service';


interface btnConf { 
  lg: string; 
  gtxs: string;
  xs: string;
  class: string; 
};

@Component({
  providers: [ DeltaTango ],
  selector: 'app-tango-button',
  templateUrl: './tango-button.component.html',
  styleUrls: ['./tango-button.component.scss']
})

export class TangoButtonComponent implements OnInit, OnDestroy, AfterViewInit {
  @Input() buttonConfig: string;
  user: any = null;
  isAnonymous: boolean = false;
  token: any = null;
  data: any = null;
  userSubscription: Subscription;
  btnConf: btnConf;
  camStatus: boolean;
  micStatus: boolean;
  subscriptionContainer: Subscription = new Subscription;
 
  constructor(
    public a: AuthService,
    private router: Router,
    private db: AngularFireDatabase,
    private deltaTango: DeltaTango,
    private cref: ChangeDetectorRef,
    public ots: OpentokService,
  ) {
    // ...
    /*
    const camSub = this.ots.camEmitter
      .subscribe(val => {
        console.log('tango, sync cam: ', val);
        this.camStatus = val;
      });  
    this.subscriptionContainer.add(camSub);
    const micSub = this.ots.micEmitter
      .subscribe(val => {
        console.log('tango, sync mic: ', val);
        this.micStatus = val;
      });  
    this.subscriptionContainer.add(micSub);
    */
  }

  ngOnInit() {
    
    switch(this.buttonConfig) {
      case 'center':
        this.btnConf = { lg: '30%', gtxs: '50%', xs: '95%', class: 'tangoFAB' };
        break;
      case 'right':
        this.btnConf = { lg: '30%', gtxs: '50%', xs: '95%', class: 'tangoFABRight' };
        break;
      default:
        this.btnConf = { lg: '100%', gtxs: '100%', xs: '100%', class: 'default' };
        break;
    }
    // console.log(this.btnConf);

  }

  ngAfterViewInit() {
    // subscription must happen in after view init
    // NOT in constructor or on init
    this.userSubscription = this.a.usr$
      .pipe(filter(val => val !== null))
      .subscribe(
        payload => {
          // console.log('userSubscription: ', payload);
          this.user = payload.uid;
          this.isAnonymous = payload.isAnonymous;
          this.cref.detectChanges();
        });
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
    this.subscriptionContainer.unsubscribe();
  }

  gotoRoom() {
    this.token = this.deltaTango.generate();
    this.router.navigateByUrl('/room/' + this.token);
  }

  /**
   * this function initiates the room with a given id
   */
  createTango() {
    // generate a new random token to set up new room
    this.token = this.deltaTango.generate(5);
    // no listener, but query once
    this.db.object(`/now/` + this.token)
      .query
      .once('value')
      .then(data => {
        // console.log('data: ', token, data)
        this.data = data;
        if (data) {
          // only dummy data transmitted for triggering Firebase
          const obj: Object = { 
            owner: this.user,
            locked: false,
            name: this.token, // create a room Name thats human readable 
          };
          // console.log(obj);
          this.db.object(`/now/` + this.token)
            .set(obj)
            .then(() => {
              // console.log('success, created obj: ', obj);
            })
            .catch(error => {
              console.log('set catch error', error);
            });
          // redirect to regarding room
          this.router.navigateByUrl('/room/' + this.token);
        } else {
          console.log(`FOUND`, data.key);
        }
      })
      .catch(error => {
        console.log('catch error', error);
      });
  }

}
