import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { AuthService } from '../shared/auth.service';
import { Observable, of } from 'rxjs';
import { AngularFireDatabase } from '@angular/fire/database';
import { filter, map, takeLast, mergeMap, take, takeUntil, takeWhile } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InviteResolverService {

  constructor(
    private a: AuthService,
    private db: AngularFireDatabase
    // private route: ActivatedRouteSnapshot,
  ) { }

  resolve(route: ActivatedRouteSnapshot) {
    // console.log('resolver->inviteToken: ', route.params.inviteToken);
    // ...
    const tk = route.params.inviteToken;
    let inviteLookup: any;
    if (tk) {
      inviteLookup = this.db
        .object('invites/' + tk)
        .valueChanges()
        .pipe(
          map((val: any) => {
            // console.log('1 val: ', val)
            // break with returning null
            // if (!val) { return null; } // ###
            if (val === null) { return { action: 'faultyToken' }}
            // store in local var
            const tkData = { 
              nickname: val.nickname, 
              coachId: val.uid, 
              timestamp: val.timestamp, 
              inviteToken: tk,
              initiatorId: val.initiatorId,
              userActions: val.userActions, 
            };
            return tkData;
          })
        );
    } 

    const data = {
      inviteToken: route.params.inviteToken || null,
      //userData: this.a.userData,
      user: this.a.afa.user,
      relatedTokenData: inviteLookup,
    }
    return data;
  }

// end of class
}
