import { Injectable, EventEmitter } from '@angular/core';
import * as OT from '@opentok/client';
import { environment } from '../../environments/environment';
import { DeviceDetectorService } from 'ngx-device-detector';
import { BehaviorSubject, combineLatest, fromEvent, Observable } from 'rxjs';
import { filter, distinctUntilChanged, map, debounceTime, mergeMap, combineAll } from 'rxjs/operators';
// import { AudioService } from './audio.service';
import { Router } from '@angular/router';
import { from } from 'rxjs/internal/observable/from';
import { merge } from 'lodash';
import { PublisherStoreService } from '../publisher/publisher-store.service';

/**
 * constraints.video.width = 640;
 * constraints.video.aspectRatio = { ideal: (4/3) };
 */
export interface AVConstraints {
  video: {
    // deviceId: { exact: this.devices.videoInputSelected }
    // deviceId: { ideal: <any>this.devices.videoInputSelected }
    deviceId: string | any, //{ ideal: string },
    width?: number,
    aspectRatio?: any, // = { ideal: (4/3) };
  },
  audio: {
    // deviceId: { exact: this.devices.audioInputSelected },
    // deviceId: { ideal: <any> this.devices.audioInputSelected },
    deviceId: string | any, //{ ideal: string }, // <any>this.devices.audioInputSelected,
    echoCancellation: boolean,
    noiseSuppression: boolean,
    autoGainControl: boolean
  }
}

/**
 * TODO: simplify and put all prefs in this obj???
 */
export interface sessionPrefs {
  mic: boolean,
  cam: boolean,
  resolution: '640x480' | '1280x720',
  mirrorMode: boolean,
  bitrate: 64000 | 128000 | 192000,
  headphoneMode: boolean,
  stereoMode: boolean,
  videoDeviceId?: string | any, // todo: fix any
  audioInDeviceId?: string | any,
  audioOutDeviceId?: string | any,
  echoCancellation?: boolean,
  noiseSuppression?: boolean,
  autoGainControl?: boolean
}

@Injectable({
  providedIn: 'root'
})

export class OpentokService {

  errorsEmitter: EventEmitter<any> = new EventEmitter(); // payload -> generic OT error
  streamEmitter: EventEmitter<any> = new EventEmitter(); // payload -> { action: 'default', content: ... }
  deviceEmitter: EventEmitter<any> = new EventEmitter();
  Change: EventEmitter<any> = new EventEmitter();
  controlsEmitter: EventEmitter<any> = new EventEmitter(); // payload -> { cam | mic | 50/50 }

  camEmitter: BehaviorSubject<any> = new BehaviorSubject(true);
  micEmitter: BehaviorSubject<any> = new BehaviorSubject(true);

  sessionPrefsEmitter: BehaviorSubject<sessionPrefs>; 

  // publish actual connection and session status
  connectionsEmitter: EventEmitter<any> = new EventEmitter();
  connectionsStatus$ = this.connectionsEmitter
    .pipe(
      distinctUntilChanged(),
      filter(val => val.type !== null && val.type !== undefined),
      map(val => {
        // console.log(val);
        return val.type === 'sessionConnected' ? true : false;
      })
    );

  sessionTrigger = { 
    publisher: null, 
    sessionCurrentState: 'disconnected',
    session: null
  };
  sessionEmitter: BehaviorSubject<any> = new BehaviorSubject(this.sessionTrigger);

  session: any; // OT.Session;
  sessionId: string;
  token: string;
  // browser detection
  deviceInfo: any;

  // observe device change events
  public deviceChange$: Observable<any> = fromEvent(navigator.mediaDevices, 'devicechange')
    .pipe(
      debounceTime(100),
      distinctUntilChanged(),
    )
  // provide devices data, pulled from updateDeviceList()
  // deviceListFiltered$: BehaviorSubject<any>; 
  deviceList$: BehaviorSubject<any>; 
  
  // constraints
  private constraints: AVConstraints = {
    video: {
      deviceId: 'default',
    },
    audio: {
      deviceId: 'default',
      echoCancellation: false,
      noiseSuppression: false,
      autoGainControl: false
    }
  };

  // default params as default for initialization
  private sessionPrefs: sessionPrefs = {
    mic: true,
    cam: true,
    resolution: '640x480',
    mirrorMode: false,
    bitrate: 64000,
    headphoneMode: true,
    stereoMode: false,
    videoDeviceId: 'default', // todo: fix any
    audioInDeviceId: 'default',
    audioOutDeviceId: 'default',
    echoCancellation: false,
    noiseSuppression: false,
    autoGainControl: false
  }

  // mirror Mode for video
  private mirrorMode: boolean = false;
  // stereo Mode for audio
  private stereoMode: boolean = false;
  private resolution: any = '640x480';

  // ### constructor ###
  constructor(
    private deviceService: DeviceDetectorService,
    // private ats: AudioService, 
    public router: Router,
  ) {
    // read all device Infos for reevaluation...
    this.deviceInfo = this.deviceService.getDeviceInfo();
    // read from local Storage
    this.sessionPrefs = this.getSessionPrefsStorage();
    // console.log(this.sessionPrefs)
    this.sessionPrefs.mic = this.sessionPrefs.mic ?? true;
    this.sessionPrefs.cam = this.sessionPrefs.cam ?? true;
    this.sessionPrefs.resolution = this.sessionPrefs.resolution ?? '640x480';
    this.sessionPrefs.mirrorMode = this.sessionPrefs.mirrorMode ?? false;
    this.sessionPrefs.stereoMode = this.sessionPrefs.stereoMode ?? false;
    this.sessionPrefs.bitrate = this.sessionPrefs.bitrate ?? 64000;
    this.sessionPrefs.headphoneMode = this.sessionPrefs.headphoneMode ?? true;
    this.sessionPrefs.videoDeviceId = this.sessionPrefs.videoDeviceId ?? 'default'; // this.getVideoDeviceId();
    this.sessionPrefs.audioInDeviceId = this.sessionPrefs.audioInDeviceId ?? 'default'; // this.getAudioInDeviceId();
    this.sessionPrefs.audioOutDeviceId = this.sessionPrefs.audioOutDeviceId ?? 'default';

    // get AV device Id and in case of failure set the default one
    this.constraints = {
      video: {
        deviceId: this.sessionPrefs.videoDeviceId,
      },
      audio: {
        deviceId: this.sessionPrefs.audioInDeviceId,
        echoCancellation: this.sessionPrefs.echoCancellation ?? false,
        noiseSuppression: this.sessionPrefs.noiseSuppression ?? false,
        autoGainControl: this.sessionPrefs.autoGainControl ?? false, 
      }
    };

    // init the observable with proper data
    this.sessionPrefsEmitter = new BehaviorSubject(this.sessionPrefs);

    // 
    this.updateAsyncSessionPrefs()

    //..
    this.initUpdateDeviceList();
  }

  // we must place the async call outside the constructor, otherwise they will not be resolved
  async updateAsyncSessionPrefs(){
    this.sessionPrefs.videoDeviceId = await this.getVideoDeviceId();
    this.sessionPrefs.audioInDeviceId = await this.getAudioInDeviceId();
    // ISSUE: this triggers too fast an update for the GUM processes  
    // this.sessionPrefsEmitter.next(this.sessionPrefs); 
    // update in constrints as well 
    this.constraints.video.deviceId = this.sessionPrefs.videoDeviceId;
    this.constraints.audio.deviceId = this.sessionPrefs.audioInDeviceId;
  }

  // initial pull
  async initUpdateDeviceList() {
    // const dl = await this.updateDeviceListFiltered('videoinput'); // filtered
    const dl = await this.updateDeviceList(); // all device kinds
    // this.deviceListFiltered$ = new BehaviorSubject(dl);
    this.deviceList$ = new BehaviorSubject(dl);
  }

  // use this to refresh the status, e.g. by triggering it with deviceChange$
  async refreshDeviceList() {
    console.log('do refresh....')
    // const dl = await this.updateDeviceListFiltered('videoinput'); // filtered
    const dl = await this.updateDeviceList(); // all device kinds // all
    // this.deviceListFiltered$.next(dl);
    this.deviceList$.next(dl);
  }

  // ???
  getAudioMode() {
    let headphoneMode = true;
    if (this.constraints.audio.echoCancellation === false && this.constraints.audio.autoGainControl === false) {
      headphoneMode = true;
    } else {
      headphoneMode = false;
    }
    return headphoneMode;
  }

  setAudioMode(mode: boolean) {
    this.sessionPrefs.headphoneMode = mode;
    this.setSessionPrefs();
  }

  getOT() {
    return OT;
  }
  // OT.NONE = 0, OT.ERROR = 1, OT.WARN = 2, OT.INFO = 3, OT.LOG = 4, OT.DEBUG = 5, OT.SPAM = 6
  setOTLogLevel(level:number){
    OT.setLogLevel(level);
    // Push error to generic error handling component
  }

  initSession(sessionId, token) {
    this.session = this.getOT().initSession(environment.opentok.apiKey, sessionId);
    this.token = token;
    return Promise.resolve(this.session);
  }

  connect() {
    return new Promise((resolve, reject) => {
      this.session.connect(this.token, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve(this.session);
        }
      });
    });
  }

/*
  checkScreensharing() {
    const browser = this.deviceInfo.browser;
    return new Promise((resolve, reject) => {
      OT.checkScreenSharingCapability(function(response) {
        if (response === null || response === undefined){
          reject('error');
        // for firefox skip the rest
        } else if (!response.supported) {
          // This browser does not support screen sharing.
          console.log('Ot screensharing: not supported', response);
          resolve('hide');
        } else {
          // init
          // console.log('Ot screensharing: init');
          resolve('show');
        }
      });
    });
  }
*/

  stopGUM() {
    return navigator.mediaDevices
      .getUserMedia(this.constraints)
      .then(stream => {
        stream.getVideoTracks().forEach(item => item.stop());
        stream.getVideoTracks().forEach(item => item.stop());
      });
  }

  initOTExeptionhandling() {
    // OT.addEventListener('exception', exceptionHandler);
    function exceptionHandler(event) {
      alert("exception event. \n  code == " + event.code + "\n  message == " + event.message);
    }
  }

  // ### constraints handling START ###
  getConstraints() {
    console.log('constraints: ', this.constraints)
    return this.constraints;
  }
  setConstraints(constraints) {
    // console.log('setConstraints: ', constraints)
    this.constraints = constraints;
    this.setSessionPrefs();
  }

  setAudioConstraints(audioConstraints) {
    // console.log('setAudioConstraints: ', audioConstraints)
    this.constraints.audio = audioConstraints;
    this.sessionPrefs.audioInDeviceId = audioConstraints.deviceId;
    this.sessionPrefs.echoCancellation = audioConstraints.echoCancellation;
    this.sessionPrefs.noiseSuppression = audioConstraints.noiseSuppression;
    this.sessionPrefs.autoGainControl = audioConstraints.autoGainControl; 
    this.setSessionPrefs();
  }

  // Init function
  // gets config, if not available, set it with default data
  getSessionPrefsStorage() {
    // TODO: design more robust, in case of errors
    try {
      // can be null || undefined
      const conf = JSON.parse(localStorage.getItem('sessionPrefs'));
      // ... or an empty object
      // catch both cases and set defaults
      if(conf) {
        if (Object.keys(conf).length !== 0 && conf.constructor === Object) {
          this.sessionPrefs = conf;
        } else {
          this.setSessionPrefs();
        } 
      } else {
        this.setSessionPrefs();
      }
      
    } catch (error) {
      console.log('getSessionPrefs->error: ', error);
      // in case of errors, e.g. due to corrupted storage, set defaults
      // this.setConstraints(this.constraints);  
    }
    return this.sessionPrefs;    
  }

  // simple getter
  getSessionPrefs(){
    return this.sessionPrefs;
  }

  /** 
   * provides device to OT publisher init
   * possible values: deviceIds | true
   * check whether the actual deviceId is still valid and existing,
   * otherwise return "true", for generic video/audio
   */
  async getVideoDeviceId() {
    // we must await them here, otherwise not resolved
    const current = await this.sessionPrefs.videoDeviceId;
    try {
      const availableDevices = await navigator.mediaDevices
        .enumerateDevices();
      const isAvailable = availableDevices
        .filter(item => item.kind === 'videoinput')
        .some(item => item.deviceId === current);
      // console.log('getVideoDeviceId->available?, current: ', isAvailable, current);

      // repair and set the first device as default
      if(!isAvailable) {
        const firstDevice = availableDevices
          .find(item => item.kind === 'videoinput');
          this.sessionPrefs.videoDeviceId = firstDevice.deviceId ?? 'default';  
        this.setSessionPrefs();
      }
      // return the result of the operation
      return isAvailable ? current : true;
    } catch (error) {
      console.log('getVideoDeviceId->error: ', error)
      return true; 
    } 
  }
  async getAudioInDeviceId() {
    // we must await them here, otherwise not resolved
    const current = await this.sessionPrefs.audioInDeviceId;
    try {
      const availableDevices = await this.updateDeviceListFiltered('audioinput')
      const isAvailable = availableDevices.some(item => item.deviceId === current);
      //console.log('getAudioInDeviceId->available?, current: ', isAvailable, current);

      // repair and set the first device as default
      if(!isAvailable) {
        const firstDevice = this.sessionPrefs.audioInDeviceId = availableDevices[0].deviceId ?? 'default';  
        this.setSessionPrefs();
      }
      // return the result of the operation
      return isAvailable ? current : true;
    } catch (error) {
      console.log('getAudioDeviceId->error: ', error)
      return true; 
    } 
  }
  
  // can be used from outside of the class as setter
  setSessionPrefs(sessionPrefs?: sessionPrefs) {
    sessionPrefs = sessionPrefs || this.sessionPrefs;

    // direty trick, to be enhanced asap
    this.sessionPrefs.echoCancellation = sessionPrefs.echoCancellation || false;
    this.sessionPrefs.noiseSuppression = sessionPrefs.noiseSuppression || false;
    this.sessionPrefs.autoGainControl = sessionPrefs.autoGainControl || false;

    const conf = JSON.stringify(sessionPrefs);
    // console.log('store sessionPrefs: ', conf);
    localStorage.setItem('sessionPrefs', conf);
    this.sessionPrefsEmitter.next(sessionPrefs);
  }

  // ### constraints handling END ###

  /* ### handle other prefs ### */
  getMirrorModePrefs() {
    const sessionPrefs = JSON.parse(localStorage.getItem('sessionPrefs'));
    this.sessionPrefs.mirrorMode = sessionPrefs.mirrorMode ?? false;
    return this.sessionPrefs.mirrorMode;
  }

  getMirrorMode() {
    return this.sessionPrefs.mirrorMode;
  }

  setMirrorMode(mirrorMode) {
    this.sessionPrefs.mirrorMode = mirrorMode;
    localStorage.setItem('sessionPrefs', JSON.stringify(this.sessionPrefs));
    this.sessionPrefsEmitter.next(this.sessionPrefs);
  }

  toggleMirrorMode() {
    this.sessionPrefs.mirrorMode = !this.sessionPrefs.mirrorMode;
    this.setMirrorMode(this.sessionPrefs.mirrorMode);
  }

  /* handle stereo Mode prefs */
  /* handle mirror mode prefs */
  getStereoModePrefs() {
    const sessionPrefs = JSON.parse(localStorage.getItem('sessionPrefs'));
    this.sessionPrefs.stereoMode = sessionPrefs.stereoMode ?? false;
    return this.sessionPrefs.stereoMode;
  }

  getStereoMode() {
    return this.sessionPrefs.stereoMode;
  }

  setStereoMode(stereoMode) {
    this.sessionPrefs.stereoMode = stereoMode;
    localStorage.setItem('sessionPrefs', JSON.stringify(this.sessionPrefs));
    this.sessionPrefsEmitter.next(this.sessionPrefs);
  }

  toggleStereoMode() {
    this.sessionPrefs.stereoMode = !this.sessionPrefs.stereoMode;
    this.setStereoMode(this.sessionPrefs.stereoMode);
  }

  // handle resolution
  getResolutionPrefs(): '640x480' | '1280x720' {
    const sessionPrefs = JSON.parse(localStorage.getItem('sessionPrefs'));
    this.resolution = sessionPrefs.resolution ?? '640x480';
    return this.sessionPrefs.resolution;
  }

  getResolution() {
    return this.sessionPrefs.resolution;
  }

  setResolution(resolution) {
    this.sessionPrefs.resolution = resolution;
    localStorage.setItem('sessionPrefs', JSON.stringify(this.sessionPrefs));
    this.sessionPrefsEmitter.next(this.sessionPrefs);
  }

  toggleResolution() {
    // console.log(this.resolution)
    this.sessionPrefs.resolution = this.sessionPrefs.resolution === '640x480' ? '1280x720' : '640x480';
    this.setResolution(this.sessionPrefs.resolution);
  }

  async updateDeviceListFiltered(mediaDeviceKind: 'videoinput' | 'audioinput' | 'audiooutput') {
    // TODO: correct typings
    const gotDevices:any = await navigator // should be -> MediaDeviceInfo[]
      .mediaDevices
      .enumerateDevices()!
      .catch(error => {
        console.log('updateDeviceListFiltered error: ', error);
      });
    // console.log(gotDevices)
    const mediaDeviceArr = <MediaDeviceInfo[]>gotDevices.filter(item => {
      return item.kind === mediaDeviceKind;
    });
    return mediaDeviceArr;
  }

  async updateDeviceList() {
    // TODO: correct typings
    const gotDevices: MediaDeviceInfo[] | void = await navigator // should be -> MediaDeviceInfo[]
      .mediaDevices
      .enumerateDevices()!
      .catch(error => {
        console.log('updateDeviceListFiltered error: ', error);
      });
    // console.log(gotDevices)
    return gotDevices;
  }

  async deviceAvailable(deviceId: string) {
    const availableDevices = await navigator.mediaDevices
      .enumerateDevices();
    // console.log('availableDevices: ', availableDevices.filter(item => item.kind === 'videoinput' || item.kind === 'audioinput'));
    return availableDevices
      .filter(item => item.kind === 'videoinput' || item.kind === 'audioinput')
      .some(item => item.deviceId === deviceId);
  }

// class end
}
