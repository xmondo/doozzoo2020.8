import { Component, Input, OnInit } from '@angular/core';
import { UserData } from '../auth.service';

@Component({
  selector: 'app-anonymous-hint',
  templateUrl: './anonymous-hint.component.html',
  styleUrls: ['./anonymous-hint.component.scss']
})
export class AnonymousHintComponent implements OnInit {
  @Input() user: UserData;
  
  constructor() { }

  ngOnInit(): void {
  }

}
