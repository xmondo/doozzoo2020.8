import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnonymousHintComponent } from './anonymous-hint.component';

describe('AnonymousHintComponent', () => {
  let component: AnonymousHintComponent;
  let fixture: ComponentFixture<AnonymousHintComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnonymousHintComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnonymousHintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
