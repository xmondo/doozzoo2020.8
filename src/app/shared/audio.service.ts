import { Injectable, EventEmitter, NgZone } from '@angular/core';
import { from, Observable } from 'rxjs';
import * as Tone from 'tone';
import { Channelstrip } from './channelstrip';

interface GainObject {
  name: string;
  id: string;
  gainObj: AudioNode;
}

@Injectable({
  providedIn: 'root'
})
export class AudioService {
  recorderEmitter: EventEmitter<any> = new EventEmitter(); // payload -> { action: 'updateClipArray' }
  // old
  gainNodes = [];
  // new
  gainObjects: Array<Channelstrip> = [];

  ctx: AudioContext = Tone.context;
  masterDelay: Tone.delay; 
  remoteStatus: boolean = false;
  headphoneMode: boolean = true;

  // set defaults for Channelstrips
  // used for to persist these values as well
  globalChannelstripStatus = {
    toMaster: true,
    toClass: true,
    muted: false,
  };

  // globalMediaStream = new MediaStream();
  // globalMediaStreamSource: MediaStreamAudioSourceNode; //  = this.ctx.createMediaStreamSource(this.globalMediaStream);

  // TODO: do not create here, but in publisher, otherwise its not active any more when restartimg
  // globalMediaStreamDestination = this.ctx.createMediaStreamDestination();
  globalMediaStreamDestination: MediaStreamAudioDestinationNode;

  /**
   * possible presets for audio bitrate
   * 64000 | 128000 | 192000
   */
  audioBitrateArr: Array<64000 | 128000 | 192000> = [64000, 128000, 192000];
  private audioBitrate: 64000 | 128000 | 192000;

  constructor (
    private zone: NgZone,
  ) {
    // TODO: coandidate for refactoring, this will break in later versions
    // init global AudioContext as Tone.context
    this.ctx = Tone.context;

    // TODO: is it needed to initialize already here?
    // master delay for compensation of local audio nodes vs. mic
    this.initMasterDelay();

    const br = localStorage.getItem('doozzooAudioBitrate');
    if(br !== null && br !== undefined){
      this.audioBitrate = <64000 | 128000 | 192000>parseInt(br);  
    } else {
      // this.audioBitrate = this.audioBitrateArr[1]; // 128000
      this.setBitrate(0);
    }
    // console.log('default audio context (Tone): ', Tone.context, Tone.context._context.sampleRate);

  }

  setBitrate(index: number){
    this.audioBitrate = this.audioBitrateArr[index];
    localStorage.setItem('doozzooAudioBitrate', JSON.stringify(this.audioBitrate));
  }

  getBitrate(): 64000 | 128000 | 192000 {
    // as anything could be the value retrieved from local storage,
    // we explicitly make sure that the current value is in the LOV of audio bitrates
    if(this.audioBitrateArr.some(item => item === this.audioBitrate)){
      return this.audioBitrate; 
    } else {
      // otherwise we set and return the default value [1]
      this.setBitrate(0);
      return this.audioBitrateArr[0]; 
    } 
  }

  setHeadphoneMode(mode: boolean) {
    this.headphoneMode = mode || true;
  }

  getHeadphoneMode() {
    return this.headphoneMode;
  }

  getSystemSampleRate() {
    const sr = Tone.context._context.sampleRate;
    return sr;
  }

  initMasterDelay() {
    // in case there is allready one, remove
    if(this.masterDelay) {
      this.masterDelay.dispose();
    }

    // create a new one
    this.masterDelay = new Tone.Delay(0, 1);
    const tmp = localStorage.getItem('masterDelay');
    if (tmp) {
      this.masterDelay.delayTime.value = parseFloat(tmp);
    } else {
      this.masterDelay.delayTime.value = 0.1;
    }
  }

  // old
  addGainNode(name, gainObj) {

    const index = this.gainNodes.findIndex(item => { 
      return item.name === name;
    });
    // if obj already exists, remove it befor adding the current one
    if (index !== -1) {
    // audio nodes must be rewired, therefore 1. purged and 2. reconnected
    // console.log('+++++ reset gain node: ', name)
      this.gainNodes.splice(index, 1 , { name: name, gainObj: gainObj });
      // this.gainNodes.push({ name: name, gainObj: gainObj });
    // add new audio node 
    } else {
      this.gainNodes.push({ name: name, gainObj: gainObj });
    }
    // console.log('gainNodes after adding: ', this.gainNodes);

  }

  removeGainNode(name) {
    const index = this.gainNodes.findIndex(item => {
      return item.name === name;
    });
    // if obj exists, remove it
    if (index !== -1) {
      this.gainNodes.splice(index);
    }
  }

  /**
   * 
   * @param name name of channelstrip
   * @param level initial level of volume
   * @param toMaster connect to local monitor
   * @param toClass connect RTC channel
   * @param muted start muted
   * @param id gives the possibility to create
   * a dedicated channelstrip for different sources
   */
  createChannelstrip(
    name,
    level?: number,
    toMaster?: boolean,
    toClass?: boolean,
    muted?: boolean,
    id?: string,
  ) {
    /**
     * first check if a cs with the same id already exists
     * in this case return existing
     */
    const existingChannel = this.findChannelstripId(id);
    if(existingChannel !== null) {
      // return existingChannel;
      existingChannel.dispose();
      return new Channelstrip(name, this, level, toMaster, toClass, muted, id, this.zone);
    } else {
      return new Channelstrip(name, this, level, toMaster, toClass, muted, id, this.zone);
    } 
  }

  findChannelstripId(channelstripId: string): Channelstrip | null {
    const index = this.gainObjects.findIndex(item => item.id === channelstripId);
    if (index !== -1) {
      return this.gainObjects[index];
    } else {
      return null;
    }
  }

  findChannelstripName(channelstripName: string): Channelstrip | null {
    const index = this.gainObjects.findIndex(item => item.name === channelstripName);
    if (index !== -1) {
      return this.gainObjects[index];
    } else {
      return null;
    }
  }

  /**
   * iterated through all chanelstrips and appys settings
   * not for Microphone!!
   *
   * @param toMaster connect to local monitor
   * @param toClass connect RTC channel
   * @param muted set muted
   */
  switchChannelstripAll(
    toMaster: boolean,
    toClass: boolean,
    muted: boolean
  ) {
    this.gainObjects.forEach((item: Channelstrip) => {
      if (item.id !== 'microphoneId') { 
        item.switchAll(toMaster, toClass, muted); 
      }
    });
    // persits changed status for newly created gainnodes
    this.globalChannelstripStatus = {
      toMaster: toMaster,
      toClass: toClass,
      muted: muted,
    };
  }

  getMasterDelay() {
    return this.masterDelay.delayTime.value;
  }

  setMasterDelay(value: number) {
    this.masterDelay.delayTime.value = value;
    // this.storeMasterDelay(); 
    localStorage.setItem('masterDelay', value.toString()); 
  }

  setMasterDelayFactor(factor: number) {
    const fc = parseFloat(factor.toFixed(3))
    this.masterDelay.delayTime.value = this.masterDelay.delayTime.value + fc;
    // this.storeMasterDelay(); 
    localStorage.setItem('masterDelay', this.masterDelay.delayTime.value.toString()); 
  }

  storeMasterDelay() {
    const md = this.getMasterDelay();
    localStorage.setItem('masterDelay', md.toString());
  }

  initGlobalAudioNodes() {
    // rewire global audio nodes
    // TODO: responsible for doubling the audio channels?
    this.initMasterDelay();
    this.globalMediaStreamDestination = this.ctx.createMediaStreamDestination();
    this.masterDelay.connect(this.globalMediaStreamDestination);
  }

  /**
   * expects as input a MediaStream stream obj
   * returns an audioTrack
   */
  rewireAudioTrack(stream){
    // ### audio plugin START ###
    // bugfix due to security constraints
    // https://developers.google.com/web/updates/2017/09/autoplay-policy-changes#webaudio
    this.ctx.resume()
      .then(() => {
        //console.log('rewireAudioTrack ctx resumed');
      })
      .catch(error => console.log('ctx resume error: ', error));

    const myLocalMic = this.ctx.createMediaStreamSource(stream);
    // gainnode for mic
    // new mixer approach
    const micGain = this.createChannelstrip(
      'Microphone',
      -5, 
      false, 
      false, 
      false,
      'microphoneId'
    );
    myLocalMic.connect(micGain.gainObj);
    micGain.gainObj.connect(this.globalMediaStreamDestination);

    // extract audio channel as Media Track for handing over to OT.initPublisher
    let destAudioTrack = this.globalMediaStreamDestination.stream.getAudioTracks()[0];
    
    // fallback in case the audio track was ended
    // TODO: still necessary??
    if (destAudioTrack.readyState !== 'live') {
      // TODO: BUG??? was destAudioTrack = stream.getVideoTracks()[0];
      destAudioTrack = stream.getAudioTracks()[0];
    }
    return destAudioTrack;
    // ### audio plugin END ###
  }

  // generic audio transformation functions
  blobToDataUrl(blob) {
    // promise
    return new Promise((resolve, reject) => {
        const fileReader = new FileReader();
        fileReader.onload = e => {
            // return the dataUrl
            // resolve(e.target.result); // does not work in typeScript
            resolve(fileReader.result);
        }
        fileReader.onerror = error => {
            reject(error);
        }
        fileReader.readAsDataURL(blob);
    });
  }

  blobToDecodedArrayBuffer(blob, audioContext) {
    // promise
    return new Promise((resolve, reject) => {
        const fileReader = new FileReader();
        fileReader.onload = e => {
            // console.log('fr', fileReader.result);
            audioContext.decodeAudioData(fileReader.result)
              .then(buffer => resolve(buffer));
        };
        fileReader.onerror = error => {
            reject(error);
        };
        fileReader.readAsArrayBuffer(blob);
    });
  }

  trimDecodedBuffer(decodedBuffer, start, end, audioContext){
    const buffer = decodedBuffer;
    const ctx = audioContext || Tone.context;
    // find out length of buffer that was previously loaded for wavesurfer
    const bufferLength = buffer.length;
    const nrOfChannels = buffer.numberOfChannels;
    const newLength = end - start; // in seconds
    const newFrom = start; // in seconds
    const length = Math.floor(ctx.sampleRate * newLength);
    const from = Math.floor(ctx.sampleRate * newFrom);
    // console.log('bufferLength, nrOfChannels, from, length -> ', bufferLength, nrOfChannels, from, length);
    // create empty audioBuffer
    // length of incomming buffer
    const myAudioBuffer = ctx.createBuffer(nrOfChannels, length, ctx.sampleRate);
    // extract channel data from incomming buffer
    // iterate through original channels
    const myChannelData = [];
    for (let i = 0; i < nrOfChannels; i++) {
        // trim from beginning, newFrom = 0 or
        // from within the audio file, newFrom = * < bufferlength - newFrom
        // get the channel data and cut out the subarray in one operation
        // assign to the vars channel position
        myChannelData[i] = buffer.getChannelData(i).subarray(from, from + length); // index, howmany, newData
        // copy to empty audioBuffer
        // repeat for all channels
        // channel nr start with 0!!
        myAudioBuffer.copyToChannel(myChannelData[i], i, 0);
    }
    return myAudioBuffer;
  }

  // 
  doPatchBay(status: boolean, isSender: boolean) {
    // Headphone-Mode
    /// console.log('false, false, true')
    if (this.headphoneMode) {
      // coach
      if (isSender) {
        // remote ON
        if (status) {
          // mon/class/mute off/off/on
          this.switchChannelstripAll(false, false, true);
        // remote OFF
        } else {
          // mon/class/mute on/on/off
          this.switchChannelstripAll(true, true, false);
        }
      // student
      } else {
        // remote ON
        if (status) {
          // mon/class/mute on/on/off
          this.switchChannelstripAll(true, true, false);
        // remote OFF
        } else {
          // mon/class/mute off/off/on
          this.switchChannelstripAll(false, false, true);
        }
      }
    // Speaker-Mode
    } else {
      // coach
      if (isSender) {
        // remote ON
        if (status) {
          // mon/class/mute off/off/on
          this.switchChannelstripAll(false, false, true);
        // remote OFF
        } else {
          // mon/class/mute on/off/off
          this.switchChannelstripAll(true, false, false);
        }
      // student
      } else {
        // remote ON
        if (status) {
          // mon/class/mute on/off/off
          this.switchChannelstripAll(true, false, false);

        // remote OFF
        } else {
          // mon/class/mute off/off/on
          this.switchChannelstripAll(false, false, true);
        }
      }
    }
    console.log('audioService->doPatchBay: isSender, status, headphoneMode, gainObjects', isSender, status, this.gainObjects);
  }

}
