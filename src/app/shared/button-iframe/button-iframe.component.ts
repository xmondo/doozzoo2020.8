import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-button-iframe',
  template: `
    <button
      class="support-button"
      mat-raised-button
      color="primary"
      (click)="open()">
      {{'general.support' | translate}}
    </button>
  `,
  styleUrls: ['./button-iframe.component.scss']
})
export class ButtonIframeComponent {
  @Input() url: string;
  constructor(
    public dialog: MatDialog,
    private sanitizer: DomSanitizer,
  ) {

  }

  open() {
    const dialogRef = this.dialog.open(DialogIframeComponent, { data: { url: this.sanitizer.bypassSecurityTrustResourceUrl(this.url)}});
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

}

@Component({
  selector: 'dialog-content-example-dialog',
  template: `
    <div mat-dialog-title>
      <button class="dialog-close" mat-icon-button mat-dialog-close tabindex="-1">
        <mat-icon>close</mat-icon>
      </button>
    </div>
    <div class="dialog-content">
        <iframe [src]="data.url" frameborder="0" allow-same-origin allowfullscreen></iframe>
    </div>
  `,
  styleUrls: ['./button-iframe.component.scss'],
})
export class DialogIframeComponent {
  constructor (
    @Inject(MAT_DIALOG_DATA) public data,
    public dialogRef: MatDialogRef<DialogIframeComponent>,
  ) {}

  onClose() {

  }
}
