import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonIframeComponent } from './button-iframe.component';

describe('ButtonIframeComponent', () => {
  let component: ButtonIframeComponent;
  let fixture: ComponentFixture<ButtonIframeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ButtonIframeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonIframeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
