import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { ActivatedRouteSnapshot } from '@angular/router';
import { map, delay } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';


@Injectable({
  providedIn: 'root'
})
export class TestdriveResolverService {
  foo = 'hallo';
  constructor(
    private ats: AuthService,
    public afa: AngularFireAuth,
    // private route: ActivatedRouteSnapshot,
  ) { 
    this.ats.usr$.subscribe(val => this.foo = val.isAnonymous);
  }

  resolve(route: ActivatedRouteSnapshot) {
    // TODO: resolves firebaseAuth before routing
    return this.foo;
  }
}
