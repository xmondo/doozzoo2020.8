import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'urlify'
})
export class UrlifyPipe implements PipeTransform {
  transform(input: string, maxlength: number = 20): string {
    const urlRegex = /(https?:\/\/[^\s]+)/g;
    let tmp = input.replace(urlRegex, function(url) {
        // open link in new window
        let label = url;
        if(url.length > maxlength) {
          label = label.slice(0, maxlength) + '...';
        }
        return '<a target="_blank" title="' + url + '" href="' + url + '">' + label + '</a>';
    })
    // or alternatively
    // return text.replace(urlRegex, '<a href="$1">$1</a>')
    // console.log('UrlifyPipe: ', tmp);
    return tmp;
  }
}   