import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-language',
  templateUrl: './language.component.html',
  styleUrls: ['./language.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LanguageComponent implements OnInit {

  langArr: Array<any>;

  // ['en', 'de', 'nl', 'it', 'fr', 'es']
  languages: Array<any> = [
    {
      short: 'en',
      label: 'English',
      img: ''
    },
    {
      short: 'de',
      label: 'Deutsch',
      img: ''
    },
    {
      short: 'nl',
      label: 'Nederlands',
      img: ''
    },
    {
      short: 'it',
      label: 'Italiano',
      img: ''
    },
    {
      short: 'fr',
      label: 'Français',
      img: ''
    },
    {
      short: 'es',
      label: 'Español',
      img: ''
    },
    {
      short: 'ja',
      label: '日本語 | Japanese',
      img: ''
    },
    {
      short: 'zh',
      label: '中文 | Chinese',
      img: ''
    },
  ]

  constructor(
    public translate: TranslateService,
  ) { }

  ngOnInit() {
    this.langArr = this.translate.getLangs();
  }

  toggleLanguage() {
    const langArr = this.translate.getLangs();
    console.log(langArr);
    let lang = this.translate.currentLang;
    // this.translate.setDefaultLang('en');
    let current: number = langArr.findIndex(item => item === lang);
    if(current < langArr.length - 1) {
      current++
      lang = langArr[current];
    } else {
      lang = langArr[0];
    } 
    console.log('lang: ', lang, current);
    this.translate.use(lang);  
  }

  switchToLang(lang){
    //
    console.log(lang);
    this.translate.use(lang);
  }

}
