import { TestBed } from '@angular/core/testing';

import { InviteResolverService } from './invite-resolver.service';

describe('InviteResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InviteResolverService = TestBed.get(InviteResolverService);
    expect(service).toBeTruthy();
  });
});
