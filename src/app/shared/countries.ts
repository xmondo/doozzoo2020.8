import { Injectable } from "@angular/core";

@Injectable()
export class Countries {
    countries: any;
    constructor() {
        this.countries = [
            ['Austria', 'AT'],
            ['Belgium', 'BE'],
            ['Bulgaria', 'BU'],
            ['Croatia', 'HR'],
            ['Cyprus', 'CY'],
            ['Czech Republic', 'CZ'],
            ['Denmark', 'DK'],
            ['Estonia', 'ES'],
            ['Finland', 'FI'],
            ['France', 'FR'],
            ['Germany', 'DE'],
            ['Greece', 'GR'],
            ['Hungary', 'HU'],
            ['Ireland', 'IR'],
            ['Italy', 'IT'],
            ['Latvia', 'LA'],
            ['Lithuania', 'LI'],
            ['Luxembourg', 'LU'],
            ['Malta', 'MT'],
            ['Netherlands', 'NL'],
            ['Poland', 'PL'],
            ['Portugal', 'PT'],
            ['Romania', 'RO'],
            ['Slovakia', 'SK'],
            ['Slovenia', 'SI'],
            ['Spain', 'ES'],
            ['Sweden', 'SV'],
            ['United Kingdom', 'UK'],
            ['United States of America', 'US'],
            ['South Africa', 'ZA'],
            ['Other', 'NN'],
        ];
    }
}
