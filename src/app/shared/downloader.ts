import { Injectable } from "@angular/core";


@Injectable({
    providedIn: 'root'
})

export class Downloader {
    // expects Blob as data
    saveData(data, fileName, mimeType) {
        const url = window.URL.createObjectURL(data);
        const a = <any> document.createElement('a');
        const el = <NodeList> document.querySelectorAll('body');
        const extension = this.parseExtension(fileName, mimeType)
        // console.log('el: ', el[0]);
        el[0].appendChild(a);
        a.style.display = 'none';
        a.href = url;
        a.setAttribute('download', fileName + extension);
        a.click();
        window.URL.revokeObjectURL(url);
    }

    /** if filename has extension already, keep it
     * otherwise derive from mimeType
     * map audio/mpeg -> .mp3
     */
    parseExtension(fileName, mimeType) {
        let extension = '.';
        if(fileName.split('.').length > 1) {
            // in this case we keep the existing extension
            // return extension + fileName.split('.')[1];
            return '';
        } else {
            const ext = mimeType.split('/')[1];
            if(ext === 'mpeg'){
                return extension + 'mp3';
            } else {
                return extension + ext;
            }
        }
    }

    downloadUrl(url, filename, mimeType) {
        const self = this;
        return new Promise((resolve, reject) => {
            const request = new XMLHttpRequest();
            request.open('GET', url, true);
            request.responseType = 'blob';
            request.onload = () => {
                self.saveData(request.response, filename, mimeType);
                resolve({
                    status: 'success', 
                    message: 'Download was succesful'
                });
            };
            request.send();

            request.onerror = error => {
                reject({
                    status: 'error',
                    message: 'Download failed. ',
                    error: error
                });
            };
        });
    }

}
