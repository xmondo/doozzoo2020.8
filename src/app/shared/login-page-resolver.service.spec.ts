import { TestBed } from '@angular/core/testing';

import { LoginPageResolverService } from './login-page-resolver.service';

describe('LoginPageResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoginPageResolverService = TestBed.get(LoginPageResolverService);
    expect(service).toBeTruthy();
  });
});
