// wo kommt das her?????
import { Injectable } from "@angular/core";
@Injectable()

export class Avatar {
    avatarArr = [
        'assets/img/avatars/avataaars-default.png',
        'assets/img/avatars/avataaars_0.svg',
        'assets/img/avatars/avataaars_1.svg',
        'assets/img/avatars/avataaars_2.svg',
        'assets/img/avatars/avataaars_3.svg',
        'assets/img/avatars/avataaars_4.svg',
        'assets/img/avatars/avataaars_5.svg',
        'assets/img/avatars/avataaars_6.svg',
        'assets/img/avatars/avataaars_7.svg',
        'assets/img/avatars/avataaars_8.svg',
        'assets/img/avatars/avataaars_9.svg',
        'assets/img/avatars/avataaars_10.svg',
        'assets/img/avatars/avataaars_11.svg',
        'assets/img/avatars/avataaars_12.svg',
        'assets/img/avatars/avataaars_13.svg',
        'assets/img/avatars/avataaars_14.svg',
        'assets/img/avatars/avataaars_15.svg',
        'assets/img/avatars/avataaars_16.svg',
        'assets/img/avatars/avataaars_17.svg',
        'assets/img/avatars/avataaars_18.svg',
        'assets/img/avatars/avataaars_19.svg',
        'assets/img/avatars/avataaars_20.svg',
        'assets/img/avatars/avataaars_21.svg',
        'assets/img/avatars/avataaars_22.svg',
        'assets/img/avatars/avataaars_23.svg',
        'assets/img/avatars/avataaars_24.svg',
        'assets/img/avatars/avataaars_25.svg',
        'assets/img/avatars/avataaars_26.svg',
        'assets/img/avatars/avataaars_27.svg',
        'assets/img/avatars/avataaars_28.svg',
        'assets/img/avatars/avataaars_29.svg',
        'assets/img/avatars/avataaars_30.svg',
        'assets/img/avatars/avataaars_31.svg',
        'assets/img/avatars/avataaars_32.svg',
        'assets/img/avatars/avataaars_33.svg',
        'assets/img/avatars/avataaars_34.svg',
        'assets/img/avatars/avataaars_35.svg',
        'assets/img/avatars/avataaars_36.svg',
        'assets/img/avatars/avataaars_37.svg',
        'assets/img/avatars/avataaars_38.svg',
        'assets/img/avatars/avataaars_39.svg',
        'assets/img/avatars/avataaars_40.svg',
        'assets/img/avatars/avataaars_41.svg',
        'assets/img/avatars/avataaars_42.svg',
        'assets/img/avatars/avataaars_43.svg',
        'assets/img/avatars/avataaars_44.svg',
        'assets/img/avatars/avataaars_45.svg',
        'assets/img/avatars/avataaars_46.svg',
        'assets/img/avatars/avataaars_47.svg',
        'assets/img/avatars/avataaars_48.svg',
        'assets/img/avatars/avataaars_49.svg',
        'assets/img/avatars/avataaars_50.svg',
    ];

        // ### https://getavataaars.com/ ###
        /*
        'https://avataaars.io/?avatarStyle=Circle&topType=LongHairStraight&accessoriesType=Blank&hairColor=BrownDark&facialHairType=Blank&clotheType=BlazerShirt&eyeType=Default&eyebrowType=Default&mouthType=Default&skinColor=Light',
        'https://avataaars.io/?avatarStyle=Circle&topType=ShortHairShortCurly&accessoriesType=Prescription01&hairColor=Black&facialHairType=Blank&clotheType=ShirtVNeck&clotheColor=Gray01&eyeType=Cry&eyebrowType=UpDown&mouthType=Smile&skinColor=Yellow',
        'https://avataaars.io/?avatarStyle=Circle&topType=ShortHairFrizzle&accessoriesType=Blank&hairColor=Blonde&facialHairType=MoustacheMagnum&facialHairColor=BrownDark&clotheType=Overall&clotheColor=Red&eyeType=Cry&eyebrowType=SadConcerned&mouthType=Serious&skinColor=Pale',
        'https://avataaars.io/?avatarStyle=Circle&topType=ShortHairShortFlat&accessoriesType=Kurt&hatColor=Red&hairColor=Black&facialHairType=Blank&clotheType=GraphicShirt&clotheColor=Blue02&graphicType=Skull&eyeType=Side&eyebrowType=UpDown&mouthType=Sad&skinColor=Black',
        'https://avataaars.io/?avatarStyle=Circle&topType=LongHairStraightStrand&accessoriesType=Blank&hairColor=BrownDark&facialHairType=BeardMedium&facialHairColor=BrownDark&clotheType=ShirtVNeck&clotheColor=Pink&eyeType=EyeRoll&eyebrowType=SadConcernedNatural&mouthType=Serious&skinColor=Tanned',
        'https://avataaars.io/?avatarStyle=Circle&topType=WinterHat3&accessoriesType=Prescription02&hatColor=Red&hairColor=Brown&facialHairType=BeardLight&facialHairColor=Auburn&clotheType=ShirtScoopNeck&clotheColor=Blue03&eyeType=Surprised&eyebrowType=FlatNatural&mouthType=Twinkle&skinColor=Yellow',
        'https://avataaars.io/?avatarStyle=Circle&topType=ShortHairSides&accessoriesType=Sunglasses&hairColor=PastelPink&facialHairType=Blank&clotheType=Overall&clotheColor=Heather&eyeType=Cry&eyebrowType=SadConcernedNatural&mouthType=Serious&skinColor=Pale',
        'https://avataaars.io/?avatarStyle=Circle&topType=LongHairFroBand&accessoriesType=Blank&hairColor=Brown&facialHairType=Blank&clotheType=BlazerShirt&eyeType=Default&eyebrowType=RaisedExcitedNatural&mouthType=Smile&skinColor=Light',
        'https://avataaars.io/?avatarStyle=Circle&topType=ShortHairShortRound&accessoriesType=Wayfarers&hairColor=Brown&facialHairType=BeardMagestic&facialHairColor=Blonde&clotheType=Overall&clotheColor=Gray01&eyeType=Surprised&eyebrowType=RaisedExcited&mouthType=Smile&skinColor=Yellow',
        'https://avataaars.io/?avatarStyle=Circle&topType=ShortHairShortCurly&accessoriesType=Round&hairColor=SilverGray&facialHairType=Blank&facialHairColor=Auburn&clotheType=BlazerSweater&clotheColor=Gray01&eyeType=Side&eyebrowType=UpDownNatural&mouthType=Serious&skinColor=Pale',
        'https://avataaars.io/?avatarStyle=Circle&topType=ShortHairFrizzle&accessoriesType=Wayfarers&hairColor=Platinum&facialHairType=BeardMedium&facialHairColor=Black&clotheType=Overall&clotheColor=Gray02&eyeType=WinkWacky&eyebrowType=Angry&mouthType=Default&skinColor=Pale',
        'https://avataaars.io/?accessoriesType=Blank&avatarStyle=Circle&clotheType=ShirtScoopNeck&eyeType=EyeRoll&eyebrowType=Default&facialHairType=BeardMedium&hairColor=BlondeGolden&mouthType=Sad&skinColor=Brown&topType=ShortHairShortCurly',
        'https://avataaars.io/?accessoriesType=Blank&avatarStyle=Circle&clotheColor=Red&clotheType=GraphicShirt&eyeType=Close&eyebrowType=FlatNatural&facialHairColor=Platinum&facialHairType=BeardMedium&hairColor=Red&hatColor=Heather&mouthType=Tongue&skinColor=Pale&topType=ShortHairDreads01',
        'https://avataaars.io/?accessoriesType=Blank&avatarStyle=Circle&clotheColor=PastelRed&clotheType=BlazerShirt&eyeType=Default&eyebrowType=UpDownNatural&facialHairColor=Platinum&facialHairType=Blank&graphicType=Pizza&hairColor=Auburn&hatColor=Gray01&mouthType=ScreamOpen&skinColor=Brown&topType=ShortHairSides',
        'https://avataaars.io/?accessoriesType=Blank&avatarStyle=Circle&clotheColor=PastelRed&clotheType=ShirtScoopNeck&eyeType=Default&eyebrowType=RaisedExcitedNatural&facialHairColor=BrownDark&facialHairType=BeardMagestic&graphicType=Hola&hairColor=Auburn&hatColor=Red&mouthType=Smile&skinColor=DarkBrown&topType=Hijab',
        'https://avataaars.io/?accessoriesType=Blank&avatarStyle=Circle&clotheColor=Blue01&clotheType=Overall&eyeType=Wink&eyebrowType=SadConcerned&facialHairColor=Black&facialHairType=MoustacheMagnum&graphicType=Resist&hairColor=Platinum&hatColor=Heather&mouthType=Grimace&skinColor=Light&topType=LongHairBun',
        'https://avataaars.io/?accessoriesType=Blank&avatarStyle=Circle&clotheColor=Blue02&clotheType=ShirtVNeck&eyeType=Default&eyebrowType=Angry&facialHairColor=Blonde&facialHairType=BeardLight&graphicType=SkullOutline&hairColor=SilverGray&hatColor=PastelGreen&mouthType=Eating&skinColor=Brown&topType=ShortHairSides',
        'https://avataaars.io/?accessoriesType=Blank&avatarStyle=Circle&clotheColor=PastelBlue&clotheType=CollarSweater&eyeType=EyeRoll&eyebrowType=UnibrowNatural&facialHairColor=Black&facialHairType=BeardMedium&graphicType=Cumbia&hairColor=Auburn&hatColor=PastelRed&mouthType=Sad&skinColor=DarkBrown&topType=ShortHairShortRound',
        'https://avataaars.io/?accessoriesType=Blank&avatarStyle=Circle&clotheColor=Gray02&clotheType=ShirtCrewNeck&eyeType=Happy&eyebrowType=SadConcernedNatural&facialHairColor=Auburn&facialHairType=MoustacheFancy&graphicType=Cumbia&hairColor=BlondeGolden&hatColor=PastelGreen&mouthType=Smile&skinColor=Pale&topType=LongHairDreads',
        'https://avataaars.io/?accessoriesType=Round&avatarStyle=Circle&clotheColor=PastelOrange&clotheType=BlazerSweater&eyeType=WinkWacky&eyebrowType=RaisedExcited&facialHairColor=BlondeGolden&facialHairType=BeardMedium&graphicType=Hola&hairColor=Auburn&hatColor=Gray01&mouthType=Default&skinColor=Yellow&topType=ShortHairFrizzle'
        ###

        */

    random() {
        const random = Math.floor((Math.random() * this.avatarArr.length));
        return this.avatarArr[random];
    }
}
