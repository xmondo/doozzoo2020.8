import { Component, OnInit, Input } from '@angular/core';
import { Avatar } from './avatar';
import { AuthService } from '../../shared/auth.service';

@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss']
})
export class AvatarComponent implements OnInit {
  @Input() userId: any;
  @Input() avatarURL: URL;
  ref: any;

  constructor(
    public avatar: Avatar,
    public ats: AuthService,
  ) {
    // ...
  }

  ngOnInit() {
    // console.log('avatarURL: ', this.avatarURL);
  }

  setAvatar(URL) {
    this.ats.avatarEmitter.emit(URL);
  }

}

/**
 * read avatar URL from input, 
 * it typically comes from coach or student form
 * if other avatar has been selected,
 * emit new URL to form.
 * Only when form is saved,
 * avatar URL is updated in DB
 */
