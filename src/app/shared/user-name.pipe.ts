import { Pipe, PipeTransform } from '@angular/core';
import { AngularFireDatabase, AngularFireObject } from '@angular/fire/database';
import { map } from 'rxjs/operators';

/**
 * <input> | userName:'default'
 */

@Pipe({
  name: 'userName'
})
export class UserNamePipe implements PipeTransform {
  constructor(
    private db: AngularFireDatabase,
  ) {
    // ...
  }

  transform(userId: string, type: 'default' | 'nickname' | 'email'): unknown {
    const _type = type ?? 'default';
    // console.log(type)
    if(!userId) {
      return null;
    } else {
      if(_type === 'default') {
        return this.db
          .object(`profiles/${userId}/contactData/nickname`)
          .valueChanges()
          .pipe(
            map(val => val ?? userId),
          )

      } else if (_type === 'nickname') {
        return this.db
          .object(`profiles/${userId}/contactData/nickname`)
          .valueChanges()
          .pipe(
            map(val => val ?? userId),
          )
      } else if (_type === 'email') {
        return this.db
          .object(`profiles/${userId}/contactData/email`)
          .valueChanges()
          .pipe(
            map(val => val ?? userId),
          )
      }
    }

  }
}
