import { Injectable, Input } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AuthService } from 'src/app/shared/auth.service';
import { Observable, Subscription } from 'rxjs';
import { map, subscribeOn, filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FeatureToggleService {
  user:any;
  tempSub:Subscription;
  ft:any;

  constructor(
    private ats: AuthService,
    private db: AngularFireDatabase
  ) {

  }

  checkFeatureToggle(featureId){
    console.log(featureId);
    return true;
  }

  // end of class
}

/**
 * requests from a service status based on a feature id:string
 * evaluates status based on configured rule depending on dedicated feature
 * e.g.
 * feature: archiving
 * rule: unlocked on user base
 * check: is feature true for specified user
 * 
 * feature: media
 * rule: unlocked on role base: all coaches are allowed to use media
 * check: is feature true for role 1
 * 
 * user -> featureToggle -> featureId: boolean
 * featureToggles -> featureId -> rules {
 *  user:boolean;
 *  type: user | role | group | institution
 * }

input expected: 
const status = checkFeature(featureId):boolean
-> read feature obj from featureToggles
-> read feature <-> user | role | group | institution
-> return result
*/
