import * as Tone from 'tone';
import { AudioService } from './audio.service';
import { MyUid } from './my-uid';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { distinctUntilChanged, map, startWith } from 'rxjs/operators';
import { NgZone } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

interface ChannelSettings {
    level: number,
    isToMaster: boolean,
    isToClass: boolean,
    isMuted: boolean,
}

export class Channelstrip {
    name: string;
    id: string;
    // should all be private?
    private level: number;
    gainObj: Tone.Volume;
    isToMaster: boolean;
    isToClass: boolean;
    isMuted: boolean;
    meter: Tone.Meter;
    meterIntervalToken: any;
    meterLevel: number = 0;
    doAnimate: boolean = true;
    // Create a meter level subject - The thing that will be watched by the observable
    private meterLevelVar = new Subject<number>();
    public meterLevelVar$: Observable<any>;
    // Create a volume level subject - The thing that will be watched by the observable
    private volLevelVar = new BehaviorSubject<number>(-5);
    public volLevelVar$: Observable<number>;
    // getLevelObserver: Observable<any>;
    // zone = NgZone.prototype.runOutsideAngular;
    private translate: TranslateService;

    constructor(
        name: string,
        private as: AudioService,
        level = -5,
        toMaster = true,
        toClass = false,
        muted = false,
        // gives the possibility to create 
        // a dedicated channelstrip for different sources
        id?: string,
        private zone?: NgZone,
    ) {
        // 1st initialization from submitted parameters
        this.name = this.trimLabel(name);
        this.id = id || MyUid.newGuid();
        this.level = level || -5;
        this.gainObj = new Tone.Volume(this.level);
        this.isToMaster = toMaster;
        this.isToClass = toClass;
        this.isMuted = muted;
        // 2nd -> check for stored config
        this.getFromStorage()

        // Create an observable to watch the subject and send out a stream of updates (You will subscribe to this to get the update stream)
        // init 
        // console.log(`+++ start level for ${this.name}: ${this.level}`)
        this.volLevelVar$ = this.volLevelVar
            .asObservable();
        // this.volLevelVar.next(level);
        this.setLevel(this.level)
        /*
        // same for the meter
        this.meter = new Tone.Meter()
        this.meter.normalRange = true;
        this.gainObj.connect(this.meter);
        this.meterLevelVar$ = this.meterLevelVar
            .asObservable()
            .pipe(
                startWith(this.meter.getValue())
            );
        */
        this.init();
    }

    setLevel(level: number) {
        // console.log('set level: ', level, this.name);
        // console.log('gainObjects: ', this.as.gainObjects);
        this.level = level;
        this.gainObj.volume.value = level;
        // new approach, use subject for level
        this.volLevelVar.next(level);
        /*
        this.volLevelVar$ = this.volLevelVar
            .asObservable()
            .pipe(startWith(this.level))
        */
        // set to local storage
        this.saveToStorage();
    }

    getLevel(): number {
        return this.gainObj.volume.value;
    }

    streamMeter() {
        // this.meterIntervalToken = setInterval(() => this.meterLevelVar.next(this.meter.getValue()), 10);
        // this.meterIntervalToken = setInterval(() => this.meterLevel = this.meter.getValue(), 10);
        this.zone.runOutsideAngular(() => {      
            const loop = () => {
                // this.meterLevel = Tone.dbToGain(this.meter.getValue());
                this.meterLevelVar.next(this.meter.getValue());
                this.doAnimate ? requestAnimationFrame(loop) : '';
            };
            loop();
           // this.meterIntervalToken = setInterval(() => this.meterLevelVar.next(this.meter.getValue()), 10);
        });   
    }

    // 
    toggleToMaster() {
        const previous = this.isToMaster;
        this.isToMaster = !this.isToMaster;
        this.isToMaster ? this.gainObj.connect(Tone.Master) : (previous ? this.gainObj.disconnect(Tone.Master) : status = status );
        this.saveToStorage();
    }

    setToMaster(status: boolean) {
        const previous = this.isToMaster;
        try {
            status ? this.gainObj.connect(Tone.Master) : (previous ? this.gainObj.disconnect(Tone.Master) : status = status );    
        } catch (error) {
            console.warn('Channelstrip->setToMaster error: ', error);
        }
        
        this.isToMaster = status;
        this.saveToStorage();
    }

    toggleToClass() {
        const previous = this.isToClass;
        this.isToClass = !this.isToClass;
        // tslint:disable-next-line:max-line-length
        this.isToClass ? this.gainObj.connect(this.as.masterDelay) : ( previous ? this.gainObj.disconnect(this.as.masterDelay) : this.isToClass = this.isToClass );
        this.saveToStorage();
    }
    setToClass(status: boolean) {
        const previous = this.isToClass;
        // tslint:disable-next-line:max-line-length 
        try {
            status ? this.gainObj.connect(this.as.masterDelay) : ( previous ? this.gainObj.disconnect(this.as.masterDelay) : status = status );    
        } catch (error) {
            console.warn('Channelstrip->setToClass error: ', error);
        }
        this.isToClass = status;
        this.saveToStorage();
    }

    toggleMute() {
        this.gainObj.mute = !this.isMuted;
        this.isMuted = !this.isMuted;
        this.saveToStorage();
    }

    setMute(status: boolean){
        this.gainObj.mute = status;
        this.isMuted = status;
        this.saveToStorage();
    }

    disconnectAll() {
        if (this.isToMaster || this.isToClass) { 
            // you can only disconnect once, otherweise error will be thrown
            this.gainObj.disconnect();
        }
    }

    switchAll(
        toMaster: boolean,
        toClass: boolean,
        muted: boolean,
    ) {
        // setToMaster, for microphone must always be false
        if (this.id !== 'microphoneId') {
            this.setToMaster(toMaster);
            this.setToClass(toClass);
            this.setMute(muted);
        } else {
            this.setToMaster(false);
            this.setToClass(false);
            this.setMute(false);
        }
        this.saveToStorage();
    }

    rewire() {
        this.switchAll(
            this.isToMaster,
            this.isToClass,
            this.isMuted,
        );
    }

    dispose() {
        const index = this.as.gainObjects.findIndex(item => item.id === this.id);
        if (index !== -1) {
            // if already existing, 1. dispose and 2. replace
            this.as.gainObjects[index].disconnect();
            // TODO: should we add/replace the same???!
            this.as.gainObjects.splice(index, 1); // this.as.gainObjects.splice(index, 1, this);
            // console.log(`${this.name} was disconnected & disposed from gainObjects`);
        }
        // dispose in the Tone context
        this.gainObj.dispose();
        this.doAnimate = false;
    }

    disconnect() {
        this.gainObj.disconnect();
        // stops propagation of meter level
        clearInterval(this.meterIntervalToken);
    }

    trimLabel(label: string, length: number = 10): any {
        if (label.length > length) {
            return label.substr(0, length) + '...';
        } else {
            return label;
        }
        /*
        this.translate
            .get(label)
            .subscribe((res: string) => {
                res = res ?? label;
                if (res.length > length) {
                    return res.substr(0, length) + '...';
                } else {
                    return res;
                }
            });
        */
    }

    addToArray() {
        /*
        console.log('as.gainObjects: ', this.as.gainObjects.map(val => {
            return { name: val.name, id: val.id };
        }));
        */
        // const index = this.as.gainObjects.findIndex(item => item.name === this.name);
        const index = this.as.gainObjects.findIndex(item => item.id === this.id);
        //
        if (index !== -1) {
            // if already existing, 1. dispose and 2. replace
            //this.as.gainObjects[index].dispose();
            //this.as.gainObjects.splice(index, 1 , this);
            // console.log(`${this.name} was spliced from/to gainObjects`);
        // add new audio node 
        } else {
            this.as.gainObjects.push(this);
            // console.log(`${this.name} was added to gainObjects`);
        }
    }

    // redundant, but keep legacy
    removeFromArray() {
        //
        this.dispose();
    }

    getFromStorage() {
        const defaultChannelSettings: ChannelSettings = {
            level: this.level,
            isToMaster: this.isToMaster,
            isToClass: this.isToClass,
            isMuted: this.isMuted,
        }
        const tempChannelSettings: ChannelSettings = JSON.parse(localStorage.getItem(`channelSettings-${this.id}`));

        let channelSettings: ChannelSettings;
        if(tempChannelSettings !== undefined && tempChannelSettings !== null) {
            channelSettings = tempChannelSettings;
        } else {
            channelSettings = defaultChannelSettings;
            // save defaults to storage
            this.saveToStorage();
        }
        // now apply to class attributes
        this.setLevel(channelSettings.level);
        this.isToMaster = channelSettings.isToMaster;
        this.isToClass = channelSettings.isToClass;
        this.isMuted = channelSettings.isMuted;

        // also return
        return channelSettings;
    }

    saveToStorage() {
        const currentChannelSettings: ChannelSettings = {
            level: this.level,
            isToMaster: this.isToMaster,
            isToClass: this.isToClass,
            isMuted: this.isMuted,
        }
        // only for standard apps
        if(['microphoneId', 'metronomeId','tunerId','keyboardId','recorderId'].includes(this.id)) {
        // if(['metronomeId','tunerId','keyboardId','recorderId'].includes(this.id)) {
            localStorage.setItem(`channelSettings-${this.id}`, JSON.stringify(currentChannelSettings))
        };
    }

    init() {
        // console.log(`init channelstrip ${this.name}`)
        /**
         * first and in any case we have to connect the nodes,
         * otherwise an error is thrown, when tryining to disconnect.
         * After this any status can be set by the given parameters
         * 
         * microphone already is connected to the mediastream destination
         * we only want to expose it for beeing able to handle volume
         */
        if ( this.id !== 'microphoneId') { 
            // TODO: WHY?????
            // every gainnode is per default connected with local audio Destination 
            // to remote, this will be intercepted by the masterDelay node...
            this.gainObj.connect(Tone.Master);

            this.setToMaster(this.isToMaster);
            this.setToClass(this.isToClass);
            this.setMute(this.isMuted);
        }

        this.addToArray();

    }
}
