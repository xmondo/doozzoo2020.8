import { Pipe, PipeTransform } from '@angular/core';
/*
 * Raise the value exponentially
 * Takes an filesize in bytes
 * Example:
 *    value = 2048
 *   {{ value | filesize:'MB' }}
 *   formats to: 2 MB
 * 
 * Unit defaults to 'MB'
*/
@Pipe({name: 'filesize'})
export class Filesize implements PipeTransform {

  transform(value: number, unit: 'BT' | 'KB' | 'MB' | 'GB' = 'MB' ): string {
    // bytes in Bytes
    switch(unit) {
      case 'BT':
        let s = value;
        return value.toString();
        break;
      case 'KB':
        s = (value / 1024);
        return value.toString();
        break;
      case 'GB':
        s = ((value / 1024) / 1024) / 1024;
        return s.toFixed(2);
        break;
      default: // 'MB'
        s = (value / 1024) / 1024;
        return s.toFixed(2);
    }
  }
}