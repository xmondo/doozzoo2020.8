import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterUser'
})
export class FilterUserPipe implements PipeTransform {

  transform(items: any[], filter: string): any {
    // console.log('pipe input: ', items);
    if (!items || !filter) {
      return items;
    } else if (filter.length < 3) {
      console.log('filter: ', filter, filter.length);
      return items;
    }
    // normalize to lower case
    // filter = filter.toLowerCase();

    // filter items array, items which match and return true will be
    // kept, false will be filtered out
    console.log('item[5]', items[5]);
    return items.filter(item => item.key.indexOf(filter) !== -1);
  }
// end of class
}
