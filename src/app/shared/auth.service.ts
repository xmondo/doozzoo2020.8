import { Injectable, EventEmitter, NgZone } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
// import { auth } from 'firebase/app';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable, BehaviorSubject, Subscription } from 'rxjs';
import { map, filter, take, distinctUntilChanged } from 'rxjs/operators';
import { Router } from '@angular/router';

// make available for other modules as well
export interface UserData {
    uid: string,
    isAnonymous: boolean,
    email: string,
    emailVerified: boolean,
    role: number,
    userActions: string,
    photoURL: string,
    plan: 'starter'| 'pro' | 'premium', 
    displayName?: string,
    nickName?: string,
    profiMode?: boolean,
    betaMode?: boolean,
    betatester?: false,
    featureToggles?: any,
    lastSignInTime?: number,
    creationTime?: number,
    initiatorId?: string,
    institutionId?: string,
    triggerType?: string, // institution | coach | self
}

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  // static vars
  // TODO: this should be private
  // private userData: UserData = {
  userData: UserData = {
    uid: null,
    isAnonymous: true,
    email: null,
    emailVerified: false,
    role: -2,
    userActions: null,
    photoURL: 'assets/img/avatars/avataaars-default.png',
    plan: null, 
    displayName: null,
    nickName: null,
    profiMode: false,
    betaMode: false,
    betatester: false,
  };

  userId: string = null;
  user: AngularFireAuth['user'] = null;

  inviteEmitter: EventEmitter<any> = new EventEmitter();
  avatarEmitter: EventEmitter<any> = new EventEmitter();
  userDataEmitter: BehaviorSubject<UserData> = new BehaviorSubject(this.userData);

  // Observables
  usr$: Observable<any>;
  user$: Observable<any>; // Observable<User|null>
  // provides role only
  role$: Observable<any>; // Observable<User|null>
  auth$: any; // initialized firebase.auth.Auth
  loginStatus$: Observable<boolean>;
  userId$: Observable<string>;
  userAvatar$: Observable<any>;
  emailVerified$: Observable<boolean>;
  userAndNotEmailVerified$: Observable<boolean>;
  showLoginRegister$: Observable<boolean>;
  featureToggles$: Observable<any>;
  profileMissingStatus$: Observable<boolean>;
  isAnonymous$: Observable<boolean>;
  isEdu$: Observable<boolean>;
  getInstitutionId$: Observable<string>;

  subContainer = new Subscription();

  constructor(
    public afa: AngularFireAuth,
    public db: AngularFireDatabase,
    public router: Router,
    private zone: NgZone,
  ) {

    // AngularFireAuth.user
    this.user$ = afa.user; // emits user | null
    // initialized firebase.auth.Auth
    this.auth$ = afa.auth;
    // init store

    // short alias for userData Observable
    this.usr$ = this.userDataEmitter;

    // helper subscribe for user.uid | null
    this.userId$ = afa.user.pipe(
      // filter out undefined|null events
      filter(val => val !== null),
      // map(val => val.uid ? val.uid : null)
      map(val => val.uid)
    );

    /* ### helpers start ### */
    this.isAnonymous$ = this.usr$
      .pipe(
        map(val => val.isAnonymous || true) // if user === null -> true
      );

    // helpers for edu
    this.isEdu$ = this.usr$
      .pipe(
        filter((val:any) => val.triggerType !== null),
        map((val:any) => val.triggerType === 'institution' || val.triggerType === 'institutionCoach' ? true : false)
      );
    this.getInstitutionId$ = this.usr$
      .pipe(
        filter((val:any) => val.triggerType === 'institution' || val.triggerType === 'institutionCoach'),
        map(val => val.institutionId ? val.institutionId : 'unset')
      );

    // helper for email status
    this.emailVerified$ = afa.user
      .pipe(
        filter(val => val !== null),
        map((val: any) => val.emailVerified)
      );

    // helper for not isEmailVerified && user logged in status
    this.userAndNotEmailVerified$ = afa.user
      .pipe(
        // when user === null -> there was no login, do not show verify tab
        // when user && user.emailVerified === false -> no reason to show the tab
        // when user and !val.emailVerified -> email verification needed
        map((val: any) => val === null ? false : !val.emailVerified)
      );

    this.showLoginRegister$ = this.usr$
      .pipe(
        distinctUntilChanged(),
        map(val => {
          // console.log(val)
          if (val.uid !== null) {
            return false;
          } else if (val.isAnonymous === true || val.role < 0) {
            return true;
          } else {
            return true;
          }
        })
      );

    // helper for role
    this.role$ = this.usr$
      .pipe(
        map(val => val.role)
      );

    // helper for login status true | false
    // this.loginStatus$ = this.afa.user
    this.loginStatus$ = this.usr$
      .pipe(
        // map(val => val.role > -1)
        // map(val => val.isAnonymous === false && val.emailVerified === true),
        map(val => {
          if (val === null || val === undefined) {
            return false;
          } else if (
            val.isAnonymous === false &&
            val.emailVerified === true &&
            (val.role > -1 || val.userActions === 'coachRequestPending')
          ) {
            return true;
          } else {
            return false;
          }
        })
      );

    this.profileMissingStatus$ = this.usr$
      .pipe(
        filter(val => val !== null),
        map(val => {
          return val.role === -1 && val.emailVerified === true ? true : false;
        })
      );

    // helper for avatar -> url | null
    this.userAvatar$ = this.usr$
      .pipe(
        filter(val => val !== null),
        filter(_val => _val.photoURL !== null),
        map(val => val.photoURL),
      );
    // helper for feature toggle
    this.featureToggles$ = this.usr$
      .pipe(
        filter(val => val !== null),
        filter(val => val.featureToggles !== null),
        map(val => val.featureToggles),
      );

    // onAuthStateChanged(nextOrObserver, error, completed) returns function()
    // see: https://firebase.google.com/docs/reference/js/firebase.auth.Auth
    this.auth$
      .onAuthStateChanged(user => {
        if (user !== null) {
          // console.log('onAuthStateChanged:', 'user.ra: ', user.ra); // JWT
          // console.log('onAuthStateChanged:', 'user.metadata: ', user.metadata);
          /**
          * fetch first the data set from firebase user
          */
          // fetch login times for not anonymous users
          user.isAnonymous ? '' : this.updateUser(user);

          if(user.metadata.lastSignInTime === null) {
            console.log('lastSignInTime -> first login');
          }
          this.userData.uid = user.uid;
          this.userData.isAnonymous = user.isAnonymous;
          this.userData.emailVerified = user.emailVerified;
          this.userData.email = user.email;
          this.userData.displayName = user.displayName;
          this.userData.creationTime = user.metadata.creationTime;
          this.userData.lastSignInTime = user.metadata.lastSignInTime;
          // institutional users
          if (user.initiatorId) {
            this.userData.initiatorId = user.initiatorId; 
          }
          if (user.photoURL) {
            this.userData.photoURL = user.photoURL; 
          } else {
            this.userData.photoURL = 'assets/img/avatars/avataaars-default.png'
          }

          // anonymous user and those that did not verify email, keep status -1
          if (user.isAnonymous || !user.emailVerified) {
            this.userData.role = -1;
            // finally update user data for this option
            this.userDataEmitter.next(this.userData);
          } else {
            /**
             * now enrich user data with doozzoo data set
             */
            const userSub = this.db.object('users/' + user.uid)
              .valueChanges()
              .pipe(
                // process data before parsing
                map((_val: any) => {
                  return !_val ? null : this.parseUserData(_val, user);
                })
              )
              .subscribe(
                val => {
                  // user data might not exist due to registration interim phase of double opt in
                  if (val === null || val === undefined) {
                    console.log('null val: ', val);
                    this.userData.role = -1;
                  // correct user data...
                  } else {
                    // console.log('user data update after: ', val);
                    this.userData.role = val.role;
                    this.userData.plan = val.plan;
                    this.userData.betatester = val.betatester;
                    this.userData.displayName = val.nickname;
                    this.userData.nickName = val.nickname;
                    this.userData.betaMode = val.betaMode;
                    this.userData.profiMode = val.profiMode;
                    this.userData.userActions = val.userActions;
                    this.userData.featureToggles = val.featureToggle;
                    if(val.trigger) {
                      this.userData.triggerType = val.trigger.type;
                      this.userData.initiatorId = val.trigger.initiatorId;
                      this.userData.institutionId = val.trigger.institutionId;
                    }
                    // TODO: not existing in user data
                    // this.userData.photoURL = val.photoURL;
                  }
                  // finally update user data for this option
                  this.userDataEmitter.next(this.userData);
                }
              );
            /**
             * fetch avatar URL and enrich userData obj
             */
            this.fetchAvatarUrl(user.uid);
            this.fetchNickName(user.uid);

            this.subContainer.add(userSub);
          }

/*
          // if now in registration process, which means
          // 1. there is a user
          // 2. not anonymous 
          // 3. email is verified
          // --> redirect to login
          if (this.userData.emailVerified && this.userData.role === -1) {
            console.log('redirect to /login');
            this.router.navigateByUrl('/login');
          }
*/          
        // no user at all
        } else {
          this.userData.role = -2;
          // finally update user data for this option
          this.userDataEmitter.next(this.userData);
          /**
           * TODO: do not redirect for 
           * invites & login & room direct links
           */
          // console.log('router.url->', this.router.url);
          if (
            
            this.router.url.indexOf('/invite/') !== -1 || 
            this.router.url.indexOf('/login') !== -1 ||
            this.router.url.indexOf('/login/standalone') !== -1 ||
            this.router.url.indexOf('/room/') !== -1 ||
            this.router.url.indexOf('/pro/') !== -1 ||
            this.router.url.indexOf('/free/') !== -1 ||
            this.router.url.indexOf('/test') !== -1 ||
            this.router.url.indexOf('/directlogin') !== -1
            
            //['/invite/','/login','/login/standalone','/room/','/test','/directlogin','/free/','/plus/','/pro/'].some(item => item.indexOf(this.router.url) !== -1)
          ) {
            // do nothing
          } else {
            console.warn('AuthService->onAuthStateChanged: redirect to root');
            this.zone.run(() => this.router.navigateByUrl('/'));
            // TODO: will now only be done in testdrive page
            // this.signInAnonymously();
          }

        }

      },
      error => {
        console.log('auth service->onAuthStateChanged: error: ', error);
      },
      // completed
      () => {
        // console.log('auth service->onAuthStateChanged: completed');
      });


  // constructor end
  }

    institutionalUser(user) {
      // if(user.insti)
    }

  /**
   *
   * @param data
   *  active: false
      betaMode: true
      email: "coach1@mypelz.de"
      emailVerified: true
      firstname: "Christoph Coach1"
      id: "auth0|582c7e4cedb8c73048379aad"
      lastname: "Pelz Coach1"
      nickname: "coach1"
      profiMode: true
      role: 1
      user_id: "auth0|582c7e4cedb8c73048379aad"
   */
  parseUserData(data, user) {
    // console.log('parseUserData: ', data);
    // case: no role available
    data.role = (data.role === null || data.role === undefined) ? -1 : data.role;
    // case: no nickname available
    data.nickname = !data.nickname ? user.email.split('@')[0] : data.nickname;
    data.userActions = !data.userActions ? 'default' : data.userActions;
    data.betatester = !data.betatester ? false : data.betatester;
    return data;
  }

  // helper for role based visibility
  perm(rolesArr: Array<Number>) {
    return this.userDataEmitter.pipe(
      map(val => rolesArr.some(item => item === val.role)),
      take(1), // TODO: will only execute once, does that work?
    );
  }

  signInAnonymously() {
    // performs general sign in
    this.auth$
      .signInAnonymously()
      .catch(function(error) {
        // Handle Errors here.
        const errorCode = error.code;
        const errorMessage = error.message;
        console.log(error);
      // ...
    });
  }

  logout() {
    // this.userData.institutionId = null
    console.log('authService->do logout...');
    // this.subContainer.unsubscribe();
    this.afa.auth.signOut();

    // this.userData = null;
    this.userData.uid = null;
    this.userData.initiatorId = null;
    this.userData.institutionId = null;
    this.userDataEmitter.next(this.userData);
    /*
    // this terminates the userdata listener, so that no relogin is possible
    setTimeout(() => {
      this.subContainer.unsubscribe();
      this.auth$.auth.signOut();
    }, 2000);
    */
  }

  fetchAvatarUrl(userId: string) {
    // same for Avatar URL
    const ref = this.db
      .object('profiles/' + userId + '/contactData/avatar');

    const avatarURL = ref
      .valueChanges()
      .pipe(
        filter(_val => _val !== null),
      )
      .subscribe((val: string) => {
        this.userData.photoURL = val;
        // console.log('fetchAvatarUrl: ', this.userData);
        this.userDataEmitter.next(this.userData);
      });
    this.subContainer.add(avatarURL);
  }

  fetchNickName(userId: string) {
    // same for Avatar URL
    const ref = this.db
      .object('profiles/' + userId + '/contactData/nickname');

    const nickSub = ref
      .valueChanges()
      .pipe(
        filter(_val => _val !== null),
      )
      .subscribe((val: string) => {
        this.userData.nickName = val;
        this.userData.displayName = val;
        // console.log('fetchAvatarUrl: ', this.userData);
        this.userDataEmitter.next(this.userData);
      });
    this.subContainer.add(nickSub);
  }

  // update user signin etc times
  updateUser(user){
    const data = {
      creationTime: user.metadata.creationTime,
      lastSignInTime: user.metadata.lastSignInTime,
      ct: Date.parse(user.metadata.creationTime),
      lst: Date.parse(user.metadata.lastSignInTime),
    }
    const ref = this.db
      .object('users/' + user.uid)
      .update(data);  
  }

  setDisplayName(name) {
    this.userData.nickName = name;
    this.userData.displayName = name;
    // broadcast
    this.userDataEmitter.next(this.userData);
  }
  getDisplayName() {
    return this.userData.nickName;
  }

  // for 
  setUidNullAndSignOut() {
    this.auth$
      .signOut()
      .then(() => {
        //...
        // this.userData.uid = null;
        // this.userDataEmitter.next(this.userData);
        this.setUserData('uid', null);
      })
      .catch(e => console.log('setUidNullAndSignOut: ', e));
  }

  // userData getter/setter
  setUserData(key: string, value: any){
    this.userData[key] = value;
    this.userDataEmitter.next(this.userData);
  }

  getUserData(){
    return this.userData;
  }

// end of class
}
