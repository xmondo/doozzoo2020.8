import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import { AccountState, AccountStoreService } from '../account-store.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  vm$: Observable<any>;
  constructor(
    public a: AuthService,
    public deviceService: DeviceDetectorService,
    private ast: AccountStoreService
  ) {
    this.vm$ = ast.vm$;
   }

  ngOnInit() {
  }

}
