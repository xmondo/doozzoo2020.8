import { TestBed, inject, async } from '@angular/core/testing';
import * as OT from '@opentok/client';

import { OpentokService } from './opentok.service';
import config from '../../config';
import { environment } from '../../environments/environment';
import { DeviceDetectorService } from 'ngx-device-detector';


describe('OpentokService', () => {
  let opentokService: OpentokService;
  // let environment: Environment;
  let deviceDetectorService: DeviceDetectorService;
  let session;
  config.API_KEY = 'apiKey';
  config.SESSION_ID = 'sessionId';
  config.TOKEN = 'token';

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        OpentokService,
        DeviceDetectorService,
      ]
    });
    opentokService = TestBed.get(OpentokService);
    deviceDetectorService = TestBed.get(DeviceDetectorService);
  });

  it('should be created', () => {
    expect(opentokService).toBeTruthy();
  });

  it('getOT() should return OT', () => {
    expect(opentokService.getOT()).toEqual(OT);
  });

  describe('initSession()', () => {
    const mockOT = {
      initSession() {}
    };

    beforeEach(() => {
      spyOn(opentokService, 'getOT').and.returnValue(mockOT);
      session = jasmine.createSpyObj('session', ['connect', 'on']);
      spyOn(mockOT, 'initSession').and.returnValue(session);
    });

    // TODO: WTF do I test here???
    it('should call OT.initSession', () => {
      opentokService.initSession('sessionId', 'token');
      expect(opentokService.session).toEqual(session);
      expect(mockOT.initSession).toHaveBeenCalledWith(environment.opentok.apiKey, config.SESSION_ID);
      expect(opentokService.session).toEqual(session);
    });

    it('connect should call connect', () => {
      opentokService.initSession('sessionId', 'token');
      expect(opentokService.session).toEqual(session);
      opentokService.connect();
      expect(opentokService.session.connect).toHaveBeenCalledWith(config.TOKEN, jasmine.any(Function));
    });

  });

/*
  describe('service', () => {
    config.API_KEY = 'apiKey';
    config.SESSION_ID = 'sessionId';
    config.TOKEN = 'token';
    let service;
    beforeEach(inject([OpentokService], (s: OpentokService) => {
      service = s;
    }));

    it('should be created', () => {
      expect(service).toBeTruthy();
    });

    it('getOT() should return OT', () => {
      expect(service.getOT()).toEqual(OT);
    });

    describe('initSession()', () => {
      const mockOT = {
        initSession() {}
      };
      let session;

      beforeEach(() => {
        spyOn(service, 'getOT').and.returnValue(mockOT);
        session = jasmine.createSpyObj('session', ['connect', 'on']);
        spyOn(mockOT, 'initSession').and.returnValue(session);
      });

      it('should call OT.initSession', () => {
        service.initSession();
        expect(mockOT.initSession).toHaveBeenCalledWith(config.API_KEY, config.SESSION_ID);
        expect(service.session).toEqual(session);
      });

      it('connect should call connect', () => {
        service.initSession();
        service.connect();
        expect(service.session.connect).toHaveBeenCalledWith(config.TOKEN, jasmine.any(Function));
      });
    });
  });
*/
});
