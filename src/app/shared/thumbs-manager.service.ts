import { Injectable, EventEmitter } from '@angular/core';
import { galeryGrid } from '../room/gallery-grid';
import { DeviceDetectorService } from 'ngx-device-detector';

export interface thumbsEvent {
  action: string, // create | delete | isHero | update
  id: string,
  event?: any,
}

export interface thumbs {
  id: string,
  layout: string, // LOV: default | mini | overflow
  isOwner?:boolean,
  isPinned?: boolean,
  hasAudio?: boolean,
  hasVideo?: boolean,
  OTObject?: any,
}

@Injectable({
  providedIn: 'root'
})

export class ThumbsManagerService {
  // handling of video thumbs in room
  thumbsEmitter: EventEmitter<thumbsEvent> = new EventEmitter(); //
  private thumbsArr: Array<thumbs> = [];

  viewModeArr: Array<string>;
  private viewMode: string;
  private thumbsTotalWidth: number;
  private restCount: number = 0;
  windowInnerWidth: number = 786;
  windowInnerHeight: number;
  private collapseMode: boolean = false;
  private gridArray: Array<any> = null;
  
  constructor(
    //...
    private dds: DeviceDetectorService,
  ) { 
    /**
     * handle thumbs
     * -> create thumb
     * -> delete thumb
     * -> make hero
     */
    this.thumbsEmitter
      .subscribe(val => {
        // console.log('+++ tms thumbsemitter: ', val)
        switch(val.action) {

          case 'create':
            let index = this.thumbsArr.findIndex(item => item.id === val.id);
            index === -1 ? this.doCreate(val) : '';
            break;

          case 'delete':
            // console.log('delete: ', val);
            index = this.thumbsArr.findIndex(item => item.id === val.id); 
            if (index !== -1) {
              this.thumbsArr.splice(index, 1);
              // reset view mode 
              if(this.thumbsArr.length === 1) {
                this.viewMode = this.viewModeArr[0];
              }
              this.dispatchLayout();
            } else {
              console.log('thumb does not exist');
            }
            break;

          case 'togglePin':
            index = this.thumbsArr.findIndex(item => item.id === val.id); 
            index !== -1 ? this.doTogglePin(index) : '';
            break; 

          case 'toggleViewMode':
            this.toggleViewMode();
            break; 

          case 'propertyChange':
            this.changeProperties(val);
          break; 

          case 'windowResize':
            // console.log('window resize event: ', val);
            // this.windowInnerWidth = val.event.target.innerWidth;
            // console.log(this.windowInnerWidth);
            this.dispatchLayout();
          break; 

          case 'collapseThumbs':
            // console.log('window resize event: ', val);
            this.collapseMode = !this.collapseMode;
            this.dispatchLayout();
          break; 

        } 
          
      });

    // no galery mode for mobile
    this.viewModeArr = this.dds.isMobile() ? ['hero', 'dual', 'multi'] : ['hero', 'dual', 'multi', 'galery'];
    this.viewMode = this.viewModeArr[0];
  // end of constructor
  }

  changeProperties(val) {
    const index = this.thumbsArr.findIndex(item => item.id === val.id);
    index !== -1 ? this.thumbsArr[index][val.event.changedProperty] = val.event.newValue : '';
  }

  doCreate(val) {
    const item = this.dispatchThumbsItem(val);
    if(this.thumbsArr.length > 0) {
      /**
       * counts nr of pinned items
       * as those should have priority in beeing presented,
       * insert new item after the pinned ones
       */
      const isPinnedCount = this.thumbsArr.filter(item => item.isPinned).length;
      this.thumbsArr.splice(isPinnedCount, 0, item);
    } else {
      this.thumbsArr.push(item);
    }
    // sort by pinned status
    this.thumbsArr.sort((a,b) => (b.isPinned ? 1 : -1) - (a.isPinned ? 1 : -1));
    // assign layout flags
    this.dispatchLayout();
  }
  
  doTogglePin(index: number){
    const isPinnedCount = this.thumbsArr.filter(item => item.isPinned).length;
    // console.log('isPinnedCount', isPinnedCount);

    if (this.viewMode === 'hero') {
      // first record current status
      const currentPinStatus: boolean = this.thumbsArr[index].isPinned;
      // ...then reset all
      this.thumbsArr.forEach(val => val.isPinned = false); 
      // now set the targeted one to the toggled boolean value
      this.thumbsArr[index].isPinned = !currentPinStatus;
    
    } else if (this.viewMode === 'dual') {
      // first record current status
      const currentPinStatus: boolean = this.thumbsArr[index].isPinned;
      // case when item is already pinned -> unpin it
      if(currentPinStatus) {
        this.thumbsArr[index].isPinned = false;  
      // if count of pins < 2, then simply pin it
      } else {
        if(isPinnedCount < 2) {
          this.thumbsArr[index].isPinned = true;
        // in this case replace the first already pinned one
        } else {
          this.thumbsArr[0].isPinned = false;
          this.thumbsArr[index].isPinned = true;  
        }
      }
    // pin a max of 4 items to the begin of the array
    // valid for multi and galery mode
    } else if (this.viewMode === 'multi' || this.viewMode === 'galery') {
      // first record current status
      const currentPinStatus: boolean = this.thumbsArr[index].isPinned;
      // case when item is already pinned -> unpin it
      if(currentPinStatus) {
        this.thumbsArr[index].isPinned = false;  
      // if count of pins < 4, then simply pin it
      } else {
        if(isPinnedCount < 4) {
          this.thumbsArr[index].isPinned = true;
        // in this case replace the first already pinned one
        } else {
          this.thumbsArr[0].isPinned = false;
          this.thumbsArr[index].isPinned = true;  
        }
      }
    }

   // lets sort pinned to index 0
    this.thumbsArr.sort((a,b) => (b.isPinned ? 1 : -1) - (a.isPinned ? 1 : -1));
    // update layouts afterwards
    this.dispatchLayout();
  }
  
  toggleViewMode(){
    const currentLength = this.thumbsArr.length;
    const currentIndex = this.viewModeArr.findIndex(item => item === this.viewMode);
    let nextIndex;
    // calc the next possible index
    (currentIndex === this.viewModeArr.length - 1) ? nextIndex = 0 : nextIndex = currentIndex + 1;

    if (currentLength > 3) {
      this.viewMode = this.viewModeArr[nextIndex];
    } else {
      // first two options: hero | dual only, if length 1/2
      // limit to option 0 | 1
      if(currentIndex === 1) {
        this.viewMode = this.viewModeArr[0];
      } else {
        this.viewMode = this.viewModeArr[nextIndex];
      }
    } 

    // update layouts afterwards
    this.dispatchLayout();    
  }

  viewModeGet() {
    return this.viewMode;
  }

  dispatchThumbsItem(val) {
    // console.log('dispatchThumbsItem: ', val);

    let hasAudio = true;
    let hasVideo = true;

    // dispatch kind of objects
    try {
      if (val.event.type === 'streamCreated') {
        hasAudio = val.event.stream.hasAudio;
        hasVideo = val.event.stream.hasVideo;
      } else if (val.event.type === 'videoElementCreated') {
        hasAudio = val.event.target.stream.hasAudio;
        hasVideo = val.event.target.stream.hasVideo;
      }
    } 
    catch (error) {
      console.log('dispatchThumbsItem->error: ', error);
    }

    const item: thumbs = {
      id:val.id, 
      layout: 'default',
      isOwner: false,
      isPinned: false,
      hasAudio: true,
      hasVideo: true,
    }
    return item;
  }

  dispatchLayout() {
    // viewport width
    const reservedspace: number = 80;
    const footerHeight = 50;
    const windowInnerWidth = window.innerWidth;
    const windowInnerHeight = window.innerHeight - footerHeight;
    // console.log(windowInnerWidth);
    let vw = windowInnerWidth - reservedspace;

    // thumbswidth
    const midiWidth: number = 180;
    const miniWidth: number = 40;
    let midiCount;
    let miniCount;
    let restCount;
    const sortArr: Array<number> = [];
    const reducer = (accumulator, currentValue) => accumulator + currentValue;

    // if galery view, calculate grid
    if(this.viewMode === 'galery') {
      this.gridArray = galeryGrid(windowInnerWidth, windowInnerHeight, this.thumbsArr.length);
    }
    
    // find out how many items are used in main grid
    const viewModeThumbsDefaultArr = [1, 2, 4];
    const viewModeThumbsDefaultIndex: number = this.viewModeArr.findIndex(item => item === this.viewMode);
    const viewModeThumbsDefault: number = viewModeThumbsDefaultArr[viewModeThumbsDefaultIndex];
    const thumbsCount: number = this.thumbsArr.length - viewModeThumbsDefault;

    let total = 0;
    if(thumbsCount > 0) {
      while(sortArr.length < thumbsCount) {
        sortArr.push(midiWidth);
      }
      // console.log(sortArr);
      total = sortArr.reduce(reducer);
    }

    if ((vw / miniWidth) >= thumbsCount) {
      while (total > vw) {
        const idx = sortArr.findIndex(item => item === midiWidth);
        sortArr[idx] = miniWidth;
        total = sortArr.reduce(reducer)
      }

      // apply collapse mode, sets manually all midis to mini
      if(this.collapseMode) {
        // apply only for midi
        sortArr.forEach((item, index) => item === midiWidth ? sortArr[index] = miniWidth : '');
      }

      // final reduce, make sure that array is not empty
      sortArr.length > 0 ? this.thumbsTotalWidth = sortArr.reduce(reducer) : this.thumbsTotalWidth = 0;
      //this.thumbsTotalWidth = sortArr.reduce(reducer);
      // console.log(sortArr);

      miniCount = sortArr.filter(item => item === miniWidth).length;
      midiCount = sortArr.filter(item => item === midiWidth).length;
      this.restCount = 0;
    } else {
      miniCount = Math.floor(vw / miniWidth);
      midiCount = 0;
      this.restCount = thumbsCount - miniCount;
    }

    // apply to thumbsArray 
    // mini | default
    // prerequisite to calculate the behaviour of the thumbnails section
    // e.g. colapse, expand
    this.thumbsArr.forEach((item: thumbs, index: number) => {
      if(index < viewModeThumbsDefault) {
        item.layout = 'default';
      } else {
        if(index < viewModeThumbsDefault + midiCount) {
          item.layout = 'default';
        } else if (index < viewModeThumbsDefault + midiCount + miniCount) {
          item.layout = 'mini';
        // if galery view mode, we also set to "default"
        } else if (this.viewMode === 'galery') {
          item.layout = 'default';
        // fallback: we did not find anything, so hide
        } else {
          item.layout = 'overflow';
        }
      }
    });
  }

  // getter, setter
  thumbsArrayGet() {
    return this.thumbsArr;  
  }

  /** 
   * defines for each grid item the position
  */
  getThumbsStatus(thumbsId) {
    const index = this.thumbsArr.findIndex(item => item.id === thumbsId);
    // console.log('getThumbsStatus->index: ', thumbsId, index);
    let status;
    let right;
    // only if items in arr
    if(index !== -1) {
      switch(this.viewMode){
        case 'hero':
          if(index === 0) {
            status = { class: 'hero' }
          } else {
            // console.log('getThumbsStatus->to right: ', this.thumbsArr[index].layout);
            const midiCount: number = this.thumbsArr.filter(item => item.layout === 'default').length;
            if(this.thumbsArr[index].layout === 'mini'){
              const midi = (midiCount - 1) * (180); // + 5);
              const mini = (index - midiCount) * (40); // + 1);
              right = midi + mini; // static
              // console.log('midi/mini: ', midi, mini)
            } else {
              right = ((index - 1) * (180)); // + 5));
            }
            status = { class: 'items', right: right }  
          }
        break;
        case 'dual': // two sections grid
          // set the css classes for video 1/2
          if(index === 0) {
            status = { class: 'dual one' }
          } else if (index === 1) {
            status = { class: 'dual two' }
          // set the class and the offset right for thumbnails
          } else {
            // console.log('getThumbsStatus->to right: ', this.thumbsArr[index].layout);
            const midiCount: number = this.thumbsArr.filter(item => item.layout === 'default').length;
            if(this.thumbsArr[index].layout === 'mini'){
              const midi = (midiCount - 2) * (180); // + 5);
              const mini = (index - midiCount) * (40); // + 1);
              right = midi + mini; // static
              // console.log('midi/mini: ', midi, mini)
            } else {
              right = ((index - 2) * (180)); // + 5));
            }
            status = { class: 'items', right: right }  
          }
        break;
        case 'multi': // four sections grid
          // set the css classes for video 1-4
          if(index === 0) {
            status = { class: 'multi one' }
          } else if (index === 1) {
            status = { class: 'multi two' }
          } else if (index === 2) {
            status = { class: 'multi three' }
          } else if (index === 3) {
            status = { class: 'multi four' }
          // set class and offset right for thumbnails videos
          } else {
            // console.log('getThumbsStatus->to right: ', this.thumbsArr[index].layout);
            const midiCount: number = this.thumbsArr.filter(item => item.layout === 'default').length;
            if(this.thumbsArr[index].layout === 'mini'){
              const midi = (midiCount - 4) * (180); // + 5);
              const mini = (index - midiCount) * (40); // + 1);
              right = midi + mini; // static
              // console.log('midi/mini: ', midi, mini)
            } else {
              right = ((index - 4) * (180)); // + 5));
            }
            status = { class: 'items', right: right }  
          }
        break;
        case 'galery':
          // set classes for galery items
          status = { 
            class: 'galery items', 
            top: this.gridArray[index].top,
            left: this.gridArray[index].left,
            width: this.gridArray[index].width,
            height: this.gridArray[index].height, 
          }  
        break;
      }
    } else {
      status = { class: '', right: 0 }
      return status;
    }
    return status;
  }

  getThumbsIndex(thumbsId) {
    const index = this.thumbsArr.findIndex(item => item.id === thumbsId);
    return index;
  }

  getThumbsTotalWidth() {
    // set to 0 for these to views, so that the control is shown at the windows border
    const tWidth = this.viewMode === 'multi' || this.viewMode === 'galery' ? 0 : this.thumbsTotalWidth;
    return tWidth;
  }

  getRestCount() {
    return this.restCount;
  }

  getCollapseMode() {
    return this.collapseMode;
  }


// end of class
}
