import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DeviceDetectorModule,
    // TranslateModule,
  ],
  exports: [
    DeviceDetectorModule,
    // TranslateModule,
  ]
})
export class BasicsSharedModule {}
