import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CookieTemplateComponent } from './cookie-template/cookie-template.component';


@Component({
  selector: 'app-cookie-hint',
  templateUrl: './cookie-hint.component.html',
  styleUrls: ['./cookie-hint.component.scss']
})
export class CookieHintComponent implements OnInit {
  config = { cookieHint: false, timestamp: Date.now() };
  constructor(
    public snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
    this.getConfig();
    // console.log(this.config);
    if (this.config.cookieHint === false) {
      setTimeout(() => this.cookieHint(), 1000);
    }
  }

  cookieHint() {
    // tslint:disable-next-line:max-line-length
    const msg = `This website uses cookies to ensure you get the best experience on our website.`;
    // const snbRef = this.snackBar.open( msg, 'ok' );
    const snbRef = this.snackBar.openFromComponent(CookieTemplateComponent);
    snbRef.afterDismissed().subscribe(() => {
      this.config.cookieHint = true;
      console.log('cookieHint flag set', this.config);
      this.setConfig();
    });
    snbRef.onAction().subscribe(() => {
      console.log('The snack-bar action was triggered!');
    });
  }

  setConfig() {
    localStorage.setItem('cookieHint', JSON.stringify(this.config));
  }

  getConfig() {
    const config = JSON.parse(localStorage.getItem('cookieHint'));
    if (config) {
      this.config = config;
    }
  }

}
