import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-cookie-template',
  template:
    `<div
      class="cookie-hint"
      fxLayout="row"
      fxLayoutAlign="space-between center">
      <span>
        This website uses cookies to ensure you get the best experience on our website. <a [href]="url" target="_blank">Learn more</a>
      </span>
      <button 
        name="cookie-hint-button"
        mat-stroked-button (click)="snackBar.dismiss()">
        ok
      </button>
    </div>`,
  styleUrls: ['./cookie-template.component.scss']
})
export class CookieTemplateComponent {
  url = 'https://www.doozzoo.com/datenschutzerklaerung';
  constructor(public snackBar: MatSnackBar) {
    // ...
  }
}
