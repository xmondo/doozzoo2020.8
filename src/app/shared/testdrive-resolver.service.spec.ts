import { TestBed } from '@angular/core/testing';

import { TestdriveResolverService } from './testdrive-resolver.service';

describe('TestdriveResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TestdriveResolverService = TestBed.get(TestdriveResolverService);
    expect(service).toBeTruthy();
  });
});
