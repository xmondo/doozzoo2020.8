import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { AuthService } from '../shared/auth.service';
import { filter } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class LoginPageResolverService implements Resolve<any> {
  user: any;
  constructor(
    private ats: AuthService,
  ) { 
    //...
  }

  resolve(route: ActivatedRouteSnapshot) {
    // TODO: resolves firebaseAuth before routing
    return this.ats.profileMissingStatus$;
  }
}
