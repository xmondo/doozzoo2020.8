import { Component, OnInit, Input } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-user-name',
  templateUrl: './user-name.component.html',
  styleUrls: ['./user-name.component.scss']
})
export class UserNameComponent implements OnInit {
  @Input() userId: any;
  privateData: Observable<{}>;
  contactData: Observable<{}>;

  constructor(
    public db: AngularFireDatabase,
  ) {
    // ...
  }

  ngOnInit() {
    // console.log(this.userId);
    if (this.userId !== null && this.userId !== undefined ) {
      this.contactData = this.db.object('profiles/' + this.userId + '/contactData')
        .valueChanges();
      this.privateData = this.db.object('profiles/' + this.userId + '/privateData')
        .valueChanges();
    }
  }

}
