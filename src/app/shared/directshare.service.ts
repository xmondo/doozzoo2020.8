import { Injectable, EventEmitter } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable, Subscription } from 'rxjs';
import { throttleTime, filter, distinctUntilChanged, distinctUntilKeyChanged } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { MyUid } from '../shared/my-uid';
import { WindowObject } from './../apps/direct-share-multi/window-object';
import { DsEvent } from './../apps/direct-share-multi/ds-event';

@Injectable({
  providedIn: 'root'
})
export class DirectshareService {
  subscriptionContainer: Subscription = new Subscription();

  // Observable for direct share events
  directshareEmitter: EventEmitter<any> = new EventEmitter();

  /** remote emitter
   *  manage events between participants of a session -> e.g. coach remote controlling the metronome of a student
   *  Event structure:
   *  {
   *    target: 'metronome',
   *    event: {
   *      action: start | stop | open
   *      bpm: 120
   *      beat: '4/4'
   *      ----------------------------
   *      position: viewportX, viewportY
   *      scroll: scrollTop, scrollLeft
   *    }
   *  }
   */


  // Event emitter for Direct Share windows (DSWindows),
  // containing viewer apps for pdfs, looper, etc. 
  dsEvent: EventEmitter<DsEvent> = new EventEmitter();
  // Array containing all DSWindows objects
  windowsArr: Array<any> = [];

  // TODO: do we need the following
  // helper for UI events
  remoteStatusEmitter: EventEmitter<any> = new EventEmitter();

  ref: any;
  remoteSessionId: string;

  /**
   * set from classroom functions
   */
  isOwner: boolean = false;
  isOwnerId: string = null;
  userId: string = null;
  // ...
  constructor (
    // ...
    public db: AngularFireDatabase,
    public ar: ActivatedRoute,
  ) {
    // ...
    // inits DSWindows
    this.initDSWindows();
    
    // ### for debugging ###
    // this.initDSWindowsSubscribe();
    // this.initRemoteDSSender('auth0|582c7e4cedb8c73048379aad'); // coach1
    // this.initRemoteDSWindowsListener('halloWelt');
  }

  /**
   * 
   * subscribes only for window = null events,
   * those create a new WindowObject and push it to the windowsArr.
   * From the windows array, the window components will be invoked in app-direct-share-windows
   */
  initDSWindows() {
    const tempSub = this.dsEvent
      .pipe(filter(val => val.windowId === null))
      .subscribe((val: any) => {
        // strip down only to used fields
        // console.log('initDSWindows val: ', val);
        const asset = this.assetObjNormalize(val.event);

        // we are creating a pseudo UUID
        // const id = MyUid.newGuid();
        const obj: WindowObject = {
          // id: id,
          id: asset.md5Hash,
          isRemoteActive: false,
          isParent: null,
          isInFocus: false,
          asset: asset,
          width: undefined,
          height: undefined,
        };
        this.pushDSWindow(obj);

      });
    this.subscriptionContainer.add(tempSub);
  }

  dsWindowsUnsubscribe() {
    this.subscriptionContainer.unsubscribe();  
  }

  /**
   * 
   * @param window -> WindowObject
   * checks if window is already existing,
   * if not, pushes into DSWindow array
   */
  pushDSWindow(window: WindowObject) {
    if (this.windowsArr.findIndex(item => item.id === window.id) === -1) {
      this.windowsArr.push(window);
      // console.log('DSWindow ', window.id, ' created...')
    } else {
      console.warn('DSWindow ', window.id, 'exists...')
    }
  }

  // short for functions below
  initRemoteDS(remoteSessionId) {
    this.initRemoteDSSender(remoteSessionId);
    this.initRemoteDSWindowsListener(remoteSessionId);
  }

  /**
   * ### initRemoteDSSender ###
   * @param remoteSessionId --> 
   * equal to owner userId || OTSessionId,
   * (better option, because of defined lifecycle)
   * initialize for the owner only
   * Reads 1. own events generated in the DS windows
   * sends them 2. to FB, for beeing exposed to remote subscribers
   */
  initRemoteDSSender(remoteSessionId: string) {
    this.ref = this.db.object('directShare/' + remoteSessionId + '/remote');
    /** 
     * if window is parent, emits window events
     * 
     * "remoteStatus" is the global one, steered by the regarding button in classroom
     * this status sets the matrix for audio events and streams
     * its shared with the remote peers via FB.
     * Its not send via the event stream, but persisted per remote session
     * initialy set to FALSE  
    */
    // used to provide to helper functions
    // such as "setRemoteStatus"
    this.remoteSessionId = remoteSessionId;
    this.setRemoteStatus(false);

    return this.dsEvent
      .pipe(
        // bugfix release 3.00 - not really clear
        filter(payload => payload.windowId !== null),
        /*
        filter(payload => payload !== null),
        // filters initial call, where the window id is by intent null
        filter(payload => payload.windowId !== null),
        // only events from a window, switched to "Parent" mode will go through
        filter((payload: any) => payload.isParent ===  true),
        // throttles the event stream in general
        throttleTime(100),
        */
        // distinctUntilChanged(),
        // distinctUntilKeyChanged('target'),
      )
      .subscribe(payload => {
        // console.log('initRemoteDSSender payload: ', payload);
        this.ref.set(payload);
      });

  }

  /**
   * function returns an observable for the DS event stream, 
   * optionally prefiltered for the target window.
   * Should be used by the direct share multi component
   * ...
   * @param {string} window - target window of the remote sync, 
   * if not available, provides unfiltered stream of all events
   */
  remoteDSWindowsListener(
    remoteSessionId: string,
    windowId?: string
  ): Observable<any> {
    const ref = this.db.object('directShare/' + remoteSessionId + '/remote');
    if (windowId) {
      return ref.valueChanges()
        .pipe(
          filter((val: DsEvent) => val !== null),
          filter((val: DsEvent) => val.windowId === windowId)
        );
    } else {
      return ref.valueChanges()
        .pipe(filter((val: DsEvent) => val !== null));
    }
  }

  remoteDSAppsListener(
    remoteSessionId: string,
    targetApp: string
  ): Observable<any> {
    const ref = this.db.object('directShare/' + remoteSessionId + '/remote');
    return ref.valueChanges()
      .pipe(
        filter((val: DsEvent) => val !== null && val !== undefined),
        filter((val: DsEvent) => val.target === targetApp),
        distinctUntilChanged(),
      );
 
  }

  /**
   * 
   * @param remoteSessionId listens ONLY for the creation event of a new window 
   * (filter for "remote" target and a value.status of "true")
   * after the window has been created, it listens on its own for remote events
   */
  initRemoteDSWindowsListener(remoteSessionId) {
    // used to provide to helper functions
    // such as "setRemoteStatus"
    this.remoteSessionId = remoteSessionId;
    const tmpL = this.remoteDSWindowsListener(remoteSessionId);
    tmpL
      .pipe(
        filter(val => val.target === 'remote')
      )
      .subscribe(val => {
        // console.log('remoteDSWindowsListener->');

        // init new remote window
        if (val.event.action) {
          // console.log('create new remote window: ', val);

          const remoteWin: WindowObject = {
            id: val.windowId,
            isRemoteActive: true,
            isParent: false,  // as this is now a remote window at a session peer
            isInFocus: true,
            asset: val.event.value,
            width: undefined,
            height: undefined,
          };
          this.pushDSWindow(remoteWin);
          // newly created window should be on top of the others
          this.setFocus(val.windowId);
        }
      });
  }

  stopRemoteDS(remoteSessionId){
    const ref = this.db.object('directShare/' + remoteSessionId);
    ref.remove()
      .then(result => console.warn('stopRemoteDS done: ', result))
      .catch(error => console.warn('stopRemoteDS failed: ', error));
  }

  /**
   * 
   * @param obj this helper function reduces the storage object metadata, 
   * to the fields we need for the DSWindow object
   * 
   * TODO: issue!!
   * shared assets are having a diffent datamodel
   * this causes issues with normalization
   */
  assetObjNormalize(obj) {
    // handle legacy media
    // console.log('assetObjNormalize asset before: ', obj);
    // first option is default
    let md5Hash:string = obj.md5Hash ? obj.md5Hash : MyUid.newGuid();
    // to cover legacy
    if (obj.metadata) {
      if (obj.metadata.md5Hash) {
        md5Hash = obj.metadata.md5Hash;
      }
    } else if (obj.id) {
      md5Hash = obj.id;
    } 

    // TODO: investigate why metadata for shared items is missing
    // create artificial metadata obj
    if(!obj.metadata) {
      const data = {
        filename: obj.filename,
        firebaseStorageDownloadTokens: 'foo',
        md5Hash: md5Hash,
        ownerId: obj.ownerId,
      }
      obj.metadata = data;
    }

    const asset = {
      // contentType: obj.contentType,
      contentType: this.simpleContentType(obj.contentType),
      downloadLink: obj.downloadLink,
      filename: obj.filename,
      md5Hash: md5Hash,
      metadata: obj.metadata,
      size: obj.size,
      // updated: obj.updated,
      // updatedOn: obj.updatedOn,
    };
    // console.log('assetObjNormalize asset after: ', asset);
    return asset;
  }
  /**
   * 
   * @param ct simplyfies 
   */
  simpleContentType(contentType) {
    const ctArr = ['audio', 'video', 'image', 'pdf', 'text'];
    const ct = ctArr.find(item => contentType.indexOf(item) !== -1);
    return ct === undefined ? 'other' : ct;
  }

  /**
   * 
   * @param status helper to set focus on a DSWindow
   * 
   */
    // event handling
  setFocus(windowId) {
    const index = this.windowsArr.findIndex(item => item.id === windowId);
    this.windowsArr.forEach(item => item.isInFocus = false);
    this.windowsArr[index].isInFocus = true;
  }

  /**
   * shared object for setting/getting the remote status
   * TODO: currently this applies for ALL students/non owners in a session.
   * Should be configurable per dedicated participant
   * @param status 
   */
  setRemoteStatus(status: boolean) {
    const ref = this.db.object('directShare/' + this.remoteSessionId);
    ref.update({remoteStatus: status});
  }

  getRemoteStatus() {
    // observer gets value from FB as a boolean
    return this.db.object('directShare/' + this.remoteSessionId + '/remoteStatus')
      .valueChanges();
  }

  /**
   * function returns an observable, prefiltered for the target app
   * for subscription of the event object, persisted in firebase
   * it triggers a further Event Emitter, that is used to trigger UI Events
   * @param {string} target - target of the remote sync
   */
  syncRemote(target: any): Observable<any> {
    return this.ref.valueChanges()
      .pipe(
        filter(payload => payload !== null),
        filter((payload: any) => {
          // helper function for steering UI events
          // TODO: does this cause issues??!!
          this.remoteStatusEmitter.emit({ target: payload.target });
          // returns the core payload to work with
          return payload.target === target;
        }));
  }

  /**
   * used to provide throtteled events triggered by remote events,
   * that should steer UI indicators
   * TODO: rename to "remoteStatusUI"
   */
  remoteStatus(target: any): Observable<any> {
    return this.remoteStatusEmitter
      .pipe(
        filter((payload: any) => payload.target === target),
        throttleTime(2000)
      );
  }


// service end
}
