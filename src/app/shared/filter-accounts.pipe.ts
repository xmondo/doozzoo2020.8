import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterAccounts'
})
export class FilterAccountsPipe implements PipeTransform {

  transform(items: any[], filter: string): any {
    // console.log('pipe input: ', items);
    if (!items || !filter) {
      return items;
    } else if (filter.length < 3) {
      // console.log('filter: ', filter, filter.length);
      return items;
    }
    // normalize to lower case
    // filter = filter.toLowerCase();
    // filter items array, items which match and return true will be
    // kept, false will be filtered out
    return items.filter(item => {
      try {
        let str = item.email.toLowerCase() + ' ' + item.userId;
        item.name !== undefined && item.name !== null ? str += item.name.toLowerCase() : '';
        // search in coantenated string
        // console.log('pipe: ', str, str.indexOf(filter));
        return str.indexOf(filter) !== -1; 
      }
      catch(e){
        return false;
      }
    });
}

}



