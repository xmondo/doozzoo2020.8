import { Injectable } from "@angular/core";
@Injectable()
export class ToClipboard {
    constructor() {

    }
    copyToClipboard(content) { 

        const el = document.createElement('textarea');
        el.style.position = 'fixed';
        el.style.top = '-1000px';
        el.textContent = content;
        const body = document.getElementsByTagName('body')[0];
        body.appendChild(el);
        el.select();
        const result: Boolean = document.execCommand('copy');
        body.removeChild(el);

        return result;

    }
}
