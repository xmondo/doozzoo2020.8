import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterDocs',
  pure: false
})
export class FilterDocsPipe implements PipeTransform {
  transform(items: any[], filter: String): any {
      // console.log('pipe input: ', items);
      if (!items || !filter) {
        return items;
      } else if (filter.length < 3) {
        // console.log('filter: ', filter, filter.length);
        return items;
      }
      // normalize to lower case
      filter = filter.toLowerCase();
      // filter items array, items which match and return true will be
      // kept, false will be filtered out
      return items.filter(item => item.filename.toLowerCase().indexOf(filter) !== -1);
  }
}
