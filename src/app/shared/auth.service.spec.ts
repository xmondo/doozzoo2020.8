
import { TestBed, async } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { FirebaseOptionsToken } from '@angular/fire';
import { environment } from '../../environments/environment';

describe('AuthService', () => {

  let authService: AuthService;
  let afAuth: AngularFireAuth;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthService,
        AngularFireAuth,
        { provide: FirebaseOptionsToken, useValue: environment.firebase },
      ]
    });
    authService = TestBed.get(AuthService);
    afAuth = TestBed.get(AngularFireAuth);
  });

  it('should be created', () => {
    expect(authService).toBeTruthy();
  });

});
