import { Injectable } from "@angular/core";
/* used for creating human readable tokens */
@Injectable()
export class DeltaTango {
    tokens: any = <any>[];
    token: string = <string>'';
    generate: Function;
    generateTango
    constructor() {
        // tslint:disable-next-line:max-line-length
        this.tokens = ['Alfa', 'Bravo', 'Coca', 'Delta', 'Echo', 'Foxtrot', 'Gold', 'Hotel', 'India', 'Juliett', 'Kilo', 'Lima', 'Metro', 'Nectar', 'Oscar', 'Papa', 'Quebec', 'Romeo', 'Sierra', 'Tango', 'Union', 'Victor', 'Whiskey', 'eXtra', 'Yankee', 'Zulu'];
        
        // numbers
        this.generate = (numberOfSegments: number = 3) => {
            let token: string = <string>'';
            for (let i = 0; i < numberOfSegments; i++) {
                const random = Math.floor((Math.random() * this.tokens.length));
                if (i === 0) {
                    // token += this.tokens[random];
                    token += this.randomNumber();
                } else {
                    // token += '-' + this.tokens[random];
                    token += '-' + this.randomNumber();
                }
            }
            token = token.toLowerCase();
            return token;
        };

        // strings
        this.generateTango = () => {
            let token: string = <string>'';
            for (let i = 0; i < 3; i++) {
                const random = Math.floor((Math.random() * this.tokens.length));
                if (i === 0) {
                    token += this.tokens[random];
                } else {
                    token += '-' + this.tokens[random];
                }
            }
            token = token.toLowerCase();
            return token;
        };
        // generate one on ititialization
        this.token = this.generate();
    }

    randomNumberPad() {
        const n = Math.floor(Math.random() * 101);
        if (n < 10) {
             return '00' + n.toString();
        } else if  (n < 100) {
            return '0' + n.toString();
        }
    }
    randomNumber() {
        return Math.floor(Math.random() * 101);
    }
}
