import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { filter, mergeMap, tap } from 'rxjs/operators';
import { AccountStoreService } from './account-store.service';
import * as firebase from 'firebase';


@Injectable({
  providedIn: 'root'
})
export class PresenceService {

  constructor(
    private db: AngularFireDatabase,
    private accountStore: AccountStoreService,
  ) { }

  initPresence() {
    this.accountStore
      .userId$
      .pipe(
        filter(val => val !== null && val !== undefined),
        mergeMap((userId:any) => {
          console.log('++++++', userId);

          this.db
            .object('.info/connected')
            .snapshotChanges()
            .pipe(
              tap((snap:any) => console.log('...info/connected: ', snap.key))
            )
            .subscribe(
              snap => this.db.database.ref(`users/${userId}/status_`).set({state: snap.key})
            )

          const userStatusDbRef = this.db.database.ref(`users/${userId}/status`);
          const isOfflineForDatabase = {
            state: 'offline',
            last_changed: firebase.database.ServerValue.TIMESTAMP,
          };
          const isOnlineForDatabase = {
            state: 'online',
            last_changed: firebase.database.ServerValue.TIMESTAMP,
          };

          // Create a reference to the special '.info/connected' path in 
          // Realtime Database. This path returns `true` when connected
          // and `false` when disconnected.
          return this.db.database
            .ref('.info/connected')
            .once('value')
            .then(snapshot => {
              // If we're not currently connected, don't do anything.
              console.log('info/connected: ', snapshot.val())
              if (snapshot.val() == false) {
                  return;
              };

            // If we are currently connected, then use the 'onDisconnect()' 
            // method to add a set which will only trigger once this 
            // client has disconnected by closing the app, 
            // losing internet, or any other means.
            userStatusDbRef
              .onDisconnect()
              .set(isOfflineForDatabase)
              .then(function() {
                console.log('balasdfjsfs...')
                // The promise returned from .onDisconnect().set() will
                // resolve as soon as the server acknowledges the onDisconnect() 
                // request, NOT once we've actually disconnected:
                // https://firebase.google.com/docs/reference/js/firebase.database.OnDisconnect

                // We can now safely set ourselves as 'online' knowing that the
                // server will mark us as offline once we lose connection.
                userStatusDbRef.set(isOnlineForDatabase);
              });
            });

        //...
        })
      )
      .subscribe()
  }

}
