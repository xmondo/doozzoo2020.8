import { TestBed } from '@angular/core/testing';

import { AudioService } from './audio.service';
import * as Tone from 'tone';

describe('AudioService', () => {
  let audioService: AudioService;
  let tone: Tone;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AudioService,
        Tone
      ]
    });
    audioService = TestBed.get(AudioService);
    tone = TestBed.get(Tone);
  });

  it('should be created', () => {
    expect(audioService).toBeTruthy();
  });

  it('read value from local storage, should be "0.06"', () => {
    expect(audioService.masterDelay.delayTime.value).toEqual(0.06);
    // expect(audioService.masterDelay.delayTime.value).toEqual(parseInt('0.6', 10));
    localStorage.setItem('masterDelay', '0.06');
    expect(audioService.masterDelay.delayTime.value).toEqual(0.06);
  });

  it('add 2 gainNodes and then one of them twice. gainNodes.length should be 2', () => {
    const ctx = new AudioContext();
    const gain = new GainNode(ctx);
    // is the Tone library working?
    const delay = new Tone.Delay(0, 1);
    audioService.addGainNode('Microphone', gain);
    audioService.addGainNode('metronome', delay);
    audioService.addGainNode('Microphone', gain);
    expect(audioService.gainNodes.length).toEqual(2);
  });

  it('should provide global audio nodes after initialization', () => {
    audioService.initGlobalAudioNodes();
    expect(audioService.masterDelay.delayTime.value).toEqual(0.06);
    expect(audioService.masterDelay.delayTime.value).not.toEqual(null);
    expect(audioService.globalMediaStreamDestination).not.toEqual(null);
  });

  /*
  it('should rewire an audiotrack and return it', () => {
    const ctx = new AudioContext();
    // const stream = new MediaStream();
    const sdest = ctx.createMediaStreamDestination();
    // const noise = new Tone.Noise('pink').start();
    const osz = ctx.createOscillator();
    osz.type = 'square';
    osz.frequency.setValueAtTime(1000, ctx.currentTime);
    osz.start();
    osz.connect(sdest);

    const newTrack = audioService.rewireAudioTrack(sdest.stream);

    expect(typeof newTrack).toEqual('audioTrack');

  });
*/
});
