import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterContacts'
})
export class FilterContactsPipe implements PipeTransform {

  transform(items: any[], filter: string): any {
    // console.log('pipe input: ', items, filter);
    if (!items || !filter) {
      return items;
    } else if (filter.length < 3) {
      // console.log('filter: ', filter, filter.length);
      return items;
    }
    // normalize to lower case
    filter = filter.toLowerCase();
    // filter items array, items which match and return true will be
    // kept, false will be filtered out
    return items.filter(item => {
      // prevent errors in case nick is undefined
      const nick = item.contact.nickname !== undefined && item.contact.nickname !== null ? item.contact.nickname.toLowerCase() : null;
      return nick === null ? false : nick.indexOf(filter) !== -1;
    });
  }

}
