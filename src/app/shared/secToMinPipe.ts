import { Pipe, PipeTransform } from '@angular/core';
/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
*/
@Pipe({name: 'secToMin'})
export class SecToMinPipe implements PipeTransform {
  transform(value: number): string {
      // console.log('SecToMinPipe', value)
      const min = Math.floor(value / 60);
      const sec = Math.floor(value % 60);
      let secStr;
      if(sec < 10){
          secStr =  '0' + sec.toString();
      } else {
        secStr = sec.toString();
      }
      const merge = min + ':' + secStr;
    return merge;
  }
}