import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoteRingsComponent } from './remote-rings.component';

describe('RemoteRingsComponent', () => {
  let component: RemoteRingsComponent;
  let fixture: ComponentFixture<RemoteRingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RemoteRingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoteRingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
