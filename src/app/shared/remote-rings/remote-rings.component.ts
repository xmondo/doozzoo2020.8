import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-remote-rings',
  templateUrl: './remote-rings.component.html',
  styleUrls: ['./remote-rings.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,

})
export class RemoteRingsComponent {
  @Input() remoteStatus: boolean = false;
  @Input() remoteRingsLabel: string = null;
  @Input() color: string = '#000000';
  @Input() disabled: boolean = true;
  @Output() setRemoteStatus: EventEmitter<boolean> = new EventEmitter();
  
  // constructor() { }
  // ngOnInit(): void {}
}

/*

<app-remote-rings 
 [remoteStatus]="<boolean>"
 [remoteRingsLabel]="'a nice label'"
 [color]="'red'"
 [disabled]="<boolean>"
 (setRemoteStatus)="setRemoteStatusFn($event)">
</app-remote-rings>

 */
