import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BetatesterHintComponent } from './betatester-hint.component';

describe('BetatesterHintComponent', () => {
  let component: BetatesterHintComponent;
  let fixture: ComponentFixture<BetatesterHintComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BetatesterHintComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BetatesterHintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
