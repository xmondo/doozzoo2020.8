import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-betatester-hint',
  templateUrl: './betatester-hint.component.html',
  styleUrls: ['./betatester-hint.component.scss']
})
export class BetatesterHintComponent implements OnInit {
  @Input() layout: 'banner' | 'badge';
  @Input() userId: string;
  constructor(
    public ats: AuthService,
  ) { }

  ngOnInit(): void {
    this.layout = this.layout ?? 'badge';
  }

}
