import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { BehaviorSubject, from } from 'rxjs';

/** controlls emitter
   *  manage events between participants of a session -> e.g. coach remote controling the metronome of a student
   *  Event structure:
   *  {
   *    targetId: '<controlls ID>',
   *    {
   *      action: init | start | stop
   *      disabled: true | false
   *      custom: {
   *        state: miscelaneous
   *      }
   *    }
   *  }
   *
   *  Example:
   *  public cs = ControllsService
   * (click)="cs.do('buttonInstallScreensharer', { disabled: true })"
   *
   * ####
   *  (click)="cs.do('logout')"
   *
   *  cs.sync('logout')
      .subscribe(payload => {
        this.a.auth$.signOut();
      });
   */

@Injectable({
  providedIn: 'root'
})
export class ControllsService {
  // controlls visibility between components on login page
  loginEmitter: BehaviorSubject<boolean> = new BehaviorSubject(false); // EventEmitter<any> = new EventEmitter();

  // ...
  controllsEmitter: BehaviorSubject<any> = new BehaviorSubject(null);
  // orchestrates communication with
  chatEmitter: EventEmitter<any> = new EventEmitter();

  // open/close room sidenav
  roomSidenavEmitter: EventEmitter<any> = new EventEmitter(); // emitts target = content for sidenav

  constructor(
  ) { 
    // ...
  }

  do(targetId, payload) {
    const data = {
      targetId: targetId,
      payload: payload
    };
    // this.controllsEmitter.emit(data);
    this.controllsEmitter.next(data);
  }

  sync(targetId?: string): Observable<any> {
    // this subscribes to all events
    if (!targetId) {
      return this.controllsEmitter;
    // filtered for target
    } else {
      return this.controllsEmitter
      .pipe(
        filter(payload => payload !== null),
        filter((payload: any) => payload.targetId === targetId)
      );
    }
  }

  state(targetId?: string): Observable<any> {
    return this.controllsEmitter.pipe(
      filter((payload: any) => {
        return payload.targetId === targetId;
      }),
      map((payload: any) => {
        if (!payload.payload.disabled) {
          return false;
        } else {
          return payload.payload.disabled;
        }
      })
    );
  }


// ### end of class ###
}





