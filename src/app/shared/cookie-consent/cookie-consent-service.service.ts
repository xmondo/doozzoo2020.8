import { Injectable } from '@angular/core';
import { CookieConsent } from './cookie-consent.component';

//

@Injectable({
  providedIn: 'root'
})
export class CookieConsentServiceService {
  private defaultCookieConsent: CookieConsent = { 
    mandatoryCookies: true,
    trackingCookies: false, // must be "false" per default,
    timestamp: Date.now() 
  };
  constructor() { }

  /**
   * 
   * @param data -> new cookie constent data
   * if not provided, store default cookie consent
   */
  saveCookieConsent(data: CookieConsent = this.defaultCookieConsent) {
    // console.log('saveCookieConsent: ', data);
    localStorage.setItem('cookieConsent', JSON.stringify(data));
  }

  getCookieConsent() {
    const cookieConsentData = JSON.parse(localStorage.getItem('cookieConsent'));
    return cookieConsentData;
  }

  getDefaultCookieConsent() {
    return this.defaultCookieConsent;
  }
  
}
