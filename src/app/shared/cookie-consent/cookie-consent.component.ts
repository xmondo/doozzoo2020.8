/**
 * componente inserted in footer component
 * service in app.component
 */

import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CookieConsentDialogComponent } from './cookie-consent-dialog/cookie-consent-dialog.component';
import { CookieConsentServiceService } from './cookie-consent-service.service';

export interface CookieDialogData {
  cookieConsent: CookieConsent;
}

export interface CookieConsent {
  mandatoryCookies: boolean, // true
  trackingCookies: boolean, // true | false,
  timestamp: number, // <timestamp>,
}

@Component({
  selector: 'app-cookie-consent',
  templateUrl: './cookie-consent.component.html',
  styleUrls: ['./cookie-consent.component.scss']
})

export class CookieConsentComponent implements OnInit {

  cookieConsent: CookieConsent = null;

  constructor(
    public dialog: MatDialog,
    private ccs: CookieConsentServiceService,
  ) { }

  ngOnInit() {
    this.getConfig();
    if (!this.cookieConsent) {
      setTimeout(() => this.cookieDialog(), 1000);
    }
  }

  cookieDialog() {
    this.getConfig();
    const data: CookieDialogData = {
      cookieConsent: this.cookieConsent,
    };
    const myDialogRef = this.dialog.open(CookieConsentDialogComponent, {
      maxWidth: '700px',
      panelClass: 'cookie-consent-dialog-container',
      autoFocus: true,
      disableClose: true,
      data: data,
    });
  }

  getConfig() {
    const cookieConsentData = this.ccs.getCookieConsent();
    if (cookieConsentData) {
      this.cookieConsent = cookieConsentData;
    }
  }

}
