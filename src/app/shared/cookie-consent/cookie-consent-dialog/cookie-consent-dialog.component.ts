import { Inject } from '@angular/core';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CookieConsentServiceService } from '../cookie-consent-service.service';
import { CookieConsent, CookieDialogData } from '../cookie-consent.component';

//

@Component({
  selector: 'app-cookie-consent-dialog',
  templateUrl: './cookie-consent-dialog.component.html',
  styleUrls: ['./cookie-consent-dialog.component.scss']
})
export class CookieConsentDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<CookieConsentDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: CookieDialogData,
    private ccs: CookieConsentServiceService,
  ) { 

  }

  ngOnInit(): void {
    console.log(this.data);
    this.data.cookieConsent = this.data.cookieConsent === null ? 
      this.ccs.getDefaultCookieConsent() : 
      this.data.cookieConsent;
  }

  accept(option) {
    console.log('consent option: ', option);
    switch(option) {
      case 'option1':
        this.data.cookieConsent.trackingCookies = true;
        this.setConfig(this.data.cookieConsent);
        break;
      case 'option2':
        this.setConfig(this.data.cookieConsent);
        break;
    }
    this.dialogRef.close();
  }

  setConfig(cookieConsent: CookieConsent) {
    // store tracking in local storage
    this.ccs.saveCookieConsent(cookieConsent);
  }

  close(): void {
    this.dialogRef.close();
  }

}
