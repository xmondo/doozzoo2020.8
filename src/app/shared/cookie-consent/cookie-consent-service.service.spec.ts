import { TestBed } from '@angular/core/testing';

import { CookieConsentServiceService } from './cookie-consent-service.service';

describe('CookieConsentServiceService', () => {
  let service: CookieConsentServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CookieConsentServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

//
