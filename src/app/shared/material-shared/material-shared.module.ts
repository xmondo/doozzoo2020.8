import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// material imports
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [],
  /*
  imports: [
    CommonModule,
    MatButtonModule,
    FlexLayoutModule,
    MatIconModule,
  ],
  */
  exports: [
    MatButtonModule,
    FlexLayoutModule,
    MatIconModule,
  ]
})
export class MaterialSharedModule { }
