import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { ControllsService } from './controlls.service';
import { map, filter, take, subscribeOn } from 'rxjs/operators';

@Injectable({
  providedIn: "root"
})
export class DefaultGuardGuard implements CanActivate {
  user: any;
  constructor(
    public ats: AuthService,
    public router: Router,
    public cs: ControllsService
  ) {
    router.parseUrl('/login');
    this.router.createUrlTree(['/login']);
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {

    return true;
    return this.ats.profileMissingStatus$
      .pipe(map(val => {
        val ? this.router.parseUrl('/login') : '';
        return val;
      }));

  }

  // end of class
}
