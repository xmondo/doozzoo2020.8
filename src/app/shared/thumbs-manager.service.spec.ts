import { TestBed } from '@angular/core/testing';

import { ThumbsManagerService } from './thumbs-manager.service';

describe('ThumbsManagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ThumbsManagerService = TestBed.get(ThumbsManagerService);
    expect(service).toBeTruthy();
  });
});
