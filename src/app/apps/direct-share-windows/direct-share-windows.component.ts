import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import { DirectshareService } from '../../shared/directshare.service';

@Component({
  selector: 'app-direct-share-windows',
  templateUrl: './direct-share-windows.component.html',
  styleUrls: ['./direct-share-windows.component.scss']
})
export class DirectShareWindowsComponent implements OnInit, OnDestroy {
  @Input() dsSessionId: string;
  isMediaPage:boolean;
  constructor(
    public ds: DirectshareService,
    private router: Router,
  ) {
    this.isMediaPage = this.router.url.indexOf('/media') !== -1;
  }

  ngOnInit() {
    // console.log('*******', this.router.url, this.isMediaPage);
    /*
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd)
      )
      .subscribe(val => console.log(val))
    */
    // inits remote session based on the
    // not on media page !!!
    this.isMediaPage ? '' : this.ds.initRemoteDS(this.dsSessionId);
    // this.ds.initRemoteDS(this.dsSessionId);
  }

  ngOnDestroy(): void {
    // ...
    this.isMediaPage ? '' : this.ds.initRemoteDS(this.dsSessionId);
  }

}
