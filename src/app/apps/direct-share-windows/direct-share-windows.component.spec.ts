import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectShareWindowsComponent } from './direct-share-windows.component';

describe('DirectShareWindowsComponent', () => {
  let component: DirectShareWindowsComponent;
  let fixture: ComponentFixture<DirectShareWindowsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectShareWindowsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectShareWindowsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
