import { Injectable } from "@angular/core";
@Injectable()
export class TypesSupported {
    types = [
        'video/webm',
        'video/mp4',
        'video/webm;codecs=vp8',
        'video/webm;codecs=vp9',
        'video/webm;codecs=vp8.0',
        'video/webm;codecs=vp9.0',
        'video/webm;codecs=h264',
        'video/webm;codecs=H264',
        'video/webm;codecs=avc1',
        'video/webm;codecs=vp8,opus',
        'video/WEBM;codecs=VP8,OPUS',
        'video/webm;codecs=vp9,opus',
        'video/webm;codecs=vp8,vp9,opus',
        'video/webm;codecs=h264,opus',
        'video/webm;codecs=h264,vp9,opus',
        'video/x-matroska;codecs=avc1',
        'video/mp4;codecs=h264',
        'video/mp4;codecs=vp8',
        'video/mp4;codecs=vp9',
        'audio/webm',
        'audio/webm;codecs=opus',
        'audio/webm;codecs=vp9',
        'audio/webm;codecs=vp8',
        'audio/mp3',
        'audio/mp3;codecs=h264',
        'audio/mpeg',
        'audio/mpeg;codecs=h264',
    ];

    // return array populated only with supported types
    getAllTypesSupport(){
        let temp = [];
        // this.types.forEach(item => temp.push({ type: item, isSupported: MediaRecorder.isTypeSupported(item) }));
        this.types.forEach(item => {
            if(MediaRecorder.isTypeSupported(item)) {
                temp.push(item);
            }
        });
        console.log('MediaRecorder->supported codecs: ', temp);
        return temp;
    }

    isSupported(type) {
        return MediaRecorder.isTypeSupported(type);
    }
}

/**
 * MediaRecorder.isTypeSupported('video/webm')
 */
