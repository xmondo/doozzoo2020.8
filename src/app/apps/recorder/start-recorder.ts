import { OpentokService } from '../../shared/opentok.service';
import { EventEmitter, NgZone  } from '@angular/core';
import { TypesSupported } from './types-supported';
import { interval, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
// import { take } from 'rxjs/internal/operators/take';

interface config {
    maxNrOfClips: number, // 20
    maxLengthOfClips: number, // 120 in sec      
}

export class StartRecorder {
    // constraints are given with video switched on/off
    // -> easiest way to switch between both modes
    // FF has issues with enabling/disabling videoTrack,
    // as mediaRecorder does not start creating chunks
    myBlob: Blob;
    blobOptions: any; // { mimeType: 'audio/webm'};
    private chunks: Array<any> = [];
    
    // reset progress bars
    public percentRecorded: number = 0;
    public timeRecorded: number = 0;
/*
    private constraints = {
        video: true,
        audio: {
            echoCancellation: false,
            autoGainControl: false,
            noiseSuppression: false,
        }
    } 
*/
    //TODO: investigate further here
    // record screen
    private gdmOptions = {
        video: {
            cursor: "always",
            displaySurface: "monitor"
        },
        audio: {
            echoCancellation: false,
            autoGainControl: false,
            noiseSuppression: false,
        }
    }

    public myMediarecorder: MediaRecorder;
    public mediaRecorderEvents = new EventEmitter();
    private types = new TypesSupported();

    constructor(
        public videoTrackEnabled: boolean = false,
        private config: config,
        private constraints: any,
        private ots: OpentokService,
        private zone: NgZone,
        private isSafari: boolean,
    ) {
        console.log('videoTrackEnabled: ', videoTrackEnabled);
        console.log('constraints: ', this.constraints);
        // this.types.getAllTypesSupport();    
    }

    async start(){
        const self = this;

        console.log('isSafari: ', this.isSafari);
        let safariTimer: Subscription;

        // TODO: throws error in FF
        // options = this.ts.isSupported('video/webm; codecs=vp8') ? { mimeType: 'video/webm; codecs=vp8'} : { mimeType: 'video/webm'};
        // console.log('media recorder options: ', options);
        // this.blobOptions = { mimeType: 'audio/webm'};

        // in this case switch off video track
        if(!this.videoTrackEnabled) {
            this.constraints.video = false;
            // this.constraints.audio = this.ots.getConstraints().audio;
            this.blobOptions = { mimeType: 'audio/webm'}; //'audio/webm'};
        } else {
            //this.constraints.video = true; // this.ots.getConstraints().video;
            //this.constraints.audio = this.ots.getConstraints().audio;

            // TODO: this is definitively a hack but somehow working
            // const vidMime = this.isSafari ? 'video/mp4' : 'video/webm';
            const vidMime = 'video/webm';
            this.blobOptions = { mimeType: vidMime }; //'video/webm'};
        }
        const stream = await navigator.mediaDevices.getUserMedia(this.constraints);
/*
        // capture screen START
        this.gdmOptions.audio = this.constraints.audio;
        const mediaDevices = navigator.mediaDevices as any; 
        const stream = await mediaDevices.getDisplayMedia(this.gdmOptions);
        const vidMime = this.isSafari ? 'video/mp4' : 'video/webm';
        this.blobOptions = { mimeType: vidMime }; //'video/webm'};
        // capture screen END
        
        console.log('mediaStream: ', stream);
*/
        const audioTrack = stream.getAudioTracks()[0];
        // TODO: why do I need that...
        audioTrack.applyConstraints(this.constraints.audio);

        this.myMediarecorder = new MediaRecorder(stream, this.blobOptions);
        console.log('myMediarecorder: ', this.myMediarecorder);

        // ### Start() and MR events ###
    
        if(this.isSafari) {
            // Safari 13 does not yet support recording in chunks
            this.myMediarecorder.start();
            // we do it NOT event driven due missing Safari support
            this.mediaRecorderEvents.emit('start');
            // start timer for safari
            safariTimer = interval(1000)
                .pipe(take(self.config.maxLengthOfClips))
                .subscribe((val: any) => {
                    self.zone.run(() => {
                        self.timeRecorded = val;
                        self.percentRecorded = Math.floor( (val * 100) / self.config.maxLengthOfClips );
                        if(val >= self.config.maxLengthOfClips - 1) {
                            console.log('max length configured reached...')
                            self.myMediarecorder.stop();  
                            safariTimer.unsubscribe(); 
                        }
                    });
                });
        } else {
            // start recorder, chunks length = 1000ms
            this.myMediarecorder.start(1000);
            // !! onstart event not supported by safari
            this.myMediarecorder.onstart = event => {
                console.log('this.myMediarecorder.onstart, state: ', this.myMediarecorder.state, event);
                // this.mediaRecorderEvents.emit('start');
                self.zone.run(() => self.mediaRecorderEvents.emit('start'));
            };
        }

        this.myMediarecorder.ondataavailable = function(event) {
            
            // console.log('myMediarecorder->ondataavailable: ', event);

            // push recorded blobs, for Safari only the final chunk
            self.chunks.push(event.data);

            // as Safari does not support chunks, event is only fired after the complete recording is done
            if(this.isSafari) {
                // ....
                // self.zone.run(() => safariTimer.unsubscribe()); 
            } else {
                // show progress in %           
                self.zone.run(() => {
                    self.timeRecorded = self.chunks.length;
                    self.percentRecorded = Math.floor( (self.chunks.length * 100) / self.config.maxLengthOfClips );
                });
        
                // must be adapted for Safari
                // stop recoding according to config.maxLengthOfClips
                if (self.myMediarecorder.state === 'recording' && self.chunks.length > self.config.maxLengthOfClips) {
                    self.myMediarecorder.stop();
                }
            }            
        
        };

        this.myMediarecorder.onstop = function(event) {
            // console.log('Inner onstop: event, blobOptions', event, self.blobOptions);
            
            self.zone.run(() => {
                self.mediaRecorderEvents.emit('stop');
                // reset duration counter
                self.percentRecorded = 0;
                self.timeRecorded = 0;
            });

            self.isSafari ? safariTimer.unsubscribe() : '';
            
            // ### final blob object ###

            self.myBlob = new Blob(self.chunks, {type: self.blobOptions.mimeType});
            // self.recordButtonStatus = false;
            // console.log('Inner: recorder stopped');      
        };
    }

    stop() {
        this.myMediarecorder.stop();  
        console.log('stop recorder')  
    }

}
