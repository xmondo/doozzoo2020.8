import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AudioTrimmerComponent } from './audio-trimmer.component';

describe('AudioTrimmerComponent', () => {
  let component: AudioTrimmerComponent;
  let fixture: ComponentFixture<AudioTrimmerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AudioTrimmerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AudioTrimmerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
