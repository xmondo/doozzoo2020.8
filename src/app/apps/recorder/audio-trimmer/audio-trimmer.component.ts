
import { Component, OnInit, OnDestroy, AfterViewInit, ElementRef, ViewChild, Input, NgZone } from '@angular/core';
import * as WaveSurfer from 'wavesurfer.js';
import RegionsPlugin from 'wavesurfer.js/dist/plugin/wavesurfer.regions.min.js';
import * as Tone from 'tone';
import { AudioService } from '../../../shared/audio.service';
import { Downloader } from '../../../shared/downloader';
import { NgForage, Driver, NgForageCache } from 'ngforage';
import { Channelstrip } from '../../../shared/channelstrip';
import { MyUid } from '../../../shared/my-uid';
import { AudioArrayItem } from '../audioArrayItem.interface';

@Component({
  selector: 'app-audio-trimmer',
  templateUrl: './audio-trimmer.component.html',
  styleUrls: ['./audio-trimmer.component.scss'],
  providers: [
    Downloader,
    // WebWorkerService
  ]
})
export class AudioTrimmerComponent implements OnInit, OnDestroy {
  // @ViewChild('waveform') waveform: ElementRef;
  @ViewChild('waveform', { static: true }) waveform;
  @Input() item: AudioArrayItem;
  @Input() index: number;
  ws: WaveSurfer;
  ctx: Tone.BaseContext = Tone.context;
  isPlaying: Boolean = false;
  uid = MyUid.newGuid(); // used to create reused audio channel

  localProgress: Boolean = false;

  // this.inputDataUrl = this.inputItem.dataUrl;
  decodedBuffer: AudioBuffer = null;
  trimmedBuffer: AudioBuffer = null;
  regionStart;
  regionEnd;
  regionArr = [];

  channelstrip: Channelstrip;

  worker: Worker = null;

  constructor(
    public as: AudioService,
    public dl: Downloader,
    private readonly ngf: NgForage,
    private readonly cache: NgForageCache,
    private zone: NgZone,
  ) {
    // ...

  }

  ngOnInit() {
    const self = this;

    // console.log('wf: ', this.waveform.nativeElement);

    this.ws = WaveSurfer.create({
      audioContext: this.ctx,
      container: this.waveform.nativeElement,
      height: 40,
      normalize: true,
      waveColor: '#333',
      progressColor: 'rgb(0, 84, 150)', // '#efefef', rgb(0, 84, 150), orange
      minPxPerSec: 100,
      responsive: true,
      cursorWidth: 2,
      cursorColor: 'rgb(0, 84, 150)', // '#efefef',
      audioRate: 1,
      plugins: [
        RegionsPlugin.create()
      ]
    });

    //const b = new Tone.Buffer.fromArray(this.item.data);
    //this.ws.loadDecodedBuffer(b.get());
    this.as.blobToDecodedArrayBuffer(this.item.data, this.ctx)
      .then(audioArray => {
        this.ws.loadDecodedBuffer(audioArray);  
      })
    // this.ws.loadBlob(this.item.data);
    // this.loadBuffer();

    // initialization of wavesurfer
    this.ws.on('ready', () => {
      // Enable creating regions by click/dragg
      this.ws.enableDragSelection({
        loop: false
      });

      this.dispatchConnections();

      // ### connect with mixer
      // TODO: Monitor and NET are set true for each iteration of a loop
      // signal is intercepted and send to mixer via gainNode
      self.ws.toggleMute();
/*
      //self.zone.run(() => {
        self.ws.backend.disconnectFilters();
        self.channelstrip = self.as.createChannelstrip(
          self.item.title,
          -10,
          self.as.globalChannelstripStatus.toMaster,
          self.as.globalChannelstripStatus.toClass,
          self.as.globalChannelstripStatus.muted,
          self.uid
        );
        self.ws.backend.setFilter(self.channelstrip.gainObj);
      //})
*/

      
    });

    // transport UI functions
    self.ws.on('play', () => {
      // Enable creating regions by click/dragg
      self.isPlaying = true;
      // ...
      // rewire per loop iteration
      /*
      self.ws.backend.disconnectFilters();

      self.channelstrip = self.as.createChannelstrip(
        self.item.title,
        -10,
        localToMaster,
        localToClass,
        localMuted,
        self.uid
      );
      self.ws.backend.setFilter(self.channelstrip.gainObj);
      */
    });
    
    self.ws.on('pause', () => {
      // Enable creating regions by click/dragg
      self.isPlaying = false;
      /*
      try {
        self.ws.backend.disconnectFilters();
        self.channelstrip.dispose();
      } catch (error) {
        console.warn('catched->app-audio-trimmer: ', error);
      }
      */
    });
    self.ws.on('finish', () => {
      self.isPlaying = false;
    });

    // regions functions
    this.ws.on('region-created', rg => {
      // console.log(rg)
      if (self.regionArr.length > 0) {
        self.regionArr.forEach(item => item.remove());
      }
      rg.color = 'rgba(10, 10, 10, 0.5)';
      self.regionStart = rg.start;
      self.regionEnd = rg.end;
      self.regionArr.push(rg);
      rg.loop = true;
    });

    this.ws.on('region-updated', rg => {
      // console.log(rg)
      self.regionStart = rg.start;
      self.regionEnd = rg.end;
    });

  }

  dispatchConnections() {
    //const gn = new Tone.Volume().toMaster()
    //this.ws.backend.setFilter(gn);

    this.ws.backend.disconnectFilters();
    this.channelstrip = this.as.createChannelstrip(
      this.item.title,
      -10,
      this.as.globalChannelstripStatus.toMaster,
      this.as.globalChannelstripStatus.toClass,
      this.as.globalChannelstripStatus.muted,
      this.uid
    );
    this.ws.backend.setFilter(this.channelstrip.gainObj);

  }

  loadBuffer() {
    const buffer = new Tone.Buffer(
      this.item.data,
      (result) => {
        // First load buffer, when available...
        // console.log('the loadBuffer is now available', result);
        // this.loading = false;
        const myAudioBuffer = buffer.get();
        // ...init WS and apply audioBuffer
        // this.initWs();
        this.ws.loadDecodedBuffer(myAudioBuffer);
      },
      error => console.log('Tone.Buffer error: ', error)
    );
  }

  ngOnDestroy() {
    if (this.ws) {
      this.ws.destroy();
      this.channelstrip.removeFromArray();
    }
    // terminate encoding worker
    try {
      if(this.worker !== null && this.worker !== undefined) {
        this.worker.terminate();
      }
    }
    catch(e) {
      console.log(e);
    }
  }

  // generic play/pause func
  playPause = () => this.ws.playPause();

  doShorten() {
    const self = this;
    this.as.blobToDecodedArrayBuffer(this.item.data, this.ctx)
      .then(decodedArrayBuffer => {
        self.shorten(decodedArrayBuffer);
      },
      error => {
          console.log('error: ', error);
      });
  }

  shorten(decodedArrayBuffer) {
    let buffer;
    // in case of multiple subsequent trims, use the last one
    if (this.trimmedBuffer !== null) {
      buffer = this.trimmedBuffer;
    } else {
      buffer = decodedArrayBuffer;
    }

    const myAudioBuffer = this.as.trimDecodedBuffer(buffer, this.regionStart, this.regionEnd, this.ctx);

    // visualize trimmed audio, empty canvas before
    this.ws.empty();
    this.ws.loadDecodedBuffer(myAudioBuffer);
    // this.ws.playPause();

    // remember trimmed buffer for undo
    this.trimmedBuffer = myAudioBuffer;

    // remove region, not using native function
    this.regionArr.forEach(item => item.remove());
    // empty array & start/end points
    this.regionArr = [];
    this.regionStart = null;
    this.regionEnd = null;

    // reconnect channelstrip
    this.dispatchConnections();

    // free memory
    buffer = null;
    // update UI
  }

  undoTrim() {
    // simply load data from input again
    this.ws.loadBlob(this.item.data);
    this.trimmedBuffer = null;
  }

  saveTrimmedAudio = () => {
    const self = this;
    self.localProgress = true;
    // ###
    const ab: any = self.trimmedBuffer;
    const channelDataArray = [];
    for (let i = 0; i < ab.numberOfChannels; i++) {
      channelDataArray.push(ab.getChannelData(i));
    }

    if (typeof Worker !== 'undefined') {

      // Create a new worker
      this.worker = new Worker('../../../workers/encode-mp3.worker', { type: 'module' });

      // blob = data;
      this.worker.onmessage = ({ data }) => {

        // console.log('encodeMp3 result: ', data);
        // worker is ready terminate him...
        this.worker.terminate();

        self.item.data = data;
        self.item.mimeType = data.type; // 'audio/mp3';
        const str = self.item.title.split('.');
        // self.item.title = str[0] + '-region' + '.' + str[1];
        console.log('title, mimetype', str, self.item.title, data.type);
        self.item.timestamp = Date.now().toString();
        self.ngf.setItem(<any> self.item.timestamp, self.item)
          .then(ngfResult => {
            console.log('ngForage.setItem->result: ', ngfResult);
            // stop progress bar
            self.localProgress = false;
            // update clipArray
            this.as.recorderEmitter.emit({ action: 'updateClipArray' });
          });
      };

      // submit channelArray for mp3 encoding
      const _data = {
        channelDataArray: channelDataArray,
        sampleRate: this.as.getSystemSampleRate() || 44100,
      }
      this.worker.postMessage(_data);

    } else {
      // Web Workers are not supported in this environment.
      // You should add a fallback so that your program still executes correctly.
      console.log('web workers not supported...')
    }
  }

  downloadTrimmedAudio() {

    const self = this;
    // this.dl.saveData(this.item.data, this.item.title);
    // audio was already converted and stored locally
    if (this.trimmedBuffer !== null) {
      // let tmp = self.inputItem.data;
      // const blob = $audio.dataUrlToBlob(tmp);
      this.dl.saveData(this.item.data, this.item.title, this.item.mimeType);
    } else {
      // start progress bar
      this.localProgress = true;
      this.as.blobToDecodedArrayBuffer(this.item.data, this.ctx)
          .then(decodedArrayBuffer => {
            // now encode to MP3
            const ab: any = decodedArrayBuffer;
            const channelDataArray = [];
            for (let i = 0; i < ab.numberOfChannels; i++) {
              channelDataArray.push(ab.getChannelData(i));
            }

            if (typeof Worker !== 'undefined') {
              // Create a new worker
              this.worker = new Worker('../../../workers/encode-mp3.worker', { type: 'module' });

              this.worker.onmessage = ({ data }) => {
                // console.log('encodeMp3 result: ', data);
                // worker is ready terminate him...
                this.worker.terminate();
                self.item.data = data;
                self.ngf.setItem(<any> self.item.timestamp, self.item)
                  .then(ngfResult => {
                      // console.log('ngForage.setItem->result: ', ngfResult);
                      // parent -> self.updateArr();
                  });
                self.dl.saveData(data, this.item.title, data.type);
                // stop progress bar
                self.localProgress = false;
              };
              // submit channelArray for mp3 encoding
              const _data = {
                channelDataArray: channelDataArray,
                sampleRate: this.as.getSystemSampleRate() || 44100,
              }
              this.worker.postMessage(_data);

            } else {
              // Web Workers are not supported in this environment.
              // You should add a fallback so that your program still executes correctly.
              console.log('web workers not supported...')
            }
            
          },
          error => {
              console.log('error: ', error);
          });

    }
  }
}
