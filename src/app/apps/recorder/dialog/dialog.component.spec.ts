import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogRecorderComponent } from './dialog.component';

describe('DialogRecorderComponent', () => {
  let component: DialogRecorderComponent;
  let fixture: ComponentFixture<DialogRecorderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogRecorderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogRecorderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
