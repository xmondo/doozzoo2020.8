import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogData {
  clipTitle: string;
}

@Component({
  selector: 'app-dialog-recorder',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogRecorderComponent {
  constructor(
    public dialogRef: MatDialogRef<DialogRecorderComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  close(action): void {
    // console.log('action: ', action);
    const data = { action: 'cancel', data: this.data.clipTitle }
    data.action = action;
    this.dialogRef.close(data);
  }

}