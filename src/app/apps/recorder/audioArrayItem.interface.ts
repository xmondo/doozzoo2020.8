export interface AudioArrayItem {
  title: string, 
  data: Blob, 
  timestamp: string, 
  uploaded: boolean, 
  mimeType: string, // MimeType,
  pinned: boolean,
}