import { Component, OnInit, AfterViewInit, Input, ElementRef, ViewChild, EventEmitter, NgZone, OnDestroy  } from '@angular/core';
import { OpentokService } from '../../shared/opentok.service';
import { AudioService } from '../../shared/audio.service';
import { MatDialog } from '@angular/material/dialog';
import { DialogRecorderComponent } from './dialog/dialog.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { RandomMusician } from '../../shared/random-musician';
import { NgForage, NgForageConfig, Driver, NgForageCache } from 'ngforage';
import { AuthService } from '../../shared/auth.service';
import { ControllsService } from '../../shared/controlls.service';
import { AngularFireStorage } from '@angular/fire/storage';
import { TypesSupported } from 'src/app/apps/recorder/types-supported';
import { StartRecorder } from './start-recorder';
import { AudioArrayItem } from './audioArrayItem.interface';
import { DeviceDetectorService } from 'ngx-device-detector';
import { AppMode, ToolbarStoreService } from '../toolbar/toolbar-store.service';

import {
  trigger,
  state,
  style,
  animate,
  transition,
  // ...
} from '@angular/animations';
import { Observable, Subscription } from 'rxjs';

interface AVDevices {
  video: Array<any>,
  audio: Array<any>,
}

@Component({
  selector: 'app-recorder',
  templateUrl: './recorder.component.html',
  styleUrls: ['./recorder.component.scss'],
  providers: [ RandomMusician, TypesSupported ],
  animations: [
    trigger('openClose', [
      // ...
      state('open', style({
        height: '30px',
        opacity: 1
      })),
      state('closed', style({
        height: '0',
        opacity: 0,
        display: 'none'
      })),
      transition('open => closed', [
        animate('1.5s')
      ]),
      transition('closed => open', [
        animate('1.5s')
      ]),
    ]),
    trigger('openClosePreview', [
      // ...
      state('open', style({
        height: '170px',
        opacity: 1
      })),
      state('closed', style({
        height: '0',
        opacity: 0,
        display: 'none'
      })),
      transition('open => closed', [
        animate('0.5s')
      ]),
      transition('closed => open', [
        animate('0.5s')
      ]),
    ]),
  ],
})

export class RecorderComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('videoPreview') videoPreview: ElementRef;
  @Input() inputHeight: String; // static | flexible
  isAnonymous: Boolean = true;
  audioArr: Array<AudioArrayItem> = []; 
  recordButtonStatus: Boolean = false;
  uploadButtonStatus = [];
  uploadTask: any = null;
  errorMsg: String;
  percentUploaded: number = 0;
  percentRecorded: number = 0;
  timeRecorded: number = 0;
  currentIndex = -1;
  userId: string = null;
  dragPosition: any;
  // takes the mr
  mediaRecorder: any;
  subContainer: Subscription = new Subscription();

  // ### config ###
  // video track is muted by default
  videoTrackEnabled = false;
  audioVideoGroup: Boolean = true;
  constraints: any = {};
  devices: AVDevices;
  audioInputSelected: any;
  videoInputSelected: any;

  //TODO: investigate further here
  // record screen
  gdmOptions = {
    video: {
      cursor: "always",
      displaySurface: "monitor"
    },
    audio: {
      echoCancellation: false,
      noiseSuppression: false,
      sampleRate: 48000, // 44100,
      autoGainControl: false,
    }
  }
  // config of recorder
  // set defaults
  config = {
      maxNrOfClips: 20,
      maxLengthOfClips: 120, // in sec
  };
  mediaRecorderStop: Function;
  // dialog
  clipTitle: string;
  // store for recorded blob
  myBlob: Blob = null;

  // animations
  isOpen = true;

  isSafari: boolean;

  // showClips
  showClipsArr = [];

  
  // viewModel for store
  vm$: Observable<any>;
  
  constructor(
    private ots: OpentokService,
    public a: AuthService,
    private as: AudioService,
    public dialog: MatDialog,
    public rm: RandomMusician,
    private readonly ngf: NgForage,
    private readonly cache: NgForageCache,
    private storage: AngularFireStorage,
    public cs: ControllsService,
    private ts: TypesSupported,
    private zone: NgZone,
    private dds: DeviceDetectorService,
    public ngfConfig: NgForageConfig,
    public store: ToolbarStoreService,
  ) {
    // configure ngForage
    ngfConfig.configure({
      // Set the database name
      name: 'mediaRecorder',
      // Set the store name (e.g. in IndexedDB this is the dataStore)
      storeName: 'recorderItems',
      // Set default cache time to 5 minutes
      cacheTime: 1000 * 60 * 5,
      // Set the driver to indexed db if available,
      // falling back to websql
      // falling back to local storage
      driver: [
        Driver.INDEXED_DB,
        Driver.LOCAL_STORAGE,
      ]
    });

    // define constraints: audio only or audio + video
    this.constraints = this.ots.getConstraints();
    this.clipTitle = this.rm.rbMusician();

  }

  async ngOnInit() {
    this.isSafari = await this.detectIsSafari();

    // init store
    this.vm$ = this.store.vm$;

    // depends on the hardware environment
    this.gdmOptions.audio.sampleRate = this.as.getSystemSampleRate();
    
    // ngForage settings
    this.ngf.name = 'mediaRecorder';
    this.ngf.storeName = 'recorderItems';
    //this.cache.driver = Driver.LOCAL_STORAGE; // Driver.INDEXED_DB;
    await this.ngf.ready()

    await this.updateArr();

    this.audioVideoGroup = true;

    this.inputHeight = 'flexible';

    // subscibe to recorderEmitter
    // manages mp3 encoding via Lame
    const reSub = this.as.recorderEmitter.subscribe(payload => {
      // console.log('payload: ', payload);
      if (payload.action === 'updateClipArray') {
        this.updateArr();
      }
    });
    this.subContainer.add(reSub);

    this.getConfig();
    const userSub = this.a.userId$
      .subscribe(val => this.userId = val);
    this.subContainer.add(userSub);

    await this.getAVDevices();
  }

  ngAfterViewInit() {
    this.toggleClip(0);
  }

  ngOnDestroy() {
    this.subContainer.unsubscribe();
  }

  toggleMode() {
    this.store.toggleAppMode('recorderId');
  }

  async toggleVideoTrack() {
    // console.log('avg: ', this.audioVideoGroup);
    this.videoTrackEnabled = this.audioVideoGroup === true ? false : true;
    if (this.videoTrackEnabled) {
      const stream = await navigator.mediaDevices.getUserMedia(this.constraints);
      const videoEl = this.videoPreview.nativeElement;
      // console.log(videoEl, videoEl[0]);
      videoEl.srcObject = stream;
      // mute, for FF, he ignores the attribute in video tag
      videoEl.muted = true;
    }
  }

  // set on changes of form controls
  setConfig = () => {
    this.config.maxLengthOfClips = this.config.maxLengthOfClips > 600 ? 600 : this.config.maxLengthOfClips;
    localStorage.setItem('mediaRecorderConfig', JSON.stringify(this.config));
  }
  // on init read from localStorage
  getConfig = () => {
    const tmp = localStorage.getItem('mediaRecorderConfig');
    if (tmp) {
      this.config = JSON.parse(tmp);
    }
  }

  updateTitle(item) {
    //console.log(item.title);
    this.ngf.setItem(item.timestamp, item)
      .then(ngfResult => {
          //console.log('ngForage.setItem->result: ', ngfResult);
      });
  }

  record() {
    this.mediaRecorder = new StartRecorder(this.videoTrackEnabled, this.config, this.constraints, this.ots, this.zone, this.isSafari);
    this.mediaRecorder.start();

    this.isOpen = false;

    const mrSub = this.mediaRecorder
      .mediaRecorderEvents
      .subscribe(msg => {
        // msg values: start | stop
        // console.log('ee->', msg);
        // must force into the ng zone, otherwise dialog behaviours are not consitant
        this.zone.run(() => {
          if(msg === 'start'){
            this.recordButtonStatus = true;
          } else if(msg === 'stop'){
            this.recordButtonStatus = false;
            this.openDialog();
          }
        });

      });
    this.subContainer.add(mrSub);
  }
  stop() {
    this.mediaRecorder.stop();
    // fade out preview video --> more space...
    this.videoTrackEnabled = false;
    this.audioVideoGroup = true;
  }

  // dialog functions
  openDialog(): void {
    const self = this;
    this.clipTitle = this.rm.rbMusician();

    const myDialogRef = this.dialog.open(DialogRecorderComponent, {
      width: '350px',
      data: { clipTitle: this.clipTitle }
    });

    myDialogRef
      .afterClosed()
      .subscribe(result => {
        // console.log('The dialog was closed: ', result);
        result.action === 'confirm' ? self.storeItem(result.data) : console.log('canceled...');
      });
  }

  openConfirm(item): void {
    const dialogRef = this.dialog.open(ConfirmComponent, {
      width: '350px',
      data: { clipTitle: item.title }
    });
    dialogRef.afterClosed()
      .subscribe(result => {
        // console.log('The dialog was closed', result);
        // result is 'true' or undefined
        if (result === true) {
          // trigger delete
          this.delete(item);
        }
      });
  }

  // ngForage functions
  async updateArr() {
    const self = this;
    this.audioArr = [];
    const getRecords = await this.ngf
      .iterate((value: AudioArrayItem, key, iterationNumber) => {
        // handle old data model
        value.pinned === null || value.pinned === undefined ? value.pinned = false : '';
        this.audioArr.push(value);
      })
      .catch(error =>  console.log('updateArr->error: ', error));
    // this.audioArr.sort((a, b) => b.timestamp - a.timestamp);
    this.audioArr.sort((a: any, b: any) => b.pinned - a.pinned || b.timestamp - a.timestamp);
  }

  async storeItem(clipName){
    const mimeType = this.mediaRecorder.blobOptions.mimeType;
    const key = Date.now().toString(10);
    const tmpObj = {
      'title': clipName,
      'data': this.mediaRecorder.myBlob,
      'timestamp': key,
      'uploaded': false,
      'mimeType': mimeType,
      'pinned': false,
    };
    // ### store in indexDB ###
    const val = await this.ngf
      .setItem(key, tmpObj)
      .catch(error => console.log('ngf.setItem: ', error));
    // console.log('ngForage.setItem->_clipName: ', val);
    this.updateArr();    
  }

  // delete function
  async delete(item) {
    // first delete entry...
    // console.log(item);

    const result = await this.ngf
      .removeItem(item.timestamp)
      .catch(error => console.log(error));

    const index = this.audioArr.findIndex(el => {
      return el.timestamp === item.timestamp;
    });
    this.audioArr.splice(index, 1);
  }

  /**
   * 
   * @param item item: { title: String, data: any, timestamp: number, uploaded: Boolean, mimeType: MimeType };
   * @param index 
   */
  upload(item: AudioArrayItem, index) {
    // console.log(item);

    // reset progress bars
    this.percentRecorded = 0;
    this.percentUploaded = 0;

    const ref = this.storage.ref('documents/' + this.userId + '/' + item.title);
    try {
      this.uploadTask = ref.put(
        item.data,
        {
          contentType: item.mimeType,
          customMetadata: {
            ownerId: this.userId,
            filename: item.title
          }
        }
      );

      // persist, that item was uploaded
      this.updateDB(item);

    } catch (error) {
      console.log('error: ', error);
      this.errorMsg = error;
    }

    // observe percentage changes
    this.percentUploaded = this.uploadTask.percentageChanges();

    const utSub = this.uploadTask
      .snapshotChanges()
      .subscribe(payload => {
        // console.log(payload);
      }, error => {
        console.log('error: ', error);
        this.errorMsg = error;
      });
    this.subContainer.add(utSub);
  }

  // persist, that this item was uploaded
  async updateDB(item) {
    const record = await this.ngf.getItem(item.timestamp);
    item.uploaded = true;
    return this.ngf.setItem(item.timestamp, record);
  }

  async detectIsSafari() {
    // we need only to switch regarding Safari,
    // if its Safari and < version 13, app should not be shown at all
    // console.log('deviceDetector: ', this.dds.browser, this.dds.browser_version, this.dds.isMobile());
    return this.dds.browser.toLowerCase().indexOf('safari') !== -1 ? true : false;
  }  

  // showHide Clips
  toggleClip(idx){
    const state = this.showClipsArr.findIndex(item => item === idx);
    state !== -1 ? this.showClipsArr.splice(state,1) : this.showClipsArr.push(idx);
    // console.log(this.showClipsArr);
  }

  unhideClip(idx):boolean {
    return this.showClipsArr.some(item => item === idx);
  }

  async pinClip(idx){
    let state: boolean = this.audioArr[idx].pinned;
    state = !state;
    this.audioArr[idx].pinned = state;
    // Why does this throw an error -> type error
    // const record: AudioArrayItem = await this.ngf.getItem(this.audioArr[idx].timestamp);
    
    // safari has an issue with db operations, 
    // so we sort first an then Try/Catch to persist the pin
    this.audioArr.sort((a: any, b: any) => b.pinned - a.pinned || b.timestamp - a.timestamp); 

    try {
      const record: any = await this.ngf.getItem(this.audioArr[idx].timestamp);
      record.pinned = state;
      const key = record.timestamp.toString();
      await this.ngf.setItem(key, record);
    }
    catch(error){
      console.log('persist pin error: ', error)
    }

    return 'clip has been pinned...';
  }

  // ### select devices
  async getAVDevices() {
    try {
      const isFF = this.dds.browser.toLowerCase().indexOf('firefox') !== -1 ? true : false;
      // START if safari/FF, first we have to call get user Media
      // for beeing able to read out devices labels
      let dummyStream;
      if(this.isSafari || isFF) {
        this.constraints = this.ots.getConstraints();
        dummyStream = await navigator.mediaDevices.getUserMedia({ video: true, audio: true });
      }
      // END safari/ff dummy call 

      const gotDevices = await navigator.mediaDevices.enumerateDevices();
      this.devices = {
        video: gotDevices.filter(item => item.kind === 'videoinput'),
        audio: gotDevices.filter(item => item.kind === 'audioinput'),
      }
      // set always No 1 as default
      this.audioInputSelected = this.devices.audio[0]; 
      this.videoInputSelected = this.devices.video[0];
      // console.log('devices: ', this.devices);

      // now deactivate stream
      if (dummyStream !== null && dummyStream !== undefined) {
        dummyStream
          .getTracks()
          .forEach((track) => {
            track.stop();
          });
      }
    }
    catch(error){
      console.log('get AVDevices -> ', error)
    }
    return this.devices;
  }

  changeAV(type, event) {
    // console.log(type, event);
    if(type === 'video') {
      this.constraints.video = {
        deviceId: { ideal: event.value.deviceId }
      }
    } else {
      this.constraints.audio = {
        deviceId: { ideal: event.value.deviceId }
      }
    }
    // refresh video preview
    this.toggleVideoTrack();
  }

// end of class
}