import { Component, OnInit, AfterViewInit, Input, ViewChild, ElementRef } from '@angular/core';
import { AudioService } from '../../../shared/audio.service';

@Component({
  selector: 'app-video-trimmer',
  templateUrl: './video-trimmer.component.html',
  styleUrls: ['./video-trimmer.component.scss']
})
export class VideoTrimmerComponent implements OnInit, AfterViewInit {
  // @ViewChild('videoTrimmerVideo', { static: false }) videoTrimmerVideo: ElementRef;
  @Input() item: { title: String, data: Blob, timestamp: number, uploaded: Boolean, mimeType: MimeType };
  @Input() index: number;
  dataUrl = null;
  constructor(
    private as: AudioService
  ) { }

  ngOnInit() {
    // ...
  }

  ngAfterViewInit() {
    // const videoEl = this.videoTrimmerVideo.nativeElement;
    const videoEl: any = document.getElementById('videoTrimmerVideo');
    // const blob = this.item.data;

    // as of now only working for Safari
    // videoEl.srcObject = blob;
    // for the rest...
    // videoEl.src = window.URL.createObjectURL(this.item.data);
    
    // working in Chrome, FF, Safari ^13
    this.as.blobToDataUrl(this.item.data)
      .then(result => {
        // videoEl.srcObject = result;
        // videoEl.src = result;
        // console.log(result)
        this.dataUrl = result;
      });
    
  }

}
