import { Component, OnInit } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';
import { SessionStoreService } from 'src/app/room/session-store.service';

@Component({
  selector: 'app-toolbar-page',
  templateUrl: './toolbar-page.component.html',
  styleUrls: ['./toolbar-page.component.scss'],
  providers: [ 
    // dummy service, is not used at all on apps page, but serves as a dummy, so that the same component (toolbar) also works standalone
    SessionStoreService,
  ]
})
export class ToolbarPageComponent implements OnInit {
  isMobile: boolean;
  constructor(
    private deviceService: DeviceDetectorService,
  ) { }
  ngOnInit(): void { 
    this.isMobile = this.deviceService.isMobile();
  }
}
