import { Injectable } from '@angular/core';
import { AppMode, Apps, ToolbarMode, ToolbarState } from './toolbar-store.service';

// local service only provided to this component
@Injectable()

export class AppsPositionService {

  layout: string = 'session';

  // defines default sequence of apps
  appsArr: Apps[] = [
    { mode: AppMode.default, appId: 'metronomeId' },
    { mode: AppMode.default, appId: 'tunerId' },
    { mode: AppMode.default, appId: 'keyboardId' },
    { mode: AppMode.default, appId: 'recorderId' },
    { mode: AppMode.default, appId: 'mixerId' }
  ];

  toolbarState: ToolbarState = {
    open: false,
    mode: ToolbarMode.maxi,
    height: 350,
    apps: this.appsArr,
  }

  localStorageTSId: string = 'toolbarState';

  constructor() {}

  initToolbarState() {
    const tempToolbarState = JSON.parse(localStorage.getItem(this.localStorageTSId));
    this.toolbarState = tempToolbarState ?? this.toolbarState;
    // console.log('not defined');
    this.saveToolbarState(this.toolbarState);
    // console.log('### ', this.toolbarState);
    return this.toolbarState;
  }

  saveToolbarState(toolbarState: ToolbarState) {
    localStorage.setItem(this.localStorageTSId, JSON.stringify(toolbarState));
  }

  // end of class
}

/**
 * requirements
 * - as a user I would like to order my apps in the toolbar freely
 * - as a user I would like to edit this order per drag and drop
 * - as a user I would like to see the edited order when I open the session next time
 * 
 * - use flexorder
 * - user drag along horizontal orientation of toolbar, when dragging flexorder is dynamically reassigned and stored
 * - store flexorder in localstorage
 * 
 * - tablet/mobile case???
 * 
 */
