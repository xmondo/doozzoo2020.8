import { Component, OnInit, OnDestroy, Renderer2, Input } from '@angular/core';
import { OpentokService } from '../../shared/opentok.service';
import { Router } from '@angular/router';
import { DirectshareService } from '../../shared/directshare.service';
import { distinctUntilChanged, distinctUntilKeyChanged, filter, map } from 'rxjs/operators';
import { Subscription, Observable, BehaviorSubject } from 'rxjs';
import { ControllsService } from '../../shared/controlls.service';
import { DsEvent } from 'src/app/apps/direct-share-multi/ds-event';
import { DeviceDetectorService } from 'ngx-device-detector';
import { AppsPositionService } from './apps-position.service';
import { AudioService } from 'src/app/shared/audio.service';
import { AppMode, ToolbarMode, ToolbarStoreService } from './toolbar-store.service';
import { SessionStoreService } from 'src/app/room/session-store.service';
import { DragRef } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
  providers: [
    AppsPositionService,
    ToolbarStoreService,
  ],
})
export class ToolbarComponent implements OnInit, OnDestroy {
  @Input() toolbarVariant: 'apps' | 'session'; // apps as fallback
  @Input() isMobile: boolean;
  // subscriptions
  subContainer = new Subscription();
  // must be dedicated for beeing able to unsubscribe
  remoteListenerSub: Subscription;

  fxLayoutAlignValue: String;

  toolbarMaxHeight: number;
  toolbarMinHeight: number = 200;
  toolbarMinimizeThreshold: number = 120; // deduct footer height

  vm$: Observable<any>;
  toolbarMode: ToolbarMode;
  isSender: boolean = false;

  myTop: number = null;

  // handlePosition$: EventEmitter<any> = new EventEmitter();
  handlePosition: BehaviorSubject<number> = new BehaviorSubject(350);
  handlePosition$ = this.handlePosition
    .asObservable()
    //.pipe(debounceTime(10));

  constructor(
    private ots: OpentokService,
    private router: Router,
    public ds: DirectshareService,
    public cs: ControllsService,
    public deviceService: DeviceDetectorService,
    public appsPosition: AppsPositionService,
    private renderer: Renderer2,
    private ads: AudioService,
    private store: ToolbarStoreService,
    private sessionStore: SessionStoreService,
  ) {
    // ...
    
    this.vm$ = this.store.vm$;
    this.fxLayoutAlignValue = 'start';

  }

  ngOnInit() {
    // console.log('init toolbar, toolbar variant: ', this.toolbarVariant);

    this.observeHandlePosition();

    if (this.toolbarVariant === 'apps') {
      // for apps page set default open
      this.store.setOpen(true);
      /**
       * as any app could be muted as a result of beeing student and 
       * just comming from a session with coach to student settings,
       * we unmute here...
       */
      this.ads.switchChannelstripAll(true,false,false);
    } else {
      // set default channel settings for microphone
      this.ads.findChannelstripId('microphoneId').setLevel(-5);
      // needed? should avoid that setting microphone on mute in a previous session, 
      // will prevent the mic to be open in a subsequent session
      this.ads.findChannelstripId('microphoneId').switchAll(false, false, false);
    }

    // subscription of the complete state
    // this subscribtion syncs out model and all related changes to localStorage
    const storeAppsSub = this.store
      .height$
      // .pipe(take(1))
      .subscribe(height => {
        // ??? saw float here
        this.handlePosition.next(Math.round(height));
      });
    this.subContainer.add(storeAppsSub);

    // only trigger specific functions, e.g. remote, in session/room
    this.toolbarVariant === 'session' ? this.initInSessionFunctions() : '';

  }

  ngOnDestroy() {
    this.subContainer.unsubscribe();
    this.remoteListenerSub ? this.remoteListenerSub.unsubscribe() : '';
  }

  initInSessionFunctions() {
    /**
     * subscribe to controlls emitter, which listens for button clicks
     * emit to the remote service
     */
    this.cs.controllsEmitter
    .pipe(
      filter(payload => payload === 'toolbar'),
    )
    .subscribe(() => {
      // is only a trigger
      this.store.toggleOpen();
    });

    /*
    // init remote listener for non owners,
    // MUST be initialized onInit
    // but only if activated from coach side
    const sessionStoreSub = this.sessionStore
      .myToolbarStatus$
      .subscribe(val => {
        if(val) {
          console.log('toolbar->initRemoteListener');
          this.initRemoteListener(); 
        } else {
          this.stopRemoteListener(); 
          this.store.setOpen(false);
        }
      });
    this.subContainer.add(sessionStoreSub);
    */

    const sessSub = this.sessionStore
      .vm$
      .pipe(
          distinctUntilKeyChanged('myToolbarStatus'),
      )
      .subscribe(val => {
          const remoteStatus = val.state.remoteStatus || false;
          this.isSender = val.isSender || false;
          const myToolbarStatus = val.myToolbarStatus || false;
          if(myToolbarStatus) {
              // console.log('toolbar->initRemoteListener()');
              this.initRemoteListener();
          } else {
              // console.log('toolbar->stopRemoteListener()');
              this.stopRemoteListener();
              this.store.setOpen(false);
          }
      })
    this.subContainer.add(sessSub);

    // ###  handle open close subscription of toolbar from store ###
    const storeOpenClose = this.store
      .open$
      .pipe(distinctUntilChanged())
      .subscribe(val => {
        // console.log('***storeOpenClose: ', val)
        // set height
        // val ? this.handlePosition.next() : '';
        const action = val ? 'open' : 'close';
        this.composeEvent({ action: 'openClose', value: action });
      });


    // makes sure that coach/student have activated apps, when beeing in 1:1 session

    // see comments below
    const anyToolbarStatusSet = this.sessionStore
      .anyToolbarStatusSet$
      .subscribe(val => {
        // console.log('dispatchDefaultSettings->anyToolbarStatusSet: ', val);
        !val ? this.sessionStore.activateFirst(true) : '';
      });
    this.subContainer.add(anyToolbarStatusSet);
  }

  toggleMode() {
    this.store.toggleMode();
  }

  toggleOpen() {
    this.store.toggleOpen();
  }

  // stores position of apps after dragging
  dropApps(event) {
    this.store.moveAppsInArray(event);
    this.store.saveToLocalStorage();
  }

  // remote
  composeEvent(value: DsEvent['event']) {
    // console.log('ToolbarComponent->composeEvent: ', value)  
    if(this.isSender) {
      this.ds.dsEvent.emit({
        senderId: this.ds.userId,
        isParent: this.ds.isOwner, 
        target: 'toolbar',
        event: value // 'open | close'
      });
    }
  }

  // relevant for child windows with "isParent = false"
  initRemoteListener() {
    // used to provide to helper functions
    // such as "setRemoteStatus"
    this.remoteListenerSub = this.ds.remoteDSAppsListener(
        this.ds.remoteSessionId,
        'toolbar'
    )
    .subscribe(val => {
        // console.log('remoteDSAppsListener->toolbar: ', val);
        switch (val.event.value) {
            case 'open':
              // this.toolbarStatus = true;
              this.store.setOpen(true);
              break;
            case 'close':
              // this.toolbarStatus = false;
              this.store.setOpen(false);
              break;
        }
    });
  }

  stopRemoteListener() {
    this.remoteListenerSub ? this.remoteListenerSub.unsubscribe() : '';
  }

  // ###########

  moveHandle(event) {
    // console.log(event)
    const height = this.calcHeight(event);
    // const height = -(event.pointerPosition.y) + 50;
    // console.log('moveHandle->height: ', height);
    // Observable for subscribing the height of the toolbar area
    this.handlePosition.next(height);
    // set mini/maxi modes regarding to drag actions
    // this.handleToolbarModes(height);
  }

  observeHandlePosition() {
    const minMaxiSub = this.handlePosition$
      .pipe(
        // debounceTime(10),
        // distinctUntilChanged(),
        map(val => val > this.toolbarMinimizeThreshold ? 'maxi' : 'mini'),
        distinctUntilChanged(),
      )
      .subscribe(mode => this.handleToolbarModes(mode));
    this.subContainer.add(minMaxiSub);
  }

  calcHeight(event){
    const footerHeight = 74; // 50px footer + 24px padding top
    //console.log(event);
    // defines dom rect of the handle bar
    const domrect = event.source.element.nativeElement.getBoundingClientRect();
    // console.log('bottom, top: ', domrect.bottom, domrect.top, domrect);
    //const toolbarContainerDomrect = this.toolbarContainer.nativeElement.getBoundingClientRect();
    const height = window.innerHeight - domrect.top - footerHeight;
    return height;
  }

  handleToolbarModes(mode) {
    if (mode === 'maxi') {
      this.fxLayoutAlignValue = 'start';
      // this.store.setAppModeAll(AppMode.default);
      // when switching to maxi, we should lookup what was stored to local storage
      // this.store.initToolbarStateFromLST();
      this.store.setMode(ToolbarMode.maxi);
    } else {
      this.fxLayoutAlignValue = 'start';
      this.store.setAppModeAll(AppMode.small);
      this.store.setMode(ToolbarMode.mini);
    }
  }

  moveHandelStopped(event) {
    const height = this.calcHeight(event);
    this.store.setToolbarHeight(height);
    this.store.saveToLocalStorage();
    event.source._dragRef.reset();
  }

  /**
   * @param point Gets called with a point containing the current position of the user's pointer on the page and should 
   * @param ref 
   * @returns constrained point, where x/y can be limited programatically to specific borders
   */
  constrainDragPosition(point: any, ref: DragRef) {
    // console.log(point, ref);
    let _point = point;
    if(point.y > 10 && point.y < (window.innerHeight - 150)) {
      _point.y = point.y;
    } else if (point.y > (window.innerHeight - 160)) {
      _point.y = window.innerHeight - 160;
    } else if (point.y < 10) {
      _point.y = 10;
    }
    //return
    return _point;
  }
  
// end of class
}
