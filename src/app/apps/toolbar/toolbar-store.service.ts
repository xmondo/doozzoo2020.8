// ...
import { moveItemInArray } from '@angular/cdk/drag-drop';
import { Injectable } from '@angular/core';
import { ComponentStore } from '@ngrx/component-store';
import { Observable } from 'rxjs';
import { AppsPositionService } from './apps-position.service';

export const enum AppMode {
  default = 'default',
  small = 'small',
  tiny = 'tiny',
}

export interface Apps {
  mode: AppMode,
  appId: string,
  order?: number, // sort order, do we really need that or use array index?
}

export const enum ToolbarMode {
  mini = 'mini',
  maxi = 'maxi',
}

export interface Remote {
  active: boolean,
  isOwner: boolean,
  senderReceiver: 'sender' | 'receiver',
}

export interface ToolbarState {
  open: boolean,
  mode: ToolbarMode,
  height: number,
  apps: Apps[],
  // remote: Remote,
}

@Injectable()
export class ToolbarStoreService extends ComponentStore<ToolbarState> {
  constructor(
    private appsPositions: AppsPositionService,
  ) {
    super({ 
      open: false,
      mode: ToolbarMode.maxi, 
      height: appsPositions.initToolbarState().height,
      // uses appsPositions service to read positions from local storage
      apps: appsPositions.initToolbarState().apps,
    });
  }

  // *********** Selects ************ //
  public readonly open$: Observable<boolean> = this.select(state => state.open);
  private readonly mode$: Observable<ToolbarMode> = this.select(state => state.mode);
  public readonly height$: Observable<number> = this.select(state => state.height);
  public readonly apps$: Observable<Apps[]> = this.select(state => state.apps);

  // ViewModel for the component
  readonly vm$ = this.select(
    this.open$,
    this.mode$,
    this.height$,
    this.apps$,
    (open, mode,height, apps) => ({
      open, 
      mode,
      height,
      apps
    })
  );

  // *********** Updaters *********** //
  readonly setMode = this.updater((state: ToolbarState, value: ToolbarMode) => ({
    ...state,
    mode: value || ToolbarMode.maxi, // maxi as a fallback
  }));

  readonly toggleMode = this.updater((state: ToolbarState) => ({
    ...state,
    mode: state.mode === ToolbarMode.maxi ? ToolbarMode.mini : ToolbarMode.maxi, // toggle between two values
  }));

  readonly setOpen = this.updater((state: ToolbarState, value: boolean) => ({
    ...state,
    open: value, 
  }));

  readonly toggleOpen = this.updater((state: ToolbarState) => ({
    ...state,
    open: !state.open, // toggle between open/close
  }));

  readonly setApps = this.updater((state: ToolbarState, value: Apps[]) => ({
    ...state,
    apps: value, // update the complete reordered array
  }));

  readonly toggleAppMode = this.updater((state: ToolbarState, appId: string) => {
    // find specific app fitting to appId and toggle mode between "default/small"
    // update complete appsArr
    const tempApps = state.apps;
    const mode = tempApps.find(item => item.appId === appId).mode;
    tempApps.find(item => item.appId === appId).mode = mode === AppMode.default ? AppMode.small : AppMode.default;
    this.appsPositions.saveToolbarState(state);
    return ({
      ...state,
      apps: tempApps,
    })
  });

  readonly setAppMode = this.updater((state: ToolbarState, value: { appId: string, appMode: AppMode }) => {
    // find specific app fitting to appId and set "default/small"
    const tempApps = state.apps;
    tempApps.find(item => item.appId === value.appId).mode = value.appMode;
    return ({
      ...state,
      apps: tempApps,
    })
  });

  readonly setAppModeAll = this.updater((state: ToolbarState, value: AppMode) => {
    // find specific app fitting to appId and set "default/small"
    const tempApps = state.apps;
    tempApps.forEach(item => item.mode = value);
    return ({
      ...state,
      apps: tempApps,
    })
  });

  readonly setToolbarHeight = this.updater((state: ToolbarState, value: number) => {
    // this.appsPositions.saveToolbarState(state);
    return ({
      ...state,
      height: value, // update height, which is in reality a css top: number
    })
  });

  readonly saveToLocalStorage = this.updater((state: ToolbarState) => {
    this.appsPositions.saveToolbarState(state);
    return state;
  });

  // moveItemInArray(this.store., event.previousIndex, event.currentIndex);
  readonly moveAppsInArray = this.updater((state: ToolbarState, event:any) => {
    const tempApps = state.apps;
    moveItemInArray(tempApps, event.previousIndex, event.currentIndex);
    return ({
      ...state,
      apps: tempApps,
    })
  });


}
