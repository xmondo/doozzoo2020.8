import { TestBed } from '@angular/core/testing';

import { AppsPositionService } from './apps-position.service';

describe('AppsPositionService', () => {
  let service: AppsPositionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AppsPositionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
