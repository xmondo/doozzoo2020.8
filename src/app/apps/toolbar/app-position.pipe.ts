import { Pipe, PipeTransform } from '@angular/core';
import { Apps } from './toolbar-store.service';

@Pipe({
  name: 'appPosition',
  pure: false
})
export class AppPositionPipe implements PipeTransform {
  /**
   * 
   * @param appsArr array which contains the DOM Ids of the apps in toolbar
   * @param appId specific dom Id which should be located in index
   * @returns position of dom id in apps array, used to define the regarding order in toolbar and layout mode of app
   */
  transform(appsArr: Apps[] = [] , appId: string = '') {
    // console.log('*** ', appsArr, appId)
    // return appsArr.findIndex(item => item.appId === appId);
    const data = {
      order: appsArr.findIndex(item => item.appId === appId),
      mode: appsArr.find(item => item.appId === appId).mode,
    }
    return data;
  }
}

/**
 * 
 */
