import { Component, OnInit } from '@angular/core';
import { values } from 'lodash';
import { Observable } from 'rxjs';
import { distinctUntilChanged, filter, map, take, takeLast } from 'rxjs/operators';
import { ParticipantList } from 'src/app/my-students/participant-list/participant-list.component';
import { SessionStoreService } from 'src/app/room/session-store.service';
import { DirectshareService } from 'src/app/shared/directshare.service';
import { isTemplateExpression } from 'typescript';
import { DsEvent } from '../../direct-share-multi/ds-event';
// import { ToolbarStoreService } from '../toolbar-store.service';

export interface ParticipantCheck {
  key: ParticipantList['key'],
  checked: boolean,
}

@Component({
  selector: 'app-activate-apps',
  templateUrl: './activate-apps.component.html',
  styleUrls: ['./activate-apps.component.scss']
})
export class ActivateAppsComponent implements OnInit {
  vm$: Observable<any>;
  constructor(
    private sessionStore:  SessionStoreService,
    // private toolbarStore: ToolbarStoreService,
    public ds: DirectshareService,
  ) { 
    // make the store view model available
    this.vm$ = this.sessionStore.vm$;
  }

  ngOnInit(): void {
    // ...
  }

  /**
   * 
   * @param item 
   * @param checked 
   */
  doActivateApps(item: ParticipantList , checked: boolean) {
    console.log('doActivateApps: ', item.key, checked)
    this.sessionStore.setParticipantApps({ key: item.key, checked: checked});
    // this event makes sure, that the counterpart student gets a triggert to open his toolbar in any case, 
    // when this option is activated for him
    this.composeEvent({ action: 'openClose', value: 'open' });
  }

  // remote
  composeEvent(value: DsEvent['event']) {
    console.log('ToolbarComponent->composeEvent: ', value)  
    this.ds.dsEvent.emit({
        senderId: this.ds.userId,
        // windowId: 'metronome',
        // we assume that always and only the owner of the room steers the metronome
        isParent: this.ds.isOwner, 
        target: 'toolbar',
        event: value // 'open | close'
    });
  }

}

/**
 * assigning access to apps on student side should not generate additional complexity and make things complicated
 * approach:
 * - participant state is persisted in sessionState and hence independent of toolbar component open/close
 * - we have to handle the following cases
 *  - participant array (parr) length = 0 -> nothing to do
 *  - parr.length = 1 OR > 1 -> her we have to check...
 *  - ... ANY toolbarStatus atrributes set (!== null) -> if so, this attribute was once set in the so far running session lifecycle, hence we change NOTHING as this should be the intended status for the participant(s). This should be valid for 1 participant and > 1 participants scenarios
 *  - NO toolbarStatus atrributes set (=== null/undefined) -> if so ...
 *    - ...set toolbarStatus = true for 1 participant
 *    - ...set parr[0].toolbarStatus = true for > 1 participants
 * - if parr.length > 0, and none have toolbarStatus = true, give a hint: "students have no access..."
 * - do not show the hint, if at least one student is activated
 */