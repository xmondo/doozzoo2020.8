import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'participantsActive'
})
export class ParticipantsActivePipe implements PipeTransform {
  /**
   * 
   * @param participants array of participants from SessionStoreService
   * @returns the number of participants in that array, which have the toolbar status set to true
   */
  transform(participants: Array<any>):number {
    return participants.filter(item => item.toolbarStatus === true).length ?? 0;
  }

}
