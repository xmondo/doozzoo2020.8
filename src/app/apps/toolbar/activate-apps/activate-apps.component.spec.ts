import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivateAppsComponent } from './activate-apps.component';

describe('ActivateAppsComponent', () => {
  let component: ActivateAppsComponent;
  let fixture: ComponentFixture<ActivateAppsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActivateAppsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivateAppsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
