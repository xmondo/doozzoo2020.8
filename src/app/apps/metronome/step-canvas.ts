import { ElementRef, EventEmitter } from '@angular/core';

export class StepCanvas {

    el: ElementRef;
    stepLevels: Array<number>;
    canvasEl: ElementRef;
    ctx: any;

    padding: number;
    nrLevels: number;

    cubeWidth: number;
    cubeHeight: number;
    w: number;
    h: number;

    c1: string;
    c2: string;
    bg: string;
    hl: string;

    canvasEventEmitter: EventEmitter<any>; // = new EventEmitter();

    i = 0;

    constructor(
        el: any, 
        w: number, 
        h: number, 
        stepLevels: Array<number>, 
        c1: string = 'rgb(63,81,181)', 
        c2: string = '#888', 
        bg: string = '#efefef', 
        hl: string = 'rgb(255,110,64)',
        padding: number = 2,
        nrLevels: number = 3,
        //  canvasEventEmitter: EventEmitter<any> = new EventEmitter(),
        // renderer can be used but must then explicitly injected during initiation of the class
        // private renderer: Renderer2,
    ) {
        this.w = w;
        this.h = h;

        this.stepLevels = stepLevels;
        this.canvasEl = el;
        
        this.ctx = el.getContext('2d');
        this.ctx.canvas.width = w;
        this.ctx.canvas.height = h;

        this.padding = padding;
        this.nrLevels = nrLevels;

        this.c1 = c1;
        this.c2 = c2;

        this.bg = bg;
        this.hl = hl;

        this.canvasEventEmitter = new EventEmitter();
        
        this.listener(this.canvasEl);
        this.init();
    }

    init() {
        // init functions
        this.drawInit();
    }

    toggleStepLevels(pointer) {
        let current = this.stepLevels[pointer];
        if (current < this.nrLevels) {
            current++;
        } else {
            current = 0;
        }
        this.stepLevels[pointer] = current;
        console.log('currentClick: ', current);
        this.draw(pointer, this.hl, 2);
    };

    listener(el) {
        // TODO:renderer2 does not work?!
        // assigning external function does not work?!
        el.addEventListener('click', ev => {
            // click event from canvas element

            this.cubeWidth = this.w / this.stepLevels.length;

            console.log('step-canvas->listener->this.stepLevels: ', this.stepLevels);
            const tempArr = this.stepLevels.map((item, index) => [this.cubeWidth * index, (this.cubeWidth * index) + this.cubeWidth]);
            const pointer = tempArr.findIndex(item => ev.offsetX > item[0] && ev.offsetX < item[1]);
            console.log('step-canvas->listener->pointer: ', pointer);
            this.toggleStepLevels(pointer);

            // emit click event for pushing changes to tone transport/scheduler
            this.canvasEventEmitter.emit(this.stepLevels);

            // put changes in local storage
            // this.setConfig();

        }, true);
        // });
    };

    draw(step, color, _padding = 0) {

        this.cubeWidth = this.w / this.stepLevels.length;
        this.cubeHeight = this.h / this.nrLevels;

        let x1 = this.cubeWidth * step;
        x1 = x1 + _padding;
        let y1 = this.cubeHeight * (this.nrLevels - this.stepLevels[step]);
        y1 = y1 + _padding;
        let x2 = this.cubeWidth;
        x2 = x2 - _padding;
        let y2 = this.cubeHeight * this.stepLevels[step];
        y2 = y2 - _padding;
        // fill current pointer
        // either clear the bg, or...
        // ctx.clearRect(x1, 0, x2, h);
        // ...fill with bgColor
        this.ctx.fillStyle = this.bg;
        this.ctx.fillRect(x1, 0, x2, this.h);
        this.ctx.fillStyle = color;
        this.ctx.fillRect(x1, y1, x2, y2);
    };

    stepDraw(step) {

        // draw current pointer
        this.draw(step, this.c1, 2);

        let prev = step - 1;
        if (step === 0) {
            prev = this.stepLevels.length - 1;
        }
        // clear prev
        // not for the very first step
        if (this.i !== 0) {
            this.draw(prev, this.c2, 2);
        }
        this.i++;
    };

    // init background
    // drawInit(bg, w, h) {
    drawInit() {
        this.ctx.fillStyle = this.bg;
        this.ctx.fillRect(0, 0, this.w, this.h);
        this.stepLevels.forEach((item, index) => {
            this.draw(index, this.c2, 2);
        });
    };

    // TODO: isnt that redundant???
    /*
    setConfig() {
        let current = JSON.parse(localStorage.getItem('metronome'));
        if(!current) {
            current = {
                bpm: 120,
                beat: '4/4',
                stepLevels: [3, 1, 2, 1] // default    
            }
        } else {
            current.stepLevels = this.stepLevels;
        }
        localStorage.setItem('metronome', JSON.stringify(current));
        console.log('step-canvas->config set: ', current);
    }
    */
}
