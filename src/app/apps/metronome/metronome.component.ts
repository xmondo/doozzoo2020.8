import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild, ElementRef, Renderer2, Input, } from '@angular/core';
import * as Tone from 'tone';
import { AudioService } from 'src/app/shared/audio.service';
import { DirectshareService } from 'src/app/shared/directshare.service';
import { AuthService } from 'src/app/shared/auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { ControllsService } from 'src/app/shared/controlls.service';
import { Subscription, Observable } from 'rxjs';
import { take, map, filter, distinctUntilChanged, throttleTime, distinctUntilKeyChanged } from 'rxjs/operators';
import { DsEvent } from 'src/app/apps/direct-share-multi/ds-event';
import { StepCanvas } from 'src/app/apps/metronome/step-canvas';
import { TranslateService } from '@ngx-translate/core';
import { Channelstrip } from 'src/app/shared/channelstrip';
import { AppMode, ToolbarStoreService } from '../toolbar/toolbar-store.service';
import { SessionStoreService } from 'src/app/room/session-store.service';

@Component({
  selector: 'app-metronome',
  templateUrl: './metronome.component.html',
  styleUrls: ['./metronome.component.scss']
})

export class MetronomeComponent implements OnInit, AfterViewInit, OnDestroy {
    @Input() toolbarVariant;
    @ViewChild('myCanvasParent', { static: true }) myCanvasParent: ElementRef;
    @ViewChild('bpmInput', { static: true }) bpmInput: ElementRef;
    // defaults

    // subscriptions
    getRemoteStatusSub: any;
    subContainer: Subscription = new Subscription();

    loading: boolean = true;

    metronomeRunning = false;
    workerRunning = false;
    beats = ['1/1', '2/2', '4/4', '3/4', '5/4', '6/4', '7/4', '8/4', '9/4'];
    stepLevels: Array<number> = [3, 1, 2, 1]; // default
    // preSelectedBeat;
    bpmToggle = false;
    step = 0;
    stepPulseCanvas;

    // Tone
    sampler: Tone.Sampler;
    synth: Tone.Synth;
    loop: Tone.Loop;
    metronomeGain: Tone.Volume;
    channelstrip: Channelstrip;

    // store selection
    config;
    bpm: any; // TODO: should be number;
    selectedBeat;
    // tap function
    tapArr = [];
    newArr = [];
    tapAvrg = 0;

    sliderValue: Number = 90;

    position: any = { pageX: Number, pageY: Number };
    dragPosition: any;

    payload: any = 'none';
    remoteStatus: boolean = false;
    isSender: boolean = false;
    userId: string;
    subscription: Subscription = new Subscription();
    remoteSub: Subscription = new Subscription();
    listenerSub: Subscription = new Subscription();
    showRemoteCheckbox: boolean = false;

    translatedTitle: string = 'Metronome';

    vm$: Observable<any>;
    sessionStoreVm$: Observable<any>;

    constructor(
        private ads: AudioService,
        private ds: DirectshareService,
        public db: AngularFireDatabase,
        public router: Router,
        public ar: ActivatedRoute,
        public cs: ControllsService,
        private auth: AuthService,
        private renderer: Renderer2,
        private translate: TranslateService,
        public store: ToolbarStoreService,
        public sessionStore: SessionStoreService,
    ) {
        this.initAudio();
    }

    ngOnInit() {
        // ...
        // init store
        this.vm$ = this.store.vm$;
        this.sessionStoreVm$ = this.sessionStore.state$;

        // read localStorage with latest metronome configuration
        try {
            this.config = JSON.parse(localStorage.getItem('metronome'));
            this.selectedBeat = this.config.beat || this.beats[2];
            this.bpm = this.config.bpm || 120;
            this.stepLevels = this.config.stepLevels || [3, 1, 2, 1];
        } catch (error) {
            this.selectedBeat = this.beats[2];
            this.bpm = 120;
            this.stepLevels = [3, 1, 2, 1];
        }

        /*
        // only relevant 
        // in room context 
        // and for students
        this.remoteSub = this.ar.url
            .pipe(
                map(segments => segments[0].path),
                map(val => val === 'room' ? true : false),
                // map(val => val === 'apps' ? true : false),
                // TODO: better distinctUntilChanged?
                take(1)
            )
            // initialization of remote services already have been done
            // only the listener has to be set up in case of not beeing room owner
            .subscribe(val => {
                if (!this.ds.isOwner && val) { 
                    console.log('metronome->initRemoteListener', this.ds.isOwner, val);
                    this.showRemoteCheckbox = false;
                    this.initRemoteListener(); 
                } else if (this.ds.isOwner) {
                    this.showRemoteCheckbox = true;
                }
            });
        */

        const sessSub = this.sessionStore
            .state$
            .pipe(
                distinctUntilKeyChanged('isSender'),
            )
            .subscribe(val => {
                this.remoteStatus = val.remoteStatus || false;
                this.isSender = val.isSender || false;
                
                if(!this.isSender) {
                    console.log('metronome->initRemoteListener()');
                    this.showRemoteCheckbox = false;
                    this.initRemoteListener();
                } else {
                    console.log('metronome->stopRemoteListener()');
                    // 
                    this.showRemoteCheckbox = true;
                    this.stopRemoteListener();
                }
            })
        this.subContainer.add(sessSub);

    }

    ngAfterViewInit() {
        // must come last, as it depend on al the vars calculated
        const el = this.renderer.selectRootElement(document.getElementById('stepLevelCanvas'));
        this.stepPulseCanvas = new StepCanvas(
            el, // this.myCanvas.nativeElement,
            168,
            40,
            this.stepLevels,
            'rgb(63,81,181)',
            '#888',
            '#efefef',
            'rgb(255,110,64)',
            2,
            4,
            // this.renderer,
        );

        this.stepPulseCanvas.canvasEventEmitter
            .subscribe(val => {
                console.log('step-canvas event: ', val);
                this.stepLevels = val;
                this.setToneSchedule();
                this.composeEvent({ action: 'setStepLevels', value: { stepLevels: this.stepLevels } });
                this.setConfig;
            })
    }

    // TODO: we should keep that until the session is stopped or better to say until
    // someone has left the room finally
    ngOnDestroy() {
        // make sure that the worker is stoped in this case
        try {
            // this.loop.stop();
            Tone.Transport.stop();
            Tone.Transport.cancel();
            this.sampler.dispose();
            this.ads.removeGainNode('metronome');
            this.setConfig();
            // this.ds.directshareEmitter.next({ target: 'metronome', event: { action: 'set', stepLevels: this.stepLevels } });
            // unsubscribe remoteStatus listener
            this.subscription.unsubscribe();
            this.remoteSub.unsubscribe();
            this.listenerSub.unsubscribe();
            this.composeEvent({ action: 'setStepLevels', value: { stepLevels: this.stepLevels } });
        } catch (error) {
            console.log('metronome->OnDestroy: ', error);
        }

        this.subContainer.unsubscribe();
    }

    initAudio() {
        // load sampler
        this.sampler = new Tone.Sampler({
            'C4' : 'assets/audio/metronome/Claves.wav',
            'D4' : 'assets/audio/metronome/Low_Woodblock.wav',
            'E4' : 'assets/audio/metronome/High_Woodblock.wav',
            'F4' : 'assets/audio/metronome/Drumsticks.wav',
        }, () => {
            // console.log('samples loaded');
            // now invoke functions
            // TODO: we should indicate loading
            this.loading = false;
        });

        /**
         * set up audio gainnode & connect with sampler audio source
         * this.metronomeGain = new Tone.Volume(-10);
         * this.sampler.connect(this.metronomeGain);
         * this connects to mixer
         * in scope of remoteControl will be connected/disconnected
         * per default first connected
         */

        // console.log('*** globalChannelstripStatusthis', this.ads.globalChannelstripStatus);
        // seting up audio nodes and wiring
        this.channelstrip = this.ads.createChannelstrip(
            this.translatedTitle, 
            -11, 
            this.ads.globalChannelstripStatus.toMaster,
            this.ads.globalChannelstripStatus.toClass,
            this.ads.globalChannelstripStatus.muted,
            'metronomeId'
        );
        // init gainNode
        this.ads.addGainNode('metronome', this.channelstrip.gainObj);
        this.sampler.connect(this.channelstrip.gainObj);
    }

    // ##############
    setToneSchedule() {
        // console.log('setToneSchedule...');
        // first clear to reset existing schedules
        // read steplevels
        // translate into scheduled triggers
        // set transport beats

        // cancels al previous events...
        Tone.Transport.cancel();

        const sampleNotesArr = ['','F4','E4','D4','C4'];
        // sampleNotesArr[item]

        this.stepLevels.forEach((item, index) => {
            Tone.Transport
                .schedule(time => {
                    // creates schendule for all tone events
                    // not for index = 0
                    if (item > 0){
                        this.sampler.triggerAttack(sampleNotesArr[item], time);
                    }
                    // triggers step-level canvas from transport
                    // do not do that directly, but use Tone.Draw
                    Tone.Draw.schedule(() => {
                        // do drawing or DOM manipulation here
                        this.stepPulseCanvas.stepDraw(index);
                    }, time);
                }, '0:' + index);

        });
       
        // set time signature
        // format for beats of measure = e.g. 7/4 -> [7, 4]
        Tone.Transport.timeSignature = [this.stepLevels.length, 4];
        // set the transport to repeat
        Tone.Transport.loopEnd = '1m';
        Tone.Transport.loop = true;
    }

    toggleMode() {
        this.store.toggleAppMode('metronomeId');
    }

    start() {
        // console.log('start metronome...');
        // console.log('*** globalChannelstripStatusthis', this.ads.globalChannelstripStatus);

        // make sure to not have a float
        // console.log('bpm: ', this.bpm)
        this.bpm = Math.floor(this.bpm);
        Tone.Transport.bpm.value = this.bpm;

        // before starting sound, make sure the patchbay wiring is correct
        this.channelstrip.switchAll(
            this.ads.globalChannelstripStatus.toMaster,
            this.ads.globalChannelstripStatus.toClass,
            this.ads.globalChannelstripStatus.muted
        )

        this.setToneSchedule();
        Tone.Transport.start();      
        this.metronomeRunning = true;

        // remote
        this.composeEvent({ 
            action: 'start',
            value: {
                bpm: this.bpm,
                selectedBeat: this.selectedBeat,
                stepLevels: this.stepLevels 
            } 
        });

    }

    stop() {
        console.log('stop metronome...');
        Tone.Transport.stop();
        Tone.Transport.cancel();

        this.metronomeRunning = false;

        // Tone.Transport.toggle();

        // this.ds.directshareEmitter.next({ target: 'metronome', event: { action: 'stop' } });
        this.composeEvent({ action: 'stop' });
    }

    // stores the actual status into local storage as a preference for next visit
    setConfig() {
        this.config = {
            bpm: this.bpm,
            beat: this.selectedBeat,
            // TODO: take care that always config is set,even with defaults
            stepLevels: this.stepLevels || [3, 1, 2, 1] // default
        };
        localStorage.setItem('metronome', JSON.stringify(this.config));
        // console.log('config set: ', this.config);
    }

    tap() {
/*
        if (this.metronomeRunning) {
            this.stop();
        }
*/
        const now = Date.now();
        // play sound & fix Safari
        // $audio.fixSafari(Tone.context);
        // play note when tapping
        // this.sampler.triggerAttack('E4');
        // calc BPM
        this.tapArr.push(now);
        // only keep the last 6 entries
        this.tapArr = this.tapArr.splice(-6);
        // wait for at least 2 taps
        if (this.tapArr.length > 2) {
            //
            this.newArr = this.tapArr.map((item, index, arr) => {
                // aggregate the differences between the timestamps
                if (index < arr.length - 1) {
                    return this.tapArr[index + 1] - item;
                } else {
                    return null;
                }
            });
            // remove trailing null
            this.newArr.splice(-1);
            // sum up and...
            this.tapAvrg = this.newArr.reduce((result, item) => {
                return result + item;
            });
            // ...calc bpm out of average
            this.bpm = Math.floor((60 / (this.tapAvrg / this.newArr.length)) * 1000);

            // store in local
            this.setConfig();
        }
    }

    // edit/set BPM
    doBpm(step) {
        if (parseInt(this.bpm, 10) < 500) {
            this.bpm = parseInt(this.bpm, 10) + step;
            // this.bpm = this.bpm || 120;
            Tone.Transport.bpm.value = this.bpm;
            // store in local storage
            this.setConfig();
            // this.ds.directshareEmitter.next({ target: 'metronome', event: { action: 'set', bpm: this.bpm } });
            this.composeEvent({ action: 'setBpm', value: { bpm: this.bpm } });
        }
    }

    changeBpm(event) { // event contains form value
        // console.log(event.target.value); 
        if (parseInt(event.target.value, 10) < 500) {
            this.bpm = event.target.value;
            Tone.Transport.bpm.value = this.bpm;
            // store in local storage
            this.setConfig();
        }
    }

    // representation of the predefined UI patterns for the
    // measures and sample distribution
    setStepLevels() {
        // console.log('###beat: ', self.selectedBeat)
        switch (this.selectedBeat) {
            case '1/1':
            this.stepLevels = [3, 3, 3, 3];
                break;
            case '2/2':
                this.stepLevels = [3, 2, 3, 2];
                break;
            case '4/4':
                this.stepLevels = [3, 1, 2, 1];
                break;
            case '5/4':
                this.stepLevels = [3, 2, 2, 3, 2];
                break;
            case '3/4':
                this.stepLevels = [3, 2, 2];
                break;
            case '6/4':
                this.stepLevels = [3, 2, 2, 3, 2, 2];
                break;
            case '7/4':
                this.stepLevels = [3, 1, 1, 1, 2, 1, 1];
                break;
            case '8/4':
                this.stepLevels = [4, 1, 2, 1, 4, 1, 2, 1];
                break;
            case '9/4':
                this.stepLevels = [3, 2, 2, 3, 2, 2, 3, 2, 2];
                break;
        }
        // this.setToneSchedule();
        // store in localStorage
        // this.setConfig();
        // this.ds.directshareEmitter.next({ target: 'metronome', event: { action: 'set', stepLevels: this.stepLevels } });
        this.composeEvent({ action: 'setStepLevels', value: { stepLevels: this.stepLevels } });
    }

    // beat to canvas control pattern
    selectBeat = (event) => {
        // console.log('selectedBeat: ', this.selectedBeat, event, this.stepPulseCanvas.canvas);
        this.setStepLevels();
        // redraw canvas in case of beat changes
        this.stepPulseCanvas.stepLevels = this.stepLevels;
        this.stepPulseCanvas.init();
        // set remote
        // this.ds.directshareEmitter.next({ target: 'metronome', event: { action: 'set', selectedBeat: this.selectedBeat } });
        this.composeEvent({ action: 'setSelectedBeat', value: { selectedBeat: this.selectedBeat } });
        this.setConfig();
    }

    // ###################################

    slider(event) {
        console.log(event, this.sliderValue);
    }

    /** ###direct share ###
     * 
     */

    composeEvent(value: DsEvent['event']) {
        // console.log('MetronomeComponent->composeEvent: ', this.ds.userId);
        /**
         * only if the user is sender, remote events are send
         * this is per default valid for the owner
         * but the owner can assign this "sender" role to any participant
         * ther can only be one single sender at once (really???)
         */
        
        if(this.isSender) {
            this.ds.dsEvent.next({
                senderId: this.ds.userId,
                // we asume that always and only the owner of the room steers the metronome
                isParent: this.ds.isOwner, 
                target: 'metronome',
                event: value // example: { action: 'pause', value: 'foo' }
            });
        }
    }



    // remote
    initRemoteListener() {
        // used to provide to helper functions
        // such as "setRemoteStatus"
        this.listenerSub = this.ds.remoteDSAppsListener(
            this.ds.remoteSessionId,
            'metronome'
        )
        .pipe(
            // we filter for viewer events only
            // filter(val => val.target === 'metronome'),

            // we only take them if remote status of the window is active
            // filter(() => this.isRemoteActive)
        )
        .subscribe(val => {
            console.log('remoteDSAppsListener->metronome: ', val);
            switch (val.event.action) {
                case 'start':
                    console.log('initRemoteListener->start()')
                    this.bpm = val.event.value.bpm;
                    this.selectedBeat = val.event.value.selectedBeat;
                    this.stepLevels = val.event.value.stepLevels;
                    // apply beat to canvas
                    // TODO: needed?? // this.setStepLevels();
                    this.stepPulseCanvas.stepLevels = this.stepLevels;
                    this.stepPulseCanvas.init();

                    this.start();
                    // this.myRemoteStatus();
                    break;

                case 'stop':
                    this.stop();
                    // this.myRemoteStatus();
                    break;

                case 'setStepLevels':
                    console.log(val.event.value.stepLevels);
                    this.stepLevels = val.event.value.stepLevels;
                    this.stepPulseCanvas.stepLevels = val.event.value.stepLevels;
                    this.stepPulseCanvas.init();
                    break;

                case 'setBpm':
                    this.bpm = val.event.value.bpm;
                    break;

                case 'mode':
                    this.store.setAppMode({ appId: 'metronomeId', appMode: val.event.value.mode })
                    break;
            }
        });
    }

    stopRemoteListener() {
        this.listenerSub ? this.listenerSub.unsubscribe() : '';
    }

    // simple solution
    myRemoteStatus() {
        // this.remoteStatus = true;
        // setTimeout(() => this.remoteStatus = false, 4000);
    }

    setRemoteStatusFn(event) {
        console.log('MetronomeComponent->setRemoteStatusFn: ', event);
        // active for owners of the room only
        this.ds.setRemoteStatus(event);
        // set audio patching
        this.ads.doPatchBay(event, this.isSender);
    }

// #####################
}
