import { Component, OnInit, OnDestroy, ChangeDetectorRef, Input } from '@angular/core';
import * as Tone from 'tone';
import { AudioService } from '../../shared/audio.service';
import { Router } from '@angular/router';
import { DirectshareService } from '../../shared/directshare.service';
import { ControllsService } from '../../shared/controlls.service';
import { Channelstrip } from '../../shared/channelstrip';
import { AppMode, ToolbarStoreService } from '../toolbar/toolbar-store.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-tuner',
  templateUrl: './tuner.component.html',
  styleUrls: ['./tuner.component.scss'],
  providers: [
    // WebWorkerService
  ]
})
export class TunerComponent implements OnInit, OnDestroy {
  note = 'A';
  pitch = '--';
  pitchValue: number = 440;
  kammertonDelta: number = 0;
  flag: Number = null;
  mic: Boolean = false;
  isPlaying: Boolean = false;
  test_frequencies = [];
  ctx: Tone;
  recording: Boolean = false;
  micStreams: MediaStreamTrack = null;
  script_processor: ScriptProcessorNode;
  changeDetectorRef: ChangeDetectorRef;

  notes: any;
  // ### conf for tuning notes ###
  oscillator: any = null;
  C4: any = null;
  play_frequencies: any = null;
  remoteStatus: Boolean = false;
  channelstrip: Channelstrip = null;
  // worker stuff
  runWorker: Boolean = false;
  worker: Worker = null;

  vm$: Observable<any>;

  constructor(
    private ref: ChangeDetectorRef,
    private as: AudioService,
    private router: Router,
    private ds: DirectshareService,
    public cs: ControllsService,
    // provided in parent
    public store: ToolbarStoreService,
  ) {
    this.ctx = Tone.context;
  }

  ngOnInit() {
    // init store
    this.vm$ = this.store.vm$;
    // init play notes functionality
    this.initSetup();
    /*
    // only in room context relevant
    if (this.router.url.indexOf('/room/') !== -1) {
      // subscribe remote
      // TODO: remote deactivated for launch of 1.0
      // this.doSyncRemote();
    }
    */

    // this.listenToolbar();

  }

  ngOnDestroy() {
    try{
      this.worker !== null ? this.worker.terminate() : '';
    }
    catch(e) {
      console.log(e);
    }
    // if not null, channelstrip was initialized at least once
    if (this.channelstrip !== null) { this.channelstrip.removeFromArray(); }
  }

  // ### start for tuning notes ###
  initSetup() {
    // Define the set of test frequencies that we'll use to analyze microphone data.
    // const C2 = 65.41; // C2 note, in Hz.
    const C2 = 130.81; // C2 ->  65.41, C3 ->  130.81, C4 -> 261.63 | note, in Hz.
    this.notes = [ 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B' ];
    this.test_frequencies = [];
    for (let i = 0; i < 25; i++) { // 24 | 36 --> multiple Octaves confuse the Hz Recognition
        const note_frequency = (C2 * Math.pow(2, i / 12));
        const note_name = this.notes[i % 12];
        
        // const just_above = { 'frequency': note_frequency * Math.pow(2, 1 / 48), 'name': note_name + " (a bit sharp)" };
        const much_above = { 'frequency': (note_frequency * Math.pow(2, 1 / 48)) + this.kammertonDelta, 'name': note_name,  'flag': +50 };
        const just_above = { 'frequency': (note_frequency * Math.pow(2, 1 / 96)) + this.kammertonDelta, 'name': note_name,  'flag': +25 };
        const note = { 'frequency': note_frequency + this.kammertonDelta, 'name': note_name, 'flag': 0 };
        const just_below = { 'frequency': (note_frequency * Math.pow(2, -1 / 96)) + this.kammertonDelta, 'name': note_name, 'flag': -25 };
        const much_below = { 'frequency': (note_frequency * Math.pow(2, -1 / 48)) + this.kammertonDelta, 'name': note_name, 'flag': -50 };
        this.test_frequencies = this.test_frequencies.concat([ much_below, just_below, note, just_above, much_above ]);
    }
    this.C4 = 261.63; // 261.63;
    // for playing tuning notes
    this.play_frequencies = [];
    // only one small range of tuning notes
    for (let i = 0; i < 13; i++) {
      const note_frequency = (this.C4 * Math.pow(2, i / 12)) + this.kammertonDelta;
      const note_name = this.notes[i % 12];
      const note = { 'frequency': note_frequency, 'name': note_name };
      this.play_frequencies.push(note);
    }

    // console.log(this.play_frequencies, this.test_frequencies);

    // oscillator status
    // this.isPlaying = false;
    // check for self.note;
  }

  initialize() {
    const self = this;
    // do GUM

    navigator.mediaDevices
      .getUserMedia({ audio: true, video: false })
      .then(stream => {
        const microphone = this.ctx.createMediaStreamSource(stream);
        self.micStreams = stream.getAudioTracks()[0];

        // window.source = microphone; // Workaround for https://bugzilla.mozilla.org/show_bug.cgi?id=934512
        self.script_processor = self.ctx.createScriptProcessor(1024, 1, 1);
        // console.log('script_processor.bufferSize: ', self.script_processor.bufferSize);

        self.script_processor.connect(self.ctx.destination);
        microphone.connect(self.script_processor);

        // Need to leak this function into the global namespace so it doesn't get
        // prematurely garbage-collected.
        // http://lists.w3.org/Archives/Public/public-audio/2013JanMar/0304.html

        let buffer = [];
        const sample_length_milliseconds = 100;
        self.recording = true;
        self.mic = true;
        
        if (typeof Worker !== 'undefined') {
          // Create a new worker
          this.worker = new Worker('../../workers/tuner.worker', { type: 'module' })
          // worker onmessage event
          this.worker.onmessage = ({ data }) => {
            // console.log('tunerresult: ', data);
            const result = data;
            self.note = result.note;
            self.pitch = result.pitch;
            self.flag = result.flag;   
          };
        } else {
          // Web Workers are not supported in this environment.
          console.log('web workers not supported...')
        }

        const capture_audio = event => {
          // triggered in chunks
          if (!self.recording) { return; }

          buffer = buffer.concat(Array.prototype.slice.call(event.inputBuffer.getChannelData(0)));
          // Stop this.recording after sample_length_milliseconds.
          if (buffer.length >= sample_length_milliseconds * self.ctx.sampleRate / 1000) {
              self.recording = false;

              const input = {
                'timeseries': buffer,
                'test_frequencies': self.test_frequencies,
                'sample_rate': self.ctx.sampleRate
              };

              this.worker.postMessage(input);

              buffer = [];
              setTimeout(function() { self.recording = true; }, 250);
          }
        };

        // self.script_processor.onaudioprocess = captureAudio(event);
        self.script_processor.onaudioprocess = event => {
          // console.log(event);
          capture_audio(event);
        };

      })
      .catch(error => {
        console.log('navigator.mediaDevices->error: ', error);
      });

  }

  interpret_correlation_result(event) {
    const self = this;
    const timeseries = event.data.timeseries;
    const frequency_amplitudes = event.data.frequency_amplitudes;

    // Compute the (squared) magnitudes of the complex amplitudes for each
    // test frequency.
    const magnitudes = frequency_amplitudes.map(function(z) { return z[0] * z[0] + z[1] * z[1]; });

    // Find the maximum in the list of magnitudes.
    let maximum_index = -1;
    let maximum_magnitude = 0;
    for (let i = 0; i < magnitudes.length; i++) {
        if (magnitudes[i] <= maximum_magnitude) {
          continue;
        }
        maximum_index = i;
        maximum_magnitude = magnitudes[i];
    }

    // Compute the average magnitude. We'll only pay attention to frequencies
    // with magnitudes significantly above average.
    const average = magnitudes.reduce(function(a, b) { return a + b; }, 0) / magnitudes.length;
    const confidence = maximum_magnitude / average;
    const confidence_threshold = 10; // empirical, arbitrary.
    if (confidence > confidence_threshold) {
      const dominant_frequency = self.test_frequencies[maximum_index];
      self.note = dominant_frequency.name;
      self.pitch = Math.round(dominant_frequency.frequency).toString();
      self.flag = dominant_frequency.flag;
    }
  }

  // implement
  turnOffMicrophone() {
    this.runWorker = false;
    this.script_processor = null;
    this.recording = false;
    try {
      this.worker.terminate();     
    } catch (error) {
      console.log('turnOffMicrophone->', error);
    }
    // console.log(this.micStreams);
    if (this.micStreams !== undefined) {
      // stop audio track
      this.micStreams.stop();
      // stop worker
      try {
        this.worker.terminate();     
      } catch (error) {
        console.log('turnOffMicrophone->', error);
      }
    }
    this.mic = false;
  }

  // toggle mode
  /*
  toggleMode(mode?) {
    // use dedicated "mode" or
    // toggle related to current status of mode
    const tmp = this.mode === 'default' ? 'small' : 'default';
    this.mode = mode ? this.mode = mode : tmp;
    this.ds.directshareEmitter.next({ target: 'tuner', event: { action: 'mode', mode: this.mode } });
  }
  */

  toggleMode() {
    this.store.toggleAppMode('tunerId');
  }

  toggleMic() {
    if (this.mic === true) {
      this.turnOffMicrophone();
      this.flag = 0;
      this.ds.directshareEmitter.next({ target: 'tuner', event: { action: 'MicOff' } });
    } else {
      this.initialize();
      this.togglePlay();
      setTimeout(() => this.togglePlay(), 200);
      this.ds.directshareEmitter.next({ target: 'tuner', event: { action: 'MicOn' } });
    }
  }

  // ### playing notes ###
  play = () => {
    // in case it is running
    if (this.mic) {
      this.turnOffMicrophone();
    }
    // set up
    // const now = this.ctx.currentTime;
    this.initSetup();

    // new mixer approach
    this.oscillator = this.ctx.createOscillator();
    // channelstripName, level, toMaster, toClass, mute
    this.channelstrip = this.as.createChannelstrip('tuner', -15, true, false, false, 'tunerId');
    const oscGain = this.channelstrip.gainObj;
    this.oscillator.connect(oscGain);

    // specifying this.oscillator type and frequency
    this.oscillator.type = 'triangle'; // 'square'; //'triangle';

    const idx = this.play_frequencies.findIndex(item => {
        return item.name === this.note;
    });
    let fq: number;
    if (idx !== -1) {
        fq = this.play_frequencies[idx].frequency;
        this.pitch = Math.round(this.play_frequencies[idx].frequency).toString();
    } else {
        fq = this.C4;
        this.note = 'A';
        this.pitch = Math.round(this.play_frequencies[0].frequency).toString();
    }

    this.oscillator.frequency.value = fq; // here frequency is set
    // console.log('fq: ', fq);
    this.oscillator.start(this.ctx.currentTime + 0.5);
    this.isPlaying = true;
    // console.log('tuner->play->note/idx: ', self.note, idx);
    // Analytics.trackEvent('tuner', 'start note');
  }

  togglePlay = () => {
      if (this.isPlaying) {
          this.oscillator.stop();
          this.isPlaying = false;
          this.ds.directshareEmitter.next({ target: 'tuner', event: { action: 'stop' } });
      } else {
          this.play();
          this.ds.directshareEmitter.next({ target: 'tuner', event: { action: 'play' } });
      }

  }
  // step = -1/+1
  stepPlay = step => {
      let idx = this.play_frequencies.findIndex(item => {
          return item.name === this.note;
      });
      if (idx === -1) {
          this.note = 'A';
          idx = 9;
      }
      const tmp = idx + step;
      if (tmp < 0) {
        idx = 11;
      } else {
        idx = tmp;
      }
      this.note = this.play_frequencies[idx].name;

      if (this.isPlaying) {
          this.oscillator.stop();
          this.play();
      } else {
          this.play();
      }
  }

  // TODO: deactivated for launch of 1.0
  /**
   * subscribes in case the user is NOT owner of the session for remote events
   * isOwner was determined with the initialization of the service in "room"
   * remoteStatus provides a throttled trigger for indicating remote activities
   */
  doSyncRemote() {
    if (!this.ds.isOwner) {
      this.ds.remoteStatus('tuner').subscribe(payload => {
        // console.log(payload);
        this.remoteStatus = true;
        setTimeout(() => this.remoteStatus = false, 1900);
      });
      this.ds.syncRemote('tuner')
        .subscribe((payload: any) => {
          // console.log('doSyncRemote->tuner: ', payload);
          switch (payload.event.action) {
          case 'MicOn':
            this.initialize();
            break;
          case 'MicOff':
            this.turnOffMicrophone();
            break;
          case 'play':
            this.play();
            break;
          case 'stop':
            if (this.isPlaying) {
              this.oscillator.stop();
              this.isPlaying = false;
            }
            break;
          // move this into toolbar
          case 'mode':
            // this.mode = payload.event.mode;
            // this.ref.detectChanges();
            break;
          }
        });
    }
  }

  formatLabel(value: number) {
    return Math.round(value) + 'Hz';
  }

  setPitchValue(event){
    //console.log(event);
    this.pitchValue = event.value;
    this.kammertonDelta = this.pitchValue - 440;
    this.initSetup();
  }

// ### end of class
}
