import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchivedMinutesComponent } from './archived-minutes.component';

describe('ArchivedMinutesComponent', () => {
  let component: ArchivedMinutesComponent;
  let fixture: ComponentFixture<ArchivedMinutesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArchivedMinutesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchivedMinutesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
