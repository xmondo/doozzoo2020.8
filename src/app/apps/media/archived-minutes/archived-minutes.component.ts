import { Component, OnInit, Input } from '@angular/core';
import { Observable, of, from } from 'rxjs';
import { AngularFireDatabase } from '@angular/fire/database';
import { filter, map } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-archived-minutes',
  templateUrl: './archived-minutes.component.html',
  styleUrls: ['./archived-minutes.component.css']
})
export class ArchivedMinutesComponent implements OnInit {
  @Input() user: any;
  archivedSubscPeriod: Observable<any>;
  totalDuration: number;
  pricePerMinute: number;
  cost: number;
  current_period_start: number;
  current_period_end: number;
  constructor(
    private db: AngularFireDatabase,
    private fst: AngularFirestore,
  ) { }

  async ngOnInit() {

    await this.getPrice();
    this.doArchiveStripe();
    // this.pricePerMinute = 0.04;

  }

  async getPrice(){
    const price = await this.fst
      .collection('stripe')
      .doc('config')
      .get()
      .toPromise();
    this.pricePerMinute = price.data().archivingSD;
  }

  async doArchiveStripe(){
    const start = await this.calcSubscriptionPeriodBack();

    const snap = await this.fst
      .collection('stripe')
      .doc(this.user.uid)
      .get()
      .toPromise();

    // console.log(snap.data())
    try{
      this.totalDuration = snap.data().meteredArchiving.total_usage || 0; 
      this.cost = this.totalDuration * this.pricePerMinute || 0;
    }
    catch(e){
      // console.log('doArchiveStripe error: ', e);
      this.totalDuration = 0; 
      this.cost = 0;
    }
    
  }

  async calcSubscriptionPeriodBack(){
    
    const snap = await this.db.database
      .ref(`stripe_customers/${this.user.uid}/subscriptions/currentSubscription`)
      .once('value');

    if(snap.val() !== null) {
      // console.log('currentSubscription: ', snap.val()) 
    
      this.current_period_start = snap.val().current_period_start * 1000; // short timestamp (sec)
      this.current_period_end = snap.val().current_period_end * 1000; // short timestamp (sec)
  
      return Date.now() - 60 * 60 * 24 * this.current_period_start; 

    } else {
      return 0;
    }


  }

}
