import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharingStatusComponent } from './sharing-status.component';

describe('SharingStatusComponent', () => {
  let component: SharingStatusComponent;
  let fixture: ComponentFixture<SharingStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharingStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharingStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
