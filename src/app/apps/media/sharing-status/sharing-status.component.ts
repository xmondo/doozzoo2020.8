import { Component, OnInit, Input, Output, EventEmitter, OnChanges, ChangeDetectionStrategy } from '@angular/core';
import { AngularFireDatabase, AngularFireAction } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { AngularFireStorage } from '@angular/fire/storage';
import { MatDialog } from '@angular/material/dialog';
import { DirectshareService } from '../../../shared/directshare.service';
import { AuthService } from 'src/app/shared/auth.service';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-sharing-status',
  templateUrl: './sharing-status.component.html',
  styleUrls: ['./sharing-status.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharingStatusComponent implements OnInit, OnChanges {
  @Input() user: any; // asset metadata
  @Input() bulkActionThresholdStatus: boolean;
  @Input() key: string;
  @Output() doClick: EventEmitter<any> = new EventEmitter<any>();
  status: Observable<any[]>;
  constructor(
    // public db: AngularFireDatabase,
    private afs: AngularFirestore,
    public dialog: MatDialog,
    public ds: DirectshareService,
    public ats: AuthService,
  ) { }

  ngOnInit() {
    // console.log('doLog: ', this.user.uid, this.key);
    this.key && this.user.uid ? this.checkDirectShareStatus() : console.log('no user and/or asset info');
  }

  ngOnChanges(changes) {
    if(changes.key) {
      changes.key.currentValue !== changes.key.previousValue ? this.checkDirectShareStatus() : '';
    }
  }

  checkDirectShareStatus() {
    // ...
    if (this.key) {
     this.status = this.afs
      .collection(
          'sharedMedia', ref => ref
          // .where('recipientId', '==', this.recipientId)
          .where('key','==', this.key)
        )
        .valueChanges({idField: 'snapKey'})
    }

  }

}
