import { Pipe, PipeTransform } from '@angular/core';

/**
 * @ct pipe accepts a content type string as input, e.g. 'application/pdf', 'image/png'
 * @type defines what to return: a material icon string or a string
 * example: <input> | contentType: 'string'
 * 
 * See: https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types
 */

@Pipe({
  name: 'contentType'
})
export class ContentTypePipe implements PipeTransform {

  transform(ct: string, type: 'string' | 'icon' = 'icon') {// image, pdf, document/text, application, video
    // console.log('#####', ct)
    const s = ct.split('/');
    if (ct === 'application/pdf') {
      s[0] = 'pdf';
    }
    switch (s[0]) {
      case 'image':
        return 'image';
        break;
      case 'pdf':
        return 'picture_as_pdf';
        break;
      case 'text':
        return 'content_copy';
        break;
      case 'video':
        return 'ondemand_video';
        break;
      case 'audio':
        return 'audiotrack';
        break;
      default:
        return 'insert_drive_file';
    }
  }

// end of pipe
}
