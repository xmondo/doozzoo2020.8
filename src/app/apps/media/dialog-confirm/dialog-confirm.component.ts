import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmDialogData } from '../media-list/media-list.component';

@Component({
  selector: 'app-dialog-confirm',
  templateUrl: './dialog-confirm.component.html',
  styleUrls: ['./dialog-confirm.component.scss']
})
export class DialogConfirmComponent {
  tagLabel: string;
  constructor(
    public dialogRef: MatDialogRef<DialogConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ConfirmDialogData,
  ) {
    this.tagLabel = data.value;
  }

  cancel(): void {
    this.dialogRef.close();
  }

  confirm(): void {
    // console.log('close');
    this.data.type === 'tagRenameConfirm' ? this.dialogRef.close(this.tagLabel) : this.dialogRef.close(true);
  }

}
