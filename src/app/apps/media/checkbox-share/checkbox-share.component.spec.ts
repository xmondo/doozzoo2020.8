import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckboxShareComponent } from './checkbox-share.component';

describe('CheckboxShareComponent', () => {
  let component: CheckboxShareComponent;
  let fixture: ComponentFixture<CheckboxShareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckboxShareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckboxShareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
