import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable, Subscription } from 'rxjs';
import { ControllsService } from 'src/app/shared/controlls.service';
import { ShareDialogData } from '../dialog-share/dialog-share.component';
import { MediaListItem } from '../media-list/media-list.component';
// import * as firebase from 'firebase/firestore';

@Component({
  selector: 'app-checkbox-share',
  templateUrl: './checkbox-share.component.html',
  styleUrls: ['./checkbox-share.component.scss']
})
export class CheckboxShareComponent implements OnInit, OnDestroy {
  @Input() data: ShareDialogData; // metadata of the regarding asset to be shared
  @Input() recipientId: any; // id of the customer to whom we want to share

  selected: boolean = false;
  items: Observable<any[]>;
  assetItem: MediaListItem = null;
  assetId: any = null; // if asset is existing in shared documents
  subContainer: Subscription = new Subscription()

  constructor(
    public db: AngularFireDatabase,
    public cts: ControllsService,
  ) {
    // ...
  }

  ngOnInit() {
    this.assetItem = this.data.isBulkOperation ? this.data.item[0] : this.data.item;
    
    console.log('assetItem, recipientId, ownerId: ', this.data, this.recipientId);
    
    this.items = this.db
      .list('sharedDocuments', ref => ref.orderByChild('key')
      .equalTo(this.assetItem.key))
      .snapshotChanges();

    this.items
      .subscribe(sharedItems => {
      // console.log('shared docs of this assetId: ', sharedItems);
      let childItemId;
      const status = sharedItems.some(childItem => {
          // console.log('childItem: ', childItem.payload.val());
          childItemId = childItem.key;
          return childItem.payload.val().recipientId === this.recipientId;
      });
      if (status === true) {
        // push shared item id, in case share should be removed
        // dlSelf.selected.push([item.$id, true, true, lastChildItemId]);
        this.selected = true;
        this.assetId = childItemId;
      } else {
        // dlSelf.selected.push([item.$id, false, false]);
        this.selected = false;
        this.assetId = null;
      }
    });

    const ctssub = this.cts.sync('shareAllAssets')
      .subscribe(val => {
        //console.log('shareAllAssets: ', val);
        if(val.payload) {
          this.selected ? '' : this.shareAsset(true);
        } else {
          this.selected ? this.shareAsset(false) : '';
        }
        
        this.shareAsset(val.payload)
      })
    this.subContainer.add(ctssub);
  }

  ngOnDestroy() {
    this.subContainer.unsubscribe();
  }

  shareAsset(status) {
    // console.log(event);

    const data: MediaListItem = {     
      key: this.assetItem.key,
      storagePath: this.assetItem.storagePath,
      filename: this.assetItem.filename,
      size: this.assetItem.size,
      contentType: this.assetItem.contentType,
      simpleContentType: this.assetItem.simpleContentType,
      downloadLink: this.assetItem.downloadLink,
      ownerId: this.data.user.uid,
      recipientId: this.recipientId,
      // updatedOn: firebase.database.ServerValue.TIMESTAMP, //Date.now(),
      updatedOn: Date.now(),
      // timeCreated: firebase.firestore.Timestamp.fromDate(new Date()),
    };

    if (status) {
      // true
      const ref = this.db.list('sharedDocuments');
      ref.push(data)
        .then(result => {
          console.log('set shared document: ', result.key);
          this.assetId = result.key;
        });
    } else {
      // false
      console.log('pre remove assetId: ', this.assetId);
      const ref = this.db.object('sharedDocuments/' + this.assetId);
      ref.remove()
        .then(result => {
          console.log('item was removed');
          this.assetId = null;
        });
    }
  }

}
