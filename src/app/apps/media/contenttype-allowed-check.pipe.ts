import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'contenttypeAllowedCheck'
})
export class ContenttypeAllowedCheckPipe implements PipeTransform {

  /**
   * 
   * @param contentType -> standard content type from media lib, e.g. image/jpg
   * @param types -> array of allowed content types
   * returns if content type is in range of allowed types
   * Example: <div *ngIf="item.contentType | contenttypeAllowedCheck:['image','pdf','audio', 'video']">...</div>
   */
  //transform(contentType: string, ...args: unknown[]): unknown {
  transform(contentType: string, types: Array<string>): unknown {
    // user reported error where this function failed, so wrap into try/catch
    // console.log('+++', contentType, types )
    try {
      // return contentType.indexOf('image') || contentType.indexOf('pdf') ? true : false;
      return types.some(val => contentType.indexOf(val) !== -1);
    }  
    catch(e) {
      console.log('pipe contenttypeAllowedCheck error: ', e);
      return false;
    }
  }

}
