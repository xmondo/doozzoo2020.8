import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../shared/auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { map, filter } from 'rxjs/operators';

@Component({
  selector: 'app-media-button',
  templateUrl: './media-button.component.html',
  styleUrls: ['./media-button.component.scss']
})
export class MediaButtonComponent implements OnInit {
  items: any;
  isNew = false;

  constructor(
    public a: AuthService,
    public db: AngularFireDatabase,
  ) { }

  /**
   * checks on init, whether there is any file, that has been shared less than 2 hrs ago
   */

  ngOnInit() {
    this.a.usr$
      .pipe(
        // uid must exist
        filter(val => val.uid !== null)
      )
      .subscribe(val => {
        // console.log('userId: ', val.uid),
        this.items = this.db.list(
          'sharedDocuments',
          ref => ref.orderByChild('recipientId').equalTo(val.uid)
        )
        .valueChanges()
        .subscribe(_val => {
          // iterate through array and return true if any item meets condition
          this.isNew = _val
            .some((item: any) => item.updatedOn > (Date.now() - 1000 * 60 * 60 * 2) ); // new, if younger than 2 hrs
          // console.log('is new: ', this.isNew);
        });

      });
  }

}
