import { Pipe, PipeTransform } from '@angular/core';

/** */

@Pipe({
  name: 'isNew'
})
export class IsNewPipe implements PipeTransform {
  /**
   * expects a timestamp and a interval and returns a boolean whether "is new" is valid or not
   * @param timestamp // Datestring
   * @param interval: in hours
   * example: <input> | isNew:2 // timestamp < now - 2h = "new"
   */
  transform(timestamp: string | number, interval: number) {
    const t = Date.now();
    const intvl = t - (1000 * 60 * 60 * interval); // new for 2 hrs
    const ts = typeof timestamp === 'number' ? timestamp : Date.parse(<string>timestamp);
    const status = ts > intvl ? true : false;
    // console.log(ts, intvl, status)
    return status;
  }

}
