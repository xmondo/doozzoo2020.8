import { Component, OnInit, Input } from '@angular/core';
import { AngularFireDatabase, AngularFireAction } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-archives-available',
  templateUrl: './archives-available.component.html',
  styleUrls: ['./archives-available.component.scss']
})
export class ArchivesAvailableComponent implements OnInit {
  @Input() user: any;
  freshRecordings: Observable<any>;
  constructor(
    private rdb: AngularFireDatabase,
  ) { }

  ngOnInit() {
    const recordings = this.rdb
      .list(
        `recordings/${this.user.uid}`, 
        ref => ref.orderByChild('status')
        .equalTo('available')
      )
      .valueChanges();

    // archives will be keept on tokbox for 72hrs
    // we show it only for 24hrs
    const timeThreshold = Date.now() - (1000 * 60 * 60 * 48);
    this.freshRecordings = recordings
      .pipe(
        map(val => {
          const fooArr = [];
          val.forEach((item: any) => {
            if(item.createdAt > timeThreshold){
              const data = {
                createdAt: item.createdAt,
                url: item.url,
                id: item.id,
              }
              fooArr.push(data);
            }
          });
          fooArr.sort().reverse();
          return fooArr;
        })
      );
  }

}
