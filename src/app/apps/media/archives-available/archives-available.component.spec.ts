import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchivesAvailableComponent } from './archives-available.component';

describe('ArchivesAvailableComponent', () => {
  let component: ArchivesAvailableComponent;
  let fixture: ComponentFixture<ArchivesAvailableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArchivesAvailableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchivesAvailableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
