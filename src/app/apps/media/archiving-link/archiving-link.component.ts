import { Component, OnInit, Input } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map, filter } from 'rxjs/operators';

@Component({
  selector: 'app-archiving-link',
  templateUrl: './archiving-link.component.html',
  styleUrls: ['./archiving-link.component.scss']
})
export class ArchivingLinkComponent implements OnInit {
  @Input() linkId: string;
  httpFunctionsUrl: string = environment.httpFunctionsUrl;
  archivingUrl: string;
  constructor(
    private http: HttpClient,
  ) { }

  ngOnInit() {
    this.getArchive();
  }

  getArchive() {
    const url = this.httpFunctionsUrl + 'archiving?action=getarchive&archiveid=' + this.linkId;
    const config = { headers: { 'Accept': 'application/json' } };

    this.http.get(url, config)
      .pipe(
        // ...
        filter((val:any) => val.status === 'success'),
        map((val:any) => val.archive.url),
      )
      .subscribe(val => {
        // ...
        // console.log('archiving link -> https: ', val)
        this.archivingUrl = val;
      });
  }

}
