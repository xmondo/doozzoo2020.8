import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchivingLinkComponent } from './archiving-link.component';

describe('ArchivingLinkComponent', () => {
  let component: ArchivingLinkComponent;
  let fixture: ComponentFixture<ArchivingLinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArchivingLinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchivingLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
