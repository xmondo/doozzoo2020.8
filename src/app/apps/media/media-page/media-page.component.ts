import { Component, OnInit, OnDestroy } from '@angular/core';
import { DirectshareService } from '../../../shared/directshare.service';
import { AuthService, UserData } from '../../../shared/auth.service';
import { distinctUntilChanged, filter } from 'rxjs/operators';
import { UserInfo } from 'os';
import { Subscription } from 'rxjs';
import { AudioService } from 'src/app/shared/audio.service';

@Component({
  selector: 'app-media-page',
  templateUrl: './media-page.component.html',
  styleUrls: ['./media-page.component.scss']
})
export class MediaPageComponent implements OnInit, OnDestroy {
  userId: any;
  user: UserData = null;
  authSub: Subscription;
  constructor(
    public ats: AuthService,
    public ads: AudioService,
    private ds: DirectshareService,
  ) { }

  ngOnInit() {
    this.authSub = this.ats
      .usr$
      .pipe(
        filter(val => val.uid !== null),
        distinctUntilChanged(), // otherwise fired 3 times
      )
      .subscribe(val => {
        // console.log('auth...', val)
        this.user = val;
      });

    /**
     * due to the fact that the general patchbay setting could have been set in the latest session,
     * reset them for the standalone use in media, otherwise the loooer could start muted
     */
    this.ads.globalChannelstripStatus.muted = false;
    this.ads.globalChannelstripStatus.toMaster = true;

    /*
    this.ats.usr$
      .pipe(filter(val => val.isAnonymous === false))// val.uid !== null))
      .subscribe(val => {
        this.user = val;
        this.userId = val.uid;
        // console.log('### this.userId: ', val)
        // testing:
        // simulate coach/owner for coach1
        //this.ds.initRemoteDSSender('auth0|582c7e4cedb8c73048379aad');
        //this.ds.initRemoteDSWindowsListener('auth0|582c7e4cedb8c73048379aad');
      });
    */
  }

  ngOnDestroy() {
    this.authSub.unsubscribe();
  }

}
