import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckboxShareSingleComponent } from './checkbox-share-single.component';

describe('CheckboxShareSingleComponent', () => {
  let component: CheckboxShareSingleComponent;
  let fixture: ComponentFixture<CheckboxShareSingleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckboxShareSingleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckboxShareSingleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
