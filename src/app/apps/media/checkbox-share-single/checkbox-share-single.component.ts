import { Component, Input, OnDestroy, OnInit, OnChanges } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Subscription } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { ShareDialogData } from '../dialog-share/dialog-share.component';
import { MediaListItem } from '../media-list/media-list.component';
// import * as firebase from 'firebase';

@Component({
  selector: 'app-checkbox-share-single',
  templateUrl: './checkbox-share-single.component.html',
  styleUrls: ['./checkbox-share-single.component.scss']
})
export class CheckboxShareSingleComponent implements OnInit, OnDestroy, OnChanges {
  @Input() data: ShareDialogData; // metadata of the regarding asset to be shared
  @Input() recipientId: any; // id of the customer to whom we want to share
  @Input() checkboxTrigger: boolean;
  selected: boolean = false;
  docKey: string = null;
  subContainer: Subscription = new Subscription();

  constructor(
    private afs: AngularFirestore,
  ) { }

  ngOnInit(): void {
    this.doIsSelected();
  }

  ngOnChanges(change) {
    if(change.checkboxTrigger.currentValue && !this.selected) {
      //console.log('create share');
      this.shareAsset();
    } else if(!change.checkboxTrigger.currentValue && this.selected) {
      //console.log('remove share');
      this.removeShareAsset(this.docKey);
    }
  }

  ngOnDestroy() {
    this.subContainer.unsubscribe();
  }

  doIsSelected() {
    
    const isSelectedObs = this.afs
      .collection(
        'sharedMedia', ref => ref
        .where('recipientId', '==', this.recipientId)
        .where('key','==', (<MediaListItem>this.data.item).key)
      )
      .valueChanges({idField: 'snapKey'});

    const selSub = isSelectedObs
      .pipe(
        map(val => val.map(item => item.snapKey)),
        distinctUntilChanged(),
      )
      .subscribe(val => {
        // console.log(...val);
        if(val.length > 0) {
          this.selected = true;
          this.docKey = val[0];
        } else {
          this.selected = false;
          this.docKey = null;
        }
      })
    this.subContainer.add(selSub);
        
  }

  doShareAsset(status) {
    status ? this.shareAsset() : this.removeShareAsset(this.docKey);
  }

  async shareAsset() {
    const assetItem: MediaListItem = <MediaListItem>this.data.item;

    const data: MediaListItem = {     
      key: assetItem.key,
      storagePath: assetItem.storagePath,
      filename: assetItem.filename,
      size: assetItem.size,
      contentType: assetItem.contentType,
      simpleContentType: assetItem.simpleContentType,
      downloadLink: assetItem.downloadLink,
      ownerId: this.data.user.uid,
      recipientId: this.recipientId,
      timeCreated: assetItem.timeCreated,
      updatedOn: Date.now(),
    };

    // should we first check if item exists already?
    await this.afs
      .collection('sharedMedia')
      .add(data)
    // console.log('share was added...')

  }

  removeShareAsset(docKey) {
    this.afs
      .collection('sharedMedia')
      .doc(docKey)
      .delete();
    // console.log('share was removed...')
  }

}
