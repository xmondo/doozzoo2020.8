
import { Component, OnInit, Inject } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { DataSnapshot } from '@angular/fire/database/interfaces';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { combineLatest, concat, merge, Observable, of } from 'rxjs';
import { mergeMap, map, concatAll, mergeAll, distinctUntilKeyChanged } from 'rxjs/operators';
import { UserData } from 'src/app/shared/auth.service';
import { ControllsService } from 'src/app/shared/controlls.service';
import { MediaListItem } from '../media-list/media-list.component';

export interface ShareDialogData {
  item: MediaListItem | MediaListItem[];
  user: UserData; // owner
  isBulkOperation: boolean,
}

@Component({
  selector: 'app-dialog-share',
  templateUrl: './dialog-share.component.html',
  styleUrls: ['./dialog-share.component.scss'],
})
export class DialogShareComponent implements OnInit {
  
  customerList$: Observable<any>;
  customerListSorted$: Observable<any>;
  filterargs: string;
  headlineLabel$: Observable<string>;

  constructor(
    public dialogRef: MatDialogRef<DialogShareComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ShareDialogData,
    private db: AngularFireDatabase,
    public cts: ControllsService,
    private translate: TranslateService,
  ) {
    // ...
  }

  close(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    // console.log(this.data);
    this.getCustomer();
    
    this.headlineLabel$ = this.data.isBulkOperation ? this.translate.get('media.selected-documents') : of((<MediaListItem>this.data.item).filename);
  }

  getCustomer() {

    // case user is coach
    // !! important: a coach can have either students as share target or coaches
    // hence we have to merge both observables in this case
    if(this.data.user.role === 1) {

      const studentListObs$ = this.db
        .list(
          'coachCustomer/' + this.data.user.uid, 
          ref => ref.orderByKey()
        )
        .snapshotChanges();
      const coachListObs$ = this.db
        .list(
          'coachCustomer',
          ref => ref.orderByChild(this.data.user.uid).equalTo(true)
        )
        .snapshotChanges();

      this.customerList$ = studentListObs$
          .pipe(
            // map(val => map((item:any) => item.key)),
            mergeMap(val1 => coachListObs$
              .pipe(
                // merge two result sets from FB querys with MergeMap
                // spread them into a single Array and map out only the userIds
                // by converting them into a "Set" and directly transforming them back to an Array we deduplicate the array
                map((val2:any) => [...val1.map(item => item.key), ...val2.map(item => item.key)]),
                map(arr => Array.from(new Set(arr))),
              )
            )
          );
    } else {
      this.customerList$ = this.db.list(
        'coachCustomer',
        ref => ref.orderByChild(this.data.user.uid).equalTo(true)
      )
      .snapshotChanges()
      .pipe(
        map(snapArr => [...snapArr.map(item => item.key)]),
      )
    }

    this.customerListSorted$ = this.customerList$
    .pipe(
      // provide sanp result array for resolution
      mergeMap((snapArr: Array<string>) => {
        // create per array item an inner observable
        // combineLatest -> when any observable emits a value, emit the latest value from each.
        return combineLatest(snapArr.map((userId: any) => this.db // DataSnapshot) => this.db
          .object(`profiles/${userId}/contactData`)
          .valueChanges()
          .pipe(
            map(val => {
              // use retrieved contact data or fall back to id
              const contact = val ?? userId; 
              return {
                key: userId,
                contact: contact,
              }
            }),
          )
        ))
      })
    );

  }

// end of class
}




