import { Component, Input, OnInit } from '@angular/core';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { ShareDialogData } from '../dialog-share/dialog-share.component';
import { MediaListItem } from '../media-list/media-list.component';
import { Subject, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-checkbox-share-bulk',
  templateUrl: './checkbox-share-bulk.component.html',
  styleUrls: ['./checkbox-share-bulk.component.scss']
})
export class CheckboxShareBulkComponent implements OnInit {
  @Input() data: ShareDialogData; // metadata of the regarding asset to be shared
  @Input() recipientId: any; // id of the customer to whom we want to share

  selected: boolean = false;
  docKeyArr: { item: string, snapKey: string }[] = [];
  docKeyArrSnapKeyLength: number = 0;
  subContainer: Subscription = new Subscription();
  isIndeterminate: boolean = false;
  selectedObs$: Subject<boolean> =  new Subject();

  constructor(
    private afs: AngularFirestore,
  ) { }

  ngOnInit(): void {
    this.doIsSelected();
    // console.log('data: ', this.data)
  }

  ngOnChanges(change) {
    /*
    if(change.checkboxTrigger.currentValue && !this.selected) {
      //console.log('create share');
      this.shareAsset();
    } else if(!change.checkboxTrigger.currentValue && this.selected) {
      //console.log('remove share');
      this.removeShareAsset(this.docKey);
    }
    */
  }

  ngOnDestroy() {
    this.subContainer.unsubscribe();
  }

  doIsSelected() {
    // take the list of assets, selected in bulk and filter it for their keys
    const sourceArr = (<MediaListItem[]>this.data.item).map(item => item.key);
    // distribute the keys in chinks of 10, as we can not do a search operation "in" for more then 10
    // const keyArr = this.chunkArray(sourceArr, 2);
    // console.log('keyArr: ', keyArr);
    this.docKeyArr = [];
/*
    from(sourceArr)
      .subscribe(val => console.log(val))
    //  console.log(sourceArr)
*/
    this.afs
      .collection(
        'sharedMedia', ref => ref
        .where('recipientId', '==', this.recipientId)
        .where('ownerId','==', this.data.user.uid)
      )
      .valueChanges({idField: 'snapKey'})
      .pipe(
        // ...
        map(snapArr => {
          return sourceArr.map((item:any) => {
            return { item: item, snapKey: snapArr.find((_item:any) => _item.key === item)?.snapKey }
          })
        }),
      )
      .subscribe(val => {
        // console.log('--> ', val);
        this.docKeyArr = val;
        this.dispatchCheckboxState();
      })
       
  }

  dispatchCheckboxState() {
    const sourceArr = <MediaListItem[]>this.data.item;
    // binary value for boolean
    this.docKeyArrSnapKeyLength = this.docKeyArr.filter(item => item.snapKey !== undefined ).length;
    if(this.docKeyArrSnapKeyLength > 0) {
      this.selected = true;
    } else {
      this.selected = false;
    }

    if(this.docKeyArrSnapKeyLength > 0 && this.docKeyArrSnapKeyLength < sourceArr.length) {
      this.isIndeterminate = true;
    } else if (this.docKeyArrSnapKeyLength === 0 || this.docKeyArrSnapKeyLength === sourceArr.length) {
      this.isIndeterminate = false;
    }
  }

  async doShareAsset(status) {
    if(status) {
      await this.batchShareAsset();
    } else {
      await this.batchRemoveAssets();
    }
    this.doIsSelected();
  }

  async shareAsset(assetItem) {
    // const assetItem: MediaListItem = <MediaListItem>this.data.item;

    const data: MediaListItem = {     
      key: assetItem.key,
      storagePath: assetItem.storagePath,
      filename: assetItem.filename,
      size: assetItem.size,
      contentType: assetItem.contentType,
      simpleContentType: assetItem.simpleContentType,
      downloadLink: assetItem.downloadLink,
      ownerId: this.data.user.uid,
      recipientId: this.recipientId,
      updatedOn: assetItem.updatedOn,
      timeCreated: assetItem.timeCreated,
    };

    // should we first check if item exists already?
    await this.afs
      .collection('sharedMedia')
      .add(data)
    console.log('share was added...', assetItem.key)
  }

  async batchShareAsset() {
    // create all items as new shareRelations, where snapKey is undefined
    const relationsArr = this.docKeyArr.filter(item => item.snapKey === undefined);
    const sourceArr = <MediaListItem[]>this.data.item;
    const assetArr = sourceArr.filter(item => relationsArr.some(_item => _item.item === item.key));
    // console.log('batchShareAsset: ', assetArr);

    const batch = this.afs.firestore.batch();
    assetArr.forEach(assetItem => {
      const data: MediaListItem = {     
        key: assetItem.key,
        storagePath: assetItem.storagePath,
        filename: assetItem.filename,
        size: assetItem.size,
        contentType: assetItem.contentType,
        simpleContentType: assetItem.simpleContentType,
        downloadLink: assetItem.downloadLink,
        ownerId: this.data.user.uid,
        recipientId: this.recipientId,
        timeCreated: assetItem.timeCreated,
        updatedOn: assetItem.updatedOn,
      };
      const ref = this.afs.firestore.collection('sharedMedia').doc();
      batch.set(ref, data);
    });
    
    // Commit the batch
    await batch.commit();
    // console.log('asset list was added');
    return 'asset list was added';
  }

  removeShareAsset(docKey) {
    this.afs
      .collection('sharedMedia')
      .doc(docKey)
      .delete();
    console.log('share was removed...', docKey)
  }

  async batchRemoveAssets() {
    // this.docKeyArr
    // filter all snapKeys from docKeyArr, that are not undefined and batch remove them
    const assetArr = this.docKeyArr.filter(item => item.snapKey !== undefined);
    // console.log('batchRemoveAssets: ', assetArr);
    const batch = this.afs.firestore.batch();
    assetArr.forEach(key => {
      const ref = this.afs.firestore
        .collection('sharedMedia')
        .doc(key.snapKey)
      batch.delete(<DocumentReference>ref);
    })
    
    // Commit the batch
    await batch.commit();
    console.log('assets deleted');
    return 'assets deleted';
  }

  /**
   * Returns an array with arrays of the given size.
   *
   * @param myArray {Array} Array to split
   * @param chunkSize {Integer} Size of every group
   */
  chunkArray(myArray, chunk_size){
    let results = [];
    while (myArray.length) {
        results.push(myArray.splice(0, chunk_size));
    }
    return results;
  }

}
