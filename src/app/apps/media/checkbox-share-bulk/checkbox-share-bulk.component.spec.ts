import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckboxShareBulkComponent } from './checkbox-share-bulk.component';

describe('CheckboxShareBulkComponent', () => {
  let component: CheckboxShareBulkComponent;
  let fixture: ComponentFixture<CheckboxShareBulkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckboxShareBulkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckboxShareBulkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
