import { Pipe, PipeTransform } from '@angular/core';
import { MediaListTag } from './media-list/media-list.component';

/**
 * tags -> array<MediaListTag>
 * <div *ngIf="item.tagId | getTagLabel:tags:'default'">...</div>
 */

@Pipe({
  name: 'getTagLabel'
})
export class GetTagLabelPipe implements PipeTransform {
  /**
   * 
   * @param tagId -> tag id assigned to media
   * @param tags -> array of user tags
   */
    transform(tagId: string, tags: Array<MediaListTag>, ignore?: string): unknown {
      // console.log('+++', tagId, tags )
      if(tagId === null || tagId === undefined || tagId === ignore) {
        return null
      } else {
        const tag = tags.find(item => item.id === tagId);
        // here we would fallback to the id of the tag if it would NOT be in the current tag array
        // this would cause a blib of the pure id, when the tag is deleted
        // Solution: return a 'default', which is ignored in frontend
        // return tag !== undefined ? tag.label : tagId;
        return tag !== undefined ? tag.label : 'default';
      }
    }

}
