import { Component, OnInit, Input, Output, EventEmitter, NgZone } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { UserData } from 'src/app/shared/auth.service';
import { MyUid } from 'src/app/shared/my-uid';

export interface ItemUploadStatus {
  status: 'ongoing' | 'ready' | 'canceled',
  item: any,
}

export interface CustomMetadata {
    ownerId: string,
    filename: string,
    md5Hash: string, // legacy is of course a uuid and not a Hash
    uuid: string, // THE uuid for the asset, which shoulkd be used all along the lifecycle
    tags: string, // must be stringified
}

@Component({
  selector: 'app-upload-item',
  templateUrl: './upload-item.component.html',
  styleUrls: ['./upload-item.component.scss'],
})
export class UploadItemComponent implements OnInit {
  @Input() inputFileObj: any;
  @Input() user: UserData;
  @Input() tags: Array<string>; // stringified array
  @Output() uploadStatus: EventEmitter<ItemUploadStatus> = new EventEmitter();
  _storage = null;
  percentUploaded: any;
  uploadTask: any = null;
  errorMsg: {} = null;
  hideComponent: boolean = false;

  constructor(
    // ...
    private storage: AngularFireStorage,
    private zone: NgZone,
  ) { }

  /**
   * upload is executed by browser and placed via firebase storage in the regarding bucket, 
   * but displaying in UI, search and browse is executed against an entry in /documents
   * which is populated by a function, that is invoked after successful upload to bucket
   * 
   * relevant function: "documents"
   */

  ngOnInit() {
    const uuid = MyUid.newGuid();
    // console.log('### tags, userId,muserRole, fileObj: ', this.tags, this.user.uid, this.user.role, this.inputFileObj);
    let ref;
    if (this.user.role > -1) {
      ref = this.storage.ref('documents/' + this.user.uid + '/' + this.inputFileObj.name);
    } else {
      ref = this.storage.ref('temp/' + this.user.uid + '/' + this.inputFileObj.name);
    }
    // TODO: limitation rather should be done via backend
    if (this.user.role < 0 && this.inputFileObj.size > (5 * 1024 * 1024)) {
      this.errorMsg = { code: 'Anonymous user limits', message: 'For anonymous users, upload filesize is limited!'};
    } else {
      const metaData = {
        contentType: this.inputFileObj.type,
        customMetadata: {
          ownerId: this.user.uid,
          filename: this.inputFileObj.name,
          md5Hash: uuid, // legacy is of course a uuid and not a Hash
          uuid: uuid, // legacy is of course a uuid and not a Hash
          tags: JSON.stringify(this.tags), // must be stringified
        },
        md5Hash: uuid,
      }
      // console.log('metaData: ', metaData)
      try {
        this.uploadTask = ref.put(
          this.inputFileObj,
          metaData,
        );
      } catch (error) {
        console.log('error: ', error);
        this.errorMsg = error;
      }

      // observe percentage changes
      this.percentUploaded = this.uploadTask.percentageChanges();
      this.percentUploaded
        .subscribe(val => {
          // console.log('percentUploaded: ', val);
          if(val >= 99) {
            console.log('ready...')
            setTimeout(() => {
              this.hideComponent = true;
              this.uploadStatus.next({ status: 'ready', item: this.inputFileObj});
            }, 2000);
          }
        })

      this.uploadTask
        .snapshotChanges()
        .subscribe(payload => {
          // console.log('upload snapshotChanges:', payload);
        }, error => {
          console.log('error: ', error);
          this.errorMsg = error;
        });

    }

  // onInit end
  }

  // cancels upload
  cancelUpload = () => {
    this.uploadTask.cancel();
    this.uploadStatus.next({ status: 'canceled', item: this.inputFileObj});
  }

}
