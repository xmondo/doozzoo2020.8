import { Component, ElementRef, Input, OnInit, Renderer2, ViewChild, OnChanges, Output, EventEmitter } from '@angular/core';
import { from, of } from 'rxjs';
import { UserData } from 'src/app/shared/auth.service';
import { ItemUploadStatus } from '../upload-item/upload-item.component';

export interface UploadStatusEvent { 
  status: 'idle' | 'ongoing' | 'finished' 
};

@Component({
  selector: 'app-upload-area',
  templateUrl: './upload-area.component.html',
  styleUrls: ['./upload-area.component.scss']
})
export class UploadAreaComponent implements OnInit, OnChanges {
  @Input() tagsSelectedIds: Array<string>;
  @Input() fileToUploadArray: Array<File>;
  @Input() maxFileCount: number = 10;
  @Input() user: UserData;
  @Input() disabled: boolean = false;
  @Input() layout: 'standalone' | 'session' | 'session-mobile' = 'standalone';
  @Output() uploadStatus: EventEmitter<UploadStatusEvent> = new EventEmitter();
  @ViewChild('dropZone', { static: true }) dropZone: ElementRef;
  @ViewChild('fileUploadId', { static: true }) fileUploadId: ElementRef;
  
  dragOverStatus: boolean = false;
  tags: string;
  lessFilesAlert: boolean = false;

  constructor(
    private renderer: Renderer2,
  ) { 
    
  }

  ngOnInit(): void {
    // ...
  }

  ngOnChanges(changes) {
    // console.log(changes)
    if(changes.fileToUploadArray?.currentValue) {
      // console.log('###', changes.fileToUploadArray?.currentValue)
      const fileArrInput = changes.fileToUploadArray?.currentValue;
      if(fileArrInput.length <= this.maxFileCount) {
        this.lessFilesAlert = false;
        // this.fileToUploadArray = [...fileArrInput];
        // console.log(this.fileToUploadArray)
        this.fileToUploadArray.length > 0 ? this.uploadStatus.emit({ status: 'ongoing' }) : this.uploadStatus.emit({ status: 'idle' });
      } else {
        this.lessFilesAlert = true;
        console.log('please select less files');
        this.fileToUploadArray = [];
        this.uploadStatus.emit({ status: 'idle' });
      }
    }
  }

  uploadStatusEvents(event: ItemUploadStatus) {
    // console.log('uploadStatusEvents: ', event.item.name)
    const idx = this.fileToUploadArray.findIndex(item => item.name === event.item.name)
    this.fileToUploadArray.splice(idx,1);
    this.fileToUploadArray.length > 0 ? this.uploadStatus.emit({ status: 'ongoing' }) : this.uploadStatus.emit({ status: 'idle' });
  }

  selectAndUpload(event) {
    // console.log('selectAndUpload', event);
    // file array lives in event.target.files

    // for uploading via file select input/button, tag is set to default
    this.tagsSelectedIds['default'];

    //console.log( 'files: ', event.target.files);

    if(event.target.files.length <= this.maxFileCount) {
      this.lessFilesAlert = false;
      this.fileToUploadArray = [...this.fileToUploadArray, ...event.target.files];
      // console.log(this.fileToUploadArray)
      this.uploadStatus.emit({ status: 'ongoing' });
    } else {
      this.lessFilesAlert = true;
      console.log('please select less files');
      this.fileToUploadArray = [];
      this.uploadStatus.emit({ status: 'idle' });
    }

  }

}
