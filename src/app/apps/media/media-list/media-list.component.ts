import { Component, ElementRef, OnInit, Renderer2, ViewChild, OnDestroy, AfterViewInit, NgZone, Input } from '@angular/core';
import { fromEventPattern, merge, Observable, of, Subject, Subscription } from 'rxjs';
import { AngularFireDatabase } from '@angular/fire/database';
import { catchError, debounceTime, distinctUntilChanged, distinctUntilKeyChanged, filter, map, mergeMap, switchMap, take, tap, throttleTime } from 'rxjs/operators';
import { DeviceDetectorService } from 'ngx-device-detector';
import { Downloader } from 'src/app/shared/downloader';
import { AngularFirestore } from '@angular/fire/firestore';
import { firestore } from 'firebase/app';
import { AuthService, UserData } from 'src/app/shared/auth.service';
import { FormControl, Validators } from '@angular/forms';
import { AngularFireStorage } from '@angular/fire/storage';
// import * as firebase from 'firebase';

// const uuid = MyUid.newGuid();

// dialog components
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogConfirmComponent } from '../../media/dialog-confirm/dialog-confirm.component';
import { DirectshareService } from 'src/app/shared/directshare.service';
import { DialogShareComponent, ShareDialogData } from '../dialog-share/dialog-share.component';
import { DataSnapshot } from '@angular/fire/database/interfaces';
import { fromEvent } from 'rxjs';
import { UploadStatusEvent } from '../upload-area/upload-area.component';
import { DefaultLangChangeEvent, LangChangeEvent, TranslateService } from '@ngx-translate/core';


declare type WhereFilterOp = '<' | '<=' | '==' | '>=' | '>' | 'array-contains' | 'array-contains-any' | 'in' | 'not-in';

export interface MediaLayout {
  layout: 'standalone' | 'session' | 'session-mobile';
}

export interface MediaStats {
  totalCount: number,
  totalFilesize: number,
  sharedFromYou: number,
  sharedWithYou: number,
}

export interface ConfirmDialogData {
  type: 'itemConfirm' | 'tagConfirm' | 'tagRenameConfirm',
  value?: any,
}

export interface MediaListItem {
  key: string, // redundant as uuid, md5hash, 
  filename: string,
  storagePath: string, // path -> filename
  contentType: string,
  simpleContentType: string,
  downloadLink: string,
  size: number,
  updatedOn: number | object,
  tags?: Array<string>
  timeCreated?: string | number | object,
  ownerId?: string,
  recipientId?: string,
  selected?: boolean,
  // name?: string, // due to compatibility reasons
}

export interface MediaListTag {
  label: string,
  id: string,
  type: 'tag' | 'folder',
  count?: number,
  selected?: boolean,
}

interface MediaQuery {
  tags: Array<any>,
  sortBy: any,
  contentType: any,
}

@Component({
  selector: 'app-media-list',
  templateUrl: './media-list.component.html',
  styleUrls: ['./media-list.component.scss']
})
export class MediaListComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('tagList') tagList: ElementRef;
  @ViewChild('mediaList') mediaList: ElementRef;
  @Input() layout: 'standalone' | 'session' | 'session-mobile' = 'standalone';

  fileToUploadArray: Array<File> = [];
  maxFileCount: number = 20;
  uploadStatusIsIdle: boolean = true;

  // get this from service in future
  mediaQuotas = {
    maxCount: 2000,
    maxFilesize: 1 * 1024 * 1024 * 1024, // calc of GB
  }
  stats$: Observable<MediaStats> = null;
  isQuotaExeeded$: Observable<boolean>;
  quotas$: Observable<any>;

  // newTagCtrl = new FormControl();
  newTagCtrl = new FormControl('', [ Validators.required, Validators.minLength(3) ]);

  inputType: string = 'owner';
  user: UserData = null;

  mediaListTagged$: Observable<any>;
  mediaQuery$: Subject<MediaQuery>;
  mediaListTags$: any; //Observable<MediaListTags | unknown>
  newTag: any;
  tagsSelected: Array<any> = [];
  // stringified Array
  tagsSelectedIds: Array<string>;
  isSharedWithMe: boolean = false;

  searchArr: any;
  filterargs: string;

  sortDirection: 'desc' | 'asc' = 'asc';
  sortAttribute: 'filename' | 'contentType' = 'filename';
  selectedSortBy: any;
  selectedContentType: any;

  itemsPerPage: number = 20;
  localProgress: boolean = false;

  // ### bulk action ###
  // mediaListItems: any;
  indeterminateModel: boolean = false;
  mediaListItemsSelected: Array<MediaListItem> = [];
  medialistIsCheckedCount: number = 0;
  bulkActionThreshold: number = 100;
  bulkDeleteRunning: boolean = false;
  bulkSelect: FormControl = new FormControl();

  isMobile: boolean;

  sortByCtrl: FormControl = new FormControl();
  contentTypeCtrl: FormControl = new FormControl();
  tagCtrl: FormControl = new FormControl();

  tags: Array<MediaListTag> = [];
  deleteTagHint: boolean = false;

  toggleFilenameEdit: boolean = false;
  maxFolders: number = 10;

  sortBy = [
    { label: 'A-Z', attribute: 'filename', value: 'asc', translateId: 'media.sortby-labels.a-z' }, //media.sortby-labels.a-z
    { label: 'Z-A', attribute: 'filename', value: 'desc', translateId: 'media.sortby-labels.z-a' },
    { label: 'newest', attribute: 'updatedOn', value: 'desc', translateId: 'media.sortby-labels.newest' },
    { label: 'oldest', attribute: 'updatedOn', value: 'asc', translateId: 'media.sortby-labels.oldest' },
    { label: 'largest files', attribute: 'size', value: 'desc', translateId: 'media.sortby-labels.largest-files' },
    { label: 'smallest files', attribute: 'size', value: 'asc', translateId: 'media.sortby-labels.smallest-files' },
  ]

  contentTypes = [
    { label: 'All content types', value: 'all', translateId: 'media.contenttypes-labels.all' },
    { label: 'Audio', value: ['audio'], translateId: 'media.contenttypes-labels.audio' },
    { label: 'Pdf', value: ['pdf'], translateId: 'media.contenttypes-labels.pdf' },
    { label: 'Image', value: ['image'], translateId: 'media.contenttypes-labels.image' },
    { label: 'Text', value: ['text'], translateId: 'media.contenttypes-labels.text' },
    { label: 'Video', value: ['video'], translateId: 'media.contenttypes-labels.video' },
  ]

  defaultTag: MediaListTag = {
    label: 'My documents',
    id: 'default',
    type: 'folder',
    selected: true,
  }

  lessonArchiveTag: MediaListTag = {
    label: 'lesson archives',
    id: 'lesson_archive',
    type: 'folder',
    selected: false,
  }

  // sharedWithMe
  sharedWithMeTag: MediaListTag = {
    label: 'Shared with me',
    id: 'sharedWithMe',
    type: 'folder',
    selected: false,
  }

  subContainer = new Subscription();

  constructor(
    private db: AngularFireDatabase,
    private afs: AngularFirestore,
    public deviceService: DeviceDetectorService,
    public dl: Downloader,
    private renderer: Renderer2,
    public ats: AuthService,
    public dialog: MatDialog,
    public storage: AngularFireStorage,
    public ds: DirectshareService,
    public zone: NgZone,
    private translate: TranslateService,
  ) { 
    //this.selectedTag = this.tags[0];
    this.selectedSortBy = this.sortBy[2]; // preselect newest as default
    this.selectedContentType = this.contentTypes[0];
    this.tagsSelectedIds = ['default']; 
  }

  async ngOnInit() {
    this.isMobile = this.deviceService.isMobile();

    // fetch current user
    const auth = this.ats
      .usr$
      .pipe(
        filter(val => val.uid !== null),
        distinctUntilChanged(), // otherwise fired 3 times
      )
      .subscribe(val => {
        // console.log('auth...', val)
        this.user = val;
        this.doInit();
      });
    this.subContainer.add(auth);

    this.fetchTranslations();

    const trsChange = this.translate
      .onLangChange.subscribe((event: LangChangeEvent) => {
        // do something
        this.fetchTranslations();
      });
    this.subContainer.add(trsChange);
  }

  ngAfterViewInit() {
    //this.layout === 'standalone' ? this.initDnD() : '';
    this.initDnD();
  }

  ngOnDestroy() {
    this.subContainer.unsubscribe();
  }

  uploadStatus(event) {
    // console.log('media-list upload-status: ', event);
    this.uploadStatusIsIdle = event.status === 'idle' ? true : false;
  }

  fetchTranslations() {
    const defaultTags = ['media.default-tags.my-documents','media.default-tags.session-archives','media.default-tags.shared-with-me']
    const trsDef = this.translate
      .get(defaultTags)
      .subscribe(( val: Array<string> ) => {
        // console.log('defaultTags: ', val);
        this.defaultTag.label = val['media.default-tags.my-documents'];
        this.lessonArchiveTag.label = val['media.default-tags.session-archives'];
        this.sharedWithMeTag.label = val['media.default-tags.shared-with-me'];
      });
    this.subContainer.add(trsDef);

    const sortByLabels = this.sortBy.map(item => item.translateId);
    const trsLab = this.translate
      .get(sortByLabels)
      .subscribe(( val: Array<string> ) => {
        // console.log('sort by labels: ', val);
        this.sortBy.forEach((item) => item.label = val[item.translateId])
      });
    this.subContainer.add(trsLab);

    const contentTypesLabels = this.contentTypes.map(item => item.translateId);
    const trsCont = this.translate
      .get(contentTypesLabels)
      .subscribe(( val: Array<string> ) => {
        // console.log('sort by labels: ', val);
        this.contentTypes.forEach((item) => item.label = val[item.translateId])
      });
    this.subContainer.add(trsCont);
  }

  tagQuota() {
    // feature toggle
    // TODO: fetch maxFolders Quota
    this.maxFolders = 10;
    this.tags.length > (this.maxFolders + 2) ? this.newTagCtrl.disable() : this.newTagCtrl.enable();
  }

  doInit() {
    this.getMediaListTags();
    this.getMediaListTagged();
    this.getMediaStats();

    // init tags array
    /*
    this.tags.push(this.defaultTag);
    this.tags.push(this.lessonArchiveTag);
    this.tags.push(this.sharedWithMeTag);
    */
    // init filters
    // set defaults
    this.sortByCtrl.setValue(this.sortBy[2]);
    this.contentTypeCtrl.setValue(this.contentTypes[0]);
    this.tagCtrl.setValue(this.tags[0]);
    
    this.selectFilters();
  }

  getMediaStats() {
    // TODO: get from prefs from service
    // max 1000 files and/or max 5GB storage

    this.stats$ = <Observable<MediaStats>>this.afs
      .collection(`media`)
      .doc(this.user.uid)
      .valueChanges()
      .pipe(filter(val => val !== undefined))

    this.quotas$ = this.afs
      .collection('media')
      .doc('mediaQuotas')
      .valueChanges()
      .pipe(
        map((val:any) => {
          const role = this.user.role ?? 0;
          const maxCount = role === 1 ? val.maxCountCoach : val.maxCountStudent;
          const maxFilesize = role === 1 ? val.maxFilesizeCoach : val.maxFilesizeStudent;
          return { 
            maxCount: maxCount, 
            maxFilesize: maxFilesize 
          };
        })
      )

    this.isQuotaExeeded$ = this.stats$
      .pipe(
        // tap(val => console.log('isQuotaExeeded: ', val)),
        filter(val => val !== undefined),
        mergeMap(stats => this.quotas$
          .pipe(
            // filter(val => val !== undefined), // faster, see http://typeofnan.blogspot.com/2011/01/typeof-is-fast.html
            // 1 * 1024 * 1024 * 1024, // calc of GB
            map(quotas => stats.totalCount > quotas.maxCount || stats.totalFilesize > (quotas.maxFilesize  * 1024 * 1024 * 1024) ? true : false),
          )) 
      );

  }

  // executed with every line item checkbox change
  doSelected(status, mediaListArr) {
    const selected = mediaListArr.filter(item => item.isChecked === true);
    this.mediaListItemsSelected = selected.map(item => { 
      const data: MediaListItem = { 
        key: item.key, 
        filename: item.filename,
        storagePath: item.storagePath, // path -> filename
        contentType: item.contentType,
        simpleContentType: item.simpleContentType,
        downloadLink: item.downloadLink,
        size: item.size,
        updatedOn: item.updatedOn,
        timeCreated: item.timeCreated,
        ownerId: item.ownerId,
      }
      return data;
    });

    /**
     * handle behaviour of select all checkbox for its 3 states: selected, indeterminate, unselected
     */
    // console.log('ev, selected: ', status, this.mediaListItemsSelected);
    this.medialistIsCheckedCount = this.mediaListItemsSelected.length;
    const bulkSelectValue = this.medialistIsCheckedCount === mediaListArr.length ? true : false;
    this.bulkSelect.setValue(bulkSelectValue);
    this.indeterminateModel = this.medialistIsCheckedCount > 0 && this.medialistIsCheckedCount < mediaListArr.length ? true : false;

    return selected;
  }

  resetBulkSelectcheckbox() {
    this.indeterminateModel = false;
    this.bulkSelect.setValue(false);
    this.mediaListItemsSelected = [];
    this.medialistIsCheckedCount = 0;
  }

  medialistToggleSelectAll(status, mediaListArr) {
    // console.log('event: ', status);
    // toggle the checked status
    mediaListArr.forEach(item => item.isChecked = status);
    // filter all selected
    this.doSelected(status, mediaListArr);
  }

  formControlsSub() {
    // set defaults
    this.sortByCtrl.setValue(this.sortBy[0]);
    this.contentTypeCtrl.setValue(this.contentTypes[0]);
    // this.tagCtrl.setValue('default'); // will be triggered by Observable/Subject

    // const fctl$ = merge(
    const fctl$ = merge([
      this.sortByCtrl.valueChanges
        .pipe( map( val => { val.origin = 'sortBy'; return val })),
      this.contentTypeCtrl.valueChanges
        .pipe( map( val => { val.origin = 'sortBy'; return val })),
      this.tagCtrl.valueChanges
        .pipe( map( val => { val.origin = 'tags'; return val })),
    ])
    .pipe()
    .subscribe(val => console.log('formCtrl sub: ', val));
  }

  selectFilters() {
    // as long as we only handle one item
    this.tags.forEach(item => item === this.tagCtrl.value ? item.selected = true : item.selected = false);

    // prepare for handline more than one
    // return array of selected tags
    const tagsSlected = this.tags.filter(item => item.selected) || ['default'];
    const mQuery: MediaQuery = {
      tags: tagsSlected,
      sortBy: this.sortByCtrl.value || 'asc',
      contentType: this.contentTypeCtrl.value || 'all',
    }

/*
    console.log(
      this.sortByCtrl.value,
      this.contentTypeCtrl.value,
      this.tagCtrl.value,
    )
*/
    this.tagsSelected = tagsSlected;
    this.isSharedWithMe = tagsSlected.some(item => item.id === 'sharedWithMe')

    // now execute query
    this.mediaQuery$.next(mQuery);
  }

  resetFilters() {
    this.tags.forEach(item => item === this.tagCtrl.value ? item.selected = true : item.selected = false);
    this.tags[0].selected = true;
    // console.log('tags: ', this.tags)
    // this.selectFilters();
  }

  // documents in Firestore
  async getMediaListTags() {

    return this.afs
      .collection(`media`)
      .doc(this.user.uid)
      .collection(`tags`)
      .valueChanges({idField:'id'})
      .pipe(
        distinctUntilChanged(),
      )
      .subscribe((tagArr: any) => {
        // console.log('*** ', val)
        // set default Tags
        const defaultTags = [];
        defaultTags.push(this.defaultTag)
        defaultTags.push(this.lessonArchiveTag);
        defaultTags.push(this.sharedWithMeTag);

        // console.log(tagArr)

        // retrieve user tags and concat with default tags
        this.tags = defaultTags.concat(tagArr);
        // reset select
        this.tags.forEach(item => item.selected = false);
        // select default as preference
        this.tags[0].selected = true;
        // set default for option list as well
        this.tagCtrl.setValue(this.tags[0]);

        // ###
        this.selectFilters();

        this.tagQuota();
      })

  }

  // documents in Firestore
  async getMediaListTagged() {
    const t0 = performance.now()

    this.mediaQuery$ = new Subject();
    const mediaListTaggedQuery = this.mediaQuery$
      .pipe(
        filter(val => val.tags.length > 0),
        switchMap((qry: MediaQuery) => {
          // console.log('qry: ', qry);
          // const tags = qry.tags.map(item => item.value); // ['blues','jazz'] //qry.tags[0].value ?? 'default';
          const tags = qry.tags[0].id ?? 'default';
          
          // standard search
          if(qry.tags.some(item => item.id === 'sharedWithMe' && qry.contentType.value === 'all')) {
            // console.log('sharedWithMe->', qry, this.user.uid)
            return this.afs
              .collection(
                'sharedMedia', ref => ref
                .where('recipientId', '==', this.user.uid)
                .orderBy(qry.sortBy.attribute, qry.sortBy.value)
              )
              .valueChanges({idField: 'key'})
          } else if(qry.tags.some(item => item.id === 'sharedWithMe' && qry.contentType.value !== 'all')) {
            // console.log('sharedWithMe->', qry, this.user.uid)
            return this.afs
              .collection(
                'sharedMedia', ref => ref
                .where('recipientId', '==', this.user.uid)
                .where('simpleContentType','in', qry.contentType.value)
                .orderBy(qry.sortBy.attribute, qry.sortBy.value)
              )
              .valueChanges({idField: 'key'})
          } else if (qry.contentType.value !== 'all') {
            return this.afs
              .doc(`media/${this.user.uid}`)
              .collection(
                'documents', ref => ref
                .where('tags', 'array-contains', tags)
                .where('simpleContentType','in', qry.contentType.value)
                .orderBy(qry.sortBy.attribute, qry.sortBy.value)
              )
              .valueChanges({idField: 'key'})
          }  else {
            return this.afs
              .doc(`media/${this.user.uid}`)
              .collection(
                'documents', ref => ref
                // .where('tags', 'array-contains-any', tags) // throws type error but works
                .where('tags', 'array-contains', tags) // throws type error but works
                .orderBy(qry.sortBy.attribute, qry.sortBy.value)
              )
              .valueChanges({idField: 'key'})
          }
          
        })
      );

    this.mediaListTagged$ = mediaListTaggedQuery
      .pipe(
        map((snapArr: any) => {
            // console.log('snapArr length: ', snapArr.length)
            return snapArr.map(item => {
            const data: MediaListItem = {
              key: item.key,
              storagePath: item.name,
              filename: item.filename,
              size: item.size,
              updatedOn: item.updatedOn,
              timeCreated: item.timeCreated,
              downloadLink: item.downloadLink,
              contentType: item.contentType,
              simpleContentType: item.simpleContentType,
              tags: item.tags,
              ownerId: item.ownerId,
            }
            return data;
          })
        }),
        tap(val => {
          // const t1 = performance.now()
          // console.log('execution time: ', Math.floor(t1 - t0));
          // console.log('+++++++', val)
          this.mediaListTaggedDone();
        })
      );

    // this.mediaQuery$.next('default');
    this.selectFilters();
  }

  selectSortAttribute(eventValue) {
    // console.log(event);
    this.sortAttribute = eventValue;
  }

  selectSortDirection(eventValue) {
    // console.log(event);
    this.sortDirection = eventValue;
  }

  async downloadItem(item) {
   
    // this.downloadProgress = true;
    this.localProgress = true;
    // console.log('item: ', item.downloadLink);
    // this.dl.downloadUrl(item.downloadLink, item.filename, item.mimeType )
    this.dl.downloadUrl(item.downloadLink, item.filename, item.contentType )
      .then(result => {
        // this.downloadProgress = false;
        this.localProgress = false;
        // console.log(result);

        // google Analytics
        // this.gas.eventEmitter('media', 'download', item.filename);

      });
  }

  // ### START drag drop ####
  initDnD() {
    // viewChild does not work properly?!
    // const el = this.tagList.nativeElement;
    // const el = this.tagListSession.nativeElement;
    const el = this.layout !== 'session-mobile' ? 
      // this.renderer.selectRootElement(document.querySelector('.tag-list'), true) :
      this.renderer.selectRootElement(document.querySelector('.media-pane-left-col'), true) :
      this.renderer.selectRootElement(document.querySelector('.upload-pane-session'), true);

      merge(
        fromEvent(document, 'dragstart'),
        fromEvent(el, 'dragenter'),
        fromEvent(el, 'dragleave'),
        fromEvent(el, 'drop'),
      )
      .pipe(
        /**
         * all dragDrop elements are flagged with data-dragdrop-allowed = allowed | disallowed
         * hence filtering is possible on event stream level
         */
        // if null || undefined, disallow
        // then filter out only allowed
        filter((val:any) => (<any>val.target).dataset?.dragdropAllowed ?? 'disallowed'),
        filter((val:any) => (<any>val.target).dataset?.dragdropAllowed === 'allowed')
      )
      .subscribe((val: DragEvent) => {

        // console.log(val.type, (<any>val.target).id, (<any>val.target).dataset.dragdropAllowed)

        switch(val.type) {
          case 'dragstart':
            this.doDragStart(val)
            break;
          case 'dragenter':
              this.doDragEnter(val)
              break;
          case 'dragleave':
            this.doDragLeave(val)
            break;
          case 'drop':
            this.doDragDrop(val)
            break;
          case 'dragend':
            this.doDragEnd(val)
            break;
        }

      });
    
  }

  doDragStart(event:DragEvent) {
    // console.log('dragstart: ', event.target);
    const _key = (<any>event).target.getAttribute('data-key'); // TODO: issues to cast the right element
    (<DragEvent>event).dataTransfer.setData('key', _key);
  }

  doDragEnter(event:DragEvent) {
    //console.log('*** enter dt: ', (<any>event).dataTransfer.types.includes('key'))
    
    console.log('*** key ', (<any>event).dataTransfer.getData('hallo'));
    // shows whether the drag comes from media list
    // !!! getData does only work for the drop event
    // to find out if data for "key" is available (indicator for a drag from the media list)
    // we have to query the types array of the datatransfer object
    const isDragFromList: boolean = (<any>event).dataTransfer.types.includes('key');
    // element id is used to identify contentwise, which elements are relevant
    const _id = (<any>event).target.id;
    // an existing targetId indicates a valid drop target together with the "drop-allowed" class

    /**
     * allow only for media list drags, if the target is different from "default" and "shared with me"
     * indicators are the destination (is "key" available or not) and the target id
     */
    let dropAllowed;
    if(isDragFromList) {
      dropAllowed = _id !== 'sharedWithMe' && _id !== 'default' ? true : false;
    } else {
      dropAllowed = _id !== 'sharedWithMe' ? true : false;
    }

    // console.log('###' , dropAllowed, _id, isDragFromList)

    if(dropAllowed) {
      event.preventDefault();
      event.dataTransfer.dropEffect = "copy";
      this.renderer.addClass(event.target, 'dragEnter');
      /**
       * triggering dragovers directly form the element creates a bad interaction with the ng changetracker,
       * which ends up in a delayed and flickering UI behaviour.
       * putting it outside or the ngZone does the trick
       */
      this.zone.runOutsideAngular(() => {
        this.renderer.listen(event.target, 'dragover', event => {
          event.preventDefault();
          event.dataTransfer.dropEffect = "copy";
        });
      })
    } else {
      // event.preventDefault();
      event.dataTransfer.dropEffect = "none";
      this.renderer.addClass(event.target, 'dragNotAllowed');
    }

  }

  doDragLeave(event:DragEvent) {
    // console.log('***leave', event.target)
    event.preventDefault();
    // remove d&d relevant indicator classes
    this.renderer.removeClass(event.target, 'dragEnter');
    this.renderer.removeClass(event.target, 'dragNotAllowed');
  }

  doDragEnd(event:DragEvent) {
    // Remove all of the drag data
    // console.log('dragEnd');
    const dt = event.dataTransfer;
    if (dt.items) {
      // Use DataTransferItemList interface to remove the drag data
      for (let i = 0; i < dt.items.length; i++) {
          dt.items.remove(i);
      }
    } else {
      // Use DataTransfer interface to remove the drag data
      event.dataTransfer.clearData();
    }
  }

  doDragDrop(event:DragEvent) {
    // console.log('***drop', event)

    event.preventDefault();
    const isDragFromList: boolean = event.dataTransfer.getData('key') !== '' ? true : false;
    // console.log('drop->isDragFromList: ', isDragFromList);

    // extract tag id from target, selected by dragover action
    const tagId = (<any>event).target.getAttribute('data-tag-id');
    // parse tags array statically, "default" must be always there
    this.tagsSelectedIds = ['default', tagId]; // // this.tagsSelected.map(item => item.id));

    if(!isDragFromList) {
      // Use DataTransferItemList interface to access the file(s)
      const dt = event.dataTransfer;
      const tempFileArr = [];
      for (let i = 0; i < dt.items.length; i++) {
        if (dt.items[i].kind === 'file') {
          const f = dt.items[i].getAsFile();
          // console.log('file ' + i + ': ', f.name, dt.items);
          tempFileArr.push(f);
        }
      }
      // populate fileArray
      // use ngZone, otherwise changetracking does not work!!
      this.zone.run(() => this.fileToUploadArray = tempFileArr);

    } else {
      const documentKey = event.dataTransfer.getData('key');  
      // choose whether to work in bulk or in the non bulk function
      this.mediaListItemsSelected.length > 0 ? 
        this.mediaListItemsSelected.forEach(item => this.setTag(item.key, tagId)) : 
        this.setTag(documentKey, tagId);
    }

    this.renderer.addClass(event.target, 'dragWasDropped');
    this.renderer.addClass(event.target, 'blink-faster');

    setTimeout(() => {
      this.renderer.removeClass(event.target, 'dragWasDropped');
      this.renderer.removeClass(event.target, 'dragEnter');
      this.renderer.removeClass(event.target, 'blink-faster');
    }, 500);

  }
  // ### End DragDrop ###

  async setTag(documentKey: string, tagUid: string) {
    if(tagUid !== '') {
      const doc = await this.afs
        .doc(`media/${this.user.uid}`)
        .collection('documents')
        .doc(documentKey)
        .get()
        .toPromise();

      // console.log(doc.data().tags, tagUid)

      const tagsArr = doc.data().tags;
      const exists = tagsArr.length > 0 ? tagsArr.some(item => item === tagUid) : false;
      // console.log('tag exists? ', exists);

      // !! never delete default -> item would be not shown anymore
      if(exists && tagUid !== 'default') {
        await this.afs
          .doc(`media/${this.user.uid}`)
          .collection('documents')
          .doc(documentKey)
          .update({
            tags: firestore.FieldValue.arrayRemove(tagUid)
          })
      } else {
        await this.afs
          .doc(`media/${this.user.uid}`)
          .collection('documents')
          .doc(documentKey)
          .update({
            tags: firestore.FieldValue.arrayUnion(tagUid)
          })
      }
    } else {
      console.log('setTag: tagUid is zero');
    }
  }

  /**
   * 
   * we must consolidate input from tag navigation, filter controls and submit it to the subject
   */
  // ### filter ###
  selectTag(tag){
    this.tags.forEach(item => item.selected = false);
    const index = this.tags.findIndex(_item => _item === tag);
    this.tags[index].selected = true;
    this.tagCtrl.setValue(this.tags[index])
    this.selectFilters();

    // allways reset bulk checkbox, when selecting a folder
    this.resetBulkSelectcheckbox();
  }

  async saveNewTag(type: any = 'folder') {
    // console.log('new Tag: ', this.newTagCtrl.value, this.newTagCtrl.invalid);

    this.tagQuota();

    // Checks: already existing?
    const tag = this.newTagCtrl.value;

    if(!this.tags.find(item => item.label === tag) && !this.newTagCtrl.invalid) {
      const res = await this.afs
        .collection(`media`)
        .doc(this.user.uid)
        .collection('tags')
        .add({
          label: tag,
          type: type,
          selected: false,
        });

      const data: MediaListTag = {
        label: tag,
        id: res.id,
        type: type,
        selected: false,
      }

      this.newTagCtrl.reset();
    } else {
      this.tags.find(item => item.label === tag) ? console.log('saveNewTag: item exists ', tag) : '';
      this.newTagCtrl.invalid ? console.log('errors: ', this.newTagCtrl.errors) : '';
    }
  }

  async renameTag(tag, result) {
    //
    await this.afs
      .collection(`media`)
      .doc(this.user.uid)
      .collection('tags')
      .doc(tag.id)
      .update({
        label: result,
      });
    console.log(`tag ${tag.label} renamed to ${result}`);
  }

  // ####

  selectSortBy(item){
    this.selectedSortBy = item;
  }

  selectContentType(item) {
    this.selectedContentType = item;
  }

  directShare(item) {
    this.ds.directshareEmitter.next({ action: 'default', asset: item });
    // google Analytics
    // this.gas.eventEmitter('media', 'directShare', item.filename);
  }

  multiShare(item) {
    // this.ds.dsEvent.next({ window: null, target: 'string', value: item });
    this.ds.dsEvent.next({ windowId: null, target: null, event: item });
  }

  // dialog functions
  /**
   * 
   * @param item -> item is a single MediaListItem or an array of those
   * @param isBulkOperation -> triggers the correct assignment, if bulk, the item is pulled out of the mediaListItemsSelected Array
   */
  shareAssetDialog(item: MediaListItem | MediaListItem[], isBulkOperation: boolean): void {

    // set single or bulk
    if(isBulkOperation && this.medialistIsCheckedCount > 0) {
      item = this.mediaListItemsSelected;
    } else {
      item = item;
    }
    // console.log('item', item, isBulkOperation);

    const data: ShareDialogData = { 
      item: item, 
      user: this.user,
      isBulkOperation: isBulkOperation,
    }

    const myDialogRef = this.dialog.open(DialogShareComponent, {
      width: '750px',
      minWidth: '350px',
      data: data,
    });
    // google Analytics
    // this.gas.eventEmitter('media', 'multiShare', item.filename);
  }

  // for deleting items
  openConfirm(item): void {
    const dialogRef = this.dialog.open(DialogConfirmComponent, {
      width: '450px',
      minWidth: '350px',
      data: { type: 'itemConfirm' }
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        // console.log('The dialog was closed', result);
        // result is 'true' or undefined
        if (result === true) {
          // trigger delete, single or bulk
          this.medialistIsCheckedCount > 0 ? this.deleteBulk() : this.deleteItem(item);
          // reset in any case the bulk checkbox
          console.log('reset...')
          this.resetBulkSelectcheckbox();
          // this.resetBulk();
        } else {
          item ? item.toBeDeleted = false : '';
        }
      });
  }

  // for deleting tags
  openConfirmTag(tag): void {
    const dialogRef = this.dialog.open(DialogConfirmComponent, {
      width: '450px',
      minWidth: '350px',
      data: { type: 'tagConfirm' }
    });
    dialogRef.afterClosed()
      .subscribe(result => result ? this.deleteTag(tag) : '');
  }

  // for deleting tags
  openRenameTag(tag): void {
    const dialogRef = this.dialog.open(DialogConfirmComponent, {
      width: '450px',
      minWidth: '350px',
      data: { type: 'tagRenameConfirm', value: tag.label }
    });
    dialogRef.afterClosed()
      .subscribe(result => {
        result.length > 3 ? this.renameTag(tag, result) : '';
      });
  }

  deleteTag(item) {
    // ...
    // console.log(this.user.uid, item)
    this.afs
      .collection(`media`)
      .doc(this.user.uid)
      .collection(`tags`)
      .doc(item.id)
      .delete();

    // as item has been removed fall back to default
    // this.resetFilters();
    this.selectTag(this.tags[0]);

    // check for 
    this.tagQuota();
  }

  deleteItem(item) {
    // Delete the file
    // console.log('to be deleted: ', item);
    // delete from storage
    const storageRef = this.storage.ref(item.storagePath);
    // storageRef.getMetadata().subscribe(val => console.log('its metadata: ', val));
    storageRef
      .delete()
      // this catches the error and returns an observable
      // printing it in the subscription payload
      .pipe(catchError(error => of(JSON.stringify(error))))
      .subscribe(payload => {
        if (payload) {
          console.log('storage delete error: ', payload);
        }
      });
    
    // google Analytics
    // this.gas.eventEmitter('media', 'delete', item.filename);
  }

  // TODO: the reset bulk operation is not optimal
  // should be triggered after last delete has been executed -> refactor needed
  deleteBulk() {
    // sets a UI change 
    this.bulkDeleteRunning = true;
    this.mediaListItemsSelected.forEach(item => {
      const storageRef = this.storage.ref(item.storagePath);
      storageRef
        .delete()
        // this catches the error and returns an observable
        // printing it in the subscription payload
        .pipe(catchError(error => of(JSON.stringify(error))))
        .subscribe(payload => {
          if (payload) {
            console.log('storage delete error: ', payload);
          }
        });
      });
    setTimeout(() => {
      this.bulkDeleteRunning = false;
      // this.resetBulk();
      this.resetBulkSelectcheckbox();
    }, 6000);
  }

  shareBulk() {
    this.mediaListItemsSelected.forEach(item => {
      console.log('bulk share item...', item)
    })
  }

  /*
  resetBulk() {
    // reset checkbox
    this.bulkSelect.setValue(false);
    // this.bulkSelect.
    // empty array
    this.mediaListItemsSelected = [];
    this.bulkSelect.indeterminate = (false)
  }
  */

  mediaListTaggedDone() {
    // this.resetBulk();
    this.resetBulkSelectcheckbox();
  };

  trackByFn(item) {
    return item.key;
  }

  updateFilename(item) {
    // console.log(item.ref, item)
    // const itemsRef = this.db.list('documents/' + this.user.uid);
    // itemsRef.update(item.md5Hash, { filename: item.filename });
    this.afs
      .doc(`media/${this.user.uid}`)
      .collection('documents')
      .doc(item.key)
      .update({ 
        filename: item.filename,
        metadata: { filename: item.filename },
        updatedOn: Date.now(),
      })
    
    this.toggleFilenameEdit = false;
  }

// end of class
}
