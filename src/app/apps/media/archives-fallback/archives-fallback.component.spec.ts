import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchivesFallbackComponent } from './archives-fallback.component';

describe('ArchivesFallbackComponent', () => {
  let component: ArchivesFallbackComponent;
  let fixture: ComponentFixture<ArchivesFallbackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArchivesFallbackComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchivesFallbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
