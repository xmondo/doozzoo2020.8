import { HttpClient } from '@angular/common/http';
import { Component, Inject, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-archives-fallback-button',
  template: `
    <button
      class="archives-fallback-button"
      mat-icon-button
      [matTooltip]="'media.archiving.fallback-popup.button-tooltip' | translate"
      (click)="open()">
      <mat-icon>more_vert</mat-icon>
    </button>
  `,
  styleUrls: ['./archives-fallback.component.scss'],
})
export class ButtonArchivesFallbackComponent {
  @Input() userId: string;
  constructor(
    public dialog: MatDialog,
  ) { }

  open() {
    const dialogRef = this.dialog.open(ArchivesFallbackComponent, { data: { userId: this.userId}});
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

}

@Component({
  selector: 'app-archives-fallback',
  templateUrl: './archives-fallback.component.html',
  styleUrls: ['./archives-fallback.component.scss']
})
export class ArchivesFallbackComponent {
  httpFunctionsUrl: string = environment.httpFunctionsUrl;
  archivings$: Observable<any>;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private http: HttpClient,
    public dialogRef: MatDialogRef<ArchivesFallbackComponent>,
  ) {}

  ngOnInit() {
    this.archivings$ = this.getArchive();
  }

  getArchive() {
    console.log('*** userId: ', this.data.userId)
    // https://europe-west1-doozzoo-dev.cloudfunctions.net/archiving?action=listarchivescoachid&coachid=auth0|582c7e4cedb8c73048379aad&secret=MeinTollesRumpel!!Stilzchen84
    const url = this.httpFunctionsUrl + `archiving?action=listarchivescoachid&coachid=${this.data.userId}&secret=MeinTollesRumpel!!Stilzchen84`;
    const config = { headers: { 'Accept': 'application/json' } };
    return this.http.get(url, config)
      .pipe(
        // ...
        // filter((val:any) => val.status === 'success'),
        // map((val:any) => val.archive.url),
      );
  }

}
