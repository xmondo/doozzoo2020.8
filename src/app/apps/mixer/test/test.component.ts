import { Component, OnInit } from '@angular/core';
import { Channelstrip } from '../../../shared/channelstrip';
import { AudioService } from '../../../shared/audio.service';

@Component({
  selector: 'app-test2',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class Test2Component implements OnInit {

  testObj: any;
  testObj2: any;
  testMessage = 'Hello world';
  testArr: any;
  constructor(
    public as: AudioService,
  ) {
    this.testArr = as.gainObjects;
  }

  ngOnInit() {
    // const foo = this.as.createChannelstrip('mama', 20, false, true, false);
    // const fooo = this.as.createChannelstrip('helloWorld', 10, true, false, true);
    // const foooo = this.as.createChannelstrip('papa', 10, false, true, false);
    // console.log(this.testObj);
  }

}
