import { Component, OnInit, OnChanges, OnDestroy, SimpleChanges, Input } from '@angular/core';
import { AudioService } from '../../../shared/audio.service';
import { Channelstrip } from '../../../shared/channelstrip';
// import { OpentokService } from '../../../shared/opentok.service';
import * as Tone from 'tone';
// import { map } from 'rxjs/operators';

@Component({
  selector: 'app-channelstrip',
  templateUrl: './channelstrip.component.html',
  styleUrls: ['./channelstrip.component.scss']
})

export class ChannelstripComponent implements OnInit, OnChanges, OnDestroy {
  @Input() gainObj: Channelstrip;
  @Input() remoteStatus: Boolean;
  @Input() isOwner: Boolean;

  isMicrophone: boolean;
  inClassroom = false;

  constructor(
    private as: AudioService, 
    // private ots: OpentokService
  ) {
    // ...
    
  }

  doChange(event) {
    console.log(event);
    this.gainObj.setMute(event.checked);
  }

  ngOnInit() {
    // ...
    // console.log('gainObj: ', this.gainObj.name, this.gainObj.gainObj.volume.value);
    this.isMicrophone = this.gainObj.id === 'microphoneId';
  }

  ngOnDestroy() {
    // ...
    // console.log(`channelstrip ${this.gainObj.name} destroyed`)
  }

  // gives us trigger and value of @input changes
  // will trigger the rewiring based on input changes
  ngOnChanges(changes: SimpleChanges) {
    // console.log(changes);
    // TODO: to prevent issues with disconnecting stuff that was never connected
    // can I use the previous value check
    this.remoteStatus = changes.remoteStatus.currentValue;
  }

// end of class
}
