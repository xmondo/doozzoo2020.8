import { Component, OnInit } from '@angular/core';
import { AudioService } from '../../shared/audio.service';
import { OpentokService } from '../../shared/opentok.service';
import { ControllsService } from '../../shared/controlls.service';
import { DirectshareService } from '../../shared/directshare.service';
import { Router } from '@angular/router';
import { from, Observable } from 'rxjs';
import { AppMode, ToolbarStoreService } from '../toolbar/toolbar-store.service';


@Component({
  selector: 'app-mixer',
  templateUrl: './mixer.component.html',
  styleUrls: ['./mixer.component.scss']
})
export class MixerComponent implements OnInit {
  mode = 'default'; // large | small | tiny
  gainNodes = [];
  masterDelay: number;
  dragPosition: any;
  remoteStatus: boolean = false;
  isOwner: boolean = false;

  gainNodes$: any;
  vm$: Observable<any>;
  
  constructor( 
    public ads: AudioService, 
    private ots: OpentokService,
    public cs: ControllsService,
    private ds: DirectshareService,
    public router: Router,
    // provided in parent
    public store: ToolbarStoreService,
  ) {
    // ...
  }

  ngOnInit() {
    // init store
    this.vm$ = this.store.vm$;

    // this.gainNodes = this.ads.gainNodes;
    this.gainNodes = this.ads.gainObjects;
    this.gainNodes$ = from(this.ads.gainObjects.map(val => val.name));

    this.masterDelay = this.ads.masterDelay.delayTime.value;
    this.remoteStatus = this.ads.remoteStatus;
    // console.log('ads.remoteStatus: ', this.ads.remoteStatus);
    // console.log('mixer delay: ', this.masterDelay);
    // only in room context relevant
    this.isOwner = this.ds.isOwner;
    if (this.router.url.indexOf('/room/') !== -1) {
      // must be invoked for both, owner and student
      this.doSyncRemote();
    }
  }

  toggleMode() {
    this.store.toggleAppMode('mixerId');
  }

  setMasterDelay(factor) {
    /*
    console.log(factor);
    // this.ads.setMasterDelay(factor);
    this.masterDelay += factor;
    this.masterDelay = parseFloat(this.masterDelay.toFixed(3));
    this.ads.masterDelay.delayTime.value = this.masterDelay;
    */
    this.ads.setMasterDelayFactor(factor);
    this.masterDelay = this.ads.getMasterDelay();
  }

  /**
   * subscribes for owner and user to the dedicated firebase obj, 
   * giving the overall remote status.
   * Most simple solution
   */
  doSyncRemote() {
    const tmp = this.ds.getRemoteStatus();
    tmp.subscribe((val: boolean) => {
      // console.log('mixer: ', val);
      this.ads.remoteStatus = val;
      this.remoteStatus = val;
    });
  }

// end of class
}
