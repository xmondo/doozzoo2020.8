
import {
  Component,
  OnInit,
  OnDestroy,
  ElementRef,
  Renderer2,
  ViewChild,
  Input,
  OnChanges,
  SimpleChanges,
  NgZone
} from '@angular/core';
import * as WaveSurfer from 'wavesurfer.js';
import RegionsPlugin from 'wavesurfer.js/dist/plugin/wavesurfer.regions.min.js';
import * as Tone from 'tone';
import { AudioService } from '../../shared/audio.service';
import { Downloader } from '../../shared/downloader';
import { timer, Observable, fromEventPattern, fromEvent, Subscription } from 'rxjs';
import { filter, map, throttleTime, take } from 'rxjs/operators';
import { DirectshareService } from '../../shared/directshare.service';
import { ActivatedRoute } from '@angular/router';
import { Channelstrip } from '../../shared/channelstrip';
import { WindowObject } from '../direct-share-multi/window-object';
import { DsEvent } from '../direct-share-multi/ds-event';
import { SimpleFilter } from 'soundtouchjs';
import { getWebAudioNode } from 'soundtouchjs';
import { SoundTouch } from 'soundtouchjs';
import { DeviceDetectorService } from 'ngx-device-detector';
// import { foo } from './looper.function';
@Component({
  selector: 'app-looper',
  templateUrl: './looper.component.html',
  styleUrls: ['./looper.component.scss'],
  providers: [
    Downloader,
    // WebWorkerService
  ]
})
export class LooperComponent implements OnInit, OnChanges, OnDestroy {
  @ViewChild('waveform', { static: true }) waveform;
  @Input() item: { title: String, data: any, timestamp: number, uploaded: Boolean, mimeType: MimeType };
  @Input() index: number;
  @Input() window: WindowObject;
  @Input() width: number;

  remoteStatus: Boolean = false;
  inClassroom: Boolean = false;

  // handle all subscriptions regarding unsubscribe
  subscription: Subscription = new Subscription();

  ws: WaveSurfer;
  ctx: Tone = Tone.context;
  isPlaying: Boolean = false;
  loading: Boolean = true;
  looperVolume: number = -10;

  // pitchshift
  st: SoundTouch;

  localProgress: Number = 10;
  px: Observable<any>;

  // this.inputDataUrl = this.inputItem.dataUrl;
  decodedBuffer: AudioBuffer = null;
  trimmedBuffer: AudioBuffer = null;
  regionStart;
  regionEnd;
  regionArr = [];

  currentRegion = {
    start: 0,
    end: 0,
    loop: true,
    element: null
  };

  autoCenter;

  // number of pixels in waveform, used to visualize 1 sec of audio
  pxPerSec = 100;
  factor: number = 1.0;
  totalTime = 0;
  playbackRate = 1;
  stretchFactor = 1;
  seekingPos = 0;
  pitch: number = 0; // pitch change in semitones

  source: Observable<any>;

  pbrArr = [ 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1, 1.05, 1.1, 1.15, 1.2, 1.25, 1.3, 1.35, 1.4, 1.45, 1.5, 1.55, 1.6, 1.65, 1.7, 1.75, 1.8, 1.85, 1.9, 1.95, 2 ];
  currentPbr = 10;
  pbrLabel = this.pbrArr[this.currentPbr];

  channelstrip: Channelstrip;

  disableControlls = false;

  constructor(
    public as: AudioService,
    // private ref: ChangeDetectorRef,
    public dl: Downloader,
    public el: ElementRef,
    public renderer: Renderer2,
    public ds: DirectshareService,
    private r: ActivatedRoute,
    public deviceService: DeviceDetectorService,
    private zone: NgZone,
  ) {
    // ...
  }

  ngOnInit() {
    const self = this;

    /**
     * child mode
     */
    if (this.window.isParent === false) {
      this.initRemoteListener();
      this.disableControlls = true;
    }

    /**
     * helper function to redraw waveform, when window is resized (x-axis)
     * reads resize events from directshare component
     * relevant for student!!
     */
    const dsSub = this.ds.dsEvent
      .pipe(
        filter((val: DsEvent) => val.event.action === 'resize'),
        map((val: DsEvent) => val.event),
      )
      .subscribe((val: any) => {
        this.width = val.width;
        this.recalcZoom();
      });
    this.subscription.add(dsSub);

    // only relevant 
    // in room context 
    // and for students
    // map uri segments and only execute once (take 1)
    const uri = this.r.url.pipe(map(segments => segments[0].path, take(1)));
    const uriSub = uri.subscribe(val => this.inClassroom = val === 'room' ? true : false );
    this.subscription.add(uriSub);

    // next after 100ms, then repeat every 100ms
    this.source = timer(100, 100)
      .pipe(map(val => this.ws.getCurrentTime()));

    this.ws = WaveSurfer.create({
      audioContext: this.ctx,
      container: this.waveform.nativeElement,
      height: 128,
      normalize: true,
      autoCenter: true,

      waveColor: '#666',
      progressColor: 'orange',
      barHeight: 1,
      cursorColor: 'orange',

      // changed by zoom function -> min pixel per secon of audio
      // TODO: do we need that?
      minPxPerSec: 50,
      responsive: true,

      cursorWidth: 1,
      // Whether to fill the entire container or draw only according to minPxPerSec
      fillParent: true,
      // Whether to scroll the container with a lengthy waveform. Otherwise the waveform is shrunk to the container width (see fillParent)
      scrollParent: false,

      audioRate: 1,
      partialRender: true,
      pixelRatio: 1,

      plugins: [
        RegionsPlugin.create(
          {
            color: 'green',
          }
        )
      ]
    });
  
    // initialization of wavesurfer
    this.ws.on('ready', function () {
      // loading completed
      self.loading = false;
      self.totalTime = self.ws.getDuration();
      self.calcPxPerSec();

      // Enable creating regions by click/dragg
      self.ws.enableDragSelection({
        loop: false
      });
      // ### connect with mixer
      // TODO: Monitor and NET are set true for each iteration of a loop
      // signal is intercepted and send to mixer via gainNode
      self.ws.toggleMute();

      /**
       * fetch audio nodes from audio service
       * and sets global audio routing for mixer
       */ 
      // new mixer approach     
      self.channelstrip = self.as.createChannelstrip(
        'Player',
        self.looperVolume,
        self.as.globalChannelstripStatus.toMaster,
        self.as.globalChannelstripStatus.toClass,
        self.as.globalChannelstripStatus.muted,
        // self.uid
      );

      if (!self.inClassroom) {
        self.channelstrip.setToMaster(self.channelstrip.isToMaster);
      }

      /**
       * Important: for the looping its crucial, that with every iteration,
       * audio nodes are dis- and re-connected
       * this includes the global mixer nodes.
       * See: doPitchshift()
       */
      self.ws.on('play', () => {
        // console.log('wavesurfer->play');
        self.isPlaying = true;
        self.doPitchshift();
      });

      self.ws.on('pause', () => {
        // console.log('wavesurfer->pause');
        self.isPlaying = false;

        self.ws.backend.disconnectFilters();

        // self.ref.detectChanges();
      });

      self.ws.on('seek', function() {
        // 
      });

      self.ws.on('finish', () => {
        self.isPlaying = false;
      });

    });

    // ### regions functions ###
    self.ws.on('region-created', rg => {
      self.currentRegion = rg;
      // console.log('region-created: ', rg);

      if (self.regionArr.length > 0) {
        self.regionArr.forEach(item => item.remove());
      }
      self.regionStart = rg.start;
      self.regionEnd = rg.end;
      self.regionArr.push(rg);
      rg.loop = true;
      // set custom classes
      this.renderer.setAttribute(rg.element, 'class', 'wavesurfer-region looped');
      this.renderer.setAttribute(rg.element, 'title', 'hello region');

    });

    /**
     * ### provides region-updated events ###
     * handling with rxjs provides options 
     * to throttle emitted event frequency to direct share
     */
    fromEventPattern(
      (handler: any) => this.ws.on('region-updated', handler),
    ).pipe(
        map(val => val),
        throttleTime(100),
    ).subscribe((rg: any) => {
      // console.log('region updated: ', rg);
      self.regionStart = rg.start;
      self.regionEnd = rg.end;
      // ### handle remote region ###
      this.ds.dsEvent.next({
        windowId: self.window.id,
        isParent: self.window.isParent,
        target: 'viewer',
        event: {
          action: 'updateRegion',
          value: {
            id: rg.id,
            loop: rg.loop,
            start: rg.start,
            end: rg.end
          }
        }
      });
      // self.ref.detectChanges();
    });

    this.ws.on('region-click', (rg: any) => {
      // console.log('selected region id: ', rg.id, rg.start, rg.end);
      // select region
      this.currentRegion = rg;
      rg.loop = true;
      // remove classes
      this.el.nativeElement.querySelectorAll('.wavesurfer-region');
      const el = this.el.nativeElement.querySelectorAll('.wavesurfer-region');
      el.forEach((item, key) => {
        this.renderer.removeClass(item, 'selected');
        // console.log(item);
      });
      // wenn angeklickt, soll es selected und looped sein
      this.renderer.setAttribute(rg.element, 'class', 'wavesurfer-region selected looped');
    });

    this.ws.on('region-dblclick', rg => {
      rg.remove();
      this.currentRegion.start = 0;
      this.currentRegion.end = 0;
      this.currentRegion.loop = false;
      this.regionArr = [];
      this.composeEvent({ action: 'removeRegion', value: rg.id });
    });

    // Load with Tone
    this.loadBuffer();

  // ### ngOnInit end ###
  }

  loadBuffer() {
    const buffer = new Tone.Buffer(
      this.window.asset.downloadLink,
      (result) => {
        // First load buffer, when available...
        // console.log('the loadBuffer is now available', result);
        this.loading = false;
        const myAudioBuffer = buffer.get();
        // ...init WS and apply audioBuffer
        // this.initWs();
        this.ws.loadDecodedBuffer(myAudioBuffer);
      },
      error => console.log('Tone.Buffer error: ', error)
    );
  }

  /** pitch shift section */
    
  doPitchshift() {

    let st = new SoundTouch(this.ws.backend.ac.sampleRate);
    const buffer = this.ws.backend.buffer;
    const channels = buffer.numberOfChannels;
    const l = buffer.getChannelData(0);
    const r = channels > 1 ? buffer.getChannelData(1) : l;
    const length = buffer.length;
    let seekingPos = null;
    let seekingDiff = 0;
    let soundtouchNode = null;

    let source = {
      extract: (target, numFrames, position) => {
        if (seekingPos != null) {
          seekingDiff = seekingPos - position;
          seekingPos = null;
        }

        position += seekingDiff;

        for (let i = 0; i < numFrames; i++) {
          target[i * 2] = l[i + position];
          target[i * 2 + 1] = r[i + position];
        }
        return Math.min(numFrames, length - position);
      }
    };

    // "~~" equals "Math.floor", but should be faster
    seekingPos = ~~(this.ws.backend.getPlayedPercents() * length);
    // console.log('seekingPos: ', seekingPos);

    st.tempo = this.ws.getPlaybackRate();
    st.pitchSemitones = this.pitch;

    // disconnect all audio nodes first,
    // otherwise no audio
    this.ws.backend.disconnectFilters();
    this.channelstrip.gainObj.disconnect();
    this.channelstrip.switchAll(
      this.channelstrip.isToMaster,
      this.channelstrip.isToClass,
      this.channelstrip.isMuted
    );
    
    if (st.tempo === 1) {
      this.ws.backend.setFilter(this.channelstrip.gainObj);
    } else {
      if (!soundtouchNode) {
        var filter = new SimpleFilter(source, st);
        soundtouchNode = getWebAudioNode(
          this.ws.backend.ac,
          filter
        );
      }
      soundtouchNode.connect(this.channelstrip.gainObj);
      this.ws.backend.setFilter(soundtouchNode, this.channelstrip.gainObj);
    }
  }
  // end pitchshift


  ngOnChanges(changes: SimpleChanges) {
    // triggers window width changes to the wavesurfer canvas renderer
    // console.log('changes: ', changes);
    if (changes.width.currentValue) {
      this.width = changes.width.currentValue;
      this.recalcZoom();
    }
  }
  
  ngOnDestroy() {
    // container subscription for handling unsubscribe 
    // for all active subscriptions of this module
    this.subscription.unsubscribe();
    //TODO: throws errors?!
    this.channelstrip.removeFromArray();
    try {
      this.ws.destroy();
    } catch (error) {
      console.warn('LooperComponent->ngOnDestroy: ', error);
    }
  }

  // remote
  // relevant for child windows with "isRemoteActive = true"
  initRemoteListener() {
    // used to provide to helper functions
    // such as "setRemoteStatus"
    const tmpL = this.ds.remoteDSWindowsListener(
      this.ds.remoteSessionId,
      this.window.id
    )
    .pipe(
      // we filter for viewer events only
      filter(val => val.target === 'viewer'),
      // and only for an active "remote status"
      // and after loading has been finished
      filter(() => this.window.isRemoteActive && !this.loading),
    )
    .subscribe(val => {
      console.log('remoteDSWindowsListener->looper: ', val, this.loading);
      switch (val.event.action) {
        case 'play':
          this.ws.play(val.event.value);
          break;
        case 'pause':
          this.ws.pause();
          break;
        case 'regionStart':
          // in case region has not been remotely set,
          // we have a fallback
          if (this.regionArr[0]) {
            this.ws.play(this.regionArr[0].start);
          } else {
            this.toStart();
          }
          break;
        case 'toStart':
          this.toStart();
          break;
        case 'updateRegion':
          this.remoteRegion(val.event.value);
          break;
        case 'removeRegion':
          this.removeRegion(val.event.value);
          break;
        case 'zoom':
          this.ws.zoom(val.event.value.pxPerSec);
          this.factor = val.event.value.factor;
          break;
        case 'playbackRate':
          // TODO: finally we only need "pbrLabel"
          // no clues why "currentPbr" is submitted as well
          this.pbrLabel = val.event.value.pbrLabel;
          this.playbackRate = val.event.value.pbrLabel;
          this.ws.setPlaybackRate(this.playbackRate);
          break;
        case 'pitch':
          this.pitch = val.event.value.pitch;
          break;
        // { action: 'regionLoop', value: this.currentRegion.loop }
        case 'regionLoop':
          this.regionLoop(val.event.value);
          break;
      }
    });
    this.subscription.add(tmpL);
  }

  remoteRegion(region) {
    console.log('remoteRegion: ', event);

    if (this.regionArr.length > 0) {
      this.regionArr.forEach(item => item.remove());
      // console.log(this.regionArr[0]);
    }

    // tslint:disable-next-line:max-line-length
    this.currentRegion = this.ws.addRegion({
      id: region.id,
      start: region.start,
      end: region.end,
      loop: region.loop
    });

    this.regionStart = region.start;
    this.regionEnd = region.end;
    this.currentRegion.loop = region.loop;

    this.regionArr.push(this.currentRegion);
    console.log('updated array: ', this.regionArr);
  }

  removeRegion(regionId) {
    console.log('do remove region: ', regionId);
    // keep it simple as long as we allow only one region
    this.ws.clearRegions();
    // rg.remove();

    // TODO: refactoring candidate
    this.currentRegion.start = 0;
    this.currentRegion.end = 0;
    this.currentRegion.loop = false;
    this.regionArr = [];
    /*
    if (this.regionArr.length > 0) {
      this.regionArr.forEach(item => item.remove());
      // console.log(this.regionArr[0]);
    }
    */
  }

  // ### ws functions ###

  playPause(ct?) {

    if (!ct) { ct = this.ws.getCurrentTime(); }
    if (this.ws.isPlaying()) {
      this.ws.pause();
      this.composeEvent({ action: 'pause' });
    } else if (this.regionArr.length > 0) {
      this.ws.play(this.regionArr[0].start);
      this.composeEvent({ action: 'regionStart' });
    } else {
      if (this.window.isParent) {
        this.composeEvent({ action: 'play', value: ct });
      }
      this.ws.play(ct);
    }
  }

  toStart() {
    // by chaining play and stop, waveform will be reset to beginning, otherwise no redraw of waveform
    this.ws.play(0, 0.1);
    this.ws.stop();
    this.composeEvent({ action: 'toStart' });
  }

  // ### zooming ###
  // reset relatively to the window with
  resetZoom() {
    // console.log('reset');
    const windowWidth = this.width || 630;
    this.pxPerSec = Math.floor(windowWidth / this.totalTime);
    this.ws.zoom(this.pxPerSec);
    this.factor = 1;
    // this.ws.drawBuffer();
  }

  recalcZoom(){
    const oldPxPerSec = this.pxPerSec;
    const windowWidth = this.width || 630;
    const pxPerSecZoomFactor1 = Math.floor(windowWidth / this.totalTime);
    // when zoom factor was 1, in other words: no zoom happened so far,
    // then scale to the new width of the resized window
    // otherwise do noting, as the zoomed waveform is anyway bigger than the window
    if (oldPxPerSec < pxPerSecZoomFactor1) {
      this.pxPerSec = pxPerSecZoomFactor1;
      this.ws.zoom(this.pxPerSec);
    }
    // for UI only
    this.factor = (this.pxPerSec / pxPerSecZoomFactor1);
    // console.log('zoom->oldPxPerSec, pxPerSec: ', oldPxPerSec, pxPerSecZoomFactor1);
  }

  // must be done when initializing
  // and when resizing window
  calcPxPerSec() {
    // 630 is minwidth of looper window
    // after resizing this.width should be populated
    const windowWidth = this.width || 630;
    this.pxPerSec = Math.floor(windowWidth / this.totalTime);
    // console.log('zoom pxPerSec: ', this.pxPerSec);
  }

  zoom = (step: number) => {
    
    // init with taking the 100% view calculated into px/sec
    let newPxPerSec = this.pxPerSec === 2 && step < 0 ? 1 : Math.floor(this.pxPerSec * (1 + step));
    // minimum increase by one
    if (this.pxPerSec === newPxPerSec) {
      newPxPerSec++;  
    } else if(newPxPerSec < 0) {
      newPxPerSec = 1;
    }
    const windowWidth = this.width || 630;
    const pxPerSecZoomFactor1 = Math.floor(windowWidth / this.totalTime);

    // console.log('zoom->pxPerSec, newPxPerSec, pxPerSecZoomFactor1 ', this.pxPerSec, newPxPerSec, pxPerSecZoomFactor1);

    // for very large files, 
    // when factor would not increase due to rounding
    if (newPxPerSec >= pxPerSecZoomFactor1) {
      if (newPxPerSec === pxPerSecZoomFactor1) {
        // increase at least by one
        newPxPerSec++;
        this.pxPerSec = newPxPerSec;
        this.ws.zoom(this.pxPerSec);  
      } else {
        this.pxPerSec = newPxPerSec;
        this.ws.zoom(this.pxPerSec);
      }
    } else  {
      this.pxPerSec = pxPerSecZoomFactor1;
      this.ws.zoom(pxPerSecZoomFactor1);
    }
    // for UI only
    this.factor = (this.pxPerSec / pxPerSecZoomFactor1);
    console.log('zoom->pxPerSec: ', this.pxPerSec, this.factor);

    this.composeEvent({ 
      action: 'zoom',
      value: {
        pxPerSec: this.pxPerSec,
        factor: this.factor
      }
    });

  }

  stepPlaybackRate = step => {
    const tmp = this.currentPbr + step;
    // console.log(tmp, pbrArr.length)
    if (tmp >= 0 && tmp < this.pbrArr.length) {
      this.setPlaybackRate(this.pbrArr[tmp]);
      this.pbrLabel = this.pbrArr[tmp];
      this.currentPbr = tmp;

      this.composeEvent({
        action: 'playbackRate',
        value: {
          pbrLabel: this.pbrLabel,
          currentPbr: this.currentPbr
        }
      });
    }
  }

  // changes in slider
  setPlaybackRate = pbr => {
    // use slider
    // self.playbackRate = self.playbackRateFactor/100;
    // use +-
    this.playbackRate = pbr || 1;
    // translate playback rate into alpha/stretch: 4->0.25, 2->0.5, 1->1, 0.5->2, 0.25->4
    // y = x / Math.pow(x, 2)
    this.stretchFactor = this.playbackRate / Math.pow(this.playbackRate, 2);
    // ~~ = Math.floor()
    this.seekingPos = Math.floor((this.ws.backend.getPlayedPercents() * this.ws.backend.buffer.length));
    // ...
    // console.log('setPlaybackRate to: ', this.playbackRate);
    this.ws.setPlaybackRate(this.playbackRate);
  }

  stepPitch = step => {
    const max = 8;
    const min = -8;
    if (this.pitch + step < max && this.pitch + step > min) {
      this.pitch = this.pitch + step;
      this.ws.pause();
      this.ws.play();
    }
    
    this.composeEvent({
      action: 'pitch',
      value: {
        pitch: this.pitch,
      }
    });

  }

  regionLoop(status?: boolean) {
    // console.log(this.currentRegion.id)
    status !== undefined ?
      this.currentRegion.loop = status :
      this.currentRegion.loop = this.currentRegion.loop ? false : true;
    // console.log(status, this.currentRegion.loop);
    // assign class
    this.currentRegion.loop ?
      this.renderer.addClass(this.currentRegion.element, 'looped') :
      this.renderer.removeClass(this.currentRegion.element, 'looped');

    this.composeEvent({ action: 'regionLoop', value: this.currentRegion.loop });
  }

  regionAllLoop(status?: boolean) {
    console.log(this.currentRegion)
    this.ws.clearRegions();
    this.regionArr = [];
    // tslint:disable-next-line:max-line-length
    const length: number = this.ws.getDuration();
    console.log('length: ', length)
    this.currentRegion = this.ws.addRegion({
      id: 'regionAll',
      start: 0,
      end: length.toFixed(2), // BUG or change: only a precision of 2 can be resolved when looping the complete track
      loop: true
    });
    this.renderer.addClass(this.currentRegion.element, 'looped')   
    //this.composeEvent({ action: 'regionLoop', value: this.currentRegion.loop });
  }

  stopDrag(status) {
    this.ds.directshareEmitter.next({
      target: 'directShare',
      event: {
        action: 'stopdrag',
        event: status
      }
    });
  }

/*
  setAutoCenter(event){
    console.log(event)
    this.ws.autoCenter = event.checked;
  }
*/
  /**
   * helper for compose event
   */
  composeEvent(value: DsEvent['event']) {
    // console.log('composeEvent: ', value)
    this.ds.dsEvent.next({
      windowId: this.window.id,
      isParent: this.window.isParent,
      target: 'viewer',
      event: value // example: { action: 'pause', value. 'foo' }
    });
  }

// ### end of class
}
