import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { OpentokService } from '../../shared/opentok.service';
import { DirectshareService } from '../../shared/directshare.service';
import { AudioService } from 'src/app/shared/audio.service';
import * as Tone from 'tone';
import { MyUid } from 'src/app/shared/my-uid';
import { Channelstrip } from 'src/app/shared/channelstrip';

@Component({
  selector: 'app-test-tone',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss'],
  providers: [
    // DirectShareService
  ]
})
export class TestToneComponent implements OnInit, OnDestroy {
  @Input() note: string;
  @Input() layout: string;
  oscillator: Tone.Oscillator;
  ctx = Tone.context;
  oscGain: any;
  channelstrip: Channelstrip;

  constructor(
    public ads: AudioService,
  ) { 
    this.note = this.note || 'C4';
  }

  ngOnInit() {

    const ccn = MyUid.newGuid();

    this.oscillator  = new Tone.FMOscillator(this.note, "sine"); // .toMaster().start();
    // channelstripName, level, toMaster, toClass, mute
    // const channelstrip = this.ads.createChannelstrip('test', -10, true, false, false, ccn);
    this.channelstrip = this.ads.createChannelstrip(`Test ${this.note}`, -20, true, false, false, `Test_${this.note}`);
    this.oscGain = this.channelstrip.gainObj;
  }

  ngOnDestroy() {
    this.channelstrip.dispose();  
  }

  togglePlay() {
    if(this.oscillator.state === 'started') {
      this.oscillator.stop();
      this.oscillator.disconnect();
    } else {
      this.oscillator.connect(this.oscGain);
      this.oscillator.start();
    }
  }

// end of class  
}
