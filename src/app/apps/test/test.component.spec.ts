import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestToneComponent } from './test.component';

describe('TestToneComponent', () => {
  let component: TestToneComponent;
  let fixture: ComponentFixture<TestToneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestToneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestToneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
