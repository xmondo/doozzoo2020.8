import { Component, ElementRef, OnInit, Renderer2, OnDestroy, ChangeDetectionStrategy, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';
import { fromEvent, Subscription } from 'rxjs';
import { filter, map, take } from 'rxjs/operators';
import { AudioService } from 'src/app/shared/audio.service';
import { Channelstrip } from 'src/app/shared/channelstrip';
import { DirectshareService } from 'src/app/shared/directshare.service';
import * as Tone from 'tone';
import { DsEvent } from '../direct-share-multi/ds-event';
import { AppMode, ToolbarStoreService } from '../toolbar/toolbar-store.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-keyboard',
  templateUrl: './keyboard.component.html',
  styleUrls: ['./keyboard.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class KeyboardComponent implements OnInit, OnDestroy, AfterViewInit {
  // @ViewChild('keyboardDiv') keyboardDiv: ElementRef;

  keyMap = {
    'KeyZ': 48,
    'KeyS': 49,
    'KeyX': 50,
    'KeyD': 51,
    'KeyC': 52,
    'KeyV': 53,
    'KeyG': 54,
    'KeyB': 55,
    'KeyH': 56,
    'KeyN': 57,
    'KeyJ': 58,
    'KeyM': 59,
    'KeyQ': 60,
    'Digit2': 61,
    'KeyW': 62,
    'Digit3': 63,
    'KeyE': 64,
    'KeyR': 65,
    'Digit5': 66,
    'KeyT': 67,
    'Digit6': 68,
    'KeyY': 69,
    'Digit7': 70,
    'KeyU': 71,
    'KeyI': 72,
  };
  loading: boolean = true;
  buffer: AudioBuffer = null;
  ctx = Tone.context;
  // computer keys are not activated per default -> change to the original ideas
  keysActive: boolean = false;
  keysGain: any;
  noteActive: boolean = false;
  channelstrip: Channelstrip = null;

  vm$: Observable<any>;

  subContainer: Subscription = new Subscription();

  constructor(
    private renderer: Renderer2,
    private ads: AudioService,
    public ar: ActivatedRoute,
    private ds: DirectshareService,
    public store: ToolbarStoreService,
  ) { 
    // ...
  }

  ngOnInit(): void {
    // init store
    this.vm$ = this.store.vm$;

    // only relevant 
    // in room context 
    // and for students
    const remoteSub = this.ar.url
      .pipe(
          map(segments => segments[0].path),
          map(val => val === 'room' ? true : false),
          // map(val => val === 'apps' ? true : false),
          // TODO: better distinctUntilChanged?
          take(1)
      )
      // initialization of remote services already have been done
      // only the listener has to be set up in case of not beeing room owner
      .subscribe(val => {
          if (!this.ds.isOwner && val) { 
              // console.log('keyboard->initRemoteListener->isOwner, isRoom: ', this.ds.isOwner, val);
              // this.showRemoteIndicator = false;
              this.initRemoteListener(); 
          } else if (this.ds.isOwner) {
              // this.showRemoteIndicator = true;
          }
      });
    this.subContainer.add(remoteSub);
  }

  ngAfterViewInit() {
    this.loadBuffer();
  }

  ngOnDestroy() {
    this.subContainer.unsubscribe();
  }

  loadBuffer() {
    const audioUrl = 'assets/audio/keyboard/harpsichord-C4.mp3';
    const buffer = new Tone.Buffer(
      audioUrl,
      (result) => {
        // First load buffer, when available...
        // console.log('the loadBuffer is now available', result);
        this.loading = false;
        this.buffer = buffer.get();
        // ...init WS and apply audioBuffer
        this.initKeyboard();
        this.initAudioConnections();
      },
      error => console.log('Tone.Buffer error: ', error)
    );
  }

  initAudioConnections() {
    this.channelstrip = this.ads.createChannelstrip(`Keyboard`, -10, true, false, false, `keyboardId`);
    this.keysGain = this.channelstrip.gainObj;

    // before starting sound, make sure the patchbay wiring is correct
    this.channelstrip.rewire();
    /*
    // global wiring
    this.channelstrip.switchAll(
      this.ads.globalChannelstripStatus.toMaster,
      this.ads.globalChannelstripStatus.toClass,
      this.ads.globalChannelstripStatus.muted
    )
    */
  }

  initKeyboard() {
    console.log()
    // const el = this.keyboardDiv.nativeElement;
    const el = this.renderer.selectRootElement(document.getElementById('keyboardDiv'), true);
    const kbMousedowns = fromEvent(el, 'mousedown');
    const kbSub = kbMousedowns
      .pipe(
        filter((e: any) => e.target.classList.contains('keyboard__key'))
      )
      .subscribe(event => {
        // console.log('kbMousedowns: ', event);
        this.playSample(+event.target.dataset.note);

        // remote
        this.composeEvent({ 
          action: 'keyDown',
          value: +event.target.dataset.note ?? 48, // if undefined, fallback to C
        });
      });
    this.subContainer.add(kbSub);

    const kbMouseups = fromEvent(el, 'mouseup');
    const kbuSub = kbMouseups
      .pipe(
        filter((e: any) => e.target.classList.contains('keyboard__key'))
      )
      .subscribe(event => {
        // console.log('kbMouseups: ', event);
        // this.playSample(+event.target.dataset.note);
        // remote
        this.composeEvent({ 
          action: 'keyUp',
          value: +event.target.dataset.note ?? 48, 
        });
      });
    this.subContainer.add(kbuSub);
    
    const kbKeydowns = fromEvent(document, 'keydown');
    const kbdSub = kbKeydowns
      .pipe(
        //debounceTime(100),
        // distinctUntilChanged((prev:any, curr:any) => prev.code === curr.code),
      )
      .subscribe((event: any) => {
        console.log('kbKeydowns: ', event);
        const note = this.keyMap[event.code];
        if (note) {
          this.keysActive ? this.playSample(note) : '';
          // this.keyboardDiv.nativeElement.querySelector(`[data-note="${note}"]`).classList.add('keyboard__key--active');
          const el = this.renderer.selectRootElement(document.querySelector(`[data-note="${note}"]`), true);
          this.renderer.addClass(el, 'keyboard__key--active');
          this.activityIndicator();
        }

        // remote
        this.composeEvent({ 
          action: 'keyDown',
          value: note ?? 48, // avoid undefined
        });

      });
    this.subContainer.add(kbdSub);

    const kbKeyups = fromEvent(document, 'keyup');
    const kbkSub = kbKeyups
      .subscribe((event: any) => {
        // console.log('keyups: ', event);
        const note = this.keyMap[event.code];
        if (note) {
          // this.keyboardDiv.nativeElement.querySelector(`[data-note="${note}"]`).classList.remove('keyboard__key--active');
          const el = this.renderer.selectRootElement(document.querySelector(`[data-note="${note}"]`), true);
          this.renderer.removeClass(el, 'keyboard__key--active');
        }

        // remote
        this.composeEvent({ 
          action: 'keyUp',
          value: note ?? 48, 
        });

      });

    this.subContainer.add(kbkSub);
  }

  playSample(note) {
    const source = this.ctx.createBufferSource();
    source.buffer = this.buffer;
    source.playbackRate.value = 2 ** ((note - 60) / 12);
    source.connect(this.channelstrip.gainObj);
    // really?
    this.channelstrip.rewire();

    source.start(this.ctx.currentTime);
    // source.stop(this.ctx.currentTime + 0.3);
  }

  toggleMode() {
    this.store.toggleAppMode('keyboardId');
  }

  toogleKeys() {
    this.keysActive = this.keysActive ? false : true;
  }

  activityIndicator() {

    this.noteActive = true;
    const foo = setTimeout(() => {
      this.noteActive = false;
      clearTimeout(foo);
    }, 200);
  }

  // remote push
  composeEvent(value: DsEvent['event']) {
    // console.log('MetronomeComponent->composeEvent: ', this.ds.userId);
    let eventData = {
      senderId: this.ds.userId,
      // ...
      isParent: this.ds.isOwner, 
      target: 'keyboard',
      event: value,
    }
    this.ds.dsEvent.next(eventData);
    // console.log('composeEvent->eventData: ', eventData)
  }

  // remote subscribe
  // relevant for child windows with "isParent = false"
  initRemoteListener() {
      // used to provide to helper functions
      // such as "setRemoteStatus"
      const listenerSub = this.ds.remoteDSAppsListener(
          this.ds.remoteSessionId,
          'keyboard'
      )
      .pipe(
          // we filter for events not from myself only
          // filter(val => val.senderId !== this.ds.userId),
          // we filter for events from owner only
          filter(val => val.senderId === this.ds.isOwnerId),
      )
      .subscribe(val => {
          // console.log('remoteDSAppsListener->keyboard: isOwner, userId, event: ', this.ds.isOwnerId, this.ds.userId, val);
          switch (val.event.action) {
            case 'keyDown':
              // console.log('keyboard event keyDown: ', val.event);
              let note = val.event.value ?? 48;
              if (note) {
                this.keysActive ? this.playSample(note) : '';
                // this.keyboardDiv.nativeElement.querySelector(`[data-note="${note}"]`).classList.add('keyboard__key--active');
                const el = this.renderer.selectRootElement(document.querySelector(`[data-note="${note}"]`));
                this.renderer.addClass(el, 'keyboard__key--active');
                this.activityIndicator();
              }
              break;
            case 'keyUp':
              // console.log('keyboard event keyUp: ', val.event);
              note = val.event.value ?? 48;
              if (note) {
                // this.keyboardDiv.nativeElement.querySelector(`[data-note="${note}"]`).classList.remove('keyboard__key--active');
                const el = this.renderer.selectRootElement(document.querySelector(`[data-note="${note}"]`));
                this.renderer.removeClass(el, 'keyboard__key--active');
              }
              break;
          }
      });
  }
  
// end of class
}
