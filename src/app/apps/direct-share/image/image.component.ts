import { Component, OnInit, Input, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { DirectshareService } from '../../../shared/directshare.service';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent implements OnInit {
  @Input() inputAsset: any;
  @ViewChild('image', { static: true }) image: ElementRef;
  localProgress: Boolean = true;
  constructor(
    public ref: ChangeDetectorRef,
    public ds: DirectshareService,
  ) { }

  ngOnInit() {
    // console.log(this.inputAsset.filename);
    this.image.nativeElement.src = this.inputAsset.downloadLink;
    // show loading for 1sec
    setTimeout(() => this.localProgress = false, 1000);

    // subscibe to stream from EventEmitter
    this.ds.directshareEmitter.subscribe(payload => {
      // console.log('###directShareEmitter->subscription: ', payload);
      if (payload.action === 'default' && payload.asset.contentType.indexOf('image') !== -1 ) {
        this.localProgress = true;
        this.image.nativeElement.src = payload.asset.downloadLink;
        // this.ref.detectChanges();
        setTimeout(() => this.localProgress = false, 1000);
      }
    });

  }
}
