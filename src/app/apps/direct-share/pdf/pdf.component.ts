import { Component, OnInit, OnDestroy , Input } from '@angular/core';
import { DirectshareService } from '../../../shared/directshare.service';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { filter, takeWhile } from 'rxjs/operators';
import { WindowObject } from '../../direct-share-multi/window-object';
import { DsEvent } from '../../direct-share-multi/ds-event';

@Component({
  selector: 'app-pdf',
  templateUrl: './pdf.component.html',
  styleUrls: ['./pdf.component.scss']
})
export class PdfComponent implements OnInit, OnDestroy {
  @Input() inputAsset: any;
  @Input() remoteStatus: Boolean = false;
  @Input() window: WindowObject;

  pdfSrc: String;
  page = 1;
  numPages = 1;
  zoom = 0.8;
  subscriptionContainer: Subscription = new Subscription();

  localProgress: Boolean = true;

  disableControlls = false;

  constructor(
    // public ref: ChangeDetectorRef,
    public ds: DirectshareService,
    public pdf: PdfViewerModule,
    public ar: ActivatedRoute,
  ) { }

  ngOnInit() {
    console.log('pdf: ', this.window.asset.downloadLink);
    this.pdfSrc = this.window.asset.downloadLink;

    /**
     * child mode
     */
    if (this.window.isParent === false) {
      this.initRemoteListener();
      this.disableControlls = true;
    }
  }

  ngOnDestroy() {
    this.subscriptionContainer.unsubscribe();
  }

  doPaginate(factor: number) {
    const tmp = this.page + factor;
    if (tmp >= 1 && tmp <= this.numPages) {
      this.page = tmp;
      if (this.ds.isOwner) {
        // this.ds.directshareEmitter.next({ target: 'pdf', event: { action: 'paginate', value: this.page } });
      }
      this.composeEvent({ action: 'paginate', value: this.page });
    }
  }

  doZoom(factor: number) {
    this.zoom = this.zoom + factor;
    this.zoom.toPrecision(3);
    if (this.ds.isOwner) {
      // this.ds.directshareEmitter.next({ target: 'pdf', event: { action: 'zoom', value: this.zoom } });
    }
    this.composeEvent({ action: 'zoom', value: this.zoom });
    // console.log('zoom: ', this.zoom);
  }

  loadComplete(pdf: any = null) {
    // do anything with "pdf"
    // console.log('pdf->loadComplete: ', pdf.numPages);
    this.numPages = pdf.numPages;
    this.localProgress = false;
  }

  // remote
  // relevant for child windows with "isParent = false"
  initRemoteListener() {
    // used to provide to helper functions
    // such as "setRemoteStatus"
    const tmpL = this.ds.remoteDSWindowsListener(
      this.ds.remoteSessionId,
      this.window.id
    )
    .pipe(
      // we filter for viewer events only
      filter(val => val.target === 'viewer'),
      // we only take them if remote status of the window is active
      filter(() => this.window.isRemoteActive)
    )
    .subscribe(val => {
      console.log('remoteDSWindowsListener->pdf: ', val);
      switch (val.event.action) {
        case 'paginate':
          this.page = val.event.value;
        break;
        case 'zoom':
            this.zoom = val.event.value;
        break;
      }
    });
    this.subscriptionContainer.add(tmpL);
  }

  /**
 * helper for compose event
 */
composeEvent(value: DsEvent['event']) {
  this.ds.dsEvent.next({
    windowId: this.window.id,
    isParent: this.window.isParent,
    target: 'viewer',
    event: value // example: { action: 'pause' }
  });
}

}
