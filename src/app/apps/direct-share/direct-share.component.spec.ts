import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectShareComponent } from './direct-share.component';

describe('DirectShareComponent', () => {
  let component: DirectShareComponent;
  let fixture: ComponentFixture<DirectShareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectShareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectShareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
