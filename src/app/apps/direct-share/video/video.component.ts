import { Component, OnInit, AfterViewInit, Input, ViewChild, ElementRef, ChangeDetectorRef, Renderer2 } from '@angular/core';
import { DirectshareService } from '../../../shared/directshare.service';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss']
})
export class VideoComponent implements AfterViewInit {
  @Input() inputAsset: any;
  @ViewChild('video', { static: true }) video: ElementRef;
  @ViewChild('source') source: ElementRef;
  localProgress: Boolean = true;
  constructor(
    public ref: ChangeDetectorRef,
    public ds: DirectshareService,
    public renderer: Renderer2,
  ) { }

  ngAfterViewInit() {
    // console.log(this.inputAsset.filename);
    // this.source.nativeElement.src = this.inputAsset.downloadLink;

    let sourceEl = this.renderer.createElement('source');
    this.renderer.setAttribute(sourceEl, 'src', this.inputAsset.downloadLink);
    this.renderer.appendChild(this.video.nativeElement, sourceEl);
    this.video.nativeElement.play();

    // show loading for 1sec
    setTimeout(() => this.localProgress = false, 1000);

    // subscribe to stream from EventEmitter
    this.ds.directshareEmitter.subscribe(payload => {
      console.log('###directShareEmitter->subscription: ', payload);
      if (payload.action === 'default' && payload.asset.contentType.indexOf('video') !== -1) {
        this.localProgress = true;

        this.video.nativeElement.pause();
        this.renderer.removeChild(this.video.nativeElement, sourceEl);
        sourceEl = this.renderer.createElement('source');
        this.renderer.setProperty(sourceEl, 'src', payload.asset.downloadLink);
        this.renderer.appendChild(this.video.nativeElement, sourceEl);
        this.video.nativeElement.load();
        this.video.nativeElement.play();

        // this.ref.detectChanges();
        setTimeout(() => this.localProgress = false, 1000);
      }
    });
  }

// end of class
}
