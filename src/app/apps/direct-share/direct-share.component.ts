import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ElementRef, EventEmitter, Renderer2 } from '@angular/core';
import { DirectshareService } from '../../shared/directshare.service';
import { OpentokService } from '../../shared/opentok.service';
import { fromEvent, Subscription } from 'rxjs';
import { throttleTime } from 'rxjs/operators';
import { Router } from '@angular/router';
import { filter, map, take } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-direct-share',
  templateUrl: './direct-share.component.html',
  styleUrls: ['./direct-share.component.scss'],
})
export class DirectShareComponent implements OnInit, OnDestroy, AfterViewInit  {
  @ViewChild('directshareWindow') directshareWindow: ElementRef;
  @ViewChild('directshareContent') directshareContent: ElementRef;
  dragEmitter: EventEmitter<any> = new EventEmitter();
  rzEmitter: EventEmitter<any> = new EventEmitter();
  showHide: Boolean = false;
  asset: any = {};
  contentType: String = null;
  style: any;
  position: object = { x: Number, y: Number }; // {x: 50, y: 50}
  size: object = { width: Number, height: Number }; // {x: 50, y: 50}
  remoteStatusUI: Boolean = false;
  remoteStatus: Boolean = false;
  draggableStatus: Boolean = true;
  // defines where the window shows handles for resizing
  rzHandles = 'all';
  subscriptionContainer: Subscription = new Subscription;

  constructor(
    public ds: DirectshareService,
    public ot: OpentokService,
    public router: Router,
    public ar: ActivatedRoute,
    public renderer: Renderer2
  ) {
    // ...
  }

  ngOnInit() {
    // 
    this.position = { x: 0, y: 0 };

    // subscibe to stream from EventEmitter
    /**
     * case 1: user opens media viewer on his own
     * fetch asset informations from emitter: filename, content type, etc.
     * open window with fitting viewer
     * 
     * case 2: wiever are opened by remote direct share function
     * open window only for pdf/image content type, in case of remoteStatus being false
     */

    // check media || classroom
    // and provide it to the next call
    this.ar.url
      .pipe(
        map(segments => segments[0].path),
        map(val => val === 'room' ? true : false),
        take(1)
      )
      .subscribe(val => {
        // student && inClassroom
        if (!this.ds.isOwner && val) {
          console.log('app-direct-share->student && inClassroom', this.ds.isOwner, val);
          /**
           * request in two steps
           * 1. find out remote status
           * 2. initDirectShareDefault and submit the regarding status
           */
          this.doGetRemoteStatusFirst(val);
        // all other cases
        } else {
          console.log('app-direct-share->!(student && inClassroom)', this.ds.isOwner, val);
          this.initDirectShareDefault(val);
        }
    });
  }

  ngAfterViewInit(): void {

  }

  /**
   * 
   * @param isInClassroom -> true | false | 
   * @param remoteStatus 
   */
  initDirectShareDefault(
    isInClassroom: boolean = null,
    remoteStatus: boolean = null
  ) {
    const mainSub = this.ds.directshareEmitter
      // .pipe(filter(val => val.action === 'default'))
      .subscribe(payload => {
        console.log('app-direct-share->directShareEmitter->subscription: ', payload);

        // now conditions
        if (payload.action === 'default') {

          this.contentType = this.ctChecker(payload.asset.contentType);
          console.log('contentType: ', this.contentType);
          this.asset = payload.asset;
          // for audio allow only resizing on x-axis
          // if (payload.asset.contentType.indexOf('audio') !== -1) {
          if (this.contentType === 'audio') {
            this.rzHandles = 'e,w';
          }

          // NOT in classroom
          if (!isInClassroom) {
            // open viewer
            this.showHide = true;
            this.initListener();

          // IN classroom
          } else {
            /**
             * relevant in classroom
             * & for owner
             * pushes direct share events, when generated locally
             */
            // owner
            if (this.ds.isOwner) {
              // open viewer
              this.showHide = true;
              this.initListener();

              this.ds.directshareEmitter.next({ target: 'directShare', event: {
                action: 'open',
                filename: payload.asset.filename,
                contentType: payload.asset.contentType,
                downloadLink: payload.asset.downloadLink }
              });
            // student
            } else {
              // open viewer regarding remote status
              // case one: no remote status, student acts on his own
              if (remoteStatus === null || remoteStatus === false) {
                // open viewer
                this.showHide = true;
                this.initListener();

              // case two: actions are remote controlled 
              } else {
                // open viewer
                this.showHide = true;
                this.initListener();
              }
            }
          }

        }
      });
    this.subscriptionContainer.add(mainSub);
  }

  ngOnDestroy() {
    this.subscriptionContainer.unsubscribe();
  }

  /**
   * listens for scroll, drag, resize events for 
   * 1. helping local functions (e.g. redraw wave after resize)
   * 2. sending these events to the remote, if relevant
   * 
   * Precondition: media viewer component must be initialized(!!)
   */
  initListener() {
    /**
     * TODO: this could be refactored to a merged subscription
     */
    // listener for scoll events
    // timeout needed to make sure that child component has been initialized
    setTimeout(() => {
      console.log('2 nativeElement: ', this.directshareContent.nativeElement);
      const scrollSub = fromEvent(this.directshareContent.nativeElement, 'scroll')
        .pipe(
          throttleTime(100),
        )
        .subscribe((result: any) => {
          console.log(result.target.scrollTop, result.target.scrollLeft);
          this.ds.directshareEmitter.next({ target: 'directShare', event: {
            action: 'scroll',
            scrollLeft: result.target.scrollLeft,
            scrollTop: result.target.scrollTop }
          });
        });
      this.subscriptionContainer.add(scrollSub);
    }, 500);

    // listener for scoll resizing
    const resizeSub = this.rzEmitter
      .pipe(
        throttleTime(100),
      )
      .subscribe((result: any) => {
        // console.log(result.size);
        this.ds.directshareEmitter.next({ target: 'directShare', event: {
          action: 'resize',
          size: result.size }
        });
      });
    this.subscriptionContainer.add(resizeSub);

    // listener for dragging
    const dragSub = this.dragEmitter
      .pipe(
        throttleTime(100),
      )
      .subscribe((result: any) => {
        // console.log(result);
        this.ds.directshareEmitter.next({ target: 'directShare', event: {
          action: 'drag',
          position: result }
        });
      });
    this.subscriptionContainer.add(dragSub);

  }

  ctChecker(contentType) {

    const ctArr = ['image', 'pdf', 'audio', 'video'];
    const ct = ctArr.find(item => contentType.indexOf(item) !== -1);
    return ct === undefined ? 'other' : ct;

  }

  close() {
    this.showHide = false;
    this.ds.directshareEmitter.next({ target: 'directShare', event: { action: 'close' } });
  }

  onStart() {}

  onStop() {}

  doDrag(data) {
    // position of drag window
    this.position = data;
  }

  doResize(data) {
    // console.log(data);
    this.renderer.setStyle(
      this.directshareWindow.nativeElement,
      'width',
      data.width + 'px'
    );
    this.renderer.setStyle(
      this.directshareWindow.nativeElement,
      'height',
      data.height + 'px'
    );
  }

  doScroll(event) {
    console.log(event);
    this.renderer.setProperty(
        this.directshareContent.nativeElement,
        'scrollTop',
        event.scrollTop
    );
    this.renderer.setProperty(
      this.directshareContent.nativeElement,
      'scrollLeft',
      event.scrollLeft
    );
  }

  /**
   * subscribes in case the user is NOT owner of the session for remote events
   * isOwner was determined with the initialization of the service in "room"
   */
  doGetRemoteStatusFirst(
    isInClassroom: boolean
  ) {
    const tempSub = this.ds.getRemoteStatus()
      .subscribe((val: boolean) => {
        this.remoteStatus = val;
        console.log('app-direct-share->doGetRemoteStatusFirst: ', val, this.contentType);
        
        this.initDirectShareDefault(isInClassroom, val);
        // subscribe/unsubscripe regarding val
        this.doSyncRemote(val);
        
        // console.log(this.remoteStatus, this.contentType);
        // if (!val) { this.showHide = false; }

        /*
        this.remoteStatus = val;
        const tmpCt = this.ctChecker(this.contentType);
        if ((tmpCt === 'audio' || tmpCt === 'video') && !val) {
          console.log('do showHide...')
        }
        console.log(this.remoteStatus, tmpCt);
        */
      });
    this.subscriptionContainer.add(tempSub);
  }

  /**
   * subscribes in case the user is NOT owner of the session for remote events
   * isOwner was determined with the initialization of the service in "room"
  */
  doSyncRemote(remoteStatus: boolean) {

    // TODO: subscribe/unsubscripe triggered by remoteStatus
    const localSub: Subscription = new Subscription();

    // remoteStatus true
    if (remoteStatus) {
      // this listens for all remote events

      const remoteStatusSub = this.ds.remoteStatus('directShare')
        .subscribe(payload => {
          this.remoteStatusUI = true;
          setTimeout(() => this.remoteStatusUI = false, 1900);
        });
      localSub.add(remoteStatusSub);

      const syncRemoteSub = this.ds.syncRemote('directShare')
        .subscribe((payload: any) => {
          console.log('syncRemote: ', payload);

          switch (payload.event.action) {
          case 'open':
            // console.log(payload.event.filename);
            this.asset.filename = payload.event.filename;
            this.asset.downloadLink = payload.event.downloadLink;
            this.contentType = this.ctChecker(payload.event.contentType);
            // for audio allow only resizing on x-axis
            if (payload.event.contentType.indexOf('audio') !== -1) {
              this.rzHandles = 'e,w';
            }
            setTimeout(() => this.showHide = true, 500);
            break;
          case 'close':
            this.showHide = false;
            break;
          case 'drag':
            // this.position = payload.event.position;
            this.doDrag(payload.event.position);
            break;
          case 'resize':
            // ...
            this.doResize(payload.event.size);
            break;
          case 'scroll':
            //
            this.doScroll(payload.event);
            break;
          }
          // this.changeDetectorRef.detectChanges();
          // console.log(this.toolbarStatus);
        });
      localSub.add(syncRemoteSub);

      // stop dragging, for remote situation
      const stopdragSub = this.ds.directshareEmitter
        .pipe(
          filter(payload => payload.target === 'directShare' && payload.event.action === 'stopdrag'),
          map(payload => payload.event.event === 'up' ? true : false)
        )
        .subscribe(
          val => {
            this.draggableStatus = val;
          }
        );
      localSub.add(stopdragSub);

    // remoteStatus false
    } else {
      localSub.unsubscribe();
    }

  }

// ### end of class ###
}
