import { Component, OnInit, OnDestroy, AfterViewInit, Input, EventEmitter, Renderer2, ViewChild, ElementRef } from '@angular/core';
import { DirectshareService } from '../../shared/directshare.service';
import { fromEvent, Subscription, Observable } from 'rxjs';
import { throttleTime, filter, take, map, skipWhile } from 'rxjs/operators';
import { WindowObject } from './window-object';
import { DsEvent } from './ds-event';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-direct-share-multi',
  templateUrl: './direct-share-multi.component.html',
  styleUrls: ['./direct-share-multi.component.scss']
})
export class DirectShareMultiComponent implements OnInit, OnDestroy, AfterViewInit {
  @Input() window: WindowObject;
  @Input() index: number;
  @Input() dsSessionId: string;
  @ViewChild('directshareWindow') directshareWindow: ElementRef;
  @ViewChild('directshareContent') directshareContent: ElementRef;
  asset: any;
  position: object = { x: Number, y: Number }; // {x: 50, y: 50}
  size: object = { width: Number, height: Number }; // {x: 50, y: 50}
  draggableStatus: Boolean = true;
  // defines where the window shows handles for resizing
  rzHandles = 'e,se,s';
  doEvents: EventEmitter<any> = new EventEmitter();
  remoteCheckboxLabel: string;

  remoteStatus = false;
  // disableClose = false;

  parentSubContainer: Subscription = new Subscription;
  remoteSubContainer: Subscription = new Subscription;

  url: Observable<any>;

  constructor(
    public ds: DirectshareService,
    private renderer: Renderer2,
    private el: ElementRef,
    private ar: ActivatedRoute,
  ) {
    // ...
    // checks async whether first path segment is room
    this.url = this.ar.url
      .pipe(
        map(segments => segments[0].path, take(1)),
        map(val => val === 'room' ? true : false)
        // for debuggin in media room
        // map(val => val === 'media' || val === 'room' ? true : false)
      );

    // console.log('app-direct-share-multi: ', this.dsSessionId);
  }

  foo() {
    // dummy
  }

  ngOnInit() {
    // console.log(this.window, this.index);
    const shiftFactor = 20 * this.index;
    // this.asset = this.window.asset;
    this.position = { x: shiftFactor, y: shiftFactor };

    // change handles for audio viewer
    if (this.window.asset.contentType === 'audio') {
      this.rzHandles = 'e,w';
    }

    // for parent AND child DSWindows
    this.initWindowEventListener();

    // set remoteCheckboxLabel in header
    // initialization case 1: parent window, newly created
    if (this.window.isParent !== false && this.window.isRemoteActive === false) {
      this.remoteCheckboxLabel = 'Start Remote'; 

    // initialization case 2: remote window, newly per remote created
    } else {
      this.remoteCheckboxLabel = 'Remote ON';
      console.log('initRemoteListener...');
      // TODO: apply classroom sessionId;
      this.initRemoteListener(this.dsSessionId);

    }

  }

  ngAfterViewInit() {
    //
  }

  ngOnDestroy() {
    this.parentSubContainer.unsubscribe();
    this.remoteSubContainer.unsubscribe();
  }

  /**
   * 
   * @param event handling the remoteToggle/checkbox and labels in DSWindow header
   */
  toggleRemoteStatus(event?) {
    // ...
    //toggle status
    this.window.isRemoteActive = !this.window.isRemoteActive;
    event = this.window.isRemoteActive;

    // case for initializing remote window
    if (event && this.window.isParent === null) {
      this.window.isParent = true;
      // tslint:disable-next-line:max-line-length
      this.ds.dsEvent.next({ windowId: this.window.id, isParent: this.window.isParent, target: 'remote', event: { action: event, value: this.window.asset } });
      // this.remoteCheckboxLabel = 'Parent ON';
      this.remoteCheckboxLabel = 'Remote ON';
      

    // case of switching ON the remote connection from "Remote" side 
    } else if (event && this.window.isParent === false) {
      this.remoteCheckboxLabel = 'Remote ON';
      console.log('switch Remote ON...');

      // case of switching OFF the remote connection from "Parent" side
    } else if (event === false && this.window.isParent === true) {
      this.window.isParent = null; // not null
      this.ds.dsEvent.next({ windowId: this.window.id, isParent: this.window.isParent, target: 'remote', event: { action: event } });
      // this.remoteCheckboxLabel = 'Parent Off';
      this.remoteCheckboxLabel = 'Remote OFF';
      // this.parentSubContainer.unsubscribe();
      // console.log('switch Remote OFF...');

      // case of switching OFF the remote connection from "Remote" side
    } else if (event === false && this.window.isParent === false) {
      this.ds.dsEvent.next({ windowId: this.window.id, isParent: this.window.isParent, target: 'remote', event: { action: event } });
      this.remoteCheckboxLabel = 'Remote Off';
      // this.remoteSubContainer.unsubscribe();

    }

    // submit remote event for remoteStatus
    // tslint:disable-next-line:max-line-length
    // setTimeout(() => this.ds.dsEvent.next({ window: this.window.id, target: 'remote', value: { status: event, asset: this.window.asset } }), 10);
    // this.ds.dsEvent.next({ window: this.window.id, target: 'remote', value: { status: event, asset: this.window.asset } })
  }

  // simply delete window obj from array
  close() {
    // send remote event
    this.ds.dsEvent.next({
      windowId: this.window.id,
      isParent: this.window.isParent,
      target: 'window',
      event: {
        action: 'close'
      }
    });

    // closes window locally
    const index = this.ds.windowsArr.findIndex(item => item.id === this.window.id);
    this.ds.windowsArr.splice(index, 1);
  }

  //
  // image, pdf, document/text, application, video
  contentType(ct) {
    // console.log('#####', ct)
    const s = ct.split('/');
    if (ct === 'application/pdf') {
      s[0] = 'pdf';
    } else if (ct === 'document/text') {
      s[0] = 'text';
    }
    return s;
  }

   /**
   * listens for scroll, drag, resize events for 
   * 1. helping local functions (e.g. redraw wave after resize)
   * 2. sending these events to the remote, if relevant
   * 
   * Precondition: media viewer component must be initialized(!!)
   */
  initWindowEventListener() {
    /**
     * listener for windows events
     * and emitting this to 
     * local AND remote subscribers
     */
    // console.log('initWindowEventListener...');
    const doEvents = this.doEvents
      .pipe(
        throttleTime(200),
      );
    this.parentSubContainer = doEvents
      .subscribe((result: DsEvent['event']) => {
        // console.log('WindowEventListener events: ', result);
        // if remoteActive is toggled false, no events are send and vice versa
        if (this.window.isRemoteActive) {
          this.ds.dsEvent.next(
            {
              windowId: this.window.id,
              isParent: this.window.isParent,
              target: 'window',
              event: result
            }
          );    
        }
      });

    /**
     * relevant for handling the resize events to the internal subscriber
     * This was easier to handle, decoupled from the remote events
     */
    const internalEvents = this.doEvents
      .pipe(
        throttleTime(200),
        filter(val => val.action === 'resize'),
      );
    internalEvents.subscribe(val => {
      // console.log(val);
      this.window.height = val.value.height;
      this.window.width = val.value.width;
      // console.log('window: ', this.window);
    })

    /**
     * listen for scroll events
     * timeout is needed to attach the listener
     * TODO: why?
     */
    setTimeout(() => {
      // console.log('2 nativeElement: ', this.directshareContent.nativeElement);
      const scrollSub = fromEvent(this.directshareContent.nativeElement, 'scroll')
        .pipe(
          throttleTime(100)
        )
        .subscribe((result: any) => {
          // console.log(result.target.scrollTop, result.target.scrollLeft);
          if (this.window.isRemoteActive) {
            this.ds.dsEvent.next(
              {
                windowId: this.window.id,
                isParent: this.window.isParent,
                target: 'window',
                event: {
                  action: 'scroll',
                  value: {
                    scrollLeft: result.target.scrollLeft,
                    scrollTop: result.target.scrollTop
                  }
                }
              }
            );
          }
        });
      this.parentSubContainer.add(scrollSub);
    }, 500);

  }

  // relevant for child windows with "isRemoteActive = true"
  initRemoteListener(remoteSessionId) {
    // used to provide to helper functions
    // such as "setRemoteStatus"
    const tmpL = this.ds.remoteDSWindowsListener(remoteSessionId, this.window.id)
      .pipe(
        // only listen for "window" events
        filter(val => val.target === 'window'),
        filter(() => this.window.isRemoteActive)
      )
      .subscribe(val => {
        // console.log('remoteDSWindowsListener->', val);
        switch (val.event.action) {
          case 'resize': this.doResize(val.event.value);
          break;
          case 'drag': this.doDrag(val.event.value);
          break;
          case 'scroll': this.doScroll(val.event.value);
          break;
          case 'close': this.close();
          break;
        }
      });
    this.remoteSubContainer.add(tmpL);
  }

  doDrag(data) {
    // position of drag window
    this.position = data;
  }

  doResize(data) {
    console.log('doResize: ', data);
    this.renderer.setStyle(
      this.directshareWindow.nativeElement,
      'width',
      data.width + 'px'
    );
    this.renderer.setStyle(
      this.directshareWindow.nativeElement,
      'height',
      data.height + 'px'
    );
    // update window object
    this.window.width = data.width;
    this.window.height = data.height;
  }

  doScroll(event) {
    console.log(event);
    this.renderer.setProperty(
        this.directshareContent.nativeElement,
        'scrollTop',
        event.scrollTop
    );
    this.renderer.setProperty(
      this.directshareContent.nativeElement,
      'scrollLeft',
      event.scrollLeft
    );
  }

// end of class
}
