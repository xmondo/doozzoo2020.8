/**
 * /**
 * helper for compose event
    composeEvent(value: DsEvent['event']) {
        console.log('composeEvent: ', value)
        this.ds.dsEvent.next({
            senderId: 'foo';
            windowId: this.window.id,
            isParent: this.window.isParent,
            target: 'viewer',
            event: value // example: { action: 'pause', value. 'foo' }
        });
    }
*/

export class DsEvent {
    // useful for multicasting
    senderId?: string;
    // for apps, no uid but the app name.
    // as we will never have multipe metronomes
    windowId?: string; 
    isParent?: boolean;
    // defines scope, useful for filtering
    // e.g. window | remote | viewer | app
    target: string;
    // here the target specific events are located
    event?: {
        action: string;
        value?: any;
    };
}
