export class WindowObject {
    id: string;
    isRemoteActive: boolean;
    isParent: boolean; // null | true | false, null = initial value
    isInFocus: boolean;
    asset: any;
    width: number;
    height: number;
}
