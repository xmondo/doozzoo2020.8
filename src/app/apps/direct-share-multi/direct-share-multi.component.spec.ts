import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectShareMultiComponent } from './direct-share-multi.component';

describe('DirectShareMultiComponent', () => {
  let component: DirectShareMultiComponent;
  let fixture: ComponentFixture<DirectShareMultiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectShareMultiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectShareMultiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
