// ...
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

// shared modules
import { MaterialSharedModule } from './shared/material-shared/material-shared.module';
import { BasicsSharedModule } from './shared/basics-shared/basics-shared.module';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule, HttpClient } from '@angular/common/http';
// flex/grid layout
import { FlexLayoutModule } from '@angular/flex-layout';
import { RoutingModule } from './routing/routing.module';
import { environment } from '../environments/environment';

import { FormsModule, ReactiveFormsModule } from '@angular/forms'; // import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// internationization
import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
registerLocaleData(localeDe, 'de');

// localForage -> indexDB local storage
// import { NgForageModule, NgForageConfig, Driver } from 'ngforage';
// import { DEFAULT_CONFIG, NgForageOptions, NgForageConfig, Driver } from 'ngforage';
// pdf viewer
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { ControllsService } from './shared/controlls.service';

// firebase
import {
  AngularFireModule,
  FirebaseOptionsToken,
  // FirebaseAppNameToken,
  // FirebaseAppConfigToken
} from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireStorageModule, StorageBucket } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
// import { AngularFireAuth } from '@angular/fire/auth';
// import { environment } from './config';
// import { DoAuthService } from './doauth.service';
import { AngularFirestoreModule } from '@angular/fire/firestore';

// Material
import { ClipboardModule } from '@angular/cdk/clipboard';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
// import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSliderModule } from '@angular/material/slider';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatChipsModule } from '@angular/material/chips';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatTableModule } from '@angular/material/table';
import { DragDropModule } from '@angular/cdk/drag-drop'; 
import { MatStepperModule } from '@angular/material/stepper';
import { MatListModule } from '@angular/material/list';

// import { ClipboardModule } from '@angular/cdk/clipboard'

// dragg / resize
import { AngularDraggableModule } from 'angular2-draggable';
import { ScrollingModule } from '@angular/cdk/scrolling';

// device detection
// import { DeviceDetectorModule } from 'ngx-device-detector';

// pipes
import { SecToMinPipe } from './shared/secToMinPipe'; // import our pipe here
import { Filesize } from './shared/filesizePipe'; // import our pipe here

// services
import { OpentokService } from './shared/opentok.service';
import { AudioService } from './shared/audio.service';
import { AuthService } from './shared/auth.service';
import { DirectshareService } from './shared/directshare.service';

// components
import { AppComponent } from './app.component';
import { PublisherComponent } from './publisher/publisher.component';
import { SubscriberComponent } from './subscriber/subscriber.component';
import { ScreensharerComponent } from './screensharer/screensharer.component';
import { HomeComponent } from './home/home.component';
import { RoomComponent } from './room/room.component';

import { SetConstraintsComponent } from './preflight/set-constraints/set-constraints.component';
import { MetronomeComponent } from './apps/metronome/metronome.component';

import { MixerComponent } from './apps/mixer/mixer.component';
import { ToolbarComponent } from './apps/toolbar/toolbar.component';
import { ChannelstripComponent } from './apps/mixer/channelstrip/channelstrip.component';
import { LevelmeterComponent } from './preflight/levelmeter/levelmeter.component';
import { PreflightComponent } from './preflight/preflight.component';
import { TangoButtonComponent } from './shared/tango-button/tango-button.component';
import { DelayComponent } from './preflight/delay/delay.component';
import { RecorderComponent } from './apps/recorder/recorder.component';
import { DialogConfirmComponent } from './apps/media/dialog-confirm/dialog-confirm.component';
import { DialogRecorderComponent } from './apps/recorder/dialog/dialog.component';
import { ConfirmComponent } from './apps/recorder/confirm/confirm.component';
import { VideoTrimmerComponent } from './apps/recorder/video-trimmer/video-trimmer.component';
import { AudioTrimmerComponent } from './apps/recorder/audio-trimmer/audio-trimmer.component';
import { TunerComponent } from './apps/tuner/tuner.component';
import { UploadItemComponent } from './apps/media/upload-item/upload-item.component';
import { DialogShareComponent } from './apps/media/dialog-share/dialog-share.component';
import { UserNameComponent } from './shared/user-name/user-name.component';
import { CheckboxShareComponent } from './apps/media/checkbox-share/checkbox-share.component';
import { DirectShareComponent } from './apps/direct-share/direct-share.component';
import { TestToneComponent } from './apps/test/test.component';
import { TestComponent } from './test/test.component';
import { Test2Component } from './apps/mixer/test/test.component';
import { ImageComponent } from './apps/direct-share/image/image.component';
import { PdfComponent } from './apps/direct-share/pdf/pdf.component';
import { AudioComponent } from './apps/direct-share/audio/audio.component';
import { VideoComponent } from './apps/direct-share/video/video.component';
import { ControllsComponent } from './screensharer/controlls/controlls.component';
import { LooperComponent } from './apps/looper/looper.component';
import { LoginComponent } from './login/login.component';
import { LoginDialogComponent } from './login/login-dialog/login-dialog.component';
import { LoginButtonComponent } from './login/login-button/login-button.component';
import { EmailHandlerComponent } from './login/email-handler/email-handler.component';
import { ProfileComponent } from './profile/profile.component';
import { CustomerComponent } from './profile/customer/customer.component';
import { CoachComponent } from './profile/coach/coach.component';
import { MyCoachesComponent } from './my-coaches/my-coaches.component';
import { MyStudentsComponent } from './my-students/my-students.component';
import { UserContentRowComponent } from './profile/user-content-row/user-content-row.component';
import { MyCoachesButtonComponent } from './my-coaches/my-coaches-button/my-coaches-button.component';
import { FooterComponent } from './shared/footer/footer.component';
import { ConfirmRemoveComponent } from './my-students/confirm-remove/confirm-remove.component';
import { InviteStudentComponent } from './my-students/invite-student/invite-student.component';
import { InviteComponent } from './invite/invite.component';
import { AvatarComponent } from './shared/avatar/avatar.component';
import { StudentlistComponent } from './my-students/studentlist/studentlist.component';
import { ParticipantListComponent } from './my-students/participant-list/participant-list.component';
import { MediaPageComponent } from './apps/media/media-page/media-page.component';
import { FilterDocsPipe } from './shared/filter-docs.pipe';
import { StripeComponent } from './stripe/stripe.component';
import { CreditcardComponent } from './stripe/creditcard/creditcard.component';
import { ClipboardButtonComponent } from './room/clipboard-button/clipboard-button.component';
import { MediaButtonComponent } from './apps/media/media-button/media-button.component';
import { InfoNavigationComponent } from './home/info-navigation/info-navigation.component';
import { SharingStatusComponent } from './apps/media/sharing-status/sharing-status.component';
import { CookieHintComponent } from './shared/cookie-hint/cookie-hint.component';
import { CookieTemplateComponent } from './shared/cookie-hint/cookie-template/cookie-template.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { AudioModeComponent } from './room/audio-mode/audio-mode.component';
import { DirectShareMultiComponent } from './apps/direct-share-multi/direct-share-multi.component';
import { DirectShareWindowsComponent } from './apps/direct-share-windows/direct-share-windows.component';
import { IconBarComponent } from './room/icon-bar/icon-bar.component';
import { ListUsersComponent } from './admin/list-users/list-users.component';
import { DetailUsersComponent } from './admin/detail-users/detail-users.component';
import { AchivingComponent } from './room/achiving/achiving.component';
import { VoucherComponent } from './profile/voucher/voucher.component';
import { WidgetComponent } from './home/widget/widget.component';
import { WidgetAdminComponent } from 'src/app/admin/widget-admin/widget-admin.component';
import { FilterUserPipe } from './shared/filter-user.pipe';
import { TestdriveComponent } from './testdrive/testdrive.component';
import { ErrorsComponent, DialogErrors } from './room/errors/errors.component';
import { ChatComponent } from './room/chat/chat.component';
import { UrlifyPipe } from './shared/urlify.pipe';
import { EduHomeComponent } from './edu/edu-home/edu-home.component';
import { EduCoachesComponent } from './edu/edu-coaches/edu-coaches.component';
import { ConfirmRemoveCoachComponent } from './edu/edu-coaches/confirm-remove/confirm-remove.component';
import { EduProfileComponent } from './edu/edu-profile/edu-profile.component';
import { InstitutionProfileComponent } from './edu/institution-profile/institution-profile.component';
import { EduCoachesDetailComponent } from './edu/edu-coaches-detail/edu-coaches-detail.component';
import { CreateAccountsComponent } from './edu/create-accounts/create-accounts.component';
import { RoomSettingsComponent } from './room/room-settings/room-settings.component';
import { SetBitrateComponent } from './preflight/set-bitrate/set-bitrate.component';
import { ThumbsToolbarComponent } from './room/thumbs-toolbar/thumbs-toolbar.component';
import { ListAccountsComponent } from './edu/list-accounts/list-accounts.component';
import { FilterAccountsPipe } from './shared/filter-accounts.pipe';
import { PeersComponent } from './profile/peers/peers.component';
import { StatisticsComponent } from './admin/statistics/statistics.component';
import { LockButtonComponent } from './room/lock-button/lock-button.component';
import { WaitingRoomComponent } from './waiting-room/waiting-room.component';
import { RoomAccessComponent } from './my-students/room-access/room-access.component';
import { DisplayAvatarComponent } from './profile/display-avatar/display-avatar.component';
import { ArchivedMinutesComponent } from './apps/media/archived-minutes/archived-minutes.component';
import { BookArchivingComponent } from './stripe/book-archiving/book-archiving.component';
import { BookArchivingConfirmComponent } from './stripe/book-archiving-confirm/book-archiving-confirm.component';
import { ArchivesAvailableComponent } from './apps/media/archives-available/archives-available.component';
import { LoginDialogStandaloneComponent } from './login/login-dialog-standalone/login-dialog-standalone.component';
import { ArchivingLinkComponent } from './apps/media/archiving-link/archiving-link.component';
import { LanguageComponent } from './shared/language/language.component';
import { SepaComponent } from './stripe/sepa/sepa.component';

// localization -> @ngx-translate
// import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
// import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AnonymousWaitComponent } from './waiting-room/anonymous-wait/anonymous-wait.component';
import { RegisteredWaitComponent } from './waiting-room/registered-wait/registered-wait.component';
import { ContenttypeAllowedCheckPipe } from './apps/media/contenttype-allowed-check.pipe';
import { PackageHintComponent } from './my-students/package-hint/package-hint.component';
import { WaitingParticipantsComponent } from './room/waiting-participants/waiting-participants.component';
import { SortByPipe } from './shared/sort-by.pipe';
import { UserNamePipe } from './shared/user-name.pipe';
import { GetEduUserRolePipe } from './edu/get-edu-user-role.pipe';
import { ActivityMeterComponent } from './preflight/activity-meter/activity-meter.component';
import { MatRippleModule } from '@angular/material/core';
import { BetatesterHintComponent } from './shared/betatester-hint/betatester-hint.component';
import { KeyboardComponent } from './apps/keyboard/keyboard.component';
import { MediamodeComponent } from './preflight/mediamode/mediamode.component';
import { CookieConsentComponent } from './shared/cookie-consent/cookie-consent.component';
import { CookieConsentDialogComponent } from './shared/cookie-consent/cookie-consent-dialog/cookie-consent-dialog.component';
import { CookieConsentServiceService } from './shared/cookie-consent/cookie-consent-service.service';

import { MediaListComponent } from './apps/media/media-list/media-list.component';
import { ContentTypePipe } from './apps/media/content-type.pipe';
import { IsNewPipe } from './apps/media/is-new.pipe';
import { UploadAreaComponent } from './apps/media/upload-area/upload-area.component';
import { FilterContactsPipe } from './shared/filter-contacts.pipe';
import { GetTagLabelPipe } from './apps/media/get-tag-label.pipe';
import { CheckboxShareSingleComponent } from './apps/media/checkbox-share-single/checkbox-share-single.component';
import { CheckboxShareBulkComponent } from './apps/media/checkbox-share-bulk/checkbox-share-bulk.component';
import { AnonymousHintComponent } from './shared/anonymous-hint/anonymous-hint.component';
import { AppPositionPipe } from './apps/toolbar/app-position.pipe';
import { ToolbarPageComponent } from './apps/toolbar/toolbar-page/toolbar-page.component';
import { ActivateAppsComponent } from './apps/toolbar/activate-apps/activate-apps.component';
import { ParticipantsActivePipe } from './apps/toolbar/activate-apps/participants-active.pipe';
import { RemoteRingsComponent } from './shared/remote-rings/remote-rings.component';
import { MyroomButtonComponent } from './myrooms/myroom-button/myroom-button.component';
import { MyroomsListComponent } from './myrooms/myrooms-list/myrooms-list.component';
import { MyroomsEditComponent } from './myrooms/myrooms-edit/myrooms-edit.component';
import { DialogMyroomsEditComponent } from './myrooms/dialog-myrooms-edit/dialog-myrooms-edit.component';
import { PlansComponent } from './profile/plans/plans.component';
import { VerifyEmailComponent } from './login/verify-email/verify-email.component';
import { DirectloginComponent } from './login/directlogin/directlogin.component';
import { SessionTimeComponent } from './room/session-time/session-time.component';
import { EditRoleComponent } from './edu/create-accounts/edit-role/edit-role.component';
import { ToggleAudioModeComponent } from './room/room-settings/toggle-audio-mode/toggle-audio-mode.component';
import { ButtonIframeComponent, DialogIframeComponent } from './shared/button-iframe/button-iframe.component';
import { ArchivesFallbackComponent, ButtonArchivesFallbackComponent } from './apps/media/archives-fallback/archives-fallback.component';
import { AudioInListComponent } from './room/room-settings/audio-in-list/audio-in-list.component';
import { VideoInListComponent } from './room/room-settings/video-in-list/video-in-list.component';
import { WorkshopRoomsComponent } from './admin/workshop-rooms/workshop-rooms.component';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

// AoT requires an exported function for factories

export function translateHttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}


@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    // material
    MatRippleModule,
    ClipboardModule,
    MatTooltipModule,
    // MatIconModule,
    MatCardModule,
    MatCheckboxModule,
    MatRadioModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatSnackBarModule,
    MatSidenavModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatInputModule,
    MatDialogModule,
    MatMenuModule,
    MatDividerModule,
    MatBadgeModule,
    MatTabsModule,
    MatExpansionModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatTableModule,
    // MatDialogConfig,
    DragDropModule,
    MatStepperModule,
    MatListModule,

    // shared
    MaterialSharedModule,
    BasicsSharedModule,

    // drag'n'drop
    // DragDropModule, // material native
    AngularDraggableModule,
    // scrolling
    ScrollingModule,

    // firebase
    // TODO: check aold setup
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    // AngularFireAuth,
    // AngularFireAuthGuardModule,

    RoutingModule,
    // DeviceDetectorModule,

    // we still have to import it for ROOT, even if we have it in sharedModule
    DeviceDetectorModule.forRoot(),
    // Optional in Angular 6 and up
    // NgForageModule.forRoot()
    PdfViewerModule,
    HttpClientModule,

    // 3rd party pagination
    NgxPaginationModule,

    // // localization -> @ngx-translate
     // we still have to import it for ROOT, even if we have it in sharedModule
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: translateHttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    
  ],
  declarations: [
    AppComponent,
    PublisherComponent,
    SubscriberComponent,
    ScreensharerComponent,
    HomeComponent,
    RoomComponent,
    SecToMinPipe,
    Filesize,
    SetConstraintsComponent,
    MetronomeComponent,
    MixerComponent,
    ToolbarComponent,
    ChannelstripComponent,
    LevelmeterComponent,
    PreflightComponent,
    TangoButtonComponent,
    DelayComponent,
    RecorderComponent,
    // dialogs
    DialogConfirmComponent,
    DialogRecorderComponent,
    ConfirmComponent,
    DialogShareComponent,
    // ...
    VideoTrimmerComponent,
    AudioTrimmerComponent,
    TunerComponent,
    UploadItemComponent,
    UserNameComponent,
    CheckboxShareComponent,
    DirectShareComponent,
    TestToneComponent,
    TestComponent,
    Test2Component,
    ImageComponent,
    PdfComponent,
    AudioComponent,
    VideoComponent,
    ControllsComponent,
    LooperComponent,
    LoginComponent,
    LoginDialogComponent,
    LoginButtonComponent,
    EmailHandlerComponent,
    ProfileComponent,
    CustomerComponent,
    CoachComponent,
    MyCoachesComponent,
    MyStudentsComponent,
    UserContentRowComponent,
    MyCoachesButtonComponent,
    FooterComponent,
    ConfirmRemoveComponent,
    ConfirmRemoveCoachComponent,
    InviteStudentComponent,
    InviteComponent,
    AvatarComponent,
    StudentlistComponent,
    ParticipantListComponent,
    MediaPageComponent,
    FilterDocsPipe,
    StripeComponent,
    CreditcardComponent,
    ClipboardButtonComponent,
    MediaButtonComponent,
    InfoNavigationComponent,
    SharingStatusComponent,
    CookieHintComponent,
    CookieTemplateComponent,
    AudioModeComponent,
    DirectShareMultiComponent,
    DirectShareWindowsComponent,
    IconBarComponent,
    ListUsersComponent,
    DetailUsersComponent,
    AchivingComponent,
    VoucherComponent,
    WidgetComponent,
    WidgetAdminComponent,
    FilterUserPipe,
    TestdriveComponent,
    ErrorsComponent,
    DialogErrors,
    ChatComponent,
    UrlifyPipe,
    EduHomeComponent,
    EduCoachesComponent,
    EduProfileComponent,
    InstitutionProfileComponent,
    EduCoachesDetailComponent,
    CreateAccountsComponent,
    RoomSettingsComponent,
    SetBitrateComponent,
    ThumbsToolbarComponent,
    ListAccountsComponent,
    FilterAccountsPipe,
    PeersComponent,
    StatisticsComponent,
    LockButtonComponent,
    WaitingRoomComponent,
    RoomAccessComponent,
    DisplayAvatarComponent,
    ArchivedMinutesComponent,
    BookArchivingComponent,
    BookArchivingConfirmComponent,
    ArchivesAvailableComponent,
    LoginDialogStandaloneComponent,
    ArchivingLinkComponent,
    LanguageComponent,
    SepaComponent,
    AnonymousWaitComponent,
    RegisteredWaitComponent,
    ContenttypeAllowedCheckPipe,
    PackageHintComponent,
    WaitingParticipantsComponent,
    SortByPipe,
    UserNamePipe,
    GetEduUserRolePipe,
    ActivityMeterComponent,
    BetatesterHintComponent,
    KeyboardComponent,
    MediamodeComponent,
    CookieConsentComponent,
    CookieConsentDialogComponent,
    MediaListComponent,
    ContentTypePipe,
    IsNewPipe,
    UploadAreaComponent,
    FilterContactsPipe,
    GetTagLabelPipe,
    CheckboxShareSingleComponent,
    CheckboxShareBulkComponent,
    AnonymousHintComponent,
    AppPositionPipe,
    ToolbarPageComponent,
    ActivateAppsComponent,
    ParticipantsActivePipe,
    RemoteRingsComponent,
    MyroomButtonComponent,
    MyroomsListComponent,
    MyroomsEditComponent,
    DialogMyroomsEditComponent,
    PlansComponent,
    VerifyEmailComponent,
    DirectloginComponent,
    SessionTimeComponent,
    EditRoleComponent,
    ToggleAudioModeComponent,
    ButtonIframeComponent,
    DialogIframeComponent,
    ArchivesFallbackComponent,
    ButtonArchivesFallbackComponent,
    AudioInListComponent,
    VideoInListComponent,
    WorkshopRoomsComponent,
  ],
  entryComponents: [
    DialogConfirmComponent,
    DialogRecorderComponent,
    ConfirmComponent,
    DialogShareComponent,
    LoginDialogComponent,
    ConfirmRemoveComponent,
    ConfirmRemoveCoachComponent,
    CookieHintComponent,
    CookieTemplateComponent,
    DialogErrors,
    BookArchivingConfirmComponent,
  ],
  providers: [
    CookieConsentServiceService,
    OpentokService,
    AudioService,
    { provide: FirebaseOptionsToken, useValue: environment.firebase },
    { provide: StorageBucket, useValue: environment.firebase.storageBucket },
    AuthService,
    // DoAuthService,
    // AngularFireAuth,
    // AngularFireAuthModule,
    DirectshareService,
    ControllsService,
    // { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: true } }
    // Tone
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  bootstrap: [AppComponent]
})
export class AppModule {
  /*
  public constructor(ngfConfig: NgForageConfig) {
    ngfConfig.configure({
      name: 'mediaRecorder',
      driver: [ // defaults to indexedDB -> webSQL -> localStorage
        Driver.INDEXED_DB,
        Driver.LOCAL_STORAGE
      ]
    });
  }
  */
}

