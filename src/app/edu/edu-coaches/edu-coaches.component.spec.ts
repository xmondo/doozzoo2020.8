import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EduCoachesComponent } from './edu-coaches.component';

describe('EduCoachesComponent', () => {
  let component: EduCoachesComponent;
  let fixture: ComponentFixture<EduCoachesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EduCoachesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EduCoachesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
