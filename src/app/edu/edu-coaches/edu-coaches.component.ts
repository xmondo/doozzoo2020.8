import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { AuthService } from 'src/app/shared/auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { map, distinctUntilChanged } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmRemoveCoachComponent } from './confirm-remove/confirm-remove.component';
import { isNgTemplate } from '@angular/compiler';

interface AccountData {
  email:string;
  role: number; // 0,1,2,3
  status:string; // created | inviteTriggered | inviteSent | inviteAccepted
  activated:boolean;
  allowInvites:boolean;
  createdOn:number;
  coachId?:string;
  key?:string;
}

@Component({
  selector: 'app-edu-coaches',
  templateUrl: './edu-coaches.component.html',
  styleUrls: ['./edu-coaches.component.scss']
})
export class EduCoachesComponent implements OnInit {
  institutionId: string;
  coachesListRef: any;
  coachesList: any;
  invitesListRef: any;
  coachEmailValue: string;
  coachEmailValue2: string;
  institutionData: Observable<any>;
  progressBar: Observable<boolean>;
  accountRole: string;

  coachFormEmail = new FormControl('email', [ Validators.required, Validators.email ]);

  constructor(
    private aroute: ActivatedRoute,
    public ats: AuthService,
    private afs: AngularFirestore,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.institutionId = this.aroute.snapshot.paramMap.get('institutionId');
    this.getCoaches();
  }

  async getCoaches() {

    // this.progressBar = true;
    this.institutionData = this.afs
      .doc('institutions/' + this.institutionId)
      .valueChanges()

    this.coachesListRef = this.afs
      .doc('institutions/' + this.institutionId)
      .collection('coaches');

    this.coachesList = this.coachesListRef
      .valueChanges({idField: 'key'})
      .pipe(
        distinctUntilChanged(),
      )
    this.invitesListRef = this.afs
      .collection('invites');

  }

  create(){
      const data: AccountData = {
        email: this.coachEmailValue,
        role: parseInt(this.accountRole),
        status: 'created',
        activated: false,
        allowInvites: false,
        createdOn: Date.now(),
      }
      /*
      // Persist a document id
      const id = this.afs.createId();
      const item: Item = { id, name };
      this.itemsCollection.doc(id).set(item);
      */

      this.coachesListRef.add(data);
  }

  update(item: any){
    console.log('update', item);
    this.coachesListRef.doc(item.key).update(item);
  }

  toggleAccess(item, event) {
    // console.log('update', item);
    item.activated = event.checked;
    this.coachesListRef.doc(item.key).update(item);
  }

  toggleAllowInvites(item, event) {
    // console.log('update', item);
    item.allowInvites = event.checked;
    this.coachesListRef.doc(item.key).update(item);
  } 

  sendInvite(item) {
    item.status = 'inviteSentPrepare';
    this.coachesListRef.doc(item.key).update(item);
    this.createInviteLink(item);
  }

  delete(item) {
    this.coachesListRef.doc(item.key).delete();
  }

  createInviteLink(item:any) {
    // ...
    // this.institutionId
    const data = {
      type: 'institution',
      initiatorId: this.institutionId,
      initiatorName: this.institutionId,
      triggerId: item.key,
      target: {
        type: 'email',
        value: item.email,
        password: '#doozzoo',
      },
      createdOn: Date.now(),
      coachId: 'unset',
      role: this.accountRole,
    }
    this.invitesListRef
      .add(data);
  }

  confirmRemove(item): void {
    console.log(item);
    const dialogRef = this.dialog.open(ConfirmRemoveCoachComponent, {
      width: '350px',
      data: { clipTitle: item.title }
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        console.log('The dialog was closed', result);
        // result is 'true' or undefined
        if (result === true) {
          // trigger delete
          this.delete(item);
        }
      });
  }

  buttonLabel(status){
    switch (status) {
      case 'created': return 'send invite';
      break;
      case 'inviteSentPrepare': return 'in progress';
      break;
      case 'inviteSent': return 'invite sent';
      break;
      case 'inviteAccepted': return 'invite accepted';
      break;
      case 'error': return 'error';
      break;
      default: return 'send invite';
    }
  }

  icon(status) {
    if(status === "inviteSent" || status === "inviteAccepted") {
      return 'done';
    } else if (status === 'inviteSentPrepare' || status === 'created') {
      return 'send invite'
    } else if (status === 'error') {
      return 'warning';
    }
  }
  
// end of class
}
