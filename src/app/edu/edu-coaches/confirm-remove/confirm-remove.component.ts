import {Component, Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogData {
  clipTitle: string,
  messageId: string, //'create-accounts.delete',
}

@Component({
  selector: 'app-confirm-remove',
  templateUrl: './confirm-remove.component.html',
  styleUrls: ['./confirm-remove.component.scss']
})
export class ConfirmRemoveCoachComponent {

  constructor(
    public dialogRef: MatDialogRef<ConfirmRemoveCoachComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
