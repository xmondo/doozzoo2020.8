import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmRemoveCoachComponent } from './confirm-remove.component';

describe('ConfirmRemoveCoachComponent', () => {
  let component: ConfirmRemoveCoachComponent;
  let fixture: ComponentFixture<ConfirmRemoveCoachComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmRemoveCoachComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmRemoveCoachComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
