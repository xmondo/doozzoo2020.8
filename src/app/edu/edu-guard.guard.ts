import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../shared/auth.service';

@Injectable({
  providedIn: 'root'
})
export class EduGuardGuard implements CanActivate {

  role: number = -2;
  constructor(
    private ats: AuthService,
    private router: Router,
  ){
    this.ats.role$
      .subscribe(val => {
        // console.log('EduGuardGuard role$: ', val);
        this.role = val
      });
  }
  
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    // console.log('AuthGuard#canActivate called');
    // return true;
    let url: string = state.url;
    return this.checkLogin(url);
  }

  checkLogin(url: string): boolean {
    // console.log(this.role);
    if (this.role === 3) { 
      return true; 
    }
    // Navigate to the login page with extras
    this.router.navigate(['/home']);
    return false;
  }

}


