import { Component, OnInit, Input } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/shared/auth.service';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-list-accounts',
  templateUrl: './list-accounts.component.html',
  styleUrls: ['./list-accounts.component.scss']
})
export class ListAccountsComponent implements OnInit {
  @Input() mode: string; // institution | institutionCoach //?? | coach 
  @Input() institutionId: string; // id of institution

  accountsListRef: any;
  accountsList: Observable<any>;
  filterargs: any;
  inputType: any;
  itemsPerPageValue: number = 10;
  
  p: any;

  constructor(
    private aroute: ActivatedRoute,
    public ats: AuthService,
    private afs: AngularFirestore,
    public dialog: MatDialog,
  ) { 
    // ...
  }

  ngOnInit() {
    this.mode = 'all';
    this.getAccountsInstitution();
  }

  async getAccountsInstitution() {

    console.log('ListAccountsComponent->mode: ', this.mode);

    if (this.mode === 'institutionStudent') {
      this.accountsListRef = this.afs
      .doc('institutions/' + this.institutionId)
      .collection('accounts', ref => ref.where('role', '==', 0)); // shows only coaches on Institution overview
    } else if (this.mode === 'all') {
      this.accountsListRef = this.afs
      .doc('institutions/' + this.institutionId)
      .collection('accounts'); // shows all
    } 

    this.accountsList = this.accountsListRef
      .valueChanges({idField: 'key'})
      .pipe(
        // distinctUntilChanged(),
      );

  }

}
