import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AngularFireObject, AngularFireDatabase } from '@angular/fire/database';
import { AuthService } from 'src/app/shared/auth.service';
import { Router } from '@angular/router';
import { filter, distinctUntilChanged } from 'rxjs/operators';
import { Avatar } from '../../shared/avatar/avatar';
import { ControllsService } from 'src/app/shared/controlls.service';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-edu-profile',
  templateUrl: './edu-profile.component.html',
  styleUrls: ['./edu-profile.component.scss'],
  providers: [ Avatar ],
})
export class EduProfileComponent implements OnInit, OnDestroy {

  profileForm = new FormGroup({
    firstname: new FormControl(''),
    lastname: new FormControl(''),
    nickname: new FormControl(''),
    email: new FormControl(
      { value: '', disabled: true },
      [ Validators.required, Validators.email ]
    ),
    dataApproval: new FormControl(''),
  });

  localProgress = false;
  profileData: any;
  ref: AngularFireObject<any>;
  userId: any = null;
  userData: any;
  avatarURL: string = 'assets/img/avatars/avataaars-default.png';
  institutionId: string;
  snackBarConfig: any;
  translateSub: Subscription;

  constructor(

    public ats: AuthService,
    public db: AngularFireDatabase,
    public router: Router,
    public cs: ControllsService,
    public snackBar: MatSnackBar,
    public translate: TranslateService,

  ) { 
    // custom styleing for snackbar
    this.snackBarConfig = new MatSnackBarConfig();
    this.snackBarConfig.panelClass = ['background-green'];
    this.snackBarConfig.duration = 4000;
    this.snackBarConfig.verticalPosition = 'top';
  }

  ngOnInit() {
    this.localProgress = true;

    this.ats.usr$
      .pipe(
        // execute only if a valid uid exists
        filter(val => val.uid !== null),
        distinctUntilChanged(),
      )
      .subscribe(val => {
        // console.log('EduProfileComponent->ats: ', val);
        if (val) {
          this.userData = val;
          this.userId = val.uid;
          this.institutionId = val.initiatorId;

          this.ref = this.db.object('profiles/' + this.userId);
          this.profileData = this.ref.valueChanges();

          this.fetchProfile();
        }
        this.localProgress = false;
      });

    this.ats.avatarEmitter
      .pipe(filter(_val => _val !== null))
      .subscribe(val => {
        this.avatarURL = val;
        // dummy action to make form dirty
        this.profileForm.markAsDirty();
      });
  }

  fetchProfile() {
    // console.log('userId: ', this.userId);
    
    this.profileData
      .subscribe(result => {
        // console.log('user data: ', result);
        this.localProgress = false;
        // result might be null
        if (result) {
          // provide avartar Url to avatar component
          this.avatarURL = result.contactData.avatar;

          this.profileForm.patchValue({
            firstname: result.privateData.firstname,
            lastname: result.privateData.lastname,
            nickname: result.contactData.nickname,

            // take allways from fb account data --> editing diabled
            email: this.userData.email,
            dataApproval: result.contactData.dataApproval,
          });
        } else {
          // insert data from fb account
          let tmp;
          if (!this.userData.displayName && this.userData.email) {
            // TODO: throws error -> TypeError: Cannot read property 'split' of null
            tmp = this.userData.email.split('@');

            this.profileForm.patchValue({
              nickname: tmp[0]
            });
          } else {
            this.profileForm.patchValue({
              nickname: this.userData.displayName
            });
          }
          this.profileForm.patchValue({
            email: this.userData.email,
          });
        }
      });

  }

  saveProfile() {

    const data = {
      contactData: {
        avatar: this.avatarURL, 
        dataApproval: this.profileForm.get('dataApproval').value,
        email: this.profileForm.get('email').value,
        nickname: this.profileForm.get('nickname').value,
      },
      privateData: {
        firstname: this.profileForm.get('firstname').value,
        lastname: this.profileForm.get('lastname').value,
      }
    };

    let msg;
    this.ref = this.db.object('profiles/' + this.userId);
    this.ref
      .update(data)
      .then(() => {
        // dummy to mark form as dirty
        this.profileForm.markAsPristine();
        // give feedback
        this.translate.get('edu.edu-profile.snackbar-feedback')
          .subscribe(( val: string ) => {
            this.snackBar.open(val, '', this.snackBarConfig );
          });
      });
  }

  ngOnDestroy() {
    // throws error ???
  }

  // end of class
}
