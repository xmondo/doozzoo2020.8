import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EduProfileComponent } from './edu-profile.component';

describe('EduProfileComponent', () => {
  let component: EduProfileComponent;
  let fixture: ComponentFixture<EduProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EduProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EduProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
