import { Component, OnInit, Input } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

interface Institution {
  name: string;
  description: string;
  keyvisual?: string;
}

@Component({
  selector: 'app-institution-profile',
  templateUrl: './institution-profile.component.html',
  styleUrls: ['./institution-profile.component.scss']
})
export class InstitutionProfileComponent implements OnInit {
  @Input() institutionId: string;
  @Input() layout: string; // blockNoImage | blockImageRight | blockLogoRightImageBottom | cardImageTop | homeWelcome

  institution: Observable<any>;
  constructor(
    public afs: AngularFirestore,
    public sanitizer: DomSanitizer,
  ) { }

  ngOnInit() {
    // console.log('institutionId: ', this.institutionId);
    // fetch institution data
    this.institution = this.afs
      .collection('institutions')
      .doc(this.institutionId)
      .valueChanges()
      .pipe(
        map((val: any) => {
          // console.log('###', val)
          const data = val; //{ 'description': val.description, 'keyvisual': val.keyvisual, 'name': val.name };
          return data;
        }),
      );
  }

  doSafeHtml(rawHtml){
    const sanitizedHtml: SafeHtml = this.sanitizer.bypassSecurityTrustHtml(rawHtml);
    return sanitizedHtml;
  }

}
