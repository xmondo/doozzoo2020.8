import { Pipe, PipeTransform } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireDatabase, AngularFireObject } from '@angular/fire/database';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Pipe({
  name: 'getEduUserRole'
})
export class GetEduUserRolePipe implements PipeTransform {
  constructor(
    private afs: AngularFirestore,
    private db: AngularFireDatabase,
  ) {
    // ...
  }

  transform(userId: string, institutionId: string): Observable<any> {
    // ...
    // console.log('getEduUserRole', userId, institutionId);

    return this.db
      .object(`users/${userId}/role`)
      .valueChanges()
      .pipe(
        // map(val => val ?? userId),
        map(val => val ?? 0),
      )

    /*
    return this.afs
      .doc(`institutions/${institutionId}`)
      .collection('accounts', ref => ref.where('userId', '==', userId))
      //.snapshotChanges()
      .get()
      .pipe(map((val:any) => {
        console.log(val.snapshot.data())
      }))
    */
  }
}
