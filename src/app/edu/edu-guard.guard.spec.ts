import { TestBed, async, inject } from '@angular/core/testing';

import { EduGuardGuard } from './edu-guard.guard';

describe('EduGuardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EduGuardGuard]
    });
  });

  it('should ...', inject([EduGuardGuard], (guard: EduGuardGuard) => {
    expect(guard).toBeTruthy();
  }));
});
