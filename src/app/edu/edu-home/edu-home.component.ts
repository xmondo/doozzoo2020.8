import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/auth.service';
import { ControllsService } from 'src/app/shared/controlls.service';
import { Observable } from 'rxjs';
import { filter, distinctUntilChanged, map } from 'rxjs/operators';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edu-home',
  templateUrl: './edu-home.component.html',
  styleUrls: ['./edu-home.component.scss']
})
export class EduHomeComponent implements OnInit {

  eduUserList: any;

  constructor(
    public ats: AuthService,
    public cs: ControllsService,
    private db: AngularFireDatabase,
    private router: Router,
    private afs: AngularFirestore
  ) { 

    // fire only for role = Institutional Admin
    this.ats.usr$
      .pipe(
        filter(val => val !== null),
        filter(val => val.role === 3),
        distinctUntilChanged(),
      )
      .subscribe(val => {
        this.getInstitutionId(val.uid);
      });

  }

  ngOnInit() {
    //...
  }

  async getInstitutionId(uid) {
    this.eduUserList = this.afs
      .collection('institutions', ref => ref.where('users', 'array-contains', uid))
      .valueChanges({idField: 'key'});
      // .subscribe((val:any) => console.log(val)

  }

}
