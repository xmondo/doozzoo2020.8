import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable, of } from 'rxjs';
import { map, mergeMap, switchMap } from 'rxjs/operators';

export interface EditRoleEvent {
  key: string,
  role: number,
}

@Component({
  selector: 'app-edit-role',
  templateUrl: './edit-role.component.html',
  styleUrls: ['./edit-role.component.scss']
})
export class EditRoleComponent implements OnInit {
  @Input() role: number;
  @Input() key: string;
  @Input() userId: string;
  @Input() active: boolean = false;
  @Output() selectRole: EventEmitter<EditRoleEvent> = new EventEmitter();

  roleMatch: Observable<boolean>;

  constructor(
    private adb: AngularFireDatabase,
  ) { }

  /**
   * compares the to be role from institution with the one set in users/...
   */
  ngOnInit(): void {
    this.roleMatch = of(this.role)
      .pipe(
        mergeMap(role => this.adb
          .object(`users/${this.userId}/role`)
          .valueChanges()
          .pipe(
            map(val => role === val)
          )
        )
      );
  }

  doSelectRole(role:number) {
    this.selectRole.emit({ key: this.key, role: role});
  }

  doRepairRole() {
    // dirty trick: as a directly the role can not be set due to permissions,
    // we use the fact, that a newly set role in the institution db will trigger an update in users/...
    // this will only set the intended role
    this.selectRole.emit({ key: this.key, role: -1});
    this.selectRole.emit({ key: this.key, role: this.role});
    /*
    this.adb
      .object(`users/${this.userId}/role`)
      .set(this.role)
      .catch(e => console.log('doRepairRole error: ', e));
    */
  }

}
