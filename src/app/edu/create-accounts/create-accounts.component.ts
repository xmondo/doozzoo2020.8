import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/shared/auth.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormControl, Validators} from '@angular/forms';
import { map, switchMap, flatMap, first, filter, mergeMap, startWith } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmRemoveCoachComponent } from '../edu-coaches/confirm-remove/confirm-remove.component';
import { DeltaTango } from 'src/app/shared/delta-tango';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { EditRoleEvent } from './edit-role/edit-role.component';

interface AccountData {
  email:string;
  role: number; // 0,1,2,3 
  status:string; // created | inviteTriggered | inviteSent | inviteAccepted
  activated:boolean;
  allowInvites:boolean;
  createdOn:number;
  coachId?:string;
  key?:string;
  initiatorId?: string; // institutionId or coachId
}

interface userPeers {
  institutionId: string,
  userId: string,
  peerId: string,
}

@Component({
  selector: 'app-create-accounts',
  templateUrl: './create-accounts.component.html',
  styleUrls: ['./create-accounts.component.scss'],
  providers: [ DeltaTango ],
})
export class CreateAccountsComponent implements OnInit {
  /**
   *  mode="institution" // institution | coach --> if used from an institution or from a single coach account
   *  initiatorId="institutionId" // needs the regarding id as input --> institution id or coachId
   */
  @Input() mode: string; // institution | institutionCoach // ?? | coach 
  @Input() institutionId: string; // id of institution
  @Input() coachId: string; // id of coach
  initiatorId: string; // initiator of invite can be institution

  accountsListRef: any;
  accountsList: any;
  accountsList2: Observable<any>;
  freshInvites: Observable<any>;

  invitesListRef: any;
  accountEmailValue: string;
  accountEmailValue2: string;
  institutionData: Observable<any>;
  institutionCoachAllowInvites: Observable<boolean>;
  progressBar: Observable<boolean>;
  accountRole: any;
  //emailExistsFlag: boolean = false;
  itemsPerPageValue: any;

  // pagination vars
  filterInputControl = new FormControl();
  filterargs: any;
  q: any; // TODO: ???
  pageSize: Number = 10;
  p: Number = 1;
  inputType: any;

  role$: Subject<number>;
  filterRole: any = '-1'; // must be string, drives me crazy

  // [a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}
  // \w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+
  // [a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+
  accountFormEmail = new FormControl('email', [ Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}') ]); //
  //accountFormEmail = new FormControl('email', [ Validators.required, Validators.email ]);

  snackBarConfig: any;

  constructor(
    private aroute: ActivatedRoute,
    public ats: AuthService,
    private afs: AngularFirestore,
    public dialog: MatDialog,
    private deltaTango: DeltaTango,
    public snackBar: MatSnackBar,
  ) { 
    this.accountRole = '0'; // default
    this.itemsPerPageValue = '10';

    // custom styleing for snackbar
    this.snackBarConfig = new MatSnackBarConfig();
    this.snackBarConfig.panelClass = ['background-orange'];
    // this.snackBarConfig.duration = 5000;
    this.snackBarConfig.verticalPosition = 'top';
  }

  async ngOnInit() {
    // console.log('*** mode, institution, initiator', this.mode, this.institutionId, this.initiatorId);

    // subscribe to filter input control, submit to filter input
    this.filterargs = this.filterInputControl
    // this.filterInputControl
      .valueChanges
      .pipe(
        startWith(''),
        map(val => {
          // console.log('***', val, val.length)
          if(val.length > 2) {
            // reset pagination
            this.p = 1;
            return val;
          } // else nothing
        })
      )

    this.accountsListRef = this.afs
      .doc(`institutions/${this.institutionId}`)
      .collection('accounts', ref => ref.orderBy('email'));

    if(this.mode === 'institution') {
      this.accountRole = '0'; // assign this as a default role for new accounts
      this.initiatorId = this.institutionId;
      await this.getAccountsInstitution();
      // filter for showing ALL roles
      this.role$.next(-1);

      // additionally simply push freshly created accounts, which are not yet connected into the array
      this.freshInvites = this.afs
        .doc('institutions/' + this.institutionId)
        //.collection('accounts', ref => ref.where('status', '==', 'created')) //, ref => ref.where('userId', '==', undefined))
        // in works as supported by firebase core
        // ### Argument of type '"in"' is not assignable to parameter of type 'WhereFilterOp'. ###
        .collection('accounts', ref => ref.where('status', 'in', ['created','error'])) //, ref => ref.where('userId', '==', undefined))
        .valueChanges({idField: 'key'})

    } else if (this.mode === 'institutionCoach') {
      this.accountRole = '0';
      this.initiatorId = this.coachId;
      this.getAccountsInstitution();

      // additionally simply push freshly created accounts, which are not yet connected into the array
      this.freshInvites = this.afs
        .doc('institutions/' + this.institutionId)
        .collection('accounts', ref => ref.where('initiatorId', '==', this.coachId))
        .valueChanges({idField: 'key'})

    // TODO: do I need this, istn it the institution mode
    } else {
      this.accountRole = '0';
      this.getAccountsCoachObs(this.institutionId, this.coachId);
    }
    
  }

  trackByFnKey(item){
    return item.key;
  }

  async getAccountsInstitution() {
    // ref to all invites
    this.invitesListRef = this.afs
        .collection('invites');

    if (this.mode === 'institution') {
      // switchmap dependent on filter/option
      this.role$ = new Subject<number>();
      return this.accountsList = this.role$
        .pipe(
          switchMap(
            role => {
              if (role === -1) {
                return this.afs
                  .doc('institutions/' + this.institutionId)
                  .collection('accounts', ref => ref.orderBy('role','desc').orderBy('email'))
                  .valueChanges({idField: 'key'});
              } else {
                return this.afs
                  .doc('institutions/' + this.institutionId)
                  .collection('accounts', ref => ref.where('role', '==', role).orderBy('email'))
                  .valueChanges({idField: 'key'}) 
              }
            }
          ) 
        );

    } else if (this.mode === 'institutionCoach') {

      this.getAccountsCoachObs(this.institutionId,this.coachId);
      // pulls settings for the coach from backend
      this.getInstitutionCoach();

    }

  // ngOnInit END
  }

  doSelectRole(event) {
    console.log(event)
    const option: number = parseInt(event.value);
    this.role$.next(option);
  }

  async emailExists(email: string) {
    const ref = await this.afs
      .doc('institutions/' + this.institutionId)
      .collection('accounts', ref => ref.where('email', '==', email))
      .get()
      .pipe(
        first(),
      )
      .toPromise();
    // console.log('size, doc[0]: ', ref.size, ref.docs[0]);
    return ref.size > 0 ? ref.docs[0] : null;
  }

  async getUserIdForEmail(email: string) {
    const ref = await this.afs
      .doc('institutions/' + this.institutionId)
      .collection('accounts', ref => ref.where('email', '==', email))
      .get()
      .toPromise();

    return true;
  }

  getAccountsCoachObs(institutionId, userId) {

    const peersObservable = this.afs
      .collection(
        'userPeers', ref => ref
          .where('userId', '==', userId)
          .where('institutionId', '==', institutionId))
      .valueChanges({idField: 'key'});

    /**
     * subscribe to the accounts of the complete institution
     * if the line item user is student (role = 0), take only coaches (role = 1) // filter
     * 
     */
    const accObs = this.afs
      .doc('institutions/' + institutionId)
      .collection('accounts')
      .valueChanges({idField: 'key'})
      .pipe(
        map((val:any) => val.map(item => {
          return { key: item.key, ...item };
        }).filter(_item => _item.userId !== userId))
      );

      /**
       * merge the peerObservable to filter the accountsObservable
       * with mergeMap we take both arrays and filter out the selected peers
       * as well as the line item userId
       * then we enrich the items with the key of the peer relation item, as this has to be deleted in case of using the remove function
       */
      // this.accountsCoachObs = peersObservable
      this.accountsList = peersObservable
        .pipe(
          mergeMap(peersArr => accObs
            .pipe(
              map(accountsArr => {
                return accountsArr.filter(item => {
                  const index = peersArr.findIndex((_item: any) => item.userId === _item.peerId)
                  if( index !== -1 && item.userId !== userId) {
                    item.peerKey = peersArr[index].key;
                    return true;
                  } else {
                    return false;
                  }
                })
              })
            )
          )
        );
  }

  async create(){

    const check = await this.emailExists(this.accountEmailValue);
    // console.log('emailCheck', check);
    const role: number =  parseInt(this.accountRole);
    const data: AccountData = {
      email: this.accountEmailValue,
      role: role, 
      status: 'created',
      activated: true,
      allowInvites: false,
      createdOn: Date.now(),
      initiatorId: this.initiatorId, // who is parent: InstitutionId | coachId
    }

    // if no equal email exists...
    if(check === null) {
      // reset form and add account data for further processiong
      this.accountEmailValue = '';
      this.accountEmailValue2 = '';

      //console.log('+++ ', data);

      await this.accountsListRef
        .add(data)
        .catch(error => console.log('set new account error:', error));

      if (this.mode === 'institutionCoach') {
        // refresh
        return this.getAccountsCoachObs(this.institutionId,this.coachId);
      }

      return 'account created';

    } else {
    
      const msg = this.mode === 'institutionCoach' ? `email ${this.accountEmailValue} exists already and will be connected automatically!` : `email ${this.accountEmailValue} exists already!`;
      // console.log(msg);
      // this.emailExistsFlag = true;

      this.snackBar.open( msg, 'ok', this.snackBarConfig );
      // reset only repeat field
      this.accountEmailValue2 = '';

      /**
       * if student already exists in organization,
       * give feedback and connect student with coach
       * 
       */
      if (this.mode === 'institutionCoach') {
        // check contains the account obj found
        // console.log('account obj: ',  check.data());

        const peer: userPeers = {
          institutionId: this.institutionId,
          userId: this.coachId,
          peerId: check.data().userId
        }
        await this.doAddPeer(peer);
        // refresh
        this.getAccountsCoachObs(this.institutionId,this.coachId);
      }
      return msg;
    }
  }

  update(item: any){
    // console.log('update', item);
    this.accountsListRef.doc(item.key).update(item);
  }

  toggleAccess(item, event) {
    // console.log('update', item);
    item.activated = event.checked;
    this.accountsListRef.doc(item.key).update(item);
  }

  toggleAllowInvites(item, event) {
    // console.log('update', item);
    item.allowInvites = event.checked;
    this.accountsListRef.doc(item.key).update(item);
  } 

  sendInvite(item) {
    item.status = 'inviteSentPrepare';
    this.accountsListRef.doc(item.key).update(item);
    this.createInviteLink(item);
  }

  delete(item) {
    // if coach -> remove coachId from field initiatorId
    // TODO: --> what should happen with user account
    const ref = this.afs
        .doc('institutions/' + this.institutionId)
        .collection('accounts')
        .doc(item.key);
        // all acounts
    // console.log('delete: ', this.mode, item)
    ref.delete();
    
    if(this.mode === 'institutionCoach') {
      // refresh
      this.getAccountsCoachObs(this.institutionId,this.coachId);
    }
    
  }

  createInviteLink(item:any) {
    // ...
    // this.institutionId

    // generate random password
    const token = this.deltaTango.generateTango();

    //
    const data = {
      type: this.mode, // 
      initiatorId: this.initiatorId, // this.institutionId,
      institutionId: this.institutionId,
      triggerId: item.key,
      target: {
        type: 'email',
        value: item.email,
        password: token, // '#doozzoo',
      },
      createdOn: Date.now(),
      accountId: 'unset', // will be enriched by created account ater
      role: this.accountRole,
    }
    this.invitesListRef
      .add(data);
  }

  confirmRemove(item): void {
    // console.log(item);
    const dialogRef = this.dialog.open(ConfirmRemoveCoachComponent, {
      width: '450px',
      data: { 
        clipTitle: item.title,
        messageId: 'create-accounts.delete',
      }
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        //console.log('The dialog was closed', result);
        // result is 'true' or undefined
        if (result === true) {
          // trigger delete
          this.delete(item);
        }
      });
  }

  confirmRemovePeer(item): void {
    // console.log(item);
    const dialogRef = this.dialog.open(ConfirmRemoveCoachComponent, {
      width: '450px',
      data: { 
        clipTitle: item.title,
        messageId: 'create-accounts.removePeer',
      }
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        // console.log('The dialog was closed', result);
        // result is 'true' or undefined
        if (result === true) {
          // trigger delete
          this.doRemovePeer(item);
        }
      });
  }

  buttonLabel(status){
    switch (status) {
      case 'created': return 'send invite';
      break;
      case 'inviteSentPrepare': return 'in progress';
      break;
      case 'inviteSent': return 'invite sent';
      break;
      case 'inviteAccepted': return 'invite accepted';
      break;
      case 'error': return 'error';
      break;
      case 'userNotFound': return 'error';
      break;
      case 'studentAssignedToCoach': return 'connected';
      break;
      case 'coachAssignedToInstitution': return 'connected';
      break;
      default: return 'send invite';
    }
  }

  icon(status) {
    if(status === 'inviteSent' || status === 'inviteAccepted' || status === 'studentAssignedToCoach' || status === 'coachAssignedToInstitution' ) {
      return 'done';
    } else if (status === 'inviteSentPrepare' || status === 'created') {
      return 'send invite'
    } else if (status === 'error') {
      return 'warning';
    }
  }

  // return false if older than 24hrs
  youngerThan24hrs(creationTime) {
    // must be older than 24hrs
    const thresholdTime = Date.now() - (1000 * 60 * 60 * 24);
    //const comparisonTime = Date.parse(creationTime);
    const comparisonTime = creationTime; // already unix timestamp
    // const calc = (thresholdTime - comparisonTime) / (1000 * 60 * 60 * 24);
    // console.log('days since creation: ', calc);
    return comparisonTime > thresholdTime ? true : false;
  }

  // handle peers
  /** check whether record already exists */
  async getPeers(peer: userPeers) {
    const ref = await this.afs
      .collection('userPeers', ref => ref.where('institutionId', '==', peer.institutionId).where('userId', '==', peer.userId).where('peerId', '==', peer.peerId))
      .get()
      .toPromise();
    return ref.size > 0 ? true : false;
  }

  async addPeer(peer: userPeers) {
    const status = await this.afs
      .collection('userPeers')
      .add(peer)
      .catch(error => console.log('addPeer', error));
    return status;
  }

  async doAddPeer(peer) {

    // this.waitForChip = true;
    const exists = await this.getPeers(peer);
    // console.log('exists: ', exists, peer);

    if(exists) {
      // this.waitForChip = false;
      // console.log('connection exists');
      return 'connection exists';
    } else {
      // this.waitForChip = false;
      return this.addPeer(peer);
    }

  }

  async doRemovePeer(account) {
    // console.log('peer: ', account);
    const peer: userPeers = {
      institutionId: this.institutionId,
      userId: this.coachId,
      peerId: account.userId,
    }
    const ref = await this.afs
      .collection('userPeers', ref => ref.where('institutionId', '==', peer.institutionId).where('userId', '==', peer.userId).where('peerId', '==', peer.peerId))
      .get()
      .toPromise();
    ref.forEach(item => {
      item.ref.delete();
    })
  }

  getInstitutionCoach() {
    this.institutionCoachAllowInvites = this.afs
      .collection('institutions')
      .doc(this.institutionId)
      .collection('accounts', ref => ref.where('userId', '==', this.coachId))
      .valueChanges({idField: 'key'})
      .pipe(
        filter(val => val !== null),
        map((val: any) => {
          return val[0].allowInvites === true ? true : false;
        }),
      );
  }

  // 
  selectRole(event: EditRoleEvent) {
    this.accountsListRef.doc(event.key).update({ role: event.role });
  }


// end of class
}