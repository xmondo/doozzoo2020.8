import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EduCoachesDetailComponent } from './edu-coaches-detail.component';

describe('EduCoachesDetailComponent', () => {
  let component: EduCoachesDetailComponent;
  let fixture: ComponentFixture<EduCoachesDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EduCoachesDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EduCoachesDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
