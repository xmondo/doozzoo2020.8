import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edu-coaches-detail',
  templateUrl: './edu-coaches-detail.component.html',
  styleUrls: ['./edu-coaches-detail.component.css']
})
export class EduCoachesDetailComponent implements OnInit {
  institutionId: any;
  institutionCoachId: any;
  constructor(
    private aroute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.institutionId = this.aroute.snapshot.paramMap.get('institutionId');
    this.institutionCoachId = this.aroute.snapshot.paramMap.get('institutionCoachId');
    console.log('@@@ -> ', this.institutionId, this.institutionCoachId);
  }

}
