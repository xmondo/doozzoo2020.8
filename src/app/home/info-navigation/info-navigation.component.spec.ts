import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoNavigationComponent } from './info-navigation.component';

describe('InfoNavigationComponent', () => {
  let component: InfoNavigationComponent;
  let fixture: ComponentFixture<InfoNavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoNavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
