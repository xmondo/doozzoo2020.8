import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-info-navigation',
  templateUrl: './info-navigation.component.html',
  styleUrls: ['./info-navigation.component.scss']
})
export class InfoNavigationComponent {
  @Input() deviceService;
}
