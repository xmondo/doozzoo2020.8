import { Component, OnInit, AfterViewInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { Router, RouterEvent, NavigationEnd } from '@angular/router';
import { ControllsService } from '../shared/controlls.service';
import { AuthService } from '../shared/auth.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import { version } from '../../../package.json';
import { filter, distinctUntilChanged } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, AfterViewInit, OnDestroy {
  subContainer: Subscription = new Subscription();
  version: string = version;

  constructor(
    public router: Router,
    public cs: ControllsService,
    public a: AuthService,
    public ref: ChangeDetectorRef,
    public deviceInfo: DeviceDetectorService
  ) {
    // ...

  }

  ngOnInit() {
    // console.log('user: ', this.user)
    // ...
    // console.log('deviceService: ', this.deviceInfo);
    // catch user that did not fully complete their registration
    const pms = this.a.profileMissingStatus$
      .pipe(distinctUntilChanged())
      .subscribe(val => {
        // console.log('home->profileMissingStatus: ', val);
        val ? this.router.navigate(['login']) : '';
      })
    this.subContainer.add(pms);
/*
    const ied = this.a.isEdu$
      .pipe(distinctUntilChanged())
      .subscribe(val => {
        val ? this.router.navigate(['edu']) : '';
      })
    this.subContainer.add(ied);  
*/
  }

  ngAfterViewInit() {
    // ...
  }

  ngOnDestroy() {
    this.subContainer.unsubscribe();  
  }
}
