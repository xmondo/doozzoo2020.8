import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

interface Widget {
  id: string;
  timeout: number;
  show: string; // show | hide
  content: {
    header: string;
    body: string;
    date: string;
    url: string;
    imgLink: string;
  }
}

@Component({
  selector: 'app-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.scss']
})
export class WidgetComponent implements OnInit, OnDestroy {
  @Input() adminpath: string = 'hallo';
  header: SafeHtml;
  body: SafeHtml;
  widgetShow: boolean = false;
  widgetContent: Subscription;
  widgetId: string;
  hover: boolean = false;
  wdgSub: Subscription;
  date: string;
  url: string;
  imgLink: string;
  isAdminArea: boolean;
  
  constructor(
    private db: AngularFireDatabase,
    private sanitizer: DomSanitizer,
    private router: ActivatedRoute,
  ) { }

  async ngOnInit() {
    this.wdgSub = this.db
      .object('/widget')
      .valueChanges()
      .pipe(filter(val => val !== null))
      .subscribe(
        (data:any) => this.setWidgetContent(data)
      );

    this.isAdminArea = this.router.snapshot.url.toString().indexOf('admin') !== -1 ? true : false;
  }

  async setWidgetContent(data: Widget){
    // console.log('data: ', data)
    if (data.show === 'show' || data.show === 'permanent') {
      this.widgetId = data.id;
      this.header = this.sanitizer.bypassSecurityTrustHtml(data.content.header);
      this.body = this.sanitizer.bypassSecurityTrustHtml(data.content.body);
      this.date = data.content.date;
      this.url = data.content.url;
      this.imgLink = data.content.imgLink;

      const status = localStorage.getItem('doozzooWidget');

      if(!this.isAdminArea) {
        if(status) {
          this.widgetShow = (status === data.id) ? false : true;
        } else {
          this.widgetShow = true; 
        }
      }

      // override if permanent
      if(data.show === 'permanent') {
        this.widgetShow = true;   
      }

    }

    // always override for admin area
    if (this.isAdminArea) {
      this.widgetShow = true;
    }

  }

  widgetSubmit() {
    // store dismiss in session/ local storage
    localStorage.setItem('doozzooWidget', this.widgetId);
    if(!this.isAdminArea) {
      this.widgetShow = false;
    }
  }

  ngOnDestroy() {
    this.wdgSub.unsubscribe();
  }

}
