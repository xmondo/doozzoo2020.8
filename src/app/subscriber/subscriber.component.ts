import { Component, ElementRef, AfterViewInit, ViewChild, Input, OnInit, OnDestroy, Renderer2 } from '@angular/core';
import * as OT from '@opentok/client';
import { OpentokService, sessionPrefs } from '../shared/opentok.service';
import config from '../../config';
import { filter, map, distinctUntilChanged } from 'rxjs/operators';
import { ThumbsManagerService } from '../shared/thumbs-manager.service';
import { Subscription } from 'rxjs';
import { DeviceDetectorService } from 'ngx-device-detector';
import { PublisherStoreService } from '../publisher/publisher-store.service';

interface subscriberOptions {
  insertMode: 'append' | 'replace' | 'after' | 'before',
  fitMode: 'contain' | 'cover',
  width?: string,
  height?: string, 
  // test network quality
  testNetwork?: boolean,
}

@Component({
  selector: 'app-subscriber',
  templateUrl: './subscriber.component.html',
  styleUrls: ['./subscriber.component.scss']
})

export class SubscriberComponent implements AfterViewInit, OnInit, OnDestroy {
  @ViewChild('subscriberDiv') subscriberDiv: ElementRef;
  @Input() session: OT.Session;
  @Input() stream: OT.Stream;
  
  localHeroStatus = <boolean>false;
  localDistanceRight = 0;
  fitModeCover: Boolean = true;
  thumbsId: string = null;
  isHero: boolean = true;
  toRight: number = 0;
  status: any;
  thumbsObj: any;
  nameObj: any;
  subscriber: OT.Subscriber;
  subscribeAudio: boolean = true;
  hasVideo: boolean = false;
  hasAudio: boolean = false;
  audioLevel: number;
  tmsSub: Subscription;
  isSafari: boolean;
  doGrid: any;
  sessionPrefs: sessionPrefs;
  subContainer: Subscription = new Subscription();
  sinkId: string;

  constructor(
    public ots: OpentokService,
    public tms: ThumbsManagerService,
    public renderer: Renderer2,
    private dds: DeviceDetectorService,
    private publisherStore: PublisherStoreService,
    ) { 
    // ...
  }

  ngOnInit() {
    // init status attributes
    this.status = this.tms.getThumbsStatus(this.thumbsId);
    console.log('getThumbsStatus: ', this.thumbsId);
    this.isSafari = this.dds.browser.toLowerCase().indexOf('safari') !== -1 ? true : false;
    // this.sessionPrefs = this.ots.getSessionPrefs();
    // this.sinkId = this.sessionPrefs.audioOutDeviceId;
  }

  ngAfterViewInit() {
    const self = this;

    let subscriberOptions: subscriberOptions = {
      insertMode: 'append',
      fitMode: 'contain',
      //width: '100%',
      //height: '100%', 
      // test network quality
      // testNetwork: true,
    };
    if(!this.isSafari) {
      subscriberOptions.height = '100%';
      subscriberOptions.width = '100%';
    }
    
    this.subscriber = this.session
      .subscribe(
        this.stream, 
        this.subscriberDiv.nativeElement, 
        subscriberOptions, 
        (err) => {
          if (err) {
            alert(err.message);
          }
        });

    // show title bar
    // this.subscriber.setStyle('nameDisplayMode', 'on');
    this.subscriber.setStyle('audioLevelDisplayMode', 'off');

    let movingAvg = null;
    this.subscriber.on('audioLevelUpdated', function(event) {
      if (movingAvg === null || movingAvg <= event.audioLevel) {
        movingAvg = event.audioLevel;
      } else {
        movingAvg = 0.7 * movingAvg + 0.3 * event.audioLevel;
      }
      // 1.5 scaling to map the -30 - 0 dBm range to [0,1]
      self.audioLevel = (Math.log(movingAvg) / Math.LN10) / 1.5 + 1;
      self.audioLevel = Math.min(Math.max(self.audioLevel, 0), 1);
      self.audioLevel = Math.floor(self.audioLevel * 100);
      // document.getElementById('subscriberMeter').value = logLevel;
    });

    this.subscriber
      .on('videoElementCreated', (event: any) => {
      // console.log('subscriber started streaming: ', event);

      // self.doStats();

      // read availablility of A/V in streams
      self.hasVideo = event.target.stream.hasVideo;
      self.hasAudio = event.target.stream.hasAudio;

      // parse obj from initPublisher Attributes name field
      try {
        console.log('subscriber->name: ', event.target.stream.name);
        self.nameObj = JSON.parse(event.target.stream.name);
        // console.log('subscriber->name: ', event.target.stream.name);
      }
      catch(error) {
        console.log('error name parsing: ', error, event.target.stream.name);
      }

      // console.log('subscriber->videoElementCreated->nameObj: ', self.nameObj);
      self.renderSubscriberElement();

      // 
      self.thumbsId = event.target.streamId;
      self.tms.thumbsEmitter.next({ action: 'create', id: event.target.streamId, event: event });

      // set sinkId on launch of subsciber element
      // will trow error on FF/Safari etc.
      try {
        /*
        event.element.setSinkId(self.sinkId);
        // subscribe for changes
        const dve = self.ots.sessionPrefsEmitter
          .subscribe(val => {
            if (self.sinkId !== val.audioOutDeviceId) {
              // use the sinkId submitted in payload
              event.element.setSinkId(self.sinkId);
            }
          });
        */
        const dve = self.publisherStore
          .audioOutDeviceId$
          .subscribe(deviceId => event.element.setSinkId(deviceId));
        self.subContainer.add(dve);
      } catch (error) {
        console.log( 'subscriber->setSinkId: ', error );
      }

      // toggle view from "cover" to all for a better overview
      // setTimeout(() => self.toggleVideoView(), 500);
    });

    this.subscriber.on('destroyed', function(event: any) {
      // ...
      self.tms.thumbsEmitter.next({ action: 'delete', id: self.thumbsId, event: event });
      // console.log('Subscriber destroyed...', event);
    });

    // with every change, calc distance to right
    const tmsSub = this.tms.thumbsEmitter
      .pipe(
        // filter(val => val.action === 'isHero'),
        distinctUntilChanged(),
      )
      .subscribe(val => {
        // console.log(val)
        this.status = this.tms.getThumbsStatus(this.thumbsId);

        // get thumbsobject
        const thumbsArr = this.tms.thumbsArrayGet();
        this.thumbsObj = thumbsArr.find(item => item.id === this.thumbsId);

        if(val.action === 'muteIncommingAudio') {
          this.muteIncommingAudio(val.event);
        }
      });
    this.subContainer.add(tmsSub);

    // ###
    // console.log('hasAudio, hasVideo: ', this.hasAudio, this.hasVideo);

  }

  ngOnDestroy() {
    this.subContainer.unsubscribe();
  }

  doStats() {
    /*
    const self = this;
    let prevStats;
    setInterval(() => {
      self.subscriber.getStats(function(error, stats) {
        if (error) {
          console.error('Error getting subscriber stats. ', error.message);
          return;
        }
        if (prevStats) {
          const videoPacketLossRatio = stats.video.packetsLost /
            (stats.video.packetsLost + stats.video.packetsReceived);
          console.log('video packet loss ratio: ', videoPacketLossRatio);
          const videoBitRate = 8 * (stats.video.bytesReceived - prevStats.video.bytesReceived);
          console.log('video bit rate: ', videoBitRate, 'bps');
          const audioPacketLossRatio = stats.audio.packetsLost /
            (stats.audio.packetsLost + stats.audio.packetsReceived);
          console.log('audio packet loss ratio: ', audioPacketLossRatio);
          const audioBitRate = 8 * (stats.audio.bytesReceived - prevStats.audio.bytesReceived);
          console.log('audio bit rate: ', audioBitRate, 'bps');
        }
        prevStats = stats;
      });
    }, 1000);
    */
  }

  toggleVideoView() {
    // OT_fit-mode-contain | OT_fit-mode-cover
    let OT_root = this.subscriberDiv.nativeElement.querySelector('.OT_root');
    const state = OT_root.classList.value.indexOf('OT_fit-mode-cover') === -1 ? false : true;

    console.log('state: ', state);
    if (state) {
      this.fitModeCover = false;
      this.renderer.removeClass(OT_root, 'OT_fit-mode-cover');
      this.renderer.addClass(OT_root, 'OT_fit-mode-contain');
    } else {
      this.fitModeCover = true;
      this.renderer.removeClass(OT_root, 'OT_fit-mode-contain');
      this.renderer.addClass(OT_root, 'OT_fit-mode-cover');
    }
  }

  toggleAudio() {
    // const current = this.subscriber.setAudioVolume
    this.subscribeAudio = !this.subscribeAudio;
    this.subscriber.subscribeToAudio(this.subscribeAudio);
  }

  muteIncommingAudio(state: boolean) {
    this.subscribeAudio = state;
    this.subscriber.subscribeToAudio(this.subscribeAudio);
    console.log('muteIncommingAudio: ', state);
  }

  renderSubscriberElement() {
    // this.renderer.setAttribute(this.el.nativeElement, 
    
    const avatarEl = this.subscriberDiv.nativeElement.querySelector('.subscriber .OT_widget-container .OT_video-poster');  
    // this.renderer.setAttribute(avatarEl, 'style', 'background-image: url(' + nameObj.avatarUrl + ') !important');

    if(this.isSafari) {
      const videoRoot = this.subscriberDiv.nativeElement.querySelector('.OT_root.OT_subscriber');
      this.renderer.setAttribute(videoRoot, 'style', 'width: 100%; height: 100%;');
    }

    const url = this.nameObj?.avatar ? `url(${this.nameObj.avatar})` : `url(assets/img/avatars/avataaars-default.png)`;
    this.renderer.setStyle(
      avatarEl,
      'background-image',
      url
    );
  }

// end of component
}
