import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { AngularFireDatabase } from '@angular/fire/database';
import { distinctUntilChanged, tap } from 'rxjs/operators';
import { ControllsService } from 'src/app/shared/controlls.service';
import { DataSnapshot } from '@angular/fire/database/interfaces';
import { SessionStoreService } from '../session-store.service';
import { ParticipantList } from 'src/app/my-students/participant-list/participant-list.component';

@Component({
  selector: 'app-lock-button',
  templateUrl: './lock-button.component.html',
  styleUrls: ['./lock-button.component.scss']
})
export class LockButtonComponent implements OnInit, OnDestroy {
  @Input() userId; // user = coach
  @Input() roomId;

  currentRoomlock: boolean;
  subContainer: Subscription = new Subscription();
  participantsArr: ParticipantList[] = [];

  constructor(
    public db: AngularFireDatabase,
    private cs: ControllsService,
    private sessionStore: SessionStoreService,
  ) { 
    // ...

  }

  ngOnInit() {
    this.sessionStore
      .participants$
      .subscribe(participants => this.participantsArr = participants)

    const roomLockObs$:Observable<boolean | DataSnapshot> = this.db
      .object(`classrooms/${this.userId}/locked`)
      // .object(`now/${this.roomId}`)
      .valueChanges()
      .pipe(
        tap((val:boolean | DataSnapshot) => console.log(val)),
        distinctUntilChanged(),
      )

    // this.watchParticipants();

    /** lock button functions:
     * lock is like the entrance door to the session
     * students outside have access if the door is open
     * and can not enter if it is closed
     * students inside are not concerned of any lock button actions
     * in detail:
     * students in state "isWaiting" and all other states
     * - can directly enter if lock is opened (change access to true)
     * students in state "inSession" false are set to access "false" if the door is locked
    */

    const participantSub = roomLockObs$
      .subscribe((locked:boolean) => {
        // console.log('participantsKeysArr: ', this.participantsKeysArr);
        this.currentRoomlock = locked;
        if(locked === false) {
          this.participantsArr
            // .filter(val => val.isWaiting === true)
            .forEach(participantId => {
              this.db
                .object(`classrooms/${this.userId}/participants/${participantId.key}`)
                .update({ access: true });
            });
        } else {
          this.participantsArr
            .filter(val => val.inSession === false)
            .forEach(participantId => {
              this.db
                .object(`classrooms/${this.userId}/participants/${participantId.key}`)
                .update({ access: false });
            
            });
        }
      }); 
    this.subContainer.add(participantSub);
    
    // subscribe to sattelite button
    this.cs.sync('sessionLock')
      .subscribe(val => {
        // console.log('sessionLock: ', val);
        this.toggleLock();
      })
    
  }

  toggleLock() {
    this.currentRoomlock = !this.currentRoomlock;
    this.db.object(`classrooms/${this.userId}/locked`).set(this.currentRoomlock);
    this.db.object(`now/${this.roomId}/locked`).set(this.currentRoomlock);
  }

  ngOnDestroy() {
    this.subContainer.unsubscribe();
  }

}
