import { Component, OnInit } from '@angular/core';
import { ToClipboard } from '../../shared/to-clipboard';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '../../shared/auth.service';

@Component({
  selector: 'app-clipboard-button',
  templateUrl: './clipboard-button.component.html',
  styleUrls: ['./clipboard-button.component.scss']
})
export class ClipboardButtonComponent implements OnInit {
  showTooltip = false;
  hoverState: boolean = false;
  constructor(
    private clip: ToClipboard,
    private router: Router,
    public snackBar: MatSnackBar,
    public a: AuthService,
  ) {
    // ...
  }

  ngOnInit() {
    this.a.usr$
      .subscribe(val => {
        // coach
        /*
        if (val.role === 1) {
          this.showTooltip = true;
          setTimeout(() => this.showTooltip = false, 6000);
        // student
        } else 
        */
       // show only in test mode
       if (val.role > -1) {
          this.showTooltip = false;
        } else {
          this.showTooltip = true;
        }
      });
  }

  doClose() {
    this.showTooltip = !this.showTooltip;
  }

  doShowTooltip() {
    this.hoverState = true;
    const timeout = setTimeout(() => { 
      if (this.hoverState) {
        this.showTooltip = true;
      }
    }, 500);   
  }

  copyToClipboard() {
    const url = this.router.url;
    const fullUrl = location.port ? `${location.protocol}//${location.hostname}:${location.port + url}` : `${location.protocol}//${location.hostname + url}`;
    
    this.snackBar.open( 'Room URL was copied to clipboard: ' + fullUrl, '', { duration: 4000 } );
    this.clip.copyToClipboard(fullUrl);
    this.showTooltip = false;
  }

}
