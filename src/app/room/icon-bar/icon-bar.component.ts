import { Component, OnInit, OnDestroy, Input, ViewChild, ElementRef, AfterViewInit, ChangeDetectionStrategy } from '@angular/core';
import { AuthService } from 'src/app/shared/auth.service';
import { AudioService } from 'src/app/shared/audio.service';
import { DirectshareService } from 'src/app/shared/directshare.service';
import { fromEvent, merge, Observable, Subscription, timer } from 'rxjs';
import { debounce, distinctUntilChanged, distinctUntilKeyChanged, skip } from 'rxjs/operators';
import { SessionState, SessionStoreService } from '../session-store.service';

@Component({
  selector: 'app-icon-bar',
  templateUrl: './icon-bar.component.html',
  styleUrls: ['./icon-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IconBarComponent implements OnInit, OnDestroy, AfterViewInit {
  @Input() isMobile: boolean;
  @ViewChild('remoteButton') remoteButton: ElementRef;
  remoteStatusObserver: Observable<any>;
  disabled = false;

  showIconbar = false;
  remoteStatus = false;
  remoteStatusSubscription: Subscription = new Subscription();

  subContainer: Subscription = new Subscription();

  vm$: Observable<any>;
  isOwner: boolean = false;

  constructor(
    public a: AuthService,
    public ads: AudioService,
    public ds: DirectshareService,
    private sessionStore: SessionStoreService,
  ) { 
    // make the store view model available
    this.vm$ = this.sessionStore.vm$;
  }

  ngOnInit() {
    // observes the AUDIO remote status, 
    // stored in the firebase session
    /*
    this.remoteStatusObserver = this.ds.getRemoteStatus();
    
    const tmpSub = this.remoteStatusObserver
      .pipe(skip(1)) // ignor the first one, when just opening the room
      .subscribe(() => this.doShowIconbar());
    this.remoteStatusSubscription.add(tmpSub);

    // subscribe to current status and init audio matrix
    if (!this.isOwner ) {
      const tmpSub2 = this.remoteStatusObserver
        .subscribe(val => {
          console.log('doPatchbay: ', val)
          setTimeout(() => this.doPatchBay(val), 1000);
        });
      this.remoteStatusSubscription.add(tmpSub2);
    } else {
      // init audio matrix as owner
      this.doChange(false);
    }
    */

    /**
     * "isOwner" must be changed to "isSender"
     */

    const sessSub = this.sessionStore
      .state$
      .pipe(distinctUntilKeyChanged('remoteStatus'))
      .subscribe((state: SessionState) => {
        // console.log('icon-bar->remoteStatusObserver: ', state.remoteStatus);
        this.isOwner = state.isOwner;
        this.remoteStatus = state.remoteStatus;
        this.doPatchBay(this.remoteStatus);
        // ??? // !this.isOwner ? this.doPatchBay(this.remoteStatus) : this.doChange(false);
      })
    this.subContainer.add(sessSub);
    
  }

  ngAfterViewInit() {
    const el = this.remoteButton.nativeElement;
    const moSub = merge(fromEvent(el, 'mouseover'), fromEvent(el, 'mouseout'))
      .pipe(
        debounce(() => timer(1000))
      )
      .subscribe((val: MouseEvent) => {
        val.type === 'mouseover' ? this.doShowIconbar() : this.doHideIconbar();
      })
    this.subContainer.add(moSub);
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    // this.remoteStatusSubscription.unsubscribe();
    this.subContainer.unsubscribe();
  }

  doClose() {
    this.showIconbar = !this.showIconbar;
  }

  doShowIconbar() {
    this.showIconbar = true;
    setTimeout(() => this.showIconbar = false, 4000);  
  }
  doHideIconbar() {
    setTimeout(() => this.showIconbar = false, 2000);  
  }

  doChange(status) {
    // active for owners of the room only
    this.ds.setRemoteStatus(status);
    // changes invoke patchbay switches
    this.doPatchBay(status);
  }

  // TODO: this should happen in audio service!!
  // sets the audio matrix for owner/coach vs. student
  doPatchBay(status) {
    // Headphone-Mode
    /// console.log('false, false, true')
    if (this.ads.headphoneMode) {
      // coach
      if (this.isOwner) {
        // remote ON
        if (status) {
          // mon/class/mute off/off/on
          this.ads.switchChannelstripAll(false, false, true);
        // remote OFF
        } else {
          // mon/class/mute on/on/off
          this.ads.switchChannelstripAll(true, true, false);
        }
      // student
      } else {
        // remote ON
        if (status) {
          // mon/class/mute on/on/off
          this.ads.switchChannelstripAll(true, true, false);
        // remote OFF
        } else {
          // mon/class/mute off/off/on
          this.ads.switchChannelstripAll(false, false, true);
        }
      }
    // Speaker-Mode
    } else {
      // coach
      if (this.isOwner) {
        // remote ON
        if (status) {
          // mon/class/mute off/off/on
          this.ads.switchChannelstripAll(false, false, true);
        // remote OFF
        } else {
          // mon/class/mute on/off/off
          this.ads.switchChannelstripAll(true, false, false);
        }
      // student
      } else {
        // remote ON
        if (status) {
          // mon/class/mute on/off/off
          this.ads.switchChannelstripAll(true, false, false);

        // remote OFF
        } else {
          // mon/class/mute off/off/on
          this.ads.switchChannelstripAll(false, false, true);
        }
      }
    }
    // console.log('doPatchBay: isOwner, status, headphoneMode, gainObjects', this.isOwner, status, this.ads.gainObjects);
  }

}
