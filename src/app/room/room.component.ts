
// ...
import { Component, OnInit, OnDestroy, ViewChild, NgZone, HostListener } from '@angular/core';
import { OpentokService } from '../shared/opentok.service';
import * as OT from '@opentok/client';
import { AngularFireDatabase } from '@angular/fire/database';
import { Router } from '@angular/router';
import { Observable, fromEvent, Subscription, timer,  combineLatest,  BehaviorSubject, iif } from 'rxjs';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarRef } from '@angular/material/snack-bar';
import { ToClipboard } from '../shared/to-clipboard';
import { DirectshareService } from '../shared/directshare.service';
import { AuthService } from '../shared/auth.service';
import { filter, map, debounceTime, take, distinctUntilChanged, startWith, tap, mergeMap } from 'rxjs/operators';
import { ControllsService } from '../shared/controlls.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import { ThumbsManagerService } from '../shared/thumbs-manager.service';
import { AudioService } from '../shared/audio.service';
import { TranslateService } from '@ngx-translate/core';
import { ParticipantList } from '../my-students/participant-list/participant-list.component';
import { AccountData, FullRoomToken, SessionDelete, SessionState, SessionStoreService, TokenAndSessionId, UserIds } from './session-store.service';
import { SessionService } from './session.service';
import { AccountState, AccountStoreService } from '../shared/account-store.service';
// import { Key } from 'readline';
import { PublisherStoreService } from '../publisher/publisher-store.service';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss'],
  providers: [ 
    ToClipboard,
    SessionStoreService,
    SessionService,
  ]
})
export class RoomComponent implements OnInit, OnDestroy {
  @ViewChild('sidenavRight', { static: true }) sidenavRight;

  @HostListener('window:beforeunload', ['$event'])
  beforeunloadHandler($event) {
    // console.log('beforeunload -> ', $event)
    $event.preventDefault();
    $event.returnValue=true;
    // wait until confirmed, then unload-handler executes deleteRoom
  }
  @HostListener('window:unload', [ '$event' ])
  unloadHandler(event) {
    // …
    this.deleteRoom();
  }

  sidenavContent: any;
  session: any = null; // OT.Session = null;
  streams: Array<OT.Stream> = [];
  room: Observable<any>;
  searching: boolean = true;
  // prefixedRoomToken: string = null;
  fullRoomToken: FullRoomToken = null;
  myTimer: Observable<any> = null;
  myTime: Subscription;
  sessionTimer: any = null;
  isOwner = null; // TODO: or initially as false?
  ownerId: string = null; // take from room subscription
  camStatus = true;
  micStatus = true;
  publisherStatus = false;
  userObj: any;
  userId: any = null;
  role: number = -2;
  userName: string = null;
  roomSubscription: Subscription;
  isPermanentRoom: boolean = false;
  moreThanOneCam$: Observable<any>;
  deviceInfo: any;
  isMobile: boolean;
  layoutGrid: boolean = false;
  screenshareObserver: Observable<boolean>;
  snackBarConfig: MatSnackBarConfig;
  snackBarWaitingConfig: MatSnackBarConfig;
  chatObserver: Observable<boolean>;
  thumbsArr: Array<any>;
  viewMode: string;
  resizeWindow: Observable<Event>;
  maxStreams: number = 0;
  isWaiting: Array<[any]> = [];
  participantsValueArr: ParticipantList['key'][] = [];
  registrationStatus: string;
  subscriptionContainer = new Subscription();
  waitForPublisher$: Observable<string>; //<'waiting' | 'success' | 'error' | 'close'>;
  myTimer$: Observable<number>;
  errorDialogClose$ = new BehaviorSubject(false);

  debug: boolean = false;

  vm$: Observable<any>;
  as$: Observable<any>;

  constructor(
    public ots: OpentokService,
    public tms: ThumbsManagerService,
    private db: AngularFireDatabase,
    private router: Router,
    private clip: ToClipboard,
    public ds: DirectshareService,     
    public snackBar: MatSnackBar,
    public a: AuthService,
    public cs: ControllsService,
    public deviceService: DeviceDetectorService,
    public ats: AudioService,
    private zone: NgZone,
    private translate: TranslateService,
    public sessionStore: SessionStoreService,
    private sessionService: SessionService,
    // private myroomsStore: MyroomsStoreService,
    private accountStore: AccountStoreService,
    public store: SessionStoreService,
    private publisherStore: PublisherStoreService,
  ) {
    // make the store view model available
    this.vm$ = this.store.vm$;
    this.publisherStore.setPublisherStatus(null);

    // OT log levels
    // this was try and error
    // OT.NONE = 0, OT.ERROR = 1, OT.WARN = 2, OT.INFO = 3, OT.LOG = 4, OT.DEBUG = 5, OT.SPAM = 6
    this.ots.setOTLogLevel(1);
    // Push error to generic error handling component
    OT.on('exception', event => {
      console.log('OT exeption: ', event);
      this.ots.errorsEmitter.emit({ code: 'OT_error', event: event });
    });

    // new chat messages pending?
    this.chatObserver = this.cs.chatEmitter
      .pipe(
        map(val => val === 'newChat' ? true : false),
      );

    // custom styling for snackbar
    this.snackBarConfig = new MatSnackBarConfig();
    this.snackBarConfig = {
      panelClass: ['background-orange'],
      duration: 12000,
      verticalPosition: 'top',
    }
    this.snackBarWaitingConfig = new MatSnackBarConfig();
    this.snackBarWaitingConfig = this.snackBarConfig;
    this.snackBarWaitingConfig.duration = 6000;

    this.isMobile = this.deviceService.isMobile();
 
    const rtSub = this.router.events
      .pipe(
        // The "events" stream contains all the navigation events. 
        // It will be one of:
					// - imperative (ie, user clicked a link).
					// - popstate (ie, browser controlled change such as Back button).
					// - hashchange
        filter((event:any) => event.navigationTrigger === 'popstate'),
      )
      .subscribe((event:any) => {
        console.group("browser back event");
        // will act according to role -> close room/disconnect from OT session
        this.deleteRoom();  
      });
    this.subscriptionContainer.add(rtSub);

    // subscribe to available devices and resolve to a boolean "true", if more than 1 cam is available
    this.moreThanOneCam$ = this.ots.deviceList$
      .pipe(
        map(val => val.filter(item => item.kind === 'videoinput').length  > 1 ? true : false),
        distinctUntilChanged(),
      );

    // refresh device list, when devices are changed
    const dcSub = this.ots.deviceChange$
      .subscribe(() => this.ots.refreshDeviceList())
    this.subscriptionContainer.add(dcSub);

    /*
    const reSub = this.sessionStore
      .redirectTo$
      .pipe(filter(val => val !== null))
      .subscribe(val => {
        // console.log('redirect to ', val);
        this.snackBar
          .open( 'Room was closed. Start fresh from Home!', '', { duration: 2000 });
        this.router.navigate([val])
      });
    this.subscriptionContainer.add(reSub);
    */
    this.myTimer$ = timer(0, 10000);
    // 'waiting' | 'success' | 'error' | 'close'
    // wait 10sec before firing the error state
    this.waitForPublisher$ = combineLatest([
      // timer(0, 10000),
      this.myTimer$,
      this.publisherStore.publisherStatus$,
      // use this subject to manually close the publishing error dialog
      this.errorDialogClose$,
    ])
    .pipe(
      // if stream is created everything is fine, so lets ignore that
      filter(val => val[1] !== 'streamCreated'),
      tap(val => console.log(val)),
      map(val => {
        const threshold = 1;
        // if you want to close you can, independent of the two other race conditions
        if (val[2] === true) {
          return 'close'; 
        } else if(val[0] < threshold) {
          return val[1] === 'videoElementCreated' || val[1] === 'streamCreated' ? 'success' : 'waiting';
        } else if (val[0] >= threshold && !val[1] && val[2] === false) {
          return 'error';
        } else {
          return 'success';
        }
      }),
      startWith('waiting'),
      // take(2),
    )

  }

  // init session...
  ngOnInit() {

    // must be first...!!!
    // consolidates important account attributes in sessionStore
    const asSub = this.accountStore
      .state$
      .pipe(
        tap({
          next: (val: AccountState) => {
            // local userId
            this.userId = val.userId;
            const userName = val.role < 0 ? val.nickName + ' (Guest)' ?? 'Guest' : '';
            const data: AccountData = { 
              userId: val.userId, 
              role: val.role,
              nickName: userName, //val.nickName, 
              photoURL: val.photoURL,
            }
            this.sessionStore.setAccountDataBatch(data);
          },
          error: (e) => console.error(e),
        }),
      ).subscribe()
    this.subscriptionContainer.add(asSub);

    this.fullRoomToken = this.sessionService.parseRoomTokenFromURL(this.router.url);
    // console.log('fullRoomToken: ', this.fullRoomToken)
    // this.prefixedRoomToken = this.fullRoomToken.prefixedRoomToken;
    this.sessionStore.setRoomToken(this.fullRoomToken);
    this.sessionStore.sessionListener(this.sessionStore.prefixedRoomToken$); 
    this.sessionStore.getQuota(this.sessionStore.roomToken$);  

    this.sessionStore
      .getUserIds$
      .subscribe((val: UserIds) => {
        this.ds.userId = val.userId;
        this.ds.isOwnerId = val.ownerId;
        this.ds.isOwner = val.isOwner;
        
        // set local vars for legacy
        this.ownerId = val.ownerId;
        this.isOwner = val.isOwner;

        // now initParticipants after all userIds are dispatched
        this.getParticipants();
        this.sessionStore.getParticipants(val.ownerId);
        // TODO: needed??
        this.sessionStore.setParticipantsSessionStatus(val.isOwner);
        this.sessionStore.setInSessionState(true);
      })

    // init session when all core attributes are consolidated
    const sitkSub = this.sessionStore
      .sessionIdAndToken$
      .pipe(filter((val:TokenAndSessionId) => val.token !== null && val.sessionId !== null))
      .subscribe(val => this.initSession(val.sessionId, val.token));
    this.subscriptionContainer.add(sitkSub);

    const staSub = this.sessionStore
      .state$
      .subscribe((val: SessionState) => {
        this.isPermanentRoom = val.isPermanentRoom ?? false;
      });
    this.subscriptionContainer.add(staSub);

    //this.sessionStore
    //  .getParticipants(this.sessionStore.ownerId$)

    const astSub = this.accountStore
      .accountStatus$
      .pipe(
        mergeMap(status => iif(() => status === 'testDrive', this.sessionStore.testTimerState$, null)),
      )
      .subscribe(value => {
        // reminder alert
        switch(value) {
          case 120:
            this.translate.get('room.testTimer.disconnecting-in-120')
              .subscribe(( val: string ) => this.snackBar.open(val, '', this.snackBarConfig));
          break;
          case 3:
            this.translate.get('room.testTimer.test-time-over')
              .subscribe(( val: string ) => {this.snackBar.open(val, '', this.snackBarConfig)});
              // countdown is ready, unsubscribe...
              this.deleteRoom();
          break;
        }
      });
    this.subscriptionContainer.add(astSub);

    

    //  ### old stuff ###
    const micSub = this.ots.controlsEmitter
      .pipe(filter(val => val === 'mic'))
      .subscribe(() => this.micStatus = this.micStatus ? false : true );
    this.subscriptionContainer.add(micSub);

    const camSub = this.ots.controlsEmitter
      .pipe(filter(val => val === 'cam'))
      .subscribe(() => this.camStatus = this.camStatus ? false : true );
    this.subscriptionContainer.add(camSub);

    // handling thumbs
    const tmsSub = this.tms.thumbsEmitter
      .subscribe(() => {
        this.thumbsArr = this.tms.thumbsArrayGet();
        this.viewMode = this.tms.viewModeGet();
      });
    this.subscriptionContainer.add(tmsSub);

    // listen to window resize
    this.resizeWindow = fromEvent(window, 'resize');
    const rezSub = this.resizeWindow
      .pipe(
        debounceTime(500),
      )
      .subscribe( event => this.tms.thumbsEmitter.next({ action: 'windowResize', id: 'foo', event: event }));
    this.subscriptionContainer.add(rezSub);

    // sidenavEmitter
    this.cs.roomSidenavEmitter
      .subscribe(content => this._sidenavRight(content));
  
  // end ngOnInit()
  }

  closeErrorDialog() {
    console.log('invoke close...')
    this.errorDialogClose$.next(true);
  }


  copyToClipboard() {
    const url = this.router.url;
    const fullUrl = location.protocol + '//' + location.hostname + ':' + location.port + url;
    this.translate.get('room.room-url-clipboard')
      .subscribe(( val: string ) => this.snackBar.open( val + ' ' + fullUrl, '', { duration: 4000 } ));
    this.clip.copyToClipboard(fullUrl);
  }

  deleteRoom() {
        
    const derSub = this.sessionStore
      .sessionDelete$
      .subscribe((val: SessionDelete) => {

        if (val.isOwner) {
          // if owner terminates room, send him to home
          this.router.navigateByUrl('/');
    
          // permanent room?
          let trigger = val.isPermanentRoom ? { delete: 'keep' } : { delete: 'delete' };
    
          // write trigger into db
          this.db.database
            .ref(`/now/` + this.fullRoomToken.prefixedRoomToken)
            .update(trigger)
            .catch(error => {
              console.log(error);
            });
    
        } else {
          console.log('delete room - !Owner...');
          this.sessionStore.setInSessionState(false);

          // redirect students to "mycoaches" & anonymous users / non owner coaches to waiting room
          // TODO: students belonging to this coach, should be redirected to mycoaches
          // it should be checked if they are really registered, otherwise??? tbd
          val.myRegistrationStatus === 'isCoachCustomer' ? this.router.navigateByUrl('/mycoaches') : this.router.navigateByUrl(`/${this.fullRoomToken.prefix}/${this.fullRoomToken.roomToken}/waitingroom`);
        }

        // terminate 
        if (this.session) { this.session.disconnect(); }
        // ...
        this.ots.streamEmitter.emit({action: 'stopAVTracks'});

      })
      this.subscriptionContainer.add(derSub);

  }

  // Output event of session-time component
  stopSession() {
    this.deleteRoom();
  }

  initSession(sessionId, token) {
    const self = this;

    console.log('--> init OT session...');
    this.ots.initSession(sessionId, token)
      .then((session: OT.Session) => {
        this.session = session;
        this.session.on('streamCreated', (event) => {
          // console.log('room->initSession->streamCreated: ', this.streams)
          this.streams.push(event.stream);
          this.sessionStore.setSessionState(this.session.currentState);
        });
        this.session.on('streamDestroyed', (event) => {
          // console.log('stream destroyed: ', event.stream);
          const idx = this.streams.indexOf(event.stream);
          if (idx > -1) {
            this.streams.splice(idx, 1);
          }
          this.sessionStore.setSessionState(this.session.currentState);
        });
        this.session.on('sessionDisconnected', (event) => {
          console.log('on: session disconnected...');
          this.sessionStore.setSessionState(this.session.currentState);
          // broadcast terminated session
          this.ots.sessionTrigger.sessionCurrentState = this.session.currentState;
          this.ots.sessionTrigger.session = null;
          this.ots.sessionEmitter.next(this.ots.sessionTrigger);
          // trigger session state for screenSharing controlls
          this.cs.do('controllsComponent', { sessionState: false });
          // relevant for chat
          this.ots.connectionsEmitter.emit(event);
          // TODO: Problem, wenn der participant gerade gelöscht wurde
          this.sessionStore.setInSessionState(false);
        });
        this.session.on('sessionConnected', (event) => {
          // now fire connection emitter
          // console.log('on: session connected...');
          this.ots.connectionsEmitter.emit(event);
          this.sessionStore.setInSessionState(true);
          this.sessionStore.setSessionState(this.session.currentState);
          // this.inSession('connected', event);
          // new approach !!! seems to be too early
          this.publisherStore.setSessionConnected(true);
        });

        this.session.on('streamPropertyChanged', function (event) {
          //console.log('streamPropertyChanged', event.changedProperty, event.newValue, event);
          self.tms.thumbsEmitter.next({ action: 'propertyChange', id: event.stream.streamId, event: event }); 
        });

      })
      .then(() => {
        // console.log('--> connect OT session...');
        this.ots.connect()
          .then(() => {
            console.log('--> OT session connected...');
            
            this.ots.sessionTrigger.sessionCurrentState = this.session.currentState;
            this.ots.sessionTrigger.session = this.session;
            this.ots.sessionEmitter.next(this.ots.sessionTrigger);

            // trigger session state for screenSharing controlls
            this.cs.do('controllsComponent', { sessionState: true });
            this.cs.do('controllsComponent', { publishingState: false });

            // start sessionTimer
            this.sessionService.startSessionTimer();
          });
      })
      .catch((error) => {
        console.error('ots connect error: ', error);
        // alert('Unable to connect. Please try again.');
        this.ots.errorsEmitter.emit({ code: 'roomInitSessionError', event: error });
        const snackRef = this.snackBar.open(
          'Sorry, we can not connect you. Please restart from Home',
          'Got it');
        snackRef.onAction().subscribe(() => {
          console.warn('app-room->initSession->redirect to root');
          // this.router.navigateByUrl('/');
          this.zone.run(() => this.router.navigate(['/']));
        });
      });
  }

  _sidenavRight(content) {
    this.sidenavRight.toggle();
    this.sidenavContent = content;
    if (content === 'chat') {
      this.cs.chatEmitter.emit('chatOpen');
    }
  }

  ngOnDestroy() {
    // ...
    this.subscriptionContainer.unsubscribe();
    /**
     * empty windows array, when leaving room for owner and participants
     * otherwise emoty window will be created, when reentering
     */
    this.ds.windowsArr = [];
    this.sessionService.stopSessionTimer();
  }

  getToggleViewModeIcon() {
    switch(this.viewMode){
      case 'hero':
        return 'border_outer';
      break;
      case 'dual':
        return 'border_vertical';
      break;
      case 'multi':
        return 'border_all';
      break;
      case 'galery':
        return 'view_comfy';
      break;
      default:
        return 'border_outer';
    }
  }

  getParticipants(){

    let snackRef: MatSnackBarRef<any>;

    const participantsObs = this.db
      .list(`classrooms/${this.ownerId}/participants`)
      //.snapshotChanges()
      .stateChanges()
      //.valueChanges()
      .pipe(
        // distinctUntilChanged(),
        map((action: any) => { 
          // console.log('pre action', action);
          const data = {
            type: action.type,
            key: action.key, 
            isWaiting: action.payload.val().isWaiting,
            name: action.payload.val().name,
            access: action.payload.val().access,
          }
          return data;
        }),
        distinctUntilChanged(),
      );
      
    // set up participant listener
    const partSub = participantsObs
      .subscribe(action => {
        // msg owner about waiting customers:
        // only for room owner this is triggered, 
        // when flag isWaiting = true AND access is false (otherwise we shouldn´t bother the coach here)
        // and the record is initially created
        if(this.isOwner && action.isWaiting === true && action.access === false) {
          console.log('###', action)
          // use name or uid
          const name = action.name === null || action.name === undefined ? action.key : action.name
          const found = this.isWaiting.some(item => item === action.key);

          if(!found) {
            this.translate.get('room.waiting-participants.wants-to-join')
              .subscribe(( val: string ) => {
                console.log(val)
                snackRef = this.snackBar.open(`${name} ${val}`, 'Ok', this.snackBarWaitingConfig);
                snackRef.onAction().subscribe(() => {
                  // console.log('The snack-bar action was triggered!');
                  this.db
                    .object(`classrooms/${this.ownerId}/participants/${action.key}`)
                    .update({ access: true });
                });
                this.isWaiting.push(action.key);

              });
          } else {
            console.log('we know that ', action.name, ' is waiting...');
          }
               
        }; 
      
        // delete muted guest participant from mute-list
        if(this.isOwner && action.type === 'child_removed') {
          // splice from 
          const idx = this.isWaiting.findIndex(item => item === action.key);
          this.isWaiting.splice(idx,1);
        }
        
        // msg participant that he has to leave
        if(this.userId === action.key && (action.access === false || action.type === 'child_removed')) {
          //
          this.translate.get('room.waiting-participants.must-leave')
            .subscribe(( val: string ) => {
              this.snackBar.open(`${action.name} ${val}`, '', this.snackBarConfig);
            });
          
          // throws out regarding participant 
          setTimeout(() => this.deleteRoom(), 2000);
        } 
      });
    this.subscriptionContainer.add(partSub);

    // return participantsArr;
    return true;
  }

  isProAudio(){
    const bitrate: number = this.ats.getBitrate();
    return this.ots.getStereoMode() &&  bitrate > 64000 ? true : false;
  }

// end of class
}

