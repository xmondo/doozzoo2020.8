import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { OpentokService } from 'src/app/shared/opentok.service';
import { distinctUntilChanged, throttleTime, takeLast, take, first, distinctUntilKeyChanged } from 'rxjs/operators';
import { Subscription } from 'rxjs';


export interface DialogData {
  errorTitle: string;
  errorHeadline: string;
  errorSuggestion?: string;
  errorCode?: number;
  errorRaw?: string;
}

interface RawError {
  code: string;
  event?: any;
}

@Component({
  selector: 'app-errors',
  templateUrl: './errors.component.html',
  styleUrls: ['./errors.component.css']
})
export class ErrorsComponent implements OnInit, OnDestroy {
  errorsSub: Subscription;
  constructor(
    public dialog: MatDialog,
    public ots: OpentokService,
  ) { 

    this.errorsSub = this.ots.errorsEmitter
      .pipe(
        // throttleTime(1000),
        distinctUntilChanged(),
        // take(1),
        // first(),
        // distinctUntilKeyChanged('code'),
      )
      .subscribe(val => {
        console.log('errorEmitter fire: ', val);
        // this.dispatchError(val);
      });
  }

  ngOnInit() {
    //...
  }

  ngOnDestroy() {
    this.errorsSub.unsubscribe();  
  }

  openDialog(data: DialogData) {
    const dialogRef = this.dialog.open(DialogErrors, {
      width: '60vw',
      data: data,
    });

    dialogRef
      .afterClosed()
      .subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  dispatchError(error:RawError) {
    
    console.log('Error code: ', error.code)
    console.log('Error event: ', error.event)

    // change -> roomInitSessionError | publisherDoGUMError

    const errorData: DialogData = {
      errorTitle: 'We are so sorry, something went wrong...',
      errorHeadline: 'Something unexpected happened, we are sorry for that!',
      errorSuggestion: 'Please try again. If this does not work, close and restart your browser.',
      errorRaw: error.event,
    }

    // simplyfiy approach:
    // parse everything to string and search for dedicated patern to dispatch
    if (typeof error.event === 'string') {
      errorData.errorRaw = error.event;
    } else if (typeof error.event === 'object') {
      errorData.errorRaw = JSON.stringify(error.event);
    }
    
    if( error.code === 'publisherDoGUMError' ) {
      errorData.errorTitle = 'We can\'t access your Camera and/or Microphone';
      errorData.errorHeadline = 'We can\'t access your Camera and/or Microphone. Access is denied.';
      errorData.errorSuggestion = 'Please allow access to these devices. Look out for the camera icon in the header of your browser or check your preferences.'
      this.openDialog(errorData);

    } else if (error.code === 'roomInitSessionError') {
      // 
      errorData.errorHeadline = 'Connection failed, publishing not possible...';
      errorData.errorSuggestion = 'Please try again. If this does not work, close and restart your browser.';
      this.openDialog(errorData);
    }  else if (error.code === 'OT_error' && errorData.errorRaw.indexOf('Unable to Publish') !== -1 ) {
      // generic 
      errorData.errorHeadline = 'Connection failed, publishing not possible...';
      errorData.errorSuggestion = 'Please try again. If this does not work, close and restart your browser.';
      this.openDialog(errorData);
    } 
    /*
    else if (errorData.errorRaw.indexOf('Invalid token format') !== -1 ) {
      // generic 
      errorData.errorHeadline = 'Session authentication failed. Maybe this room was opened already some time ago...';
      errorData.errorSuggestion = 'Please try again. If this does not work, close and restart your browser.';
      this.openDialog(errorData);

    } else if (errorData.errorRaw.indexOf('OT_NOT_CONNECTED') !== -1 ) {
      // generic 
      errorData.errorHeadline = 'Connection failed, publishing not possible...';
      errorData.errorSuggestion = 'Please try again. If this does not work, close and restart your browser.';
      this.openDialog(errorData);

    } else if (errorData.errorRaw.indexOf('Unable to Publish') !== -1 ) {
      // generic 
      errorData.errorHeadline = 'Connection failed, publishing not possible...';
      errorData.errorSuggestion = 'Please try again. If this does not work, close and restart your browser.';
      this.openDialog(errorData);
    } 
    */
    
  }

// end of class
}

@Component({
  selector: 'errors-dialog',
  templateUrl: 'errorsDialogTemplate.html',
  styleUrls: ['./errors.component.css']
})
export class DialogErrors {

  constructor(
    public dialogRef: MatDialogRef<DialogErrors>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
  //...

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

/*
--> camera access
errorData.errorRaw.indexOf('NotAllowedError: Permission denied') !== -1 || errorData.errorRaw.indexOf('DOMException: Permission denied')

// from signal
// signal error (OT_NOT_CONNECTED): Unable to send signal - you are not connected to the session.
*/
