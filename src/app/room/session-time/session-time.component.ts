import { Component, Input, OnInit, Output, EventEmitter, OnChanges } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-session-time',
  templateUrl: './session-time.component.html',
  styleUrls: ['./session-time.component.scss']
})
export class SessionTimeComponent implements OnInit, OnChanges {
  @Input() minutesAvailable: number = 0;
  @Input() plan: string;
  @Output() stopSession: EventEmitter<any> = new EventEmitter();
  showTooltip = false;
  hoverState: boolean = false;
  
  constructor(
    private router: Router,
  ) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes) {
    console.log(changes)
    if(changes.minutesAvailable?.currentValue < 2 && !changes.minutesAvailable?.firstChange) {
      this.showTooltip = true;
    }
    changes.minutesAvailable?.currentValue <= 0 && !changes.minutesAvailable?.firstChange ? this.stopSession.emit() : '';
  }

  doClose() {
    this.showTooltip = !this.showTooltip;
  }

  doShowTooltip() {
    this.hoverState = true;
    const timeout = setTimeout(() => { 
      if (this.hoverState) {
        this.showTooltip = true;
      }
    }, 800);   
  }

  doStopSession() {
    //[routerLink]="'/login'"
    //[queryParams]="{step:'plan'}"
    this.stopSession.emit();
    this.router.navigateByUrl('/login?step=plan');
  }

}
