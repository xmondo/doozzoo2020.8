import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AccountStoreService } from '../shared/account-store.service';

@Injectable({
  providedIn: 'root'
})
export class RoomGuard implements CanActivate {

  constructor(
    private router: Router,
    private accountStore: AccountStoreService,
  ){ }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    // console.log('RoomGuard#canActivate called', state);
    /**
     * check role for beeing not anonymous
     * if true, go to session
     * otherwise redirect to waitingroom
     */
    return this.accountStore
      .role$
      .pipe(
        map(role => role > -2 ? true : false),
        tap(status => !status ? this.router.navigate([state.url + '/waitingroom']) : ''),
      );
  }
}
