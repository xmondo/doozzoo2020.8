/**
 * 
 * @param w: area width
 * @param h: area height
 * @param items: number of items 
 */
export function galeryGrid(_w: number, _h: number, _items: number) {
    // define area and number of items
    const aw = _w;
    const ah = _h;
    const items = _items; //

    let rows; // final rows
    let cols; // final rows
    let overhang; // to stack behind grid area

    const orientation = (aw / ah) > 1 ? 'landscape' : 'portrait';
    //console.log(orientation);

    // iterate for the longest edge,
    // as long as division is > 1
    // i - factor determins how much the original aspect ratio will be kept
    for(let i = 1; (items / i) >= (i);i++){ // better i-1??
        const r = items / i;
    rows = i;
    overhang = items % i;
    // i determins the num of rows
    // console.log(`${r}:${i}`)
    // modulo determins the items to stack behind the r/i grid area
    // console.log('modulo: ', items % i)
    }
    cols = Math.floor(items / rows);
    // console.log('result: ', cols, rows, overhang);


    // per item we now need top left corner position: x, y from 0,0 (+ Basisversatz)
    // and the size of the rectangle

    const overhangWidth = ah / 2; //(aw/ah);
    // console.log(overhangWidth);
    const w = overhang > 0 ? (aw - overhangWidth) / cols : aw / cols;
    const h = ah / rows;

    const itemArr = [];
    for(let i = 0; i < rows; i++) {
        for(let c = 0; c < cols; c++)	{
            const x = c * w;
            const y = i * h;
            const data = {
                left: Math.floor(x), 
                top: Math.floor(y), 
                width: Math.floor(w), 
                height: Math.floor(h)
            }
            itemArr.push(data);
        }	
    }
    const overhangItemHeight = ah / overhang;
    for(let m = 0; m < overhang; m++){
        const x = aw - overhangWidth;
    const y = overhangItemHeight * m;
        const data = {
            left: Math.floor(x), 
            top: Math.floor(y), 
            width: Math.floor(overhangWidth), 
            height: Math.floor(overhangItemHeight)
        }
        itemArr.push(data);	
    }
    // console.log('itemArr: ', itemArr);
    return itemArr;
}

