import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AudioInListComponent } from './audio-in-list.component';

describe('AudioInListComponent', () => {
  let component: AudioInListComponent;
  let fixture: ComponentFixture<AudioInListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AudioInListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AudioInListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
