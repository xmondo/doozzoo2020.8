import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { from, Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

interface DeviceInfo {
  deviceId: string,
  label: string,
}

@Component({
  selector: 'app-audio-in-list',
  templateUrl: './audio-in-list.component.html',
  styleUrls: ['./audio-in-list.component.scss']
})
export class AudioInListComponent implements OnInit {
  @Input() audioInSelected: string;
  @Output() selectAudioIn: EventEmitter<string> = new EventEmitter();
  deviceList$: Observable<DeviceInfo>;
  constructor() { }

  ngOnInit(): void {
    this.deviceList$ = from(navigator.mediaDevices.enumerateDevices())
      .pipe(
        // filter((val:MediaDeviceInfo[] | any) => val.kind === 'audioinput'),
        map((val:any) => val.filter(item => item.kind === 'audioinput').map((item: any) => ({ deviceId: item.deviceId, label: item.label }) )),
      );
  }

  doSelectAudioIn(event): void {
    // console.log('++++', event)
    this.selectAudioIn.next(event.value);  
  }

}
