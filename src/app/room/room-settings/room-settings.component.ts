import { Component, OnInit, Input } from '@angular/core';
import { OpentokService } from 'src/app/shared/opentok.service';
import { AudioService } from 'src/app/shared/audio.service';
import { PublisherStoreService } from 'src/app/publisher/publisher-store.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-room-settings',
  templateUrl: './room-settings.component.html',
  styleUrls: ['./room-settings.component.scss']
})
export class RoomSettingsComponent implements OnInit {
  vm$: Observable<any>;
  debug = false;
  
  constructor(
    public ots: OpentokService,
    private ads: AudioService,
    public publisherStore: PublisherStoreService,
  ) {
    this.vm$ = this.publisherStore.vm$;
   }

  ngOnInit() {
    //...    
  }

  selectAudioOption(event) {
    console.log('selectAudioOption -> value, selected: ', event.option.value, event.option.selected);
    switch(event.option.value) {
      case 'echoCancellation':
        this.publisherStore.setEchoCancellation(event.option.selected);
        break;
      case 'noiseSuppression':
        this.publisherStore.setNoiseSuppression(event.option.selected);
        break;
      case 'autoGainControl':
        this.publisherStore.setAutoGainControl(event.option.selected);
        break;
    }
  }

  setHeadphoneMode(event: boolean) {
    console.log('set headphone mode to: ', event);
    this.publisherStore.toggleHeadphoneMode(event);
  }

  selectVideoIn(deviceId) {
    this.publisherStore.setVideoSource(deviceId);
  }

  selectAudioIn(deviceId) {
    this.publisherStore.setAudioSource(deviceId);
  }

// end of class
}

