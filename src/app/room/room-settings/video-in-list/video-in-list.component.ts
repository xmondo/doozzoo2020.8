import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { from, Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

interface DeviceInfo {
  deviceId: string,
  label: string,
}

@Component({
  selector: 'app-video-in-list',
  templateUrl: './video-in-list.component.html',
  styleUrls: ['./video-in-list.component.scss']
})
export class VideoInListComponent implements OnInit {
  @Input() videoInSelected: string;
  @Output() selectVideoIn: EventEmitter<string> = new EventEmitter();
  deviceList$: Observable<DeviceInfo>;
  constructor() { }

  ngOnInit(): void {
    this.deviceList$ = from(navigator.mediaDevices.enumerateDevices())
      .pipe(
        // filter((val:MediaDeviceInfo[] | any) => val.kind === 'audioinput'),
        map((val:any) => val.filter(item => item.kind === 'videoinput').map((item: any) => ({ deviceId: item.deviceId, label: item.label }) )),
      );
  }

  doSelectVideoIn(event): void {
    // console.log('++++', event)
    this.selectVideoIn.next(event.value);  
  }

}
