import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoInListComponent } from './video-in-list.component';

describe('VideoInListComponent', () => {
  let component: VideoInListComponent;
  let fixture: ComponentFixture<VideoInListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VideoInListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoInListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
