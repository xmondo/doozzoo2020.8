import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ToggleAudioModeComponent } from './toggle-audio-mode.component';

describe('ToggleAudioModeComponent', () => {
  let component: ToggleAudioModeComponent;
  let fixture: ComponentFixture<ToggleAudioModeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ToggleAudioModeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ToggleAudioModeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
