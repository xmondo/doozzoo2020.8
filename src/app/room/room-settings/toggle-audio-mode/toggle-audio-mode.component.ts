import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-toggle-audio-mode',
  templateUrl: './toggle-audio-mode.component.html',
  styleUrls: ['./toggle-audio-mode.component.scss']
})
export class ToggleAudioModeComponent implements OnInit {
  @Input() currentValue: string;
  @Output() setHeadphoneMode: EventEmitter<boolean> = new EventEmitter();
  
  constructor() { }

  ngOnInit(): void {}

  toggleHeadphoneMode(event) {
    // console.log('+++', event);
    // this.setHeadphoneMode.emit(event.value === 'headphoneMode' ? true : false);
    this.setHeadphoneMode.emit(event.value);
  }

}
