import { Component, OnInit, Input } from '@angular/core';
import { OpentokService } from 'src/app/shared/opentok.service';
import { filter, map, debounceTime, take, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from 'src/app/shared/auth.service';
import { environment } from 'src/environments/environment';
// import { FeatureToggleService } from 'src/app/shared/feature-toggle.service';

@Component({
  selector: 'app-achiving',
  templateUrl: './achiving.component.html',
  styleUrls: ['./achiving.component.scss']
})
export class AchivingComponent implements OnInit {
  @Input() session: OT.Session; // OT.Session;
  @Input() userId: string; 
  _session: any = null;
  recordButtonStatus: boolean = false;
  httpFunctionsUrl: string = environment.httpFunctionsUrl;
  archiveEvent: any = null;
  resolution: string = 'normal'; // normal | high

  ft:any;

  constructor(
    public ots: OpentokService,
    private http: HttpClient,
    public a: AuthService,
    // public ft: FeatureToggleService,
  ) { 
    // ...
  }

  ngOnInit() {
    this.ots.sessionEmitter
      .pipe(
        filter((val:any) => val.sessionCurrentState === 'connected'),
        take(1),
      )
      .subscribe(val => {
        // console.log('sessionEmitter: ', val);
        this._session = val.session;
        this._session.on('archiveStarted', event => {
          // ...
          // console.log('archive_started: ', event); 
          this.archiveEvent = event;
          this.recordButtonStatus = true;
        });
        this._session.on('archiveStopped', event => {
          // ...
          // console.log('archive_stopped', event);
          this.recordButtonStatus = false;
        });
      });

    this.a.featureToggles$
      .pipe(
        filter(val => val !== null),
        // take(1)
      )
      .subscribe(val => {
        // console.log('archiving->ft: ', val);
        this.ft = val;
      })
    
  }

  toggleArchive() {
    !this.recordButtonStatus ? this.startArchive() : this.stopArchive();
  }

  startArchive() {
    const url = this.httpFunctionsUrl + 'archiving?action=startarchive&coachid=' + this.userId + '&sessionid=' + this._session.id + '&resolution=' + this.resolution;
    const config = { headers: { 'Accept': 'application/json' } };
    // console.log('start url: ', url);

    this.http.get(url, config)
      .pipe(
        // catchError(this.handleError<Hero[]>('getHeroes', []))
      )
      .subscribe(val => {
        // ...
        // console.log('https: ', val)
      });
    
  }

  
  stopArchive() {
    const url = this.httpFunctionsUrl + 'archiving?action=stoparchive&archiveid=' + this.archiveEvent.id;
    const config = { headers: { 'Accept': 'application/json' } };

    this.http.get(url, config)
      .pipe(
        // ...
      )
      .subscribe(val => {
        // ...
        // console.log('https: ', val)
      });
  }

  getArchive() {
    const url = this.httpFunctionsUrl + 'archiving?action=getarchive&archiveid=' + this.archiveEvent.id;
    const config = { headers: { 'Accept': 'application/json' } };

    this.http.get(url, config)
      .pipe(
        // ...
      )
      .subscribe(val => {
        // ...
        // console.log('https: ', val)
      });
  }

// end of class
}
