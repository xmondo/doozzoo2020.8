import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AchivingComponent } from './achiving.component';

describe('AchivingComponent', () => {
  let component: AchivingComponent;
  let fixture: ComponentFixture<AchivingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AchivingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AchivingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
