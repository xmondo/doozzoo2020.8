import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AudioModeComponent } from './audio-mode.component';

describe('AudioModeComponent', () => {
  let component: AudioModeComponent;
  let fixture: ComponentFixture<AudioModeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AudioModeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AudioModeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
