import { Component, OnInit } from '@angular/core';
import { OpentokService } from 'src/app/shared/opentok.service';

@Component({
  selector: 'app-audio-mode',
  templateUrl: './audio-mode.component.html',
  styleUrls: ['./audio-mode.component.scss']
})
export class AudioModeComponent implements OnInit {
  headphoneMode = true;
  constructor(
    public ots: OpentokService,
  ) { }

  ngOnInit() {
    // ...
    this.headphoneMode = this.ots.getAudioMode();
    // hm = this.translateService.get('room.audioMode.headphone-mode')
  }

}
