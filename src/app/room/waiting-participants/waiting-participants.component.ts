import { Component, Input } from '@angular/core';
import { ControllsService } from 'src/app/shared/controlls.service';

@Component({
  selector: 'app-waiting-participants',
  templateUrl: './waiting-participants.component.html',
  styleUrls: ['./waiting-participants.component.scss']
})
export class WaitingParticipantsComponent {
  @Input() isWaiting: number;
  @Input() haveAccess: number;
  constructor(
    public cs: ControllsService,
  ) { }

}
