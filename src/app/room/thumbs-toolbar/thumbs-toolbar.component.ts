import { Component, OnInit } from '@angular/core';
import { ThumbsManagerService } from 'src/app/shared/thumbs-manager.service';

@Component({
  selector: 'app-thumbs-toolbar',
  templateUrl: './thumbs-toolbar.component.html',
  styleUrls: ['./thumbs-toolbar.component.scss']
})
export class ThumbsToolbarComponent implements OnInit {
  audioState: boolean = true;
  collapseState: boolean = false;
  constructor(
    public tms: ThumbsManagerService,
  ) { }

  ngOnInit() {
  }

  toggleMuteAudio() {
    this.audioState = !this.audioState;
    this.tms.thumbsEmitter.next({ action: 'muteIncommingAudio', id: 'foo', event: this.audioState })
  }

  toggleShowThumbs() {
    this.tms.thumbsEmitter.next({ action: 'collapseThumbs', id: 'foo' })
  }

}
