import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThumbsToolbarComponent } from './thumbs-toolbar.component';

describe('ThumbsToolbarComponent', () => {
  let component: ThumbsToolbarComponent;
  let fixture: ComponentFixture<ThumbsToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThumbsToolbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThumbsToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
