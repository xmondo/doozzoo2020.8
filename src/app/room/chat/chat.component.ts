import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { OpentokService } from 'src/app/shared/opentok.service';
import { ControllsService } from 'src/app/shared/controlls.service';
import { distinctUntilChanged } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';


@Component({
    selector: 'app-chat',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, OnDestroy {
    message: any = null;
    messageArr: Array<any> = [];
    @Input() session: any; // OT.Session;
    @Input() userName: string;
    @Input() user: any;
    connSub: Subscription;
    snackBarConfig: any;
    userIdArr = [];

    constructor(
        private ots: OpentokService,
        private cs: ControllsService,
        public snackBar: MatSnackBar,
    ) { 
        // custom styleing for snackbar
        this.snackBarConfig = new MatSnackBarConfig();
        this.snackBarConfig.panelClass = ['background-orange'];
        this.snackBarConfig.duration = 6000;
        this.snackBarConfig.verticalPosition = 'top';
        // this.snackBarConfig.horizontalPosition = 'right';
    }

    ngOnInit() {
        const self = this;
        // console.log(this.user, this.userName, this.session);
        /*
        this.cs.chatEmitter
            .subscribe(val => {
                if(val === 'chatOpen') {
                    //    
                }
            })
        */
        // will fire when 
        this.connSub = this.ots.connectionsEmitter
            .pipe(
                distinctUntilChanged(),
            )
            .subscribe(val => {
                //console.log('chat->connectionsEmitter', val);
                if(val.type === 'sessionConnected') {
                    this.message = '...joined the session';
                    this.initSessionForSignal();
                    this.sendChat();
                } else if (val.type === 'sessionDisconnected'){
                    this.messageArr = [];    
                }
            })

    }

    ngOnDestroy() {
        this.connSub.unsubscribe();
    }

    initSessionForSignal() {
        const colorsArr = ['#cccccc', '#00af00', '#0099e0', '#808080', '#ae7474', '#00af00', '#0099e0', '#808080', '#ae7474'];
        this.session.on({
            signal: (event) => {
                // console.log("Signal sent from connection " + event.from.id, event);
                // Process the event.data property, if there is any data.
                const connectionArr = [];
                
                this.session.connections.forEach(item => connectionArr.push(item.connectionId));
                let tmp = connectionArr.findIndex(item => event.from.connectionId === item);

                if (tmp !== -1) event.data.connectionColor = colorsArr[tmp];

                event.data.time = Date.now(); // event.from.creationTime;
                // console.log('time: ', event.data.time);
                if (event.data.message.length > 1) {
                    this.messageArr.unshift(event.data); // add at the beginning
                    let msg = event.data.nickname + ': ' + event.data.message;
                    // if(event.data.user_id !== self.user.id) this.openToast(msg);
                    // emit new chat event for signals from others
                    if(event.data.user_id !== this.user) {
                        this.cs.chatEmitter.emit('newChat');  
                        
                        if (this.isFirstTime(event.data.user_id)) {
                            this.snackBar.open( event.data.nickname + ' is connecting...', '', this.snackBarConfig );
                        }
                    }
                }

            }
        });
    }


    isFirstTime(userId){
        let tmp = this.userIdArr.findIndex(item => userId === item);
        if (tmp === -1) {
            this.userIdArr.push(userId);
            return true;    
        } else {
            return false;
        }     
    }

    sendChat() {
        // ...
        const self = this;
        // console.log('send chat');

        this.session.signal(
            {
                data:
                {
                    'message': self.message,
                    'user_id': self.user,
                    'nickname': self.userName,
                    'name': self.userName,
                }
            },
            function (error) {
                if (error) {
                    console.log("signal error (" + error.name + "): " + error.message);
                } else {
                    // console.log("signal sent.");
                }
            }
        );
        // reset let message
        this.message = '';
    }

    // end of class
}

