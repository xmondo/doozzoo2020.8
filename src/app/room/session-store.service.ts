import { Injectable } from '@angular/core';
import { ComponentStore } from '@ngrx/component-store';
import { tap, map, catchError, switchMap, filter, mergeMap, skip, distinctUntilChanged } from 'rxjs/operators';
import { combineLatest, EMPTY, iif, Observable } from 'rxjs';
import { AngularFireDatabase, snapshotChanges } from '@angular/fire/database';
import { ParticipantCheck } from '../apps/toolbar/activate-apps/activate-apps.component';
import { AngularFirestore } from '@angular/fire/firestore';
import { Plan } from '../shared/account-store.service';
import { ParticipantList } from '../my-students/participant-list/participant-list.component';

export interface SessionState {
  isOwner: boolean,
  isPermanentRoom: boolean,
  isSender: boolean,
  maxParticipants: number,
  minutesAvailable: number, // in min
  myroomId: string,
  nickName: string,
  ownerId: string,
  participants: ParticipantList[],
  photoURL: string,
  plan: Plan,
  prefix: string,
  prefixedRoomToken: string,
  publisherStatus: boolean,
  redirectTo: string,
  remoteStatus: boolean,
  role: number,
  roomCountdown: number,
  roomCreationTime: number,
  roomToken: string,
  senderId: string,
  sessionDuration: number, // in sec
  sessionId: string,
  sessionState: string,
  toBeDeleted: boolean,
  token: string,
  userId: string,
  locked: boolean,
}

export interface FullRoomToken {
  prefix: string,
  roomToken: string,
  prefixedRoomToken: string,
}

export interface TokenAndSessionId {
  token: string,
  sessionId: string,
}

export interface UserIds {
  userId: string, 
  ownerId: string, 
  isOwner: boolean,
}

export interface AccountData {
  userId: string, 
  role: number,
  nickName: string, 
  photoURL: string,
}

export interface RoomData {
  ownerId: string,
  sessionId: string,
  token: string,
  roomCreationTime: number,
  roomCountdown: number,
  isPermanentRoom: boolean,
  plan: Plan,
  locked: boolean,
}

export interface SessionDelete {
  isOwner: boolean, 
  isPermanentRoom: boolean, 
  myRegistrationStatus: 'isOwner' | 'isCoachCustomer' | 'isAnonymous' | void,
}

@Injectable()
export class SessionStoreService extends ComponentStore<SessionState> {

  private readonly maxParticipantsPremium: number = 5;
  private readonly minutesPremium: number = 600;

  constructor(
    public db: AngularFireDatabase,
    private afs: AngularFirestore,
  ) {
    super({ 
      isOwner: false,
      isPermanentRoom: false,
      isSender: false,
      maxParticipants: 0,
      minutesAvailable: 0, // in min
      myroomId: null,
      nickName: null,
      ownerId: null,
      participants: [],
      photoURL: 'assets/img/avatars/avataaars-default.png',
      plan: null,
      prefix: null,
      prefixedRoomToken: null,
      publisherStatus: false,
      redirectTo: null,
      remoteStatus: false,
      role: -2,
      roomCountdown: null,
      roomCreationTime: null,
      roomToken: null,
      senderId: null,
      sessionDuration: 0,
      sessionId: null,
      sessionState: null,
      toBeDeleted: false,
      token: null,
      userId: null,
      locked: false,
    });
  }

  // *********** SELECTS ************ //
  public readonly isOwner$: Observable<boolean> = this.select(state => state.isOwner);
  public readonly isPermanentRoom$: Observable<boolean> = this.select(state => state.isPermanentRoom);
  public readonly remoteStatus$: Observable<boolean> = this.select(state => state.remoteStatus);
  public readonly participants$: Observable<ParticipantList[]> = this.select(state => state.participants);
  public readonly participantsKeyList$: Observable<any[]> = this.select(state => {
    return state.participants.filter(item => item.inSession === true ?? false).map(item => item.key);
  });
  public readonly participantIdsToolbarTrue$: Observable<any[]> = this.select(state => {
    return state.participants
      .filter(item => item.toolbarStatus === true)
      .map(item => {
          return { key: item.key, toolbarStatus: item.toolbarStatus };
        }
      );
  });
  // return boolean: was any attribute toolbarStatus set to a value?
  public readonly anyToolbarStatusSet$: Observable<boolean> = this.select(state => {
    return state.participants
      .some(item => item.toolbarStatus !== null && item.toolbarStatus !== undefined);
  });
  // toolbar access is allowed for me as user. Only relevant for not owners
  public readonly myToolbarStatus$: Observable<boolean> = this.select(state => {
    return state.participants.some(item => item.key === state.userId && item.toolbarStatus === true );
  });
  private readonly userId$: Observable<string> = this.select(state => state.userId);
  public readonly ownerId$: Observable<string> = this.select(state => state.ownerId);

  private readonly plan$: Observable<string> = this.select(state => state.plan);
  public readonly role$: Observable<number> = this.select(state => state.role);
  private readonly isSender$: Observable<boolean> = this.select(state => state.isSender);

  private readonly sessionDuration$: Observable<number> = this.select(state => state.sessionDuration);
  private readonly roomCountdown$: Observable<number> = this.select(state => state.roomCountdown);
  public readonly myroomId$: Observable<string> = this.select(state => state.myroomId);
  public readonly roomToken$: Observable<string> = this.select(state => state.roomToken);
  public readonly prefixedRoomToken$: Observable<string> = this.select(state => state.prefixedRoomToken);
  public readonly redirectTo$: Observable<string> = this.select(state => state.redirectTo);
  private readonly token$: Observable<string> = this.select(state => state.token);
  private readonly sessionId$: Observable<string> = this.select(state => state.sessionId);
  public readonly myRegistrationStatus$: Observable< 'isOwner' | 'isCoachCustomer' | 'isAnonymous' | 'isRegistered' |void > = this.select(state => {
    if(state.isOwner) {
      return 'isOwner';
    } else {
      const participant = state.participants.find((item: ParticipantList) => item.key === state.userId);
      return participant ? participant.registrationStatus ?? null : null;
    }
  });

  // ### ViewModel for the component ###
  public readonly vm$ = this.select(
    this.state$,
    this.isSender$,
    this.myToolbarStatus$,
    this.myRegistrationStatus$,
    (state, isSender, myToolbarStatus, myRegistrationStatus) => ({
      state, 
      isSender,
      myToolbarStatus,
      myRegistrationStatus
    })
  );

  // ### ViewModel for sessionIds ###
  public readonly sessionIdAndToken$: Observable<TokenAndSessionId>  = this.select(
    this.token$,
    this.sessionId$,
    (token, sessionId) => ({
      token, 
      sessionId,
    }),
    {debounce: true}
  );

  // ### ViewModel for sessionIds ###
  public readonly getUserIds$: Observable<UserIds>  = this.select(
    this.userId$,
    this.ownerId$,
    this.isOwner$,
    (userId, ownerId, isOwner) => ({
      userId, 
      ownerId,
      isOwner
    }),
    {debounce: true}
  ) // make sure both are dispatched, as this should trigger dependend functionalities
  .pipe(filter((val:UserIds) => val.userId !== null && val.ownerId !== null))

  // ### ViewModel for sessionIds ###
  public readonly testTimerState$: Observable<number>  = this.select(
    this.sessionDuration$,
    this.roomCountdown$,
    (sessionDuration, roomCountdown) => roomCountdown - sessionDuration,
  )
  .pipe(
    skip(3), // as the first emition would return timeover true
    filter(val => val >= 0) // avoid getting negative
  );

  // check whether participant can join or should be redirected due to maxParticipants
  public readonly participantQuotaFull$: Observable<boolean> = this.select(state => {
    return state.participants.filter(participant => participant.inSession === true).length >= state.maxParticipants;
    // const maxParticipants = this.get().plan === 'premium' ? this.maxParticipantsPremium : this.get().maxParticipants;
    // return state.participants.length >= maxParticipants ? true : false;
  });

  // ### select for session delete ###
  public readonly sessionDelete$: Observable<any>  = this.select(
    this.isOwner$,
    this.isPermanentRoom$,
    this.myRegistrationStatus$,
    (isOwner, isPermanentRoom, myRegistrationStatus) => ({
      isOwner, isPermanentRoom, myRegistrationStatus
    }),
    {debounce: true}
  )

  // ### select for waiting-participants button ###
  // this makes the button a more dumb component
  public readonly waitingParticipants$: Observable<{ isWaiting: number, haveAccess: boolean }>  = this.select(state => {
    const isWaiting: number = state.participants.filter(item => item.isWaiting === true).length;
    const haveAccess: boolean = state.participants.some(item => item.access === true);
    return { isWaiting: isWaiting, haveAccess: haveAccess };
  })


  // *********** UPDATERS *********** //
  readonly setRemoteStatus = this.updater((state: SessionState, value: SessionState['remoteStatus']) => ({
    ...state,
    remoteStatus: value || false, 
  }));

  readonly setParticipants = this.updater((state: SessionState, value: SessionState['participants']) => ({
    ...state,
    participants: value, 
  }));

  readonly setOwnerId = this.updater((state: SessionState, value: SessionState['ownerId']) => ({
    ...state,
    ownerId: value, 
  }));

  readonly setSenderId = this.updater((state: SessionState, value: SessionState['senderId']) => ({
    ...state,
    senderId: value, 
  }));

  readonly setQuotas = this.updater((state: SessionState, value: any) => ({
    ...state,
    myroomId: value.key, 
    minutesAvailable: value.minutesAvailable,
    maxParticipants: value.maxParticipants,
  }));

  readonly setIsOwner = this.updater((state: SessionState, value: SessionState['isOwner']) => ({
    ...state,
    isOwner: value, 
  }));

  readonly setUserId = this.updater((state: SessionState, value: SessionState['userId']) => ({
    ...state,
    userId: value, 
  }));

  readonly setIsSender = this.updater((state: SessionState, value: SessionState['isSender']) => ({
    ...state,
    isSender: value, 
  }));

  readonly setPlan = this.updater((state: SessionState, value: SessionState['plan']) => ({
    ...state,
    plan: value ?? null, 
  }));

  readonly setRoomToken = this.updater((state: SessionState, value: FullRoomToken) => ({
    ...state,
    roomToken: value.roomToken,
    prefix: value.prefix,
    prefixedRoomToken: value.prefixedRoomToken,
  }));

  readonly setSessionDuration = this.updater((state: SessionState, value: SessionState['sessionDuration']) => ({
    ...state,
    sessionDuration: value, 
  }));

  readonly setMinutesAvailable = this.updater((state: SessionState, value: SessionState['minutesAvailable']) => ({
    ...state,
    minutesAvailable: value, 
  }));

  readonly setMaxParticipants = this.updater((state: SessionState, value: SessionState['maxParticipants']) => ({
    ...state,
    maxParticipants: value, 
  }));

  readonly setToBeDeleted = this.updater((state: SessionState, value: SessionState['toBeDeleted']) => ({
    ...state,
    toBeDeleted: value, 
  }));

  readonly setRedirectTo = this.updater((state: SessionState, value: SessionState['redirectTo']) => ({
    ...state,
    redirectTo: value, 
  }));
  readonly setSessionId = this.updater((state: SessionState, value: SessionState['sessionId']) => ({
    ...state,
    sessionId: value, 
  }));
  readonly setIsPermanentRoom = this.updater((state: SessionState, value: SessionState['isPermanentRoom']) => ({
    ...state,
    isPermanentRoom: value ?? false, 
  }));
  readonly setToken = this.updater((state: SessionState, value: SessionState['token']) => ({
    ...state,
    token: value, 
  }));
  readonly setSessionState = this.updater((state: SessionState, value: SessionState['sessionState']) => ({
    ...state,
    sessionState: value, 
  }));

  readonly setAccountDataBatch = this.updater((state: SessionState, value: AccountData) => ({
    ...state,
    userId: value.userId, 
    role: value.role,
    nickName: value.nickName, 
    photoURL: value.photoURL,
  }));

  readonly setRoomDefaults = this.updater((state: SessionState, value: RoomData) => ({
    ...state,
    ownerId: value.ownerId,
    sessionId: value.sessionId,
    token: value.token,
    roomCreationTime: value.roomCreationTime,
    roomCountdown: value.roomCountdown,
    isPermanentRoom: value.isPermanentRoom ?? false, // in most cases not set
    plan: value.plan ?? null,
    locked: value.locked ?? false,
  }));

  readonly setPublisherStatus = this.updater((state: SessionState, value: any) => ({
    ...state,
    publisherStatus: value, 
  }));

  // ************ Effects ************ //
  readonly getParticipants = this.effect((ownerId$: Observable<string>) => {
    // set two in one...
    return ownerId$
      .pipe(
        // tap(id => console.log('ownerId: ', id)),
        filter(id => id !== null),
        switchMap((id) => this.db
          .list(
            `classrooms/${id}/participants`, //,
            ref => ref.orderByKey()
            // filter out only those participants beeing in the session
            // ref => ref.orderByChild('inSession')
            // .equalTo(true)
          )
          .snapshotChanges()
          .pipe(
            map((val:any) => val.map(_val => ({ key: _val.key, ..._val.payload.val() }))),
            tap({
              next: (val) => this.setParticipants(val),
              error: (e) => console.log('error', e),
            }),
            // 👇 Handle potential error within inner pipe.
            catchError(() => EMPTY),
          )
        )
      )
  });

  readonly getDirectShare = this.effect((sessionId$: Observable<string>) => {
    return sessionId$
      .pipe(
        switchMap((sessionId) => this.db
          .object(`directShare/${sessionId}`)
          // .snapshotChanges()
          .valueChanges()
          .pipe(
            filter(val => val !== null && val !== undefined),
            /*
            map((val:any) => { 
              console.log('+++ getDirectShare: ', val);
              // console.log('+++ ', val.payload.val().remoteStatus, val.payload.val().senderId);
              // return { remoteStatus: val.payload.val().remoteStatus , senderId: val.payload.val().senderId }
              return null;
            }),
            */
            tap({
              next: (val:any) => {
                this.setSenderId(val.senderId);
                this.setRemoteStatus(val.remoteStatus);
              },
              error: (e) => console.error(e),
            }),
            // 👇 Handle potential error within inner pipe.
            catchError(() => EMPTY),
          ),
        ),
        // 👇 Handle potential error within inner pipe.
        catchError(() => EMPTY),
      )
  });

  /**
   * ...
   */
  readonly getQuota = this.effect((roomToken$: Observable<string>) => {
    // set two in one...
    return roomToken$
      .pipe(
        tap(val => console.log('roomToken: ', val)),
        switchMap((roomToken: string) => {
          return this.afs
            .collection('myRooms', ref => ref
              .where('roomToken', '==', roomToken)
            )
            .valueChanges({idField:'key'})
            .pipe(
              // we can not filter for null, as we have to handle premium!!
              map((snapArr:any) => {
                // const maxParticipants = this.get().plan === 'premium' ? this.maxParticipantsPremium : snapArr[0]?.maxParticipants;
                // const minutesAvailable = this.get().plan === 'premium' ? this.maxParticipantsPremium : snapArr[0]?.maxParticipants;
                return {
                  key: snapArr[0]?.key ?? null,
                  minutesAvailable: snapArr[0]?.minutesAvailable ?? 0,
                  maxParticipants: snapArr[0]?.maxParticipants ?? 0,
                  // maxParticipants: maxParticipants,
                }
              }),
              // tap(val => console.log('session-store->getQuota: ', val)),
              tap({
                next: (val:any) => this.setQuotas({key: val.key, minutesAvailable: val.minutesAvailable, maxParticipants: val.maxParticipants}),
                error: (e) => console.log('error', e),
              }),
              // 👇 Handle potential error within inner pipe.
              catchError(() => EMPTY),
            )
        })
      );
  });

  /**
   * @data$ { item: ParticipantList, checked: boolean }
   * --> sets toolbarstatus for a specific user
   */
  readonly setParticipantApps = this.effect((data$: Observable<ParticipantCheck>) => {
    // set two in one...
    return data$
      .pipe(
        switchMap((data) => this.db
          .object(`classrooms/${this.get().ownerId}/participants/${data.key}`)
          .update({ toolbarStatus: data.checked })
        )
      )
  });

  /**
   * @data$ { item: ParticipantList, checked: boolean }
   * --> sets toolbarstatus for a specific user
   */
  readonly activateFirst = this.effect((data$: Observable<any>) => {
    // set two in one...
    return data$
      .pipe(
        switchMap((data) => {
          const tmpParticipants = this.get().participants.filter(participant => participant.inSession ?? false);
          if(tmpParticipants.length > 0) {
            return this.db
              .object(`classrooms/${this.get().ownerId}/participants/${tmpParticipants[0].key}`)
              .update({ toolbarStatus: true });
          } else {
            return EMPTY;
          }
        })
      )
  });

  /**
   * @participantsKeyList$ SessionState['participants']
   * contains business logic for participant handling
   */
  readonly initParticipantsQuotaFull = this.effect((participantsKeyList$: Observable<SessionState['participants']>) => {
    // set two in one...
    return participantsKeyList$
      .pipe(
        map((participantsKeyList) => {
          // console.log('*** participantsKeyList', participantsKeyList)
          const maxParticipants = this.get().maxParticipants;
          const participantQuotaFull = participantsKeyList.length >= maxParticipants ? true : false;
          return participantQuotaFull;
        }),
        // tap(val => console.log(val, this.get().toBeDeleted)),
        filter(val => !this.get().toBeDeleted),
        tap({
          next: val => {
            /*
            this.db
              .object(`now/${this.get().prefixedRoomToken}/participantQuotaFull`)
              .set(val)
              .catch(error => console.log('initParticipantsQuotaFull error -> ', error))
            */
          },
          error: (e) => console.log('error', e),
        }),
        // 👇 Handle potential error within inner pipe.
        catchError(() => EMPTY),
      )
  });

  // prefixedRoomToken$: Observable<string>
  // { prefixedRoomToken$: this.sessionStore.prefixedRoomToken$, userId$: this.accountStore.userId$ }
  readonly sessionListener = this.effect((prefixedRoomToken$: Observable<string>) => {
    // ...
    return prefixedRoomToken$
      .pipe(
        switchMap((prefixedRoomToken) => {
          return this.db
            .object(`now/${prefixedRoomToken}`)
            .valueChanges()
            .pipe(
              // is empty, redirect, otherwise pipe through
              map(val => {
                val === undefined || val === null ? this.setRedirectTo('/') : val;
                return val;
              }),
              // continue only if value
              filter(val => val !== undefined && val !== null),
              filter((val:any) => val.sessionId ?? false),
              map((val:any) => {
                // this store flag can better trigger delete actions
                const _delete = val.delete === 'delete' ?? false;
                this.setToBeDeleted(_delete);

                // set defaults form session payload
                const roomData: RoomData = {
                  ownerId: val.owner,
                  sessionId: val.sessionId,
                  token: val.token,
                  roomCreationTime: val.timestamp,
                  roomCountdown: val.countdown,
                  isPermanentRoom: val.permanent,
                  plan: val.plan ?? null, 
                  locked: val.locked, 
                }
                this.setRoomDefaults(roomData);
      
                // this.owner = payload.owner;
                // this.isOwner = (this.user === payload.owner) ? true : false;
                const isOwner: boolean = val.owner === this.get().userId;
                this.setIsOwner(isOwner);
                isOwner ? this.setIsSender(true) : '';
                // !!! ### deprechated -> backend takes care of it
                // isOwner ? this.initParticipantsQuotaFull(this.participantsKeyList$) : '';
                // this.getParticipants(val.owner);
                // ### this.sessionStore.setUserId(this.user);
                this.getDirectShare(val.sessionId);
                // this.sessionStore.sessionListener(this.sessionStore.prefixedRoomToken$);
                // for non Owners
                // this.setPlan(val.plan);
                // this.sessionStore.getQuota(this.roomToken);
      
                // init of isSender state to session OWNER
                // if(this.isOwner) {
                  // ### this.sessionStore.setIsSender(true);
                  // andere Lösung ??? this.role < 0 ? this.testTimer(payload) : '';
                  // check max Participants and update now session
                  // ### this.sessionStore.initParticipantsQuotaFull(this.sessionStore.participants$);
                // }

                return val;
              }),

              // tap(val => console.log('++++ sessionListener: ', val)),

              // 👇 Handle potential error within inner pipe.
              catchError(() => EMPTY),
            )
        })
      )
  });

  readonly setParticipantsSessionStatus = this.effect((isOwner$: Observable<boolean>) => {
    return isOwner$
      .pipe(
        switchMap((isOwner) => {
          if(!isOwner) {
            return this.db
              .object(`classrooms/${this.get().ownerId}/participants/${this.get().userId}`)
              .update({ isWaiting: false, inSession: true }); //, access: true }); // TODO: correct access = true???
          } else {
            console.log('you´re the owner...');
            return EMPTY;
          }
        })
      )
  })

  // to be used for all cases???
  readonly setInSessionState = this.effect((inSession$: Observable<boolean>) => {
    return inSession$
      .pipe(
        // tap(val => console.log('session-store->setInSessionState: ', val)),
        mergeMap((inSession) => {
          return this.participantsKeyList$
            .pipe(
              // tap(val => console.log('session-store->setInSessionState->participantsKeyList: ', val)),
              map((participantsKeyList) => participantsKeyList.some(val => val === this.get().userId)),
              distinctUntilChanged(),
              // tap(val => console.log('session-store->participants userfound: ', val)),
              map((userFound:boolean) => userFound && !this.get().isOwner ? this.db.object(`classrooms/${this.get().ownerId}/participants/${this.get().userId}`).update({isWaiting: !inSession, inSession: inSession}) : '')
            )
          }
        )
      );
  })

  /**
   * // find out if user is still in participants list,
    // if not, do not write inSession flag,
    // otherwise this would recreate the entry
    const userFound = this.participantsValueArr.some(val => val.key === this.userId);
    // actions for students/participants
    // if(!this.isOwner && this.user !== 'anonymous' && !this.justRemoved) {
    if(!this.isOwner && userFound) {
      const inSession = status === 'connected' ? true : false;
      const ref = this.db.object(`classrooms/${this.ownerId}/participants/${this.userId}`);
      // TODO: only do update if a FB node is existing
      ref.update({inSession: inSession})
    // actions for owner
    }
   */



// end of class
};

