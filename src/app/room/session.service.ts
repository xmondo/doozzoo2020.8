import { Injectable } from '@angular/core';
import { EMPTY, Subscription, timer } from 'rxjs';
import { catchError, filter, map, mergeMap, startWith, take, tap } from 'rxjs/operators';
import { FullRoomToken, SessionStoreService } from './session-store.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { firestore } from 'firebase/app';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable()
export class SessionService {
  private timer: Subscription = null;
  constructor(
    private sessionStore: SessionStoreService,
    private afs: AngularFirestore,
    private db: AngularFireDatabase,
  ) { }

  sessionTimer() {
    const myTimer = timer(1000, 1000)
      .pipe(
        tap({
          next: (val) => this.sessionStore.setSessionDuration(val),
          error: (e) => console.error(e),
        }),
      )
    return myTimer;
  }

  startSessionTimer() {
    // console.log('start timer...');
    // if timer is invoked several times, it will be resetted 
    if(this.timer !== null) {
      this.timer.unsubscribe();
    } else {
      this.timer = timer(1000, 1000)
        .pipe(
          // start after first minute
          startWith(1),
          mergeMap(counter => this.sessionStore
            .myroomId$
            .pipe(
              map(myroomId => {
              return {counter: counter, myroomId: myroomId};
              })
            )
          )
        )
        .subscribe(val => {
          const decrementTreshold = 60; // seconds
          // console.log('sessionTimer: ', val);
          // this.sessionStore.setSessionTimer(val.counter);
          this.sessionStore.setSessionDuration(val.counter);

          // deprechated: will now be managed by backend
          // trigger decrement according to 60 sec intervals
          // start only after first decrementTreshold interval
          // (val.counter > decrementTreshold-1) && (val.counter % decrementTreshold === 0) ? this.decrementMinutesAvailable(val.myroomId) : '';
        });
    }
  }

  stopSessionTimer() {
    this.timer ? this.timer.unsubscribe() : '';
  }

  testTimer(roomCreationTime: number, roomCountdown: number) {
    const ms = Date.now() - roomCreationTime; // sec since room creation
    const seconds = Math.floor(ms / 1000);
    const countdown = roomCountdown - seconds;

    const testTimer = timer(1000, 1000)
      .pipe(
          take(countdown),
          map(val => countdown - val),
          // 👇 Handle potential error within inner pipe.
          catchError(() => EMPTY),
      );
    return testTimer;
  }

  logSessionTimer(roomToken) {
    this.afs
      .collection('myRooms', ref => ref
        .where('roomToken', '==', roomToken)
      )
  }

  decrementMinutesAvailable(myroomeId: string){
    // const increment = firestore.FieldValue.increment(1);
    const decrement = firestore.FieldValue.increment(-1);
    this.afs
      .doc(`myRooms/${myroomeId}`)
      .update({ minutesAvailable: decrement, modifiedOn: Date.now() });
  }

  parseRoomTokenFromURL(url):FullRoomToken {
    // const url = this.router.url;
    const prefix = url.split('/')[1];
    const roomToken = url.split('/')[2];
    // const temp = fullRoomToken.split('-')[0]; // + '-';
    // const prefix = ['free','plus','pro','edu'].includes(temp) ? temp + '-' : '';
    // const roomToken = fullRoomToken.replace(prefix, '');
    const fullRoomToken = prefix + '_' + roomToken;
    return { prefix: prefix, roomToken: roomToken, prefixedRoomToken: fullRoomToken };
  }


// end of class
}
