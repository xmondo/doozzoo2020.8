import { Component, Input, NgZone, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-myroom-button',
  templateUrl: './myroom-button.component.html',
  styleUrls: ['./myroom-button.component.scss']
})
export class MyroomButtonComponent implements OnInit {
  @Input() roomToken: string;
  @Input() userId: string;
  roomExists: boolean = false;
  roomExists$: Observable<boolean>;
  constructor(
    private router: Router,
    private db: AngularFireDatabase,
    private zone: NgZone,
  ) { }

  ngOnInit(): void {
    this.getRoomState();
    // reactive way
    this.roomExists$ = this.db
      .object(`/now/${this.roomToken}`)
      .valueChanges()
      .pipe(
        map(val => {
          // console.log(val);
          return val === null ? false : true
        })
      )
  }

  // imperative way
  async getRoomState() {
    // first we should check if a session is running
    const currentRoomState = await this.db
      .object(`/now/` + this.roomToken)
      .query
      .once('value');
    console.log('currentRoomState: ', currentRoomState.exists());
    this.zone.run(() => this.roomExists = currentRoomState.exists());
  }

  gotoRoom() {
    this.router.navigateByUrl(`/room/${this.roomToken}`);
  }

  async createMyRoom() {
    const obj = { 
      owner: this.userId,
      locked: false,
      name: this.roomToken, // create a room Name thats human readable 
    };
    // console.log(obj);
    const status = await this.db.object(`/now/${this.roomToken}`)
      .set(obj)
      .catch(error => {
        console.log('set catch error', error);
      });
    console.log('createMyRoom: ', status);
    // redirect to regarding room
    this.router.navigateByUrl(`/room/${this.roomToken}`);

    /*

    // generate a new random token to set up new room
    // no listener, but query once
    this.db.object(`/now/` + this.roomToken)
      .query
      .once('value')
      .then(data => {
        // console.log('data: ', token, data)
        this.data = data;
        if (data) {
          // only dummy data transmitted for triggering Firebase
          const obj: Object = { 
            owner: this.user,
            locked: false,
            name: this.roomToken, // create a room Name thats human readable 
          };
          // console.log(obj);
          this.db.object(`/now/` + this.roomToken)
            .set(obj)
            .then(() => {
              // console.log('success, created obj: ', obj);
            })
            .catch(error => {
              console.log('set catch error', error);
            });
          // redirect to regarding room
          this.router.navigateByUrl('/room/' + this.roomToken);

          // google Analytics
          this.gas.eventEmitter('roomCreated', 'isAnonymous', this.isAnonymous ? 'true' : 'false');

        } else {
          console.log(`FOUND`, data.key);
        }
      })
      .catch(error => {
        console.log('catch error', error);
      });

        */
  }


// end of class
}
