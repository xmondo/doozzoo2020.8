import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyroomButtonComponent } from './myroom-button.component';

describe('MyroomButtonComponent', () => {
  let component: MyroomButtonComponent;
  let fixture: ComponentFixture<MyroomButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyroomButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyroomButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
