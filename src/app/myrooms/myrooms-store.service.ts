import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFirestore } from '@angular/fire/firestore';
import { ComponentStore } from '@ngrx/component-store';
import { timer } from 'rxjs';
import { Observable, EMPTY } from 'rxjs';
import { map, switchMap, tap, catchError, take, filter } from 'rxjs/operators';
import { DeltaTango } from '../shared/delta-tango';
import { MyroomPrefix, MyroomsService } from './myrooms.service';

export interface MyRoom {
  key: string,
  roomToken: string,
  plan: string,
  maxParticipants?: number,
  roomExists?: boolean,
  minutesAvailable?: number, // in minutes
  roomPassword?: string,
}

export interface MyRoomsState {
  myRoomsList: Array<MyRoom>,
  myRoomSelected: MyRoom,
  myRoomSelectedToken: string,
  myRoomSelectedExists: boolean,
  myRoomSelectedEditAllowed: boolean,
  disableUI: boolean,
  prefix: string,
}

@Injectable()
export class MyroomsStoreService extends ComponentStore<MyRoomsState> {
  constructor(
    private deltaTango: DeltaTango,
    private afs: AngularFirestore,
    private db: AngularFireDatabase,
    private myroomsService: MyroomsService,
  ) { 
    super({ 
      myRoomsList: [],
      myRoomSelected: null,
      myRoomSelectedToken: null,
      myRoomSelectedExists: false,
      myRoomSelectedEditAllowed: true,
      disableUI: true,
      prefix: null,
    });

    const disableUI = timer(1000,2000)
      .pipe(
        take(2),
        map(val => val > 0 ? false : true)
      )
    this.setDisableUI(disableUI);
  }

  // *********** Selects ************ //
  private readonly myRoomsList$: Observable<MyRoom[]> = this.select(state => state.myRoomsList);
  public readonly myRoomSelected$: Observable<MyRoom> = this.select(state => state.myRoomSelected);
  public readonly myRoomToken$: Observable<string> = this.select(state => state.myRoomSelectedToken);
  private readonly myRoomSelectedExists$: Observable<boolean> = this.select(state => state.myRoomSelectedExists);
  private readonly myRoomSelectedEditAllowed$: Observable<boolean> = this.select(state => state.myRoomSelectedEditAllowed);
  // *********** Updaters *********** //
  readonly setMyRoomsList = this.updater((state: MyRoomsState, value: MyRoomsState['myRoomsList']) => ({
    ...state,
    myRoomsList: value || [], // maxi as a fallback
  }));
  readonly setMyRoomsSelected = this.updater((state: MyRoomsState, value: MyRoom) => {
    // set prefix
    this.setPrefix(value.plan);
    // set token for more conveience
    this.setMyRoomSelectedToken(value.roomToken);
    // check room exists
    // const fullToken = this.get().prefix + value.roomToken;
    const fullToken = this.myroomsService.getPrefix(value.plan) + '_' + value.roomToken;
    this.roomExistsLookup(fullToken);
    return ({
        ...state,
        myRoomSelected: value, // maxi as a fallback
    })
  });
  readonly myRoomSelectedExists = this.updater((state: MyRoomsState, value: boolean) => ({
    ...state,
    myRoomSelectedExists: value, // maxi as a fallback
  }));
  readonly setDisableUI = this.updater((state: MyRoomsState, value: MyRoomsState['disableUI']) => ({
    ...state,
    disableUI: value, // maxi as a fallback
  }));
  readonly setPrefix = this.updater((state: MyRoomsState, value: MyRoomsState['prefix']) => {
    let prefix = null;
    switch(value) {
      case 'starter':
        prefix = MyroomPrefix.starter;
        break;
      case 'pro':
        prefix = MyroomPrefix.pro;
        break;
      case 'premium':
        prefix = MyroomPrefix.premium;
        break;
      case 'edu':
        prefix = MyroomPrefix.edu;
        break;
    }
    prefix = prefix ? prefix += '/' : null;
    return ({
      ...state,
      prefix: prefix, 
    })
  });
  readonly setMyRoomSelectedToken = this.updater((state: MyRoomsState, value: MyRoomsState['myRoomSelectedToken']) => ({
    ...state,
    myRoomSelectedToken: value, // maxi as a fallback
  }));

  // ViewModel for the component
  public readonly vm$ = this.select(
    this.myRoomsList$,
    this.myRoomSelected$,
    this.myRoomSelectedExists$,
    this.state$,
    (myRoomsList,myRoomSelected,myRoomSelectedExists,state) => ({
      myRoomsList, 
      myRoomSelected,
      myRoomSelectedExists,
      state
    }),
    {debounce: true}, // 👈 setting this selector to debounce
  );

  // *********** Effects *********** //
  /**
   * 
   */
  readonly getMyRooms = this.effect((getMyRoomsData$: Observable<any>) => { //(userId$: Observable<string>) => {
    // set two in one...
    return getMyRoomsData$
      .pipe(
        switchMap((getMyRoomsData) => this.afs
          .collection('myRooms', ref => ref
            //.where('userId', '==', userId)
            .where('userIds', 'array-contains', getMyRoomsData.userId)
          )
          .valueChanges({idField:'key'})
          .pipe(
            // tap(val => console.log('... ', val, getMyRoomsData)),
            map(val => {
              // dont set a random token for starter plan
              if(getMyRoomsData.plan !== 'starter') {
                const randomToken = {
                  key: 'randomToken',
                  roomToken: this.deltaTango.generate(5),
                  plan: 'premium', // covers legacy somehow
                  maxParticipants: 5, // defined for legacy coaches
                }
                return [randomToken, ...val.map((item: MyRoom) => {
                  return { 
                    key: item.key, 
                    roomToken: item.roomToken,
                    maxParticipants: item.maxParticipants,
                    plan: item.plan,
                    minutesAvailable: item.minutesAvailable ?? null,
                    roomPassword: item.roomPassword ?? null,
                  }
                })];
              } else {
                return [...val.map((item: MyRoom) => {
                  return { 
                    key: item.key, 
                    roomToken: item.roomToken,
                    maxParticipants: item.maxParticipants,
                    plan: item.plan,
                    minutesAvailable: item.minutesAvailable ?? null,
                    roomPassword: item.roomPassword ?? null,
                  }
                })];
              }
            }),
            // tap(val => console.log('+++ ', val)),
            // filter 
            // filter(val => val.length >= 1),
            tap({
              next: (val) => {
                this.setMyRoomsList(val);
                // preselect 2 = named room, or if roomlist === 1 the generic room id
                val.length > 1 ? this.setMyRoomsSelected(val[1]) : this.setMyRoomsSelected(val[0]);
              },
              error: (e) => console.error(e),
            }),
            // 👇 Handle potential error within inner pipe.
            catchError(() => EMPTY),
          )
        )
      )

      return 
  });

  /**
   * this checks whether the room is already up and running, e.g. when the coach left for a break...
   */
  readonly roomExistsLookup = this.effect((roomToken$: Observable<string>) => {
    // set two in one...
    return roomToken$
      .pipe(
        switchMap((fullRoomToken: string) => {
          return this.db
            .object(`/now/${fullRoomToken}`)
            .valueChanges()
            .pipe(
              map(val => {
                // console.log('roomExistsLookup ', fullRoomToken, val);
                return val === null ? false : true
              }),
              tap({
                next: (val) => {
                  this.myRoomSelectedExists(val);
                },
                error: (e) => console.error(e),
              }),
              // 👇 Handle potential error within inner pipe.
              catchError(() => EMPTY),
            );
        })
      )
  });

// end of class
}
