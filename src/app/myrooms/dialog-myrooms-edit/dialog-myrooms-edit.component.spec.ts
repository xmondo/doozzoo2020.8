import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogMyroomsEditComponent } from './dialog-myrooms-edit.component';

describe('DialogMyroomsEditComponent', () => {
  let component: DialogMyroomsEditComponent;
  let fixture: ComponentFixture<DialogMyroomsEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogMyroomsEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogMyroomsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
