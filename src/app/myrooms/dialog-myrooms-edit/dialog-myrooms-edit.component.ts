//...

import { Component, Inject, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { startsWith } from 'lodash';
import { combineLatest, from, merge, Observable, of, Subject, Subscription } from 'rxjs';
import { debounceTime, filter, map, startWith, switchMap, tap } from 'rxjs/operators';
import { DeltaTango } from 'src/app/shared/delta-tango';
import { MyroomsEditData } from '../myrooms-edit/myrooms-edit.component';
import { MyRoom, MyroomsStoreService } from '../myrooms-store.service';
import { MyroomsService } from '../myrooms.service';


@Component({
  selector: 'app-dialog-myrooms-edit',
  templateUrl: './dialog-myrooms-edit.component.html',
  styleUrls: ['./dialog-myrooms-edit.component.scss'],
  providers: [
    // both are needed in combination
    DeltaTango,
    MyroomsService,
  ]
})
export class DialogMyroomsEditComponent implements OnInit {
  // config
  readonly maxLength = 24;
  readonly minLength = 6;
  readonly maxLengthPw = 12;
  readonly minLengthPw = 6;

  tagLabel: string;
  subContainer: Subscription = new Subscription();
  roomTokenValue = new FormControl(this.data.roomTokenData.roomToken, [Validators.minLength(this.minLength), Validators.maxLength(this.maxLength)]);
  roomPassword = new FormControl(this.data.roomTokenData.roomPassword, [Validators.minLength(this.minLength), Validators.maxLength(this.maxLength)]);
  
  uniqueRoomName$: Subject<string>;
  uniqueRoomNameLookup$: Observable<boolean>;
  buttonDisabled$: Observable<any>;

  vm$: Observable<any>;

  constructor(
    public dialogRef: MatDialogRef<DialogMyroomsEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: MyroomsEditData,
    private afs: AngularFirestore,
    public myroomsService: MyroomsService,
  ) {
    // this.roomToken = data.roomToken;
    console.log('*** data: ', this.data);
  }

  ngOnInit() {
    const valueSub = this.roomTokenValue
      .valueChanges
      .pipe(
        debounceTime(200),
        filter(val => val !== null && val !== undefined && val.length > 0),
      )
      .subscribe((val: string) => {
        console.log('roomTokenValue IN: ', val)
        // let tempVal = val.toLowerCase();
        // only allow the following cases: a-z A-Z 0-9 -
        // and directly lowercase and truncate gth maxLength
        const tempVal = val.replace(/[^a-zA-Z0-9-]/gm, '').toLowerCase().substr(0,this.maxLength);
        
        // IMPORTANT: emitEvent: false prevents feedback loop
        this.roomTokenValue.setValue(tempVal, {emitEvent: false}); 
        this.uniqueRoomName$.next(tempVal);
      })
    this.subContainer.add(valueSub);

    const pwSub = this.roomPassword
      .valueChanges
      .pipe(
        debounceTime(200),
        filter(val => val !== null && val !== undefined && val.length > 0),
      )
      .subscribe((val: string) => {
        console.log('roomPassword IN: ', val)
        // let tempVal = val.toLowerCase();
        // only allow the following cases: a-z A-Z 0-9 -
        // and directly lowercase and truncate gth maxLength
        const tempVal = val.replace(/[^a-zA-Z0-9-]/gm, '').toLowerCase().substr(0,this.maxLengthPw);
        
        // IMPORTANT: emitEvent: false prevents feedback loop
        this.roomPassword.setValue(tempVal, {emitEvent: false}); 
      })
    this.subContainer.add(pwSub);

    this.initUniqueRoomNameSearch();

    // use value changes as trigger to then evaluate 
    // whether values have been changes and are valid
    this.buttonDisabled$ = merge(
      // this.uniqueRoomNameLookup$,
      this.roomTokenValue.valueChanges,
      this.roomPassword.valueChanges,
      // of(this.roomTokenValue.dirty),
      // of(this.roomPassword.valid),
      // of(this.roomPassword.dirty),
    )
    .pipe(
      switchMap(() => combineLatest([
        // should be different from current token
        // of(this.roomTokenValue.valid && this.roomTokenValue.value !== this.data.roomTokenData.roomToken),
        of(this.roomTokenValue.valid && this.roomTokenValue.value !== this.data.roomTokenData.roomToken),
        of(this.roomTokenValue.dirty),
        // !! also "null" is valid = no password
        of(this.roomPassword.valid),
        of(this.roomPassword.dirty),
      ])
      .pipe(
        // ...
        tap(val => console.log('### ', val)),
        map(val => val[1] || val[3] ? ((val[0] || this.roomTokenValue.value === this.data.roomTokenData.roomToken) && val[2]) : false),
      ))
    );
    // this.buttonDisabled$.subscribe(val => console.log('buttonDisabled$: ', val))
  }

  cancel(): void {
    this.dialogRef.close(null);
  }

  confirm(): void {
    // console.log('confirm');
    const data: MyRoom = {
      key: this.data.roomTokenData.key,
      roomToken: this.roomTokenValue.value,
      plan: this.data.roomTokenData.plan,
      roomPassword: this.roomPassword.value,
    }
    console.log('*** confirm: ', data);
    this.dialogRef.close(data);
  }

  initUniqueRoomNameSearch() {
    const originalRoomName = this.data.roomTokenData.roomToken;
    this.uniqueRoomName$ = new Subject();
    this.uniqueRoomNameLookup$ = this.uniqueRoomName$
      .pipe(
        filter(val => val !== originalRoomName),
        tap(val => console.log('+++', val)),
        switchMap((roomToken: string) => this.afs
          .collection('myRooms', ref => ref
            .where('roomToken', '==', roomToken)
          )
          .valueChanges()
          .pipe(
            // tap(val => console.log('tap before->(val): ', val)),
            map(val => val.length > 0 ? false : true),
            tap(val => console.log('tap->(roomToken, val): ', roomToken, val)),
          )
        ),
        startWith(true),
      )
  }

  resetRoomPassword() {
    // ...
    // this.roomPassword.markAsPristine();
    // markAsDirty must be first
    this.roomPassword.markAsDirty();
    this.roomPassword.setValue(null);
  }

// end of class
}

