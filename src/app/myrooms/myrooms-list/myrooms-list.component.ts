import { Component, Input, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { from } from 'rxjs';
import { Observable, of, timer } from 'rxjs';
import { debounce, debounceTime, filter, map, startWith, take } from 'rxjs/operators';
import { Plan } from 'src/app/shared/account-store.service';
import { UserData } from 'src/app/shared/auth.service';
import { DeltaTango } from 'src/app/shared/delta-tango';
import { DialogMyroomsEditComponent } from '../dialog-myrooms-edit/dialog-myrooms-edit.component';
import { MyRoom, MyroomsStoreService } from '../myrooms-store.service';
import { MyroomsService } from '../myrooms.service';

interface btnConf { 
  lg: string; 
  gtxs: string;
  xs: string;
  class: string; 
};

@Component({
  selector: 'app-myrooms-list',
  templateUrl: './myrooms-list.component.html',
  styleUrls: ['./myrooms-list.component.scss'],
  providers: [ 
    DeltaTango,
    MyroomsStoreService,
    MyroomsService,
  ],
})
export class MyroomsListComponent implements OnInit {
  @Input() userData: UserData;
  @Input() layout: string = 'button';
  @Input() buttonConfig: string = 'default';
  vm$: Observable<any>
  disableUI: Observable<boolean>;
  btnConf: btnConf;

  debug: boolean = false;

  constructor(
    private db: AngularFireDatabase,
    private router: Router,
    public myRoomsStore: MyroomsStoreService,
    private myroomsService: MyroomsService,
    private dialog: MatDialog,
    private afs: AngularFirestore,
  ) {
    // make the store view model available
    this.vm$ = this.myRoomsStore.vm$;
  }

  async ngOnInit() {
    // do it in the store
    this.myRoomsStore
      .getMyRooms({
        userId: this.userData.uid,
        plan: this.userData.plan
      });

    this.disableUI = timer(1000,2000)
      .pipe(
        take(2),
        map(val => val > 0 ? false : true)
      )

    switch(this.buttonConfig) {
      case 'center':
        this.btnConf = { lg: '30%', gtxs: '50%', xs: '95%', class: 'tangoFAB' };
        break;
      case 'right':
        this.btnConf = { lg: '30%', gtxs: '50%', xs: '95%', class: 'tangoFABRight' };
        break;
      default:
        this.btnConf = { lg: '100%', gtxs: '100%', xs: '100%', class: 'default' };
        break;
    }
  }

  async createRoomName(){
    const tempRoomToken = this.userData.nickName;
    const status = await this.myroomsService
      .createMyRoom(this.userData.uid, 'coach', Plan.premium , tempRoomToken);
    console.log('*** createRoomName: ', status);
  }

  goToMyRoom(roomTokenData) {
    const token = roomTokenData.roomToken
    const prefix = roomTokenData.plan ? this.myroomsService.getPrefix(roomTokenData.plan) : '';
    this.router.navigateByUrl(`/${prefix}/${token}`);
  }

  async createMyRoom(roomTokenData) {
    const token = roomTokenData.roomToken;
    const prefix = roomTokenData.plan ? this.myroomsService.getPrefix(roomTokenData.plan) : '';
    const obj = { 
      owner: this.userData.uid,
      myroomId: roomTokenData.key,
      maxParticipants: roomTokenData.maxParticipants,
      name: `${prefix}_${token}`, // create a room Name thats human-readable 
      // due to legacy this can be empty
      plan: this.userData.plan ?? null,
    };
    
    // console.log(obj);
    if(prefix !== undefined && token !== undefined) {
      await this.db
        .object(`/now/${prefix}_${token}`)
        .set(obj)
        .catch(error => {
          console.log('createMyRoom set error: ', error);
        });
      // redirect to regarding room
      this.router.navigateByUrl(`/${prefix}/${token}`);
      return 'room created and session joined';
    } else {
      return 'could not create room...';
    }
    
  }

  /**
   * 
   * @param roomTokenData 
   * key: string,
   * roomToken: string,
   * roomExists?: boolean,
   * minutesAvailable?: number, // in minutes
   */
  editMyRoom(roomTokenData: MyRoom, prefix: string) {
    const myDialogRef = this.dialog.open(DialogMyroomsEditComponent, {
      width: '450px',
      minWidth: '350px',
      data: { 
        type: 'myroomsEdit',
        roomTokenData: roomTokenData,
        prefix: prefix,
      }
    });
    myDialogRef.afterClosed()
      .pipe(filter(val => val !== null))
      .subscribe((result: MyRoom) => this.saveRoomNameModified(result));
  }

  saveRoomNameModified(myRoomData: MyRoom) {
    const data = myRoomData.roomPassword ? {roomToken: myRoomData.roomToken, roomPassword: myRoomData.roomPassword } : {roomToken: myRoomData.roomToken };
    this.afs
      .collection('myRooms')
      .doc(myRoomData.key)
      .update({roomToken: myRoomData.roomToken, roomPassword: myRoomData.roomPassword })
      .catch(error => console.log(error));
    // select now modified room
    setTimeout(() => this.myRoomsStore.setMyRoomsSelected(myRoomData), 1000);
  }

  // ...
  copySuccess(event) {
    console.log(event)
  }

  parseUrlPrefix() {
    const url = this.router.url;
    return location.port ? `${location.protocol}//${location.hostname}:${location.port}/room/` : `${location.protocol}//${location.hostname}/room/`;
  }

// end of class
}
