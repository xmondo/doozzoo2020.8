import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyroomsListComponent } from './myrooms-list.component';

describe('MyroomsListComponent', () => {
  let component: MyroomsListComponent;
  let fixture: ComponentFixture<MyroomsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyroomsListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyroomsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
