import { TestBed } from '@angular/core/testing';

import { MyroomsService } from './myrooms.service';

describe('MyroomsService', () => {
  let service: MyroomsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MyroomsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
