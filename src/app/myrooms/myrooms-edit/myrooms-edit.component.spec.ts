import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyroomsEditComponent } from './myrooms-edit.component';

describe('MyroomsEditComponent', () => {
  let component: MyroomsEditComponent;
  let fixture: ComponentFixture<MyroomsEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyroomsEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyroomsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
