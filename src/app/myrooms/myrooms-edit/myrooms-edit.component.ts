import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogMyroomsEditComponent } from '../dialog-myrooms-edit/dialog-myrooms-edit.component';

export interface MyroomsEditData {
  type: string,
  roomTokenData: any,
}

@Component({
  selector: 'app-myrooms-edit',
  templateUrl: './myrooms-edit.component.html',
  styleUrls: ['./myrooms-edit.component.scss']
})
export class MyroomsEditComponent implements OnInit {
  @Input() roomToken: string;
  constructor(
    public dialog: MatDialog,
  ) { }

  ngOnInit(): void {
  }

  editMyRoom() {
    const myDialogRef = this.dialog.open(DialogMyroomsEditComponent, {
      width: '750px',
      minWidth: '350px',
      data: { 
        type: 'myroomsEdit',
        roomToken: this.roomToken 
      }
    });
  }

}
