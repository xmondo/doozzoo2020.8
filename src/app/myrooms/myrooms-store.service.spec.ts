import { TestBed } from '@angular/core/testing';

import { MyroomsStoreService } from './myrooms-store.service';

describe('MyroomsStoreService', () => {
  let service: MyroomsStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MyroomsStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
