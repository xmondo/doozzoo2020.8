import { Injectable, NgZone } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { map, subscribeOn, switchMap, tap } from 'rxjs/operators';
import { Plan } from '../shared/account-store.service';
import { DeltaTango } from '../shared/delta-tango';

export const planQuota = {
  starter: {
    minutesAvailable: 100,
    maxParticipants: 1,
  },
  pro: {
    minutesAvailable: 400,
    maxParticipants: 5,
  },
  premium: {
    minutesAvailable: 600,
    maxParticipants: 5,
  },
}

export const enum MyroomPrefix {
  starter = 'free',
  pro = 'go',
  premium = 'pro',
  edu = 'edu',
}

export interface MyRoomDb {
  roomToken: string,
  userIds: Array<string>,
  createdOn: number, // timestamp
  modifiedOn: number, // timestamp
  minutesAvailable: number,
  maxParticipants: number,
  type?: 'coach' | 'institution',
  roomPassword?: string,
}

@Injectable()
export class MyroomsService {

  constructor(
    private afs: AngularFirestore,
    private deltaTango: DeltaTango,
    private zone: NgZone,
  ) {}

  /**
   * 
   * @param userId 
   * @param type -> coach | institution
   * @param roomName -> string
   * 
   * we must make sure that roomname is unique
   * testcases:
   * - plan, y/n
   * - roomName, y/n
   * - combinations
   * - TODO: create unit tests
   */
  async createMyRoom(
    userId: string, 
    type: MyRoomDb['type'], 
    plan: Plan = null, 
    roomName?: string,
    roomPassword?: string,
  ): Promise<string | 'roomtokenInUse' | 'starterPlanFull' | void> {

    console.log('*** createMyRoom: ', userId, type, plan, roomName);

    // for all plans with only one room as limit
    // to cover the case of an upgrade, we delete any free rooms with this userId
    // const result = plan === 'premium' ? await this.deleteExistingFreePro(userId) : '';
    // console.log('### createMyRoom: ', result);

    const roomToken = await this.generateUnique(plan,roomName);  
    const roomCount = await this.myroomsCountPromise(userId);
    
    // threshold for starter plan is 1 room
    if(plan === 'starter' && roomCount > 0) {
      // no way, you already have a room
      // this covers also the case of trying to update to a payed plan, 
      // but then returning back before the final subscription has been triggered 
      return 'starterPlanFull'; 
    // stop here if token is not unique
    } else if (roomToken === 'roomtokenInUse') {
      return 'roomtokenInUse'; // no way, this token is already taken
    } else {
      const data = {
        roomToken: roomToken,
        userIds: [userId], // TODO: create multiple at once?
        createdOn: Date.now(), // timestamp
        modifiedOn: Date.now(), // timestamp
        type: type,
        roomPassword: roomPassword ?? null,
        // set regarding quotas from prefs
        minutesAvailable: planQuota[plan].minutesAvailable,
        maxParticipants: planQuota[plan].maxParticipants,
        plan: plan ?? null,
      }

      await this.afs
        .collection('myRooms')
        .add(data )
        .catch(e => console.log(e));
      return roomToken;
    }
  }

  async generateUnique(plan: Plan, roomName?: string): Promise<string | 'roomtokenInUse' | void> {

    let roomToken = roomName === undefined ? this.deltaTango.generate(4) : roomName;
    
    /*
    // if plan was provided, apply prefix
    if(plan) {
      switch(plan) {
        case 'starter':
          roomToken = MyroomPrefix.starter + '-' + roomToken;
          break;
        case 'pro':
          roomToken = MyroomPrefix.pro + '-' + roomToken;
          break;
        case 'premium':
          roomToken = MyroomPrefix.premium + '-' + roomToken;
          break;
      }
    }
    */

    const isUnique = await this.afs
        .collection('myRooms', ref => ref
          .where('roomToken', '==', roomToken)
          .where('plan', '==', plan)
        )
        .get()
        .toPromise()

    // console.log('isUnique.length, roomToken: ', isUnique.size, roomToken)

    // if roomname was provided, return 
    if(roomName){
      return isUnique.size > 0 ? 'roomtokenInUse' : roomToken;
    } else {
      if(isUnique.size > 0) {
        this.generateUnique(plan);
      } else {
        return roomToken;
      }
    }

  }

  myroomsCount(userId:string): Observable<number> {
    return this.afs
      .collection('myRooms', ref => ref
        // .where('userId', '==', userId)
        .where('userIds', 'array-contains', userId)
      )
      .valueChanges()
      .pipe(
        map(val => val.length ?? 0),
        tap(val => console.log('*** myroomsCount: ', val)),
      );
  }

  async findExisting(userId:string): Promise<number | any[]> {
    return this.afs
      .collection('myRooms', ref => ref
        .where('userIds', 'array-contains', userId)
      )
      .get()
      .pipe(
        map(val => val.docs.map(item => item.id) ?? []),
        tap(val => console.log('*** findExisting: ', val)),
      )
      .toPromise();
  }

  async deleteExisting(userId: string) {
    of(userId)
      .pipe(
        switchMap(userId => this.afs
          .collection('myRooms', ref => ref
            .where('userIds', 'array-contains', userId)
          )
          .get()
          .pipe(
            map(val => val.docs.map(item => item.id) ?? []),
            map(val => {
              val.forEach(item => this.afs.collection('myRooms').doc(item).delete())
            }),
            // tap(val => console.log('*** deleteExisting: ', val)),
          )
        )
      )
      .toPromise()
  }

  async deleteExistingFreePro(userId: string) {
    of(userId)
      .pipe(
        switchMap(userId => this.afs
          .collection('myRooms', ref => ref
            .where('userIds', 'array-contains', userId)
            .where('plan', 'in', ['starter','pro'])
          )
          .get()
          .pipe(
            map(val => val.docs
              .map(item => item.id) ?? []),
            map(val => {
              console.log('&&& ', val);
              val.forEach(item => this.afs.collection('myRooms').doc(item).delete())
            }),
            tap(val => console.log('*** deleteExisting: ', val)),
          )
        )
      )
      .toPromise()
  }
  
  // TODO: should be reworked to reactive approach
  async myroomsCountPromise(userId:string): Promise<any> {
    return this.afs
      .collection('myRooms', ref => ref
        .where('userIds', 'array-contains', userId)
      )
      .get()
      .pipe(
        map(val => val.size ?? 0)
      )
      .toPromise();
  }

  getPrefix(plan: string) {
    let prefix = null;
    switch(plan) {
      case 'starter':
        prefix = MyroomPrefix.starter;
        break;
      case 'pro':
        prefix = MyroomPrefix.pro;
        break;
      case 'premium':
        prefix = MyroomPrefix.premium;
        break;
      case 'edu':
        prefix = MyroomPrefix.edu;
        break;
      case null:
        prefix = null; 
    }
    // return prefix ? prefix += '_' : null;
    return prefix;
  }

// end of class
}
