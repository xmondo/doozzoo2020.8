/// <reference lib="webworker" />

// bad hack... :-/
declare var require: any;
const lamejs = require('lamejs');

// declare function importScripts(...urls: string[]): void;

// data feeded with channelDataArray
addEventListener('message', ({data}) => {
  // console.log('encodeMp3 worker started.', data);
  // const response = `worker response to ${data}`;

  const kbps = 128; // _kbps || 128; // encode 128kbps mp3
  const sampleRate = data.sampleRate || 44100; // 44100; // _sampleRate || 44100; // 44.1khz (normal mp3 samplerate)
  // can be anything but make it a multiple of 576 to make encoders life easier
  const sampleBlockSize = 1152; // _sampleBlockSize || 1152;
  // output container
  let blob;

  const channelDataArray = data.channelDataArray;

  // self.importScripts('https://cdnjs.cloudflare.com/ajax/libs/lamejs/1.2.0/lame.min.js');
  const mp3encoder = new lamejs.Mp3Encoder(channelDataArray.length, sampleRate, kbps);

  // helper function
  function floatTo16BitPCM(input, output) {
      for (let i = 0; i < input.length; i++) {
      const s = Math.max(-1, Math.min(1, input[i]));
      output[i] = (s < 0 ? s * 0x8000 : s * 0x7FFF);
      }
  }
  // helper function
  function convertBuffer(_data){
      // let data = new Float32Array(arrayBuffer);
      // let data = data;
      // console.log(data)
      const out = new Int16Array(_data.length);
      // console.log(out)
      floatTo16BitPCM(_data, out);
      // console.log(out)
      return out;
  }

  try{

      let mp3Data = [];
      let mp3buf;
      let channelData = [];

      for (let i = 0; i < channelDataArray.length; i++) {
          channelData.push(convertBuffer(channelDataArray[i]));
      }

      for (let i = 0; i < channelData[0].length; i += sampleBlockSize) {
          // array containing left/right
          const channelDataChunks = [];
          for (let n = 0; n < channelData.length; n++) {
              channelDataChunks.push(channelData[n].subarray(i, i + sampleBlockSize));
          }

          if (channelData.length === 1){ 
              mp3buf = mp3encoder.encodeBuffer(channelDataChunks[0]);
          }
          if (channelData.length === 2) {
              mp3buf = mp3encoder.encodeBuffer(channelDataChunks[0], channelDataChunks[1]); 
          }

          if (mp3buf.length > 0) {
              mp3Data.push(mp3buf);
          }
      }

      mp3buf = mp3encoder.flush();   //finish writing mp3

      if (mp3buf.length > 0) {
          mp3Data.push(new Int8Array(mp3buf));
      }

      // free memory
      channelData = null;
      data = null;

      blob = new Blob(mp3Data, {type: 'audio/mp3'});
      // return blob;

      postMessage(blob);

  } catch (error) {
      console.log (error);
      // postMessage(error);
  }
  
});

