/// <reference lib="webworker" />

// data feeded with channelDataArray
addEventListener('message', ({data}) => {
    // console.log('tuner worker started.');
    const response = `worker response to ${data}`;
    const input = data;

    // console.log('tuner worker started: ', input);

    // Allocate required libraries (local or remote)
    // Since the host may change, we'll request it from the caller
    // importScripts(`${input.protocol}//${input.host}/scripts/xlsx.full.min.js`);

    const timeseries = input.timeseries;
    const test_frequencies = input.test_frequencies;
    const sample_rate = input.sample_rate;

    // 2pi * frequency gives the appropriate period to sine.
    // timeseries index / sample_rate gives the appropriate time coordinate.
    const scale_factor = 2 * Math.PI / sample_rate;
    const amplitudes = test_frequencies.map
    (
        function(f) {
            const frequency = f.frequency;

            // Represent a complex number as a length-2 array [ real, imaginary ].
            const accumulator = [ 0, 0 ];
            for (let t = 0; t < timeseries.length; t++) {
                accumulator[0] += timeseries[t] * Math.cos(scale_factor * frequency * t);
                accumulator[1] += timeseries[t] * Math.sin(scale_factor * frequency * t);
            }

            return accumulator;
        }
    );
    // return amplitudes;

    const frequency_amplitudes = amplitudes;

    // Compute the (squared) magnitudes of the complex amplitudes for each
    // test frequency.
    const magnitudes = frequency_amplitudes.map(function(z) { return z[0] * z[0] + z[1] * z[1]; });

    // Find the maximum in the list of magnitudes.
    let maximum_index = -1;
    let maximum_magnitude = 0;
    for (let i = 0; i < magnitudes.length; i++) {
        if (magnitudes[i] <= maximum_magnitude) {
            continue;
        }
        maximum_index = i;
        maximum_magnitude = magnitudes[i];
    }

    // Compute the average magnitude. We'll only pay attention to frequencies
    // with magnitudes significantly above average.
    const average = magnitudes.reduce(function(a, b) { return a + b; }, 0) / magnitudes.length;
    const confidence = maximum_magnitude / average;
    const confidence_threshold = 3; // 10 ⁄ empirical, arbitrary.
    // console.log(average, confidence);
    
    // only post if confidencelevel above threshold
    if (confidence > confidence_threshold) {
        const dominant_frequency = test_frequencies[maximum_index];
        const result = {
            note: dominant_frequency.name,
            pitch: Math.round(dominant_frequency.frequency),
            flag: dominant_frequency.flag
        };
        // console.log('tuner worker: ', result);
        postMessage(result);
    }

});
