import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { AngularFireDatabase, AngularFireObject } from '@angular/fire/database';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-detail-users',
  templateUrl: './detail-users.component.html',
  styleUrls: ['./detail-users.component.css']
})
export class DetailUsersComponent implements OnInit {
  userId: string;
  studentListRef: any;
  studentList: any;
  localProgress: boolean = false;
  nothingfound: any;
  userData: any;
  httpFunctionsUrl: string = environment.httpFunctionsUrl;

  constructor(
    private route: ActivatedRoute,
    public db: AngularFireDatabase,
  ) { }

  ngOnInit() {
    this.userId = this.route.snapshot.paramMap.get('userId');

    this.userData = this.db
      .object('users/' + this.userId)
      .valueChanges();

    this.getStudentsArr();
  }

  getStudentsArr() {
    this.studentListRef = this.db
      .list(
        'coachCustomer/' + this.userId,
        ref => ref.orderByKey()
      );
    this.studentList = this.studentListRef
      .snapshotChanges()
      // .valueChanges()
      // .stateChanges()
      .pipe(
        map((changes: any) =>
          // mapping the object to the relevant values
          changes.map(_changes => ({ key: _changes.key, value: _changes.payload.val() }))
        )
      );
    this.localProgress = false;
  }

}
