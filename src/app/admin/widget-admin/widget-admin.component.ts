import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { AuthService } from '../../shared/auth.service';

interface Widget {
  id: string;
  timeout: number;
  show: string; // show | hide
  content: {
    header: string;
    body: string;
    date: string;
    url?: string;
  }
}

@Component({
  selector: 'app-widget-admin',
  templateUrl: './widget-admin.component.html',
  styleUrls: ['./widget-admin.component.css']
})
export class WidgetAdminComponent implements OnInit {
  header: string;
  body: string;
  widgetShow: string;;
  widgetTimeout: number = 4000;
  widgetContent: any = {
    header: null,
    body: null,
    date: null,
    url: null,
    imgLink: null,
  };
  widgetId: string;
  hover: boolean = false;
  wdgSub: Subscription;

  constructor(
    private db: AngularFireDatabase,
  ) { }

  async ngOnInit() {
    this.wdgSub = this.db
      .object('/widget')
      .valueChanges()
      .pipe(filter(val => val !== null))
      .subscribe(
        (data: any) => this.setWidgetContent(data)
      );
  }

  setWidgetContent(data) {
    console.log('setWidgetContent: ', data);
    this.widgetId = data.id,
    this.widgetTimeout = data.timeout;
    this.widgetShow = data.show;
    this.widgetContent.header = data.content.header;
    this.widgetContent.body = data.content.body;
    this.widgetContent.date = data.content.date;
    this.widgetContent.url = data.content.url;
    if (data.content.imgLink) {
      this.widgetContent.imgLink = data.content.imgLink;
    }
  }

  triggerShowHide() {
    // console.log(event);

    const data = {
      id: this.widgetId,
      timeout: this.widgetTimeout,
      show: this.widgetShow, // show | hide | permanent
      content: {
        header: this.widgetContent.header,
        body: this.widgetContent.body,
        date: this.widgetContent.date,
        url: this.widgetContent.url,
        imgLink: this.widgetContent.imgLink,
      }
    }
    if(this.widgetContent.url) {
      data.content.url = this.widgetContent.url;
    }
    if(this.widgetContent.imgLink) {
      data.content.imgLink = this.widgetContent.imgLink;
    }

    //if (this.widgetShow = "show") {
      this.db.object('/widget')
        .update(data);
    //}


  }

  // end of class
}

/*

this.db.object(`/now/` + this.token)
      .query
      .once('value')
      .then(data => {
        // console.log('data: ', token, data)
        this.data = data;
        if (data) {
          // only dummy data transmitted for triggering Firebase
          const obj: Object = { owner: this.user };
          // console.log(obj);
          this.db.object(`/now/` + this.token)
            .set(obj)
            .then(() => {
              // console.log('success, created obj: ', obj);
            })
            .catch(error => {
              console.log('set catch error', error);
            });


*/