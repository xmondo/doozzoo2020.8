import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/auth.service';
import { AngularFireDatabase, AngularFireObject, AngularFireList } from '@angular/fire/database';
import { Observable, Subject } from 'rxjs';
import { FormControl } from '@angular/forms';
import { debounceTime, filter, mergeMap, map } from 'rxjs/operators';

interface searchSubject {
  key: 'key' | 'role' | 'userActions' | 'paginate'
  value: any,
}

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {

  profileList$: Observable<any>;

  rolesArr = [
    { value: 0, viewValue: 'Student' },
    { value: 1, viewValue: 'Coach' },
    { value: 2, viewValue: 'Admin' }
  ];
  filterargs: any;
  inputType: any;
  itemsPerPageValue: number = 10;

  filterKey = new FormControl();
  keys: Array<string> = ['key', 'role', 'userActions', 'paginate'];
  filterValue = new FormControl();
  filterSub$: Subject<searchSubject> = new Subject();
  pagesize = 1000;
  
  constructor(
    public ats: AuthService,
    public db: AngularFireDatabase,
  ) { }

  ngOnInit() {

    // default
    this.filterKey.setValue('key');

    this.profileList$ = this.filterSub$
      .pipe(
        filter((val: any) => val !== null && val !== undefined  && val.value.length > 0),
        mergeMap((subject: any) => {
          console.log('search subject: ', subject);

          switch(subject.key) {
            case 'key':
              return this.db
                .list('users', ref => ref.orderByKey().equalTo(subject.value))
                .snapshotChanges();
              break; 
            case 'role':
              const roleVal = parseInt(subject.value)
              return this.db
                .list('users', ref => ref.orderByChild('role').equalTo(roleVal))
                .snapshotChanges();  
              break; 
            case 'userActions':
              return this.db
                .list('users', ref => ref.orderByChild('userActions').equalTo(subject.value))
                .snapshotChanges(); 
              break; 
            case 'paginate':
              const main$ = this.db
                .list('users', ref => ref.orderByKey().limitToFirst(1000))
              break; 
            case 'paginateStartAt':
              return this.db
                .list('users', ref => ref.orderByKey().startAt(subject.value).limitToFirst(this.pagesize))
                .snapshotChanges(); 
              break; 
            case 'paginateEndAt':
              return this.db
                .list('users', ref => ref.orderByKey().endAt(subject.value).limitToFirst(this.pagesize))
                .snapshotChanges();   
              break; 
          }
          
        })
      )

    const keySub = this.filterKey
      .valueChanges
      .pipe(
        // debounceTime(200),
        filter(val => val !== null && val !== undefined && val.length > 0),
      )
      .subscribe((val: string ) => {
        switch(val) {
          case 'userActions':
            this.filterValue.setValue('createdByInstitution');
            this.filterSub$.next({ key: this.filterKey.value, value: 'createdByInstitution'});
            break;
          case 'role':
            this.filterValue.setValue('1');
            this.filterSub$.next({ key: this.filterKey.value, value: 1});
            break;
          case 'paginate':
            this.filterValue.setValue('1');
            this.filterSub$.next({ key: this.filterKey.value, value: 1});
            break;
        }

      })

    const valueSub = this.filterValue
      .valueChanges
      .pipe(
        debounceTime(200),
        filter(val => val !== null && val !== undefined && val.length > 0),
      )
      .subscribe((val: string) => this.filterSub$.next({ key: this.filterKey.value, value: val}))
  }

  featureToggle = {

  }

  doChange(name, key, event) {
    console.log('item: ', key, event);

    if (key) {
      let updateObj;
      if (name === 'betatester') {
        updateObj = { betatester: event.checked };
        this.db.list('users').update(key, updateObj);

      } else if (name === 'mediaMode') {
        // needed to update the user
        const _key = `${key}/featureToggle`;
        updateObj = { mediaModeToggle: event.checked };
        this.db.list('users').update(_key, updateObj);

      } else if (name === 'role') {
        updateObj = { role: event.value };
        this.db.list('users').update(key, updateObj);

      } else if (name === 'studentLimit50') {
       
        // needed to update the user
        const key1 = `${key}/featureToggle`;
        event.checked ? updateObj = { maxStudentCount: 50 } : updateObj = { maxStudentCount: null }
        this.db.list('users').update(key1, updateObj);
        
        // dummy stripe entry as trigger for the student count...
        const key2 = `${key}/subscriptions/currentSubscription`
        event.checked ? updateObj = { quantity: 6 } : updateObj = { quantity: 0 }
        this.db.list('stripe_customers').update(key2, updateObj);
      }
    }

  }

// end of class
}
