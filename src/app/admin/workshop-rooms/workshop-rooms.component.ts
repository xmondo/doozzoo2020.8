import { Component, OnInit, OnDestroy } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { FormControl, Validators } from '@angular/forms';
import { from, Observable, of, Subject, Subscription } from 'rxjs';
import { filter, map, startWith, switchMap, tap } from 'rxjs/operators';

interface WorkshopRoomAttributes {
  key: string, 
  owner: string,
  locked: boolean,
  timestamp: string,
}

@Component({
  selector: 'app-workshop-rooms',
  templateUrl: './workshop-rooms.component.html',
  styleUrls: ['./workshop-rooms.component.scss']
})
export class WorkshopRoomsComponent implements OnInit, OnDestroy {

  roomsListRef: AngularFireList<any>;
  roomsList$: Observable<any[]>;

  roomName = new FormControl('', [ Validators.required ]);
  owner = new FormControl('', [ Validators.required ]);
  updateOwnerInput = new FormControl('');

  toggleFilenameEdit: number = null;
  ownerQuery$: Subject<string>;
  ownerQueryObs$: Observable<boolean>;

  subContainer: Subscription = new Subscription();

  constructor(
    public db: AngularFireDatabase,
  ) {
    // reference list
    this.roomsListRef = this.db.list('now');

    this.roomsList$ = this.db
      .list(
        'now',
        ref => ref.orderByChild('permanent').equalTo(true)
      )
      .snapshotChanges()
      .pipe(
        map(snapArr => [...snapArr.map((item: WorkshopRoomAttributes | any) => ({
          key: item.key, 
          owner: item.payload.val()?.owner,
          locked: item.payload.val()?.locked,
          timestamp: item.payload.val()?.timestamp,
        }))])
      );
  }

  deleteItem(key: string) {
    this.roomsListRef.remove(key);
  }

  toggleLock(key: string, locked: boolean) {
    console.log(key, locked);
    this.roomsListRef.update(key, { locked: !locked });
  }

  addWorkshopRoom() {
    const roomName = this.roomName.value;
    const owner = this.owner.value;
    // this.roomsListRef.push({ owner: owner,  });
    this.roomsListRef.set(roomName, { owner: owner, permanent: true });
  }

  updateOwner(key: string){
    const owner = this.updateOwnerInput.value;
    console.log(key, owner);
    this.roomsListRef.update(key, { owner: owner });
    this.toggleFilenameEdit = null;
  }

  setToggleFilenameEdit(item: WorkshopRoomAttributes, idx: number) {
    this.toggleFilenameEdit = idx;
    this.updateOwnerInput.setValue(item.owner);
  }

  trackByFn(item) {
    return item.key;
  }

  ngOnInit(): void {
    this.ownerQueryObs$ = this.updateOwnerInput
      .valueChanges
      .pipe(
        filter(val => val.length > 8),
        switchMap((qry: any) => from(this.db.database.ref(`users/${qry}`).once('value'))),
        map(val => val.exists()),
        tap(val => console.log('***', val)),
        startWith(false),
      );
  }

  ngOnDestroy(): void {

  }

}
