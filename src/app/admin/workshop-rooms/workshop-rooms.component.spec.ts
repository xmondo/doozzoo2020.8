import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkshopRoomsComponent } from './workshop-rooms.component';

describe('WorkshopRoomsComponent', () => {
  let component: WorkshopRoomsComponent;
  let fixture: ComponentFixture<WorkshopRoomsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkshopRoomsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkshopRoomsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
