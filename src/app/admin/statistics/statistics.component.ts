import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit {
  sessionStats: Observable<any>;
  kpis: Observable<any>;
  columnsToDisplay1 = [
    'date',
    'coaches', 
    'students',
    'disconnectedStudents',
    'disconnectedStudents7',
    'disconnectedStudents30',
    'admins',
    'institutionalCoaches',
    'institutionalStudents',
    'institutionalAdmins',
  ];

  columnsToDisplay2 = [
    'date',
    'activeCoaches1',
    'activeStudents1',
    'activeCoaches7',
    'activeStudents7',
    'activeCoaches30',
    'activeStudents30',
  ];

  columnsToDisplay3 = [
    'date',
    'sessions1numSessions',
    'sessions1duration',
    'sessions7numSessions',
    'sessions7duration',
  ];

  constructor(
    public ats: AuthService,
    public db: AngularFireDatabase,
    public fst: AngularFirestore,
  ) { }

  ngOnInit() {
      this.sessionStats = this.db.list('logs/logStream')
        .valueChanges();

      this.kpis = this.fst
        .collection('kpis')
        .valueChanges();
  }

  numStreams(obj) {
    const length = Object.keys(obj).length - 3;
    return length;
  }

}
