import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityMeterComponent } from './activity-meter.component';

describe('ActivityMeterComponent', () => {
  let component: ActivityMeterComponent;
  let fixture: ComponentFixture<ActivityMeterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActivityMeterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityMeterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
