import { Component, Input, OnInit, NgZone, OnDestroy, Renderer2, OnChanges, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import * as Tone from 'tone';

@Component({
  selector: 'app-activity-meter',
  templateUrl: './activity-meter.component.html',
  styleUrls: ['./activity-meter.component.scss']
})
export class ActivityMeterComponent implements OnInit, OnChanges, AfterViewInit {
  @Input() layout: 'default' | 'pulse' | 'square';
  @Input() meterValue: number;
  @ViewChild('activitymeterPulse1', { static: true }) activitymeterPulse1: ElementRef;

  constructor(
    private renderer: Renderer2,
  ) { }

  ngOnInit(): void {
    this.layout = this.layout ?? 'default';
    console.log(this.activitymeterPulse1)
  }

  ngAfterViewInit() {
    console.log('*', this.activitymeterPulse1)
  }

  ngOnChanges(changes) {
    this.draw();
  }

  ngOnDestroy() {
    console.log('activitymeter destroyed...')
  }

  draw() {
    console.log(this.meterValue)
    if(this.layout === 'pulse') {
      const el = this.activitymeterPulse1.nativeElement;
      // this.renderer.setAttribute(el, 'width', `${this.meterValue}%`);
      // this.renderer.setAttribute(el, 'height', `${this.meterValue}%`);
      // this.meterValue < -0.3 ? this.renderer.setAttribute(el, 'r', `${this.meterValue}`) : '';
      this.meterValue > -0.03 ? this.renderer.setAttribute(el, 'fill', 'red') : this.renderer.setAttribute(el, 'fill', 'rgb(0,84,150)');
    } else {
      const el = this.renderer.selectRootElement('.activitymeterPulse1', true);
      this.renderer.setAttribute(el, 'width', `${this.meterValue}%`);
      this.meterValue > 100 ? this.renderer.setAttribute(el, 'fill', 'red') : this.renderer.setAttribute(el, 'fill', 'rgb(0,84,150)');
    }
  }
// end of class
}
