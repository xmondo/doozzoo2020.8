import { Component, Input, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable, Observer, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserData } from 'src/app/shared/auth.service';

@Component({
  selector: 'app-mediamode',
  templateUrl: './mediamode.component.html',
  styleUrls: ['./mediamode.component.scss']
})
export class MediamodeComponent implements OnInit {
  @Input() user: UserData;

  mediaModeObs$: Observable<any>; // <'routed' | 'relayed'>
  mediaModeObsSub: Subscription = new Subscription();
  currentMediaMode: 'routed' | 'relayed' = 'routed';
  
  constructor(
    private db: AngularFireDatabase,
  ) { }

  ngOnInit(): void {
    this.mediaModeObs$ = this.db
      .object(`classrooms/${this.user.uid}/mediaMode`)
      .valueChanges()

    this.mediaModeObsSub = this.mediaModeObs$
      .pipe(map( val => val ?? 'routed'))
      .subscribe(val => this.currentMediaMode = val)
  }

  toggleMediaMode(event) {
    console.log('toggle mediaMode', event);
    this.currentMediaMode = event.value;
    this.db.object(`classrooms/${this.user.uid}/mediaMode`).set(this.currentMediaMode);
  }

}
