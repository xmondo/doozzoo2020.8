import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MediamodeComponent } from './mediamode.component';

describe('MediamodeComponent', () => {
  let component: MediamodeComponent;
  let fixture: ComponentFixture<MediamodeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MediamodeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MediamodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
