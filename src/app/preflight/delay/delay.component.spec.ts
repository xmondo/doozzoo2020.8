import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Router } from '@angular/router';
import * as WaveSurfer from 'wavesurfer.js';
import * as Tone from 'tone';
import { AudioService } from '../../shared/audio.service';

import { DelayComponent } from './delay.component';

describe('DelayComponent', () => {
  let component: DelayComponent;
  let fixture: ComponentFixture<DelayComponent>;
  const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        DelayComponent
      ],
      providers: [
        { provide: Router, useValue: routerSpy },
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DelayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create wavesurfer module', () => {
    const ws = WaveSurfer.create({
      container: '#waveform'
    });
    expect(ws).toBeTruthy();
  });

  // main config should be there
  it('should check config', () => {
    expect(component.delayCalculated).toBe(false);
    expect(component.testIsRunning).toBe(false);
    expect(component.url).toEqual('https://dev.doozzoo.com/assets/audio/metronome/click_1s.mp3');
    // component.startRecorder('playerMic', 0);
  });

  // this should be showing that recording function is working and delivering a peak ms number as result
  it('should start recorder and peak ms number',
    (done: DoneFn) => {
      component.startRecorder('playerMic', 0)
        .then(peak => {
          console.log('****', peak)
          expect(peak !== null).toBeTruthy();
          expect(typeof peak).toEqual('number');
          expect(peak < 10000 && peak > 3000).toBeTruthy();
          done();
      });
  });
  
});
