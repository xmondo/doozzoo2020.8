// otherwise does not find 'MediaRecorder' name and throws error.
/// <reference path="../../../../node_modules/@types/dom-mediacapture-record/index.d.ts" />

import { Component, OnInit, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import * as WaveSurfer from 'wavesurfer.js';
import * as Tone from 'tone';
import { AudioService } from '../../shared/audio.service';
import { Router } from '@angular/router';
import { OpentokService } from 'src/app/shared/opentok.service';

@Component({
  selector: 'app-delay',
  templateUrl: './delay.component.html',
  styleUrls: ['./delay.component.scss'],
})
export class DelayComponent implements OnInit, AfterViewInit {
  // @ViewChild('subscriberDiv') subscriberDiv: ElementRef;
  ws: WaveSurfer;
  ctx: Tone = Tone.context;
  delay: number = 0;
  url = 'https://dev.doozzoo.com/assets/audio/metronome/click_1s.mp3';
  player: Tone.Player;
  stop: Function;
  output: string = null;
  delayCalculated: boolean = false;
  testIsRunning: boolean = false;
  statusMsg: string = 'Test idle...'
  constraints: any;

  constructor(
    private as: AudioService,
    public router: Router,
    private ots: OpentokService,
  ) {
    // ...
    this.constraints = this.ots.getConstraints();
    this.player = new Tone.Player(this.url).toMaster();
  }

  ngOnInit() {
    // console.log('constraints: ', this.constraints);
  }

  ngAfterViewInit() {
    const self = this;
    this.ws = WaveSurfer.create({
      audioContext: this.ctx,
      container: '#waveform',
      waveColor: 'green',
      progressColor: 'purple',
      cursorWidth: 1,
      normalize: true
    });

    this.ws.load(this.url);

    this.ws.on('ready', function () {
      // this.ws.play();
      self.ws.zoom(1000);
    });
  }

  // triggered when buffer loaded
  analyze(bufferArr){
    // let bufferArr = player.buffer.getChannelData(0);
    const channelData = bufferArr.getChannelData(0)
    const max = this.findMax(channelData)
    // console.log('max: ', max)

    const t = this.peakPosition(channelData, max, 0.8);
    // console.log('### peak time: ', t);
    // display buffer
    this.ws.loadDecodedBuffer(bufferArr);
    // return the peak time
    return t;
  }

  findMax(bufferArr) {
    let previous = bufferArr[0];
    bufferArr.forEach((item, index) => {
      if (item > previous && item > 0) {
        previous = item;
      };
    });
    return previous;
  }
  
  peakPosition(bufferArr, max, treshold){
    // defines which volumes we take into the peak area
    treshold = treshold || 0.8;
    let resultArr = [];
    bufferArr.forEach((item, index) => {
      // keep only + part of wave
      if (item < 0) { 
        item = 0;
      }
      const cap = max * treshold;
      if (item > cap){
        resultArr.push([index, item]);
      }
    });
    // console.log(resultArr)

    let sum = 0;
    resultArr.forEach(item => {
      sum = sum + item[0];
    });
    // find out the sampleNr average, which will be the time of the peak
    const tmp = Math.floor(sum / resultArr.length);
    return tmp;
  }

  startRecorder(mode, delayValue){
    const self = this;
    // mode options:
    // player -> only player is recorded
    // mic -> only player to speaker to mic is recorded
    // playerMic -> both are recorded
    // delay -> player vs. mic
    mode = mode || 'player'
    delayValue = delayValue || 0;
    // constraints
    /*
    const constraints = {
      video: true,
      audio: {
        echoCancellation: false,
        noiseSuppression: false,
        autoGainControl: true
      }
    };
    */
    const constraints = {
      video: false,
      audio: this.constraints.audio
    }

    return new Promise((resolve, reject) => {
      navigator.mediaDevices.getUserMedia(constraints)
        .then(stream => {

          console.log(stream)
  
          // fetch MIC as mediaStreamSource
          const localMic = this.ctx.createMediaStreamSource(stream);
          // create custom MediaStream destination
          const streamDestination = this.ctx.createMediaStreamDestination();
  
          const gain = new Tone.Gain(2) // over amplify mic chain
          const merger = this.ctx.createChannelMerger(1);
          if (mode === 'mic' || mode === 'playerMic') {
            localMic.connect(gain, 0);
          }
          gain.connect(merger, 0);

          // player.connect(merger, 0)
          merger.connect(streamDestination);
  
          const delay = new Tone.Delay({
            'maxDelay' : 0.5 ,
            'delayTime' : delayValue
          });
  
          if (mode === 'player' || mode === 'playerMic') { 
            this.player.connect(delay);
          }
          // connect localMic
          delay.connect(merger, 0);
  
          // start player for recording
          setTimeout(() => this.player.start(), 50);
  
          // ### recorder ###
          // record streamDestination
      
          const mr = new MediaRecorder(streamDestination.stream);
          let chunks = [];
          mr.ondataavailable = function(event) {
            // console.log("mr->ondataavailable: ", event);
            chunks.push(event.data);
            // stops after first 1000msec
            // console.log('event.data: ', event.data, chunks);
            if (mr.state === 'recording') {
              mr.stop();
            }
          };
  
          mr.onstop = function(event) {
            // ...
            const blob = new Blob(chunks, {'type' : 'audio/mp3; codecs=h264' });
            self.ws.loadBlob(blob);
  
            self.blobToDecodedArrayBuffer(blob)
              .then(bufferArr => {
                  // console.log('buffer: ', bufferArr)
                  resolve(self.analyze(bufferArr));
              });
          };
          mr.start(500); 
          this.stop = () => mr.stop();

      })
      .catch(error => {
        console.log(error);
        reject(error);
      });
    });
  }

  blobToDecodedArrayBuffer(blob) {
      const self = this;
      // promise
      return new Promise((resolve, reject) => {
          const fileReader = new FileReader();
          fileReader.onload = e => {
              // console.log('fr', fileReader.result);
              self.ctx.decodeAudioData(fileReader.result)
                .then(buffer => resolve(buffer));
          };
          fileReader.onerror = error => {
              reject(error);
          };
          fileReader.readAsArrayBuffer(blob);
      });
  }

  doIterate(mode, NrOfTestRuns, timeout) {
    // return new Promise((resolve: (value: Array<any>)) => {
    return new Promise<any[]> ((resolve, reject) => {
      let i = 0;
      let arr: Array<any> = [];
      try {
        const tmo = setInterval(() => {
          this.startRecorder(mode, 0)
            .then(peakTime => {
              console.log('i, peakTime, NrOfTestRuns: ', i, peakTime, NrOfTestRuns);
              // not the first one, in most cases this is delayed
              arr.push(peakTime);
              // console.log(arr, i)
              if (i === NrOfTestRuns - 1) {
                clearInterval(tmo);
                this.output += '### sample times "' + mode + '": ' + arr + ' ###';
                console.log(this.output);
                resolve(arr);
                i = 0;
              }
              i++;
            });
        }, timeout);
      } catch (error) {
        reject(error);
      }
    });
  }
  
  automate (NrOfTestRuns, timeout) {
    NrOfTestRuns = NrOfTestRuns || 1;
    timeout = timeout || 2000;
    let playerResults, micResults;
    
    this.doIterate('player', NrOfTestRuns, timeout)
      .then(arr => {
        console.log('### arr: ', arr)
        let tmp = arr.length;
        playerResults = arr.reduce((sum, item) => {
          return sum + item;
        });
        playerResults = playerResults / tmp;
        
        this.doIterate('mic', NrOfTestRuns, timeout)
          .then(arr => {
            // console.log(arr)
            tmp = arr.length;
            micResults = arr.reduce((sum, item) => {
              return sum + item;
            });
            micResults = micResults / tmp;
            this.delay = this.calcDelay(playerResults, micResults);
            this.output += '### DELAY: ' + this.delay + ' ###';
            console.log('### DELAY:', this.delay, ' ###');

            // apply and show waveforms
            // apply to general service value
            this.as.masterDelay.delayTime.value = this.delay;
            this.as.storeMasterDelay();
            
            // show in waveform
            this.applyDelay();

          });

        });

  }
  

  calcDelay (playerResults, micResults) {
    // TODO: found out that factor 2 brings realistic results
    const tmp = ((micResults - playerResults) / Tone.context.sampleRate) * 2;
    return tmp;
  }

  startTest = () => {
    this.delayCalculated = false;
    this.testIsRunning = true;
    this.statusMsg = 'Test started, you should hear "Clicks" and see them visualized in the waveform...';
    // warmup
    this.startRecorder('playerMic', 0);
    // start test
    setTimeout(() => this.automate(6, 700), 2000);
  }

  startTestNoDelay = () => {
    this.startRecorder('playerMic', 0);
    this.statusMsg = 'No compensation > You should see TWO peaks in the waveform.';
  }
  startTestDelay = () => {
    // compensate for factor 2
    this.startRecorder('playerMic', this.delay/2);
    this.statusMsg = 'Compensation applied > You should see ONE peak in the waveform.';
  }

  applyDelay(){
    // this.startRecorder('playerMic', 0);
    setTimeout(() => {
      this.startTestNoDelay();
    }, 2000);
    setTimeout(() => {
       this.startTestDelay();

       // completion trigger for UI
       this.delayCalculated = true;
       this.testIsRunning = false;
    }, 5000);
  }

// component end
}

