import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-set-bitrate',
  templateUrl: './set-bitrate.component.html',
  styleUrls: ['./set-bitrate.component.css']
})
export class SetBitrateComponent implements OnInit {
  @Input() selectedBitrate: number;
  @Output() setBitrate: EventEmitter<64000 | 128000 | 192000> = new EventEmitter();
  audioBitrateOptions: number[] = [64000, 128000, 192000]; 
  constructor(
    // ...
  ) { }

  ngOnInit() {
    // this.selectedBitrate = this.ads.getBitrate(); 
    // console.log('initBitrate', this.selectedBitrate);
  }

  doSetBitrate(event): void {
    // console.log('++++', event)
    this.setBitrate.next(event.value);  
  }

}
