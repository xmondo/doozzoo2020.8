import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetBitrateComponent } from './set-bitrate.component';

describe('SetBitrateComponent', () => {
  let component: SetBitrateComponent;
  let fixture: ComponentFixture<SetBitrateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetBitrateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetBitrateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
