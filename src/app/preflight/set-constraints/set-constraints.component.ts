import { Component, OnInit, Input } from '@angular/core';
import { OpentokService } from '../../shared/opentok.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import { Router } from '@angular/router';

@Component({
  selector: 'app-set-constraints',
  templateUrl: 'set-constraints.component.html',
  styleUrls: ['set-constraints.component.scss']
})
export class SetConstraintsComponent implements OnInit {
  @Input() layout: String;
  @Input() stream: MediaStream;
  constraints: any;
  checked = false;
  wasClicked = false;
  deviceInfo;

  constructor(
    private ots: OpentokService, 
    private deviceService: DeviceDetectorService,
    public router: Router,
  ) {
    // ...
    this.deviceInfo = this.deviceService.getDeviceInfo();
    // get contraints form service
    this.constraints = this.ots.getConstraints();
  }

  ngOnInit() {
    // ...
    // console.log('layout: ', this.layout)
    /*
    this.ots.streamEmitter.subscribe(payload => {
      if (payload.action === 'preflightTrigger') {
          // this.initConstraints();
      }
    });
    */
    this.initConstraints();
  }

  async initConstraints() {
    // get constraints status
    const supports = navigator.mediaDevices.getSupportedConstraints();
    // const stream: MediaStream = await navigator.mediaDevices
    //  .getUserMedia({ audio: this.constraints.audio });
    const stream = this.stream;

    // read a generic audio track
    const audioTrack = stream.getAudioTracks()[0];
    // read current settings
    const currentSettings = audioTrack.getSettings();
  
    // apply them to model
    if (supports['echoCancellation']) {
      this.constraints.audio.echoCancellation = currentSettings['echoCancellation'];
    }
    if (supports['noiseSuppression']) {
      this.constraints.audio.noiseSuppression = currentSettings['noiseSuppression'];
    }
    if (supports['autoGainControl']) {
      this.constraints.audio.autoGainControl = currentSettings['autoGainControl'];
    }
    // console.log('current audio constraints settings/constraints: ', currentSettings, currentConstraints, this.constraints.audio);
    
  }

  submitSelection(event) {
    // console.log('event', event)
    // ...
    this.wasClicked = true;

    // set new constraints in global var
    const allConstraints = this.ots.getConstraints();
    allConstraints.audio = this.constraints.audio;
    // persist in local storage
    this.ots.setConstraints(allConstraints);
    // emit event for subscribers
    const sessionPrefs = this.ots.getSessionPrefs();
    sessionPrefs.echoCancellation = this.constraints.audio.echoCancellation;
    sessionPrefs.noiseSuppression = this.constraints.audio.noiseSuppression;
    sessionPrefs.autoGainControl = this.constraints.audio.autoGainControl;
    // console.log('submitSelection: ', sessionPrefs);
    // this.ots.setSessionPrefs(sessionPrefs);
    this.ots.setSessionPrefs();
  }

  reload() {
    this.router.navigateByUrl('/presession')
  }

// end of class
}

/**
 * these constraints can be switched in Firefox at any time per audioTrack.applyConstraints()
 * In Chromium they only have effekt if we are starting with constraint set to "false". After this it can be switched on/off (WTF!!)
 */


