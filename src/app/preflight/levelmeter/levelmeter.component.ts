import { Component, OnInit, ElementRef, NgZone, Input, OnDestroy, Renderer2, OnChanges } from '@angular/core';
@Component({
  selector: 'app-levelmeter',
  templateUrl: './levelmeter.component.html',
  styleUrls: ['./levelmeter.component.scss']
})

export class LevelmeterComponent implements OnInit, OnDestroy, OnChanges {
  // @ViewChild('myMeter', { static: true }) myMeter: ElementRef;
  // @ViewChild('rectBar1', { static: true }) rectBar1: ElementRef;
  // @ViewChild('rectBar2', { static: true }) rectBar2: ElementRef;
  // @ViewChild('rectBar3', { static: true }) rectBar3: ElementRef;
  @Input() stream: MediaStream;
  @Input() layout: 'default' | 'pulse' | 'square';
  // stream: MediaStream;
  canvasContext: CanvasRenderingContext2D;
  // meter = new Tone.Meter({ smoothing : 0.5 });
  level: number;
  myInterval: any;
  doAnimate: boolean = true;
  // stream: MediaStream = null;
  myWidth: number = 70;

  constructor(
    private ngZone: NgZone,
    private renderer: Renderer2,
    public el: ElementRef,
    ) { 
      // ...
  }

  ngOnInit() {
    this.layout = this.layout ?? 'default';
  }

  // we listen to the changes from the stream assignment
  ngOnChanges(changes) {
    // console.log('*** ', changes);
    this.initLevelmeter();
  }

  ngOnDestroy() {
    console.log('levelmeter destroyed...')
    // !!! cancelAnimationFrame(this.myInterval) !!!
    this.doAnimate = false;
  }

  async initLevelmeter() {
    // console.log('initLevelmeter->audioInDeviceId: ', this.audioInDeviceId);
    setTimeout(async () => {
      // this.stream = await navigator.mediaDevices
      //  .getUserMedia(<MediaStreamConstraints>_constraints);
      // console.log('levelmeter input', this.stream);
      this.drawVolume();
    }, 500);
  }

  // ### level meter ###
  // use Tone.js for drawing volume
  drawVolume() {
    // use standard audio context
    const audioContext = new AudioContext();

    // Safari -> connect in < 2sec //
    const input = audioContext.createMediaStreamSource(this.stream);
    var analyser = audioContext.createAnalyser();
    analyser.smoothingTimeConstant = 0.9;
    analyser.minDecibels = -90;
    analyser.maxDecibels = -10;
    analyser.fftSize = 32; // 64

    input.connect(analyser);

    const bufferLength = analyser.frequencyBinCount; // 16
    const dataArray = new Uint8Array(bufferLength);
    // the current level of the mic input in decibels
    // try catch, as the 

    const rootElStr = this.layout === 'pulse' ? '.pulse' : '.rectBar1';
    const rootEl = this.renderer.selectRootElement(rootElStr, true);
    this.draw1(analyser, dataArray, rootEl);


  }

  draw1(analyser, dataArray, rootEl) {
    this.ngZone.runOutsideAngular(() => {
      const loop = () => {
        analyser.getByteFrequencyData(dataArray);

        // const arrMin = (Math.min(...dataArray) * 255) / 100;
        // const arrMax = (Math.max(...dataArray) * 255) / 100;
        // const arrAvg = ((dataArray.reduce((a,b) => a + b, 0) / dataArray.length) * 125) / 100;
        // this.rectBar1.nativeElement.setAttribute("width", `${arrAvg}px`);

        if(this.layout === 'pulse') {
          const arrAvg = ((dataArray.reduce((a,b) => a + b, 0) / dataArray.length) * 25) / 100;
          // const el = this.renderer.selectRootElement('.pulse1', true);
          // this.renderer.setAttribute(el, 'width', `${arrAvg}%`);
          // this.renderer.setAttribute(el, 'height', `${arrAvg}%`);
          arrAvg < 25 ? this.renderer.setAttribute(rootEl, 'r', `${arrAvg}`) : '';
          arrAvg > 20 ? this.renderer.setAttribute(rootEl, 'fill', 'red') : this.renderer.setAttribute(rootEl, 'fill', 'rgb(0,84,150)');
        } else {
          const arrAvg = ((dataArray.reduce((a,b) => a + b, 0) / dataArray.length)) * 1.6; // * 125) / 100;
          try{ 
            // const el = this.renderer.selectRootElement('.rectBar1', true);
            this.renderer.setAttribute(rootEl, 'width', `${arrAvg}`);
            arrAvg > 230 ? this.renderer.setAttribute(rootEl, 'fill', 'red') : this.renderer.setAttribute(rootEl, 'fill', 'rgb(0,84,150)');
           }
          catch(e) {
            console.log('levelmeter->draw->error: ', e);
          }
          
        }

        // this.rectBar1.nativeElement.setAttribute("width", `${arrAvg}px`);
        // this.rectBar2.nativeElement.setAttribute("width", `${arrMin}px`);
        // this.rectBar3.nativeElement.setAttribute("width", `${arrMax}px`);
        // easiest and only way to terminate loop by destroying the component
        // all other ways did not succed in really terminating, loop was still running
        this.doAnimate ? requestAnimationFrame(loop) : '';
      };
      loop();
    });
  }

// end of class
}
