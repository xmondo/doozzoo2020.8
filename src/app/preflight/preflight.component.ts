import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, OnDestroy, Renderer2 } from '@angular/core';
import { OpentokService, sessionPrefs } from '../shared/opentok.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import { Router } from '@angular/router';
import { AudioService } from '../shared/audio.service';
import { from, Observable, Subject, Subscription } from 'rxjs';
import { AuthService } from '../shared/auth.service';
import { AudioConstraints, PublisherState, PublisherStoreService } from '../publisher/publisher-store.service';
import { distinctUntilChanged, map, skip, switchMap, take, tap } from 'rxjs/operators';
import { PublisherService } from '../publisher/publisher.service';

@Component({
  selector: 'app-preflight',
  templateUrl: './preflight.component.html',
  styleUrls: ['./preflight.component.scss']
})
export class PreflightComponent implements OnInit, AfterViewInit, OnDestroy  {
  @ViewChild('myVideo', { static: true }) myVideo: ElementRef;
  
  videoInputSelected: string = 'default';
  audioInputSelected: string = 'default';
  audioOutputSelected: string = 'default';
  videoInput = [];
  audioInput = [];
  audioOutput = [];
  deviceInfo: any;
  isMobile: any;
  // changeDetectorRef: ChangeDetectorRef;
  checked: boolean;
  headphoneMode = true;
  uiError: any = null;
  stream: MediaStream = null;
  showLevelmeter: boolean = false;
  overlay = true;
  masterDelay: number;
  constraints: any;
  completeButton: boolean = false;
  mirrorMode: boolean = false;
  sessionPrefs: sessionPrefs;
  inputNode: AudioNode = null;
  meterLevel: number;
  isSafari: boolean;
  triggerGUM$: Subject<any> = new Subject();

  vm$: Observable<any>;

  debug = false;

  subContainer: Subscription = new Subscription();

  constructor(
    public ots: OpentokService, 
    public deviceService: DeviceDetectorService,
    // private ref: ChangeDetectorRef,
    public router: Router,
    public as: AudioService,
    public a: AuthService,
    public publisherStore: PublisherStoreService,
    private publisherService: PublisherService,
    private renderer: Renderer2,
  ) {
    // ...
    this.vm$ = this.publisherStore.vm$;

    this.deviceInfo = this.deviceService.getDeviceInfo();
    this.isMobile = this.deviceService.isMobile();
    this.isSafari = this.deviceService.browser.toLowerCase().indexOf('safari') !== -1 ? true : false;
    // console.log('++++++', this.deviceInfo, this.isMobile);
    // this.constraints = this.ots.getConstraints();
    // console.log(this.constraints)
  }

  ngOnInit() {
    // init sync of prefs to localStorage
    this.publisherService.saveToLocalStorage();
    this.masterDelay = this.as.getMasterDelay();
  }

  ngAfterViewInit() {
    
    // listen to device changes and apply them 
    const dcSub = this.ots.deviceChange$
      .pipe(
        // take(1),
        distinctUntilChanged(),
        switchMap(() => this.publisherStore.state$)
      )
      .subscribe(publisherStorePrefs => {
        console.log('', publisherStorePrefs)
        this.doGUM(publisherStorePrefs)
      });
    this.subContainer.add(dcSub);
    
    // triggered by input device changes and change of resolution
    const spChSub = this.publisherStore
      .preSessionCheckChanges$
      .subscribe(val => {
        // console.log('received sessionPrefs update: ', val);
        this.triggerGUM$.next();
      })
    this.subContainer.add(spChSub);

    // triggeres publisher initiation
    const trgumSub = this.triggerGUM$
      .pipe(
        switchMap(() => this.publisherStore
          .state$
          .pipe(
            // tap(val => console.log('trigger GUM executed...')),
            take(1),
            // ### 1. fetches usermedia and... ###
            switchMap(publisherStorePrefs => {
              // console.log('publisherStorePrefs: ', publisherStorePrefs);
              return this.doGUM(publisherStorePrefs)
            })
          )
        )
      )
      .subscribe();
    this.subContainer.add(trgumSub);
    this.triggerGUM$.next();
  }

  ngOnDestroy() {
    // console.log('preflight check component destroyed');
    this.subContainer.unsubscribe();
  }

  async doGUM(publisherStorePrefs: PublisherState) {
    const self = this;

    const constraints = this.publisherService.buildConstraints(publisherStorePrefs);

    // ### stream created ###
    // try cats for Firefox...
    const stream: MediaStream | any = await navigator
      .mediaDevices
      .getUserMedia(constraints)
      .catch(error => {
        console.log('*** GUM error: ', error.name, error);

        // user did not allow camera, microphone
        if(error.name === 'NotAllowedError') {
          console.log('error catch -> permission');
          this.overlay = false;
          this.router.navigate(['/camera-access']);
        }

        // covers a strange error on Firefox side, when switching the audio ins
        if(error.toString().toLowerCase().indexOf('concurrent mic process limit') !== -1) {
          // this.publisherStore.setAudioSource('default');
          setTimeout(() => this.triggerGUM$.next(), 3000);
        }

      });

    if(stream !== undefined) {
      this.stream = stream;

      // for safari this must be located after a user interaction otherwise throws
      // const gotDevices = await this.updateDeviceList();
      const gotDevices = <MediaDeviceInfo[]>await this.ots.updateDeviceList();
      // console.log('gotDevices: ', gotDevices)
      this.videoInput = <MediaDeviceInfo[]>await this.ots.updateDeviceListFiltered('videoinput');
      this.audioInput = <MediaDeviceInfo[]>await this.ots.updateDeviceListFiltered('audioinput');
      this.audioOutput = <MediaDeviceInfo[]>await this.ots.updateDeviceListFiltered('audiooutput');

      const vTrack = stream.getVideoTracks()[0];
      const vTrackSelected = gotDevices.find(item => { 
        return item.label === vTrack.label && item.kind === 'videoinput';
      });
      this.videoInputSelected = vTrackSelected.deviceId;
      console.log('----------->', vTrackSelected.label);

      const aTrack = <MediaStreamTrack>stream.getAudioTracks()[0];
      const aTrackSelected = gotDevices.find(item => { 
        return item.label === aTrack.label && item.kind === 'audioinput';
      });
      // as this is not supported in some browsers
      // forseen error catch
      try {
        this.audioInputSelected = aTrackSelected.deviceId;
        console.log('----------->', aTrackSelected.label);
        // sinkId
        this.audioOutputSelected = publisherStorePrefs.audioOutDeviceId;
      } catch (error) {
        console.log('GUM catch: ', error);
        this.uiError = { name: error.name, message: error.message };
        this.overlay = false;
      }
    
      // console.log(this.constraints);
      //this.myVideo.nativeElement.srcObject = stream;
      const el = this.renderer.selectRootElement(document.getElementById('myVideo'), true);
      el.setAttribute('style', 'background: lightgreen;');
      el.muted = true;
      el.srcObject = stream;
      setTimeout(() => this.overlay = false, 1000);

      const audioContext = new AudioContext();
      this.inputNode = audioContext.createMediaStreamSource(this.stream);

    } else {
      console.log('media stream could not be created');
    }
    
  }

  // setting new values
  onSelectionChange(event, objValue) { 
    console.log('device onSelectionChange: ', event, objValue);    
    // event -> {source: MatRadioButton, value: "64c76e0e0615a921417810296460239cb63d949bdfbf0890cdc675fe73350675"}
    const deviceId = event.value;
    // init video switch
    if (objValue.type === 'videoinput') {
      this.videoInputSelected = deviceId;
      ;
      // store selected deviceId
      // publisherStorePrefs.videoDeviceId = objValue.deviceId;
      if(deviceId.indexOf('default') === -1){
        /**
         * as we hand over to the native OT properties for init publisher, we use the pure device ID for assigning a custom device
         */
        // this.constraints.video.deviceId = deviceId;
        this.publisherStore.setVideoSource(deviceId);
      } else {
        // this.constraints.video.deviceId = 'default';
        this.publisherStore.setVideoSource('default');
      }
      // this.ots.setSessionPrefs(this.sessionPrefs);
    };

    // audio inputs
    if (objValue.type === 'audioinput') {
      // this.wasClicked = true;
      // this.drawVolumeChange(deviceId);
      // store selected deviceId
      // this.sessionPrefs.audioInDeviceId = deviceId;
      // this.constraints.audio.deviceId = deviceId;
      if(deviceId.indexOf('default') === -1){
        // this.constraints.audio.deviceId = { ideal: deviceId };
        // this.constraints.audio.deviceId = { exact: deviceId };
        this.publisherStore.setAudioSource(deviceId);
      } else {
        // this.constraints.audio.deviceId = 'default';
        this.publisherStore.setAudioSource('default');
      }
      // this.ots.setSessionPrefs(this.sessionPrefs);
    };
    // audio outputs
    if (objValue.type === 'audiooutput') {
      // store selected sinkId
      // this.sessionPrefs.audioOutDeviceId = deviceId;
      this.publisherStore.setAudioOutDeviceId(deviceId);
      // this.ots.setSessionPrefs(this.sessionPrefs);
    }
  }


  refreshStorageSettings() {
    this.overlay = true;
    this.publisherStore.resetState();
    setTimeout(() => {
      this.overlay = false;
      this.completeButton = true;
    }, 3000);  
  }


  setMasterDelay(factor) {
    /*
    console.log(factor);
    // this.as.setMasterDelay(factor);
    this.masterDelay += factor;
    this.masterDelay = parseFloat(this.masterDelay.toFixed(3));
    this.as.masterDelay.delayTime.value = this.masterDelay;
    */
    this.as.setMasterDelayFactor(factor);
    this.masterDelay = this.as.getMasterDelay();
  }

  // ### REFACTOR ###

  selectAudioOption(event) {
    console.log('selectAudioOption -> value, selected: ', event.option.value, event.option.selected);
    switch(event.option.value) {
      case 'echoCancellation':
        this.publisherStore.setEchoCancellation(event.option.selected);
        break;
      case 'noiseSuppression':
        this.publisherStore.setNoiseSuppression(event.option.selected);
        break;
      case 'autoGainControl':
        this.publisherStore.setAutoGainControl(event.option.selected);
        break;
    }
  }

  setHeadphoneMode(event: boolean) {
    console.log('set headphone mode to: ', event);
    this.publisherStore.toggleHeadphoneMode(event);
  }

  setAudioBitrate(bitrateNumber) {
    // console.log(bitrateNumber);
    this.publisherStore.setAudioBitrate(bitrateNumber);

    // set in audioService as well
    const index = this.as.audioBitrateArr.findIndex(item => item === bitrateNumber); // || 1;
    // console.log(bitrateNumber, index);
    this.as.setBitrate(index);
  }

  selectVideoIn(deviceId) {
    this.publisherStore.setVideoSource(deviceId);
  }

  selectAudioIn(deviceId) {
    this.publisherStore.setAudioSource(deviceId);
  }

}
