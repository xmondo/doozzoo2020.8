import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { GumAccessComponent } from './gum-access.component';
import { MaterialSharedModule } from '../shared/material-shared/material-shared.module';
// notshared material modules
import { MatListModule } from '@angular/material/list';
import { BasicsSharedModule } from '../shared/basics-shared/basics-shared.module';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  { path: '', component: GumAccessComponent }
];

@NgModule({
  declarations: [GumAccessComponent],
  imports: [
    CommonModule,
    MaterialSharedModule,
    BasicsSharedModule,
    // not shared material
    MatListModule,
    RouterModule.forChild(routes),
    TranslateModule.forChild(),
  ]
})
export class GumAccessModule { }
