import { Component, OnInit } from '@angular/core';
import { from, Observable, of } from 'rxjs';
import { catchError, debounceTime, map, tap } from 'rxjs/operators';
import { Location } from '@angular/common'
import { DeviceDetectorService } from 'ngx-device-detector';
import { TranslateService } from '@ngx-translate/core';

export enum BrowserDetected {
  'chrome',
  'firefox',
  'safari',
  'other'
}
@Component({
  selector: 'app-gum-access',
  templateUrl: './gum-access.component.html',
  styleUrls: ['./gum-access.component.scss']
})

export class GumAccessComponent implements OnInit {

  accessAllowed$: Observable<boolean>;
  browserDetected$: Observable<string>;

  constructor(
    public location: Location,
    public deviceService: DeviceDetectorService, 
    public translateService: TranslateService,
  ) { }

  ngOnInit(): void {
    this.browserDetected$ = of(this.deviceService.browser)
      .pipe(
        // tap(val => console.log('+++', val)),
        map(val => val.toLowerCase()),
        map(val => ['chrome', 'firefox', 'safari'].includes(val) ? val : 'other'),
        // for debugging
        // map(() => 'safari'),
      )

    // console.log('+++', location)
    // success would be a valid MediaStream
    // fail is a "NotAllowedError" error
    this.accessAllowed$ = from(navigator.mediaDevices.getUserMedia({video: true, audio: true}))
      .pipe(
        // catchError(error => of(`GUM error: ${error.name}`)),
        catchError(error => of(error.name)),
        map(val => val === 'NotAllowedError' ? false : true),
        // tap((val: any) => console.log('accessAllowed: ', val)),
        tap(val => val ? setTimeout(() => this.location.back(), 3000) : 'stay here and solve the issue!'),
      );
    // console.log('currentLang', this.translateService.currentLang);
  }

}
