import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GumAccessComponent } from './gum-access.component';

describe('GumAccessComponent', () => {
  let component: GumAccessComponent;
  let fixture: ComponentFixture<GumAccessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GumAccessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GumAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
