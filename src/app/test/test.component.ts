import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {
  participantsArr: Observable<any>;
  keysOnly: Observable<any>;
  isWaiting: Observable<any>;
  inSession: Observable<any>;
  notInSession: Observable<any>;
  userId: string = '2X1ZM44k8lXX6t2w1X17fx76DPK2';
  keyArr: Array<string>;

  constructor(
    public db: AngularFireDatabase,
  ) { }

  ngOnInit(): void {

    
    this.participantsArr = this.db
      .list(
        `classrooms/${this.userId}/participants`,
        // ref => ref.orderByKey()
        ref => ref.orderByChild('name')
      )
      .snapshotChanges()
      .pipe(map((val:any) => val.map(_val => ({ key: _val.key, ..._val.payload.val() }))));

    // create a allways up to date key array for "all"-changes
    this.keyArr = [];
    const partSub = this.participantsArr
        .subscribe(val => this.keyArr = val.map(_val => _val.key));

    this.inSession = this.participantsArr
      .pipe(
        map((val:any) => {
          return val
            .map(_val => _val)
            .filter(item => item.access === true && item.inSession === true );
        })
      ); 

    this.isWaiting = this.participantsArr
      .pipe(
        map((val:any) => {
          return val
            .map(_val => _val)
            .filter(item => item.isWaiting === true );
        })
      ); 

    this.notInSession = this.participantsArr
      .pipe(
        map((val:any) => {
          return val
            .map(_val => _val)
            .filter(item => (!item.inSession || item.inSession === false) && !item.isWaiting === true);
        })
      );

  }

}
