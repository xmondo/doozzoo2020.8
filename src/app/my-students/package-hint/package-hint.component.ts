import { Component, Input, OnInit, OnChanges} from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-package-hint',
  templateUrl: './package-hint.component.html',
  styleUrls: ['./package-hint.component.scss']
})
export class PackageHintComponent implements OnInit, OnChanges {
  @Input() userId: string;
  @Input() studentList: Array<any>;
  quantity: Observable<any>;
  packageSize: number;
  numberOfActivatedStudents: number;
  constructor(
    private db: AngularFireDatabase,
  ) { }

  ngOnInit(): void {
    this.packageSize = 5;
    this.numberOfActivatedStudents = this.studentList.filter(val => val.value === true).length
    this.fetchQuantity();
  }

  ngOnChanges(changes){
    // console.log(changes);
    this.numberOfActivatedStudents = this.studentList.filter(val => val.value === true).length
  }

  fetchQuantity(){
    this.quantity = this.db
      .object(`/stripe_customers/${this.userId}/customerData/subscriptions/data/0/items/data/0/quantity`)
      .valueChanges()
      .pipe(map(val => val ?? 0));
  }

}
