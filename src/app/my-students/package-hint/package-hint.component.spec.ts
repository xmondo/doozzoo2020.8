import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackageHintComponent } from './package-hint.component';

describe('PackageHintComponent', () => {
  let component: PackageHintComponent;
  let fixture: ComponentFixture<PackageHintComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackageHintComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PackageHintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
