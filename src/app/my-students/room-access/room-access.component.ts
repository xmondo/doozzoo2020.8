import { Component, OnInit, Input } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-room-access',
  templateUrl: './room-access.component.html',
  styleUrls: ['./room-access.component.css']
})
export class RoomAccessComponent implements OnInit {
  @Input() userId: string;
  @Input() coachId: string;
  ref: any;
  roomAccess: Observable<any>;
  constructor(
    private rdb: AngularFireDatabase,
  ) {
    // 
  }

  ngOnInit() {
    // console.log(this.coachId, this.userId)
    this.ref = this.rdb.object(`coachCustomer/${this.coachId}/${this.userId}`);
    this.roomAccess = this.ref.valueChanges();
  }

  toggleRoomAccess(event) {
    // console.log(event);
    // take care of eventually waiting flags
    this.ref
      .set(event.checked)
      //.then(result => console.log(result))
      .catch(error => console.log(error));
  }
  
// end of class
}
