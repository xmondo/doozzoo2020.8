import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomAccessComponent } from './room-access.component';

describe('RoomAccessComponent', () => {
  let component: RoomAccessComponent;
  let fixture: ComponentFixture<RoomAccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomAccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
