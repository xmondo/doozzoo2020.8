import { Component, Input, OnInit, OnDestroy, NgZone } from '@angular/core';
import { AuthService } from '../../shared/auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { map, startWith } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmRemoveComponent } from '../confirm-remove/confirm-remove.component';
import { Observable, Subscription } from 'rxjs';
import { OpentokService } from 'src/app/shared/opentok.service';
import { ControllsService } from 'src/app/shared/controlls.service';
import { SessionStoreService } from 'src/app/room/session-store.service';


export interface ParticipantList {
  key?: string, // key must not be provided as it is inherited from the firebase parent node
  access: boolean,
  name?: string,
  inSession?: boolean,
  isWaiting?: boolean,
  isRegistered?: boolean,
  toolbarStatus?: boolean,
  registrationStatus: 'isCoachCustomer' | 'isRegistered' | 'isAnonymous',
}

@Component({
  selector: 'app-participant-list',
  templateUrl: './participant-list.component.html',
  styleUrls: ['./participant-list.component.scss'],
})
export class ParticipantListComponent implements OnInit, OnDestroy {
  @Input() userId: string;
  @Input() roomId: string;

  studentListRef: any;
  studentList: any;

  // participantsArr: Observable<ParticipantList[]>;
  isWaiting: Observable<ParticipantList[]>;
  inSession: Observable<ParticipantList[]>;
  notInSession:  Observable<ParticipantList[]>;
  toolbarStatus$:  Observable<ParticipantList[]>;

  keyArr: Array<string>;
  subContainer: Subscription = new Subscription();
  toggleAccessAllValue: boolean = true;

  toggleSortValue: 'asc' | 'desc' = 'asc';

  roomLockObs: Observable<any> = null;

  vm$: Observable<any>;

  constructor(
    public a: AuthService,
    public db: AngularFireDatabase,
    public dialog: MatDialog,
    public cs: ControllsService,
    private store: SessionStoreService,
  ) { 
    // make the store view model available
    this.vm$ = this.store.vm$;
  }

  ngOnInit() {
    this.getParticipantsArr();
    // subscribe as read only for the sattelite lock button
    // we have to wrap the boolean, otherwise we can not use "Observable as"
    this.roomLockObs = this.db
      .object(`now/${this.roomId}/locked`)
      .valueChanges()
      .pipe(map(val => { return { locked: val }}))
  }

  ngOnDestroy(){
    this.subContainer.unsubscribe();  
  }

  getParticipantsArr() {
    // fill store
    // this.store.getParticipants(this.userId);

    // general Observable
    /*
    this.participantsArr = this.db
      .list(
        `classrooms/${this.userId}/participants` //,
        // ref => ref.orderByKey()
        // ref => ref.orderByChild('name')
      )
      .snapshotChanges()
      .pipe(map((val:any) => val.map(_val => ({ key: _val.key, ..._val.payload.val() }))));
    */

    this.inSession = this.store.participants$
      .pipe(
        map((val:any) => {
          return val
            .map(_val => _val)
            .filter(item => item.inSession === true );
        })
      ); 

    this.isWaiting = this.store.participants$
      .pipe(
        map((val:any) => {
          return val
            .map(_val => _val)
            .filter(item => item.isWaiting === true );
        })
      ); 

    this.notInSession = this.store.participants$
      .pipe(
        map((val:any) => {
          return val
            .map(_val => _val)
            .filter(item => (!item.inSession || item.inSession === false) && !item.isWaiting === true);
        })
      );

    // create a allways up to date key array for "all"-changes
    this.keyArr = [];
    const partSub = this.store.participantsKeyList$
        .subscribe(val => this.keyArr = val.map(_val => _val.key));
    this.subContainer.add(partSub);
  }

  toggleAccess(item, event) {
    // console.log(item, event);
    // sets both, with regard to event
    const updateData: any = {
      access: event.checked  
    }
    // if user was inSession, set "isWaiting" to true, as he is send into his waitingzone
    // if user was not in session, set isWaiting to the default "false"
    this.db
      .object('classrooms/' + this.userId + '/participants/' + item.key)
      .update(updateData)
      // .then(_result => console.log(_result))
      .catch(error => console.log(error));
  }

  toggleAccessAll(event) {
    //console.log('event, keyArr', event, this.keyArr);
    this.toggleAccessAllValue = event.checked;
    const updateData: any = {
      access: event.checked  
    }
    // If I click, I have seen the notification, so we reset it
    // event.checked ? updateData.isWaiting = false : '';
    this.keyArr
      .forEach((key:string) => {
        this.db
          .object(`classrooms/${this.userId}/participants/${key}`)
          .update(updateData)
          //.then(_result => console.log(_result))
          .catch(error => console.log(error));
    });
  }

  /**
   * unfortunately it needs two clicks to execute sorting correctly, somehow refresh of the ngFor is not triggered -->
  */
  toggleSort() {
    this.toggleSortValue = this.toggleSortValue === 'asc' ? 'desc' : 'asc';
    // console.log(this.toggleSortValue);
  }

  removeAnonymous(key) {
    //console.log('remove... ', key)
    this.db
      .object('classrooms/' + this.userId + '/participants/' + key)
      .remove();
  }

  confirmRemove(key): void {
    //console.log(key);
    const dialogRef = this.dialog.open(ConfirmRemoveComponent, {
      width: '350px',
      data: { 
        // dialogTitle: ,
        context: 'anonymousParticipants',
      }
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        //console.log('The dialog was closed', result);
        // result is 'true' or undefined
        if (result === true) {
          // trigger delete
          this.removeAnonymous(key);
        }
      });
  }

  assignSender(item: ParticipantList , checked: boolean) {
    console.log('checked: ', checked);
    // this.toolbarStatus = checked;
    this.db
      .object('classrooms/' + this.userId + '/participants/' + item.key)
      .update({ toolbarStatus: checked })
      .catch(error => console.log(error));
  }

  trackByFn(index, item){
    // return item ? item.key : undefined;
    // console.log(item.key)
    return item.key;
  }

}
