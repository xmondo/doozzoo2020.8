import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../../shared/auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { map, filter, distinctUntilChanged } from 'rxjs/operators';
import { ToClipboard } from '../../shared/to-clipboard';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-invite-student',
  templateUrl: './invite-student.component.html',
  styleUrls: ['./invite-student.component.scss'],
  providers: [ ToClipboard ]
})
export class InviteStudentComponent implements OnInit {
  @Input() userId: string;
  @Input() layout: string; // coach | institution
  localProgress: Boolean = false;
  inviteListRef: any;
  inviteList: any;
  // userId: any;
  tld: string; // = 'https://' + window.location.hostname + ':' + window.location.port + '/invite/';
  stripePlan: any;
  students: any;
  betatester: any;

  constructor(
    public db: AngularFireDatabase,
    public a: AuthService,
    private c: ToClipboard,
    public snackBar: MatSnackBar,
  ) { }

  ngOnInit() {
    if(window.location.port) {
      this.tld = 'https://' + window.location.hostname + ':' + window.location.port + '/invite/';
    } else {
      this.tld = 'https://' + window.location.hostname + '/invite/';
    }
    
    this.localProgress = true;

    this.getInviteLinks();

    /*
    this.a.usr$
      .pipe(
        // filter out undefined|null events
        filter(val => val !== null),
        // filer out null ids
        filter((val: any) => val.uid !== null),
        distinctUntilChanged(),
      )
      .subscribe(val => {
        this.userId = val.uid;
        // console.log('val: ', val);
        if (val.betatester === true) {
          this.betatester = val.betatester;
          this.students = this.db.list('coachCustomer/' + this.userId).valueChanges();
        } else {
          this.getStripeSubscription();
        }
        this.getInviteLinks();

      });
    */
  }

  getInviteLinks() {
    // console.log('userId: ', this.userId)
    this.inviteListRef = this.db
      .list(
        'invites',
        ref => ref.orderByChild('uid').equalTo(this.userId)
      );
    this.inviteList = this.inviteListRef
      .snapshotChanges()
      // .valueChanges()
      // .stateChanges()
      .pipe(
        map((changes: any) =>
          // mapping the object to the relevant values
          changes.map(_changes => ({ key: _changes.key, active: _changes.payload.val().active }))
        )
      );

    this.localProgress = false;
  }

  createInviteLink() {
    // ...
    this.inviteListRef.push({
        'uid': this.userId,
        'nickname': this.a.userData.displayName,
        'count': 0,
        'active': false,
        'timestamp': Date.now()
    });
  }

  toggleLinkStatus(key, event) {
    this.inviteListRef
      .update(key, { active: event.checked })
      .then(_result => console.log(_result))
      .catch(error => console.log(error));
  }

  remove(key) {
    // ...
    this.inviteListRef.remove(key);
  }

  // ### get stripe basics ###
  getStripeSubscription() {
    // fetch the latest subscription trigger subscription triggers

    const cust = this.db
      .object('/stripe_customers/' + this.userId + '/subscriptions/currentSubscription/quantity')
      .valueChanges();

    this.stripePlan = cust.pipe(
      map((val: any) => {
        if (this.betatester) {
          return 5;
        }
        if (val === null) {
          return 0;
        } else {
          return val;
        }
      })
    );

    this.students = this.db.list('coachCustomer/' + this.userId).valueChanges();

  }

/*

  this.snackBar.open( 'Room URL was copied to clipboard: ' + fullUrl, '', { duration: 4000 } );

  var ref = firebase.database().ref().child('invites/' + self.profile.id );

  var getInviteLinks = function(){
      var ref = firebase.database().ref().child('invites/');
      // define query
      var query = ref.orderByChild("uid").equalTo(self.profile.id);
      // the $firebaseArray service properly handles database queries as well 
      self.arr = $firebaseArray(query);

      // promise resolution state
      self.arr.$loaded()
          .then(function(data) {
              //$rootScope.loadingStatus = false;
              self.localLoadingStatus = false;
          })
          .catch(function(error) {
              console.log('getCustomer->error: ' + error);
          });
  }

  this.createInviteLink = function(){
      // read the status once
      // could be true, false, null (not existing yet)
      
      // in case no nickname exists
      self.profile.nickname = self.profile.nickname || self.profile.email.split('@')[0];
      if(self.arr.length < 5){
          self.arr.$add({
              'uid': self.profile.id,
              'nickname': self.profile.nickname,
              'count': 0,
              'timestamp': firebase.database.ServerValue.TIMESTAMP
          });

      } else {
         self.showInfo('','Only a maximum of 5 open invite links is allowed.')  
      }
      
  }
*/


// end of class
}
