import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../../shared/auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { map, mergeMap, filter, defaultIfEmpty, toArray } from 'rxjs/operators';
import { ConfirmRemoveComponent } from '../confirm-remove/confirm-remove.component';
import { MatDialog } from '@angular/material/dialog';
import { combineLatest, from, iif, Observable, of } from 'rxjs';
import { DataSnapshot } from 'firebase-functions/lib/providers/database';

@Component({
  selector: 'app-studentlist',
  templateUrl: './studentlist.component.html',
  styleUrls: ['./studentlist.component.scss']
})
export class StudentlistComponent implements OnInit {
  @Input() userId: string;
  localProgress = false;
  // in this case -> coachId
  // userId: string;
  studentListRef: any;
  studentList$:  Observable<any>;
  studentListSorted$: Observable<any>;

  constructor(
    public a: AuthService,
    public db: AngularFireDatabase,
    public dialog: MatDialog,
  ) { 
    // console.log('userId', this.userId);
  }

  ngOnInit() {
    this.localProgress = true;
    
    this.getStudentsArr();
    /*
    this.a.usr$
      .subscribe(val => {
        this.userId = val.uid;
        this.getStudentsArr();
      });
    */
  }

  getStudentsArr() {
    this.studentListRef = this.db.list('coachCustomer/' + this.userId);
    this.studentListSorted$ = this.studentListRef
      .snapshotChanges()
      .pipe(
        // we mergeMap the array input from FB datasnapshot
        // is array is empty, the observable will not complete
        // by using the conditional iif, we emit in this case an artificial empty array "of([])"
        // otherwise combine the latest results of the inner observable, returned form the helperfunction "getNicknames"
        // WHY: we want 
        mergeMap((snapArr: Array<DataSnapshot>) => iif(() => snapArr.length === 0, of([]), combineLatest(snapArr.map(item => this.getNicknames(item))))) //
      );
  }

  getNicknames(item) {
    return this.db // DataSnapshot) => this.db
      .object(`profiles/${item.key}/contactData/nickname`)
      .valueChanges()
      .pipe(
        map(val => {
          const name = val ?? item.key;
          return {
            key: item.key,
            value: item.payload.val(), // is correct but shows intellisense error -> payload
            name: name,
          }
        }),
      )
  }

  trackByFn(index, item){
    return item.key;
  }

  toggleAccess(item, event) {
    console.log(item, event);
    // updated for primitives are not possible, 
    // use "set" in this case
    this.studentListRef
      .set(item.key, event.checked)
      .then(_result => console.log(_result))
      .catch(error => console.log(error));
  }

  confirmRemove(item): void {
    console.log(item);
    const dialogRef = this.dialog.open(ConfirmRemoveComponent, {
      width: '350px',
      data: { clipTitle: item.title }
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        console.log('The dialog was closed', result);
        // result is 'true' or undefined
        if (result === true) {
          // trigger delete
          this.studentListRef
            .remove(item.key)
            .then(_result => console.log(_result))
            .catch(error => console.log(error));
        }
      });
  }

}
