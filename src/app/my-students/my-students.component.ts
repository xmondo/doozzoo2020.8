import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../shared/auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFirestore } from '@angular/fire/firestore';
import { ConfirmRemoveComponent } from './confirm-remove/confirm-remove.component';
import { MatDialog } from '@angular/material/dialog';
import { map, filter } from 'rxjs/operators';
import { Subscription, Observable } from 'rxjs';

@Component({
  selector: 'app-my-students',
  templateUrl: './my-students.component.html',
  styleUrls: ['./my-students.component.scss']
})
export class MyStudentsComponent implements OnInit, OnDestroy {
  studentListRef: any;
  studentList: any;
  coaches: any;
  userId: string;
  coachCustomerRef: any;
  localProgress: Boolean = false;
  userSub: Subscription;
  institutionId: string = null;
  institutionCoach: Observable<any>;

  constructor(
    public a: AuthService,
    public db: AngularFireDatabase,
    public afs: AngularFirestore,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.localProgress = true;
    this.userSub = this.a.usr$
      .pipe(
        // distinctUntilChanged(),
        filter(val => val.uid !== null)
      )
      .subscribe(val => {
        // console.log('my-students->usr: ', val);
        this.userId = val.uid;
        this.getStudentsArr();
        if(val.triggerType === 'institution') {
          this.getInstitution(val.initiatorId);
          this.institutionId = val.initiatorId;
        }
      });
  }

  ngOnDestroy() {
    this.userSub.unsubscribe();  
  }

  getStudentsArr() {
    this.studentListRef = this.db
      .list(
        'coachCustomer/' + this.userId,
        ref => ref.orderByKey()
      );
    this.studentList = this.studentListRef
      .snapshotChanges()
      .pipe(
        map((changes: any) =>
          // mapping the object to the relevant values
          changes.map(_changes => ({ key: _changes.key, value: _changes.payload.val() }))
        )
      );
    this.localProgress = false;
  }

  confirmRemove(item): void {
    console.log(item);
    const dialogRef = this.dialog.open(ConfirmRemoveComponent, {
      width: '350px',
      data: { 
        dialogTitle: item.title,
        context: 'myStudents',
      }
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        console.log('The dialog was closed', result);
        // result is 'true' or undefined
        if (result === true) {
          // trigger delete
          this.studentListRef
            .remove(item.key)
            .then(_result => console.log(_result))
            .catch(error => console.log(error));
        }
      });
  }

  toggleAccess(item, event) {
    console.log(item, event);
    // updated for primitives are not possible, 
    // use "set" in this case
    this.studentListRef
      .set(item.key, event.checked)
      .then(_result => console.log(_result))
      .catch(error => console.log(error));

  }

  getInstitution(initiatorId) {
    
    this.institutionCoach = this.afs
      .collection('institutions')
      .doc(initiatorId)
      .collection('accounts', ref => ref.where('userId', '==', this.userId))
      .valueChanges({idField: 'key'})
      .pipe(
        filter(val => val !== null),
        map((val: any) => {
          const data = { 'activated': val[0].activated, 'allowInvites': val[0].allowInvites };
          return data;
        }),
      );
  }

// end of class
}
