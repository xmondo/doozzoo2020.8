import { Component, OnInit, OnDestroy } from '@angular/core';
import { AngularFireDatabase, AngularFireObject } from '@angular/fire/database';
import { AuthService } from '../shared/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { FormControl, Validators } from '@angular/forms';
import { pipe, Subscription } from 'rxjs';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarRef } from '@angular/material/snack-bar';
import { distinctUntilKeyChanged, mergeMap, map, filter, distinctUntilChanged, debounce, debounceTime, takeWhile, skipWhile, startWith, take, throttle, throttleTime, tap } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { ParticipantList } from '../my-students/participant-list/participant-list.component';

export interface waitingroomData {
  roomOwner: string,
  roomName: string,
  roomToken: string,
  roomId: string,
  roomLocked: boolean,
  userId: string,
  role: number, // in case role is still missing (null)
  displayName: string,
}

@Component({
  selector: 'app-waiting-room',
  templateUrl: './waiting-room.component.html',
  styleUrls: ['./waiting-room.component.scss']
})
export class WaitingRoomComponent implements OnInit, OnDestroy {
  // roomId: string;
  locked: Observable<boolean>;
  participants: Observable<any>;
  userId: string;
  lockedValue: boolean = true; // synchronous

  anonymousName = new FormControl('', [ Validators.required ]);
  roomOwner: string;
  roomName: string;
  roomToken: string;
  displayName: string;
  participantRef: any;
  
  anonymousParticipant: Observable<any>;
  anonymousParticipantState: Observable<any>;
  nameSub: Subscription;

  registeredParticipant: Observable<any>;
  registeredParticipantAccess: boolean = null;
  regPartSub: Subscription;

  subContainer: Subscription = new Subscription();
  snackBarConfig: MatSnackBarConfig;

  mergedRoomObs: Observable<waitingroomData>

  constructor(
    public ats: AuthService,
    private db: AngularFireDatabase,
    public aRoute: ActivatedRoute,
    public router: Router,
    public snackBar: MatSnackBar,
    private translate: TranslateService,
  ) {
    //...
    this.snackBarConfig = new MatSnackBarConfig();
    this.snackBarConfig.panelClass = ['background-orange'];
    this.snackBarConfig.duration = 4000;
    this.snackBarConfig.verticalPosition = 'top';
  }

  ngOnInit() {
    const prefix = this.aRoute.snapshot.url[0].path;
    // const roomToken = this.aRoute.snapshot.url[1].path;
    this.roomToken = this.aRoute.snapshot.params.roomId;
    const fullToken = prefix + '_' + this.roomToken;
   
    // console.log('waiting-room->fullToken: ', fullToken)

    const roomObs = this.db
        .object(`now/${fullToken}`)
        .valueChanges();

    this.mergedRoomObs = roomObs
      .pipe(
        mergeMap(
          (_val: any) => this.ats.usr$
            .pipe(
              filter(val => _val !== null && _val !== undefined),
              // tap(val => console.log('*** mergedRoomObs: ', _val, val)),
              map((val: any) => {
                const data = {
                  roomOwner: _val.owner,
                  roomName: _val.name,
                  prefix: prefix,
                  roomToken: this.roomToken,
                  roomId: fullToken, // not the best solution, hmmm...
                  roomLocked: _val.locked,
                  participantQuotaFull: _val.participantQuotaFull,
                  userId: val.uid,
                  role: val.role, //  ? val.role : -2, // in case role is still missing (null)
                  displayName: val.displayName,
                }
                return data;
              },
              tap(val => console.log('mergedRoomObs: ', val)),
            ),
          )
        )
      );

    // in case the room is destroyed, redirect at home
    const redirectSub = roomObs
      // .pipe(tap(val => console.log('mergedRoomObs: ', val)))
      .subscribe(val => !val ? this.redirectHome() : '');
    this.subContainer.add(redirectSub);

  }

  ngOnDestroy() {
    this.subContainer.unsubscribe(); 
  }

  fetchRoom(val:any) {
    try {
      // handles anonymous users
      val.role === -1 ? this.fetchAnonymous(val.roomOwner, val.userId) : '';
      // handles al registered users
      val.role > -1 ? this.fetchRegistered(val.roomOwner, val.userId) : '';
      this.userId = val.userId
      this.roomOwner = val.roomOwner;
      this.roomName = val.roomName;
/* 
      this.locked = <Observable<boolean>>this.db
        .object(`classrooms/${this.roomOwner}/locked`)
        .valueChanges()
        .pipe(
          startWith(true),
          map(val => val ?? true)
        );
*/
    } catch (error) {
      // most probably room does not exist
      this.redirectHome();
    }
   
  }

  redirectHome(){
    /* Session has been closed, redirecting to homepage... */
    this.translate.get('room.waitingroom.session-closed-redirect-home')
      .subscribe(( val: string ) => {
        this.snackBar.open( val, '', this.snackBarConfig );
      });
    setTimeout(() => this.router.navigateByUrl('/'), 500);
  }

  async saveName(){
    console.log(this.anonymousName.value);

    const userObj = await this.ats.auth$
      .signInAnonymously()
      .catch(function(error) {
        // Handle Errors here.
        console.log(error);
      // ...
      });
    this.userId = userObj.user.uid;
    // console.log('uid: ', userObj.user.uid)

    const data = {
      access: false,
      // isAnonymous: true,
      name: this.anonymousName.value,
      inSession: false,
      isWaiting: true,
      registrationStatus: 'isAnonymous',
    }

    // if room is not locked, 
    // participant can enter directly
    if(this.lockedValue === false) {
      data.access = true;
      data.isWaiting = false;
    }
    
    await this.db
      .object(`classrooms/${this.roomOwner}/participants/${this.userId}`)
      .set(data);

    this.displayName = this.anonymousName.value;
    this.ats.setDisplayName(this.anonymousName.value);

    // disable form field
    this.anonymousName.disable();

  }

  // TODO: loops if ....
  // query.once('value')???
  fetchAnonymous(roomOwner: string, userId: string) {
    this.anonymousParticipant = this.db
      .object(`classrooms/${roomOwner}/participants/${userId}`)
      .valueChanges();

    this.nameSub.unsubscribe();
    this.nameSub = this.anonymousParticipant
      .pipe(
        debounceTime(1000),
        // distinctUntilKeyChanged('access'),
        map((val:any) => {
          // console.log('### pre', val)
          if(val === null || val === undefined) {
            return null;
          } else if (val.name === null || val.name === undefined) {
            return null;
          } else {
            return val.name;
          }
        }),
      )
      .subscribe( val => {
        // console.log('### post', val)
        if(val !== null) {
          this.ats.setDisplayName(val);
          this.displayName = val;
          this.anonymousName.disable();
        } else {
          this.displayName = null;
          this.ats.setDisplayName(null);
          this.anonymousName.setValue(null);
          this.anonymousName.enable();
        }
      });
    this.subContainer.add(this.nameSub);

  }

  fetchRegistered(roomOwner: string, userId: string){
    // console.log('fetch registered')
    this.regPartSub.unsubscribe();
    this.regPartSub = this.db
      .object(`classrooms/${roomOwner}/participants/${userId}`)
      //.stateChanges()
      .valueChanges()
      .pipe(
        map((val: any) => {
          // console.log('+++', val);
          return val === null ? null : val.access;
        }),
        //filter(val => val.key === 'access'),
      )
      //.subscribe((val: any) => this.registeredParticipantAccess = val.val());
      .pipe(distinctUntilChanged())
      .subscribe((val: any) => this.registeredParticipantAccess = val);
    this.subContainer.add(this.regPartSub);
  }

  // TBD: if session is unlocked: should everyone get in independent from individual settings?
  checkAccessRegistered(): boolean {
    return this.registeredParticipantAccess === null ? true : !this.registeredParticipantAccess;
  }
  checkDisableRequestAccess() {
    // not disable when room is locked and 
    // registered user did not request access
    return this.registeredParticipantAccess === null ? false : true;
  }

  saveNameRegistered() {
    //
    if(this.roomOwner !== this.userId) {
      const data: ParticipantList = {
        access: false,
        // isAnonymous: true,
        isRegistered: true,
        registrationStatus: 'isRegistered',
        name: this.displayName,
        inSession: false,
        isWaiting: true,
      }
      
      // if room is not locked, 
      // participant can enter directly
      if(!this.lockedValue) {
        data.access = true;
        data.isWaiting = false;
      }

      this.db
        .object(`classrooms/${this.roomOwner}/participants/${this.userId}`)
        .set(data)
        //.then(() => console.log('saveNameRegistered -> ', this.userId, data))
        .catch(e => console.log('saveNameRegistered: ', e));
    } else {
      // do nothing...
    }
  }

  /**
   * Denkfehler: es gibt 3 Arten von unregistrierten user:
   * 1. registriert && mit dem coach verbunden = in der participants liste
   * 2. registriert aber NICHT mit dem coach verbunden
   * 3. anonymer
   * 
   * brauche Lösung für temporäre Participants
   * --> wenn nicht in Liste sollte beitreten wie anonymous
   */

}
