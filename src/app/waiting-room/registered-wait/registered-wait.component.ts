import { Component, Input, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { distinctUntilChanged, first, map, startWith } from 'rxjs/operators';
import { ParticipantList } from 'src/app/my-students/participant-list/participant-list.component';
import { waitingroomData } from '../waiting-room.component';

@Component({
  selector: 'app-registered-wait',
  templateUrl: './registered-wait.component.html',
  styleUrls: ['./registered-wait.component.scss']
})
export class RegisteredWaitComponent implements OnInit {
  @Input() waitingroomData: waitingroomData;
  participant: Observable<any>;
  participantAccess: Observable<boolean>;
  isOwner: boolean;

  constructor(
    private db: AngularFireDatabase,
  ) { }

  ngOnInit(): void {
    console.log
    this.isOwner = this.waitingroomData.userId === this.waitingroomData.roomOwner ? true : false;
    this.isOwner ? '' : this.fetchRegistered(); 
    !this.isOwner && this.waitingroomData.role > -1 ? this.saveNameRegistered() : '';
  }

  fetchRegistered(){
    // console.log('fetch registered')
    // Important this Observable will return the following LOV: null | true | false
    // null = no participat record at all
    const participantObs = this.db
      .object(`classrooms/${this.waitingroomData.roomOwner}/participants/${this.waitingroomData.userId}`)
      .valueChanges()
    
    // does participant exists at all?
    this.participant = participantObs
      .pipe(
        // startWith(false),
        // map(val => val !== null ?? false)
      );

  }

  saveNameRegistered() {
    //
    const data: ParticipantList = {
      access: false,
      name: this.waitingroomData.displayName,
      inSession: false,
      isWaiting: true,
      registrationStatus: 'isRegistered',
    }
    
    // if room is not locked, 
    // participant can enter directly
    if(!this.waitingroomData.roomLocked) {
      data.access = true;
      data.isWaiting = false;
    }

    this.db
      .object(`classrooms/${this.waitingroomData.roomOwner}/participants/${this.waitingroomData.userId}`)
      .set(data)
      .then(() => console.log('saveNameRegistered -> ', this.waitingroomData.userId, data))
      .catch(e => console.log('saveNameRegistered: ', e));
  }

}
