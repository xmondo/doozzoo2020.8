import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisteredWaitComponent } from './registered-wait.component';

describe('RegisteredWaitComponent', () => {
  let component: RegisteredWaitComponent;
  let fixture: ComponentFixture<RegisteredWaitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisteredWaitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisteredWaitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
