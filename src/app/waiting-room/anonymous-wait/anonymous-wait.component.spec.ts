import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnonymousWaitComponent } from './anonymous-wait.component';

describe('AnonymousWaitComponent', () => {
  let component: AnonymousWaitComponent;
  let fixture: ComponentFixture<AnonymousWaitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnonymousWaitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnonymousWaitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
