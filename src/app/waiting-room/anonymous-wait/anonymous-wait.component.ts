import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { waitingroomData } from '../waiting-room.component';
import { AngularFireDatabase, AngularFireObject } from '@angular/fire/database';
import { AuthService } from '../../shared/auth.service';
import { Observable, Subscription } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { ParticipantList } from 'src/app/my-students/participant-list/participant-list.component';

@Component({
  selector: 'app-anonymous-wait',
  templateUrl: './anonymous-wait.component.html',
  styleUrls: ['./anonymous-wait.component.scss']
})
export class AnonymousWaitComponent implements OnInit, OnDestroy {
  @Input() waitingroomData: waitingroomData;
  anonymousName = new FormControl('', [ Validators.required ]);
  subContainer: Subscription = new Subscription();
  nameSub: Subscription;
  anonymousParticipant: Observable<any>;
  participantAccess: boolean = false;

  debug: boolean = true;
  
  constructor(
    public ats: AuthService,
    private db: AngularFireDatabase,
    //public aRoute: ActivatedRoute,
    //public router: Router,
    //public snackBar: MatSnackBar,
    //private translate: TranslateService,
  ) { }

  ngOnInit(): void {
    const state: boolean = this.waitingroomData.userId ? true : false;
    state ? this.fetchAnonymous() : '';
  }

  ngOnDestroy(): void {
    this.subContainer.unsubscribe(); 
  }

  async saveName(){
    // console.log(this.anonymousName.value);

    const userObj = await this.ats.auth$
      .signInAnonymously()
      .catch(function(error) {
        // Handle Errors here.
        console.log(error);
      // ...
      });
    this.waitingroomData.userId = userObj.user.uid;
    // console.log('uid: ', userObj.user.uid)

    const data: ParticipantList = {
      access: false,
      name: this.anonymousName.value,
      inSession: false,
      isWaiting: true,
      registrationStatus: 'isAnonymous'
    }

    // if room is not locked, 
    // participant can enter directly
    // but indicate, he is still waiting
    if(this.waitingroomData.roomLocked === false) {
      data.access = true;
      data.isWaiting = true;
    }
    
    await this.db
      .object(`classrooms/${this.waitingroomData.roomOwner}/participants/${this.waitingroomData.userId}`)
      .set(data);

    // persist display name for publishing
    this.ats.setDisplayName(this.anonymousName.value);

    // disable form field
    this.anonymousName.disable();

    // invoke subscription of new participant
    // this toggle al the UI relevant data
    this.fetchAnonymous();

  }

    // TODO: loops if ....
  // query.once('value')???
  fetchAnonymous() {

    this.anonymousParticipant = this.db
      .object(`classrooms/${this.waitingroomData.roomOwner}/participants/${this.waitingroomData.userId}`)
      .valueChanges();

    this.nameSub = this.anonymousParticipant
      .pipe(
        // debounceTime(1000),
        // distinctUntilKeyChanged('access'),
        map((val:any) => {
          // console.log('### pre', val)
          return val ?? null;
        }),
      )
      .subscribe( val => {
        // console.log('### AnonymousWaitComponent->fetchAnonymous: ', val);
        if(val !== null) {
          const name = val.name ?? null;
          this.ats.setDisplayName(name);
          this.waitingroomData.displayName = name;
          this.anonymousName.disable();
          this.participantAccess = val.access ?? null;
        } else {
          this.waitingroomData.displayName = null;
          this.ats.setDisplayName(null);
          this.anonymousName.setValue(null);
          this.anonymousName.enable();
          this.participantAccess = false;
        }
      });
    this.subContainer.add(this.nameSub);

  }


}
