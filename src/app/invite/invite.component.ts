import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { AuthService } from '../shared/auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { filter, map, merge, mergeMap, concat, take, takeLast, distinctUntilChanged, distinctUntilKeyChanged } from 'rxjs/operators';
import { of, from, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { ControllsService } from '../shared/controlls.service';

@Component({
  selector: 'app-invite',
  templateUrl: './invite.component.html',
  styleUrls: ['./invite.component.scss']
})
export class InviteComponent implements OnInit, OnDestroy {
  inviteToken: string;
  inviteTokenData: any = null;
  localProgress = false;
  success: string;
  error: boolean = false;
  inviteTokenListener: any;
  inviteListener: any;
  dsptchr: any;
  dsptchrSub: any;
  postInviteStatus: boolean = true;
  action: string;
  inviteLookup: any;
  profileMissingStatusSub: Subscription;

  constructor(
    public db: AngularFireDatabase,
    public a: AuthService,
    public router: Router,
    private r: ActivatedRoute,
    public cs: ControllsService,
    public ref: ChangeDetectorRef,
  ) {

    const resData = this.r.snapshot.data.inviteData;

    this.dsptchr = resData.relatedTokenData
      .pipe(
        mergeMap((val: any) => this.a.usr$.pipe(
          filter(val => val !== null),
          // distinctUntilChanged(),
          // take(1),
          map((_val: any) => {
            // console.log('###', val, _val);
            // if val === null, break by returning error obj
            // same if coachId equals userId, you can not invite yourself
            if (!val) {
              return { action: 'emptyVal'};
            } else if (val.coachId === _val.uid) {
              return { action: 'sameCoach'}
            }
            // else define actions for further processing
            // we need 3 stati:
            // role = null or < -1 => register
            // role =  -1 => completeProfile
            // role > -1 => userExists -> start over
            if (val.action !== 'faultyToken') {
              val.action = _val.role > -1 ? 'userExists' : 'registerOrLogin';
            } 
            val.customerId = _val.uid;
            val.role = _val.role;
            val.emailVerified = _val.emailVerified;
            return val;
          })
        ))
    );

    this.dsptchrSub = this.dsptchr
      .subscribe(val => {
        // console.log('dsptchrSub->', val, val.coachId);
        // inject data for local storage

        try {
          this.inviteTokenData = { 
            nickname: val.nickname, 
            coachId: val.coachId, 
            timestamp: val.timestamp, 
            inviteToken: val.inviteToken,
            userActions: val.userActions,
            initiatorId: val.initiatorId,
          };
        }
        catch(error){
          console.log('dsptchrSub->error: ', val);
          this.inviteTokenData = { 
            nickname: val.nickname, 
            coachId: val.coachId, 
            timestamp: val.timestamp, 
            inviteToken: val.inviteToken,
            // userActions: val.userActions,
            // initiatorId: val.initiatorId,
          };
        }
        console.log('dsptchrSub->', val);

        switch (val.action) {
          case 'emptyVal':
            this.error = true;
          break;

          case 'faultyToken':
            this.error = true;
          break;

          case 'userExists':
            this.dsptchrSub.unsubscribe();
            this.error = false;
            this.success = 'afterRegistrationOrLogin';
            this.storeInvite(val);
          break;

          case 'registerOrLogin':
            this.error = false;
          break;

        }
      });

      this.profileMissingStatusSub = this.a.profileMissingStatus$
        .pipe(
          distinctUntilChanged(),
        )
        .subscribe(val => {
          console.log('invite->profileMissingStatus: ', val);
          val ? this.router.navigate(['login']) : '';
        })

  // constructor end
  }

  /**
   * 1. check if token is there
   * 2. then lookup if token is valid and fetch regarding data
   * 3. merge map lookup for role in the pipe,
   *    so that the subscription fetches a consolidated data set
   */
  ngOnInit() {
    //...  
  }

  ngOnDestroy() {
    // avoid memoryleaks
    this.dsptchrSub.unsubscribe();
    this.profileMissingStatusSub.unsubscribe();
  }

  inviteRegister() {
    // persist invite, for refetching it after e-Mail-verification
    console.log('inviteRegister: ', this.inviteTokenData);
    localStorage.setItem('doozzooInvite', JSON.stringify(this.inviteTokenData));
    this.cs.do('loginDialog', { tabIndex: 1 })
  }

  storeInvite(val) {
    console.log('storeInvite: ', val.userActions, val);

    // flag invite to trigger backendFunction
    this.db
      .object('invites/' + val.inviteToken)
      .update({ inviteeId: val.customerId })
      .then(result => console.log('customer ', val.customerId, ' was assigned to coach: ', val.coachId, result))
      .catch(error => console.log(error))

    this.db
      .object('coachCustomer/' + val.coachId)
      .update({ [val.customerId]: true })
      .then(() => {
        console.log('customer ', val.customerId, ' was assigned to coach: ', val.coachId);

        // ### cleaning up ###
        // stop subscription
        this.dsptchrSub.unsubscribe();

        // remove used token
        this.db
          .object('invites/' + val.inviteToken)
          .remove();

/*
        // TODO: move all of that into background process
        // Update user with institutional flag
        if (val.userActions === 'createdByInstitution') {
          this.db
          .object('user/' + val.customerId)
          .update({ 
            userActions: 'createdByInstitution',
            trigger: {
              initiatorId: val.initiatorId,
              triggerId: val.coachId,
              type: 'institution'
            }
          })
          .then(result => console.log('customer ', val.customerId, 'was flaged as institutional', result))
          .catch(error => console.log(error))
        }
*/
        // finally
        this.error = false;
      })
      .catch(error => console.log(error));
  }

  //       //
  // delete from local storage
  // localStorage.removeItem('doozzooInvite');

// end of class
}
