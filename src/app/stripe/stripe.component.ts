import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectorRef,
  Renderer2,
  EventEmitter,
  NgZone,
  Input
} from '@angular/core';
import { AngularFireDatabase, AngularFireObject } from '@angular/fire/database';
import { AuthService } from '../shared/auth.service';
import { map, filter, startWith, distinctUntilChanged } from 'rxjs/operators';
import { Observable, Subscription, timer } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { StripePlans } from 'src/app/stripe/plans/stripe-plans';
import { ControllsService } from '../shared/controlls.service';
// import { GoogleAnalyticsService } from '../shared/google-analytics.service';
import { AccountStoreService, Plan } from '../shared/account-store.service';
import { StripeStoreService } from './stripe-store.service';
import { MyroomsService } from '../myrooms/myrooms.service';
import { DeltaTango } from '../shared/delta-tango';

interface SubscriptionData {
  quantity: number;
  current_period_start: number;
  current_period_end: number;
  id: string;
  status: string;
  interval: string;
  discount: any;
  students: number;
  // TODO: possible to have multiple coupons?
  coupon?: any;
};

@Component({
  selector: 'app-stripe',
  templateUrl: './stripe.component.html',
  styleUrls: ['./stripe.component.scss'],
  providers: [ 
    StripePlans,
    StripeStoreService,
    DeltaTango,
    MyroomsService,
  ]
})
export class StripeComponent implements OnInit, OnDestroy {
  @Input() userId: string = null;
  @Input() nickName: string = null;
  doozPlanz: any = {};
  subscription: any;
  interval = 'month';
  currentInterval: any = null;
  package: any = 0;
  currentPackage: any = null;
  quantityChange: boolean = false; // indicates a local chenge in selecting a new value for the subscription
  paymentChange: boolean = false; // indicates a local chenge in selecting a new payment method
  progressBar: Boolean = false;
  showSubscription: Boolean = false;
  overlay: Boolean = false;

  stripeIdAvailable: Observable<boolean>; // ???
  stripeSubscriptionAvailable: Observable<boolean>;
  currentSubscription: Observable<any>;
  stripeSourcesAvailable: Observable<{}>;
  __stripeSourcesAvailable: any = false;
  stripeDefaultSource: Observable<{}>;
  __stripeDefaultSource: any;
  stripeSubscriptionData: Observable<{}>;

  stripeControllActions: Observable<any>;
  changeDefaultSource: Boolean = false;
  stripeEvents: EventEmitter<any> = new EventEmitter();
  timer: Observable<any>;
  subscriptionChangeDetect: Observable<any>;
  intervalDetect: Observable<any>;
  couponCode: string = null;
  cardOrSepa: string = 'card';

  subscriptionStatus$: Observable<any>;
  subscriptionContainer: Subscription = new Subscription();
  vm$: Observable<any>;

  stripeDebug: boolean = false;

  constructor(
    private cd: ChangeDetectorRef,
    public db: AngularFireDatabase,
    public a: AuthService,
    public snackBar: MatSnackBar,
    private sp: StripePlans,
    public cs: ControllsService,
    private renderer: Renderer2,
    private zone: NgZone,
    public accountStore: AccountStoreService,
    public stripeStore: StripeStoreService,
    private myrooms: MyroomsService,
  ) {
    // ...
    this.vm$ = this.stripeStore.vm$;
    
  }

  ngOnInit() {
    
    this.stripeStore.getStripeCustomer(this.accountStore.userId$);
    this.stripeStore.getStripeSubscription(this.accountStore.userId$);
    this.stripeStore.getStripeDefaultSource(this.accountStore.userId$);

    this.timer = timer(3000, 1000);

    this.progressBar = true;

    this.doozPlanz = this.sp.plans;

    this.cs.sync('ccComponent')
      .subscribe(val => {
        // console.log('ccComponent', val);
        this.changeDefaultSource = false;
        // this.cd.detectChanges();
      });

    this.stripeControllActions = this.cs.sync('stripe')
      .pipe(map((val: any) => {
        // console.log('stripeControllActions: ', val.payload);
        return val.payload;
      }));

  }
  // end onInit

  // 
  vmAction(action) {
    this.stripeStore.setVmAction(action);
  }

  // indicate if package val was changed and is different from  current one
  selectionPackage(event) {
    console.log('selectionPackage: ', event, this.package);
    this.stripeStore.quantityChange(event.value);
    /*
    // catching the case, when this.subscription could not have been retrieved properly or first subscription creation
    try {
      this.quantityChange = this.package + 1 === this.subscription.quantity ? false : true;
    }
    catch(e) {
      console.log('selectionPackage: ', e);
      this.quantityChange = false;
    }
    */
    
  }

  assignVoucherCode($event) {
    // 'assignVoucherCode: ', $event);
    this.couponCode = $event;
  }

  myPlans(interval) {
    // console.log(interval);
    return this.doozPlanz.filter(val => val.interval === interval);
  }

  ngOnDestroy() {
    // ...
    this.subscriptionContainer.unsubscribe();
  }

  async doSubscribe(quantity: number, interval: number) {

    const pkg = this.myPlans(interval)[quantity-1];
    // add coupon if there is one
    if (this.couponCode !== null) {
      pkg.coupon = this.couponCode;
    }
    //console.log('subscribe: ', pkg);
    pkg.timestamp = Date.now();
    const result = await this.db
      .list('/stripe_customers/' + this.userId + '/subscriptions/trigger')
      .push(pkg);

    // intermediate solution
    const roomToken = await this.myrooms
      .createMyRoom(this.userId, 'coach', Plan.premium, this.nickName); // we need the nickname
    console.log('&&& ', roomToken);
    
    return roomToken;
  }

  // scroll to very end of page
  scrollDown() {
    setTimeout(() => window.scrollTo(0, 2500), 500);
  }

// end of class
}
