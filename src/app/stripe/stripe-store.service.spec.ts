import { TestBed } from '@angular/core/testing';

import { StripeStoreService } from './stripe-store.service';

describe('StripeStoreService', () => {
  let service: StripeStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StripeStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
