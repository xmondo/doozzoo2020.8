import {
  Component,
  OnInit,
  AfterViewInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
  Output,
  Inject,
  Renderer2,
  AfterContentInit,
} from '@angular/core';
import { AngularFireDatabase, AngularFireObject } from '@angular/fire/database';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../shared/auth.service';
import { map, filter } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ControllsService } from '../../shared/controlls.service';
// import { DOCUMENT } from '@angular/platform-browser';
import { DOCUMENT } from '@angular/common';
import { environment } from 'src/environments/environment';

// prevent errors with syntax checks
declare var Stripe: any;

@Component({
  selector: 'app-creditcard',
  templateUrl: './creditcard.component.html',
  styleUrls: ['./creditcard.component.scss']
})
export class CreditcardComponent implements OnInit, AfterViewInit, AfterContentInit, OnDestroy {
  private nativeElement : Node;
  @ViewChild('cardInfo', { static: true }) cardInfo: ElementRef;

  @Output() paymentUpdateSuccess: Boolean = false;
  card: any;
  cardHandler = this.onChange.bind(this);
  iban: any;
  error: string;
  userId: any;
  submitButtonStatus: Boolean = false;
  dummy = true;
  _stripe: any;
  _elements: any;
  paymentType: string = 'sepa'; //'card' | 'sepa';

  // declare var Stripe;

  constructor(
    @Inject(DOCUMENT) private document: any,
    private renderer: Renderer2,
    private cd: ChangeDetectorRef,
    public db: AngularFireDatabase,
    public a: AuthService,
    public snackBar: MatSnackBar,
    public cs: ControllsService,
  ) { }

  ngOnInit() {
    this.a.usr$
      .pipe(filter(val => val !== null))
      .subscribe(val => {
        this.userId = val.uid;
      });
  }

  ngAfterViewInit() {}

  ngAfterContentInit() {
    const script = this.renderer.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://js.stripe.com/v3/';
    this.renderer.listen(script, 'load', () => {
      console.log('stripe succesfully loaded');
      // TODO: adapt to config
      this._stripe = Stripe(environment.stripePublishableKey); // publishable key
      // console.log('stripePublishableKey: ', environment.stripePublishableKey);
      // console.log('stripe obj: ', this._stripe);
           
      this._elements = this._stripe.elements();
      this.card = this._elements.create('card');
      this.card.mount(this.cardInfo.nativeElement);
      this.card.addEventListener('change', this.cardHandler);
        
    });
    this.renderer.appendChild(this.document.head, script);
  }

  ngOnDestroy() {
    this.card.removeEventListener('change', this.cardHandler);
    this.card.destroy();
  }

  onChange({ error }) {
    if (error) {
      this.error = error.message;
    } else {
      this.error = null;
    }
    this.cd.detectChanges();
  }

  updateForm() {
    this.cd.detectChanges();
  }

  async onSubmit(form: NgForm) {

    // TODO: set timeout, to prevent multipe submissions
    // disable button on UI
    this.submitButtonStatus = true;

    const { token, error } = await this._stripe.createToken(this.card);

    if (error) {
      console.log('Something is wrong:', error);
      // unlock UI
      this.submitButtonStatus = false;
      this.snackBar
        .open( 'Something went wrong, please try again!', 'Got it');
    } else {
      // console.log('Success!', token);
      // ...send the token to the your backend to process the charge
      // .database.ref('/stripe_customers/{userId}/tokens/{pushId}')
      // this.db.object('/stripe_customers/' + {userId} + /tokens/{pushId}')
      this.db.list('/stripe_customers/' + this.userId + '/tokens')
        .push(token)
        .then(result => {
          // ... done
          console.log('stripe token saved: ', result);
        });
      // unlock UI
      // but set timeout
      setTimeout(() => { 
        this.submitButtonStatus = false;
      }, 4000);

      // hide CC form
      this.cs.do('ccComponent', { paymentUpdateSuccess: true });

      // fire toast message
      this.snackBar
        .open( 'Your payment data has been updated!', 'Got it');

    }
  }

}

/**
 * testdata: 
 * https://stripe.com/docs/testing#cards
 * 4242424242424242 VISA
 * 4000056655665556	Visa (debit)
 * 5555555555554444	Mastercard
 * 5200828282828210	Mastercard (debit)
 * 
 * SEPA
 * DE89370400440532013000
 */
