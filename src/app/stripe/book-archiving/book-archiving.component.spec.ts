import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookArchivingComponent } from './book-archiving.component';

describe('BookArchivingComponent', () => {
  let component: BookArchivingComponent;
  let fixture: ComponentFixture<BookArchivingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookArchivingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookArchivingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
