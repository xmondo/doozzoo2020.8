import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from 'src/app/shared/auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { BookArchivingConfirmComponent } from '../book-archiving-confirm/book-archiving-confirm.component';
import { MatDialog } from '@angular/material/dialog';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-book-archiving',
  templateUrl: './book-archiving.component.html',
  styleUrls: ['./book-archiving.component.scss']
})
export class BookArchivingComponent implements OnInit {
  @Input() userId: string;
  pricePerMinute: number;
  archivingObsv: Observable<any>;
  revocationStatus: boolean = false;
  constructor(
    public ats: AuthService,
    private rtd: AngularFireDatabase,
    private fst: AngularFirestore,
    public dialog: MatDialog,
  ) { }

  async ngOnInit() {
    // get price per minute from Stripe
    // this.pricePerMinute = 0.06;
    await this.getPrice();
  }

  async getPrice(){
    const price = await this.fst
      .collection('stripe')
      .doc('config')
      .get()
      .toPromise();
    this.pricePerMinute = price.data().archivingSD;
  }

  setArchiving(status: boolean){
    console.log('setArchiving: ', status);
    this.rtd.database
      .ref(`users/${this.userId}/featureToggle`)
      .update({ archiving: status })
  }
  setArchivingHighRes(event){
    // in case archiving in general was not selected before
    console.log(event);
    if(event.checked === true) {
      this.rtd.database
        .ref(`users/${this.userId}/featureToggle`)
        .update({ archiving: event.checked })
    }
    this.rtd.database
      .ref(`users/${this.userId}/featureToggle`)
      .update({ archivingHighRes: event.checked })
  }

  openConfirm(status: boolean): void {
    const dialogRef = this.dialog.open(BookArchivingConfirmComponent, {
      width: '450px',
      data: { checked: status, pricePerMinute: this.pricePerMinute }
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        console.log('The dialog was closed', result);
        // result is 'true' or undefined
        if (result) {
          console.log('canceled...');
          this.setArchiving(!status)
        } else {
          console.log('canceled...');
          this.setArchiving(status);
        }
      });
  }

  submitRevocation($event){
    console.log(event);
    this.revocationStatus = !this.revocationStatus;
  }

}
