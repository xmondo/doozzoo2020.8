import {
  Component,
  OnInit,
  AfterViewInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
  Output,
  Inject,
  Renderer2,
  AfterContentInit,
} from '@angular/core';

import { AngularFireDatabase, AngularFireObject } from '@angular/fire/database';
import { NgForm, FormGroup, FormControl, Validators, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from '../../shared/auth.service';
import { filter } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ControllsService } from '../../shared/controlls.service';
// import { DOCUMENT } from '@angular/platform-browser';
import { DOCUMENT } from '@angular/common';
import { environment } from 'src/environments/environment';

// prevent errors with syntax checks
declare var Stripe: any;

@Component({
  selector: 'app-sepa',
  templateUrl: './sepa.component.html',
  styleUrls: ['./sepa.component.scss']
})
export class SepaComponent implements OnInit, AfterViewInit, OnDestroy {
  private nativeElement : Node;
  @ViewChild('ibanInfo', { static: true }) ibanInfo: ElementRef;
  @Output() paymentUpdateSuccess: Boolean = false;

  sepaForm = new FormGroup({
    accountholderName: new FormControl('', [ Validators.required ]),
    email: new FormControl('', [ Validators.required, Validators.email ]),
  });

  ibanHandler = this.onChange.bind(this);
  iban: any;
  error: any = null;
  errorMessage: string;
  userId: any;
  submitButtonStatus: Boolean = false;
  dummy = true;
  _stripe: any;
  _elements: any;

  constructor(
    @Inject(DOCUMENT) private document: any,
    private renderer: Renderer2,
    private cd: ChangeDetectorRef,
    public db: AngularFireDatabase,
    public a: AuthService,
    public snackBar: MatSnackBar,
    public cs: ControllsService,
  ) { }

  ngOnInit() {
    this.a.usr$
      .pipe(filter(val => val !== null))
      .subscribe(val => {
        console.log(val)
        this.userId = val.uid;
        this.sepaForm.patchValue({ email: val.email });
        // val.displayName ? this.sepaForm.patchValue({ accountholderName: val.displayName }) : '';
      });
  }

  ngAfterViewInit() {
    const script = this.renderer.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://js.stripe.com/v3/';
    this.renderer.listen(script, 'load', () => {
      console.log('stripe succesfully loaded');
      // TODO: adapt to config
      this._stripe = Stripe(environment.stripePublishableKey); // publishable key
      // console.log('stripePublishableKey: ', environment.stripePublishableKey);
      // console.log('stripe obj: ', this._stripe);
          
      this._elements = this._stripe.elements();
      
      const style = {
        base: {
          // Add your base input styles here. For example:
          fontSize: '16px',
          color: "#32325d",
        }
      };
      const options = {
        style: style,
        supportedCountries: ['SEPA'],
        // If you know the country of the customer, you can optionally pass it to
        // the Element as placeholderCountry. The example IBAN that is being used
        // as placeholder reflects the IBAN format of that country.
        placeholderCountry: 'DE',
      }
      // this._elements = this._stripe.elements();
      // Create an instance of the iban Element.
      this.iban = this._elements.create('iban', options);   
      // Add an instance of the iban Element into the `iban-element` <div>.
      this.iban.mount(this.ibanInfo.nativeElement);
      this.iban.addEventListener('change', this.ibanHandler);

      
    });
    this.renderer.appendChild(this.document.head, script);
    
  }

  ngOnDestroy() {
    this.iban.removeEventListener('change', this.iban);
    this.iban.destroy();
  }

  onChange({ error }) {
    if (error) {
      this.error = error;
      this.errorMessage = error.message;
    } else {
      this.error = null;
      this.errorMessage = null;
    }
    this.cd.detectChanges();
  }

  updateForm() {
    this.cd.detectChanges();
  }

  async onSubmit(form: NgForm) {

    console.log('submit form', form)

    // TODO: set timeout, to prevent multipe submissions
    // disable button on UI
    this.submitButtonStatus = true;

    const { token, error } = await this._stripe
      .createToken(
        this.iban, 
        { 
          currency: 'eur', 
          account_holder_name: this.sepaForm.get('accountholderName').value,
          account_holder_type: 'individual', 
        }
      );

    console.log('tke -> ', token, error)

    if (error) {
      console.log('Something is wrong:', error);
      this.error = error.message;
      // unlock UI
      this.submitButtonStatus = false;
      this.snackBar
        .open( 'Something went wrong, please try again!', 'Got it');
    } else {
      // console.log('Success!', token);
      // ...send the token to the your backend to process the charge
      // .database.ref('/stripe_customers/{userId}/tokens/{pushId}')
      // this.db.object('/stripe_customers/' + {userId} + /tokens/{pushId}')
      this.db.list('/stripe_customers/' + this.userId + '/tokens')
        .push(token)
        .then(result => {
          // ... done
          console.log('stripe token saved: ', result);
        });
      // unlock UI
      // but set timeout
      setTimeout(() => { 
        this.submitButtonStatus = false;
      }, 4000);

      // hide CC form
      this.cs.do('ccComponent', { paymentUpdateSuccess: true });

      // fire toast message
      this.snackBar
        .open( 'Your payment data has been updated!', 'Got it');

    }
  }

}

/**
 * testdata: 
 * https://stripe.com/docs/testing#cards
 * 4242424242424242 VISA
 * 4000056655665556	Visa (debit)
 * 5555555555554444	Mastercard
 * 5200828282828210	Mastercard (debit)
 * 
 * SEPA
 * DE89370400440532013000
 */
