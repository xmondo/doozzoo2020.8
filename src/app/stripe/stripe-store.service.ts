import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { ComponentStore, tapResponse } from '@ngrx/component-store';
import { database } from 'firebase';
import { Observable, EMPTY } from 'rxjs';
import { switchMap, catchError, tap, filter, map } from 'rxjs/operators';
import { StripePlans } from './plans/stripe-plans';

// make available for other modules as well
export interface StripeState {
  customerId: string,
  subscriptionsTotalCount: number,
  defaultSource: any,
  quantity: number,
  currentQuantity: number,
  quantityChange: boolean,
  current_period_start: string,
  current_period_end: string,
  status: string,
  interval: string,
  discount: number,
  students: number,
  vmAction: string,
  // TODO: possible to have multiple coupons?
  coupon?: string,
}

@Injectable()
export class StripeStoreService extends ComponentStore<StripeState> {

  constructor(
    private db: AngularFireDatabase,
    private sp: StripePlans,
  ) { 
    super({ 
      customerId: null,
      subscriptionsTotalCount: 0,
      defaultSource: null,
      quantity: 1,
      currentQuantity: 1,
      quantityChange: false,
      current_period_start: null,
      current_period_end: null,
      status: null,
      interval: 'month',
      discount: null,
      students: 0,
      vmAction: null,
      // TODO: possible to have multiple coupons?
      coupon: null,
    });
  }

  // *********** Updaters *********** //
  readonly setDefaults = this.updater((state: StripeState, value: any) => ({
    ...state,
    ...value, 
  }));
  readonly setQuantity = this.updater((state: StripeState, value: StripeState['quantity']) => ({
    ...state,
    quantity: value ?? 1, 
  }));
  readonly setCurrentQuantity = this.updater((state: StripeState, value: StripeState['currentQuantity']) => ({
    ...state,
    currentQuantity: value ?? 1, 
  }));
  readonly setDefaultSource = this.updater((state: StripeState, value: StripeState['defaultSource']) => ({
    ...state,
    defaultSource: value ?? null, 
  }));
  readonly setVmAction = this.updater((state: StripeState, value: StripeState['vmAction']) => ({
    ...state,
    vmAction: value ?? null, 
  }));
  readonly setQuantityChange = this.updater((state: StripeState, value: StripeState['quantityChange']) => ({
    ...state,
    quantityChange: value ?? false, 
  }));
  
  // *********** selects *********** //
  public readonly isCustomer$: Observable<boolean> = this.select(state => state.customerId !== null);
  public readonly hasSource$: Observable<boolean> = this.select(state => state.defaultSource !== null);
  public readonly hasSubscription$: Observable<boolean> = this.select(state => state.subscriptionsTotalCount > 0);
  public readonly showOverlay$: Observable<boolean> = this.select(this.state$, this.hasSubscription$, (state, hasSubscription) => {
    // should show an overlay if either:
    // flag didSubscribe is set and the qauntity changed (= change in subscription)
    // or we have a initial subscription, then the regarding flag returns false
    return state.vmAction === 'didSubscribe' && (state.quantityChange || !hasSubscription);
  });
  private readonly getCurrentQuantity$: Observable<number> = this.select(state => state.currentQuantity);

  // *********** view model *********** //
  public readonly vm$ = this.select(
    this.state$,
    this.isCustomer$,
    this.hasSource$,
    this.hasSubscription$,
    (state, isCustomer, hasSource, hasSubscription) => ({ state, isCustomer, hasSource, hasSubscription }),
    // {debounce: true}, // 👈 setting this selector to debounce
  );

  // *********** Effects *********** //
  readonly getStripeCustomer = this.effect((userId$: Observable<string>) => {
    return userId$
      .pipe(
        filter(userId => userId !== null),
        switchMap((userId) => this.db
          .object(`stripe_customers/${userId}/customerData`)
          .valueChanges()
          .pipe(
            // tap(val => console.log('1...', val)),
            filter(val => val !== null && val !== undefined),
            map((val: any) => {
              // map data
              const data = {                
                customerId: val.id,
                subscriptionsTotalCount: val.subscriptions.total_count,
                // defaultSource: val.default_source ?? null,
                // status: sub.status,
                //quantity: quantity,
                // interval: interval,
                // current_period_start: sub.current_period_start,
                // current_period_end: sub.current_period_end,
                // discount: _discount[0]?.discount ?? 0,
                // students: _studentsPkg[0]?.students ?? 0,
                // TODO: possible to have multiple coupons?
                // coupon: sub.discount?.coupon ?? null,
              };
              console.log('data: ', data)
              return data;
            }),
            // startWith(false),
            tap({
              next: (val:any) => {
                //this.setSubscribed(val);
                this.setDefaults(val);
              },
              error: (e) => console.error(e),
            }),
            // 👇 Handle potential error within inner pipe.
            catchError(() => EMPTY),
          )
        )
      )
  });

  readonly getStripeSubscription = this.effect((userId$: Observable<string>) => {
    return userId$
      .pipe(
        filter(userId => userId !== null),
        switchMap((userId) => this.db
          .object(`stripe_customers/${userId}/customerData/subscriptions/data/0`)
          .valueChanges()
          .pipe(
            // tap(val => console.log('getStripeSubscription: ', val)),
            filter(val => val !== null && val !== undefined),
            map((val: any) => {
              const interval = val.plan.interval;
              const quantity = val.quantity;
              
              // lookup savings
              // const _discount = this.doozPlanz.filter(item => item.interval === val.plan.interval && item.quantity === val.quantity);
              const _discount = this.sp.plans.filter(item => item.interval === interval && item.quantity === quantity);
              // console.log('d', _discount[0].discount);
              // const _studentsPkg = this.doozPlanz.filter(item => item.quantity === val.quantity);
              const _studentsPkg = this.sp.plans.filter(item => item.quantity === quantity);
              console.log('_studentsPkg: ', _studentsPkg);
    
              // map data
              const data = {
                quantity: quantity,
                // set both to the initial quantity read from backend
                currentQuantity: quantity,
                current_period_start: val.current_period_start,
                current_period_end: val.current_period_end,
                customerId: val.customer,
                // subscriptionsTotalCount: subscriptionsTotalCount,
                // defaultSource: val.default_source,
                status: val.status,
                interval: interval,
                discount: _discount[0]?.discount ?? 0,
                students: _studentsPkg[0]?.students ?? 0,
                // TODO: possible to have multiple coupons?
                coupon: val.discount?.coupon ?? null,
              };
              console.log('data: ', data)
              return data;
            }),
            // startWith(false),
            tap({
              next: (val:any) => {
                //this.setSubscribed(val);
                this.setDefaults(val);
              },
              error: (e) => console.error(e),
            }),
            // 👇 Handle potential error within inner pipe.
            catchError(() => EMPTY),
          )
        )
      )
  });

  readonly getStripeDefaultSource = this.effect((userId$: Observable<string>) => {
    return userId$
      .pipe(
        filter(userId => userId !== null),
        switchMap((userId) => this.db
          .object(`stripe_customers/${userId}/customerData/sources/data/0`)
          .valueChanges()
          .pipe(
            // tap(val => console.log('1...', val)),
            filter(val => val !== null && val !== undefined),
            map((val: any) => {
              // sepa
              if(val.sepa_debit) {
                return {
                  sepa_debit: val.sepa_debit,
                  owner: val.owner
                }
              } else if (val.object === 'card') {
                return val;
              } else {
                return val;
              }
            }),
            tap({
              next: (val:any) => {
                this.setDefaultSource(val);
              },
              error: (e) => console.error(e),
            }),
            // 👇 Handle potential error within inner pipe.
            catchError(() => EMPTY),
          )
        )
      )
  });

  readonly quantityChange = this.effect((selectedQuantity$: Observable<number>) => {
    return selectedQuantity$
      .pipe(
        switchMap((selectedQuantity) => this.getCurrentQuantity$
          .pipe(
            map(currentQuantity => currentQuantity !== selectedQuantity),
            tap({
              next: (val:boolean) => this.setQuantityChange(val),
              error: (e) => console.error(e),
            }),
            // 👇 Handle potential error within inner pipe.
            catchError(() => EMPTY),
          )
        )
      )
  });

// end of class
}
