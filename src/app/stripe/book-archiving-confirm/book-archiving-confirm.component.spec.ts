import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookArchivingConfirmComponent } from './book-archiving-confirm.component';

describe('BookArchivingConfirmComponent', () => {
  let component: BookArchivingConfirmComponent;
  let fixture: ComponentFixture<BookArchivingConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookArchivingConfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookArchivingConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
