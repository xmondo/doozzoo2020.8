import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface ArchivingDialogData {
  checked: boolean,
  pricePerMinute: number,
}

@Component({
  selector: 'app-book-archiving-confirm',
  templateUrl: './book-archiving-confirm.component.html',
  styleUrls: ['./book-archiving-confirm.component.scss']
})
export class BookArchivingConfirmComponent {
  param = { value: this.data.pricePerMinute };
  constructor(
    public dialogRef: MatDialogRef<BookArchivingConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ArchivingDialogData
  ) {
    //...
  }

  cancel(): void {
    this.dialogRef.close(false);
  }

  confirm(): void {
    // console.log('close');
    this.dialogRef.close(true);
  }

}
