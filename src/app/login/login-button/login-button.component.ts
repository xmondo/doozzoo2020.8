import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../../shared/auth.service';
import { ControllsService } from '../../shared/controlls.service';
import { filter, single, first, map, distinctUntilChanged } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
// import { Clipboard } from '@angular/cdk/clipboard';

@Component({
  selector: 'app-login-button',
  templateUrl: './login-button.component.html',
  styleUrls: ['./login-button.component.scss']
})
export class LoginButtonComponent implements OnInit {
  @Input() layout: string; // single | both | footer
  color = 'white';
  preEduToggle: Observable<boolean>;
  currentUrl: Array<any>; // string;

  loginDebug: boolean = false;

  constructor(
    public cs: ControllsService,
    public a: AuthService,
    private router: Router,
    public aRoute: ActivatedRoute,
    public translate: TranslateService,
    // public clipboard: Clipboard,
  ) {
    // ...

    this.preEduToggle = this.aRoute.url
      .pipe(
        distinctUntilChanged(),
        // looking for path in first sub root segment
        map(val => {
          if(val === undefined) {
            return false;
          } else if(val[0] === undefined) {
            return false;
          } else {
            return val[0].path === 'edu' ? true : false;
          }
        })
      );
    // injects current route into the dialog component
    this.currentUrl = this.aRoute.snapshot.url;
    // this.currentUrl = this.router.url
  }

  ngOnInit() {
    // get language
    // this.translate.currentLang;
  }

  toggleLanguage() {
    const langArr = this.translate.getLangs();
    console.log(langArr);
    let lang = this.translate.currentLang;
    // this.translate.setDefaultLang('en');
    let current: number = langArr.findIndex(item => item === lang);
    if(current < langArr.length - 1) {
      current++
      lang = langArr[current];
    } else {
      lang = langArr[0];
    } 
    console.log('lang: ', lang, current);
    this.translate.use(lang);  
  }

  copySuccess(event) {
    console.log(event)
  }

// end of class
}
