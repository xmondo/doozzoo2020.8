import { Component, OnInit, Output, EventEmitter, ChangeDetectorRef, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../shared/auth.service';
import { FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { filter, single, first } from 'rxjs/operators';
import { ControllsService } from 'src/app/shared/controlls.service';

/**
 * process incomming fb links for auth processes, e.g. double opt in
 *
 * Example link "reset password":
 * https://<tld>/pathSegment?mode=resetPassword&oobCode=<fb_generated_token>&lang=en
 * Example link "reset password":
 * https://<tld>/pathSegment?mode=verifyEmail&oobCode=<fb_generated_token>&apiKey=<fb_generated_api-key>&lang=en
 *
 URL part before ? can be set up in fb backend, so router has to handle:
 ?mode=resetPassword&oobCode=<fb_generated_token>&lang=en
 */

@Component({
  selector: 'app-email-handler',
  templateUrl: './email-handler.component.html',
  styleUrls: ['./email-handler.component.scss']
})
export class EmailHandlerComponent implements OnInit {

  uiAction = 'default';
  localProgressBar = false;

  email = null;
  password = null;
  errorCode = null;
  errorMessage = null;
  successMessage = false;
  url: any;
  mode: String = null;
  actionCode: String;
  apiKey: String;
  language: String;
  inviteObj: any = null;
  fbUser: any = null;
  subContainer: Subscription = new Subscription;

  passwordCtrl = new FormControl('', [Validators.required]);

  constructor(
    public router: Router,
    private r: ActivatedRoute,
    public a: AuthService,
    public cs: ControllsService,
    public ref: ChangeDetectorRef,
    private zone: NgZone,
  ) {
    // queryParams.subscribe
    /*
    this.url = this.r.queryParams
      .subscribe(params => {
        // console.log(params);
        this.mode = params['mode'];
        this.actionCode = params['oobCode'];
        this.apiKey = params['apiKey'];
        this.language = params['lang'];
        this.email = params['u'];
        this.password = params['p'];
        this.zone.run(() => this.dispatch());
      });
    this.subContainer.add(this.url); 
    */

    this.mode = this.r.snapshot.queryParams.mode;
    this.actionCode = this.r.snapshot.queryParams.oobCode;
    this.apiKey = this.r.snapshot.queryParams.apiKey;
    this.language = this.r.snapshot.queryParams.lang;
    this.email = this.r.snapshot.queryParams.u;
    this.password = this.r.snapshot.queryParams.p;

    const userSub = this.a.user$
      .pipe(filter(val => val !== null))
      .subscribe(user => {
        this.fbUser = user;
      });
    this.subContainer.add(userSub); 
  }

  ngOnInit() {
    // ...
    this.dispatch();
  }

  dispatch() {
    // console.log('dispatch')
    switch (this.mode) {
      case 'resetPassword':
        // Display reset password handler and UI.
        this.handleResetPassword();
        break;
      case 'recoverEmail':
        // Display email recovery handler and UI.
        // handleRecoverEmail(fbAuth, actionCode, lang);
        break;
      case 'verifyEmail':
        // Display email verification handler and UI.
        // handleVerifyEmail(fbAuth, actionCode, lang);
        this.handleVerifyEmail();
        break;
      case 'login':
        // for testing purposes
        this.doLogin();
        break;
      case 'logindelete':
        // for testing purposes
        this.doLoginDelete();
        break;
      case 'logout':
        // for testing purposes
        this.doLogout();
        break;
      case undefined:
        // console.log('mode is null');
        /**
         * this emits a boolean status, if the profile component should be initiated or not
         * if no email handler is initiated (mode === undefined),
         * profile status will be true = unblocked for other actions
         */
        this.cs.loginEmitter.next(true);
        break;
    }
  }

  // const resetPasswordErrorCodeArr = ['auth/invalid-action-code'];
  // function handleResetPassword(fbAuth, actionCode, continueUrl, lang){
  handleResetPassword() {
    const self = this;
    self.uiAction = 'resetPassword';
    self.localProgressBar = true;
    // Localize the UI to the selected language as determined by the lang
    // parameter.
    // let accountEmail;
      // Verify the password reset code is valid.
      this.a.auth$
        .verifyPasswordResetCode(this.actionCode)
        .then(email => {
          self.email = email;
          // console.log('verifyPasswordResetCode: ', self.email);
          self.localProgressBar = false;
          // TODO: Show the reset screen with the user's email and ask the user for
          // the new password.
          // reset URL to avoid reloads with wrong content snippets
          self.zone.run(() => self.router.navigate(['/login']));
        }).catch(function(error) {
          // console.log('verifyPasswordResetCode->error: ', error)
          self.localProgressBar = false;
          self.errorCode = error.code;
          self.errorMessage = error.message;
          // reset URL to avoid reloads with wrong content snippets
          self.zone.run(() => self.router.navigate(['/login']));
        });
  }

  // Save the new password.
  confirmPasswordReset = () => {
    const self = this;
    self.localProgressBar = true;
    self.errorCode = null;

    this.a.auth$.confirmPasswordReset(this.actionCode, self.password)
      .then(() => {
        console.log('confirmPasswordResetCode');

        self.localProgressBar = false;
        self.successMessage = true;

        // Password reset has been confirmed and new password updated.

        // TODO: Display a link back to the app, or sign-in the user directly
        // if the page belongs to the same domain as the app:
        // fbAuth.signInWithEmailAndPassword(self.email, self.password);

        // TODO: If a continue URL is available, display a button which on
        // click redirects the user back to the app via continueUrl with
        // additional state determined from that URL's parameters.

      }).catch(function(error) {
        console.log('confirmPasswordResetCode->error: ', error);
        // Error occurred during confirmation. The code might have expired or the
        // password is too weak.
        self.localProgressBar = false;
      });

  }


  // const verifyEmailErrorCodeArr = ['auth/invalid-action-code'];
  // function handleVerifyEmail(fbAuth, actionCode, continueUrl, lang){
  handleVerifyEmail() {
    const self = this;

    self.uiAction = 'verifyEmail';
    self.localProgressBar = true;
    self.errorCode = null;
    self.successMessage = null;
    // Localize the UI to the selected language as determined by the lang
    // parameter.

    self.a.auth$.applyActionCode(this.actionCode)
      .then(() => {

        self.successMessage = true;
        self.localProgressBar = false;
        // TODO: does not work yet -> reset url 
        // self.zone.run(() => self.router.navigate(['/login?mode=emailVerifySuccess']));

        /**
         * firebase stores the email verification status in the session
         * to consistantly retrieve it, we need to reload the fb user 
         * and then can safely read it from the user obj and apply to the local proxy obj "userData"
         */

        // console.log('applyActionCode->app-email-handler: ', this.a.userData);

        // trigger auth renew and set email verification flag
        // this.a.userData.emailVerified = true;
        // this.a.userDataEmitter.next(this.a.userData);

        // TODO: Display a confirmation message to the user.
        // You could also provide the user with a link back to the app.

        // TODO: If a continue URL is available, display a button which on
        // click redirects the user back to the app via continueUrl with
        // additional state determined from that URL's parameters.
      })
      .catch(function(error) {
        console.log('handleVerifyEmail->error: ', error)
        self.successMessage = false;
        self.localProgressBar = false;
        self.errorCode = error.code;
        self.errorMessage = error.message;
        // reset url
        self.zone.run(() => self.router.navigate(['/login']));
        self.zone.run(() => self.cs.loginEmitter.next(false));
    });

    // try to fetch inviteObj, 
    // in case its existing, show redirect button to invite page
    this.inviteObj = JSON.parse(localStorage.getItem('doozzooInvite'));
  }

  refreshFbUser() {
    const self = this;
    const updateUser = this.a.user$
      .pipe(
        first(val => val !== null),
      );

    const updateUserSub = updateUser
      .subscribe(val => {
        console.log('###updateCurrentUser->user: ', val);

        val.reload()
          .then(result => {
            // console.log('++++++++++', val.emailVerified, val.uid)
            this.zone.run(() => self.cs.loginEmitter.next(true));
          });
      })
    this.subContainer.add(updateUserSub); 
  }

  doLogin() {
    this.a.auth$.signInWithEmailAndPassword(this.email, this.password)
      .then(response => {
        console.log('auth->doLogin: ', response.user.uid);
        // this.router.navigateByUrl('/login');
        this.zone.run(() => this.router.navigate(['/login']));
      }).catch(function(error) {
        console.error('Authentication failed: ', error);
      });
  }

  doLoginDelete() {
    this.a.auth$.signInWithEmailAndPassword(this.email, this.password)
      .then(user => {
        // console.log('auth->doLoginDelete: ', user.user.uid);
        setTimeout(() => this.doDelete(user), 100);
      }).catch(function(error) {
        console.error('Authentication failed: ', error);
      });
  }

  /**
   * 
   * @param fbUser delete logged in user
   */
  doDelete(fbUser) {
    // delete user
    if (this.fbUser !== null) {
      this.fbUser.delete()
        .then(result => {
          console.log('emailHandler->User deleted successfully!', result);
        }).catch(function(error) {
          console.error('emailHandler->error: ', error);
        });
    } else {
      console.log('no valid user...');
    }
  }

  verifyNextAction() {
    this.a.usr$
      .subscribe(val => {
        console.log('LoginComponent->verifyNextAction: ', val);
      });
  }

  doLogout() {
    this.a.auth$.signOut();
  }

  ngOnDestroy() {
    this.subContainer.unsubscribe();
  }

// ### end of class
}
