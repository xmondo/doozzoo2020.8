// TODO: remove ngModel from Forms module!!

import { Component, OnInit, OnDestroy, Inject, ChangeDetectorRef } from '@angular/core';
import { NgForm, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthService } from '../../shared/auth.service';
import { UserData } from '../../shared/auth.service';
import { ControllsService } from '../../shared/controlls.service';
import { auth } from 'firebase/app';
import { filter, take } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

export interface error {
  code: string,
  message: string,
}

@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.scss'],
  providers: [
    // FormControl,
    // Validators
  ]
})
export class LoginDialogComponent implements OnInit, OnDestroy {
  email: any = null;
  emailRepeated: any;
  password: any;
  passwordRepeated: any;
  localProgress: Boolean = false;
  errorCode: String = null;
  errorMessage: String = null;
  successStatus: Boolean = false;
  selectedIndex = 0; // tab index
  user: UserData = null;
  userSub: Subscription;
  mode: string = null;

  forgottenFormGroup:FormGroup = new FormGroup({
    forgottenEmailCtrl:new FormControl('', [Validators.required, Validators.email, Validators.minLength(6)]),
    forgottenEmailRepeatedCtrl: new FormControl('', [Validators.required])
  });

  emailCtrl = new FormControl('sdfsdfsf', [Validators.required, Validators.email, Validators.minLength(6)]);
  emailRepeatedCtrl = new FormControl('', [ ]);
  passwordCtrl = new FormControl('', [Validators.required, Validators.minLength(6)]); //, Validators.pattern('[a-zA-Z0-9 ]*')]);
  passwordRepeatedCtrl = new FormControl('', [ ]);
  both: Boolean = true;
  isPreEdu: boolean = false;
  isWaitingRoom: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<LoginDialogComponent>,
    public a: AuthService,
    public cs: ControllsService,
    public cRef: ChangeDetectorRef,
    private router: Router,
    private translate: TranslateService,
  ) {
    // ...
  }

  getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
        this.email.hasError('email') ? 'Not a valid email' : '';
  }

  ngOnInit() {
    // ...
    const self = this;

    // console.log('input data: ', this.data)

    this.selectedIndex = this.data.tabIndex;
    // console.log('idx', this.selectedIndex)
    this.isPreEdu =  this.data.isPreEdu;
    // console.log('isPreEdu', this.isPreEdu)

    // catches the case when no url is provided
    const currentUrl = this.data.currentUrl ?? ['dummy'];
    this.isWaitingRoom = currentUrl.findIndex(val => val.path === 'waitingroom') !== -1 ? true : false;
    // console.log('isWaitingRoom', this.isWaitingRoom, currentUrl)
    
    this.userSub = this.a.usr$
      .pipe(take(1)) // only the first subscription should fire
      .subscribe(val => {
        // if there is already a valid user, prepopulate the value
        val.uid !== null ? self.email = val.email : null;
        // console.log('*****', val)
        this.forgottenFormGroup.patchValue({forgottenEmailCtrl: self.email});

        // user data dependent redirects
        // case that user is registered as FB user but has not verified his email
        // but not for "testdrive" users
        val.userActions !== 'testdrive' && 
        val.uid !== null && 
        val.emailVerified === false ? this.redirectUnverified() : '';

      });
  
      // console.log(this.forgottenFormGroup);
  
  }

  ngOnDestroy() {
    this.userSub ? this.userSub.unsubscribe() : '';
  }

  loginProvider(provider) {
    const self = this;
    self.errorCode = null;
    self.localProgress = true;
    let _provider;
    switch (provider) {
      case 'google':
        _provider = new auth.GoogleAuthProvider();
        break;
      case 'facebook':
        _provider = new auth.FacebookAuthProvider();
        break;
      case 'twitter':
        _provider = new auth.TwitterAuthProvider();
        break;
    }
    this.a.auth$.signInWithPopup(_provider)
      .then(result => {
        console.log('signInWithPopup: ', result);
        // check if socialProvider comes with email,
        // if not ask for providing one
        if (!result.user.email) {
          this.selectedIndex = 3;
        // otherwise init emailVerification
        } else if (!result.user.emailVerified) {
          this.doEmailVerificationCheck(result.user);
        } else {
          self.close();
          self.localProgress = false;
        }
      })
      .catch(function(error) {
        self.localProgress = false;
        // Handle Errors here.
        self.dispatchError(error);
      });
  }

  /**
   * general login dialog
   * if login succesful, we have to catch two specific cases:
   * 1.) user has a valid firebase user after registration (username=email/password), 
   * but not yet verified hier email. In this case the login dialog should give a verbal feedback 
   * and redirect to the verification tab.
   * 2.) the registration as coach has not been finalized. 
   * In this case the user should get a verbal feedback 
   * and be redirected to the registration form.
   */
  login() {
    const self = this;
    self.errorCode = null;
    self.localProgress = true;
    self.a.auth$.signInWithEmailAndPassword(self.email, self.password)
      .then(result => {
        // console.log('signInWithEmailAndPassword: ', result.user);
        
        // email not yet verified -> throw error and redirect
        if(!result.user.emailVerified){
          this.redirectUnverified();

        // existing user with validated email -> close dialog
        } else {
          // if user came from testdrive, but was a registered user
          result.user.userActions === 'testdrive' ? this.a.setUserData('testdrive', null) : '';
          self.close();
        }
        // 
        self.localProgress = false;
      })
      .catch(function(error) {
        self.localProgress = false;
        // Handle Errors here.
        self.dispatchError(error);
      });
  }

  register() {
    // console.log('start register');
    const self = this;
    self.errorCode = null;
    self.successStatus = false;
    self.localProgress = true;

    // if user came from testdrive, and now did first registration step
    const userData = this.a.getUserData();
    userData.userActions === 'testdrive' ? this.a.setUserData('testdrive', null) : '';

    self.a.auth$.createUserWithEmailAndPassword(self.email, self.password)
      .then(result => {
        // console.log('register->result: ', result);
        self.successStatus = true;
        self.localProgress = false;
      })
      .catch(function(error) {
        // Handle Errors here.
        self.dispatchError(error);
        self.localProgress = false;
      });
      
  }

  passwordForgotten() {
    const self = this;
    self.localProgress = true;
    self.successStatus = false;
    const actionCodeSettings = {
      url: 'https://localhost:4200/login?email=' + self.email
    };
    self.errorCode = null;
    self.localProgress = true;

    const email = this.forgottenFormGroup.get('forgottenEmailCtrl').value;

    // TODO: clarify if actionCode Settings are needed
    // self.a.auth$.sendPasswordResetEmail(self.email, actionCodeSettings)
    // self.a.auth$.sendPasswordResetEmail(self.email)
    self.a.auth$.sendPasswordResetEmail(email)
      .then(() => {
        // Password reset email sent.
        self.successStatus = true;
        self.errorCode = null;
        self.localProgress = false;
        // button grey
        this.forgottenFormGroup.reset();
        this.forgottenFormGroup.markAsPristine();
      })
      .catch(error => {
        // Error occurred. Inspect error.code.
        self.dispatchError(error);
        self.localProgress = false;
      });
  }

  logout() {
    this.a.auth$.signOut();
  }

  close(): void {
    this.dialogRef.close();
  }

  getTranslate(key) {
    const val = this.translate.get(key)
      .subscribe(( val: string ) => {
        this.errorMessage = val;
      });
    
  }

  dispatchError(error: error): void {
    // ...
    console.log('dispatchError->error: ', error);
    this.errorCode = error.code;
    this.errorMessage = error.message;
    if (error.code === 'auth/wrong-password') {
      // this.errorMessage = 'Wrong password, please try again!';
      this.getTranslate('login.messages.wrong-password');
    } else if (error.code === 'auth/email-already-in-use') {
      // this.errorMessage = 'E-mail already in use, try another one!';
      this.getTranslate('login.messages.email-already-in-use');
    } else if (error.code === 'auth/user-disabled') {
      // this.errorMessage = 'Sorry, but your account has been disabled!';
      this.getTranslate('login.messages.user-disabled');
    } else if (error.code === 'auth/user-exists-email-not-verified') {
      // this.errorMessage = 'To complete your registration, you need to verify your email first!';
      this.getTranslate('login.messages.user-exists-email-not-verified');
      // redirect to verify
      // dependent on user journey: do not redirect for "testdrive" users
      const userData = this.a.getUserData();
      userData.userActions === 'testdrive' ? '' : this.selectedIndex = 3;

    } else if (error.code === 'auth/user-not-found') {
      // this.errorMessage = 'User not found ! There is no user record corresponding to this identifier.';
      this.getTranslate('login.messages.user-not-found');

    } else if (error.code === 'auth/too-many-requests') {
      // Original -> 'Too many requests. We have blocked all requests from this device due to unusual activities';
      // this.errorMessage = 'Too many requests. You have been logged out automatically. Please close this dialog, wait a minute, then try again!';
      this.getTranslate('login.messages.too-many-requests');
      this.logout();
      // redirect to login
      this.selectedIndex = 0;
    } else if (error.code === 'auth/user-token-expired') {
      // this.errorMessage = 'Your login is not longer valid. Please log in again';
      this.getTranslate('login.messages.user-token-expired');
      this.logout();
      this.selectedIndex = 0;
    } else if (error.code === 'auth/invalid-action-code') {
      // this.errorMessage = 'The action code is invalid. This can happen if the code is malformed, expired, or has already been used.';
      this.getTranslate('login.messages.invalid-action-code');
      this.router.navigate(['/login']);
    } else {
      // this.errorMessage = 'Ooops, something went wrong! Please try again!';
      this.getTranslate('login.messages.generic-error');
    }
    // TODO: strange effekt happening in testing
    // auth/too-many-requests
  }

  // this function should only be visible for users with email.verified === false
  // flow in case we have registered users without email, 
  // this can be the case when reistered with social providers
  verify() {
    const self = this;
    self.localProgress = true;
    this.errorCode = null;
    const user = this.a.auth$.currentUser;
    if (user) {
      // is user has no email yet
      if(user.email === null || user.email === undefined){
        user.updateEmail(this.email)
        .then(result => {
          console.log(result);
          this.doEmailVerificationCheck(user);
        })
        .catch(error => {
          // An error happened.
          self.dispatchError(error);
          self.localProgress = false;
          console.log('emailUpdate to ' + user.uid + ' created an error: ', error);
          // reset url
        });
      } else if(user.emailVerified === false) {
        this.doEmailVerificationCheck(user); 
      }
    // user is null
    // user must login first or even have to start registering
    } else {
      self.localProgress = false;
      // throws generic error
      this.dispatchError({ code: 'oops', message: ''});
      console.log('user must login first...');
    }

  }

  /**
   * prerequisite for user email verification is a registered user!!
   * user must be logged in
   * user must be of emailVerified === false
   * @param user -> user.email, user.uid, user.emailVerified
   */
  doEmailVerificationCheck(user) {
    const self = this;
    user.sendEmailVerification()
      .then(() => {
      // Email sent
      self.successStatus = true;
      this.errorCode = null;
      self.localProgress = false;
      console.log('emailVerification to ' + user.uid + ' send successfully!');
      // self.cRef.detectChanges();
      // reset form
      // select "verify" tab
      // self.selectedIndex = 1;
      self.selectedIndex = 3;
      
      this.emailRepeatedCtrl.reset()
      this.emailCtrl.reset()
      
    }).catch(error => {
      // An error happened.
      // select "verify" tab
      self.selectedIndex = 3;
      self.dispatchError(error);
      self.localProgress = false;
      console.log('emailVerification to ' + user.uid + ' created an error: ', error);
      // self.cRef.detectChanges();
    });

  }

  // email not yet verified -> throw error and redirect
  redirectUnverified(){
    // do redirect, but not for waitingroom
    if (!this.isWaitingRoom) {
      const error = {
        code: 'auth/user-exists-email-not-verified',
        message: ''
      }
      this.dispatchError(error);
      this.selectedIndex = 3;  
    }
  }

  // reset all form data when changing tabs
  tabChanged(event){
    console.log('tabChanged: ', event);
    this.email = null;
    this.emailRepeated = null;
    this.password = null;
    this.passwordRepeated = null;
    // this.errorCode = null;
    // this.errorMessage = null;

    // for password forgotten, reset error code
    if(event.index === 2) {
      this.errorCode = null;
      this.errorMessage = null;
      this.forgottenFormGroup.reset();
    }

  }

// ### end of class ###
}
