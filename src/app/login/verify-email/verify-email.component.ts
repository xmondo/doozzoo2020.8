import { Component, Input, NgZone, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AccountStoreService } from 'src/app/shared/account-store.service';
import { AuthService } from 'src/app/shared/auth.service';

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.scss']
})
export class VerifyEmailComponent implements OnInit {
  @Input() actionCode: string;
  @Input() emailVerified: boolean;
  localProgressBar = true;
  errorCode = null;
  successMessage = null;
  errorMessage = null;
  inviteObj: any;
  vm$: Observable<any>;

  constructor(
    public ats: AuthService,
    private zone: NgZone,
    private router: Router,
    public accountStore: AccountStoreService,
  ) { 
    this.vm$ = this.accountStore.vm$;
  }

  ngOnInit(): void {
    !this.emailVerified && (this.actionCode !== null && this.actionCode !== undefined) ? this.handleVerifyEmail() : '';
  }

  // const verifyEmailErrorCodeArr = ['auth/invalid-action-code'];
  // function handleVerifyEmail(fbAuth, actionCode, continueUrl, lang){
  handleVerifyEmail() {
    // this.uiAction = 'verifyEmail';
    this.localProgressBar = true;
    this.errorCode = null;
    this.successMessage = null;
    // Localize the UI to the selected language as determined by the lang
    // parameter.
    
    this.ats.auth$.applyActionCode(this.actionCode)
      .then(() => {
        this.successMessage = true;
        this.localProgressBar = false;
        this.router.navigateByUrl('login');
        // setTimeout(() => this.ats.user$.subscribe(val => this.ats.auth$.updateCurrentUser(val)), 2000);
        setTimeout(() => this.accountStore.setEmailVerified(true), 1000);
      })
      .catch((error) => {
        console.log('handleVerifyEmail->error: ', error)
        this.successMessage = false;
        this.localProgressBar = false;
        this.errorCode = error.code;
        this.errorMessage = error.message;
        // reset url
        this.router.navigate(['/login']);
      })

    // try to fetch inviteObj, 
    // in case its existing, show redirect button to invite page
    // this.inviteObj = JSON.parse(localStorage.getItem('doozzooInvite'));
  }

}
