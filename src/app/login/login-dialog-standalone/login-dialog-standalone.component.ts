import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ControllsService } from 'src/app/shared/controlls.service';
import { AuthService } from 'src/app/shared/auth.service';
import { Router, ParamMap, ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login-dialog-standalone',
  templateUrl: './login-dialog-standalone.component.html',
  styleUrls: ['./login-dialog-standalone.component.scss']
})
export class LoginDialogStandaloneComponent implements OnInit, AfterViewInit, OnDestroy {
  redirectUrl: string;
  target: string; // defaults to _blank
  language: string;
  welcomeMessage: string;
  buttonLabel: string;
  loginStatusSub: Subscription;

  constructor(
    public dialog: MatDialog,
    public cs: ControllsService,
    public ats: AuthService,
    public route: ActivatedRoute,
  ) { }

  ngOnInit() {
    // ...
    // this.route.snapshot.queryParams.get()
    const url = this.route.snapshot.queryParams['redirect'] || environment.tld;
    this.redirectUrl = `https://${url}`;
    this.target = this.route.snapshot.queryParams['target'] || '_blank';
    this.language = this.route.snapshot.queryParams['language'] || 'en';
    this.welcomeMessage = this.route.snapshot.queryParams['welcomemessage'] || 'Welcome, you successfully logged in to doozzoo';
    this.buttonLabel = this.route.snapshot.queryParams['buttonlabel'] || 'Start with doozzoo';
    console.log(this.redirectUrl, this.target);
  }

  ngAfterViewInit() {
    /*
    this.loginStatusSub = this.ats
      .loginStatus$
      .subscribe(val => val ? '' : this.cs.do('loginDialog', { tabIndex: 0, isPreEdu: true }));
    */
    this.cs.do('loginDialog', { tabIndex: 0, isPreEdu: true })
  }

  ngOnDestroy(){
    // this.loginStatusSub.unsubscribe();
  }

}
