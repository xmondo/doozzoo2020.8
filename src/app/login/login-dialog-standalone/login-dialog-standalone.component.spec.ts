import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginDialogStandaloneComponent } from './login-dialog-standalone.component';

describe('LoginDialogStandaloneComponent', () => {
  let component: LoginDialogStandaloneComponent;
  let fixture: ComponentFixture<LoginDialogStandaloneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginDialogStandaloneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginDialogStandaloneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
