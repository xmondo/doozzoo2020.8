import { Component, OnInit, ViewChild } from '@angular/core';
// import { AngularFireAuth } from '@angular/fire/auth';
import { ControllsService } from '../shared/controlls.service';
import { ActivatedRoute } from '@angular/router';
import { map, mergeMap, take, tap } from 'rxjs/operators';
import { iif, Observable, of, Subscription } from 'rxjs';
import { AccountStoreService } from '../shared/account-store.service';
import { MatStepper } from '@angular/material/stepper';

interface ModeState {
  modeState: boolean,
  currentMode: string,
  actionCode: string,
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  @ViewChild('stepper') stepper: MatStepper;
  allModes: Observable<string>;
  modeState: Observable<ModeState>
  //profileStatus: Observable<boolean>;
  //resolverUser: any;
  loginDebug: boolean = false;

  // stepper
  isEditable: boolean = true;
  stepperSelectedIndex: number = 1;
  defaultStepperIndex$: Observable<number>;

  stepsVisible$: Observable<boolean>;
  subscriptionVisible$: Observable<boolean>;
  vm$: Observable<any>;

  subContainer = new Subscription();

  constructor(
    //public a: AuthService,
    //private ref: ChangeDetectorRef,
    //public dialog: MatDialog,
    public cs: ControllsService,
    private r: ActivatedRoute,
    //private router: Router,
    public accountStore: AccountStoreService,
  ) {
      // ...
      // console.log('', this.r.snapshot.queryParams.mode)
      this.vm$ = this.accountStore.vm$;
  }

  ngOnInit() {
    // this.profileStatus = this.cs.loginEmitter;
    // queryParams.subscribe
    // all possible modes
    // 'resetPassword','recoverEmail','verifyEmail','login','logindelete','logout'
    const modes = ['resetPassword','recoverEmail','login','logindelete','logout']
    this.modeState = this.r.queryParams
      .pipe(
        // 
        map(val => {
          return {
            modeState: modes.includes(val['mode']) ?? false,
            currentMode: val['mode'],
            actionCode: val['oobCode'],
          }
        })
      )

    this.stepsVisible$ = this.accountStore
      .vm$
      .pipe(
        map(val => val.profileType !== 'student' ?? false)
      )
    this.subscriptionVisible$ = this.accountStore
      .plan$
      .pipe(
        map(val => (val === 'pro' || val === 'premium') ?? false)
      )

    // observable of the current status in account store,
    // mapped to the resulting step we would like to show,
    // if email is verified, we skip the first step
    const isEmailVerified$ = this.accountStore
      .emailVerified$
      .pipe(map(val => val ? 1 : 0));

    // we take the first emition, which should give us the potential step as get param
    // this returns an index in the array or a -1
    // for the latter we emit the email verification related index for the steps the regarding indexes
    this.defaultStepperIndex$ =  this.r.queryParams
      .pipe(
        take(1),
        map(val => ['validateEmail','profile','plan','subscription','done'].findIndex(item => item === val['step'])),
        mergeMap(val => iif(() => val !== -1, of(val), isEmailVerified$)),
        tap(val => this.stepperSelectedIndex = val),
      );
  }

  doSelectionChange(event) {
    console.log(event);
    this.stepperSelectedIndex = event.selectedIndex;
  }

// ### end of class ###
}
