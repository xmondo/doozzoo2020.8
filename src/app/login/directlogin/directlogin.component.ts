import { Component, NgZone, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/shared/auth.service';

@Component({
  selector: 'app-directlogin',
  template: '',
  // styleUrls: ['./directlogin.component.scss']
})
export class DirectloginComponent implements OnInit {
  constructor(
    private atr: ActivatedRoute,
    private ats: AuthService,
    private zone: NgZone,
    private router: Router,
  ) { }
  ngOnInit(): void {
    const u = this.atr.snapshot.queryParams.u;
    const p = this.atr.snapshot.queryParams.p;
    this.doLogin(u,p);
  }

  doLogin(u,p) {
    this.ats
      .auth$
      .signInWithEmailAndPassword(u, p)
      .then(val => {
        console.log('DirectLoginGuard: ', u, ' logged in sucessfully...');
        // redirecting causes automated tests to stuck...
        // this.zone.run(() => this.router.navigate(['/']))
      })
      .catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        if (errorCode === 'auth/wrong-password') {
          console.log('DirectLoginGuard: Wrong password.');
        } else {
          console.log('DirectLoginGuard: ', errorMessage);
        }
        // redirecting causes automated tests to stuck...
      }); 
  }
}
