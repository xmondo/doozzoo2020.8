import { Component, OnInit, Input } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-display-avatar',
  templateUrl: './display-avatar.component.html',
  styleUrls: ['./display-avatar.component.scss']
})
export class DisplayAvatarComponent implements OnInit {
  @Input() userId: string;
  avatar: Observable<any>;
  constructor(
    private rdb: AngularFireDatabase,
  ) { 

  }

  ngOnInit() {
    // console.log('---->', this.userId)
    if(this.userId === null || this.userId === undefined) {
      this.avatar = null;
    } else {
      this.avatar = this.rdb
        .object(`profiles/${this.userId}/contactData/avatar`)
        .valueChanges();
    }
  }

}
