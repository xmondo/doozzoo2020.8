import { Component, OnInit, OnDestroy, Output, EventEmitter  } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { debounceTime, distinctUntilChanged, distinctUntilKeyChanged, filter, map, tap} from 'rxjs/operators';
import { Observable, Subscription, of } from 'rxjs';

// TODO: useful?
interface internalVoucher {
  id: string;
  name: string;
  validFrom?: number;
  validTo?: number;
  action?: {
    name: string;
    value?: string;
  }
}

interface ValidationResponse {
  isValid: boolean,
  name: string,
}

@Component({
  selector: 'app-voucher',
  templateUrl: './voucher.component.html',
  styleUrls: ['./voucher.component.scss']
})
export class VoucherComponent implements OnInit, OnDestroy {
  @Output() vouchercode = new EventEmitter<string>();

  // httpFunctionsUrl: string = environment.httpFunctionsUrl;
  // https://europe-west1-doozzoo-dev.cloudfunctions.net/
  httpFunctionsUrl: string; // = 'https://europe-west1-doozzoo-dev.cloudfunctions.net/';
  // getStripeVoucher?token=XXXFabio

  voucherToken: string = '';
  status: string;
  couponListArray: Array<any>;
  vouchers: Observable<any>;
  vouchersValidation: Subscription;
  valResult: any = null;
  voucherObserver: Observable<any>;
  subContainer: Subscription = new Subscription;
  progressBar: boolean = false;

  constructor(
    private http: HttpClient,
  ) { }

  ngOnInit() {
    // this.getVouchers();
    if (environment.name === 'dev') {
      this.httpFunctionsUrl = 'https://europe-west1-doozzoo-dev.cloudfunctions.net/';
    } else if (environment.name === 'stage') {
      this.httpFunctionsUrl = 'https://europe-west1-project-6816589442602044124.cloudfunctions.net/';
    } else if (environment.name === 'prod') {
      this.httpFunctionsUrl = 'https://europe-west1-doozzoo-eve.cloudfunctions.net/';   
    }
  }

  getVouchers() {
    const url = this.httpFunctionsUrl + 'getStripeVoucher';
    const config = { headers: { 'Accept': 'application/json' } };
    // console.log('getStripeVoucherurl: ', url);
    this.vouchers = this.http.get(url, config)
      .pipe(
        filter(val => val !== null),
        map((val:any) => val.couponList),
      );  
      
    this.vouchers
      .pipe(map((val:any) => val.data))
      .subscribe(val => this.couponListArray = val);
  }

  doVoucherValidation() {
    this.progressBar = true;
    const url = this.httpFunctionsUrl + 'getStripeVoucherValidate?coupon=' + this.voucherToken;
    const config = { headers: { 'Accept': 'application/json' } };
    // console.log('getStripeVoucherValidate: ', url);
    const tmpSub = this.vouchersValidation = this.http.get(url, config)
      .pipe(
        filter(val => val !== null),
        // tap(val => console.log('vouchersValidation tap prefilter: ', val)),
        filter((val:any) => val.isCouponValid ?? false),
        map((val:any) => {
          return {
            isValid: val.isCouponValid.valid ?? false,
            name: val.isCouponValid.name ?? null,
          }
        }),
        distinctUntilKeyChanged('isValid'),
        tap(val => console.log('vouchersValidation tap: ', val)),
      )
      .subscribe((val:ValidationResponse) => {
        // console.log('vouchersValidation filtered: ', val);
        this.valResult = val;
        this.progressBar = false;
        // if valid store for subscription trigger
        if (val.isValid === true) {
          this.vouchercode.emit(this.voucherToken);
        }
      });
    this.subContainer.add(tmpSub);
  }

  validVoucherLength(voucher:string):boolean {
    return voucher.length > 4 && voucher.length <= 10 ? true : false;
  }

  checkVoucher(event:string) {
    console.log(this.validVoucherLength(event), event);
    if(this.validVoucherLength(event)) {
      this.doVoucherValidation();
    } else {
      this.valResult = null;
    }
  }

  ngOnDestroy() {
    this.subContainer.unsubscribe();
  }



// end of class
}
