import { Component, OnInit, Input } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Subscription, Observable, of, iif, interval, pipe } from 'rxjs';
import { MatChipInputEvent } from '@angular/material/chips';
import { FormControl } from '@angular/forms';
import { startWith, map, filter, defaultIfEmpty, withLatestFrom, mergeMap, switchMap } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmRemoveCoachComponent } from '../../edu/edu-coaches/confirm-remove/confirm-remove.component';
import { Subject } from 'rxjs/internal/Subject';

interface accounts {
  key: string,
  email: string,
  name: string,
  userId: string,
  role: number,
}

interface userPeers {
  institutionId: string,
  userId: string,
  peerId: string,
}

@Component({
  selector: 'app-peers',
  templateUrl: './peers.component.html',
  styleUrls: ['./peers.component.scss']
})
export class PeersComponent implements OnInit {
  @Input() userId: string; // userId of user to which we show peer relations
  @Input() key: string; // record key
  @Input() institutionId: string; // id of institution where the user is registered
  @Input() role: number; // role of the user to which we show peer relations
  @Input() accountsListObs: Observable<any>; // role of the user to which we show peer relations

  peersObservable: Observable<any>;
  accountsObservableSelected: Observable<any>;
  accountsObservableFiltered: Observable<any>;
  myControl = new FormControl();
  accountsFiltered: Observable<any>
  subContainer: Subscription = new Subscription();
  waitForChip: boolean = false;

  constructor(
    private afs: AngularFirestore,
    public dialog: MatDialog,
  ) { 
    // ...
  }

  async ngOnInit() {

    this.subscribePeers();
    // otherwise an empty autosuggest pane will be shown when focussing first time
    // this.accountsArr = await this.getAccounts();

    this.subscribeAccounts();

    /**
     * !!! not in use as we filter the ngFor of the option list with a external pipe, feeded by the input control !!!
     * create an Observable cointaining the items to be shown in autosuggest
     * triggered by input from myControl
     * filtered by matches with the complete accounts results
     * fetched by getAccounts
     */
    /*
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
    */
  }

  /*
  private _filter(value: string): {}[] {
    if(typeof value === 'string' || value === '') {
      // console.log('incomming value: ', value);
      const filterValue = value.toLowerCase();
      return this.accountsArr.filter((option:any) => {
        try{
          return option.email.toLowerCase().includes(filterValue);
        }
        catch(e){
          return false;
        }
        
      });
    }
  }
  */

  // subscribe to the peers connected to the userId of the line item (userId) of the list
  subscribePeers() {
    this.peersObservable = this.afs
      .collection('userPeers', ref => ref.where('userId', '==', this.userId).where('institutionId', '==', this.institutionId))
      .valueChanges({idField: 'key'});
  }

  subscribeAccounts() {
    /**
     * subscribe to the accounts of the complete institution
     * if the line item user is student (role = 0), take only coaches (role = 1) // filter
     * 
     */
    const accObs = this.afs
      .doc('institutions/' + this.institutionId)
      .collection('accounts')
      .valueChanges({idField: 'key'})
      .pipe(
        map((val:any) => val.map(item => {
          // ({ key: _val.key, ..._val })
          const data: accounts = {
            key: item.key,
            email: item.email,
            name: item.name,
            userId: item.userId,
            role: item.role,
          }
          return data;
        }).filter(_item => {
          if(this.role === 0) {
            return _item.userId !== this.userId && _item.role === 1 ;
          } else {
            return _item.userId !== this.userId;
          }
        }))
      );

      // this.accountsObservable = accObs; 

      /**
       * merge the peerObservable to filter the accountsObservable
       * with mergeMap we take both arrays and filter out the selected peers
       * as well as the line item userId
       * then we enrich the items with the key of the peer relation item, as this has to be deleted in case of using the remove function
       */
      this.accountsObservableSelected = this.peersObservable
        .pipe(
          mergeMap(peersArr => accObs
            .pipe(
              map(accountsArr => {
                return accountsArr.filter(item => {
                  const index = peersArr.findIndex(_item => item.userId === _item.peerId)
                  if( index !== -1 && item.userId !== this.userId) {
                    item.peerKey = peersArr[index].key;
                    return true;
                  } else {
                    return false;
                  }
                });
              })
            )
          )
        );
      /**
       * merge the peerObservable to filter the accountsObservable
       * with mergeMap we take both arrays and filter out the already connected peers
       * as well as the line item userId
       */
      this.accountsObservableFiltered = this.peersObservable
        .pipe(
          mergeMap(peersArr => accObs
            .pipe(
              map(accountsArr => {
                return accountsArr.filter(item => !peersArr.some(_item => item.userId === _item.peerId) && item.userId !== this.userId);
              })
            )
          )
        );

      

  }

  async addPeer(peer: userPeers) {
    const status = await this.afs
      .collection('userPeers')
      .add(peer)
      .catch(error => console.log('addPeer', error));
    this.waitForChip = false;
    return status;
  }

  async removePeer(docId: string) {

    const status = await this.afs
      .collection('userPeers')
      .doc(docId)
      .delete()
      .catch(error => console.log('removePeer', error));
    // console.log('remove peerId: ', docId);

    return status;
  }

  /** check whether record already exists */
  async getPeers(peer: userPeers) {
    const ref = await this.afs
      .collection('userPeers', ref => ref.where('institutionId', '==', peer.institutionId).where('userId', '==', peer.userId).where('peerId', '==', peer.peerId))
      .get()
      .toPromise();
    return ref.size > 0 ? true : false;
  }

  async add(event) {
    // console.log('+++', event.option.value)
    // {key: "Oy53a6dJ36aTKnN0dt22", email: "yanstudent@doozzoo.com", name: undefined, userId: "0xElPxatHPPAh1LF7wHdHM1BFzm2"}
    this.waitForChip = true;

    // console.log('key, val: ', this.key, this.myControl.value);
    
    // dirty trick: by setting value ? does not work
    // disabling and enabling again makes the input trigering the autocomplete pane
    this.myControl.setValue(null);
    this.myControl.disable()

    const peer: userPeers = {
      institutionId: this.institutionId,
      userId: this.userId,
      peerId: event.option.value.userId
    }

    const exists = await this.getPeers(peer);
    // console.log('exists: ', exists, peer);

    // dirty trick, see above...
    this.myControl.enable();

    if(exists) {
      this.waitForChip = false;
      console.log('connection exists');
      return 'connection exists';
    } else {
      // this.waitForChip = false;
      return this.addPeer(peer);
    }

  }

  confirmRemove(item): void {
    // console.log(item);
    const dialogRef = this.dialog.open(ConfirmRemoveCoachComponent, {
      width: '450px',
      data: { 
        clipTitle: item.title,
        messageId: 'peers.remove',
      }
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        // console.log('The dialog was closed', result);
        // result is 'true' or undefined
        if (result === true) {
          // trigger delete
          this.removePeer(item);
        }
      });
  }

  displayFn(option): string {
    return option && option.email ? option.email : '';
  }

  trackByKeyFn(item){
    return item.key;
  }

}
