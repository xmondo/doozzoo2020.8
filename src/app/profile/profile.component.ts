import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../shared/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';
// import { of, from } from 'rxjs';
import { filter, map} from 'rxjs/operators';
import { AccountState, AccountStoreService } from '../shared/account-store.service';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit, OnDestroy {
  localProgress: Boolean = false;
  role: any = -1;
  // coachOrCustomer = 'customer';
  coachOrStudent: 'student' | 'coach';
  nickname: any;
  foo: any;
  fooSub: any;
  coc: any;

  vm$: Observable<any>;
  cosSub: Subscription;
  showRadio$: Observable<boolean>;

  constructor(
    public a: AuthService,
    public snackBar: MatSnackBar,
    // public ref: ChangeDetectorRef
    public accountStore: AccountStoreService,
  ) {
    // ...
    this.vm$ = this.accountStore.vm$;
  }

  ngOnInit() {
    this.localProgress = true;

    this.cosSub = this.vm$
      .subscribe(val => this.coachOrStudent = val.state.profileType ?? 'student');
    
    // !(coachOrStudent === 'coach' && vm.accountStatus === 'planSubscribed')
    this.showRadio$ = this.vm$
      .pipe(
        map(val => val.profileType === 'coach' && val.accountStatus === 'planSubscribed' ? false : true)
      );


    /*
    this.a.usr$
      .pipe(
        filter(val => val !== null && val !== undefined)
      )
      .subscribe(val => {
        // console.log('ProfileComponent->usr$: ', val);
        if (val) {
          this.role = val.role;
          this.coachOrCustomer = val.role === 1 || val.userActions === 'coachRequestPending' ? 'coach' : 'customer';
        }
        this.localProgress = false;
        // this.ref.detectChanges();
      });
    */

    // invite pending?
    // request invite, for refetching it after e-Mail-verification
    // this.inviteStatus = JSON.parse(localStorage.getItem('doozzooInvite'));
    
  }

  ngOnDestroy() {
    this.cosSub.unsubscribe();
  }

  selectCoachCustomer(event) {
    console.log(event)
  }

}

/**
 * 1. fetch profile based on userId
 * show coach or customer version based on 1.
 * 2. take in profile data based on form input
 * 3. In case of valid submission, check role and set from -1 to 0 or keep 1 for coach(?)
 // tslint:disable-next-line:no-trailing-whitespace
 *
 */
