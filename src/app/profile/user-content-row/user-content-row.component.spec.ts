import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserContentRowComponent } from './user-content-row.component';

describe('UserContentRowComponent', () => {
  let component: UserContentRowComponent;
  let fixture: ComponentFixture<UserContentRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserContentRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserContentRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
