import { Component, OnInit, Input } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AuthService } from 'src/app/shared/auth.service';


@Component({
  selector: 'app-user-content-row',
  templateUrl: './user-content-row.component.html',
  styleUrls: ['./user-content-row.component.scss']
})
export class UserContentRowComponent implements OnInit {
  @Input() userId: any;
  @Input() layout: any;
  userData: any;
  userStatus: any;
  user: any;
  contactData: any;
  privateData: any;

  constructor(
    public db: AngularFireDatabase,
    public ats: AuthService,
  ) {
    this.layout = this.layout ?? 'default';
  }

  ngOnInit() {
    // ...
    this.userData = this.db.object('profiles/' + this.userId)
    // this.db.object('users/' + this.userId)
      .valueChanges()
      // .subscribe(val => this.userData = val);
  }

  cut(userData) {
    // char 1 to 2
    if (userData.contactData?.nickname) {
        return userData.contactData.nickname.slice(0, 2);
    } else if (userData.contactData?.email) {
        return userData.contactData.email.slice(0, 2);
    } else { return 'AA'; }
  }

  nameExists() {
    return true;
  }

// end of class
}
