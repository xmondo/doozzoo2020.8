import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService, UserData } from '../../shared/auth.service';
import { AngularFireDatabase, AngularFireObject } from '@angular/fire/database';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Countries } from '../../shared/countries';
import { HttpClient } from '@angular/common/http';
import { distinctUntilKeyChanged, filter, map, startWith } from 'rxjs/operators';
import { Avatar } from '../../shared/avatar/avatar';
import { pipe, Observable, Subscription } from 'rxjs';
import { ControllsService } from '../../shared/controlls.service';
// import { GoogleAnalyticsService } from 'src/app/shared/google-analytics.service';
import { AccountStoreService, ProfileType } from 'src/app/shared/account-store.service';

@Component({
  selector: 'app-coach',
  templateUrl: './coach.component.html',
  styleUrls: ['./coach.component.scss'],
  providers: [
    Countries,
    Avatar
  ]
})

export class CoachComponent implements OnInit {

  profileForm = new FormGroup({
    // avatar: new FormControl(''), //
    // title: new FormControl(''),
    firstname: new FormControl(''),
    lastname: new FormControl(''),
    nickname: new FormControl('', [
      Validators.required,
      Validators.minLength(4)
    ]),
    email: new FormControl(
      {
        value: '', disabled: true
      },
      [
        Validators.required,
        Validators.email
      ]
    ),
    dataApproval: new FormControl(''),
    // gender: new FormControl(''),

    address_street1: new FormControl(''),
    address_street2: new FormControl(''),
    zipcode: new FormControl(''),
    city: new FormControl(''),
    country: new FormControl(''),
    company: new FormControl(''),
    birthday: new FormControl(''),
    website: new FormControl(''),
    phone: new FormControl(''),

    language: new FormControl('en'),
    timezone: new FormControl('UTC+02:00'),

  });

  localProgress = false;
  profileData: any;
  ref: AngularFireObject<any>;
  userId: any;
  userData: any;
  avatarURL: string = 'assets/img/avatars/avataaars-default.png';
  stripeIdAvailable: Observable<boolean>;
  subContainer: Subscription = new Subscription();
  vm$: Observable<any>;

  stripeDebug: boolean = false;

  constructor(
    public countries: Countries,
    private http: HttpClient,
    public a: AuthService,
    public db: AngularFireDatabase,
    public snackBar: MatSnackBar,
    public avatar: Avatar,
    public cs: ControllsService,
    public accountStore: AccountStoreService,
    // public gas: GoogleAnalyticsService,
  ) { 
    this.vm$ = this.accountStore.vm$;
  }

  ngOnInit() {
    this.localProgress = true;

    const uidSub = this.accountStore
      .state$
      .pipe(
        filter(state => state.userId !== null),
        distinctUntilKeyChanged('userId'),
      )
      .subscribe(state => {
        this.userData = state;
        this.userId = state.userId;
        this.profileData = this.db
          .object(`profiles/${state.userId}`)
          .valueChanges();
        this.fetchProfile();
        // subscribe stripe status
        this.getStripeCustomer();
        this.localProgress = false;
      });
    this.subContainer.add(uidSub);

    const aem = this.a.avatarEmitter
      .pipe(filter(_val => _val !== null))
      .subscribe(val => {
        this.avatarURL = val;
        // dummy action to make form dirty
        this.profileForm.markAsDirty();
      });
    this.subContainer.add(aem);
  }

  // ### get stripe basics ###
  getStripeCustomer() {
    const sia = this.db
      .object('/stripe_customers/' + this.userId + '/customerData/id')
      .valueChanges();
    this.stripeIdAvailable = sia
      .pipe(
        map(val => val ? true : false),
        startWith(false),
      );
  } 

  fetchProfile() {
    // console.log('fetchProfile->userId: ', this.userId, this.userData.email);
    this.profileData
      .subscribe(result => {
        this.localProgress = false;
        if (result) {
          // provide avartar Url to avatar component
          // if != undefined
          if(result.contactData.avatar){
            this.avatarURL = result.contactData.avatar;
          }
          this.profileForm.patchValue({
            // avatar: result.contactData.avatar,
            // title: result.privateData.title,
            firstname: result.privateData.firstname,
            lastname: result.privateData.lastname,
            nickname: result.contactData.nickname,
            // email: result.contactData.email,
            // take allways from fb account data --> editing diabled
            email: this.userData.email,
            dataApproval: result.contactData.dataApproval,
            // gender: result.privateData.gender,

            address_street1: result.privateData.address_street1,
            address_street2: result.privateData.address_street2,
            zipcode: result.privateData.zipcode,
            city: result.privateData.city,
            country: result.privateData.country,
            company: result.privateData.company,
            birthday: result.privateData.birthday,
            website: result.privateData.website,
            phone: result.contactData.phone,

            language: result.privateData.language,
            timezone: result.privateData.timezone,

          });
        } else {
          // insert data from fb account
          this.profileForm.patchValue({
            nickname: this.userData.nickName,
            email: this.userData.email,
          });
        }
      });
  }

  saveProfile() {
    const data = {
      contactData: {
        avatar: this.avatarURL, 
        dataApproval: this.profileForm.get('dataApproval').value,
        // not necessary as field is disabled
        // email: this.profileForm.get('email').value,
        nickname: this.profileForm.get('nickname').value,
        phone: this.profileForm.get('phone').value,
      },
      privateData: {
        // title: this.profileForm.get('title').value,
        firstname: this.profileForm.get('firstname').value,
        lastname: this.profileForm.get('lastname').value,

        address_street1: this.profileForm.get('address_street1').value,
        address_street2: this.profileForm.get('address_street2').value || null,
        zipcode: this.profileForm.get('zipcode').value,
        city: this.profileForm.get('city').value,

        country: this.profileForm.get('country').value,
        company: this.profileForm.get('company').value || null,
        birthday: this.profileForm.get('birthday').value,
        website: this.profileForm.get('website').value || null,

        language: this.profileForm.get('language').value || null,
        timezone: this.profileForm.get('timezone').value || null
      }
    };

    let msg;
    this.db
      .object(`profiles/${this.userId}`)
      .update(data)
      .then(() => {
        msg = 'Profile was updated sucessfully';
        const snack = this.snackBar.open(
          msg,
          '',
          { duration: 2000 });
        // dummy action to make form pristine/default
        this.profileForm.markAsPristine();
      });

    // profileType can be set regardless of any conditions
    this.db
      .object(`users/${this.userId}`)
      .update({ 
        // role: 0, 
        updatedOn: Date.now(),
        profileType: ProfileType['coach'],
      })
      .then(() => {
        // google analytics
        // this.gas.eventEmitter('profile', 'coachRequestPending', this.userId);
        // TODO: setting on client side is not secure and conflics with the auth.usr observer
        // this.userData.role = 0;
        // this.a.userDataEmitter.next(this.a.userData);
      });

    // check role constraints, must be -1 or 0
    // 0 ist the case, when a student wants to convert to coach
    /*
    this.accountStore
      .role$
      // only if role was -1 before
      .pipe(filter(role => role === -1 || role === 0))
      .subscribe(role => {
        console.log('+++ role: ', role)
        this.db
          .object(`users/${this.userId}`)
          .update({ 
            // role: 0, 
            updatedOn: Date.now(),
            profileType: ProfileType['coach'],
          })
          .then(() => {
            // google analytics
            this.gas.eventEmitter('profile', 'coachRequestPending', this.userId);
            // TODO: setting on client side is not secure and conflics with the auth.usr observer
            // this.userData.role = 0;
            // this.a.userDataEmitter.next(this.a.userData);
          });
      });
    */

    /** 
     * we persist the flag 'coachRequestPending' in the user obj/fb
     * to ensure that in case of interuptions, the process can be picked up again 
     * and continued to the stripe subscription
     * we submit the event directly into the user object to propagate into all subscribing components
    */
    /*
    this.db
      .object('profiles/' + this.userId)
      .update(data)
      .then(() => {
        // dummy action to make form pristine/default again
        this.profileForm.markAsPristine();

        // coach application is initiated
        if (this.userData.role < 1 && !(this.userData.userActions === 'coachRequestPending')) {

          // same for avatar URL not visible for user
          this.userData.photoURL = this.avatarURL;
          this.userData.userActions = 'coachRequestPending';
          this.a.userDataEmitter.next(this.userData);

          // save flag 'coachRequestPending'in fb 
          this.apply4Coach();
        }

        const snackRef = this.snackBar.open(
          'Profile was updated sucessfully',
          '',
          { duration: 2000 });
        
      });
      */

  }

  apply4Coach() {
    this.ref = this.db.object('users/' + this.userId);
    this.ref
      .update({ userActions: 'coachRequestPending', updatedOn: Date.now() })
      .then(result => {      
        //console.log('user.userActions in db updated to "coachRequestPending"', this.userData);
        //console.log('result: ', result);
      });
  }

}
