import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MyroomsService } from 'src/app/myrooms/myrooms.service';
import { AccountStoreService, Plan } from 'src/app/shared/account-store.service';
import { UserData } from 'src/app/shared/auth.service';
import { DeltaTango } from 'src/app/shared/delta-tango';

@Component({
  selector: 'app-plans',
  templateUrl: './plans.component.html',
  styleUrls: ['./plans.component.scss'],
  providers: [
    DeltaTango,
    MyroomsService,
  ]
})

export class PlansComponent implements OnInit {
  @Input() userId: string;
  // @Output() planEmitter: EventEmitter<'starter'| 'pro' | 'premium'> = new EventEmitter();
  // planSelected: 'starter'| 'pro' | 'premium';
  myroomsCount$: Observable<any>;
  planSelected: Plan = null;
  starterExists$: Observable<boolean>;
  vm$: Observable<any>;

  upToInclusive:number = 5;
  maxFileCount:number = 11;

  constructor(
    // ...
    private db: AngularFireDatabase,
    private myrooms: MyroomsService,
    public accountStore: AccountStoreService,
  ) {
    this.vm$ = this.accountStore.vm$;
  }

  ngOnInit(): void {

    // wrap result numberinto an object to handle better async in template
    this.myroomsCount$ = this.myrooms
      .myroomsCount(this.userId)
      .pipe(
        map(val => {
          return { roomsCount: val }
        })
      );
    
    this.accountStore
      .plan$
      .subscribe(plan => this.planSelected = plan);
  }

  selectPlan(plan) {
    this.planSelected = plan;
    // this.planEmitter.next(plan);
  }

  confirmPlan(plan) {
    // this.planSelected = plan;
    // this.planEmitter.next(plan);
    switch(plan) {
      case 'starter':
        this.saveStarterPlan();
        break;
      case 'premium':
        // ...
        this.savePremiumPlan();
        break;
    }
  }

  /**
   * TODO: check if plan was booked already
   * downgrade should not be possible
   * role should not be set in frontend for payed plans
   * CFn should listen for plan changes:
   * -> set role 1 ok if...
   *  - if plan === starter && valid account (user, email validated, profile)
   *  - if plan === pro/premium && valid stripe subscription
   */
  async saveStarterPlan() {
    await this.db
      .object(`users/${this.userId}`)
      .update({plan: 'starter', role: 1})
      .catch(e => console.log('saveStarterPlan error: ', e));
    // now create the workshop room
    // roomName without prefix
    const result = await this.myrooms
      .createMyRoom(this.userId,'coach', Plan.starter);//, plan: string = 'undefined', roomName?: string)
    console.log('result: ', result);
  }

  async savePremiumPlan() {
    await this.db
      .object(`users/${this.userId}`)
      .update({plan: 'premium', role: -1}) // should we then allways reset role, so that subscription needs to be completed?
      .catch(e => console.log('savePremiumPlan error: ', e));
    // not here, first subscription must be completed
    // const result = await this.myrooms
    //   .createMyRoom(this.userId,'coach', Plan.premium);//, plan: string = 'undefined', roomName?: string)
  }

}
