import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../shared/auth.service';
import { AngularFireDatabase, AngularFireObject } from '@angular/fire/database';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Avatar } from '../../shared/avatar/avatar';
// import { GoogleAnalyticsService } from 'src/app/shared/google-analytics.service';
import { distinctUntilChanged, distinctUntilKeyChanged, filter, map } from 'rxjs/operators';
import { AccountStoreService, ProfileType } from 'src/app/shared/account-store.service';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss'],
  providers: [ Avatar ],
})
export class CustomerComponent implements OnInit, OnDestroy {
  inviteStatus = null;

  profileForm = new FormGroup({
    coachOrCustomer: new FormControl('customer'),
    // tslint:disable-next-line:max-line-length
    // TODO: integrate avatar selector 7 randomizer
    // tslint:disable-next-line:max-line-length
    // avatar: new FormControl(''),
    // title: new FormControl(''),
    firstname: new FormControl(''),
    lastname: new FormControl(''),
    nickname: new FormControl(''),
    email: new FormControl(
      { value: '', disabled: true },
      [ Validators.required, Validators.email ]
    ),
    dataApproval: new FormControl(''),
    // gender: new FormControl('')
  });

  localProgress = false;
  profileData: any;
  // ref: AngularFireObject<any>;
  userId: any = null;
  userData: any;
  avatarURL: string = 'assets/img/avatars/avataaars-default.png';
  vm$: Observable<any>;

  subContainer: Subscription = new Subscription();

  constructor(
    // private countries: Countries,
    public a: AuthService,
    public db: AngularFireDatabase,
    public snackBar: MatSnackBar,
    public router: Router,
    public accountStore: AccountStoreService,
  ) { 
    this.vm$ = accountStore.vm$;
  }

  ngOnInit() {
    this.localProgress = true;

    const uidSub = this.accountStore
      .state$
      .pipe(
        filter(state => state.userId !== null),
        distinctUntilKeyChanged('userId'),
      )
      .subscribe(state => {
        this.userData = state;
        this.userId = state.userId;
        this.profileData = this.db
          .object(`profiles/${state.userId}`)
          .valueChanges();
        this.fetchProfile();
        this.localProgress = false;
        // react on state changes
        // console.log('customer->ngOnInit->state: ', state);
      });
    this.subContainer.add(uidSub);

    const aem = this.a.avatarEmitter
      .pipe(filter(_val => _val !== null))
      .subscribe(val => {
        this.avatarURL = val;
        // dummy action to make form dirty
        this.profileForm.markAsDirty();
      });
    this.subContainer.add(aem);

    // invite pending?
    // request invite, for refetching it after e-Mail-verification
    this.inviteStatus = JSON.parse(localStorage.getItem('doozzooInvite'));
  }

  ngOnDestroy() {
    this.subContainer.unsubscribe();
  }

  fetchProfile() {
    // console.log('userId: ', this.userId);
    
    this.profileData
      .subscribe(result => {
        // console.log('user data: ', result);
        this.localProgress = false;
        // result might be null
        if (result) {
          // provide avartar Url to avatar component
          this.avatarURL = result.contactData.avatar;

          this.profileForm.patchValue({
            // avatar: result.contactData.avatar,
            // title: result.privateData.title,
            firstname: result.privateData.firstname,
            lastname: result.privateData.lastname,
            nickname: result.contactData.nickname,
            // email: result.contactData.email,
            // take allways from fb account data --> editing diabled
            email: this.userData.email,
            dataApproval: result.contactData.dataApproval,
            // gender: result.privateData.gender,
          });
        } else {
          // insert data from fb account
          this.profileForm.patchValue({
            nickname: this.userData.nickName,
            email: this.userData.email,
          });
        }
      });

  }

  // TODO: update role issue
  saveProfile() {
    // const gender = this.profileForm.get('title').value === 'Mr.' ? 'male' : 'female';
    const data = {
      contactData: {
        avatar: this.avatarURL, 
        dataApproval: this.profileForm.get('dataApproval').value,
        email: this.profileForm.get('email').value,
        nickname: this.profileForm.get('nickname').value,
        // phone: this.profileForm.get('phone').value,
      },
      privateData: {
        // title: this.profileForm.get('title').value,
        firstname: this.profileForm.get('firstname').value,
        lastname: this.profileForm.get('lastname').value,
      }
    };

    let msg;
    this.db
      .object(`profiles/${this.userId}`)
      .update(data)
      .then(() => {
        msg = 'Profile was updated sucessfully';
        const snack = this.snackBar.open(
          msg,
          '',
          { duration: 2000 });
        // dummy action to make form pristine/default
        this.profileForm.markAsPristine();
      });

    // update role to customer = 0
    // in case it was -1 before
    console.log('+++ role1 ', this.userData.role)
    this.accountStore
      .role$
      // only if role was -1 before
      .pipe(filter(role => role === -1))
      .subscribe(role => {
        console.log('+++ role2 ', role)
        this.db
          .object(`users/${this.userId}`)
          .update({ 
            role: 0, 
            updatedOn: Date.now(),
            profileType: ProfileType['student'],
          })
          .then(() => {
            // TODO: setting on client side is not secure and conflics with the auth.usr observer
            this.userData.role = 0;
            // this.a.userDataEmitter.next(this.a.userData);
            setTimeout(() => this.successRedirect(), 1000);
          });
      });
  }

  successRedirect() {
    // check whether an invite is pending
    // persist invite, for // etching it after e-Mail-verification
    const inviteStatus = JSON.parse(localStorage.getItem('doozzooInvite'));
    // TODO: should we check ids?
    // read invite Token and redirect to invite page for completion
    if (inviteStatus) {
      // update user data in session
      this.userData.userActions = 'inviteComplete';
      this.a.userDataEmitter.next(this.a.userData);
      this.storeInvite(inviteStatus);
    } else {
      // redirect to "done"...

    }

  }

  storeInvite(val) {
    console.log('storeInvite: ', val);
    // flag invite to trigger backendFunction
    this.db
      .object('invites/' + val.inviteToken)
      .update({ inviteeId: this.userId })
      .then(result => console.log('customer ', this.userId, ' was assigned to coach: ', val.coachId, result))
      .catch(error => console.log(error))

    this.db
      .object('coachCustomer/' + val.coachId)
      .update({ [this.userId]: true })
      .then(() => {
        console.log('customer ', this.userId, ' was assigned to coach: ', val.coachId);
      })
      .catch(error => console.log(error));
    // cleaning up
    localStorage.removeItem('doozzooInvite');
    this.inviteStatus = null;
  }

// end of class
}
