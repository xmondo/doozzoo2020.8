import { Component, OnInit, OnDestroy } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';
import { AuthService } from '../shared/auth.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-testdrive',
  templateUrl: './testdrive.component.html',
  styleUrls: ['./testdrive.component.scss']
})
export class TestdriveComponent implements OnInit, OnDestroy {
  signinSub: Subscription;
  constructor(
    public deviceInfo: DeviceDetectorService,
    public ats: AuthService,
    public router: Router,
  ) { }

  ngOnInit() {
    this.signinSub = this.ats.usr$
      .subscribe(val => {
        if(val.role === -2){
          this.signInAnonymously();
        } else if (val.role > -1){
          this.router.navigate(['/']);
        }
      });

    // indicator for login/registration process
    this.ats.setUserData('userActions', 'testdrive');

  }

  async ngOnDestroy() {
    this.signinSub.unsubscribe();
  }

  signInAnonymously() {
    // performs general sign in
    this.ats.auth$
      .signInAnonymously()
      .catch(function(error) {
        // Handle Errors here.
        const errorCode = error.code;
        const errorMessage = error.message;
        console.log(error);
      // ...
    });
  }

}
