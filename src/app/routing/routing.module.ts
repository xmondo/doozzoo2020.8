import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { AppComponent } from './app.component';
import { HomeComponent } from '../home/home.component';
import { RoomComponent } from '../room/room.component';
import { WaitingRoomComponent } from '../waiting-room/waiting-room.component';
import { ToolbarComponent } from '../apps/toolbar/toolbar.component';
import { RecorderComponent } from '../apps/recorder/recorder.component';
import { MetronomeComponent } from '../apps/metronome/metronome.component';
import { MixerComponent } from '../apps/mixer/mixer.component';
import { TunerComponent } from '../apps/tuner/tuner.component';
import { MediaPageComponent } from '../apps/media/media-page/media-page.component';
import { PreflightComponent } from '../preflight/preflight.component';
import { DelayComponent } from '../../app/preflight/delay/delay.component';
import { LooperComponent } from '../apps/looper/looper.component';
import { LoginComponent } from '../login/login.component';
import { ProfileComponent } from '../profile/profile.component';
import { MyCoachesComponent } from '../my-coaches/my-coaches.component';
import { MyStudentsComponent } from '../my-students/my-students.component';
import { InviteComponent } from '../invite/invite.component';
import { StripeComponent } from '../stripe/stripe.component';
import { ListUsersComponent } from '../admin/list-users/list-users.component';
import { DetailUsersComponent } from '../admin/detail-users/detail-users.component';

import { LoginPageResolverService } from 'src/app/shared/login-page-resolver.service';
import { WidgetAdminComponent } from 'src/app/admin/widget-admin/widget-admin.component';

import { InviteResolverService } from '../shared/invite-resolver.service';
import { TestdriveComponent } from '../testdrive/testdrive.component';
import { TestdriveResolverService } from '../shared/testdrive-resolver.service';
import { EduHomeComponent } from '../edu/edu-home/edu-home.component';

import { EduCoachesComponent } from '../edu/edu-coaches/edu-coaches.component';
import { EduProfileComponent } from '../edu/edu-profile/edu-profile.component';
import { EduCoachesDetailComponent } from '../edu/edu-coaches-detail/edu-coaches-detail.component';
import { StatisticsComponent } from '../admin/statistics/statistics.component';

// Guards
import { AdminGuardGuard } from 'src/app/shared/admin-guard.guard';
import { RoomGuard } from 'src/app/room/room.guard';
import { EduGuardGuard } from '../edu/edu-guard.guard';
import { LoginDialogStandaloneComponent } from '../login/login-dialog-standalone/login-dialog-standalone.component';
import { TestComponent } from '../test/test.component';
import { KeyboardComponent } from '../apps/keyboard/keyboard.component';
import { ToolbarPageComponent } from '../apps/toolbar/toolbar-page/toolbar-page.component';
import { DirectloginComponent } from '../login/directlogin/directlogin.component';
import { WorkshopRoomsComponent } from '../admin/workshop-rooms/workshop-rooms.component';



const routes: Routes = [
  { path: 'test', component: TestComponent },
  { 
    path: '', 
    component: HomeComponent,
    // canActivate: [ DefaultGuardGuard ],
    // resolve: { loginData: LoginPageResolverService }
  },
  { 
    path: 'login', 
    component: LoginComponent,
    runGuardsAndResolvers: 'always',
    // canActivate: [ DefaultGuardGuard ],
    // resolve: { loginData: LoginPageResolverService }
  },
  { 
    path: 'directlogin', 
    component: DirectloginComponent,
  },
  { 
    path: 'login/verify', 
    component: LoginComponent,
    runGuardsAndResolvers: 'always',
    // canActivate: [ DefaultGuardGuard ],
    // resolve: { loginData: LoginPageResolverService }
  },
  { 
    path: 'login/standalone', 
    component: LoginDialogStandaloneComponent,
    runGuardsAndResolvers: 'always',
    // canActivate: [ DefaultGuardGuard ],
    // resolve: { loginData: LoginPageResolverService }
  },
  {
    path: 'invite/:inviteToken',
    component: InviteComponent,
    resolve: { inviteData: InviteResolverService }
  },
  {
    path: 'testdrive',
    component: TestdriveComponent,
    //resolve: { _anonymous: TestdriveResolverService }
  },
  { path: 'profile', component: ProfileComponent },
  { path: 'mycoaches', component: MyCoachesComponent },
  { path: 'mystudents', component: MyStudentsComponent },
  { 
    path: 'presession', 
    component: PreflightComponent,
    runGuardsAndResolvers: 'always', 
  },
  { path: 'apps', component: ToolbarPageComponent },
  { path: 'apps/metronome', component: MetronomeComponent },
  { path: 'apps/mixer', component: MixerComponent },
  { path: 'apps/delay', component: DelayComponent },
  { path: 'apps/recorder', component: RecorderComponent },
  { path: 'apps/tuner', component: TunerComponent },

  { path: 'media', component: MediaPageComponent },
  // { path: 'media', component: MediaListComponent },

  { path: 'apps/looper', component: LooperComponent },
  { path: 'apps/keyboard', component: KeyboardComponent },
  // ### main room component
  { 
    path: 'room/:roomId', 
    component: RoomComponent,
    canActivate: [ RoomGuard ],
  },
  { 
    path: 'free/:roomId', 
    component: RoomComponent,
    canActivate: [ RoomGuard ],
  },
  { 
    path: 'plus/:roomId', 
    component: RoomComponent,
    canActivate: [ RoomGuard ],
  },
  { 
    path: 'pro/:roomId', 
    component: RoomComponent,
    canActivate: [ RoomGuard ],
  },
  { 
    path: 'room/:roomId/waitingroom', 
    component: WaitingRoomComponent,
  },
  { 
    path: 'free/:roomId/waitingroom', 
    component: WaitingRoomComponent,
  },
  { 
    path: 'plus/:roomId/waitingroom', 
    component: WaitingRoomComponent,
  },
  { 
    path: 'pro/:roomId/waitingroom', 
    component: WaitingRoomComponent,
  },
  { 
    path: 'admin/listusers', 
    component: ListUsersComponent,
    canActivate: [ AdminGuardGuard ],
  },
  { 
    path: 'admin/listusers', 
    component: ListUsersComponent,
    canActivate: [ AdminGuardGuard ],
  },
  { 
    path: 'admin/listusers/:userId', 
    component: DetailUsersComponent,
    canActivate: [ AdminGuardGuard ],
  },
  { 
    path: 'admin/widget', 
    component: WidgetAdminComponent,
    canActivate: [ AdminGuardGuard ],
  },
  {
    path: 'admin/statistics', 
    component: StatisticsComponent,
    canActivate: [ AdminGuardGuard ],
  },
  {
    path: 'admin/workshop-rooms', 
    component: WorkshopRoomsComponent,
    canActivate: [ AdminGuardGuard ],
  },
  { 
    path: 'edu', 
    component: EduHomeComponent,
    runGuardsAndResolvers: 'always',
    canActivate: [ EduGuardGuard ],
  },
  { 
    path: 'edu/:institutionId', 
    component: EduCoachesComponent,
    canActivate: [ EduGuardGuard ],
    // resolve: { inviteData: InviteResolverService }
  },
  { 
    path: 'edu/:institutionId/:institutionCoachId', 
    component: EduCoachesDetailComponent,
    canActivate: [ EduGuardGuard ],
    // resolve: { inviteData: InviteResolverService }
  },
  { 
    path: 'eduprofile', 
    component: EduProfileComponent,
    // canActivate: [ EduGuardGuard ],
    // resolve: { inviteData: InviteResolverService }
  },
  { 
    path: 'camera-access', 
    loadChildren: () => import('../gum-access/gum-access.module').then(m => m.GumAccessModule) 
  },

  { path: '**', redirectTo: '/', pathMatch: 'full' }
];

@NgModule({
  // enables reload of page when re requested
  // https://medium.com/engineering-on-the-incline/reloading-current-route-on-click-angular-5-1a1bfc740ab2
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
  exports: [RouterModule],
})
export class RoutingModule { }


