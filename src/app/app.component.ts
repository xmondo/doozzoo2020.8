import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { OpentokService } from './shared/opentok.service';
import { LoginDialogComponent } from './login/login-dialog/login-dialog.component';
import { ControllsService } from './shared/controlls.service';
import { AuthService } from './shared/auth.service';
import { combineLatest, of, Subscription, concat, merge } from 'rxjs';
import { filter, map, take, takeLast, distinctUntilChanged, mergeAll } from 'rxjs/operators';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { environment } from '../environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { CookieConsent } from './shared/cookie-consent/cookie-consent.component';
import { CookieConsentServiceService } from './shared/cookie-consent/cookie-consent-service.service';
import { AccountStoreService } from './shared/account-store.service';
//import { AngularFireAuth } from '@angular/fire/auth';
//import { PresenceService } from './shared/presence.service';


// declare ga as a function to set and sent the events
declare let ga: Function;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [
    // AccountStoreService,
    // PresenceService,
  ]
})
export class AppComponent implements OnInit, OnDestroy {

  subscriptionArr: Subscription = new Subscription();
  rootDebug: boolean = false;
  accountDebug: boolean = false;

  constructor(
    private ots: OpentokService,
    public dialog: MatDialog,
    public cs: ControllsService,
    public a: AuthService,
    // public ads: AudioService,
    public router: Router,
    private r: ActivatedRoute,
    private translateService: TranslateService,
    private ccs: CookieConsentServiceService,
    private accountStore: AccountStoreService,
    //private presence: PresenceService,
    //private auth: AngularFireAuth,
  ) {

    /**
     * TODO: work on persitence and presence
     * see following: https://medium.com/ngconf/run-code-when-you-leave-your-angular-app-d4ef30472d20
     */
    // local | session | none
    // this.auth.auth.setPersistence('none').then(result => console.log('setPersistence: ', result))
    // this.presence.initPresence();

    // internationalization
    this.translateService.addLangs(['en', 'de', 'nl', 'it', 'fr', 'es', 'ja', 'zH']);
    this.translateService.setDefaultLang('en');
    const tl = translateService.getBrowserLang();
    this.translateService.use(tl.match(/en|de|nl|it|fr|es|ja|zH/) ? tl : 'en');
    
    // ...
    // sets log level for the open tok service on root component
    this.ots.setOTLogLevel(0);

    // Push error to generic error handling component
    // reads opening data from controlls emittings
    const tmpSub1 = cs.sync('loginDialog')
      .subscribe(val => {
        this.openLoginDialog(val.payload.tabIndex, val.payload.isPreEdu, val.payload.currentUrl);
      });
    this.subscriptionArr.add(tmpSub1);
      const tmpSub2 = cs.sync('logout')
      .subscribe(payload => {
        // this.a.auth$.signOut();
        this.a.logout();
      });
    this.subscriptionArr.add(tmpSub2);

    console.log('environment name: ', environment.name);
  }

  ngOnInit() {
    // ...
    // tracking deactivated
    /*
    const trackSub = combineLatest([
      of(this.getTrackingCookieConsent()),
      this.a.isEdu$
        .pipe(
          filter(val => val !== null),
          distinctUntilChanged(),
        )
    ])
    .subscribe(val => {
      // console.log('cookie, isEdu: ', val);
      // if isEdu (2nd Observable), then in any case disable tracking or dont enable it better to say
      // if isEdu is false, take the value form the stored cookie into account
      if (val[1]) {
        // this.gaTrack(false); // NO tracking
        const data = this.ccs.getDefaultCookieConsent();
        data.trackingCookies = false;
        this.ccs.saveCookieConsent(data); // will save default No tracking status to localstorage
      } else {
        // set tracking according to user decision
        // this.gaTrack(val[0]);
      }
    })
    this.subscriptionArr.add(trackSub);
    */

  }

  ngOnDestroy() {
    this.subscriptionArr.unsubscribe();
  }

  /*
  getTrackingCookieConsent(): boolean | null {
    const cookieConsentData: CookieConsent = this.ccs.getCookieConsent();
    return cookieConsentData ? cookieConsentData.trackingCookies : null;
  }
  */

  // dialog functions
  openLoginDialog(tabIndex = 0, isPreEdu = false, currentUrl = null): void {
    const self = this;
    // console.log(tabIndex);

    const myDialogRef = this.dialog.open(LoginDialogComponent, {
      width: 'auto',
      data: {
        tabIndex: tabIndex,
        isPreEdu: isPreEdu,
        currentUrl: currentUrl,
      }
    });

    myDialogRef.afterClosed()
      .subscribe(result => {
        // console.log('The dialog was closed', result);
      });
  }

// ### end class ###
}

