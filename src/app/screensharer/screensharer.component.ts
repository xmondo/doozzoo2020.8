import { Component, ElementRef, OnInit, OnDestroy, AfterViewInit, ViewChild, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { OpentokService } from '../shared/opentok.service';
import { ControllsService } from '../shared/controlls.service';
import { filter, map } from 'rxjs/operators';
import { ThumbsManagerService } from '../shared/thumbs-manager.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-screensharer',
  templateUrl: './screensharer.component.html',
  styleUrls: ['./screensharer.component.scss']
})
export class ScreensharerComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('screensharerDiv') screensharerDiv: ElementRef;
  @Input() session: any; // OT.Session;
  @Input() userName: string;
  @Input() userId: any;
  @Input() photoURL: any;
  @Input() isOwner: boolean;
  screensharer: OT.Publisher;
  publishingState: Boolean = false;
  sessionState: Boolean = false;
  isSupported: Promise<any>;
  // localHeroStatus: Boolean = false;
  // localDistanceRight: Number = 0;
  thumbsId: string = null;
  isHero: boolean = true;
  toRight: number = 0;
  // session: any; // OT.Session;

  thumbsObj: any;

  // defines location of video item
  status: any; // class | right

  layoutGrid: boolean = false;

  audioLabel: string;
  videoLabel: string;

  // for managing bulk unsubscription
  subContainer: Subscription = new Subscription();

  constructor(
    public ots: OpentokService,
    public tms: ThumbsManagerService,
    public cs: ControllsService,
  ) {
    this.publishingState = false;
  }


  ngOnInit() {
    const OT = this.ots.getOT();

    if (this.session) {
      this.session.on('sessionConnected', () => {
        this.sessionState = true;
        // console.log('screensharer->sessionState: ', this.sessionState );
      });
      this.session.on('sessionDisconnected', () => {
        this.sessionState = false;
      });
      //console.log('screensharer->sessionState: ', this.sessionState );
    }

    // filters for trigger by payload 'fiftyFifty'
    this.ots.controlsEmitter
      .pipe(filter(val => val === 'fiftyFifty'))
      .subscribe( _ => this.layoutGrid = this.layoutGrid ? false : true);

    // init status attributes
    this.status = this.tms.getThumbsStatus(this.thumbsId);

  }

  ngOnDestroy() {
    try {
      if (this.screensharer) {
        this.screensharer.destroy();
        console.log('screensharer->ngOnDestroy');
      }
    } catch (error) {
      console.log(error);
    }
    
    this.subContainer.unsubscribe();
  }

  ngAfterViewInit() {
    // with every change, calc distance to right
    this.tms.thumbsEmitter
      // .pipe(filter(val => val.action === 'isHero'))
      .subscribe(val => {
        this.status = this.tms.getThumbsStatus(this.thumbsId);
      });

    this.preparePublish();

    // ### init event listener ###
    // with every change, calc distance to right
    const theSub = this.tms.thumbsEmitter
      // .pipe(filter(val => val.action === 'isHero'))
      .subscribe(val => {
        this.status = this.tms.getThumbsStatus(this.thumbsId);

        // get thumbsobject
        const thumbsArr = this.tms.thumbsArrayGet();
        this.thumbsObj = thumbsArr.find(item => item.id === this.thumbsId);
      });
    this.subContainer.add(theSub);

  }

  preparePublish() {
    let props: Object;
    props = {
      name: this.parseVideoName(),
      insertMode: <string>'append', // append | replace
      frameRate: <number>15, // 30, 15, 7, and 1
      width: <string>'100%',
      height: <string>'100%',
      videoSource: <string>'screen', // screen, window, application
      audioSource: <boolean>false,
      publishAudio: <boolean>false,
      publishVideo: <boolean>true,
      mirror: <boolean>false,
      fitMode: <string>'contain',
      maxResolution: {
        width: <number>1280,
        height: <number>900
      }
    };

    this.screensharer = OT.initPublisher( this.screensharerDiv.nativeElement, props );

    this.screensharer.on('streamCreated', event => {
      // console.log('screensharer->streamCreated; ', event);
      this.publishingState = true;
      this.cs.do('controllsComponent', { publishingState: true });
      this.thumbsId = event.stream.streamId;
      this.tms.thumbsEmitter.next({ action: 'create', id: event.stream.streamId, event: event });
    });

    this.screensharer.on('streamDestroyed', (event: any) => {
      // console.log('screensharer->streamDestroyed; ', event);
      this.publishingState = false;
      // this.cs.do('controllsComponent', { publishingState: false });
      this.tms.thumbsEmitter.next({ action: 'delete', id: event.target.streamId, event: event });

      // emitters must emit here as in the next step the cs service is somehow not available any more
      this.cs.do('controllsComponent', { publishingState: false });
      this.cs.do('buttonInstallScreensharer', { action: false });

      console.log('screensharer-->stream destroyed');
    });

    this.screensharer.on('destroyed', function(event: any) {
      // ...
      // self.ots.streamEmitter.emit({ action: 'publisherDestroyed', event: event });
      // self.tms.thumbsEmitter.next({ action: 'delete', id: event.target.streamId });
      // console.log('screensharer destroyed...', event);
      console.log('screensharer-->destroyed');
    });

    // setTimeout(()=> this.publish(), 1000);
    this.publish();

  }

  publish() {
      // console.log('screensharer->session: ', this.session);
    if(this.session.currentState !== 'connected'){
      // console.log(this.session.currentState);
    }
    this.session.publish(this.screensharer, (error) => {
      if (error) {
        console.log('screensharer->error: ', error);
        this.cs.do('buttonInstallScreensharer', { action: false });
      } else {
        // console.log('screensharer: started publishing');
      }
    });
  }

  stopPublish() {
    this.screensharer.destroy();
    // this.publishingState = false;
  }

  parseVideoName() {
    const data = {
      id: this.userId,
      name: this.userName,
      isOwner: this.isOwner,
      avatar: this.photoURL,
      audioLabel: 'Framerate: 30fps',
      videoLabel: 'Shared Screen',
    } 
  
    // artificial
    // this.audioLabel = 'Shared Screen';
    this.videoLabel = 'Shared Screen';
  
    return JSON.stringify(data);
  }

// end of class
}
