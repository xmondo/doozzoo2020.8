import { Component, OnInit } from '@angular/core';
import { ControllsService } from '../../shared/controlls.service';
// import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { OpentokService } from '../../shared/opentok.service';

@Component({
  selector: 'app-screensharer-controlls',
  templateUrl: './controlls.component.html',
  styleUrls: ['./controlls.component.scss']
})
export class ControllsComponent implements OnInit {
  sessionState: Observable<any>;
  publishingState: Observable<any>;
  isSupported = 'show'; //'install';

  // for debugging
  // screenshareObserver: Observable<boolean>;

  constructor(
    public cs: ControllsService,
    public ots: OpentokService,
  ) {
    /**
     * filter via service for specific controlls event stream -> controllsComponent (1)
     * prefiltering is done in service
     * subscription is filtered for specific event (2) and resulting value is processed via map
     */
    // (1)
    const cs$ = cs.sync('controllsComponent');

    // (2)
    this.sessionState = cs$.pipe(
      filter(val => val.payload.sessionState !== undefined),
      map(val => val.payload.sessionState)
    );

    this.publishingState = cs$.pipe(
      filter(val => val.payload.publishingState !== undefined),
      map(val => val.payload.publishingState)
    );

  }

  ngOnInit() {
    // ...
  }

  stopPublish() {
    this.cs.do('buttonInstallScreensharer', { action: false });
  }

  preparePublish() {
    this.cs.do('buttonInstallScreensharer', { action: true });
  }

}
