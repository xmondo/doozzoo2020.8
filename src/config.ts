export default {
  // Set this to the base URL of your sample server, such as 'https://your-app-name.herokuapp.com'.
  // Do not include the trailing slash. See the README for more information:
  SAMPLE_SERVER_BASE_URL: 'http://localhost:4200',
  // OR, if you have not set up a web server that runs the learning-opentok-php code,
  // set these values to OpenTok API key, a valid session ID, and a token for the session.
  // For test purposes, you can obtain these from https://tokbox.com/account.
  API_KEY: '46105142',
  SESSION_ID: '1_MX40NjEwNTE0Mn5-MTU0NDI5Njg1MzIxMH41R1ROcDFPZGZRT25NU1VCYk45cGo4M2x-UH4',
  TOKEN: `T1==cGFydG5lcl9pZD00NjEwNTE0MiZzaWc9N2I0NmU0NjdjZWJkYjNhMDI2MWRjMzc4NjhlY2Y5MzkyYzY0M2UyMTpzZXNzaW9uX2lkPTFfTVg0ME5qRXdOVEUwTW41LU1UVTBOREk1TmpnMU16SXhNSDQxUjFST2NERlBaR1pSVDI1TlUxVkNZazQ1Y0dvNE0yeC1VSDQmY3JlYXRlX3RpbWU9MTU0NDI5NjkwNCZub25jZT0wLjgzMjMwMDY2NTg1OTYzODQmcm9sZT1wdWJsaXNoZXImZXhwaXJlX3RpbWU9MTU0Njg4ODkwMyZpbml0aWFsX2xheW91dF9jbGFzc19saXN0PQ==`,

  THUMB_WIDTH: 180, // 120,
  THUMB_HEIGHT: 135, // 90,
};
