/**
 * TODO: ...
 */

// indirect ref of admin
const admin = require('./fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();

const completeInvite = async (snap, context) => {
    // snap.before.exists() // brand new data
    // snap.after.exists() // data that was exisitng before

    console.log(JSON.stringify(snap), JSON.stringify(context));
    const inviteId = context.params.inviteId;
    const inviteData = snap.after.val();

    // delete ONLY in this case, because it is the ending point for the EDU flow
    if(inviteData.userActions === 'createdByInstitution') {
        //const before = snap.before.data();
        //const after = snap.after.data();

        // await updateUserData(inviteData);
        return updateUserData(inviteData);

        // war scheinbar ein Fehler das hier zu positionieren
        // return deleteInvite(inviteId);
    } else {
        // TODO: update coachCustomer here
        // do nothing
        console.log('non EDU invite was updated...');
        return 'non EDU invite was updated...';
        // return deleteInvite(inviteId);
    }
    
};

async function deleteInvite(inviteId) {
    try{
        await admin.database().ref(`invites/${inviteId}`).remove(); 
    }
    catch(error) {
        console.log('deleteInvite: ', inviteId, JSON.stringify(error));
    }
    return 'invite obj deleted...';
}

async function updateUserData(inviteData) {
    const data = {
        // userActions: 'createdByInstitution',
        userActions: 'inviteComplete',
        trigger: {
            initiatorId: inviteData.initiatorId,
            triggerId: inviteData.uid,
            type: 'institution'
        }
    }
    return await admin.database().ref('users/' + inviteData.inviteeId).update(data);
}

// final export
module.exports = completeInvite;

