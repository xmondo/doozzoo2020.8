// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
// CORS Express middleware to enable CORS Requests.
const cors = require('cors')({origin: true});

interface inviteTokenData {

}

const validateInviteToken = async (req, res) => {
    const token = req.query.token || 'no token provided';
    const scope = req.query.cope || null;
    
    console.log('token/scope: ', token, scope);
    
    const inviteLookup = await admin.database()
        .ref('invites/' + token)
        .once('value');

    console.log('inviteLookup data ', inviteLookup);

    const userLookup = await admin.database()
        .ref('user/' + inviteLookup.coachId)
        .once('value');

    const data = { 
        action: userLookup.role > -1 ? 'userExists' : 'registerOrLogin',
        nickname: val.nickname, 
        coachId: val.uid, 
        customerId = inviteLookup.uid;
        timestamp: val.timestamp, 
        inviteToken: tk 
    }
    val.action = _val.role > -1 ? 'userExists' : 'registerOrLogin';
    val.customerId = _val.uid;
    val.role = _val.role;
    val.emailVerified = _val.emailVerified;

    // Enable CORS using the `cors` express middleware.
    const response = data => {
		cors(req, res, () => {
			// expects data obj
			return res.status(200)
		        .type('application/json')
		        .send(data)
		});
  	}
  	data = {
        status: 'success',
        couponList: couponList,  
	}
    response(data);
};

// final export
module.exports = {
    validateInviteToken,
    // more...
}