
const admin = require('../fooAdmin');
const fst = admin.firestore();

const addPeerConnection = async function addPeerConnection(userId, peerId, institutionId) {
    // 1. make sure that connection not already exists for userId
    const resultUser = await fst
        .collection('userPeers')
        .where('userId', '==', userId)
        .where('peerId', '==', peerId)
        .where('institutionId', '==', institutionId)
        .get()
        .catch(error => console.log('result error: ', error));
    // 
    if (resultUser.size > 0) {
        // console.log('result: ', resultUser.size);
        console.log('primary connection exists...');
        return 'primary connection exists...';
    } else {
        const data = {
            institutionId: institutionId,
            userId: userId,
            peerId: peerId,
        }
        // 2. connect...
        return await fst
            .collection('userPeers')
            .add(data);
    }
}
module.exports = addPeerConnection;