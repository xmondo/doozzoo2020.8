/**
 * fn listens for writes in userPeers
 * if a relation is added it checks whether a complementary entry exists
 * if not it creates it
 * in case of removing an entry all complementaries are removed as well
 * Additionally these relations are synced with coachCustomer
 */

// indirect ref of admin
const admin = require('../fooAdmin');
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

const syncUserPeers = async (snap, context) => {
    // .document('userPeers/{docId}')

    //console.log('snap, context: ', snap, context);
    console.log('snap.before.exists: ', snap.before.exists);
    /*
    console.log('snap.before', snap.before.data());
    console.log('snap.after', snap.after.data());
    */
    // in case of deletion, after === null
    const after = snap.after.exists ? snap.after.data() : null;
    // Get an object with the previous document value (for update or delete)
    const before = snap.before.data();

    /**
     * cases for create and delete first
     */
    // case create
    if(snap.before.exists === false) {
        // newly created record
        console.log('newly created', after);
            // add complementary
        await addComplementaryPeer(after)
        return addCoachCustomer(after);
    // case delete
    } else if (after === null) {
        // deleted record
        console.log('just deleted', before);
        // TODO: deactivate for MIGRATION
        await removeComplementaryPeer(before)
        return removeCoachCustomer(before);  
    } else {
        return 'nothing applied...';
    }
    
}

/**
 * coachCustomer relation will be added,
 * but only for users, which are coaches:
 * Relation: coach -> customer
 */
async function addCoachCustomer(after) {
    // check role
    const role = await db
        .ref(`users/${after.userId}/role`)
        .once('value');
    if(role.val() === 1) {
        const coachId = after.userId;
        const customerId = after.peerId;
        // update coachCustomer
        const updates = {};
        updates['/coachCustomer/' + coachId + '/' + customerId] = true;
        return admin.database().ref().update(updates);
    } else {
        return 'role not 0, coachCustomer not updated...';
    }
}

async function removeCoachCustomer(before) {
    const coachId = before.userId;
    const customerId = before.peerId;
    const updates = {};
    updates['/coachCustomer/' + coachId + '/' + customerId] = null;
    return admin.database().ref().update(updates);
}

async function addComplementaryPeer(after) {
    const institutionId = after.institutionId;
    const userId = after.userId;
    const peerId = after.peerId;

    // check whether complementary entry already exists
    const result = await fst
        .collection('userPeers')
        .where('institutionId', '==', institutionId)
        .where('userId', '==', peerId)
        .where('peerId', '==', userId)
        .get()
        .catch(error => console.log('result error: ', error));
    // 
    if (result.size > 0) {
        console.log('result: ', result.size);
        return 'peer already existing...';
    } else {
        // ad complementary data
        const data = {
            institutionId: institutionId,
            userId: peerId,
            peerId: userId,
        }
        console.log('complementary peer added...');
        return fst
            .collection('userPeers')
            .add(data);
    }
}

async function removeComplementaryPeer(after) {
    const institutionId = after.institutionId;
    const userId = after.userId;
    const peerId = after.peerId;

    const result = await fst
        .collection('userPeers')
        .where('userId', '==', peerId)
        .where('peerId', '==', userId)
        .where('institutionId', '==', institutionId)
        .get();
    console.log('complementary peer deleted...');
    return result.forEach(item => fst
        .collection('userPeers')
        .doc(item.id)
        .delete());
}

// final export
module.exports = syncUserPeers;

