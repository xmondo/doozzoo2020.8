
const admin = require('../fooAdmin');
const fst = admin.firestore();
const db = admin.database();

const syncUserPeersFromCoachCustomers = async function syncUserPeersFromCoachCustomers(snap, context) {
    // -> .ref('/coachCustomer/{coachId}/{customerId}').onDelete
    // console.log('snap, context.params' , snap, context.params);
    const coachId = context.params.coachId;
    const customerId = context.params.customerId;
    const institutionId = await db    
        .ref(`users/${coachId}/trigger/institutionId`)
        .once('value');

    try {
        console.log('coachId, customerId', coachId, customerId, institutionId.val());
        await removePeerConnection(coachId, customerId, institutionId.val());
        return `deleted peer ${customerId}`;
    }
    catch(error) {
        const msg = 'deletion of peer failed: ' + error;
        return msg;
    }
}

async function removePeerConnection(userId, peerId, institutionId) {

    const result = await fst
        .collection('userPeers')
        .where('userId', '==', peerId)
        .where('peerId', '==', userId)
        .where('institutionId', '==', institutionId)
        .get();
    console.log('complementary peer deleted...');
    return result.forEach(item => fst
        .collection('userPeers')
        .doc(item.id)
        .delete());
}

module.exports = syncUserPeersFromCoachCustomers;