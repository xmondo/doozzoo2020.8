/**
 * TODO: ...
 */

const functions = require('firebase-functions');
const firestore = require('@google-cloud/firestore');
// indirect ref of admin
const admin = require('./fooAdmin');
// const client = new admn.firestore.v1.FirestoreAdminClient();
const client = new admin.firestore.v1.FirestoreAdminClient();

// put into config
const bucket = functions.config().backup.bucket; // 'gs://doozzoo-dev-firestore-backups';

const scheduledFirestoreExport = (context) => {
    const projectId = process.env.GCP_PROJECT || process.env.GCLOUD_PROJECT;
    const databaseName = client.databasePath(projectId, '(default)');
    console.log('databaseName', databaseName);

    admin.firestore.FirestoreAdminClient

    return client.exportDocuments({
        name: databaseName,
        outputUriPrefix: bucket,
        // Leave collectionIds empty to export all collections
        // or set to a list of collection IDs to export,
        // collectionIds: ['users', 'posts']
        collectionIds: []
    }).then(responses => {
        console.log(`Operation Name: ${responses['name']}`);
        const response = responses[0];
        return response;
    })
    .catch(err => {
        console.error(err);
        throw new Error('Export operation failed');
    });

// end of main class
}
    
// final export
module.exports = scheduledFirestoreExport;

