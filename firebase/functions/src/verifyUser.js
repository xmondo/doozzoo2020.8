

// CORS Express middleware to enable CORS Requests.
const cors = require('cors')({origin: true});
const admin = require('./fooAdmin');

const verifyEmail = async (req, res) => {
    // const oobCode = req.query.oobCode || 'no token provided';
    // const uid = req.query.oobCode;
    const email = req.query.email;
    // const email = req.query.email;
    let error = false;
    let errorMsg = null;

    // validate uid/email here
    // if valid, set EMAILVERIFIED

    // https://doozzoo.com/login
    // ?mode=verifyEmail
    // &oobCode=55Iflj6un9NnyC74XojeP78PeMIcpI8nCY4w4ZFy7e4AAAFw6YqyZg // test -> oob code = uid
    // ### &uid=<uid>

    const user = await admin.auth().getUserByEmail(email);
    if(user) {
      const uid = user.uid;
      admin.auth().updateUser(uid, {
        // email: 'modifiedUser@example.com',
        // phoneNumber: '+11234567890',
        emailVerified: true,
        // password: 'newPassword',
        displayName: email,
        // photoURL: 'http://www.example.com/12345678/photo.png',
        // disabled: true
      })
      .then((userRecord) => {
        // See the UserRecord reference doc for the contents of userRecord.
        console.log('Successfully updated user', userRecord.toJSON());
        return userRecord;
      })
      .catch((error) => {
        console.log('Error updating user:', error);
        error = true;
        errorMsg = error;
      });
    } else {
      error = true;
      errorMsg = 'user was not created';
    }

    // Enable CORS using the `cors` express middleware.
    const response = data => {
      cors(req, res, () => {
        // expects data obj
        return res.status(200)
              .type('application/json')
              .send(data)
      });
  	}
  	data = {
        status: 'success',
        emailVerified: verifyStatus,
        user: JSON.stringify(user),
	  }
    error ? response({ status: 'error', statusMsg: statusMsg }) : response(data);
};

// final export
module.exports = {
    verifyEmail
}