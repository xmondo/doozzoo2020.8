

// indirect ref of admin
const admin = require('./fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
// const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

const getMigrationStats = require('./documents/getMigrationStats');
const migrationControlTable = require('./documents/migrationControlTable');
const copyShared = require('./documents/copyShared');
const cleanShared = require('./documents/cleanShared');
const syncFromBucketAssets = require('./documents/syncFromBucketAssets');
const monitorDocuments = require('./documents/__monitorDocuments').monitorDocuments;
const monitorSharedDocuments = require('./documents/__monitorDocuments').monitorSharedDocuments;

const promisePool = require('es6-promise-pool');
const { get } = require('lodash');
const { DataSnapshot } = require('firebase-functions/lib/providers/database');
const PromisePool = promisePool.PromisePool;
const MAX_CONCURRENT = 10;

const foo = async (req, res) => {
    const name = req.query.name || 'there';
    if(req.query.secure === 'hotzenplotz48') {

        // calc stats
        const total = 13627;
        const totalGTZero = await fst
            .collection('migrationControl/mediaUsers/users')
            .where('totalDocuments', '>', 0)
            .get();
        const totalMigrated = await fst
            .collection('migrationControl/mediaUsers/users')
            .where('migrated', '==', true)
            .get();
        const percent = (totalMigrated.size / totalGTZero.size) * 100;

        if(req.query.coachId && req.query.action === 'sync') {
            // 1. sync from existing objects in the bucket to media collection
            const pageSize = req.query.pageSize || 1000
            const pageToken = req.query.pageToken || null
            const dryrun = req.query.dryrun || null
            await syncFromBucketAssets(req.query.coachId, pageSize, pageToken, dryrun);
            // 2. sync from existing index in media collection to existing objects in bucket
            // await monitorDocuments(req.query.coachId);
            // 3. sync from existing index in sharedMedia collection to existing objects in bucket
            // await monitorSharedDocuments(req.query.coachId);

            res.send(`Hi ${name} we synced the assets!`);
            
            return 'we synced the assets!';
        } else if (req.query.action === 'reset') {
            await reset(req.query.limit, req.query.step, req.query.migrated, req.query.max);
            res.send(`Hi ${name} we RESET!`);
            return true;
        } else if (req.query.action === 'getMigrationStats') {
            await getMigrationStats();
            res.send(`Hi ${name} we got MigrationStats!`);
            return true;
        } else if (req.query.action === 'initControlTable') {
            await migrationControlTable();
            res.send(`Hi ${name} we initControlTable!`);
            return true;
        } else if (req.query.action === 'doMigration') {
            await doMigration(req.query.limit, req.query.step, req.query.migrated, req.query.max);
            res.send(`<p>Hi ${name} we doMigration! -> limit: ${req.query.limit}; step: ${req.query.step}; migrated: ${req.query.migrated}; max: ${req.query.max};</p><p>Here our stats: total: ${total}; total > 0: ${totalGTZero.size}; total Migrated: ${totalMigrated.size}; Percent migrated:  ${percent}% </p>`);
            return true;
        } else if (req.query.action === 'sharedDocuments') {
            await copyShared();
            const totalCopied = await fst
                .collection(`sharedMedia`)
                .get()
            console.log(`***number of shared docs to create: `, totalCopied.size);
            res.send(`Hi ${name} we copied ${totalCopied.size} items to sharedDocuments!`);
            
            return `***number of shared docs to create: ${totalCopied.size}`;

        } else if (req.query.action === 'cleanSharedDocs') {
            await cleanShared(req.query.coachId);
            res.send(`Hi ${name} we cleanShared!`);
        } else {
            // do simply nothing
            res.send(`Hi ${name} we did nothing!`);
            
            return 'we did nothing!';
        }
    } else {
        res.send(`Hi ${name}, secret is missing, we did nothing!`);
        
        return 'we did nothing!';
    }
};

/**
 * 
 * @param {*} limit 
 * @param {*} step -> step1 - step3 | all
 * @param {*} migrated -> boolean 
 * @param {*} max -> int
 */
async function doMigration(limit, step, migrated, max) {
    // check whether an object with the same name already exists in index
    limit = parseInt(limit) || 5;
    step = step || 'step1';
    migrated = migrated === 'false' ? false : true;
    max = parseInt(max);

    const migList = await fst
        .collection('migrationControl/mediaUsers/users')
        .where('migrated', '==', migrated)
        .where('totalDocuments', '>', 0)
        .where('totalDocuments', '<=', max)
        .limit(limit)
        .get()
        .catch(error => console.log('result error: ', error));

    const coachIds = [];    
    migList.forEach(val => coachIds.push(val.data().userId));
    let msgList = '';
    coachIds.forEach(item => {
        msgList = msgList + ', ' + item;
    });

    // work in a controlled promise pool
    // promise pool functions must NOT be async!!
    const promisePool = new PromisePool(() => doMiglist(coachIds, step), MAX_CONCURRENT);
    return promisePool
        .start()
        .then(() => {
            console.log(`Coaches synced: ${msgList}`);
            return true;
        })
        .catch((error) => {
            console.log('Coaches errors, ome promise rejected: ', error.message);
            return false;
        }); 
}

// synchronus!!!
function doMiglist(coachIds, step) {
    if (coachIds.length > 0) {
        // get first item and remove it from arr
        const coachId = coachIds.shift();
        // console.log('coachId: ', coachId);
        if(step === 'step1') {
            return syncFromBucketAssets(coachId);
        } else if(step === 'step2') {
            return monitorDocuments(coachId);
        } else if(step === 'step3') {
            return monitorSharedDocuments(coachId);
        } else {
            return 'do nothing';
        }             
    } else {
        return null;
    }
}

async function reset(limit, step, migrated, max) {
    // check whether an object with the same name already exists in index
    limit = parseInt(limit) || 5;
    step = step || 'step1';
    migrated = migrated === 'false' ? false : true;
    max = parseInt(max);

    const migList = await fst
        .collection('migrationControl/mediaUsers/users')
        .where('migrated', '==', migrated)
        .where('totalDocuments', '>', 0)
        .where('totalDocuments', '<=', max)
        .limit(limit)
        .get()
        .catch(error => console.log('result error: ', error));

    return migList
        .forEach(item => item.ref.set({ migrated: false, step1done: false }, { merge: true }));
    
}

module.exports = foo;

/*
DEV
// migrate a specific userId
https://europe-west1-doozzoo-dev.cloudfunctions.net/doFoo?name=Flo&secure=hotzenplotz48&coachId=2X1ZM44k8lXX6t2w1X17fx76DPK2&action=sync

https://europe-west1-doozzoo-dev.cloudfunctions.net/doFoo?name=Flo&secure=hotzenplotz48&coachId=2X1ZM44k8lXX6t2w1X17fx76DPK2&action=sync&pageSize=3

https://europe-west1-doozzoo-dev.cloudfunctions.net/doFoo?name=Flo&secure=hotzenplotz48&coachId=2X1ZM44k8lXX6t2w1X17fx76DPK2&action=sync&pageSize=3&pageToken=Cktkb2N1bWVudHMvMlgxWk00NGs4bFhYNnQydzFYMTdmeDc2RFBLMi8xMV9GYW1pbHkgQWZmYWlyIC0gTWFyeSBKLiBCbGlnZS5wZGY=

https://europe-west1-doozzoo-dev.cloudfunctions.net/doFoo?name=Flo&secure=hotzenplotz48&coachId=2X1ZM44k8lXX6t2w1X17fx76DPK2&action=sync&pageSize=3&dryrun=true

https://europe-west1-doozzoo-dev.cloudfunctions.net/doFoo?name=Flo&secure=hotzenplotz48&coachId=2X1ZM44k8lXX6t2w1X17fx76DPK2&action=sync&pageSize=3&pageToken=Cktkb2N1bWVudHMvMlgxWk00NGs4bFhYNnQydzFYMTdmeDc2RFBLMi8xMV9GYW1pbHkgQWZmYWlyIC0gTWFyeSBKLiBCbGlnZS5wZGY=&dryrun=true



// copies all shared documents into the firestore table
https://europe-west1-doozzoo-dev.cloudfunctions.net/doFoo?name=Flo&secure=hotzenplotz48&action=sharedDocuments

// populate controlTable 
https://europe-west1-doozzoo-dev.cloudfunctions.net/doFoo?name=Flo&secure=hotzenplotz48&action=initControlTable

// do Migration 
https://europe-west1-doozzoo-dev.cloudfunctions.net/doFoo?name=Flo&secure=hotzenplotz48&action=doMigration&limit=1&step=step1&migrated=false&max=0

https://europe-west1-doozzoo-dev.cloudfunctions.net/doFoo?name=Flo&secure=hotzenplotz48&action=doMigration&limit=1&step=all&migrated=false&max=0

### cleansharedDocs
https://europe-west1-doozzoo-dev.cloudfunctions.net/doFoo?name=Flo&secure=hotzenplotz48&action=cleanSharedDocs&coachId=2X1ZM44k8lXX6t2w1X17fx76DPK2

// populate controlTable 
https://europe-west1-doozzoo-dev.cloudfunctions.net/doFoo?name=Flo&secure=hotzenplotz48&action=initControlTable
// get totals from realtime database 
https://europe-west1-doozzoo-dev.cloudfunctions.net/doFoo?name=Flo&secure=hotzenplotz48&action=getMigrationStats


### PROD ###
// migrate a specific userId
https://europe-west1-doozzoo-eve.cloudfunctions.net/doFoo?name=Flo&secure=hotzenplotz48&coachId=2X1ZM44k8lXX6t2w1X17fx76DPK2&action=sync
https://europe-west1-doozzoo-eve.cloudfunctions.net/doFoo?name=Flo&secure=hotzenplotz48&action=sync&coachId=89MR0mIHd2VPlf0wqO62VNO1TBW2

// > 1000 docs
https://europe-west1-doozzoo-eve.cloudfunctions.net/doFoo?name=Flo&secure=hotzenplotz48&action=sync&coachId=9unXmlPXJIb6ZkAHbWhCQYESh0a2

https://europe-west1-doozzoo-eve.cloudfunctions.net/doFoo?name=Flo&secure=hotzenplotz48&action=sync&coachId=9unXmlPXJIb6ZkAHbWhCQYESh0a2&pageSize=3&dryrun=true

https://europe-west1-doozzoo-eve.cloudfunctions.net/doFoo?name=Flo&secure=hotzenplotz48&action=sync&coachId=9unXmlPXJIb6ZkAHbWhCQYESh0a2&pageSize=500&pageToken=Cjxkb2N1bWVudHMvOXVuWG1sUFhKSWI2WmtBSGJXaENRWUVTaDBhMi9QYXJ0eS1VcC1CYi1IUC02MC5tcDM=&dryrun=true



https://europe-west1-doozzoo-eve.cloudfunctions.net/doFoo?name=Flo&secure=hotzenplotz48&action=doMigration&limit=1&step=step1&migrated=false&max=5

// copies all shared documents into the firestore table
https://europe-west1-doozzoo-eve.cloudfunctions.net/doFoo?name=Flo&secure=hotzenplotz48&action=sharedDocuments

// populate controlTable 
https://europe-west1-doozzoo-eve.cloudfunctions.net/doFoo?name=Flo&secure=hotzenplotz48&action=initControlTable

// do Migration 
https://europe-west1-doozzoo-eve.cloudfunctions.net/doFoo?name=Flo&secure=hotzenplotz48&action=doMigration&limit=1&step=step1&migrated=false&max=1

https://europe-west1-doozzoo-eve.cloudfunctions.net/doFoo?name=Flo&secure=hotzenplotz48&action=doMigration&limit=100&step=step1&migrated=false&max=2

https://europe-west1-doozzoo-eve.cloudfunctions.net/doFoo?name=Flo&secure=hotzenplotz48&action=doMigration&limit=1&step=step1&migrated=false&max=2000

*** clean
https://europe-west1-doozzoo-eve.cloudfunctions.net/doFoo?name=Flo&secure=hotzenplotz48&action=reset&limit=10&step=step1&migrated=true&max=10

### cleansharedDocs
https://europe-west1-doozzoo-eve.cloudfunctions.net/doFoo?name=Flo&secure=hotzenplotz48&action=cleanSharedDocs&coachId=auth0|582c7e4cedb8c73048379aad

https://europe-west1-doozzoo-eve.cloudfunctions.net/doFoo?name=Flo&secure=hotzenplotz48&action=cleanSharedDocs&coachId=MHVvoBVOPFUQOAU9k89u5xWPQwI3

// populate controlTable 
https://europe-west1-doozzoo-eve.cloudfunctions.net/doFoo?name=Flo&secure=hotzenplotz48&action=initControlTable
// get totals from realtime database 
https://europe-west1-doozzoo-eve.cloudfunctions.net/doFoo?name=Flo&secure=hotzenplotz48&action=getMigrationStats

*/