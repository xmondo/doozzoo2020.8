/**
 * TODO: ...
 * !!! update times error
 * !!! update admin role error
 */

// indirect ref of admin
const admin = require('./fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();

const userUpdateAgent = async (snap, context) => {
    // snap.before.exists() // brand new data
    // snap.after.exists() // data that was existing before

    /*
    console.log('snap, context: ', snap, context);
    console.log('snap.before', snap.before.data());
    console.log('snap.after', snap.after.data());
    */
    // in case of deletion, after === null
    const after = snap.after.exists ? snap.after.val() : null;
    // Get an object with the previous document value (for update or delete)
    const before = snap.before.exists ? snap.before.val() : null;

    /**
     * cases for create and delete first
     */
    // case create
    if(before === null) {
        // newly created record
        console.log('newly created');
    // case delete
    } else if (after === null) {
        // deleted record
        console.log('just deleted');
        // return deleteUser(before, context);   
    } 

    // update the firestore object representing the initiated user with timestamps
    // this fires only for institutions
    try{
        if (after.trigger) {
            await updateTimes(after, context);
        }
    
        // update field activated
        if (after.activated !== null && after.activated !== undefined) {
            if (after.activated !== before.activated) {
                return updateActivated(after, context);    
            }
        } else {
            // dummy return if nothing fired yet
            return true;
        }
    }
    catch(error) {
        console.log(JSON.stringify(error));
        return error;
    }
    
         
};

async function updateTimes(after, context) {
    // update the firestore object representing the initiated user
    // with timestamps
    try{
        const data = {
            creationTime: after.creationTime,
            lastSignInTime: after.lastSignInTime,
            userId: context.params.userId,
            // status: 'inviteAccepted',
        }
        const matches = await fst
            .collectionGroup('accounts')
            .where('userId','==',context.params.userId)
            .get();

        return matches.forEach(item => {
            // in this case its a newly invited user,
            // set status to "inviteAccepted"
            if(item.data().status === 'inviteSent') {
                // console.log('userUpdateAgent->updateTimes: ', JSON.stringify(item));
                data.status = 'inviteAccepted';
            }
            item.ref.update(data);
        });       
    }
    catch(error) {
        console.log('could not updateTimes error: ', JSON.stringify(error));
        return 'could not updateTimes error: ', JSON.stringify(error);
    }
    
}

async function updateActivated(after, context) {
    // sync activation status from inst db
    return admin.auth()
        .updateUser(context.params.userId, {
            disabled: !after.activated,
        })
        .then((userRecord) => {
            // See the UserRecord reference doc for the contents of userRecord.
            console.log('Successfully updated user', JSON.stringify(userRecord.toJSON()));
            return userRecord.toJSON();
        })
        .catch((error) => {
            console.log('Error updating user:', context.params.userId, JSON.stringify(error));
            return error;
        });
          
// end of   
}

async function updateClaim(snap, context, data) {
    // const _data = data || 
    const _data = {
        admin: true,
        role: 45,
        info: 'helloWorldClaims',
    }
    const response = admin.auth()
        .setCustomUserClaims(uid, _data);
}

// final export
module.exports = userUpdateAgent;

