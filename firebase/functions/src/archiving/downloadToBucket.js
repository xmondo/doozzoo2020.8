/**
 * TODO: ...
 */

const functions = require('firebase-functions');
const admin = require('../fooAdmin');
const db = admin.database();

const archivingUsageBilling = require('../stripe/archvingUsageBilling').archivingUsageBilling;
const uuid4 = require('uuid/v4');
const axios = require('axios');
const { Storage } = require('@google-cloud/storage');
const https = require('https');
const storage = new Storage();
const bucketName = functions.config().bucket.location; 
const bucket = storage.bucket(bucketName);


const downloadToBucket = async function (archiveObj) {

	console.log('archiveObj.url: ', archiveObj.name, archiveObj.url);
	// name: lesson-recording_dd.MM.yyyy_hh:mm.mp4
	const blobName = archiveName(); // archiveObj.id + '.mp4';
	// const blob = bucket.file(archiveObj.name + '/' + blobName);
	const blob = bucket.file('documents/' + archiveObj.name + '/' + blobName);
 	// Generate a new UUID
    const assetUuid = uuid4();

	const stream = blob.createWriteStream({
        metadata: {
            contentType: 'video/mp4',
            metadata: {
                filename: blobName,
                assetCategory: 'lessonArchive',
                id: archiveObj.id,
                ownerId: archiveObj.name,
                updatedAt: archiveObj.updatedAt,
                width: archiveObj.width,
                height: archiveObj.height,
                resolution: archiveObj.resolution,
                firebaseStorageDownloadTokens: uuid4(),
                tags: JSON.stringify(['default','lesson_archive']),
                md5Hash: assetUuid, // legacy shiuld be replaced all along the asset lifecycle
                uuid: assetUuid,
            }
        }
    });

    const response = await axios({
        method: 'get',
        url: archiveObj.url,
        responseType: 'stream',
        headers: {
            'Accept-Encoding': 'gzip'
        }
    })
    .catch(error => console.log('https.get error: ', error));

    stream.on('error', error => {
        console.log(`error occurred`, error);
        putIntoQueue(archiveObj);
        return db.ref('/recordings/' + archiveObj.name + '/' + archiveObj.id)
            .update({ status: 'downloadFromOTError' });

    });
    
    stream.on('finish', () => {
        console.info(`archiving download success`);

        process.nextTick(() => stream.destroy());
        
        // remove item from queue
        // key is indicator, that the request comes from the cronJob
        if(archiveObj.key) {
            db.ref('/recordingsQueue/' + archiveObj.key)
                .remove();
        }

        // finally apply billing
        const duration = Math.floor( archiveObj.duration / 60 );
        // start only for sessions > 1 Minute ??? or more?
        duration > 0 ? archivingUsageBilling(archiveObj.name, duration) : '';

        // change status in helper documents table
        return db.ref('/recordings/' + archiveObj.name + '/' + archiveObj.id)
            .update({ status: 'success' });
    });

    return response.data.pipe(stream);

}

function archiveName() {
	// TODO: read timezone from user account profiles
	const timezone = 0; // timezone || 0;
	const tz = parseInt(timezone);
	const offsetTime = Date.now() + (tz * 60 * 60 * 1000);
	const d = new Date();
	d.setTime(offsetTime);

	let day = d.getDate();
	let month = d.getMonth() + 1;
	const year = d.getFullYear();
	let hour = d.getHours();
	let minute = d.getMinutes();
	day = day.toString().length === 1 ? `0${day}` : day;
	month = month.toString().length === 1 ? `0${month}` : month;
	hour = hour.toString().length === 1 ? `0${hour}` : hour;
	minute = minute.toString().length === 1 ? `0${minute}` : minute;

	// console.log(`lesson-recording_${day}.${month}.${year}_${hour}:${minute}.mp4`);
	return `lesson-recording_${day}.${month}.${year}_${hour}:${minute}.mp4`;
}

function putIntoQueue(archiveObj) {
    console.log('download was queued...')
    db.ref('recordingsQueue').push(archiveObj);
}
    
// final export
module.exports = downloadToBucket;


/*
	return https.get(
		archiveObj.url, 
		(res) => {

            console.log(`status Code: `, res.statusCode);
            res.statusCode !== 200 ? putIntoQueue(archiveObj) : '';

			stream.on('error', error => {
                console.log(`error occurred`, error);
                // return db.ref('/recordings/' + archiveObj.ownerId + '/' + archiveObj.id)
                //    .update({ status: 'downloadFromOTError' });
                putIntoQueue(archiveObj);
			})
			stream.on('finish', () => {
                console.info(`archiving download success`);
                
                // remove item from queue
                // key is indicator, that the request comes from the cronJob
                if(archiveObj.key) {
                    db.ref('/recordingsQueue/' + archiveObj.key)
                        .remove();
                }

                // finally apply billing
                const duration = Math.floor( archiveObj.duration / 60 );
                // start only for sessions > 1 Minute ??? or more?
                duration > 0 ? archivingUsageBilling(archiveObj.name, duration) : '';

                // change status in helper documents table
                return db.ref('/recordings/' + archiveObj.name + '/' + archiveObj.id)
                    .update({ status: 'success' });
                
            })
            .on('error', error => {
                console.log(`https get error occurred: `, error);
                putIntoQueue(archiveObj);
            });

			return res.pipe(stream);          
        });
*/

