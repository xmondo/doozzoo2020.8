/**
 * TODO: ...
 */

const functions = require('firebase-functions');
const admin = require('../fooAdmin');
const db = admin.database();

const archivingUsageBilling = require('../stripe/archvingUsageBilling').archivingUsageBilling;
const uuid4 = require('uuid/v4');
const { Storage } = require('@google-cloud/storage');
const https = require('https');
const storage = new Storage();
const bucketName = functions.config().bucket.location; 
const bucket = storage.bucket(bucketName);

const cronCopyArchives = async (context) => {
    // TODO: name will not work any more here, or?
    /**
     * read out recording table
     * find available items tagged as "available" < -72 from creation date
     * start copy process
     * if copy process successfull -> update recordings table
     * update usage billing
     */

    const recordingsList = await db
        .ref('recordingsQueue')
        .orderByChild('status')
        .equalTo('available')
        .once('value');

    // 
    recordingsPoolArr = [];
    const threshold = Date.now() - (1000 * 60 * 60 * 70) // will be kept for 72 hrs
    
    recordingsList.forEach(val => {
        console.log('*** val: ', val.val().name);
        const counter = val.val().downloadCounter + 1;
        const data ={
            key: val.key,
            id: val.val().id,
            ownerId: val.val().name,
            updatedAt: val.val().updatedAt,
            width: val.val().width,
            height: val.val().height,
            resolution: val.val().resolution,
            url: val.val().url,
        } 
        
        if(val.val().updatedAt > threshold) {
            recordingsPoolArr.push(data);
            // increase counter
            db.ref(`recordingsQueue/${val.key}`)
                .update({downloadCounter: counter});
        } else {
            // mark as failed in recordings table
            // delete from queue
        }
    });

    console.log('recordingsPoolArr: ', JSON.stringify(recordingsPoolArr));

    let i = 0;  
    const count = recordingsPoolArr.length <= 3 ? recordingsPoolArr.length : 3;
    while(i < count) {
        // eslint-disable-next-line no-await-in-loop
        await doCopy(recordingsPoolArr[i]);
        console.log('recordingsPoolArr: ' + i, JSON.stringify(recordingsPoolArr[i]));
        i++;
    }

};

async function doCopy(archiveObj) {
	console.log('archiveObj.url: ', archiveObj.url);
	// name: lesson-recording_dd.MM.yyyy_hh:mm.mp4
	const blobName = archiveName(); // archiveObj.id + '.mp4';
	// const blob = bucket.file(archiveObj.name + '/' + blobName);
	const blob = bucket.file('documents/' + archiveObj.ownerId + '/' + blobName);
 	// Generate a new UUID
	const uuid = uuid4();
	const stream = blob.createWriteStream(
		{
			metadata: {
                contentType: 'video/mp4',
                // simplifiedContentType: 'video',
                // tags: JSON.stringify(['default', 'lesson archive']),
				metadata: {
                    filename: blobName,
                    assetCategory: 'lessonArchive',
                    id: archiveObj.id,
                    ownerId: archiveObj.name,
                    updatedAt: archiveObj.updatedAt,
                    width: archiveObj.width,
                    height: archiveObj.height,
                    resolution: archiveObj.resolution,
                    firebaseStorageDownloadTokens: uuid4(),
                    tags: JSON.stringify(['default','lesson_archive']),
                    md5Hash: assetUuid, // legacy shiuld be replaced all along the asset lifecycle
                    uuid: assetUuid,
                }
			}
		});

	https.get(
		archiveObj.url, 
		async (res) => {

            console.log(`status Code: `, res.statusCode);

			stream.on('error', error => {
                console.log(`error occurred`, error);
                return db.ref('/recordings/' + archiveObj.ownerId + '/' + archiveObj.id)
			        .update({ status: 'downloadFromOTError' });
			})
			stream.on('finish', () => {
                console.info(`archiving download success`);
                // remove item from queue
                db.ref('/recordingsQueue/' + archiveObj.key)
                    .remove();

                // finally apply billing
                const duration = Math.floor( archiveObj.duration / 60 );
                // start only for sessions > 1 Minute ??? or more?
                duration > 0 ? archivingUsageBilling(archiveObj.ownerId, duration) : '';

                // change status in helper documents table
                return db.ref('/recordings/' + archiveObj.ownerId + '/' + archiveObj.id)
                    .update({ status: 'success' });
                
            })
            .on('error', error => {
                console.log(`https get error occurred: `, error);
            });

			return res.pipe(stream);          
        });
}

function archiveName() {
	// TODO: read timezone from user account profiles
	const timezone = 0; // timezone || 0;
	const tz = parseInt(timezone);
	const offsetTime = Date.now() + (tz * 60 * 60 * 1000);
	const d = new Date();
	d.setTime(offsetTime);

	let day = d.getDate();
	let month = d.getMonth() + 1;
	const year = d.getFullYear();
	let hour = d.getHours();
	let minute = d.getMinutes();
	day = day.toString().length === 1 ? `0${day}` : day;
	month = month.toString().length === 1 ? `0${month}` : month;
	hour = hour.toString().length === 1 ? `0${hour}` : hour;
	minute = minute.toString().length === 1 ? `0${minute}` : minute;

	// console.log(`lesson-recording_${day}.${month}.${year}_${hour}:${minute}.mp4`);
	return `lesson-recording_${day}.${month}.${year}_${hour}:${minute}.mp4`;
}
    
// final export
module.exports = cronCopyArchives;

