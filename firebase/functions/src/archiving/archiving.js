/**
 * TODO: ...
 */

// opentok lib
var OpenTok = require('opentok');
// CORS Express middleware to enable CORS Requests.
const cors = require('cors')({origin: true});
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const API_KEY = functions.config().ot.key;
const API_SECRET = functions.config().ot.secret;
const mySecret = 'MeinTollesRumpel!!Stilzchen84';

const archiving = (req, res) => {
    // Grab the get parameters.
	// 'action' defines the archiving function to execute
	const action = req.query.action;
	// 'action' defines the archiving function to execute
	const secret = req.query.secret;
	// define session
	const sessionId = req.query.sessionid;
	// define coach
	const coachId = req.query.coachid;
	// archiveId
	const archiveId = req.query.archiveid;
	// timezone
	const resolution = req.query.resolution; // normal | high
	// ot constants
	//const API_KEY = '45743172';
	//const API_SECRET = '45e387cdcb9858583b33b07bb306c7c01052f50a';

	// in any case send a message back
	let msg;
	let data = {};

	// [START usingMiddleware]
  	// Enable CORS using the `cors` express middleware.
  	// response function
  	let response = data => {
		if(secret !== mySecret) {
			cors(req, res, () => {
				// expects data obj
				return res.status(403)
					.type('text/html')
					.send('<h1>Access Forbidden 403</h1>')
			});
		} else {
			cors(req, res, () => {
				// expects data obj
				return res.status(200)
					.type('application/json')
					.send(data)
			});
		}
  	}

	// init ot
	let opentok = new OpenTok(API_KEY, API_SECRET);
	const offset = 0;
	const count = 50;
	const resol = resolution === 'high' ? '1280x720' : '640x480';
	// console.log('resolution: ', resol);
	const archiveOptions = {
		name: coachId,
		resolution : resol,
		// hasAudio: hasAudio,
		// hasVideo: hasVideo,
		// outputMode: outputMode,
    };
    
	switch(action){
		case 'startarchive':
			// action "startArchive"
			opentok.startArchive(sessionId, archiveOptions, (err, archive) => {
				if (err) {
					data = {
						status: 'error',
						error: err,
					}
					console.log(err);
					response(data);
				} else {
					// The id property is useful to save off into a database
					console.log("new archive:" + archive.id);
					data = {
						status: 'success',
						archive: archive,
						msg: 'archiving started for archive.id: ' + archive.id,
					}
					response(data);
				}
			});
			break;

		case 'stoparchive':
			// action "stopArchive"
			// const archiveId = req.query.archiveid;
			opentok.stopArchive(archiveId, (err, archive) => {
				if (err) {
					data = {
						status: 'error',
						error: err,
					}
					console.log(err);
					response(data);
				} else {
					// The id property is useful to save off into a database
					// console.log("stopped archive:" + archive.id);
					data = {
						status: 'success',
						archive: archive,
						msg: 'archiving stopped for archive.id: ' + archive.id,
					}
					response(data);
				}
			});
			break;

		case 'getarchive':
			// action "getArchive"
			opentok.getArchive(archiveId, (err, archive) => {
				if (err) {
					data = {
						status: 'error',
						error: err,
					}
					console.log(err);
					response(data);
				} else {
					// The id property is useful to save off into a database
					// console.log("stopped archive:" + archive.id);
					data = {
						status: 'success',
						archive: archive,
						msg: 'archive retrieved: ' + archive.id,
					}
					response(data);
				}
			});
			break;

		case 'deletearchive':
			// action "deleteArchive"
			opentok.deleteArchive(archiveId, (err) => {
				if (err) {
					data = {
						status: 'error',
						error: err,
					}
					console.log(err);
					response(data);
				} else {
					// console.log("deleted archive:" + archive.id);
					data = {
						status: 'success',
						msg: 'archive deleted: ' + archiveId,
					}
					response(data);

					console.log('delete: ', coachId, archiveId)

					// delete from FB table
					admin.database()
						.ref('/recordings/' + coachId + '/' + archiveId)
						.set(null);
				}
			});
			break;

		case 'listarchives':
			// action "listArchive"
			// const offset = 0;
			// const count = 50;
			opentok.listArchives({offset:0, count:50}, (err, archives, totalCount) => {
				if (err) {
					data = {
						status: 'error',
						error: err,
					}
					console.log(err);
					response(data);
				} else {
					console.log(totalCount + " archives");
					data = {
						status: 'success',
						archives: archives,
						totalCount: totalCount,
						offset: offset,
						count: count,
						msg: totalCount + ' archives have been retrieved',
					}
					response(data);
				}
			});
			break;

		case 'listarchivescoachid':

			opentok.listArchives({offset:0, count:50}, (err, archives, totalCount) => {
				// look one week back: 1000 * 60 * 60 * 24 * 7
				const lookback = Date.now() - (1000 * 60 * 60 * 24 * 7);
				const filteredArchives = archives.filter(item => item.name === coachId && item.createdAt > lookback);

				if (err) {
					data = {
						status: 'error',
						error: err,
					}
					console.log(err);
					response(data);
				} else {
					console.log(totalCount + " archives");
					data = {
						status: 'success',
						archives: filteredArchives,
						totalCount: totalCount,
						offset: offset,
						count: count,
						msg: totalCount + ' archives have been retrieved',
					}
					response(data);
				}
			});
			break;

		default:
			// {}
			data = {
				status: 'success',
				msg: 'nothing executed',
				action: action,
			}
			response(data);
	}
};
    
// final export
module.exports = archiving;

/* Example
// ### prod ###
https://europe-west1-doozzoo-eve.cloudfunctions.net/archiving

https://europe-west1-doozzoo-eve.cloudfunctions.net/archiving?action=listarchives&secret=MeinTollesRumpel!!Stilzchen84

https://europe-west1-doozzoo-eve.cloudfunctions.net/archiving?action=listarchivescoachid&coachid=auth0|582c7e4cedb8c73048379aad&secret=MeinTollesRumpel!!Stilzchen84

// ### dev ###
https://europe-west1-doozzoo-dev.cloudfunctions.net/archiving?action=listarchives&secret=MeinTollesRumpel!!Stilzchen84

https://europe-west1-doozzoo-dev.cloudfunctions.net/archiving?action=listarchivescoachid&coachid=auth0|582c7e4cedb8c73048379aad&secret=MeinTollesRumpel!!Stilzchen84

https://europe-west1-doozzoo-dev.cloudfunctions.net/archiving?action=getarchive&archiveid=f87d1b82-bb75-4046-9350-97b5a83c3953

archiving?action=startarchive&coachid=auth0|582c7e4cedb8c73048379aad&sessionid=2_MX40NjEwNTE0Mn5-MTU5MzEwMDkwMDE4MH5BbXd1aysra1g3SmNPbUE5TGVTb2I3TDB-fg&resolution=normal

// ### staging ###
// default case

https://europe-west1-project-6816589442602044124.cloudfunctions.net/archiving

https://europe-west1-project-6816589442602044124.cloudfunctions.net/archiving?action=hallowelt
// list archives
https://europe-west1-project-6816589442602044124.cloudfunctions.net/archiving?action=listarchives
https://us-central1-doozzoo-production.cloudfunctions.net/archiving?action=listarchives
// get archive
https://europe-west1-project-6816589442602044124.cloudfunctions.net/archiving?action=getarchive&archiveid=f87d1b82-bb75-4046-9350-97b5a83c3953
// start archive
https://europe-west1-project-6816589442602044124.cloudfunctions.net/archiving?action=startarchive&coachid=mypelz&sessionid=1_MX40NTc0MzE3Mn5-MTUxNDA2MDAwNzg5MX51ZnBhcnFkTTFDN2RHOXVUOFBMWGsveXl-fg

https://europe-west1-project-6816589442602044124.cloudfunctions.net/archiving?action=startarchive&sessionid=1_MX40NTc0MzE3Mn5-MTUxNDAyMjUwMjM4OX55SFZySU81Y3RxN05JeFRrWjlpcVBwa3h-fg

archive-ids: 
2ef1d72c-5241-4efe-8387-ff787b3fc6f4
60bc46fb-2dd1-4d4d-9e07-b1fe0eb7450f
*/




