/**
 * TODO: ...
 */

const functions = require('firebase-functions');
const admin = require('../fooAdmin');
const db = admin.database();

// CORS Express middleware to enable CORS Requests.
const cors = require('cors')({origin: true});
const downloadToBucket = require('./downloadToBucket');

const copyArchive = async (req, res) => {
    // TODO: name will not work any more here, or?
	const coachId = req.body.name;
	const archiveId = req.body.id;
	// calc minutes from sec
	const duration = Math.floor( req.body.duration / 60 );
	
	// ad a counter
	req.body.downloadCounter = 0;
    
    console.log('body.status, duration ', req.body.status, duration);
    const stati = ['started','stopped','success','failed','available'];
    if(stati.includes(req.body.status)) {
		await db.ref('/recordings/' + coachId + '/' + archiveId)
			.update(req.body);
		if(req.body.status === 'available') {
			return downloadToBucket(req.body);
		}
	} else {
		await db.ref('/recordings/' + coachId + '/' + archiveId)
			.set(null);
	}
	
	// in any case send a message back
	let data = {};
	// [START usingMiddleware]
  	// Enable CORS using the `cors` express middleware.
  	// response function
  	let response = data => {
		cors(req, res, () => {
			// expects data obj
			return res.status(200)
		        .type('application/json')
		        .send(data)
		});
  	}
  	data = {
		status: 'success',
	}
	return response(data);
};
   
// final export
module.exports = copyArchive;

/*
// ### dev ###
https://us-central1-doozzoo-dev.cloudfunctions.net/otMonitor


	if(state === false){
		pUpdates['/classrooms/' + coachId + '/participants/' + key] = null;
	} 
});

admin.database()
	.ref('/recordings/' + event.params.participantId)
	.set({ token: token, timestamp: admin.database.ServerValue.TIMESTAMP })

// now batch update = delete these items
//console.log('keysArr: ', JSON.stringify(keysArr));
//console.log('pUpdates: ', JSON.stringify(pUpdates));
admin.database().ref().update(pUpdates);

req.body:  { id: '3ce236fb-1500-46ff-a07f-57c2acaea479',
  status: 'available',
  name: 'auth0|582c7e4cedb8c73048379aad',
  reason: 'user initiated',
  sessionId: '2_MX40NTc0MzE3Mn5-MTUxNDM5Njk5Nzk5Nn5uanA3R3Y3NTBNVkR0eFQ1bjc0UmhTQlB-fg',
  projectId: 45743172,
  createdAt: 1514397076000,
  size: 3040979,
  duration: 54,
  outputMode: 'composed',
  hasAudio: true,
  hasVideo: true,
  certificate: '',
  sha256sum: 'zHo69dP2mOdGH8KP3aFDVqP2NkoQxxOYVn6UfUHFTn8=',
  password: '',
  updatedAt: 1514397133899,
  url: 'https://s3.amazonaws.com/tokbox.com.archive2/45743172/3ce236fb-1500-46ff-a07f-57c2acaea479/archive.mp4?AWSAccessKeyId=AKIAI6LQCPIXYVWCQV6Q&Expires=1514397734&Signature=kmkHcOF6mqttOsJ9KL7BGzbYoKQ%3D',
  event: 'archive',
  partnerId: 45743172 }

  */ 




