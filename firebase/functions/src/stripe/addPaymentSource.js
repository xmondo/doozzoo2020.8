/**
 * TODO: ...
 */

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
// const fst = admin.firestore();
const db = admin.database();

const stripe = require('stripe')(functions.config().stripe.token);

const updateStripeCustomerData = require('./updateStripeCustomerData');
const stripeLog = require('./stripeLog');
const { createTestAccount } = require('nodemailer');

const addPaymentSource = async (snap, context) => {
	// database.ref('/stripe_customers/{userId}/tokens/{pushId}')

    // TODO: remove after debugging
    // console.log('stripe.token: ', functions.config().stripe.token);

    const _source = snap.val();
    console.log('_source', JSON.stringify(_source));
    let token = null;
    if (_source === null){
        return null;
    } else {
        token = _source.id;
    }

    try {
        // const snapshot = await admin.firestore().collection('stripe_customers').doc(context.params.userId).get();
        const snapshot = await admin.database()
            .ref('stripe_customers/' + context.params.userId + '/customerData/id')
            .once('value');
        // stripe customerId
        const customerId =  snapshot.val();
        console.log('customer, token: ', customerId, token);

        // for IBAN
        let response;
        if(_source.type === 'bank_account') {

            const customer = await stripe
                .customers
                .retrieve(
                    customerId
                );
            console.log('stripe customer data: ', _source.bank_account.account_holder_name, customer.email );

            // create a stripe 
            const source = await stripe
                .sources
                .create({
                    type: 'sepa_debit',
                    currency: 'eur',
                    owner: {
                        // name: customer.name,
                        name: _source.bank_account.account_holder_name,
                        email: customer.email,
                    },
                    usage: 'reusable',
                    token: token,
                    mandate: {
                        notification_method: 'email'
                    }
                }); 
            console.log('source: ', JSON.stringify(source));    
            
            response = await stripe.customers.createSource(customerId, { source: source.id });
            // response = await stripe.customers.createSource(customerId, { source: token });

        
        // for cards
        } else {
            response = await stripe.customers.createSource(customerId, { source: token });
        }

        console.log('createSource response: ', JSON.stringify(response)); 

        await admin.database()
            .ref('stripe_customers/' + context.params.userId + '/sources/' + response.fingerprint)
            .set({merge: true});

        // TODO: update default Source to latest valid source
        await stripe.customers.update(
            customerId,
            { default_source: response.id }
        );
        
        // TODO: update FB stripe object here
        await updateStripeCustomerData(customerId, context.params.userId);

        return stripeLog(context.params.userId, 'success', 'new stripe source created'); // userId, msgType, msg
        // return await stripeLog(context.params.userId, 'success', 'new stripe source created'); // userId, msgType, msg
        // return null;

    } catch (error) {
        // await snap.ref.set({'error':userFacingMessage(error)},{merge:true});
        console.log('error: ', JSON.stringify(error));
        await stripeLog(context.params.userId, 'error', error.message); // userId, msgType, msg
        return null;
        // return reportError(error, {user: context.params.userId});
    }

// end of main class
}
// final export
module.exports = addPaymentSource;

