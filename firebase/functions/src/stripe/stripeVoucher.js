// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
// initialize stripe
const stripe = require('stripe')(functions.config().stripe.token);
// CORS Express middleware to enable CORS Requests.
const cors = require('cors')({origin: true});

const stripeVoucher = async (req, res) => {
    const token = req.query.token || 'no token provided';
    console.log('token: ', token);
    const couponList = await stripe.coupons.list({ limit: 100 });
    // res.send(`couponlist: <pre>${JSON.stringify(couponList)}</pre>`);

    // Enable CORS using the `cors` express middleware.
    const response = data => {
		cors(req, res, () => {
			// expects data obj
			return res.status(200)
		        .type('application/json')
		        .send(data)
		});
  	}
  	data = {
        status: 'success',
        couponList: couponList,  
	}
    response(data);
};

const stripeVoucherValidate = async (req, res) => {
    const coupon = req.query.coupon || 'no coupon provided';
    console.log('coupon: ', coupon, 'stripe account: ', functions.config().stripe.token);
    const isCouponValid = await stripe.coupons
        .retrieve(coupon)
        .catch(error => error);

    // Enable CORS using the `cors` express middleware.
    const response = data => {
		cors(req, res, () => {
			// expects data obj
			return res.status(200)
		        .type('application/json')
		        .send(data)
		});
      }
  	data = {
        isCouponValid: isCouponValid, 
    }
    response(data);
};

// final export
module.exports = {
    stripeVoucher,
    stripeVoucherValidate
}