/**
 * TODO: ...
 * by using weebhooks, customer data must be synced triggered by the following actions:
 * 1. customer.subscription.created
 * 2. customer.subscription.updated
 * ? 3. customer.source.updated
 * ? 4. customer.source.created
 * 5. customer.created
 * 6. customer.updated
 * 
 * For analytics:
 * - report userId, stripe customer id, subscription quantity FB, subscription end FB, subscription quantity SP, subscription end SP (active)
 */

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

/*
// Set your secret key. Remember to switch to your live secret key in production!
// See your keys here: https://dashboard.stripe.com/account/apikeys
// const stripe = require('stripe')('sk_test_g30DPHz37Wx6EuH9y0YE1Dam');
const stripe = require('stripe')(functions.config().stripe.token);

// If you are testing your webhook locally with the Stripe CLI you
// can find the endpoint's secret by running `stripe listen`
// Otherwise, find your endpoint's secret in your webhook settings in the Developer Dashboard
const endpointSecret = 'whsec_Ee9SZ35y7BnUj5ZlLgqtO27DmOWQb9sT';
*/
const updateStripeCustomerData = require('./updateStripeCustomerData')
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const stripe = require('stripe')(functions.config().stripe.token);
// If you are testing your webhook locally with the Stripe CLI you
// can find the endpoint's secret by running `stripe listen`
// Otherwise, find your endpoint's secret in your webhook settings in the Developer Dashboard

// const endpointSecret = 'whsec_Ee9SZ35y7BnUj5ZlLgqtO27DmOWQb9sT'; // DEV
// const endpointSecret = 'whsec_Dn6wMCndCiusEmAx9GRgifluwF6375Y6'; // Prod
const endpointSecret = functions.config().stripe.webhooksecret;

const stripeHook = express();

// Automatically allow cross-origin requests
stripeHook.use(cors({ origin: true }));
// stripeHook.use(bodyParser.json())
stripeHook.use(bodyParser.json({ type: 'application/json' }));

// Add middleware to authenticate requests
// app.use(myMiddleware);

// stripeHook.post('/', (req, res) => res.send(Widgets.create()));
// stripeHook.post('/', bodyParser.raw({type: 'application/json'}), (req, res) => {
stripeHook.post('/', async (req, res) => {
  const sig = req.headers['stripe-signature'];
  // console.log('stripe-signature: ', sig);

  let event;
  try {
    event = req.body; // JSON.parse(req.body);
    // event = stripe.webhooks.constructEvent(req.body, sig, endpointSecret);
  }
  catch (err) {
    // -> 400 Bad Request
    res.status(400).send(`Webhook Error: ${err.message}`);
  }

  // console.log('event: ', event.type); //, JSON.stringify(event));
  // console.log('req.body: ', JSON.stringify(req.body));

  // for the subscription events the customerId is in a different data attribute
  const stripeCustomerId = event.type.indexOf('subscription') !== -1 ? event.data.object.customer : event.data.object.id;

  // const user = await admin.auth().getUserByEmail(event.data.object.email)
  const snap = await db
    .ref('stripe_customers')
    .orderByChild('customerData/id')
    .equalTo(stripeCustomerId)
    .once("value")
  const stripeuserMatches = [];
  snap.forEach(childSnap => stripeuserMatches.push(childSnap.key));  //.val()));
  // console.log('stripeuserMatches: ', JSON.stringify(stripeuserMatches));

  // Handle the event

  switch (event.type) {
    case 'customer.created':
    case 'customer.updated':
    case 'customer.subscription.created':
    case 'customer.subscription.updated':
      // console.log('update customer', stripeCustomerId, stripeuserMatches[0]);
      await updateStripeCustomerData(stripeCustomerId, stripeuserMatches[0]);
      break;

    case 'customer.deleted':
    case 'customer.subscription.deleted':
      // await setCustomerDataDelete(stripeCustomerId, stripeuserMatches[0]);
      break;

    case 'payment_method.attached':
      const paymentMethod = event.data.object;
      console.log('PaymentMethod was attached to a Customer!');
      break;
      
    // ... handle other event types
    default:
      console.log(`Unhandled event type ${event.type}`);
  }

  // reply something
  res.json({received: true});
});

// Expose Express API as a single Cloud Function:
// exports.stripeHook = functions.https.onRequest(app);
module.exports = stripeHook;


