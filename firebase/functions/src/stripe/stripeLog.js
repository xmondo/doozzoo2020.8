/**
 * TODO: ...
 */

// indirect ref of admin
const admin = require('../fooAdmin');

const stripeLog = async (userId, msgType, msg) => {
	console.log('stripeLog: ', JSON.stringify(userId, msgType, msg));
	return admin.database()
		.ref('stripe_customers/' + userId + '/log')
		.push({
			msgType: msgType, // success | error
			msg: msg, // content
			timestamp: admin.database.ServerValue.TIMESTAMP, // timestamp
		});	
}
    
// final export
module.exports = stripeLog;

