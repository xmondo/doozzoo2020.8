/**
 * TODO: ...
 */

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const rdb = admin.database();

const stripe = require('stripe')(functions.config().stripe.token);

const getCustomerUpdateFst = async (userId, customerId) => {
    
    const customer = await stripe
        .customers.retrieve(customerId)
        .catch(e => console.log('customers.retrieve error: ', e));

    await fst
        .collection('stripe')
        .doc(userId)
        .set({ 'stripe_customer': customer }, { merge: true }); // merge makes sure, that existing data will not be overwritten

    // we should update the RDB with the latest customer data as well
    await rdb
        .ref('stripe_customers/' + userId)
        .update({
            customer_id: customerId, // legacy
            customerData: customer,
        });	

    return customer;
// end of main class
}
    
// final export
module.exports = getCustomerUpdateFst;

