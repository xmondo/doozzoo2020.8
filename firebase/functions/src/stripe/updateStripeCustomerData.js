/**
 * TODO: ...
 * cancel_at_period_end: boolean --> continues or is to be canceled
 * current_period_end -> end of curent period
 * status -> incomplete, incomplete_expired, trialing, active, past_due, canceled, or unpaid
 * 
 * items -> data -> [ id, object: subscription_item, ... quantity: n, price -> id: <priceId> ]
 * 
 * 1. get subscription items of customer
 * 2. search for specific priceId and retrieve the regarding quantity
 * DEV -> price_1H08qOBJ4tJWi6xWFg4d59Tw // doozzoo PRO
 * PROD -> price_1HYp26BJ4tJWi6xWP1CCnlEB // doozzoo PRO
 */

 // indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();
const _ = require('lodash');

const stripe = require('stripe')(functions.config().stripe.token);

const updateStripeCustomerData = async (stripeCustomerId, userId) => {
	console.log('updateStripeCustomerData->input', stripeCustomerId, userId);
	let customerData = await stripe.customers
		.retrieve(stripeCustomerId);
	// console.log('customerData subscriptions: ', JSON.stringify(customerData.subscriptions.data[0].items.data));
	// console.log('customerData subscriptions: ', JSON.stringify(customerData.subscriptions));

	let updateData = {};

	if(customerData.subscriptions.total_count === 0) {
		updateData = {
			customer_id: stripeCustomerId, // legacy
			customerData: customerData, // here we have data in any case
			status: 'no_subscription_found',
			updatedOn: admin.database.ServerValue.TIMESTAMP, // ,
		}
		return doUpdate(userId, updateData);
	} else if (customerData.subscriptions.total_count > 0) {
		const subscription = customerData.subscriptions.data[0]; // here we have data in any case
		const subscriptionItems = _.isEmpty(customerData.subscriptions.data[0].items.data) ? null : customerData.subscriptions.data[0].items.data; // here we have the subscription data
		const priceId = functions.config().stripe.priceid;
		const quantity = subscriptionItems ? subscriptionItems.find(item => item.price.id === priceId).quantity : 0;
		updateData = {
			customer_id: stripeCustomerId, // legacy, here we have data in any case
			customerData: customerData, // here we have data in any case
			cancel_at_period_end: subscription.cancel_at_period_end || null,
			current_period_end: subscription.current_period_end || null,
			status: subscription.status || null,
			// console.log('***priceId: ', priceId)
			quantity: quantity,
		}
		return doUpdate(userId, updateData);
	} else {
		return 'updateStripeCustomerData->no update done';
	}
	
};

async function doUpdate(userId, data) {
	console.log('updateStripeCustomerData->userId, quantity, customerData: ', userId, JSON.stringify(data));
	return db
		.ref('stripe_customers/' + userId)
		.update(data)
		.catch(error => {
			console.log('updateStripeCustomerData->db error: ', JSON.stringify(error));
		});
}

// final export
module.exports = updateStripeCustomerData;


/*

//....
	try {
		// write to firestore
		const docRef = fst.collection('stripe').doc(userId);
		const fsSnap = docRef.get();
		// console.log('fsSnap -> ', fsSnap.exists)
		return fsSnap.exists ? docRef.update({ 
			'stripe_customer': customerData, 
			quantity: quantity,
			cancel_at_period_end: cancel_at_period_end,
			current_period_end: current_period_end,
			status: status 
		}, { merge: true }) : docRef.set({ 
			'stripe_customer': customerData, 
			quantity: quantity,
			cancel_at_period_end: cancel_at_period_end,
			current_period_end: current_period_end,
			status: status
		});

	} catch(error) {
		console.log('updateStripeCustomerData fb error: ', JSON.stringify(error));
		return error;
	}
	
*/

