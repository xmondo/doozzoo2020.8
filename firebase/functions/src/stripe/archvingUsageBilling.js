/**
 * TODO: ...
 */

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

const stripe = require('stripe')(functions.config().stripe.token);

const getCustomerUpdateFst = require('./getcustomerUpdateFst');

const archivingUsageBilling = async (userId, duration) => {
    // get current subscription
    
    // const subId = 'si_HSX3dVl5fzqxXN'; //data[0].id;
    // const subId = await getSetStripeSubscription(userId);
    const subscriptionItemId = await getCustomerSetSubscription(userId);
    console.log('subscriptionItemId', subscriptionItemId);

    const stripeTs = Math.floor(Date.now() / 1000);
    const usageRecord = await stripe.subscriptionItems
        .createUsageRecord(
            subscriptionItemId,
            {
                quantity: duration, 
                timestamp: stripeTs,
            }
        )
        .catch(error => console.log('subscriptionItems.createUsageRecord error: ', error));
    
    // retrieve summary and persist in firestore
    const listUsageRecordSummaries = await getUsageRecordSummaries(subscriptionItemId);
    await fst
        .collection('stripe')
        .doc(userId)
        .set({ 'meteredArchiving': listUsageRecordSummaries.data[0] }, { merge: true }); // merge makes sure, that existing data will not be overwritten

    return usageRecord;
    // end of main class
}

const getUsageRecordSummaries = async (subscriptionItem) => {
    
    const listUsageRecordSummaries = await stripe
        .subscriptionItems.listUsageRecordSummaries(subscriptionItem, { limit: 1 })
        .catch(error => console.log('stripe usageRecordSummaries, error: ', error));
    // write summary into FB for reference
    // ...
    //console.log('listUsageRecordSummaries: ', listUsageRecordSummaries);
    
    return listUsageRecordSummaries;

    // end of function
}

async function getCustomerSetSubscription(userId) {
    /**
     * get customer id
     * check for subscription items
     * if defined price id not found -> 
     * add price to actual subscription
     * return subscription item id, so that metered usage record could be created
     * ...
     */
    // doozzoo Now testmode
    // const meteredArchivingPriceId = 'price_1H07fOBJ4tJWi6xWCRo2xKRs';
    const meteredArchivingPriceId = functions.config().stripe.archivingpriceid;
    // console.log('getCustomerSetSubscription: ', meteredArchivingPriceId);

    const snap = await db
        .ref('/stripe_customers/' + userId + '/customerData/id')
        .once('value');
    const customerId = snap.val();
/*
    const customer = await stripe
        .customers.retrieve(customerId)
        .catch(e => console.log('customers.retrieve error: ', e));
*/
    const customer = await getCustomerUpdateFst(userId, customerId);
    // console.log('getCustomerSetSubscription->subscription: ', JSON.stringify(customer));
    // assumption: only one subscription!!
    const subscription = customer.subscriptions.data[0];
    // should be an array of assigned products/prices
    const items = subscription.items.data;
    // console.log('getCustomerSetSubscription->items: ', JSON.stringify(items));

    // check whether subscription item connected with price id is already available
    const ArchivingPriceIdAvailableIndex = items.findIndex(item => {
        return item.price.id === meteredArchivingPriceId;
    });

    // not availabe yet -> create it
    if(ArchivingPriceIdAvailableIndex === -1) {

        const subscriptionItem = await stripe.subscriptionItems.create(
            {
              subscription: subscription.id,
              price: meteredArchivingPriceId,
            })
            .catch(e => console.log('subscriptions.create error: ', e));
        return subscriptionItem.id;
    // otherwise simply return
    } else {
        const subscriptionItem = items[ArchivingPriceIdAvailableIndex];
        return subscriptionItem.id;
    }
}
    
// final export
module.exports = {
    archivingUsageBilling,
    getUsageRecordSummaries
};

