/**
 * TODO: ...
 * cancel_at_period_end: boolean --> continues or is to be canceled
 * current_period_end -> end of curent period
 * status -> incomplete, incomplete_expired, trialing, active, past_due, canceled, or unpaid
 * 
 * items -> data -> [ id, object: subscription_item, ... quantity: n, price -> id: <priceId> ]
 * 
 * 1. get subscription items of customer
 * 2. search for specific priceId and retrieve the regarding quantity
 * DEV -> price_1H08qOBJ4tJWi6xWFg4d59Tw // doozzoo PRO
 * PROD -> price_1HYp26BJ4tJWi6xWP1CCnlEB // doozzoo PRO
 */

 // indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

const stripe = require('stripe')(functions.config().stripe.token);
 
const createCoachStripeCustomer = async (snap, context) => {
    // onWrite(async (snap, context)
    // TODO: remove after debugging
    console.log('users/plan: ', JSON.stringify(snap.after.val()));

    const existingStripeCustomerId = await db
        .ref('stripe_customers/' + context.params.userId + '/customerData/id')
        .once('value');
    // console.log('existingStripeAccount: ', existingStripeCustomerId.val());
    // triggered from coach profile form
    // only if no account exists already
    if (['pro','premium'].includes(snap.after.val()) && existingStripeCustomerId.val() === null ) {
        // for security reasons we fetch email only from firebase auth data
        const user = await admin.auth().getUser(context.params.userId);
        // console.log('user', user);

        const address = await db
            .ref('profiles/' + context.params.userId + '/privateData')
            .once('value');
        const ad = address.val();

        const customer = await stripe.customers.create({
            email: user.email,
            shipping: {
                name: ad.firstname + ' ' + ad.lastname,
                address: {
                    city: ad.city,
                    country: ad.country,
                    line1: ad.address_street1,
                    line2: ad.address_street2,
                    postal_code: ad.zipcode
                    // state: 
                }
            }
        });

        // for firestore integration
        // return admin.firestore().collection('stripe_customers').doc(user.uid).set({customer_id: customer.id});
        return db
            .ref('stripe_customers/' + context.params.userId)
            .update({
                customer_id: customer.id,
                customerData: customer,
            });

    } else {
        // finalize cloud function
        return null;
    }
};
    
// final export
module.exports = createCoachStripeCustomer;

