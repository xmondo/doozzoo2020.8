
// indirect ref of admin
const admin = require('./fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

const enrichAccountNameInstitution = async function(snap, context) {

    // console.log('snap, context.params: ', snap, context.params);
    
    const userId = context.params.userId;

    const firstname = snap.after.val().firstname;
    const lastname = snap.after.val().lastname;  

    const data = { name: firstname + ' ' + lastname }

    try{
        const matches = await fst
        .collectionGroup('accounts')
        .where('userId','==',userId)
        .get();

        return matches.forEach(item => {
            item.ref.update(data);
        });
    }
    catch(error){
        return 'enrichName failed...', error;
    }
         
// end of main class
}   
// final export
module.exports = enrichAccountNameInstitution;

