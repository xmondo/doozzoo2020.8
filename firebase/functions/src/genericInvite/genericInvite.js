/**
    - read data
    - create user incl. verifying his email
    - connect user with institution
    - parse email with invite link
 */

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

// local imports
const createUserSendEmailInstitution = require('./createUserSendEmailInstitution');

let inviteId;

const genericInvite = async (snap, context) => {

    /*
    data: {"accountId":"unset","createdOn":1621682430278,"initiatorId":"ySJW2iD0poxcctlwm0lq","institutionId":"ySJW2iD0poxcctlwm0lq","role":"0","target":{"password":"delta-papa-sierra","type":"email","value":"customer4@mypelz.de"},"triggerId":"E8Zcla5lwyRC7zHPW7Nr","type":"institution"}
    */ 
    const data = snap.data();
    inviteId = context.params.inviteId;
    // console.log(JSON.stringify(snap), JSON.stringify(context));
    console.log('genericInvite: ', JSON.stringify(data));

    if (data.target.type === 'email') {
        if (data.type === 'institution' || data.type === 'institutionCoach' || data.type === 'institutionAdmin') {
            return createUserSendEmailInstitution(data, context);
        } else { // if (data.type === 'coach') {
            // ...  
            return createUserSendEmailCoach(data, context);    
        }         
    } else {
        return 'no target type defined....';
    }

};

async function createUserSendEmailCoach(data) {
    // ...
}

// final export
module.exports = genericInvite;


