
const nodemailer = require('nodemailer');

const sendCustomNotificationEmail = async function sendCustomNotificationEmail(userEmail, displayName, subject, text) {
    const from = 'support@doozzoo.com';
    const options = {
        host: 'wp12975660.mailout.server-he.de',
        port: 587, // 587 | 25
        secure: false, // false, // upgrade later with STARTTLS
        auth: {
            user: 'wp12975660-support',
            pass: 'Do50-2=48**'
        }
    };
    const transporter = nodemailer.createTransport(options);
    // verify connection configuration					
    transporter.verify((error, success) => {
        if (error) {
            console.log('sendInviteMail->error:', error);
        } else {
            console.log('sendInviteMail->success:', 'Server is ready to take our messages');
        }
    });
    const sendIt = (userEmail, from, to) => {
        var mailOptions = {
            from: from,
            to: userEmail,
            subject: subject,
            text: text
        }
        transporter.sendMail(mailOptions)
            .then(() => {
                console.log('sendInviteMail->Notification email sent to:', to);
                return true;
            })
            .catch(error => {
                // TODO: email send error...
                console.log(error)
            });
    }
    return sendIt(userEmail, from, displayName);
}

module.exports = sendCustomNotificationEmail;
