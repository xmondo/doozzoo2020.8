
// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

const connectCoachWithExistingStudent = require('./connectCoachWithExistingStudent');
const connectExistingCoachWithInstitution = require('./connectExistingCoachWithInstitution');
const sendCustomNotificationEmail = require('./sendCustomNotificationEmail');
const addPeerConnection = require('../peers/addPeerConnection');

const createUserSendEmailInstitution = async function createUserSendEmailInstitution(data, context) {
    // container for dispatching create user error
    let createUserError;
    const inviteId = context.params.inviteId;

    // console.log('data: ', JSON.stringify(data));

    // lookup institution
    const institution = await fst
        .collection('institutions')
        .doc(data.institutionId)
        .get();

    console.log('createUserSendEmailInstitution->institution: ', JSON.stringify(institution.data()));

    const institutionName = await institution.data().name;
    const displayName = 'invite from - ' + institutionName
    // const institutionLang = await institution.data().language;

    // create user
    const user = await admin
        .auth()
        .createUser({
            email: data.target.value,
            emailVerified: true,
            // phoneNumber: '+11234567890',
            password: data.target.password,
            displayName: displayName,
            // photoURL: 'http://www.example.com/12345678/photo.png',
            disabled: false,
        })
        .catch(error => {
            console.log('createUserSendEmailInstitution->createUser: ', error);
            createUserError = error;
        });

    console.log('genericInvite->createUserSendEmail: ', JSON.stringify(user), institutionName);

    // in case of existing email/account
    if(user !== null && user !== undefined) {
        /**
         * if user is role 0
         * initiatorId must be a coach, either from intitution or from self contained coach
         */

        // store user in RDB users
        const userData = {
            role: parseInt(data.role), // make sure role is written as number
            userActions: 'createdByInstitution',
            trigger: {
                type: data.type, // institution | coach | self
                initiatorId: data.initiatorId, // in this case instituionId or coachId
                institutionId: data.institutionId, // makes things easier
                triggerId: data.triggerId,
            }
        }

        const nick = data.target.value.split('@')[0];
        const contactData = {
            avatar: 'assets/img/avatars/avataaars-default.png',
            dataApproval: true,
            email: data.target.value,
            nickname: nick,
        }
        const privateData = {
            firstname: nick,
            lastname: '',
        }
        
        // Write the new post's data simultaneously in the posts list and the user's post list.
        var updates = {};
        updates['users/' + user.uid] = userData;
        updates['profiles/' + user.uid + '/contactData'] = contactData;
        updates['profiles/' + user.uid + '/privateData'] = privateData; 
        
        // if institution coach, assign to coach customer
        // if initiatorId = institutionId, do not create any connections,
        // this will create an orphaned student entry that must be connected from institution overview
        // beware of type issues with role!!
        if (parseInt(data.role) === 0 && data.initiatorId !== data.institutionId) {  // = student invited by a existing coach
            // TODO: double action due to shift to userPeers???

            updates['/coachCustomer/' + data.initiatorId + '/' + user.uid] = true; 
            
            // add to userPeers table
            await addPeerConnection(data.initiatorId, user.uid, data.institutionId);

            console.log('---###---> user connected', data.institutionId, data.initiatorId, user.uid);

        } else {
            console.log('********', data.role, typeof data.role, data.initiatorId, data.institutionId);
        }

        // combined updates
        await db.ref().update(updates);

        // 
        // const link = `https://${functions.config().email.domain}/login?mode=login&u=${data.target.value}&p=${data.target.password}`;
        const link = `https://${functions.config().email.domain}/`;

        const subject = `${institutionName} - Video-Unterricht / Online classes Information`;
        const emailTxt = `
Willkommen zum Videounterricht bei ${institutionName}.

Bitte wie folgt anmelden:
1. Auf folgenden Link klicken und der Browser öffnet sich:
https://doozzoo.com

2. Mit folgenden Daten anmelden. 
Benutzername : ${data.target.value}
Passwort: ${data.target.password}

Danach bitte ein neues Passwort vergeben über „Passwort vergessen"!

Liebe Grüße,
Euer Team von ${institutionName} & doozzoo

---------------------------------------------------------------------
Welcome to your online classes at ${institutionName} and doozzoo,

How to start:
1. Click the following link and your webbrowser will open.
${link}

2. Login with the following credentials:
Username: ${data.target.value}
Password: ${data.target.password}

After login: use the "password forgotten" function, to create a new password.

Thank you,
Your team of ${institutionName} & doozzoo
`;

        // update firestore with update send
        // ####################
        await fst
            .collection('institutions')
            .doc(data.institutionId)
            .collection('accounts')
            .doc(data.triggerId)
            .update({
                status: 'inviteSent',
                userId: user.uid, // enhancement: we directly sync the created userId into the inst. db!!
                password: data.target.password,
            })

        await removeInvite(inviteId);
            
        return sendCustomNotificationEmail(data.target.value, displayName, subject, emailTxt);

    // ### in case existing user was found ###
    } else {
        // we must make sure that the existing user has the same role as the requested one
        // since we do not have the institution role model realized
        const existingUser = await admin.auth()
            .getUserByEmail(data.target.value);
        const existingUserRole = await db
            .ref(`users/${existingUser.uid}/role`)
            .once('value');  
            
        // there is a user but the role was not defined -> not a valid user account
        if(existingUserRole.val() === null || existingUserRole.val() === undefined ) {

            console.log('genericInvite->createUserSendEmail: user role is NULL', );
            const errorMessage = 'emailAlreadyExistsNoUserRoleError'; 

            await fst
                .collection('institutions')
                .doc(data.institutionId)
                .collection('accounts')
                .doc(data.triggerId)
                .update({
                    status: 'error',
                    errorMessage: errorMessage,
                    // userId: existingUser.uid, // enhancement: we directly sync the created userId into the inst. db!!
                });
            
            await removeInvite(inviteId);
                
            const response = 'new user could not be created: user role is NULL';
            return response;

        // if requested user should be a coach but the existing is a student
        } else if(existingUserRole.val().toString() === '0' && data.role.toString() === '1') {
            console.log('genericInvite->createUserSendEmail: user / roles: ', JSON.stringify(existingUser), existingUserRole.val(), data.role);
            const errorMessage = 'emailAlreadyExistsConflictingRolesError'; 

            await fst
                .collection('institutions')
                .doc(data.institutionId)
                .collection('accounts')
                .doc(data.triggerId)
                .update({
                    status: 'error',
                    errorMessage: errorMessage,
                    // userId: existingUser.uid, // enhancement: we directly sync the created userId into the inst. db!!
                });
            
            await removeInvite(inviteId);
                
            const response = 'new user could not be created: conflicting roles';
            return response;

        // if requested user should be a student and the existing is a student, connect
        } else if(existingUserRole.val().toString() === '0' && data.role.toString() === '0') {  // = student, !! numbers as strings  

            console.log('createUserSendEmail->connectCoachWithExistingStudent');
            return connectCoachWithExistingStudent(data, institutionName, inviteId); 
        
        // no invite of existing coaches possible any more
        /*
        } else if(createUserError.errorInfo.code === 'auth/email-already-exists' && data.role === '1') {  // = coach  !! numbers as strings

            console.log('createUserSendEmail->connectExistingCoachWithInstitution');
            return connectExistingCoachWithInstitution(data, institutionName, inviteId); // 
        */
        } else {
            // user is coach so invite not allowed
            const errorMessage = 'emailAlreadyExistsWithRoleCoachError';
            // update firestore with update send
            await fst
                .collection('institutions')
                .doc(data.institutionId)
                .collection('accounts')
                .doc(data.triggerId)
                .update({
                    status: 'error',
                    errorMessage: errorMessage,
                    // userId: existingUser.uid, // enhancement: we directly sync the created userId into the inst. db!!
                });
                
            await removeInvite(inviteId);
                
            const response = 'new user could not be created';
            return response;

        }

    }
}

async function removeInvite(inviteId) {
    // global invite Id
    console.log('invite removed: ', inviteId);
    return fst
        .collection('institutions')
        .doc(inviteId)
        .delete()
        .catch(error => console.log('removeInvite->error: ', error))
}

// final export
module.exports = createUserSendEmailInstitution;