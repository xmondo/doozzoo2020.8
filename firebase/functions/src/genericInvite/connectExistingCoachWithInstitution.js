
// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();

const sendCustomNotificationEmail = require('./sendCustomNotificationEmail');
const addPeerConnection = require('../peers/addPeerConnection');

const connectExistingCoachWithInstitution = async function connectExistingCoachWithInstitution(data, institutionName, inviteId) {
    
    const user = await admin.auth()
        .getUserByEmail(data.target.value);

    /**
     * if user is role 0
     * initiatorId must be a coach, either from intitution or from self contained coach
     */

     if(user){
        // TODO: no, not needed!!!
        // store user in RDB users

        const link = `https://${functions.config().email.domain}/`;

        const subject = `${institutionName} - Video-Unterricht / Online classes Information`;
        const emailTxt = `
Hallo,
du bist als Lehrender bei ${institutionName} eingeladen worden.

1. Klicke auf den folgenden Link und der Browser öffnet sich:
${link}
2. Bitte melde dich mit ${data.target.value} als Nutzername und dem bekannten Passwort an.

Solltest du dein Passwort vergessen haben, nutze die “Passwort vergessen”-Funktion.

Liebe Grüße,
Dein Team von ${institutionName} & doozzoo

---------------------------------------------------------------------
Hello,
you have been connected by ${institutionName} as a new coach.

1. Click the following link and your webbrowser will open:  
${link}
2. Login with username: ${data.target.value} and your well known password.

In case you forgot your password, use the "password forgotten" function, to create a new one.

Thank you,
Your team of ${institutionName} & doozzoo
        `;

        await fst
            .collection('institutions')
            .doc(data.institutionId)
            .collection('accounts')
            .doc(data.triggerId)
            .update({
                status: 'coachAssignedToInstitution',
                userId: user.uid, // enhancement: we directly sync the created userId into the inst. db!!
            }) 
            
        await removeInvite(inviteId);
        return sendCustomNotificationEmail(data.target.value, user.displayName, subject, emailTxt);

    } else {
        const response = 'coachAssignedToInstitutionError';
        console.log('genericInvite->connectExistingCoachWithInstitution: ', response);

        // update firestore with update send
        // ####################
        await fst
            .collection('institutions')
            .doc(data.institutionId)
            .collection('accounts')
            .doc(data.triggerId)
            .update({
                status: 'error',
                errorMessage: response,
            });

        await removeInvite(inviteId);

        return response;

    }                             
   
}

async function removeInvite(inviteId) {
    // global invite Id
    console.log('invite removed: ', inviteId);
    return fst
        .collection('institutions')
        .doc(inviteId)
        .delete()
        .catch(error => console.log('removeInvite->error: ', error))
}


module.exports = connectExistingCoachWithInstitution;