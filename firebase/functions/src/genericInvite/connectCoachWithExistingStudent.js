
// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

const sendCustomNotificationEmail = require('./sendCustomNotificationEmail');
const addPeerConnection = require('../peers/addPeerConnection');

const connectCoachWithExistingStudent = async function connectCoachWithExistingStudent(data, institutionName, inviteId) {

    console.log('*** updated ****');
    
    const user = await admin.auth()
        .getUserByEmail(data.target.value);

    /**
     * if user is role 0
     * initiatorId must be a coach, either from intitution or from self contained coach
     */
    console.log('connectCoachWithExistingStudent->data, user.uid: ', JSON.stringify(data), user.uid);

    if(user){
        // if institution coach, assign to coach customer
        // if initiatorId = institutionId, do not create any connections,
        // this will create an orphaned student entry that must be connected from institution overview
        // const role = typeof data.role !== number ? parseInt(data.role) : data.role;
        if (data.role.toString() === '0' && data.initiatorId !== data.institutionId) { // number as string :(((
            var updates = {};
            // assign invitee to coach, regardless of role
            updates['/coachCustomer/' + data.initiatorId + '/' + user.uid] = true;
            // combined updates
            await db.ref().update(updates);

            // update user table so that the EDU branding is assinged
            await updateUserData(data.initiatorId, user.uid, data.institutionId) // (initiatorId, userId, institutionId)

            // add to userPeers table
            await addPeerConnection(data.initiatorId, user.uid, data.institutionId);

            console.log('*** updated 1')
        } 

        const link = `https://${functions.config().email.domain}/`;

        const subject = `${institutionName} - Video-Unterricht / Online classes Information`;
        const emailTxt = `
Hallo,
du bist von einem Lehrenden bei ${institutionName} eingeladen worden.

1. Klicke auf den folgenden Link und der Browser öffnet sich:
${link}
2. Bitte melde dich mit ${data.target.value} als Nutzername und dem bekannten Passwort an.

Solltest du dein Passwort vergessen haben, nutze die “Passwort vergessen”-Funktion.

Liebe Grüße,
Dein Team von ${institutionName} & doozzoo

---------------------------------------------------------------------
Hello,
you have been connected by ${institutionName} to a new coach.

1. Click the following link and your webbrowser will open:  
${link}
2. Login with username: ${data.target.value} and your well known password.

In case you forgot your password, use the "password forgotten" function, to create a new one.

Thank you,
Your team of ${institutionName} & doozzoo
        `;

        await fst
            .collection('institutions')
            .doc(data.institutionId)
            .collection('accounts')
            .doc(data.triggerId)
            .update({
                status: 'studentAssignedToCoach',
                userId: user.uid, // enhancement: we directly sync the created userId into the inst. db!!
            }) 

        // update user table so that the EDU branding is assinged
        // !!! initiatorId is equal institutionId in this case !!!
        await updateUserData(data.institutionId, user.uid, data.institutionId) // (initiatorId, userId, institutionId)
            
        await removeInvite(inviteId);
        return sendCustomNotificationEmail(data.target.value, user.displayName, subject, emailTxt);

    } else {
        const response = 'studentAssignedToCoachError';
        console.log('genericInvite->connectCoachWithExistingStudent: ', response);

        // update firestore with update send
        // ####################
        await fst
            .collection('institutions')
            .doc(data.institutionId)
            .collection('accounts')
            .doc(data.triggerId)
            .update({
                status: 'error',
                errorMessage: response,
            });

        await removeInvite(inviteId);

        return response;

    }                             
   
}

async function removeInvite(inviteId) {
    // global invite Id
    console.log('invite removed: ', inviteId);
    return fst
        .collection('institutions')
        .doc(inviteId)
        .delete()
        .catch(error => console.log('removeInvite->error: ', JSON.stringify(error)));
}

async function updateUserData(initiatorId, userId, institutionId) { 
    console.log('userupdate: ', initiatorId, userId, institutionId)
    const data = {
        userActions: 'createdByInstitution',
        trigger: {
            institutionId: institutionId,
            initiatorId: initiatorId,
            triggerId: initiatorId,
            type: 'institution'
        }
    }
    return admin.database().ref('users/' + userId).update(data);
}


module.exports = connectCoachWithExistingStudent;