/**
 * dev ->
 * https://europe-west1-doozzoo-dev.cloudfunctions.net/verifyUserEmail
 * 
 * prod ->
 * https://europe-west1-doozzoo-eve.cloudfunctions.net/verifyUserEmail 
 */

// CORS Express middleware to enable CORS Requests.
const cors = require('cors')({origin: true});
const admin = require('../fooAdmin');

const verifyUserEmail = async (req, res) => {
    // const oobCode = req.query.oobCode || 'no token provided';
    // const uid = req.query.oobCode;
    const email = req.query.email;
    // const email = req.query.email;
    let error = false;
    let errorMsg = null;
    let user = null;

    // validate uid/email here
    // if valid, set EMAILVERIFIED

    // https://doozzoo.com/login
    // ?mode=verifyEmail
    // &oobCode=55Iflj6un9NnyC74XojeP78PeMIcpI8nCY4w4ZFy7e4AAAFw6YqyZg // test -> oob code = uid
    // ### &uid=<uid>

    try {
        user = await admin.auth().getUserByEmail(email);
        console.log('user: ', JSON.stringify(user));
    }
    catch(e) {
        error = true;
        errorMsg = 'user was not created';
    }

    if(user) {
      const uid = user.uid;
      admin.auth().updateUser(uid, {
        // email: 'modifiedUser@example.com',
        // phoneNumber: '+11234567890',
        emailVerified: true,
        // password: 'newPassword',
        displayName: email,
        // photoURL: 'http://www.example.com/12345678/photo.png',
        // disabled: true
      })
      .catch((error) => {
        console.log('Error updating user:', error);
        error = true;
        errorMsg = error;
      });
    } else {
      error = true;
      errorMsg = 'user was not created';
    }

    // Enable CORS using the `cors` express middleware.
    const response = data => {
      cors(req, res, () => {
        // expects data obj
        return res.status(200)
            .type('application/json')
            .send(data)
      });
    }
    
    // fetch updated user
    if(error) {
        response({ status: 'error', errorMsg: errorMsg })
    } else {
        await admin.auth().getUserByEmail(email);
        const verifyStatus = user.emailVerified;
        const data = {
            status: 'success',
            emailVerified: verifyStatus,
            user: JSON.stringify(user),
        }
        response(data);
    }

};

// final export
module.exports = verifyUserEmail;