var jwt = require('jsonwebtoken');

// CORS Express middleware to enable CORS Requests.
const cors = require('cors')({origin: true});

const generateJwt = async (req, res) => {
    // const token = req.query.token || 'no token provided';
    // console.log('token: ', token);

    // dev
    //const secret = '869b869135d41f8dc7ae198fa874e1d4481a71ec'; //Replace this with your OpenTok API Secret
    //prod
    const secret = '___acfe89198b029f961290efcccb6b082427f1e688';

    var jwtToken = jwt.sign(
      {
        //dev
        //iss: 46105142, //Replace this with your OpenTok API Key
        //prod
        iss: 46059892, //Replace this with your OpenTok API Key
        ist: "project",
        iat: Math.floor(Date.now() / 1000), // e.g. 1472691002
        exp: Math.floor(Date.now() / 1000) + 300 // e.g. 1472691302
      },
      secret
    );

    console.log('Token: ' + jwtToken);

    // decode
    const jwtTokenDecoded = jwt.verify(jwtToken, secret);
    console.log('Decoded: ' + jwtTokenDecoded);

    // Enable CORS using the `cors` express middleware.
    const response = data => {
		cors(req, res, () => {
			// expects data obj
			return res.status(200)
		        .type('application/json')
		        .send(data)
		});
  	}
  	data = {
        status: 'success',
        jwt: jwtToken,
        jwtDecoded: jwtTokenDecoded 
	}
    response(data);
};

// final export
module.exports = {
    generateJwt,
    // stripeVoucherValidate
}