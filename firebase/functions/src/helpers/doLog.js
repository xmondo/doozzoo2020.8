/**
 * TODO: ...
 */

const { Logging } = require('@google-cloud/logging');
const logging = new Logging();

const doLog = async (functionName, data) => {

    // Instantiate the StackDriver Logging SDK. The project ID will
    // be automatically inferred from the Cloud Functions environment.
    const log = logging.log(functionName);

    // This metadata is attached to each log entry. This specifies a fake
    // Cloud Function called 'Custom Metrics' in order to make your custom
    // log entries appear in the Cloud Functions logs viewer.
    const METADATA = {
        resource: {
            type: 'cloud_function',
            labels: {
                function_name: 'doozzoo metrics',
                region: 'europe-west1',
            }
        }
    };
  
  // Write to the log. The log.write() call returns a Promise if you want to
  // make sure that the log was written successfully.
  const entry = log.entry(METADATA, data);
  // console.log('doLog: ', entry);
  log.write(entry);
  
};

// final export
module.exports = doLog;

