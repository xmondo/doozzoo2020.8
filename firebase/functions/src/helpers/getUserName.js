/**
 * TODO: ...
 */

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
// const fst = admin.firestore();
const db = admin.database();

const getUserName = async (userId) => {
    try {
         // console.log('ref', _ref);
        const profile = await db
            .ref(`profiles/${userId}/privateData`)
            .once('value');

        const data = profile.val();
        const name = data.firstname + ' ' + data.lastname;
        return name;
    }
    catch(error){
        console.log(error);
        return 'Anonymous';
    }    

// end of main class
}
    
// final export
module.exports = getUserName;

