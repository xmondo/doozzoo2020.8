/**
 * read participant obj updated from classroom/{coachId}/participants
 * preresolve 
 * 
 * TODO: this update created "undefined" session in now, when concurrent transactions (e.g. delete) where happening 
 */

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

const participantQuota = async (snap, context) => {
    // onWrite -> 'classrooms/{coachId}/participants'
    
    const pSnap = await db
        .ref(`classrooms/${context.params.coachId}`)
        .once('value');
    //prefetch participant quota and sessionId
    const maxParticipants = pSnap.val().maxParticipants || 2;
    const roomId = pSnap.val().roomId;

    // we listen on updates, catch the case of a deletion
    const snapshot = snap.after.exists() ? snap.after : snap.before;
    const participants = snapshot.val();
    // console.log('participantQuota->snapshot: ', context.params.coachId, maxParticipants, JSON.stringify(participants))

    // make sure participants data is there
    // then create an array for more convenience
    if(participants !== null && participants !== undefined) {
        const participantsArr = Object.keys(participants).map(key => participants[key]);
        // filter out only those beeing in the session
        const participantsInSessionCount = participantsArr.filter(participant => participant.inSession === true);
        // console.log('participantQuota->participantsInSessionCount: ', participantsInSessionCount.length);

        // calc the boolean whether quota is full
        // save back to session obj
        const participantQuotaFull = participantsInSessionCount.length >= maxParticipants;
        const snap = await db.ref(`now/${roomId}/delete`).once('value');
        console.log('exists: ', snap.exists(), 'now.delete: ', snap.val(), 'participantQuotaFull: ', participantQuotaFull, 'participantsInSessionCount: ', participantsInSessionCount.length, 'maxParticipants: ', maxParticipants)
        // avoid writing into the session object when it is already market for
        if(snap.exists() === false || (snap.exists() === true && snap.val() !== 'delete')) {
            return db
                .ref(`now/${roomId}`)
                .update({ participantQuotaFull: participantQuotaFull })
        } else {
            return 'no update of participantQuotaFull';
        }
        
    } else {
        return 'no update of participantQuotaFull';
    } 

// end of main class
}
    
// final export
module.exports = participantQuota;

