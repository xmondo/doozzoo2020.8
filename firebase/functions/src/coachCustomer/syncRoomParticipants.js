/**
 * TODO: ...
 * syncRoomParticipants syncs the students, allowed to access the room from myStudents (coachCustomer) with classroom/participants
 */

  /*
export interface ParticipantList {
  key: string,
  access: boolean,
  name?: string,
  inSession?: boolean,
  isWaiting?: boolean,
  isRegistered?: boolean,
}
 */

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

// helper function to doSyncCoachCustomers
const syncRoomParticipants = async (coachId, studentslist) => {
    // ...
    //console.log('studentslist: ', JSON.stringify(studentslist))

    // read lock status of classroom, which is a kind of preference
    const lockStatusSnap = await db
        .ref(`/classrooms/${coachId}/locked`)
        .once('value');
    const lockStatus = lockStatusSnap.val() || false;

    // get room list
    // create an array from snapshot
    const participantList = [];
    const participantSnap = await db
        .ref(`/classrooms/${coachId}/participants`)
        .once('value');
    participantSnap.forEach(snapshotdata => {
        // console.log('_key: ', snapshotdata.key)
        // KEY MUST BE IN QUOTES!!!!!!!!!!!!!!!
        // skip temporary participants (registrationStatus === isAnonymous, isREgistered) which are not belonging to coach)
        // standard participants are added
        snapshotdata.val().registrationStatus === 'isCoachCustomer' ? participantList.push({ 'key': snapshotdata.key, 'values': snapshotdata.val() }) : '';
    });

    // console.log('participantList: ' , JSON.stringify(participantList));

    /**
     * iterate studentlist and check if participants are already in the classroom participant list
     * if not add them to the temporary "add" array
     * iterate participantlist and check if those are in the allowed students list
     * if not add them to the temporary "remove" array
     * Finally create update array and execute the add + remove actions at once
     */
    const studentslistAdd = [];
    const studentslistRemove = [];
    studentslist.forEach(item => {
        participantList.some(_item => item.key === _item.key) ? '' : studentslistAdd.push(item);
    });
    participantList.forEach(item => {
        studentslist.some(_item => item.key === _item.key) ? '' : studentslistRemove.push(item); 
    });

    //console.log('studentslistAdd: ', JSON.stringify(studentslistAdd));
    //console.log('studentslistRemove: ', JSON.stringify(studentslistRemove));

    const updates = {};
    const participantData = {
        access: !lockStatus, 
        isWaiting: false,
        inSession: false,
        // name?: string,
        // isRegistered: true, 
        registrationStatus: 'isCoachCustomer',
    }
    studentslistAdd
        .forEach((_val) => {
            updates[`/classrooms/${coachId}/participants/${_val.key}`] = participantData;
        });
    studentslistRemove
        .forEach((_val) => {
            updates[`/classrooms/${coachId}/participants/${_val.key}`] = null;
        });

    // apply the update array
    return db.ref().update(updates);

}
// [END syncRoomParticipants]
    
// final export
module.exports = syncRoomParticipants;

