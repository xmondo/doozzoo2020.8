/**
 * TODO: ...
 * Find out if a user is in the list of coachCustomers
 */

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

// ...
const checkRegistrationStatus = async (snap, context) => { 
    // ...
    const coachId = context.params.coachId;
    const userId = context.params.userId;
    const status = await isUserInCoachCustomer(userId, coachId);
    console.log('checkRegistrationStatus: ', coachId, userId, status);
    if(status) {
        return db
            .ref(`/classrooms/${coachId}/participants/${userId}`)
            .update({registrationStatus: 'isCoachCustomer'});
    } else {
        return 'is no coachCustomer';
    }  

} 

const isUserInCoachCustomer = async (userId, coachId) => { 
    // ...
   const customerSnap = await db
        .ref(`/coachCustomer/${coachId}/${userId}`)
        .once('value');
    console.log('isUserInCoachCustomer: ', customerSnap.exists(), customerSnap.val());
    return customerSnap.exists() && customerSnap.val() === true; 

}
// [END ParticipantsInSession]
    
// final export
module.exports = {
    isUserInCoachCustomer, 
    checkRegistrationStatus
};

