/**
 * TODO: ...
 * ParticipantsInSession listen for the session deletion and resets "inSessionStatus"
 */

 /*
export interface ParticipantList {
  key: string,
  access: boolean,
  name?: string,
  inSession?: boolean,
  isWaiting?: boolean,
  registrationStatus: string,
}
 */

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

// helper function to doSyncCoachCustomers
const participantsInSessionSync = async (coachId, status) => { // status -> true | false
    // ...
    const updates = {};
    const participantData = {
        inSession: status,
    }

    const participantSnap = await db
        .ref(`/classrooms/${coachId}/participants`)
        .once('value');
    participantSnap.forEach(snapshotdata => {
        // console.log('_key: ', snapshotdata.key)
        updates[`/classrooms/${coachId}/participants/${snapshotdata.key}/inSession`] = status;
    });

    // apply the update array
    return db.ref().update(updates);

}
// [END ParticipantsInSession]
    
// final export
module.exports = participantsInSessionSync;

