/**
 * TODO: ...
 * doSyncCoachCustomer is responsible for limiting active students according to the stripe package subscribed
 * I only sets the allowed number of students to true, everything beyond to false
 * IMPORTANT: if user is EDU, the package size is not limited and the sync must be done in any case
 */

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

const maxBetatesterParticipants = 50;

const syncRoomParticipants = require('./syncRoomParticipants');

// trigger: .database.ref('/coachCustomer/{coachId}').onWrite
const syncCoachCustomer = async (snap, context) => {
    // ...
    const coachId = context.params.coachId; 
    
    // TODO: was a wrong assumption: also for EDU the sync must be done
    // check first if user is EDU
    // in this case do nothing but return...
    const eduSnap = await db
        .ref(`/users/${coachId}/trigger/type`)
        .once("value");
    const eduSnapVal = eduSnap.val();
    const isEdu = eduSnapVal === null ? false : eduSnapVal === 'institution' || eduSnapVal === 'institutionCoach';


	// fetch the list of customers of a coach		
    const customerList = snap.after;
    const customerListArr = [];
    // create an array from snapshot and sort it according to the boolean status
    customerList.forEach(val => customerListArr.push({ key: val.key, status: val.val()}));
    // console.log('customerListArr: ', customerListArr);
    customerListArr.sort((a,b) => (a.status < b.status) ? 1 : ((b.status < a.status) ? -1 : 0));
    // console.log('customerListArr: ', customerListArr);

    if(!isEdu) {
        // fetching quantity
        const stripeSnap = await db
            // TODO: nach dem stripe update liegt die quantity woanders
            // .ref('/stripe_customers/' + coachId + '/subscriptions/currentSubscription/quantity')
            .ref(`/stripe_customers/${coachId}/customerData/subscriptions/data/0/items/data/0/quantity`)
            .once("value");
        // console.log('subscription quantity: ', stripeSnap.val());

        const betatesterSnap = await db
            // max 50...
            .ref(`/users/${coachId}/betatester`)
            .once("value");
        
        // minimum quantity is always the smallest package (5) ???
        // OR really 0 = no stripe account

        // our pack size for to multiply the plan factor with
        const packageSize = 5; 
        let _quantity = stripeSnap.val() === null ? 0 : (parseInt(stripeSnap.val()) * packageSize); 
        // if betatester overwrite 0
        betatesterSnap.val() === true ? _quantity = maxBetatesterParticipants : '';

        // console.log('processed quantity: ', _quantity);

        // copy the range above the allowed quantity into a new array
        // iterate through the array and set everything that is TRUE to FALSE
        // by creating a regarding FB update array
        customerListArrOverflow = customerListArr.slice(_quantity)
        // iterate over 
        const _updates = {};
        customerListArrOverflow
            .forEach((val) => {
                val.status === true ? _updates[`/coachCustomer/${coachId}/${val.key}`] = false : '';
            });

        // console.log('', JSON.stringify(_updates))
        // apply the update array
        // return db.ref().update(_updates);

        db.ref().update(_updates);
        
        // now create the to be list, for hand over to the classroom-update function
        customerListArrSelected = customerListArr.filter(val => val.status === true);
    } else {
        // for EDU process and sync the list as is
        customerListArrSelected = customerListArr;
    }

    // now sync the classroom participant list accordingly
    return syncRoomParticipants(coachId, customerListArrSelected);

}
// [END syncCoachCustomer]
    
// final export
module.exports = syncCoachCustomer;

