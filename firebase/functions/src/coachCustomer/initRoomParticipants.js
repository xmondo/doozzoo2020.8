/**
 * when session is initiated, all students stored at coachCustomer with activation status "true" are synced into the session.
 * access status of each participant is set according to locked-preference of the room
 */

/*
export interface ParticipantList {
  key: string,
  access: boolean,
  name?: string,
  inSession?: boolean,
  isWaiting?: boolean,
  isRegistered?: boolean,
}
*/

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();
const getUserName = require('./../helpers/getUserName');
const promisePool = require('es6-promise-pool');
const PromisePool = promisePool.PromisePool;

// helper function initially populate session participants list
const initRoomParticipants = async (coachId, lockStatus) => {
    // ...

    // read lock status of classroom, which is a kind of preference when creating the participants list
    // if locked, participants access is false by default and if unlocked the other way round

    // get room list
    // create an array from snapshot
    const myStudentsSnap = await db
        .ref(`/coachCustomer/${coachId}`)
        .once('value');

    let participants = {}
    let participantArr = []
    // create update for classroom participants from coachCustomer list
    myStudentsSnap.forEach(snapshotdata => {
        // check whether the participant is activated
        if(snapshotdata.val() === true) {
            // populate participant object
            const key = snapshotdata.key;
            participants[key] = {
                access: !lockStatus, 
                isWaiting: false,
                inSession: false,
                registrationStatus: 'isCoachCustomer'
            }
            // create an array of userIds
            participantArr.push(snapshotdata.key);
        }
    });
    console.log('initRoomParticipants->participantObj: ', JSON.stringify(participants))
    
    await db
        .ref(`/classrooms/${coachId}/participants`)
        .set(participants)
        .catch(error => console.log(JSON.stringify(error)))

    // work in a controlled promise pool to resolve names 
    // TODO: names should be preresolved there!!
    // promise pool functions must NOT be async!!
    const promisePool = new PromisePool(() => doUserName(participantArr, coachId), 5);
    return promisePool
        .start()
        .then(() => {
            //console.log(`names synced`);
            return true;
        })
        .catch((error) => {
            console.log('errors, promise rejected: ', error.message);
            return false;
        }); 

}
// [END initRoomParticipants]

// synchronus!!!
function doUserName(participantArr, coachId) {
    if (participantArr.length > 0) {
        // get first item and remove it from arr
        const userId = participantArr.shift();
        // console.log('coachId: ', coachId);
        return getUserName(userId) 
            .then(userName => {
                return db
                    .ref(`/classrooms/${coachId}/participants/${userId}`)
                    .update({ name: userName });
            })
            .catch(e => console.log(e)) 
    } else {
        return null;
    }
}
    
// final export
module.exports = initRoomParticipants;

