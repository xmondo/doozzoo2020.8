/**
 * this function is triggered every 60sec by a pubsub scheduler: cronMinuteMan
 * it scans all now sessions for myroomIds, which is the indicator for a session having time limits, defined in a myRooms/... obj
 * the myroomIds are pushed to an array, which then triggers a promise pool for executing the countDown function
 * countDown reads the specific timelimit and decrement it if > 0
 * the new minute value is saved there and copied to the session 
 */

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();
const promisePool = require('es6-promise-pool');
const PromisePool = promisePool.PromisePool;

const myroomsCountDown = async (context) => {
    // ...
    // console.log('myroomsCountDown->context: ', JSON.stringify(context));
    const sessionSnap = await db
        .ref('now')
        .once('value');
    const myroomsIdArr = [];
    sessionSnap.forEach(session => {
        const sessionId = session.key;
        const myroomId = session.val().myroomId || null;
        myroomId !== null ? myroomsIdArr.push({ sessionId: sessionId, myroomId: myroomId }) : '';
    });

    // work in a controlled promise pool
    // promise pool functions must NOT be async!!
    const promisePool = new PromisePool(() => doCountDown(myroomsIdArr), 5);
    return promisePool
        .start()
        .then(() => {
            //console.log(`names synced`);
            return true;
        })
        .catch((error) => {
            console.log('errors, promise rejected: ', error.message);
            return false;
        }); 
// end of main class
}

// synchronous!!!
function doCountDown(myroomsIdArr) {
    // console.log('myroomsIdArr: ', myroomsIdArr)
    if (myroomsIdArr.length > 0) {
        // get first item and remove it from arr
        const sessionData = myroomsIdArr.shift();
        // console.log('doCountDown->sessionData: ', sessionData);
        return fst
            .doc(`myRooms/${sessionData.myroomId}`)
            .get()
            .then(myroomData => {
                // console.log('doCountDown->myroomData: ', JSON.stringify(myroomData.data()))
                const minutesAvailable = myroomData.data().minutesAvailable || null;
                if(minutesAvailable !== null) {
                    return decrementMinutesAvailable(sessionData, minutesAvailable);
                } else {
                    return 'no minutesAvailable obj found';
                }
            })
            .catch(e => console.log('doCountDown error: ', e)); 
    } else {
        return null;
    }
}

async function decrementMinutesAvailable(sessionData, minutesAvailable){
    minutesAvailable = minutesAvailable - 1;
    if(minutesAvailable > 0) {
        await db
            .ref(`now/${sessionData.sessionId}`)
            .update({ minutesAvailable: minutesAvailable});
        return fst
            .doc(`myRooms/${sessionData.myroomId}`)
            .update({ minutesAvailable: minutesAvailable, modifiedOn: Date.now() });
    } else {
        return db
            .ref(`now/${sessionData.sessionId}`)
            .update({ minutesAvailable: minutesAvailable});
    }
}
    
// final export
module.exports = myroomsCountDown;

