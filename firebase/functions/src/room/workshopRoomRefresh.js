/**
 * TODO: ...
 */

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
// const fst = admin.firestore();
const db = admin.database();

const workshopRoomRefresh = async () => {
    
    const wRoomsList = await db
        .ref('now')
        .orderByChild('permanent')
        .equalTo(true)
        .once('value');

    const updatedRoomsList = [];
    wRoomsList.forEach(val => {
        // console.log(val.val());
        // console.log(val.key);
        const data = val.val();
        updatedRoomsList.push(
            {
                // name: data.name,
                name: val.key,
                owner: data.owner,
                mediaMode: data.mediaMode,
                permanent: true,
            }
        )
    })
    // read -> name, owner, mediaMode
    // set -> name, owner, mediaMode / permanent: true, countdown: (1day) 24 * 60 * 60, lock: true
    console.log('updatedRoomsList: ', JSON.stringify(updatedRoomsList));

    const deletes = {};
    updatedRoomsList.forEach(val => {
        deletes[`now/${val.name}`] = null;
    })
    await db.ref().update(deletes);

    const updates = {};
    updatedRoomsList.forEach(val => {
        updates[`now/${val.name}`] = val;
    })
    
    return await db.ref().update(updates);

    // return 'workshop rooms refreshed...';

// end of main class
}
    
// final export
module.exports = workshopRoomRefresh;

/**
 * search in now for rooms with permanent === true
 * iterate over them and 
 *  0. read specific attributes of the room
 *  1. delete the room
 *  2. set it up again
 * 
 */

