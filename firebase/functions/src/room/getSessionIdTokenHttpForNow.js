/**
 * TODO: ...
 * this function initiates the session by creating a opentok session, fetching a session id and creating a general token, used by all participants
 * then this session model is stored in now/... and provided to the participants of the doozzoo session.
 * Then the participants are dynamically set up in session according to coachCustomer settings
 * Furthermore roomId and lock status are first taken from classroom and due to safety reasons finally written back
 */

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();
// opentok lib
var OpenTok = require('opentok');
const API_KEY = functions.config().ot.key;
const API_SECRET = functions.config().ot.secret;
const BUCKET_LOCATION = functions.config().bucket.location;
// const NOW_LIMIT = 3600; // 30min. time for session
const NOW_LIMIT = functions.config().now.limit; // eieruhr
const TOKEN_LIMIT = functions.config().now.tokenlimit; // real safety

const logNow = require('../logNow');
const initRoomParticipants = require('../coachCustomer/initRoomParticipants')

const getSessionIdTokenHttpForNow = async (snap, context) => {
    
    const roomId = context.params.roomId;
    const ownerId = snap.val().owner;
    // id for the plan rooms config & quotas -> starter, etc.
    const myroomId = snap.val().myroomId;
    const isPermanent = snap.val().permanent;
    const tokenLimit = TOKEN_LIMIT; // 4h | 14400 // 12h | in msec
    const nowLimit = isPermanent ? TOKEN_LIMIT : NOW_LIMIT;
    const maxParticipants = snap.val().maxParticipants || 2;

    // get room lock preference
    // const lockStatusSnap = await db
    const classroomSnap = await db
        .ref(`/classrooms/${ownerId}`)
        .once('value');
    const lockStatus = classroomSnap.val().lockStatus || false;
    console.log('lockStatus->', lockStatus)
    const mediaMode = classroomSnap.val().mediaMode || 'routed';
    console.log('mediaMode->', mediaMode)

    const otConfig = { mediaMode: mediaMode, archiveMode: 'manual' };

    // ### OT retrieval ###
    const opentok = new OpenTok(API_KEY, API_SECRET);

    const otSession = new Promise((resolve, reject) => {
        opentok.createSession(otConfig, (error, session) => {
            if (error) {
                reject(error);
            } else {
                resolve(session);
            }
        });
    });

    const session = await otSession
        .catch(error => console.log("createSession->Error creating session: ", error));
    
    const sessionId = session.sessionId;

    // now fetch token too
    let tokenOptions = {};
    // this is a customer = publisher only
    tokenOptions.role = "publisher"; // moderator, publisher, subscriber
    //tokenOptions.data = "classroomId=" + event.params.coachId;
    // set expiration as epoch time now * expiration time in sec
    // 1 week = 604.800
    // 12hrs = 43.200
    // 8hrs = 28.800
    // 1h = 3.600
    // 15min = 900 
    let t = Date.now();
    t = (Math.round(t/1000.0)) + tokenLimit; // NOW_LIMIT;
    tokenOptions.expireTime = t;   

    // Generate a token.
    let token = await opentok.generateToken(sessionId, tokenOptions);

    const logData = {
        userId: ownerId,
        sessionId: sessionId,
        duration: 0, 
        maxStreams: 0, 
        timestamp: Date.now(),
    }
    
    // updateDB
    await db
        .ref('/now/' + roomId)
        .update({ 
            sessionId: sessionId, // from OT
            token: token, // from OT
            tokenLimit: tokenLimit,
            timestamp: admin.database.ServerValue.TIMESTAMP, // placeholder obj
            countdown: nowLimit,
            visitCount: 0,
            lessonState: true,
            locked: lockStatus, // read lock status pref from room
            mediaMode: mediaMode, // routed or relayed
            participantQuotaFull: false, // init this value for later update
        })
        .then(result => {
            logNow('now-start', logData);
            // console.log('getSessionIdTokenHttpForNow done');
            return 'getSessionIdTokenHttpForNow done';
        })
        .catch(error => {
            logNow('now-error', error);
            console.log(error)
        });

    // set directShare Prefs
    // await ?
    db.ref(`/directShare/${sessionId}`)
        .update({ 
            senderId: ownerId,
            remoteStatus: false,
        })

    // set roomId from function
    await db
        .ref('/classrooms/' + ownerId)
        .update({ 
            roomId: roomId,
            myroomId: myroomId,
            maxParticipants: maxParticipants,
            locked: lockStatus, // in case it was not set
            roomCreated: admin.database.ServerValue.TIMESTAMP 
        });

    // init Room participants in classroom object for managing participants classes, permissions and lifecycle
    return initRoomParticipants(ownerId, lockStatus);

// end of main class
}
    
// final export
module.exports = getSessionIdTokenHttpForNow;

