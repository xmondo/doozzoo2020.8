
//const { Logging } = require('@google-cloud/logging');
//const logging = new Logging();

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

const promisePool = require('es6-promise-pool');
const PromisePool = promisePool.PromisePool;
// const secureCompare = require('secure-compare');
// Maximum concurrent account requests.
const MAX_CONCURRENT = 5;

// CORS Express middleware to enable CORS Requests.
const cors = require('cors')({origin: true});
const secureCompare = require('secure-compare');
const getCustomerProfile = require('./getCustomerProfile');

const getLogStream = async function (req, res) {
    // const token = req.query.token || 'no token provided';
    // console.log('token: ', token);

    const rowsArr = [];
    let str = '';
    let sessionStats; 

    // Exit if the keys don't match.
    // 3d95702d45dcd4efc19543447a5088397df956a2
    const key = req.query.key;
	if (!secureCompare(key, functions.config().cron.key)) {
		console.log('The key provided in the request does not match the key set in the environment. Check that', key, 'matches the cron.key attribute in `firebase env:get`');
		res.status(403).send('Security key does not match. Make sure your "key" URL query parameter matches the ' + 'cron.key environment variable.');
		return null;
	}

    // range = today | lastday | lastweek | lastmonth | date
    if(req.query.range) {
        const startOfToday = new Date().setHours(0);
        const endOfToday = new Date().setHours(23);
        let from;
        let to;
        let date;

        switch(req.query.range) {
            case 'date':
                date = new Date(req.query.date); // input -> '2020-5-17' // no padding needed
                from = date.setHours(0);
                to = date.setHours(23);
                break;
            case 'today':
                from = startOfToday;
                to = endOfToday;
                break;
            case 'lastday':
                from = startOfToday - (1000 * 60 * 60 * 24);
                to = startOfToday;
                break;
            case 'daybeforeyesterday':
                from = startOfToday - (1000 * 60 * 60 * 24 * 2);
                to = startOfToday - (1000 * 60 * 60 * 24 * 1);
                break;
            case 'lastweek':
                from = startOfToday - (1000 * 60 * 60 * 24 * 7);
                to = startOfToday;
                break;
            // last 30 days
            case 'lastmonth':
                from = startOfToday - (1000 * 60 * 60 * 24 * 30);
                to = startOfToday;
                break;
        }

        sessionStats = await db
            .ref('logs/logStream')
            .orderByChild('timestamp')
            .startAt(from)
            .endAt(to)
            .once('value');

    } else if (req.query.stepsBack) {
        const stepsBack = parseInt(req.query.stepsBack) * 100;
        sessionStats = await db
            .ref('logs/logStream')
            .orderByChild('timestamp')
            .limitToLast(100 - stepsBack)
            .once('value');

    } else {
        sessionStats = await db
            .ref('logs/logStream')
            .orderByChild('timestamp')
            .limitToLast(100)
            .once('value');
            
    }

    let rowDataArr;
    sessionStats.forEach(val => {
        const row = val.val();
        const numStreams = Object.keys(row).length - 3;
        const duration = Math.floor(row.duration / 1000);
        const _date = new Date(row.timestamp)
            .toLocaleDateString('de-DE', {  
                day : 'numeric',
                month : 'numeric',
                year : 'numeric'
            })

        const dateTime = new Date(row.timestamp)
            .toLocaleDateString('de-DE', {  
                day : 'numeric',
                month : 'numeric',
                year : 'numeric',
                hour: '2-digit',
                minute: '2-digit'
            }) 
        
        rowDataArr = [ _date, dateTime, row.userId, duration, numStreams ];
         
        rowsArr.push(rowDataArr);
        
    });   

    // console.log('before rowsArr: ', rowsArr);
    const newRowsArr = [];
    const promisePool = new PromisePool(() => createRows(rowsArr, newRowsArr), 10);
    await promisePool.start();

    // console.log('after newRowsArr: ', newRowsArr);

    // now render csv
    let row = '"Date","Date Time","UserId","Duration","Number of streams","is Anonymous","Role","is Coach","is Institution","Subscriptions","number of Students"\n';
    // insert header row
    str += row;
    // render rest of rows
    newRowsArr.forEach(rowItem => {
        row = '';
        rowItem.forEach((_item, idx) => {
            idx < rowItem.length - 1 ? row += `"${_item}",` : row += `"${_item}"\n`;
        });
        str += row;
    });

    // Enable CORS using the `cors` express middleware.
    const response = data => {
		cors(req, res, () => {
			// expects data obj
			return res.status(200)
                //.type('application/json')
                .type('text/csv')
                //.type('application/vnd.ms-excel')
		        .send(data)
		});
    }
      
    // return response(JSON.stringify(newRowsArr));

    return response(str);
}

function createRows(rowsArr, newRowsArr){

    if (rowsArr.length > 0) {
        // get firt istem and remove it from original arr
        const item = rowsArr.shift();

        // try to resolve userId
        // when userId can not be resolved (value = null or user not existing),
        // execute catch and returmn empty values
        return getCustomerProfile(item[2])
            .then(userProfile => {
                item.push(userProfile.anonymous);
                item.push(userProfile.role);
                item.push(userProfile.coach);
                item.push(userProfile.institution);
                item.push(userProfile.subscriptionQuantity);
                item.push(userProfile.numberOfStudents);
                return newRowsArr.push(item);
            })
            .catch(error => {
                let i = 0;
                while(i < 6) {
                    item.push('');
                    i++;   
                }
                return newRowsArr.push(item);
            })

    } else {
        return null;
    }

}

module.exports = getLogStream;

/*


https://europe-west1-doozzoo-eve.cloudfunctions.net/getLogStream?key=3d95702d45dcd4efc19543447a5088397df956a2

today | lastday | lastweek | lastmonth | 
https://europe-west1-doozzoo-eve.cloudfunctions.net/getLogStream?range=lastDay

https://europe-west1-doozzoo-eve.cloudfunctions.net/getLogStream?stepsBack=1

userId
    - coach | institutionalCoach
    - coach -> subscription

{ sessionId: '2_MX40NjA1OTg5Mn5-MTU4OTU0MjI0NDk2OX5nRTZQS21OYW5HdVgxalJncjlmSDBUdW1-fg',
  projectId: '46059892',
  event: 'streamDestroyed',
  reason: 'clientDisconnected',
  timestamp: 1589543254406,
  connection: 
   { id: '48a74589-a9f7-46b8-93e2-98183620d145',
     createdAt: 1589542269154,
     data: null },
  stream: 
   { id: 'fdbaaa6d-9dfa-4b15-942d-059c8571ee7a',
     connection: 
      { id: '48a74589-a9f7-46b8-93e2-98183620d145',
        createdAt: 1589542269154,
        data: null },
     createdAt: 1589542269492,
     name: '%7B%22name%22%3A%22Chr+k%22%2C%22isOwner%22%3Afalse%2C%22avatar%22%3A%22assets%2Fimg%2Favatars%2Favataaars-default.png%22%2C%22audioLabel%22%3A%22iPhone+Mikrofon%22%2C%22videoLabel%22%3A%22Frontkamera%22%7D',
     videoType: 'camera' } }

*/