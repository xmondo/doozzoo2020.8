
//const { Logging } = require('@google-cloud/logging');
//const logging = new Logging();

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

logStreamEvents = async function logStreamEvents(data) {
    // session id => events -> streamCreated | streamDestroyed / stream.id / reason / name -> parsed / lokkup userId from now | timestamp
    let duration = 0;
    const totalDuration = await db
        .ref('logs/logStream/' + data.sessionId + '/duration')
        .once('value');
    if(totalDuration.exists) {
        duration = totalDuration.val();
    }

    const userId = await db
        .ref('now')
        .orderByChild('sessionId')
        .equalTo(data.sessionId)
        .limitToFirst(1)
        .once('value');

    const dummyArr = [];
    userId.forEach(val => dummyArr.push(val.val()));
    let owner;
    try{
        owner = dummyArr[0].owner;
    }
    catch(e){
        owner = 'anonymous';
    }

    const snap = await db
        .ref('logs/logStream/' + data.sessionId)
        .once('value');
    numberOfStreams = snap.numChildren() - 4;
       
    dataSet = {
        event: data.event,
        sessionId: data.sessionId,
        timestamp: data.timestamp,
        createdAt: data.stream.createdAt,
        duration: data.timestamp - data.stream.createdAt,
        // reason: data.reason,
        userId: owner,
    }
    if (data.event === 'streamDestroyed') {
        dataSet.reason = data.reason;
    }

    //console.log('logStreamEvents->dataSet: ', dataSet);

    duration = duration + (data.timestamp - data.stream.createdAt);
    durationData = {
        duration: duration,
        timestamp: admin.database.ServerValue.TIMESTAMP,
        userId: owner,
        numberOfStreams: numberOfStreams,
    }
    await db
        .ref('logs/logStream/' + data.sessionId)
        .update(durationData);
    //console.log('duration: ', duration);

    return db.ref('logs/logStream/' + data.sessionId + '/' + data.stream.id)
        .set(dataSet);
 
}
module.exports = logStreamEvents;

/*

{ sessionId: '2_MX40NjA1OTg5Mn5-MTU4OTU0MjI0NDk2OX5nRTZQS21OYW5HdVgxalJncjlmSDBUdW1-fg',
  projectId: '46059892',
  event: 'streamDestroyed',
  reason: 'clientDisconnected',
  timestamp: 1589543254406,
  connection: 
   { id: '48a74589-a9f7-46b8-93e2-98183620d145',
     createdAt: 1589542269154,
     data: null },
  stream: 
   { id: 'fdbaaa6d-9dfa-4b15-942d-059c8571ee7a',
     connection: 
      { id: '48a74589-a9f7-46b8-93e2-98183620d145',
        createdAt: 1589542269154,
        data: null },
     createdAt: 1589542269492,
     name: '%7B%22name%22%3A%22Chr+k%22%2C%22isOwner%22%3Afalse%2C%22avatar%22%3A%22assets%2Fimg%2Favatars%2Favataaars-default.png%22%2C%22audioLabel%22%3A%22iPhone+Mikrofon%22%2C%22videoLabel%22%3A%22Frontkamera%22%7D',
     videoType: 'camera' } }

*/