// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

const getCustomerProfile = async function getCustomerProfile(userId) {

    // const numOfStudents = Object.keys(customerList).length; 
    let anonymous;
    let role;	
    let coach;
    let institution;
    let numberOfStudents;
    let hasStripeAccount;
    let subscriptionQuantity;

    let user;

    try{
        user = await admin.auth().getUser(userId);
    } 
    catch(error) {
        console.log('auth-getUser->error: ', error.code)
        return null;
    }

    user = { email: 'hallo'};

    if(user !== null && user !== undefined) {
        // console.log(user);
        anonymous = user.email ? false : true;

        doozzooUser = await db
            .ref('/users/' + userId)
            .once("value");

        role = doozzooUser.val().role;

        try {
            institution = doozzooUser.val().trigger.institutionId;

        }
        catch(error) {
            institution = false;
        }

        if(role === 1) {
            coach = institution !== false ? 'institutionCoach' : 'coach';

            const coachCustomers = await db
                .ref('/coachCustomer/' + userId)
                .once("value");

            numberOfStudents = coachCustomers.numChildren();
        }

        if(coach === 'coach') {
            try {   
                const stripe = await db
                    .ref('/stripe_customers/' + userId)
                    .once("value");

                hasStripeAccount = stripe.val() !== null ? true : false;

                const stripeAccount = await db
                    .ref('/stripe_customers/' + userId + '/subscriptions/currentSubscription/quantity')
                    .once("value");              

                if(stripeAccount.val() === null){
                    subscriptionQuantity = 0;
                } else {
                    subscriptionQuantity  = parseInt(stripeAccount.val()) * 5; // TODO: temporary change for betatesters
                }
            }
            catch(error) {
                hasStripeAccount = false;
                subscriptionQuantity = 0;
            }
        }

    } else {
        return null;
    }    

    const data = {
        anonymous: anonymous,
        role: role,
        coach: coach,
        institution: institution,
        subscriptionQuantity: subscriptionQuantity,
        numberOfStudents: numberOfStudents,
    }
  
    return data;

}

module.exports = getCustomerProfile;

/**
 * anonymous
 * role
 * coach | institutional coach
 * subscription quantity
 * number of students (coachCustomer)
 */