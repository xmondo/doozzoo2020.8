
// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

const promisePool = require('es6-promise-pool');
const PromisePool = promisePool.PromisePool;

const addPeerConnection = require('../peers/addPeerConnection');

const enrichPeers = async function() {
    
    const accounts = await fst
        .collectionGroup('accounts')
        .get();

    accountsArr = [];
    accounts.forEach(item => {
        try{
            const data = item.data();
            // there might be entries with null userId
            if(data.role === 1 && (data.userId !== null && data.userId !== undefined)) {
                const _data = { user: data.userId, institution: item.ref.parent.parent.id }
                accountsArr.push(_data);    
            }
        }
        catch(error){
            console.log('accounts loop: ', error);
        }
    });
    console.log(accountsArr.length, accountsArr);

    peersArr = [];

    // iterate array for userIds
    // query coachCustomer for peers per coachId
    // populate newArray: [[ user: coachId, peer: peerId ], [...]]

    const promisePool = new PromisePool(() => createPeers(accountsArr, peersArr), 5);
    await promisePool.start();

    console.log(peersArr.length); //, peersArr);

    const promisePool2 = new PromisePool(() => addPeers(peersArr), 5);
    await promisePool2.start();

    return 'peers enrichment done...';

// end of main class
}

function createPeers(accountsArr, peersArr){

    if (accountsArr.length > 0) {
        // get firt istem and remove it from original arr
        const item = accountsArr.shift();

        // try to resolve userId
        // when userId can not be resolved (value = null or user not existing),
        // execute catch and return empty values
        return db
            .ref(`coachCustomer/${item.user}`)
            .once('value')
            .then(_item => {
                _item.forEach(val => {
                    const data = { user: item.user, peer: val.key, institution: item.institution };
                    item.user !== val.key ? peersArr.push(data) : '';
                });
                return true;
            }).catch(error => {
                return error;
            });

    } else {
        return null;
    }

}

function addPeers(peersArr) {
    if (peersArr.length > 0) {
        // get firt istem and remove it from original arr
        const item = peersArr.shift();

        return addPeerConnection(item.user, item.peer, item.institution);

    } else {
        return null;
    }
}
    
// final export
module.exports = enrichPeers;

