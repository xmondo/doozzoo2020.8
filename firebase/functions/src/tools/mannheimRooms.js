/**
 * TODO: ...
 */

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

const mannheimRooms = async (snap, context) => {
    
    // delete
    const deletes = {};
    deletes['now/mannheim1'] = null;
    deletes['now/mannheim2'] = null;
    deletes['now/mannheim-einspielraum1'] = null;
    deletes['now/mannheim-einspielraum2'] = null;
    await db.ref().update(deletes);

    const updates = {};
    //data 
    updates['now/mannheim1'] = {
        owner: 'HjSkYtU9ClS8hk8tu2RCZJ4zdx02',
        permanent: true,
        name: 'Mannheim 1',
        lock: false,
        countdown: 57600,  
    };
    updates['now/mannheim2'] = {
        owner: 'Hii71VwAh4M2Krk8ucwNyCtGuob2',
        permanent: true,
        name: 'Mannheim 2',
        lock: false, 
        countdown: 57600,  
    };
    updates['now/mannheim-einspielraum1'] = {
        owner: 'fVPJozFtMsOg1oArWJ4dSleil7m1',
        permanent: true,
        name: 'Mannheim Einspielraum 1', 
        lock: false,
        countdown: 57600,  
    };
    updates['now/mannheim-einspielraum2'] = {
        owner: 'tVjx5jHi1BNMYbu83bMuRjgHLHj1',
        permanent: true,
        name: 'Mannheim Einspielraum 2',
        lock: false,
        countdown: 57600,  
    };
    // combined updates
    return db.ref().update(updates);

// end of main class
}
    
// final export
module.exports = mannheimRooms;

