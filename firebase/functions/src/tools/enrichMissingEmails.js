
// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

const promisePool = require('es6-promise-pool');
const PromisePool = promisePool.PromisePool;

const enrichMissingEmails = async function() {
    
    const accounts = await fst
        .collectionGroup('accounts')
        .get();

    accountsArr = [];
    accounts.forEach(item => {
        try{
            const data = item.data();
            // there might be entries with null userId
            if(data.email === null || data.email === undefined && (data.userId !== null && data.userId !== undefined)) {
                const _data = { user: data.userId }
                accountsArr.push(_data);    
            }
        }
        catch(error){
            console.log('accounts loop: ', error);
        }
    });
    console.log(accountsArr.length); //, accountsArr);

    emailArr = [];


    // iterate array for userIds
    // query coachCustomer for peers per coachId
    // populate newArray: [[ user: coachId, peer: peerId ], [...]]

    const promisePool = new PromisePool(() => findEmails(accountsArr, emailArr), 5);
    await promisePool.start();

    console.log(emailArr.length); //, emailArr); 

    const promisePool2 = new PromisePool(() => enrichAccounts(emailArr), 5);
    await promisePool2.start();

    return 'email enrichment done...';

// end of main class
}

function findEmails(accountsArr, emailArr){

    if (accountsArr.length > 0) {
        // get firt istem and remove it from original arr
        const item = accountsArr.shift();

        // try to resolve userId
        // when userId can not be resolved (value = null or user not existing),
        // execute catch and return empty values

        return admin
            .auth()
            .getUser(item.user)
            .then(user => {
                const data = { user: item.user, email: user.email};
                if(user.email !== null && user.email !== undefined) {
                    return emailArr.push(data);
                } else {
                    return 'no email found';
                }
            })
            .catch(error => {
                console.log('could not fetch email', error);
                // if(error.errorInfo.code === 'auth/user-not-found')
                const data = { user: item.user, error: 'auth/user-not-found'};
                return emailArr.push(data);
            });

    } else {
        return null;
    }

}

function enrichAccounts(emailArr) {
    if (emailArr.length > 0) {

        // get firt item and remove it from original arr
        const item = emailArr.shift();
        const myref = item.ref;
        if(item.error) {
            return fst
                .collectionGroup('accounts')
                .where('userId', '==', item.user)
                .get()
                .then(result => {
                    return result.forEach(_item => _item.ref.update({ status: 'userNotFound' }));
                });
        } else {
            return fst
                .collectionGroup('accounts')
                .where('userId', '==', item.user)
                .get()
                .then(result => {
                    return result.forEach(_item => {
                        console.log('role: ', _item.data().role);
                        if(_item.data().role !== null && _item.data().role !== undefined) {
                            _item.ref.update({ email: item.email });
                        } else {
                            // update with generic role
                            _item.ref.update({ email: item.email, role: 0 });
                        }
                        
                    });
                });
        }
        
    } else {
        return null;
    }
}
    
// final export
module.exports = enrichMissingEmails;

