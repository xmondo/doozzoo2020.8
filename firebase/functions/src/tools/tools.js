/**
 * TODO: ...
 */

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

const enrichPeers = require('./enrichPeers');
const enrichMissingEmails = require('./enrichMissingEmails');
const mannheimRooms = require('./mannheimRooms');

const tools = async (snap, context) => {
    // .ref('temp/{tool}')
    /* tool ist gleich der endpunkt d.h. der Wert */

    //console.log('snap, context: ', snap, context);
    //console.log('tools->snap: ', snap);
    /*
    console.log('snap.before', snap.before.data());
    console.log('snap.after', snap.after.data());
    */
    // in case of deletion, after === null
    const after = snap.after.exists ? snap.after.val() : null;
    // Get an object with the previous document value (for update or delete)
    const before = snap.before.val();

    if(context.params.tool === 'enrichNames') {
        // console.log(after, context.params);

        return doEnrichNames();

    } else if(context.params.tool === 'enrichPeers') {
        // console.log(after, context.params);

        return enrichPeers();
    } else if(context.params.tool === 'enrichEmails') {
        console.log(after, context.params);

        return enrichMissingEmails();
    } else if(context.params.tool === 'mannheim') {
        console.log(after, context.params);

        return mannheimRooms();
    } else {

        return 'after was null';
    }

// end of main class
}

async function doEnrichNames() {
    const accounts = await fst
        .collectionGroup('accounts')
        //.where('userId','==','foo')
        .get();

    return accounts.forEach(item => {
        const data = item.data();
        console.log('enrichNames->item: ', data);
        
        if(data.userId !== null && data.userId !== undefined){
            console.log('enrichNames->item: ', data);
            return executeEnrichment(data.userId, item.ref, data.email);
        } else {
            return 'userId was null';
        }
    })
        
}

async function executeEnrichment(userId, _ref, email) {
    // console.log('ref', _ref);
    const profile = await db
        .ref(`profiles/${userId}/privateData`)
        .once('value');

    const data = profile.val();

    try {
        const name = data.firstname + ' ' + data.lastname;
        if(name.indexOf('please edit') === -1) {
            return _ref.update({ name : name });
        } else {
            return 'dummy value found';
        }

    }
    catch(error){
        console.log(error);
        return error;
    }
    
}
    
// final export
module.exports = tools;

