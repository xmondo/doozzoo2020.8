
// const fs=require('fs'); 
const nodemailer = require('nodemailer');
// indirect ref of admin
const admin = require('./fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');

const sendWelcome = async (user) => {

    const newUser = await admin.auth().getUser(user.uid);
    // console.log('sendWelcome-getUser: ', user);
    // console.log('user/new user: ', user.emailVerified, newUser.emailVerified);
    
    const userEmail = user.email;
    const displayName = user.displayName || 'doozzoo user';
    const domain = functions.config().email.domain;
    const actionCodeSettings = {
        // URL you want to redirect back to. The domain (www.example.com) for
        // this URL must be whitelisted in the Firebase Console.
        // functions.config().email.domain
        
        url: `https://${domain}/login`,

        /*
        // This must be true for email link sign-in.
        handleCodeInApp: true,
        iOS: {
          bundleId: 'com.example.ios'
        },
        android: {
          packageName: 'com.example.android',
          installApp: true,
          minimumVersion: '12'
        },
        // FDL custom domain.
        dynamicLinkDomain: 'coolapp.page.link'
        */
    };

    // send only if the user email not has been verified yet
    if(newUser.emailVerified === false) {
        // console.log('sendWelcome-getUser: sending emial verification link');
        return admin.auth()
            .generateEmailVerificationLink(userEmail, actionCodeSettings)
            .then((link) => {
                // console.log('generateEmailVerificationLink->link: ', link)
                // Construct email verification template, embed the link and send
                // using custom SMTP server.
                return sendCustomVerificationEmail(userEmail, displayName, link);
            })
            .catch((error) => {
                // Some error occurred.
                console.log('generateEmailVerificationLink->error: ', error)
            });
    } else { 
        console.log('sendWelcome-getUser: email is already verified');
        return 'email is already verified'; 
    }
    
};

// sendCustomVerificationEmail(userEmail, displayName, link);
async function sendCustomVerificationEmail(userEmail, displayName, link) {
    const from = 'support@doozzoo.com';
    const options = {
        host: 'wp12975660.mailout.server-he.de',
        port: 587, // 587 | 25
        secure: false, // false, // upgrade later with STARTTLS
        auth: {
            user: 'wp12975660-support',
            pass: 'Do50-2=48**'
        }
    };
    const transporter = nodemailer.createTransport(options);
    // verify connection configuration					
    transporter.verify((error, success) => {
        if (error) {
            console.log('sendInviteMail->error:', error);
        } else {
            console.log('sendInviteMail->success:', 'Server is ready to take our messages');
        }
    });
    const sendIt = (userEmail, from, to) => {
        var mailOptions = {
            from: from,
            to: userEmail,
            subject: `Verify your email for doozzoo`,
            text:             
`Hello ${displayName}, 
Follow this link to verify your email address. 

${link}

If you didn’t ask to verify this address, you can ignore this email.

Thank you,
Your doozzoo team`

        };
        transporter.sendMail(mailOptions)
            .then(() => {
                console.log('sendCustomVerificationEmail->account creation mail send to: ', to);
                return true;
            })
            .catch(error => console.log('transporter.sendMail', error))
    }
    return sendIt(userEmail, from, displayName);
}

// final export
module.exports = sendWelcome;
