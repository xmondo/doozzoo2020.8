/**
 * TODO: ...
 */

// indirect ref of admin
const admin = require('./fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();

const institutionsAccountsUpdateAgent = async (snap, context) => {
    // path -> .document('institutions/{institutionId}/accounts/{accountId}')
    /*
        console.log('snap, context: ', snap, context);
        console.log('snap.before', snap.before.data());
        console.log('snap.after', snap.after.data());
    */
    // in case of deletion, after === null
    const after = snap.after.exists ? snap.after.data() : null;
    // Get an object with the previous document value (for update or delete)
    const before = snap.before.exists ? snap.before.data() : null;

    /**
     * cases for create and delete first
     */
    // case newly created record
    if(before === null) {
        console.log('newly created');
        return 'newly created';
    // case deleted record
    } else if (after === null) {
        console.log('just deleted', after);
        return deleteUser(before, context);   
    } // ... else if {

    /** 
     * we are interested only in changes of
     * - role
     * - activated
     * - action
     * 
     * case role change
     * 1. sync to Intitution config if institution admin (role = 3)
     * 2. for both -> sync of role to user db
     * 
     * case activated change or other role change
     * -> sync of role to user db
     * 
     * case action triggers dis|connection to peers
     */
    // case -> everything where neither before nor after === null

    // case update -> of admin role
    else if (after.role !== before.role && (after.role === 3 || before.role === 3)) {
        return syncAdminUpdateUser(after, context);
    
    // case update -> of other roles and field activated
    } else if(after.role !== before.role || after.activated !== before.activated){
        return syncUpdateUser(after, context);

    // case of initial creation of admin account which must be synced with admin users in config
    } else if(after.status !== before.status) {
        // core sync
        return syncStatus(after, context);
    // trigger actions
    } else {
        return true;
    }
    
};

async function syncAdminUpdateUser(after, context) {
    const institutionId = context.params.institutionId;
    let data;
    if(after.role === 3) {
        data = { users: admin.firestore.FieldValue.arrayUnion(after.userId) }
    } else if (after.role !== 3) {
        data = { users: admin.firestore.FieldValue.arrayRemove(after.userId) } 
    }
    
    console.log('data', data);
    await fst
        .collection('institutions')
        .doc(institutionId)
        .update(data);
    return syncUpdateUser(after, context);
}

async function syncUpdateUser(after, context) {
    // combined updates
    /*
    var updates = {};
    updates['users/' + userId + '/role'] = after.role;
    updates['profiles/' + user.uid + '/contactData'] = contactData;
    */
    const data = {
        activated: after.activated,
        role: after.role,
        trigger: {
            institutionId: context.params.institutionId,
            initiatorId: after.initiatorId,
            type: 'institution',
        }
    }
    return await admin.database()
        .ref('users/' + after.userId)
        .update(data);
}

async function syncStatus(after, context) {
    // user did not exist before and just accepted the invite
    // trigger peer assignment
    if (after.status === 'inviteAccepted') {
        // connection is only then created automatically, when originator is the institutionCoach
        // and the newly created user is a student
        if(after.role === 0 && after.initiatorId !== context.params.institutionId) {
            return syncPeers('connect', context.params.institutionId, after.userId, after.initiatorId);
        } else if(after.role === 3) {
            // we have to sync his id into the institution users/admins list
            console.log('institutionsAccountsUpdateAgent->syncStatus: update institution id');
            const data = { users: admin.firestore.FieldValue.arrayUnion(after.userId) }
            return fst
                .collection('institutions')
                .doc(context.params.institutionId)
                .update(data);
        }
    } else {
        console.log('institutionsAccountsUpdateAgent->syncStatus: NO update')
        return 'syncStatus: NO update';
    }
    return 'syncStatus: NO update';
}

async function deleteUser(before, context) {
    // remove from peers
    await syncPeers('delete', context.params.institutionId, before.userId, 'deletedUser');

    if (before.role === 3) {
        // remove from config
        const data = { users: admin.firestore.FieldValue.arrayRemove(before.userId) }
        await fst
            .collection('institutions')
            .doc(context.params.institutionId) // TODO: error? was "institutionId"??!!
            .update(data)
            .catch(error => console.log('update of institutions user array failed...', error));
    }

    const data = {
        delete: true, // this might trigger delete, but managed from the user agent function
        activated: false, //
        // we do not overwrite, need better solution here
        /*
        role: before.role,
        trigger: {
            institutionId: context.params.institutionId,
            initiatorId: before.initiatorId,
            type: 'institution',
        }
        */
    }

    // final execution
    // in case there was no proper user initialization
    if (before.userId) {
        return admin.database()
            .ref('users/' + before.userId)
            .update(data);
    } else {
        return 'account deleted';
    }
    
}

async function syncPeers(action, institutionId, userId, peerId) {
    console.log('data:', institutionId, userId, peerId);

    /**
     * 1. check whether peer connection does already exist
     * 2. if not, create user -> peer connections
     *  -> complementary peer will automatically be added
     */
    if (action === 'connect') {
        // primary and complementary peer connection
        return addPeerConnection(userId, peerId, institutionId);
    } else if (action === 'delete') {
        // delete, regardless of connected peers
        try {
            const result = await fst
            .collection('userPeers')
            .where('userId', '==', userId)
            .where('institutionId', '==', institutionId)
            .get(); 
            result.forEach(item => fst
                .collection('userPeers')
                .doc(item.id).delete());
            return true;
        }
        catch(e) { 
            console.log('syncPeers error: ', e) 
            return e;
        }
        
    } else {
        return false;
    }
}

async function addPeerConnection(userId, peerId, institutionId) {
    // 1. make sure that connection not already exists for userId
    const resultUser = await fst
        .collection('userPeers')
        .where('userId', '==', userId)
        .where('peerId', '==', peerId)
        .where('institutionId', '==', institutionId)
        .get()
        .catch(error => console.log('result error: ', error));
    // 
    if (resultUser.size > 0) {
        // console.log('result: ', resultUser.size);
        console.log('primary connection exists...');
    } else {
        const data = {
            institutionId: institutionId,
            userId: userId,
            peerId: peerId,
        }
        // 2. connect...
        await fst
            .collection('userPeers')
            .add(data);
    }
}

// final export
module.exports = institutionsAccountsUpdateAgent;

