/**
 * TODO: ...
 */

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

//log helper
const doLog = require('../helpers/doLog');

const kpis = async (context) => {
    // ...
    //console.log('context', JSON.stringify(context));
    const students = await getNumUsersWithRole(0);
    const coaches = await getNumUsersWithRole(1);
    const admins = await getNumUsersWithRole(2);
    const discSt = await getDisonnectedStudents();
    const disconnectedStudents = discSt.orphanedTotal;
    const disconnectedStudents30 = discSt.orphaned30;
    const disconnectedStudents7 = discSt.orphaned7;

    const institutionalStudents = await getNumUsersInstitutional(0);
    const institutionalCoaches = await getNumUsersInstitutional(1);
    const institutionalAdmins = await getNumUsersWithRole(3);
    //console.log('students, coaches, admins, institutionalAdmins: ', students, coaches, admins, institutionalAdmins);

    const activeStudents1 = await getActiveUsers(0, 1);
    const activeCoaches1 = await getActiveUsers(1, 1);
    const activeAdmins1 = await getActiveUsers(2, 1);
    const activeInstitutionalAdmins1 = await getActiveUsers(3, 1);
    //console.log('active students, coaches, admins, institutionalAdmins 1: ', activeStudents1, activeCoaches1, activeAdmins1, activeInstitutionalAdmins1);

    const activeStudents7 = await getActiveUsers(0, 7);
    const activeCoaches7 = await getActiveUsers(1, 7);
    const activeAdmins7 = await getActiveUsers(2, 7);
    const activeInstitutionalAdmins7 = await getActiveUsers(3, 7);
    //console.log('active students, coaches, admins, institutionalAdmins 7: ', activeStudents7, activeCoaches7, activeAdmins7, activeInstitutionalAdmins7);

    const activeStudents30 = await getActiveUsers(0, 30)
    const activeCoaches30 = await getActiveUsers(1, 30);
    const activeAdmins30 = await getActiveUsers(2, 30);
    const activeInstitutionalAdmins30 = await getActiveUsers(3, 30);
    //console.log('active students, coaches, admins, institutionalAdmins 30: ', activeStudents30, activeCoaches30, activeAdmins30, activeInstitutionalAdmins30);

    const sessions1 = await getSessions(1);
    const sessions1numSessions = sessions1.numSessions;
    const sessions1duration = sessions1.sessionsDuration;
    const sessions7 = await getSessions(7);
    const sessions7numSessions = sessions7.numSessions;
    const sessions7duration = sessions7.sessionsDuration;

    const logData = {
        timestamp: Date.now(),

        students: students,
        coaches: coaches,
        admins: admins,
        disconnectedStudents: disconnectedStudents,
        disconnectedStudents30: disconnectedStudents30,
        disconnectedStudents7: disconnectedStudents7,

        institutionalStudents: institutionalStudents,
        institutionalCoaches: institutionalCoaches,
        institutionalAdmins: institutionalAdmins,

        activeStudents1: activeStudents1,
        activeCoaches1: activeCoaches1,
        activeAdmins1: activeAdmins1,
        activeInstitutionalAdmins1: activeInstitutionalAdmins1,

        activeStudents7: activeStudents7,
        activeCoaches7: activeCoaches7,
        activeAdmins7: activeAdmins7,
        activeInstitutionalAdmins7: activeInstitutionalAdmins7,

        activeStudents30: activeStudents30,
        activeCoaches30: activeCoaches30,
        activeAdmins30: activeAdmins30,
        activeInstitutionalAdmins30: activeInstitutionalAdmins30,

        sessions1numSessions: sessions1numSessions,
        sessions1duration: sessions1duration,
        sessions7numSessions: sessions7numSessions,
        sessions7duration: sessions7duration,
    }
    // create a log entry
    await doLog('log-kpis', logData);
    const ts = Date.now().toString();
    // write to FST
    return fst
        .collection('kpis')
        .doc(ts)
        .set(logData);


// end of main class
}

async function getNumUsersWithRole(role) {
    const snap = await db
        .ref('users')
        .orderByChild('role')
        .equalTo(role)
        .once('value');
    return snap.numChildren();
}

async function getNumUsersInstitutional(role) {
    const snap = await db
        .ref('users')
        .orderByChild('trigger/institutionId')
        .startAt(0) // trick to filter for records including "trigger..."
        .once('value');
    const userArr = [];
    snap.forEach(val => {
        val.val().role === role ? userArr.push(val.key) : '';
    }); 
    return userArr.length;
}

async function getActiveUsers(role, periodBack) {
    // miliseconds representing the timeframe we look back for activities
    const threshold = Date.now() - 1000 * 60 * 60 * 24 * periodBack;
    const snap = await db
        .ref('users')
        .orderByChild('lst')
        .startAt(threshold)
        .once('value');
    // return snap.numChildren();
    const userArr = [];
    snap.forEach(val => {
        userArr.push({ role: val.val().role, userId: val.key });
    }); //{ role: val.val().role, userId: val.key }));
    // console.log('userArr: ', userArr.length);
    // makes the array unique
    const uniqueSet = new Set(userArr);
    // spreads the object back into an array
    const uniqueUserArr = [...uniqueSet];
    return uniqueUserArr.filter(item => item.role === role).length;
}

async function getDisonnectedStudents() {
    // miliseconds representing the timeframe we look back for activities
    const snap = await db
        .ref('coachCustomer')
        .once('value');
    const studentArr = [];
    snap.forEach(val => {
        Object
            .keys(val.val())
            .forEach((key) => {
                studentArr.push(key);
            });
    });
    // as there could be the same student assigned multipe times, deduplicate
    const uniqueSet = new Set(studentArr);
    // spreads the object back into an array
    const uniqueStudentArr = [...uniqueSet];

    // all students, role = 0
    const snap2 = await db
        .ref('users')
        .orderByChild('role')
        .equalTo(0)
        .once('value');
    const allStudentsArr = [];
    snap2.forEach(val => {
        allStudentsArr.push({ key: val.key, lst: val.val().lst});
    });
    // console.log('allStudentsArr before: ', allStudentsArr.length)
    // delete all matching keys from allStudentsArr, representing the students which are properly assigned to coaches
    // left over keys represent then orphaned students
    uniqueStudentArr.forEach(item => {
        const index = allStudentsArr.findIndex(_item => _item.key === item);
        index !== -1 ? allStudentsArr.splice(index,1) : '';
    })

    let periodBack = 30;
    let threshold = Date.now() - 1000 * 60 * 60 * 24 * periodBack;
    const orphaned30Arr = [];
    allStudentsArr.forEach(item => {
        item.lst > threshold ? orphaned30Arr.push(item.key) : '';
    })
    periodBack = 7;
    threshold = Date.now() - 1000 * 60 * 60 * 24 * periodBack;
    const orphaned7Arr = [];
    allStudentsArr.forEach(item => {
        item.lst > threshold ? orphaned7Arr.push(item.key) : '';
    })
    const data = {
        orphanedTotal: allStudentsArr.length,
        orphaned30: orphaned30Arr.length,
        orphaned7: orphaned7Arr.length,
    }

    // all students  - students collected within coaches
    // console.log(allStudentsArr.length, studentArr.length, uniqueStudentArr.length, allStudentsArr, studentArr, uniqueStudentArr);
    return data; // allStudentsArr.length;
}

/*
logs/logStream

duration
numberOfStreams
timestamp

per day:

timestamp start -> 0:00
duration / numberOfStreams > 5min = 1000 * 60 * 5

-> nr of sessions
-> ~ duration of sessions
*/

async function getSessions(periodBack){
    periodBack = periodBack || 1;
    let threshold = Date.now() - 1000 * 60 * 60 * 24 * periodBack;

    const snap2 = await db
        .ref('logs/logStream')
        .orderByChild('timestamp')
        .startAt(threshold)
        .once('value');
    const sessionsArr = [0];
    snap2.forEach(val => {
        const numStreams = val.val().numberOfStreams;
        const duration = val.val().duration;
        const relDuration = duration / numStreams;
        const _5min = 1000 * 60 * 5;
        numStreams > 1 && relDuration > _5min ? sessionsArr.push(relDuration) : '';
    });
    const sessionsDuration = sessionsArr.reduce((total,val) => total + val);
    const data = {
        numSessions: sessionsArr.length,
        sessionsDuration: Math.floor(sessionsDuration / 1000), // report in sec
    }
    return data;
}
    
// final export
module.exports = kpis;

