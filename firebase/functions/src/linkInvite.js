/**
 * TODO: this will enrich link invite
 */

// indirect ref of admin
const admin = require('./fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();

const linkInvite = async (snap, context) => {

    /*
    read data.uid = coach
    check if coach from institution
    if so, add institutionId
    parse nickname to -> <coach name> + (<Institution name>)
    */

   // console.log(JSON.stringify(snap), JSON.stringify(context));
   const data = snap.val();

    const coachId = data.uid;
    const coachData = await admin.database()
        .ref('/users/' + coachId).once('value');

    if(coachData.val().userActions === 'createdByInstitution') {
        const initiatorId = coachData.val().trigger.initiatorId;
        const nickname = data.nickname;
        return updateInvite(context.params.inviteId, initiatorId, nickname);
    } else {
        return true;
    }

};

async function updateInvite(inviteId, initiatorId, nickname) {

    const institution = await fst
        .collection('institutions')
        .doc(initiatorId)
        .get();

    const code = createCode(6);
    const data = {
        nickname: nickname + ' (' + institution.data().name + ')',
        initiatorId: initiatorId,
        userActions: 'createdByInstitution',
        code: code,
    }
    
    // Write the new post's data simultaneously in the posts list and the user's post list.
    var updates = {};
    updates['invites/' + inviteId] = data;

    // combined updates
    // await admin.database().ref().update(updates);

    return await admin.database().ref('invites/' + inviteId).update(data);

}

async function updateInviteCode() {
    const code = createCode(6);
    return await admin.database().ref('invites/' + inviteId).update({code: code});    
}

function createCode(count) {
    let code = '';
    for (let i = 0;  i < count; i++) {
        code += (Math.floor(Math.random() * 9)).toString();
    }
    return code;
}

// final export
module.exports = linkInvite;



