/**
 * TODO: ...
 * 
 * save asset metadata in optional both: firestore, realtime database
 */

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

const promisePool = require('es6-promise-pool');
const PromisePool = promisePool.PromisePool;
// const secureCompare = require('secure-compare');
// Maximum concurrent account requests.
const MAX_CONCURRENT = 10;

// saving data in realtime db
// custom function
async function migrationControlTable() {

    const fileArr = [];

    const sharedSnap = await db
        .ref('users')
        .once('value');
    sharedSnap.forEach(val => {
        // ...
        fileArr.push({ userId: val.key, role: val.val().role }); // val key is default
    });
    // console.log('fileArr: ', JSON.stringify(fileArr));
    console.log(`***number of users to create: `, fileArr.length);
    // firestore media/documents collection
    fst
        .collection('migrationControl')
        .doc('mediaUsers')
        .set({ totalUsers: fileArr.length }, { merge: true })
        .catch(error => console.log('result error: ', error)); 

    // promise pool functions must NOT be async!!
    const promisePool = new PromisePool(() => createDocuments(fileArr), MAX_CONCURRENT);
    return promisePool.start();

}

// synchronus!!!
function createDocuments(fileArr){
    // console.log(`number of shared docs to create: `, fileArr.length); 
    if (fileArr.length > 0) {
        // get first item and remove it from arr
        const itemRef = fileArr.shift();

        if(itemRef.userId !== null && itemRef.userId !== undefined) {
            const role = itemRef.role !== null && itemRef.role !== undefined ? itemRef.role : -2;
            const data = {
                userId: itemRef.userId,
                role: role,
                totalDocuments: 0,
                totalDocumentsMigrated: 0,
                totalSharedDocuments: 0,
                totalSharedDocumentsMigrated: 0,
                migrated: false,
            }
            // firestore media/documents collection
            return fst
                .collection('migrationControl/mediaUsers/users')
                //.add(data)
                .doc(itemRef.userId) // this ensure uiqueness of data even if we have to repeat steps
                .set(data)
                .catch(error => console.log('result error: ', error)); 
        } else {
            return 'no user created...';
        }

    } else {
        return null;
    }

}
   
// final export
module.exports = migrationControlTable;


