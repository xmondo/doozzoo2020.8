/**
 * TODO: ...
 */

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

const { Storage } = require('@google-cloud/storage');
const storage = new Storage();
const bucketName = functions.config().bucket.location; 
const bucket = storage.bucket(bucketName);

const syncRenameDocuments = async (change, context) => {

    // .document('media/{ownerId}/documents/{docId}/{filename}')

    // console.log('change', JSON.stringify(change));
    // console.log('context', JSON.stringify(context));
    change.after.data().filename !== change.before.data().filename ? syncSharedDocuments(change.after.data().name, change.after.data().filename, context.params.ownerId) : console.log('nothing changed');

// end of main class
}

/**
 * 
 * @param {*} name -> storage path in bucket
 * @param {*} ownerId 
 */
const syncSharedDocuments = async (name, newFilename, ownerId) => {
    console.log('syncSharedDocuments: ', name, newFilename, ownerId);
    // const fileArr = [];

    // update Metadata in bucket
    const updatedMetadata = {
        metadata: { 
            filename: newFilename,
        }
    }
    const updateResult = await bucket
        .file(name)
        .setMetadata(updatedMetadata)
        .catch(e => console.log('setMetadata error', e))
    // console.log('extendedMetadata: ', JSON.stringify(updateResult));

    // firestore media/documents collection
    const documentList = await fst
        .collection(`sharedMedia`)
        .where('ownerId', '==', ownerId)
        .where('storagePath', '==', name)
        .get()
        .catch(error => console.log('result error: ', error));
    console.log(`number of shared docs to change: `, documentList.size);

    return documentList.forEach(childSnapshot => {
        // console.log('syncRenameDocuments->childSnapshot: ', childSnapshot.val());
        childSnapshot
            .ref
            .update({
                // rename file
                filename: newFilename, 
            });
        
    });
/*
    documentList.forEach(val => {
        val.
        // updatedOn < now ? fileArr.push({ name: val.data().name, key: val.key }) : '';
        fileArr.push({ name: val.data().storagePath, key: val.id }); // val key is default
    });
    console.log('fileArr: ', JSON.stringify(fileArr));

    // work in a controlled promise pool
    // promise pool functions must NOT be async!!
    const promisePool = new PromisePool(() => renameSharedDocuments(fileArr, ownerId), MAX_CONCURRENT);
    return promisePool.start();

    // return 'bucket has been synced...';
*/
// end of main class
}

function renameSharedDocuments(fileArr, ownerId){

    if (fileArr.length > 0) {
        // get first item and remove it from arr
        const fileRefItem = fileArr.shift();

        return fst
           .collection(`media/${ownerId}/documents`)
           .where('name', '==', fileRefItem.name)
           .get()
           .then(snapshot => {
                // console.log(snapshot, snapshot.size)
                if(snapshot.size === 0) {
                    console.log('delete sharedMedia: ', fileRefItem.name);
                    return fst.collection(`sharedMedia`).doc(fileRefItem.key).delete();
                } else {
                    // console.log('sharedMedia snap: ', snapshot.size, snapshot.docs[0].data().downloadLink)
                    const downloadLink = snapshot.docs[0].data().downloadLink
                    // downloadLink = snapshot.data().downloadLink;
                    return fst
                        .collection(`sharedMedia`)
                        .doc(fileRefItem.key)
                        .update({ downloadLink: downloadLink });
                }
           })    

    } else {
        return null;
    }

}
    
// final export
module.exports = syncRenameDocuments;

