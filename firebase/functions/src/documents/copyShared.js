/**
 * TODO: ...
 * 
 * ...
 */

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

const promisePool = require('es6-promise-pool');
const PromisePool = promisePool.PromisePool;
// const secureCompare = require('secure-compare');
// Maximum concurrent account requests.
const MAX_CONCURRENT = 10;

// saving data in realtime db
// custom function
async function copyShared() {

    const fileArr = [];

    const sharedSnap = await db.ref(`sharedDocuments`)
        .once('value');

    sharedSnap.forEach(val => {
        // ...
        const data = val.val();
        data.storagePath = data.name; // convert for new DB in case ....
        data.id !== undefined && data.id !== null ? data.key = data.id : ''; // convert for new DB in case we still have ids
        data.size = parseInt(data.size); // take care that size is converted into a number type
        fileArr.push({ item: data }); // val key is default
    });
    // console.log('fileArr: ', JSON.stringify(fileArr));

    console.log(`***number of shared docs to create: `, fileArr.length);

    // promise pool functions must NOT be async!!
    const promisePool = new PromisePool(() => createDocuments(fileArr), MAX_CONCURRENT);
    return promisePool
        .start()
        .then(() => {
            // console.log(`Shares synced: ${msgList}`);
            console.log(`Shares synced...`);
            return true;
        })
        .catch((error) => {
            console.log('Shares synced errors, some promise rejected: ', error.message);
            return false;
        }); 

}

// synchronus!!!
function createDocuments(fileArr){
    // console.log(`number of shared docs to create: `, fileArr.length); 
    if (fileArr.length > 0) {
        // get first item and remove it from arr
        const itemRef = fileArr.shift();

        // firestore media/documents collection
        return fst
            .collection(`sharedMedia`)
            .add(itemRef.item)
            .catch(error => console.log('result error: ', error)); 

    } else {
        return null;
    }

}
   
// final export
module.exports = copyShared;


