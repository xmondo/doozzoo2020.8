/**
 * TODO: ...
 * 
 * check if object exists in fst media
 * if yes, fine
 * if not, read bucket metadata
 * create fst entry in documents collection
 */

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();

const { Storage } = require('@google-cloud/storage');
const storage = new Storage();
const bucketName = functions.config().bucket.location; 
const bucket = storage.bucket(bucketName);

const db = admin.database();
const uuid4 = require('uuid-v4');
const saveObjectData = require('./saveObjectData');

const promisePool = require('es6-promise-pool');
const PromisePool = promisePool.PromisePool;
const MAX_CONCURRENT = 10;

const _ = require('lodash');

const syncFromBucketAssets = async (ownerId, pageSize = 1000, pageToken = null, dryrun = null) => {
    console.log('*** ownerId, pagesize, pageToken, dryrun: ', ownerId, pageSize, pageToken, dryrun);

    const result = await bucket.getFiles({
        autoPaginate: false,
        maxResults: pageSize, // default = 1000
        delimiter: '/',
        prefix: `documents/${ownerId}/`,
        pageToken: pageToken,
    });
    const files = result[0];
    // const nextToken = result[1]

    // console.log('result 0: ', JSON.stringify(result[0]));
    // console.log('result 1: ', JSON.stringify(result[1]));
    console.log(JSON.stringify(_.map(files, file => file.name)));
    console.log(`nextPageToken: ${_.get(result[1], 'pageToken')}`);
    console.log(`bucket files number for ${ownerId}: ${files.length}`);

    if(dryrun !== null) {
        console.log('nothing executed, DRYRUN...')
        return false;
    }

    // transform data per bucket item  
    const filesArr = files.map(item => {

        // tags in metadata must be always stringified if beeing an array 
        const segments = item.name.split('/')
        const filename = segments[segments.length - 1];
        // console.log('filename->', filename);

        // enrich custom metadata
        item.metadata.metadata.ownerId = ownerId;
        item.metadata.metadata.filename = getFilename(item.name);
        item.metadata.metadata.tags = item.metadata.metadata.tags !== null && item.metadata.metadata.tags !== undefined ? item.metadata.metadata.tags : JSON.stringify(['default']);
        item.metadata.metadata.md5Hash = item.metadata.metadata.md5Hash !== null && item.metadata.metadata.md5Hash !== undefined ? item.metadata.metadata.md5Hash : uuid4();
           
        return {
            contentType: item.metadata.contentType,
            id: item.metadata.id,
            mediaLink: item.metadata.mediaLink,
            metadata: item.metadata.metadata,
            name: item.name,
            selfLink: item.metadata.selfLink,
            size: item.metadata.size,
            updated: item.metadata.updated,
        }

    });

    // console.log('filesArr: ', JSON.stringify(filesArr));
    console.log(`*** ready with ${filesArr.length} objects`);
    const total = filesArr.length;

    // work in a controlled promise pool
    // promise pool functions must NOT be async!!
    const promisePool = new PromisePool(() => syncDocuments(filesArr, ownerId), MAX_CONCURRENT);
    return promisePool
        .start()
        .then(() => {
            console.log(`*** bucket synced for ${ownerId} with ${total} items...`);
            console.log(`*** nextPageToken: ${_.get(result[1], 'pageToken')}`);
            // console.log('Promise fullfilled');
            // eslint-disable-next-line promise/no-nesting
            return fst
                .collection('migrationControl/mediaUsers/users')
                .doc(ownerId)
                .set({ step1done: true, migrated: true }, { merge: true })
                .catch(error => console.log('result error: ', error));
        })
        .catch((error) => {
            console.log('Some promise rejected: ', error.message);
            // return 'error: some promise rejected: ', error.message;
            // eslint-disable-next-line promise/no-nesting
            return fst
                .collection('migrationControl/mediaUsers/users')
                .doc(ownerId)
                .set({ step1done: false, migrated: false, error: error.message }, { merge: true })
                .catch(error => console.log('result error: ', error));
        });

}

function getFilename(path) {
    const arr = path.split('/')
    return arr[arr.length - 1];
}

function getOwnerFromName(path) {
    const arr = path.split('/')
    return arr[1];
}

// synchronus!!!
function syncDocuments(filesArr, ownerId) {
    if (filesArr.length > 0) {
        // get first item and remove it from arr
        const object = filesArr.shift();
        // console.log('object.name: ', object.name);

        // check whether an object with the same name already exists in index
        return fst
            .collection(`media/${ownerId}/documents`)
            .where('name', '==', object.name)
            .get()
            .then(result => {
                if (result.size > 0) {
                    console.log(`object ${object.name} already exists...`);
                    return `object ${object.name} already exists...`;
                } else {
                    console.log('object saved: ', object.name);
                    return saveObjectData(object); 
                }
            })
            .catch(error => console.log('result error: ', error));
        
    } else {
        // console.log(`I - bucket synced for ${ownerId} ...`);
        /*
        console.log(`bucket synced for ${ownerId} ...`);
        fst
            .collection('migrationControl/mediaUsers/users')
            .doc(ownerId)
            .set({ step1done: true, migrated: true }, { merge: true })
            .catch(error => console.log('result error: ', error));
        */
        return null;
    }
}
    
// final export
module.exports = syncFromBucketAssets;



/*
bucket
contentDisposition
contentType 
crc32c //?
downloadLink
etag
filename
generation
id
kind
md5Hash
mediaLink
metadata
metageneration
name
ownerId
selfLink
simpleContentType
size // !! number
storageClass
tags
timeCreated
timeStorageClassUpdated
updated
updatedOn
uuid

### mandatory ###

files.map(item => {
    contentType: item.contentType,
    id: item.id,
    mediaLink: item.mediaLink,
    metadata: item.metadata,
    name: item.name,
    selfLink: item.selfLink,
    size: item.size,
    tags: item.tags,
    updated: item.updated,
});

// native
contentType 
id
mediaLink
metadata
name
selfLink
size 
tags
updated

// generated
downloadLink
filename
ownerId
simpleContentType
updatedOn
md5Hash
uuid

*/

