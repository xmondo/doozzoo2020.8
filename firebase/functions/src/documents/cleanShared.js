/**
 * TODO: ...
 * 
 * ...
 */

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

const promisePool = require('es6-promise-pool');
const PromisePool = promisePool.PromisePool;
// const secureCompare = require('secure-compare');
// Maximum concurrent account requests.
const MAX_CONCURRENT = 10;

// saving data in realtime db
// custom function
async function cleanShared(ownerId) {

    const mediaSnap = fst
        .collection(`media/${ownerId}/documents`)
        .limit(1000)
        .get()

    const fileArr = [];
    (await mediaSnap).forEach(item => {
        data = {
            name: item.data().name,
            key: item.data().key,
        }
        fileArr.push(data);
    })

    console.log(`***number of docs to sync: `, fileArr.length);

    // promise pool functions must NOT be async!!
    const promisePool = new PromisePool(() => syncDocuments(fileArr, ownerId), MAX_CONCURRENT);
    return promisePool
        .start()
        .then(() => {
            // console.log(`Shares synced: ${msgList}`);
            console.log(`Shares cleaned...`);
            return true;
        })
        .catch((error) => {
            console.log('Shares cleaned errors, some promise rejected: ', error.message);
            return false;
        }); 

}

// synchronus!!!
function syncDocuments(fileArr, ownerId){
    // console.log(`number of shared docs to create: `, fileArr.length); 
    if (fileArr.length > 0) {
        // get first item and remove it from arr
        const itemRef = fileArr.shift();
        // console.log('toBeKey/Name: ', itemRef.key, itemRef.name);
        // firestore media/documents collection

        return fst
            .collection(`sharedMedia`)
            .where('name','==',itemRef.name)
            //.where('ownerId','==',ownerId)
            //.limit(50)
            .get()
            .then(result => {
                // console.log('children? ', result.size);
                return result
                    .forEach(item => {
                        console.log('*** toBeKey, asIsKey: ', itemRef.key, item.data().key);
                    })

            })
    } else {
        return null;
    }

}
   
// final export
module.exports = cleanShared;


