/**
 * TODO: ...
 */

/**
 * this functions sync the documents in the rdb table with the physical docs stored in the bucket
 * if a file is in db, but not in the bucket any more, the db entry will be deleted
 */

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
// const fst = admin.firestore();
const db = admin.database();
const fst = admin.firestore();

const { Storage } = require('@google-cloud/storage');
// Creates a storage client
const gcs = new Storage();
// read environment bucket location from config
// TODO: throws error -> gcs.bucket is not a function
const bucket = gcs.bucket(functions.config().bucket.location);

const promisePool = require('es6-promise-pool');
const PromisePool = promisePool.PromisePool;
// const secureCompare = require('secure-compare');
// Maximum concurrent account requests.
const MAX_CONCURRENT = 5;
// const syncFirestoreMedia = require('./syncFirestoreMedia');

const monitorDocuments = async (coachId) => {
    // const coachId = context.params.coachId; 

    const now = Date.now();// parseInt(Date.now() - (1000 * 60 * 60 * 1)); // should be older than 1h

    const fileArr = [];
    /*
    // legacy version real time database
    // fetch the list of documents of a coach		
    const documentList = await db
        .ref(`documents/${coachId}`)
        .once('value');
    console.log('documentList length: ', documentList.numChildren());
    documentList.forEach(val => {
        const updatedOn = parseInt(val.val().updatedOn);
        updatedOn < now ? fileArr.push({ name: val.val().name, key: val.key }) : '';
    });
    */

    // firestore media/documents collection
    const documentList = await fst
        .collection(`media/${coachId}/documents`)
        .get()
        .catch(error => console.log('result error: ', error));
    console.log(`number of collection media/${coachId}/documents: `, documentList.size);

    documentList.forEach(val => {
        // updatedOn < now ? fileArr.push({ name: val.data().name, key: val.key }) : '';
        fileArr.push({ name: val.data().name, key: val.id }); // val key is default
    });
    console.log('fileArr: ', JSON.stringify(fileArr));

    // work in a controlled promise pool
    // promise pool functions must NOT be async!!
    const promisePool = new PromisePool(() => syncDocuments(fileArr, coachId), MAX_CONCURRENT);
    return promisePool.start();

    // return 'bucket has been synced...';

// end of main class
}

// synchronus!!!
function syncDocuments(fileArr, coachId){

    if (fileArr.length > 0) {
        // get first item and remove it from arr
        const fileRefItem = fileArr.shift();

        // console.log('updatedOn: ', updatedOn, now);
        /** 
         * does the file exist?
         * if not, delete the database entry
         * if yes, do nothing
         */
        const fp = bucket.file(fileRefItem.name);
        return fp.exists()
            .then(status => {
                // console.log('status: ', status, status[0])
                if(status[0] === false){ 
                    // console.log('delete rdb key: ', fileRefItem.key);
                    console.log('delete: ', fileRefItem.name, fileRefItem.key);
                    return doDelete(fileRefItem.key, coachId);
                } else {
                    // console.log('keep -> ', fileRefItem.name);
                    return 'did nothing...';
                    // return syncFirestoreMedia(fileRefItem, coachId);
                }
            })
            .catch(error => console.log('fp error: ', error));     

    } else {
        return null;
    }

}

// helper function for deletion of document references
async function doDelete(key, coachId) {
    /*
    // legacy realtime db
    // delete object from "root/documents"
    await admin.database()
        .ref('/documents/' + coachId + '/' + uuid)
        .set(null);	

    const snapshot = await db.ref('sharedDocuments')
        .orderByChild('id')
        .equalTo(uuid)
        .once("value");
    */

    // migration to firestore
    // delete in main index
    console.log(`deleted: media/${coachId}/documents/${key}`);
	await fst.collection(`media/${coachId}/documents`).doc(key).delete();
    
    // delete references in shared media
    const snapshot = await fst
        .collection(`sharedMedia`)
        .where('key', '==', key)
        .get();

    // must also be executed in promise pool
    const snapKeyArr = [];
    snapshot.forEach(childSnapshot => {
        snapKeyArr.push(childSnapshot.id); // "key" for realtimedb, "id" for firestore
    });

    const promisePool = new PromisePool(() => cleanSharedDocs(snapKeyArr), MAX_CONCURRENT);
    return promisePool.start();

}

// ### sharedMedia ###

const monitorSharedDocuments = async (ownerId) => {
    // const coachId = context.params.coachId; 
    const fileArr = [];

    // firestore media/documents collection
    const documentList = await fst
        .collection(`sharedMedia`)
        .where('ownerId', '==', ownerId)
        .get()
        .catch(error => console.log('result error: ', error));
    console.log(`number of collection sharedMedia of ${ownerId}: `, documentList.size);

    documentList.forEach(val => {
        // updatedOn < now ? fileArr.push({ name: val.data().name, key: val.key }) : '';
        fileArr.push({ name: val.data().storagePath, key: val.id }); // val key is default
    });
    console.log('fileArr: ', JSON.stringify(fileArr));

    // work in a controlled promise pool
    // promise pool functions must NOT be async!!
    const promisePool = new PromisePool(() => syncSharedDocuments(fileArr, ownerId), MAX_CONCURRENT);
    return promisePool.start();

    // return 'bucket has been synced...';

// end of main class
}

// synchronus!!!
function syncSharedDocuments(fileArr, ownerId){

    if (fileArr.length > 0) {
        // get first item and remove it from arr
        const fileRefItem = fileArr.shift();

        /** 
         * does the file exist?
         * if not, delete the database entry
         * if yes, do nothing
         */
        /*
        const fp = bucket.file(fileRefItem.name);
        return fp.exists()
            .then(status => {
                // console.log('status: ', status, status[0])
                if(status[0] === false){ 
                    // console.log('delete rdb key: ', fileRefItem.key);
                    console.log('delete sharedMedia: ', fileRefItem.name);
                    return fst.collection(`sharedMedia`).doc(fileRefItem.key).delete();
                } else {
                    // console.log('keep -> ', fileRefItem.name);
                    return 'did nothing...';
                    // return syncFirestoreMedia(fileRefItem, coachId);
                }
            })
            .catch(error => console.log('fp error: ', error)); 
        */
        if(fileRefItem.name) {
            return fst
                .collection(`media/${ownerId}/documents`)
                .where('name', '==', fileRefItem.name)
                .get()
                .then(snapshot => {
                        // console.log(snapshot, snapshot.size)
                        if(snapshot.size === 0) {
                            console.log('delete sharedMedia: ', fileRefItem.name);
                            return fst.collection(`sharedMedia`).doc(fileRefItem.key).delete();
                        } else {
                            // console.log('sharedMedia snap: ', snapshot.size, snapshot.docs[0].data().downloadLink)
                            const downloadLink = snapshot.docs[0].data().downloadLink
                            // downloadLink = snapshot.data().downloadLink;
                            return fst
                                .collection(`sharedMedia`)
                                .doc(fileRefItem.key)
                                .update({ downloadLink: downloadLink });
                        }
                });

        } else {
            return 'no storage path...'
        }
        
    } else {
        return null;
    }

}

function cleanSharedDocs(snapKeyArr) {
    if (snapKeyArr.length > 0) {
        // get first item and remove it from arr
        const key = snapKeyArr.shift();
        console.log('delete sharedMedia key: ', key);
        /*
        // legacy
        return db.ref(`sharedDocuments/${key}`).set(null);
        */
        return fst.collection(`sharedMedia`).doc(key).delete();
    } else {
        return null;
    }
}
    
// final export
module.exports = {
    monitorDocuments,
    monitorSharedDocuments
};

/*
// object -->

{ kind: 'storage#object',
  resourceState: 'exists', // not_exists
  id: 'project-6816589442602044124.appspot.com/documents/auth0|582c7e4cedb8c73048379aad/Picture (Device Independent Bitmap).jpg/1514670914134262',
  selfLink: 'https://www.googleapis.com/storage/v1/b/project-6816589442602044124.appspot.com/o/documents%2Fauth0%7C582c7e4cedb8c73048379aad%2FPicture%20(Device%20Independent%20Bitmap).jpg',
  name: 'documents/auth0|582c7e4cedb8c73048379aad/Picture (Device Independent Bitmap).jpg',
  bucket: 'project-6816589442602044124.appspot.com',
  generation: '1514670914134262',
  metageneration: '1',
  contentType: 'image/jpeg',
  timeCreated: '2017-12-30T21:55:14.077Z',
  updated: '2017-12-30T21:55:14.077Z',
  storageClass: 'STANDARD',
  size: '62314',
  md5Hash: 'Y2EwZDc5ODA0ZTFkNWViY2EzMmZkYTlmZTZhZDIwYWU=',
  mediaLink: 'https://www.googleapis.com/download/storage/v1/b/project-6816589442602044124.appspot.com/o/documents%2Fauth0%7C582c7e4cedb8c73048379aad%2FPicture%20(Device%20Independent%20Bitmap).jpg?generation=1514670914134262&alt=media',
  contentDisposition: 'inline; filename*=utf-8\'\'Picture%20%28Device%20Independent%20Bitmap%29.jpg',
  metadata: 
   { ownerId: 'auth0|582c7e4cedb8c73048379aad',
   	 filename: 'ui_ux_transparent.pdf',
     firebaseStorageDownloadTokens: 'd9f9cae2-7b42-43fa-934c-69f697a9eccf' },
  crc32c: 'xYJ+NA==' }

*/

// [END upload documents monitoring]

