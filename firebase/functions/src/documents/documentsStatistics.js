/**
 * TODO: ...
 */

/**
 * get tag literal
 * query local documents by "in" array
 * check if array length after < array before
 * remove literal from all matching documents
 */

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

const documentsStatistics = async (snap, context) => {
    // path -> .document('media/{docId}/documents/{assetId}')
    // console.log('context: ', JSON.stringify(context))
    const docId = context.params.docId;
    const assetId = context.params.assetId;

    // const modifiedAsset = snap.data();
    // const deletedTagKey = snap.key();
    // console.log('deleted item: ', tagId, JSON.stringify(deletedTag));

    const snapCount = await fst
        .collection('media')
        .doc(docId)
        .collection('documents')
        .get()

    const totalCount = snapCount.size;
    let totalFilesize = 0;
    snapCount.forEach(item => totalFilesize += parseInt(item.data().size)); //.flatMap(item => item.size); //.reduce(a,b => a + b)
    
    console.log('stats: ', totalCount, totalFilesize);

    return fst
        .collection('media')
        .doc(docId)
        .set(
            {
                totalCount: totalCount,
                totalFilesize: totalFilesize
            }, 
            { 
                merge: true 
            }
        );

}

async function totalSharedFromYou(ownerId) {
    const snap = await fst
        .collection(`sharedMedia`)
        .where('ownerId', '==', ownerId)
        .get()
    const totalCount = snap.size;

    return fst
        .collection(`media`)
        .doc(ownerId)
        .set(
            {
                totalSharedFromYou: totalCount
            }, 
            { 
                merge: true 
            }
        );

}

async function totalSharedWidthYou(ownerId) {
    const snap = await fst
        .collection(`sharedMedia`)
        .where('recipientId', '==', ownerId)
        .get()
    const totalCount = snap.size;

    return fst
        .collection(`media`)
        .doc(ownerId)
        .set(
            {
                totalSharedWithYou: totalCount
            }, 
            { 
                merge: true 
            }
        );

}
    
// final export
module.exports = {
    documentsStatistics,
    totalSharedFromYou,
    totalSharedWidthYou,
};


