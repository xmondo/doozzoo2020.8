/**
 * TODO: ...
 * 
 * check if object exists in fst media
 * if yes, fine
 * if not, read bucket metadata
 * create fst entry in media/documents collection
 */

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();

// storage
// const storage = admin.storage();
// const bucket = admin.storage().bucket(fileBucket);
// const { Storage } = require('@google-cloud/storage');

const { Storage } = require('@google-cloud/storage');
const storage = new Storage();
const bucketName = functions.config().bucket.location; 
const bucket = storage.bucket(bucketName);

const db = admin.database();
const UUID = require('uuid-v4');
const contentTypeTransform = require('./contentTypeTransform');

const syncFirestoreMedia = async (data, coachId) => {
    console.log('object: ', JSON.stringify(data));
    // data -> {"name":"documents/2X1ZM44k8lXX6t2w1X17fx76DPK2/lesson-recording_01.10.2020_10:08.mp4","key":"xw2F+SVCVfRPLbOradVt8Q=="} 
    
    const status = (await fst.collection(`media/${coachId}/documents`).doc(data.key).get()).exists;
    console.log('exists? ', data.key, status);

    if(!status) {
        // Get metadata properties
        const metadata = await bucket.file(data.name).getMetadata();
        console.log('metadata', JSON.stringify(metadata))
        // somehow metadata is wrapped as an array item...
        return saveData(metadata[0], data.key);
    } else {
        return 'nothing to sync...';
    }

// end of main class
}

// saving data in realtime db
// custom function
async function saveData(object, uuid) {

    // this does not work
    //const downloadLink = `https://firebasestorage.googleapis.com/v0/b/${bucketName}/o/${object.metadata.filename}?alt=media&token=${object.metadata.firebaseStorageDownloadTokens}`;
    const downloadLink = 'https://firebasestorage.googleapis.com/v0/b/' + object.mediaLink.slice(49) + '&token=' + object.metadata.firebaseStorageDownloadTokens;

    // console.log('downloadLink: ', downloadLink);

    // change size to type "number"
    object.size = parseInt(object.size);

    // now pimp the object metadata for more convenience
    object.downloadLink = downloadLink;
    object.filename = object.metadata.filename;
    object.ownerId = object.metadata.ownerId;
    object.simpleContentType = await contentTypeTransform(object.contentType);
    object.timeCreated = object.timeCreated !== null && object.timeCreated !== undefined ? object.timeCreated : admin.firestore.Timestamp.now();
    object.updatedOn = object.updatedOn !== null && object.updatedOn !== undefined ? object.updatedOn : Date.now(); // admin.firestore.Timestamp.now();
    object.md5Hash = uuid;
    object.uuid = uuid;

    // create tags on rootlevel -> fst migration
    // console.log('object.metadata.tags: ', object.metadata.tags);
    object.tags = (object.metadata.tags !== null && object.metadata.tags !== undefined) ?  JSON.parse(object.metadata.tags) : [ 'default' ];
    object.metadata.assetCategory === 'lessonArchive' ? object.tags.push('lesson_archive') : '';

    return fst
        .doc(`media/${object.metadata.ownerId}`)
        .collection('documents')
        .doc(uuid)
        .set(object);  
}
    
// final export
module.exports = syncFirestoreMedia;

