/**
 * TODO: ...
 * 
 * save asset metadata in optional both: firestore, realtime database
 */

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

const promisePool = require('es6-promise-pool');
const PromisePool = promisePool.PromisePool;
// const secureCompare = require('secure-compare');
// Maximum concurrent account requests.
const MAX_CONCURRENT = 10;

// saving data in realtime db
// custom function
async function getMigrationStats() {

    const migList = await fst
        .collection('migrationControl/mediaUsers/users')
        .get()
        .catch(error => console.log('result error: ', error));

    const userArr = [];
    migList.forEach(val => {
        // ...
        userArr.push({ userId: val.data().userId }); // val key is default
    });
    // console.log('userArr: ', JSON.stringify(userArr));
    console.log(`*** number of migration stats to create: `, userArr.length);

    // promise pool functions must NOT be async!!
    const promisePool = new PromisePool(() => createStats(userArr), MAX_CONCURRENT)
    return promisePool.start();

}

// synchronus!!!
function createStats(userArr){
    // console.log(`number of shared docs to create: `, fileArr.length); 
    if (userArr.length > 0) {
        // get first item and remove it from arr
        const itemRef = userArr.shift();
        
        db.ref(`documents/${itemRef.userId}`)
            .once('value')
            .then(val => {
                const totalCount = val.numChildren();
                // eslint-disable-next-line promise/no-nesting
                return fst
                    .collection('migrationControl/mediaUsers/users')
                    .doc(itemRef.userId)
                    .set({ totalDocuments: totalCount }, { merge: true });
            })
            .catch(error => console.log('result error: ', error));

        return db
            .ref(`sharedDocuments`)
            .orderByChild('ownerId')
            .equalTo(itemRef.userId)
            .once('value')
            .then(val => {
                const totalCountShared = val.numChildren();
                // eslint-disable-next-line promise/no-nesting
                return fst
                    .collection('migrationControl/mediaUsers/users')
                    .doc(itemRef.userId)
                    .set({ totalSharedDocuments: totalCountShared }, { merge: true });
            })
            .catch(error => console.log('result error: ', error));      

    } else {
        return null;
    }

}
   
// final export
module.exports = getMigrationStats;


