/**
 * TODO: ...
 */

/**
 * get tag literal
 * query local documents by "in" array
 * check if array length after < array before
 * remove literal from all matching documents
 */

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

const documentsDeleteTag = async (snap, context) => {
    // path -> .document('media/{docId}/tags/{tagId}')
    // console.log('context: ', JSON.stringify(context))
    const docId = context.params.docId;
    const tagId = context.params.tagId;

    const deletedTag = snap.data();
    // const deletedTagKey = snap.key();
    // console.log('deleted item: ', tagId, JSON.stringify(deletedTag));

    const matchingSnap = await fst
        .collection('media')
        .doc(docId)
        .collection('documents')
        .where('tags', 'array-contains', tagId)
        .get();

    return matchingSnap
        .forEach(item => {
            console.log('matchingSnap->forEach: ', item.id, item.data().filename);
            item.ref.update({ tags: admin.firestore.FieldValue.arrayRemove(tagId) }); // short fst can not be used here!!!
        });
}
    
// final export
module.exports = documentsDeleteTag;


