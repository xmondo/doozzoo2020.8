/**
 * TODO: ...
 * 
 * save asset metadata in optional both: firestore, realtime database
 */

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

const { Storage } = require('@google-cloud/storage');
const storage = new Storage();
const bucketName = functions.config().bucket.location; 
const bucket = storage.bucket(bucketName);

const contentTypeTransform = require('./contentTypeTransform');
const uuid4 = require('uuid-v4'); // const uuid = uuid4();

// saving data in realtime db
// custom function
async function saveObjectData(object) {

    // console.log('object: ', object);
    // legacy campatibility
    // new files get a uuid assigned into custom Metadata during upload
    // if this is available, it will be used for storing the meta document object in db
    // if not available the native Hash will be used, this garanties compatibility with old version
    let uuid = uuid4();
    // console.log('***uuid: ', uuid)
    try {
        // if object.metadata && object.metadata.md5Hash -> uuid was set, most likely upload via FB, lets use it
        if (object.metadata) {
            if (object.metadata.md5Hash && object.metadata.md5Hash.length > 5) {
                uuid = object.metadata.md5Hash;
            }
        }
    }
    catch(e) { 
        console.log('legacy uuid parsing error: ', e);
    }

    // this does not work
    //const downloadLink = `https://firebasestorage.googleapis.com/v0/b/${bucketName}/o/${object.metadata.filename}?alt=media&token=${object.metadata.firebaseStorageDownloadTokens}`;
    const uri = encodeURIComponent(object.name);
    const downloadLink = 'https://firebasestorage.googleapis.com/v0/b/' + bucketName + '/o/' + uri + '?alt=media&token=' + object.metadata.firebaseStorageDownloadTokens;

    // change size to type "number"
    object.size = parseInt(object.size);

    // now pimp the object metadata for more convenience
    object.downloadLink = downloadLink;

    object.filename = object.metadata.filename !== null && object.metadata.filename !== undefined ? object.metadata.filename : getFilename(object.name);
    object.metadata.filename = object.filename;

    object.ownerId = object.metadata.ownerId !== null && object.metadata.ownerId !== undefined ? object.metadata.ownerId : getOwnerFromName(object.name);
    object.metadata.ownerId = object.ownerId;

    object.simpleContentType = await contentTypeTransform(object.contentType);
    // unpack/parse stringified tags array before storing into index
    // simple approach for the core timestamp
    object.updatedOn = Date.now(); // admin.firestore.Timestamp.now();
    object.timeCreated = admin.firestore.Timestamp.now();

    object.md5Hash = uuid;
    object.metadata.md5Hash = uuid;
    object.metadata.uuid = uuid;
    
    object.key = uuid;

    // create tags on rootlevel -> fst migration
    // console.log('object.metadata.tags: ', object.metadata.tags);
    object.tags = (object.metadata.tags !== null && object.metadata.tags !== undefined) ?  JSON.parse(object.metadata.tags) : ['default']; 
    object.metadata.assetCategory === 'lessonArchive' ? object.tags.push('lesson_archive') : '';

    // in case this data is missing, update extended metadata in bucket/object
    const extendedMetadata = {
        metadata: { 
            filename: object.filename,
            ownerId: object.ownerId,
            tags: JSON.stringify(object.tags), // if saved her must be stringified...
            md5Hash: uuid, // legacy shiuld be replaced all along the asset lifecycle
            uuid: uuid,
        }
    }
    const updateResult = await bucket
        .file(object.name)
        .setMetadata(extendedMetadata)
        .catch(e => console.log('setMetadata error', e))
    // console.log('extendedMetadata: ', JSON.stringify(updateResult));

    
    // save the modified object
    return saveData(object, uuid);
}

// saving data in realtime db / firestore
// by whatever reason I can not use the enrichted object.metadata... 
// need to use first level child attributes 
// custom function
async function saveData(object, uuid) {
    // fst migration
    // console.log('saveData->object: ', JSON.stringify(object))

     // check whether an object with the same name already exists in index
     const duplicatesCheck = await fst
        .collection(`media/${object.ownerId}/documents`)
        .where('name', '==', object.name)
        .get()
        .catch(error => console.log('result error: ', error));

    if(duplicatesCheck.size > 0) {
        console.log(`object ${object.filename} already exists...`);
        return `object ${object.filename} already exists...`;
    } else {
        console.log('object saved: ', object.metadata.filename);
        // legacy db
        /*
        await db.ref(`/documents/${object.ownerId}/${uuid}`)
            .update(object);
        */
       
        return fst
            .doc(`media/${object.ownerId}`)
            .collection('documents')
            .doc(uuid)
            .set(object);  
    }  
}

function getFilename(path) {
    const arr = path.split('/')
    return arr[arr.length - 1];
}

function getOwnerFromName(path) {
    const arr = path.split('/')
    return arr[1];
}
    
// final export
module.exports = saveObjectData;



/*
bucket
contentDisposition
contentType 
crc32c //?
downloadLink
etag
filename
generation
id
kind
md5Hash
mediaLink
metadata
metageneration
name
ownerId
selfLink
simpleContentType
size // !! number
storageClass
tags
timeCreated
timeStorageClassUpdated
updated
updatedOn
uuid

### mandatory ###

files.map(item => {
    contentType: item.contentType,
    id: item.id,
    mediaLink: item.mediaLink,
    metadata: item.metadata,
    name: item.name,
    selfLink: item.selfLink,
    size: item.size,
    tags: item.tags,
    updated: item.updated,
});

// native
contentType 
id
mediaLink
metadata
name
selfLink
size 
tags
updated

// generated
downloadLink
filename
ownerId
simpleContentType
updatedOn
md5Hash
uuid

*/

