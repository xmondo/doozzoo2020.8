/**
 * TODO: ...
 */

// indirect ref of admin
const admin = require('../fooAdmin');
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const fst = admin.firestore();
const db = admin.database();

const deleteDocuments = async (object) => {
    console.log('delete object: ', object.name);

	// legacy campatibility
	// new files get a uuid assigned into custom Metadata during upload
	// if this is available, it will be used for storing the meta document object in db
	// if not available the native Hash will be used, this guaraties compatibility with old version
	let uuid;
	if (object.metadata.uuid) {
		uuid = object.metadata.uuid;
	} else if (object.metadata.md5Hash) {
		uuid = object.metadata.md5Hash;
	} else {
		let str = object.md5Hash;
		if(str.indexOf('/') !== -1){  			
			uuid = str.replace(/\//g, "-");
		} else {
			uuid = str;
		}
	}

	const ownerId = object.metadata.ownerId !== null && object.metadata.ownerId !== undefined ? object.metadata.ownerId : getOwnerFromName(object.name);

	// delete object from "root/documents"
	console.log(`delete: /documents/${ownerId}/${uuid}`);
	db.ref(`documents/${ownerId}/${uuid}`).set(null);	
	
	// migration to firestore
	fst.collection(`media/${ownerId}/documents`).doc(uuid).delete();

	// firestore media/documents collection
    const documentList = await fst
        .collection(`sharedMedia`)
        .where('ownerId', '==', ownerId)
        .where('storagePath', '==', object.name)
        .get()
        .catch(error => console.log('result error: ', error));
    console.log(`number of shared docs to change: `, documentList.size);

    return documentList.forEach(childSnapshot => {
        // console.log('delete sharedDocuments->childSnapshot: ', childSnapshot.val());
        childSnapshot
            .ref
            .delete()
			.catch(error => console.log('result error: ', error));
        
    });

// end of main class
}

function getOwnerFromName(path) {
    const arr = path.split('/')
    return arr[1];
}
    
// final export
module.exports = deleteDocuments;

