/**
 * TODO: ...
 * 
 * Module takes standard mimetypes and simplyfies them to the first segment.
 * Example "audio/mp3" transformed to "audio"
 * transforms incoming mimetype to a simple string
 * https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types
 */

const contentTypeTransform = async (contentType) => {
    // console.log('#####', ct)
    const str = contentType.split('/');
    if (contentType === 'application/pdf') {
      str[0] = 'pdf';
    }  
    
    return str[0];
}
    
// final export
module.exports = contentTypeTransform;

