/**
 * Copyright 2017 Doozzoo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

// [START all]

// [START import]
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
// for sending smtp 
const nodemailer = require('nodemailer');

// The Firebase Admin SDK to access the Firebase Realtime Database. 
// const admin = require('firebase-admin');
const admin = require('./src/fooAdmin');

// new config v.1.0.0
admin.initializeApp();

// opentok lib
var OpenTok = require('opentok');


// Stripe
/*
firebase functions:config:set stripe.currency='EUR'
firebase functions:config:set stripe.token='sk_test_g30DPHz37Wx6EuH9y0YE1Dam' // test key secret
!!! for prod/stage/dev proper tokens have to be set...
*/
// const stripeToken = functions.config().stripe.token;
const stripe = require('stripe')('sk_test_g30DPHz37Wx6EuH9y0YE1Dam');
const currency = functions.config().stripe.currency || 'EUR';

//set config
//firebase functions:config:set ot.key="THE API KEY" ot.secret ="THE CLIENT ID"
//unset config
//firebase functions:config:unset ot.key
//get config
// firebase functions:config:get

/* 
### production
firebase functions:config:set ot.key='46059892'
firebase functions:config:set ot.secret='acfe89198b029f961290efcccb6b082427f1e688'
firebase functions:config:set bucket.location='doozzoo-production.appspot.com'
firebase functions:config:set now.limit=900 // 15min.

### production | eve
firebase functions:config:set ot.key='46059892'
firebase functions:config:set ot.secret='acfe89198b029f961290efcccb6b082427f1e688'
firebase functions:config:set bucket.location='doozzoo-eve.appspot.com'
firebase functions:config:set cron.key='3d95702d45dcd4efc19543447a5088397df956a2'
firebase functions:config:set now.limit=900 // 15min.
firebase functions:config:set now.tokenlimit=43200 // 1800 -> 30min. // 43.200 -> 12h
firebase functions:config:set stripe.currency='EUR'
firebase functions:config:set stripe.token='sk_test_g30DPHz37Wx6EuH9y0YE1Dam' // test key secret
firebase functions:config:set stripe.priceid='price_1HYp26BJ4tJWi6xWP1CCnlEB' // PRO priceId
firebase functions:config:set stripe.archivingpriceid='price_1Gtx5pBJ4tJWi6xWNnQaPKP0' // archivingPriceId
firebase functions:config:set stripe.webhooksecret='whsec_Dn6wMCndCiusEmAx9GRgifluwF6375Y6'
firebase functions:config:set email.domain='doozzoo.com'

### staging
firebase functions:config:set ot.key='45743172'
firebase functions:config:set ot.secret='45e387cdcb9858583b33b07bb306c7c01052f50a'
firebase functions:config:set bucket.location='project-6816589442602044124.appspot.com'
firebase functions:config:set cron.key='3d95702d45dcd4efc19543447a5088397df956a2'
firebase functions:config:set now.limit=1800 // 30min.
firebase functions:config:set now.tokenlimit=900 // 1800 -> 30min. // 43.200 -> 12h
// firebase functions:config:set backup.bucket='gs://doozzoo-dev-firestore-backups'
firebase functions:config:set stripe.priceid='price_1HYp26BJ4tJWi6xWP1CCnlEB' // PRO priceId
firebase functions:config:set stripe.archivingpriceid='price_1HYp2KBJ4tJWi6xW5zRJHgBs' // archivingPriceId
firebase functions:config:set stripe.webhooksecret='whsec_Dn6wMCndCiusEmAx9GRgifluwF6375Y6' // same like on Prod
firebase functions:config:set email.domain='staging.doozzoo.com'

### dev
firebase functions:config:set ot.key='46105142'
firebase functions:config:set ot.secret='869b869135d41f8dc7ae198fa874e1d4481a71ec'
firebase functions:config:set bucket.location='doozzoo-dev.appspot.com'
firebase functions:config:set cron.key='3d95702d45dcd4efc19543447a5088397df956a2'
firebase functions:config:set now.limit=86400 // 24h
firebase functions:config:set now.tokenlimit=43200 // 1800 -> 30min. // 43.200 -> 12h
firebase functions:config:set backup.bucket='gs://doozzoo-dev-firestore-backups'
firebase functions:config:set stripe.priceid='price_1H08qOBJ4tJWi6xWFg4d59Tw' // PRO priceId
firebase functions:config:set stripe.archivingpriceid='price_1H07fOBJ4tJWi6xWCRo2xKRs' // archivingPriceId
firebase functions:config:set stripe.webhooksecret='whsec_Ee9SZ35y7BnUj5ZlLgqtO27DmOWQb9sT' 
firebase functions:config:set email.domain='dev.doozzoo.com'
*/

// ### staging & dev ###
//const API_KEY = '45743172';
//const API_SECRET = '45e387cdcb9858583b33b07bb306c7c01052f50a';
const API_KEY = functions.config().ot.key;
const API_SECRET = functions.config().ot.secret;
const BUCKET_LOCATION = functions.config().bucket.location;

// ### production ###
//const API_KEY = '46059892';
//const API_SECRET = 'acfe89198b029f961290efcccb6b082427f1e688';

// ### handling google cloud storage functions ###
const gcs = require('@google-cloud/storage');

// CORS Express middleware to enable CORS Requests.
const cors = require('cors')({origin: true});

/* ####### TODO: NEW structure to be migrated ######## */

/* helper for emulators */
exports.trigger = functions
	.region('europe-west1')
	.database.ref('/@trigger')
	.onWrite((snap, context) => { 
		console.log('trigger helper running...');
		return workshopRoomRefresh(context);	
	});

// url -> https://europe-west1-doozzoo-dev.cloudfunctions.net/workshopRoomRefreshHttp?key=3d95702d45dcd4efc19543447a5088397df956a2
// url ->  https://europe-west1-doozzoo-eve.cloudfunctions.net/workshopRoomRefreshHttp?key=3d95702d45dcd4efc19543447a5088397df956a2
exports.workshopRoomRefreshHttp = functions
	.region('europe-west1')
	.https
	.onRequest(async (req, res) => {

	const key = req.query.key;
	// Exit if the keys don't match.
	if (!secureCompare(key, functions.config().cron.key)) {
		console.log('The key provided in the request does not match the key set in the environment. Check that', key, 'matches the cron.key attribute in `firebase env:get`');
		res.status(403).send('Security key does not match. Make sure your "key" URL query parameter matches the ' + 'cron.key environment variable.');
		return null;
	}
  	await workshopRoomRefresh();

	console.log('workshop room refresh finished');
	// !!! needed to terminate request
	res.send('workshop room refresh finished');	
	return null;
});

// refresh workshop rooms
const workshopRoomRefresh = require('./src/room/workshopRoomRefresh');
exports.workshopRoomRefresh = functions
	.pubsub
	// .schedule('15 0 * * *')
	.schedule('* * * * *')
	.timeZone('Europe/Berlin') // ...
	.onRun((context) => {
		console.log('run every 10min', context);
		return 'hello world 10min.';
		// return workshopRoomRefresh(context);
	});

// backoffice functions
const verifyUserEmail = require('./src/backoffice/verifyUserEmail');
exports.verifyUserEmail = functions
	.region('europe-west1')
	.https.onRequest(verifyUserEmail);
// after an document has been uploaded
/*
const syncFromBucketAssets = require('./src/documents/syncFromBucketAssets');
exports.syncFromBucketAssets = functions
	.region('europe-west1')
	.storage
	.object()
	.onFinalize(object => {
		return syncFromBucketAssets(object);
	});
*/
/**
 * update statistics, when media is added or deleted or modified
 */
const documentsStatistics = require('./src/documents/documentsStatistics').documentsStatistics;
exports.documentsStatistics = functions
	.region('europe-west1')
	.firestore
  	.document('media/{docId}/documents/{assetId}')
  	.onWrite((snap, context) => { 
		  /* ... */ 
		  return documentsStatistics(snap, context);
	});

const totalSharedFromYou = require('./src/documents/documentsStatistics').totalSharedFromYou;
const totalSharedWidthYou = require('./src/documents/documentsStatistics').totalSharedWidthYou;
exports.sharedDocumentsStatistics = functions
	.region('europe-west1')
	.firestore
		.document('sharedMedia/{docId}')
		.onWrite(async (snap, context) => { 
			/* ... */ 
			const ownerId = snap.after.exists ? snap.after.data().ownerId : snap.before.data().ownerId;
			console.log('sharedDocumentsStatistics', ownerId);
			await totalSharedFromYou(ownerId);
			const recipientId = snap.after.exists ? snap.after.data().recipientId : snap.before.data().recipientId;
			return totalSharedWidthYou(recipientId);
	});

/**
 * update document tags, when tag is deleted or modified
 */
const documentsDeleteTag = require('./src/documents/documentsDeleteTag');
exports.documentsDeleteTag = functions
	.region('europe-west1')
	.firestore
  	.document('media/{docId}/tags/{tagId}')
  	.onDelete((change, context) => { 
		  /* ... */ 
		  return documentsDeleteTag(change, context);
	});

/* TODO: if a new participant record is created, this checks whether the user is in the coachCustomer list and set to true */
const checkRegistrationStatus = require('./src/coachCustomer/isUserInCoachCustomer').checkRegistrationStatus;
exports.checkRegistrationStatus = functions
	.region('europe-west1')
	.database.ref('/classrooms/{coachId}/participants/{userId}/registrationStatus')
	.onUpdate((snap, context) => { 
		return checkRegistrationStatus(snap, context);	
	});

/* TODO: if a new participant record is created, this checks the participantQuota status, if its a limited plan */
const participantQuota = require('./src/coachCustomer/participantQuota');
exports.participantQuota = functions
	.region('europe-west1')
	.database.ref('classrooms/{coachId}/participants')
	.onUpdate((snap, context) => { 
		return participantQuota(snap, context);	
	});

// takes care of creating kpis regularyly
const cronCopyArchives = require('./src/archiving/cronCopyArchives');
exports.cronCopyArchives = functions
	.region('europe-west1')
	.pubsub
	.schedule('every 60 mins')
	.onRun((context) => {
		return cronCopyArchives(context);
	});

const myroomsCountDown = require('./src/room/myroomsCountDown')
exports.cronMinuteMan = functions
	.region('europe-west1')
	.pubsub
	.schedule('every 1 mins')
	.onRun((context) => {
		const msg = 'tick tick, MinuteMan...' + Date.now();
		console.log(msg);
		return myroomsCountDown(context);
	});

// after an document has been uploaded
const documents = require('./src/documents/saveObjectData');
exports.documents = functions
	.region('europe-west1')
	.storage
	.object()
	.onFinalize(object => {
		return documents(object);
	});
// cleanup after an document has been uploaded	
const deleteDocuments = require('./src/documents/deleteDocuments');
exports.deleteDocuments = functions
	.region('europe-west1')
	.storage
	.object()
	.onDelete(object => {
		return deleteDocuments(object);
	});
/*
// monitor and sync documents
const monitorDocuments = require('./src/documents/monitorDocuments');
exports.monitorDocuments = functions
	.region('europe-west1')
	.database.ref('/documents/{coachId}/{docId}')
	.onCreate((snap, context) => { 
		return monitorDocuments(context.params.coachId);
	});
*/
// [START rename files]
// Input of function: changes in documents table -> filename updated
// Output of function: 
//	- sync changes in documents table with the regarding shared documents
// IMOPORTANT: filenames are not touched in the storage object
const syncRenameDocuments = require('./src/documents/syncRenameDocuments');
exports.syncRenameDocuments = functions
	.region('europe-west1')
	.firestore
  	.document('media/{ownerId}/documents/{docId}')
  	.onUpdate((change, context) => { 
		  /* ... */ 
		  return syncRenameDocuments(change, context);	
	});

// takes care of creating kpis regularyly
const kpis = require('./src/kpis/kpis');
exports.kpis = functions
	.region('europe-west1')
	.pubsub
	.schedule('every 24 hours')
	.onRun((context) => {
		return kpis(context);
	});

// takes care of backuping firestore regularyly
const scheduledFirestoreExport = require('./src/scheduledFirestoreExport');
exports.scheduledFirestoreExport = functions
	.region('europe-west1')
	.pubsub
	.schedule('every 24 hours')
	.onRun((context) => {
		return scheduledFirestoreExport(context);
	});

// replicated user name edits from profile to institutional accounts records
const enrichAccountNameInstitution = require('./src/enrichAccountNameInstitution');
exports.enrichAccountNameInstitution = functions
	.region('europe-west1')
	.database
	.ref('profiles/{userId}/privateData')
	.onWrite((snap, context) => {
		return enrichAccountNameInstitution(snap, context);	
	});

// parse streams Log
const getLogStream = require('./src/otMonitor/getLogStream');
exports.getLogStream = functions
	.region('europe-west1')
	.https.onRequest(getLogStream);

// relation table for connecting institutions/users and their peers
const tools = require('./src/tools/tools');
exports.tools = functions
	.region('europe-west1')
	.database
	.ref('opentok/{tool}') // abused opentok entry on eve
	.onWrite((snap, context) => {
		// console.log('syncUserPeers: ', snap.after.data());
		return tools(snap, context);
	});	

// relation table for connecting institutions/users and their peers
const syncUserPeers = require('./src/peers/syncUserPeers');
exports.syncUserPeers = functions
	.region('europe-west1')
	.firestore
	.document('userPeers/{docId}')
	.onWrite((snap, context) => {
		// console.log('syncUserPeers: ', snap.after.data());
		return syncUserPeers(snap, context);
	});	

// all functions for institution accounts
const institutionsAccountsUpdateAgent = require('./src/institutionsAccountsUpdateAgent');
exports.institutionsAccountsUpdateAgent = functions
	.region('europe-west1')
	.firestore
	.document('institutions/{institutionId}/accounts/{accountId}')
	//.onUpdate((snap, context) => {
	.onWrite((snap, context) => {
		// console.log('institutionsAccountsUpdateAgent: ', snap.after.data());
		return institutionsAccountsUpdateAgent(snap, context);
	});	

// enrich coach/customer based on invite response
const completeInvite = require('./src/completeInvite');
exports.completeInvite = functions
	.region('europe-west1')
	.database.ref('/invites/{inviteId}')
	.onUpdate((snap, context) => {
		// console.log('invite: ', snap.after.data());
		return completeInvite(snap, context);
	});

// enrich coach/institution generated link invite
const linkInvite = require('./src/linkInvite');
exports.linkInvite = functions
	.region('europe-west1')
	.database.ref('/invites/{inviteId}')
	.onCreate((snap, context) => {
		// console.log('invite: ', snap.data());
		return linkInvite(snap, context);
	});

const userUpdateAgent = require('./src/userUpdateAgent');
exports.userUpdateAgent = functions
	.region('europe-west1')
	.database.ref('/users/{userId}')
	.onWrite((snap, context) => {
		// console.log('userUpdateAgent: ', snap.after.val());
		return userUpdateAgent(snap, context);
	});		

const genericInvite = require('./src/genericInvite/genericInvite');
exports.genericInvite = functions
	.region('europe-west1')
	.firestore
	.document('invites/{inviteId}')
	.onCreate((snap, context) => {
		// console.log('invite: ', snap.data());
		return genericInvite(snap, context);
	});

const sendWelcomeEmail = require('./src/sendWelcomeEmail'); //.sendWelcome;
exports.sendWelcomeEmail = functions
	.region('europe-west1')
	.auth.user()
	.onCreate(user => {
		console.log('sendWelcomeEmail: ', JSON.stringify(user));
		return sendWelcomeEmail(user);
	});

const generateJwt = require('./src/otJwt').generateJwt;
exports.generateJwt = functions
	.region('europe-west1')
	.https.onRequest(generateJwt);

const foo = require('./src/foo');
exports.doFoo = functions
	.region('europe-west1')
	.https.onRequest(foo);

const stripeVoucher = require('./src/stripe/stripeVoucher').stripeVoucher;
exports.getStripeVoucher = functions
	.region('europe-west1')
	.https.onRequest(stripeVoucher);

const stripeVoucherValidate = require('./src/stripe/stripeVoucher').stripeVoucherValidate;
exports.getStripeVoucherValidate = functions
	.region('europe-west1')
	.https.onRequest(stripeVoucherValidate);

// ### TODO: OLD [END import] ### 

/* ####### TODO: old structure to be migrated ######## */


// [START syncCoachCustomer]
// Input of function: changes in coachCustomer table
// Output of function: 
//	- sync changes in coachCustomer with the regarding participantList of classroom
const syncUserPeersFromCoachCustomers = require('./src/peers/syncUserPeersFromCoachCustomers');
exports.deleteFromCoachCustomer = functions
	.region('europe-west1')
	.database.ref('/coachCustomer/{coachId}/{customerId}').onDelete(
	(snap, context) => {
		return syncUserPeersFromCoachCustomers(snap, context);
	}
);

/**
 * when creating student entry, e.g. by invite, sync with relevant areas, e.g. participant list
 */
const syncCoachCustomer = require('./src/coachCustomer/syncCoachCustomer');
exports.syncCoachCustomer = functions
	.region('europe-west1')
	.database.ref('/coachCustomer/{coachId}')
	.onWrite((snap, context) => {
		return syncCoachCustomer(snap, context);
	}
);

// TODO: find more efficient was to handle that
// enrich customer name when creating participants entries in classrooms
/*
const getUserName = require('./src/helpers/getUserName');
exports.syncUserNamesFromCoachCustomer = functions
	.region('europe-west1')
	.database.ref('/classrooms/{coachId}/participants/{participantId}')
	// .onCreate...
	.onUpdate(async (snap, context) => {
		const participantId = context.params.participantId; 
		// if(!snap.child('registrationStatus').exists() || snap.val().registrationStatus !== 'isAnonymous') {
		if(snap.after.val().registrationStatus !== 'isAnonymous') {
			const name = await getUserName(participantId);
			// console.log('syncUserNamesFromCoachCustomer->name', name);
			// return snap.ref.update({ name: name });
			return 'deactivated'
		} else {
			return 'name not updated...'
		}

	}
);
*/

// [START archiving]
const archiving = require('./src/archiving/archiving');
exports.archiving = functions
	.region('europe-west1')
	.https.onRequest(archiving);

const copyArchive = require('./src/archiving/copyArchive');
exports.copyArchive = functions
	.region('europe-west1')
	.https.onRequest(copyArchive);
// [END archiving]


// [START OT Monitoring]
/*
this URL is pinged from OT, when the status of the archive is changing
*/
const logStreamEvents = require('./src/otMonitor/logStreamEvents');
exports.otMonitor = functions
	.region('europe-west1')
	.https.onRequest((req, res) => {

	// console.log('monitor: ', req);
	console.log('req.body: ', JSON.stringify(req.body));
	
	if (req.body.event === 'streamCreated' || req.body.event === 'streamDestroyed') {
		logStreamEvents(req.body);
	}
	
	// in any case send a message back
	let data = {};
	// [START usingMiddleware]
  	// Enable CORS using the `cors` express middleware.
  	// response function
  	let response = data => {
		cors(req, res, () => {
			// expects data obj
			return res.status(200)
		        .type('application/json')
		        .send(data)
		});
  	}
  	data = {
		status: 'success',
	}
	response(data);

});
// [END OT Monitoring]


// logging
const logNow = require('./src/logNow');
const getSessionIdTokenHttpForNow = require('./src/room/getSessionIdTokenHttpForNow')
exports.getSessionIdTokenHttpForNow = functions
	.region('europe-west1')
	.database.ref('/now/{roomId}')
	.onCreate( async (snap, context) => {
		return getSessionIdTokenHttpForNow(snap, context);	
	});
// [END getSessionIdTokenForNow]

// [START deleteNow]
/**
 * TODO: on new create, clean up complete tree --> if timestamp + countdown < now --> delete
 * This function is triggered by the coach action of terminating the session. This triggeres setting delete: "delete".
 * the classroom entries are reset and the participant list cleared! Finally log data is collected and saved.
 */
exports.deleteNow = functions
	.region('europe-west1')
	.database.ref('/now/{roomId}/delete').onWrite(
		async (snap, context) => {
			// console.log('deleteNow->after: ', JSON.stringify(snap.after.val()));
			const roomId = context.params.roomId;

			const action = snap.after.val();

			if(action === 'delete'){
				// first: delete asap
				await admin.database().ref(`/now/${roomId}`).set(null);

				// now clean up data...
				const snap = await admin.database()
					.ref('/now/' + roomId)
					.once('value');
				const ownerId = snap.val().owner;
				const sessionId = snap.val().sessionId;
				const timestamp = snap.val().timestamp;
				const locked = snap.val().locked;
				const mediaMode = snap.val().mediaMode;

				const updates = {};	
				updates[`/classrooms/${ownerId}`] = { 
					roomId : null,
					participants : null,
					locked : locked,
					timestamp : timestamp,
					mediaMode: mediaMode,
				};

				// ### BE CAREFULL!! ###
				// apply the update array
				await admin.database().ref().update(updates);
				
				// TODO: dirty trick!!
				// remove regarding remote data 
				// delay deletion to cope with connected subscriptions
				setTimeout(() => admin.database().ref(`/directShare/${sessionId}`).set(null), 2000);

				const currentTime = Date.now();
				const duration = currentTime - timestamp;
				// console.log('duration: ', duration);
				// logging
				const logData = {
					userId: ownerId,
					sessionId: sessionId,
					duration: duration, 
					maxStreams: snap.val().maxStreams,
					timestamp: currentTime,
				}
				logNow('now-end', logData);

				return 'room ' + roomId + ' deleted';
			} else {
				return 'no delete actions';
			}
		// end fbFunction			
	});
// [END deleteNow]

// [START sync of deleteUser]
exports.removeUserFromDatabase = functions.auth.user()
    .onDelete((user, context) => {
  // Get the uid of the deleted user.
  var uid = user.uid;

  console.log('remove userId from Fb: ', uid);

  // Remove the user from your Realtime Database's /users node.
  
  	admin.database().ref("/users/" + uid).remove();
  	admin.database().ref("/profiles/" + uid).remove();
  	admin.database().ref("/coachCustomer/" + uid).remove();
// TODO: deletion of coach customer relations
/*
  	firebase.database()
  		.ref('coachCustomer')
  		.once("value")
        .then(function(snapshot) {
            snapshot.forEach(function(childSnapshot) {
                 // key will be "ada" the first time and "alan" the second time
                var childKey = childSnapshot.key;
                //console.log('--> ', childKey);
                // childData will be the actual contents of the child
                var childData = childSnapshot.val();
                childSnapshot.forEach(function(childChildSnapshot) {
                    var childChildKey = childChildSnapshot.key;
                    //console.log('--> -->', childChildKey);
                    //
                    if(childChildKey === id){
                        firebase.database().ref('coachCustomer/' + childKey + '/' + childChildKey).remove()
                            .then(function() {
                                console.log('Deleted at coachCustomer' + id);
                                self.status += 'User (coach) has been deleted from coachCustomer at: coachCustomer/' + childKey + '/' + childChildKey + ' ';
                            })
                            .catch(function(error) {
                                console.log('Deletion failed at coachCustomer: ', id);
                                self.status += 'Deletion failed at coachCustomer';
                            });   
                    }
                });
            });

        });
*/
	// dummy return
  	return true;

});
// [END sync of deleteUser]

 /**
 * When requested this Function will make sure that at least every 10 minutes an OT room will be requested
 */
 // url -> https://europe-west1-doozzoo-dev.cloudfunctions.net/triggerOt?key=3d95702d45dcd4efc19543447a5088397df956a2
 // url -> https://europe-west1-project-6816589442602044124.cloudfunctions.net/triggerOtkey=3d95702d45dcd4efc19543447a5088397df956a2
exports.triggerOt = functions
	.region('europe-west1')
	.https.onRequest(async (req, res) => {
	const key = req.query.key;

	// Exit if the keys don't match.
	// 3d95702d45dcd4efc19543447a5088397df956a2
	if (!secureCompare(key, functions.config().cron.key)) {
		console.log('The key provided in the request does not match the key set in the environment. Check that', key, 'matches the cron.key attribute in `firebase env:get`');
		res.status(403).send('Security key does not match. Make sure your "key" URL query parameter matches the ' + 'cron.key environment variable.');
		return null;
	}

	let newKey = admin.database()
		.ref().child('now').push().key;

	let updates = {};
  	updates['/now/' + newKey] = 'foo';

	await admin.database()
		.ref().update(updates);
	
	console.log('Ot trigger done');
	res.send('Ot trigger done');
	return null;

});

const promisePool = require('es6-promise-pool');
const PromisePool = promisePool.PromisePool;
const secureCompare = require('secure-compare');
// Maximum concurrent account deletions.
const MAX_CONCURRENT = 3;

/**
 * When requested this Function will delete every user accounts that has been inactive for 30 days.
 * The request needs to be authorized by passing a 'key' query parameter in the URL. This key must
 * match a key set as an environment variable using 
 * `firebase functions:config:set cron.key="3d95702d45dcd4efc19543447a5088397df956a2"`.
 */
 // url -> https://us-central1-doozzoo-dev.cloudfunctions.net/removeAnonymous?key=3d95702d45dcd4efc19543447a5088397df956a2
exports.removeAnonymous = functions
	.region('europe-west1')
	.https.onRequest(async (req, res) => {
	const key = req.query.key;

	// Exit if the keys don't match.
	// 3d95702d45dcd4efc19543447a5088397df956a2
	if (!secureCompare(key, functions.config().cron.key)) {
		console.log('The key provided in the request does not match the key set in the environment. Check that', key, 'matches the cron.key attribute in `firebase env:get`');
		res.status(403).send('Security key does not match. Make sure your "key" URL query parameter matches the ' + 'cron.key environment variable.');
		return null;
	}

	// Fetch all user details.
	const anonymousUsers = await getAnonymousUsers();
	// Use a pool so that we delete maximum `MAX_CONCURRENT` users in parallel.
	const promisePool = new PromisePool(() => deleteAnonymousUser(anonymousUsers), MAX_CONCURRENT);
	await promisePool.start();
	console.log('User cleanup finished');
	res.send('User cleanup finished');
  	return null;
});

/**
 * Deletes one Anonymous user from the list.
 */
function deleteAnonymousUser(AnonymousUsers) {
  if (AnonymousUsers.length > 0) {
    const userToDelete = AnonymousUsers.pop();
    
    // Delete the Anonymous user.
    return admin.auth().deleteUser(userToDelete.uid).then(() => {
      return console.log('Deleted user account', userToDelete.uid, 'because of beeing anonymous');
    }).catch((error) => {
      return console.error('Deletion of Anonymous user account', userToDelete.uid, 'failed:', error);
    });
  } else {
    return null;
  }
}

/**
 * Returns the list of all Anonymous users.
 */
async function getAnonymousUsers(users = [], nextPageToken) {
  const result = await admin.auth().listUsers(1000, nextPageToken);
  // Find users that have not signed in in the last 30 days.
  const AnonymousUsers = result.users.filter(
      // user => Date.parse(user.metadata.lastSignInTime) < (Date.now() - 30 * 24 * 60 * 60 * 1000));
      user => user.email === undefined);
  
  // Concat with list of previously found Anonymous users if there was more than 1000 users.
  users = users.concat(AnonymousUsers);
  
  // If there are more users to fetch we fetch them.
  if (result.pageToken) {
    return getAnonymousUsers(users, result.pageToken);
  }
  
  return users;
}
// [END sync of removeAnonymous]

// [START cleanupRooms]
// Cut off time. Child nodes older than this will be deleted.
const CUT_OFF_TIME = 12 * 60 * 60 * 1000; // 24 * 60 * 60 * 1000; // 24 Hours in milliseconds.
/**
 * This database triggered function will check for child nodes that are older than the
 * cut-off time. Each child needs to have a `timestamp` attribute.
 */
// url -> https://europe-west1-doozzoo-dev.cloudfunctions.net/cleanupRooms?key=3d95702d45dcd4efc19543447a5088397df956a2
// url ->  https://europe-west1-doozzoo-eve.cloudfunctions.net/cleanupRooms?key=3d95702d45dcd4efc19543447a5088397df956a2
exports.cleanupRooms = functions
	.region('europe-west1')
	.https.onRequest(async (req, res) => {
	const key = req.query.key;

	// Exit if the keys don't match.
	// 3d95702d45dcd4efc19543447a5088397df956a2
	if (!secureCompare(key, functions.config().cron.key)) {
		console.log('The key provided in the request does not match the key set in the environment. Check that', key, 'matches the cron.key attribute in `firebase env:get`');
		res.status(403).send('Security key does not match. Make sure your "key" URL query parameter matches the ' + 'cron.key environment variable.');
		return null;
	}


	const ref = admin.database().ref('now/');
	const now = Date.now();
	const cutoff = now - CUT_OFF_TIME;
	console.log('cutoff: ', cutoff);
	const oldItemsQuery = ref.orderByChild('timestamp').endAt(cutoff);
	const snapshot = await oldItemsQuery.once('value');

	snapshot.forEach(child => {
		//console.log('child value: ', child.val());
		const key = child.key;
		const owner = child.val().owner;
		const isPermanent = child.val().permanent;
		console.log('key, owner, isPermanent: ', key, owner, isPermanent);

		if(isPermanent !== null && isPermanent !== undefined){
			if(isPermanent === true){
				console.log('room is permanent')
			}
		} else {
			console.log('room deleted: ', key);
			admin.database()
				.ref('now/' + key)
				.remove();
			admin.database()
				.ref('classrooms/' + owner)
				.update({ roomId: null });
		}	

	});

	console.log('User cleanup finished');
	res.send('User cleanup finished');
  	return null;

});

// [END of cleanupRooms]

// ### Stripe integration ###

// [START archiving]
const stripeHook = require('./src/stripe/stripeHook');
exports.stripeHook = functions
	.region('europe-west1')
	.https
	.onRequest(stripeHook);

// ### Stripe integration ###
/**
 * see: https://github.com/firebase/functions-samples/blob/Node-8/stripe/functions/index.js
 */
/*
// [START createStripeCustomer]
// When a user is created, register them with Stripe
exports.createStripeCustomer = functions
	.region('europe-west1')
	.auth.user()
	.onCreate(async (user) => {
		const customer = await stripe.customers.create({email: user.email});
		// return admin.firestore().collection('stripe_customers').doc(user.uid).set({customer_id: customer.id});
		return admin.database()
			.ref('stripe_customers/' + user.uid)
			.update({
				customer_id: customer.id,
				customerData: customer,
			});
	});
// [END of createStripeCustomer]
*/
// [START createCoachStripeCustomer]
// When a user is created, register them with Stripe
const createCoachStripeCustomer = require('./src/stripe/createCoachStripeCustomer');
exports.createCoachStripeCustomer = functions
	.region('europe-west1')
	.database.ref('/users/{userId}/plan') // plan defines whether a stripe account is needed (pro/premium)
	.onWrite(async (snap, context) => {
		return createCoachStripeCustomer(snap, context);	
	});
/*
exports.createCoachStripeCustomer = functions
	.region('europe-west1')
	.database.ref('/users/{userId}/plan') // plan defines whether a stripe account is needed (pro/premium)
	.onWrite(async (snap, context) => {
		// TODO: remove after debugging
		// console.log('stripe.token: ', functions.config().stripe.token);
		console.log('users/plan: ', JSON.stringify(snap.val()));

		const existingStripeCustomerId = await admin.database()
			.ref('stripe_customers/' + context.params.userId + '/customerData/id')
			.once('value');
		// console.log('existingStripeAccount: ', existingStripeCustomerId.val());
		// triggered from coach profile form
		// only if no account exists already
		if (['pro','premium'].includes(snap.val()) && existingStripeCustomerId.val() === null ) {
			// for security reasons we fetch email only from firebase auth data
			const user = await admin.auth().getUser(context.params.userId);
			// console.log('user', user);

			const address = await admin.database()
				.ref('profiles/' + context.params.userId + '/privateData')
				.once('value');
			const ad = address.val();

			const customer = await stripe.customers.create({
				email: user.email,
				shipping: {
					name: ad.firstname + ' ' + ad.lastname,
					address: {
						city: ad.city,
						country: ad.country,
						line1: ad.address_street1,
						line2: ad.address_street2,
						postal_code: ad.zipcode
						// state: 
					}
				}
			});
			// return admin.firestore().collection('stripe_customers').doc(user.uid).set({customer_id: customer.id});
			return admin.database()
				.ref('stripe_customers/' + context.params.userId)
				.update({
					customer_id: customer.id,
					customerData: customer,
				});

		} else {
			// finalize cloud function
			return null;
		}
	});
*/
// [END of createStripeCustomer]

// When a user deletes their account, clean up after them
exports.removeStripeCustomer = functions
	.region('europe-west1')
	.auth.user()
	.onDelete(async (user) => {
		/*
		const snapshot = await admin.firestore().collection('stripe_customers').doc(user.uid).get();
		const customer = snapshot.data();
		await stripe.customers.del(customer.customer_id);
		return admin.firestore().collection('stripe_customers').doc(user.uid).delete();
		*/
		await admin.database()
			.ref('stripe_customers/' + user.uid)
			.remove();
		await admin.database()
			.ref('users/' + user.uid)
			.remove();
		return true;
	});
// [END of removeStripeCustomer]

// [START of addPaymentSource]
const addPaymentSource = require('./src/stripe/addPaymentSource');
exports.addPaymentSource = functions
	.region('europe-west1')
	// .firestore.document('/stripe_customers/{userId}/tokens/{pushId}') // firestore implementation
	.database.ref('/stripe_customers/{userId}/tokens/{pushId}')
	.onCreate(async (snap, context) => {
		return addPaymentSource(snap, context);
	});
// [END of addPaymentSource]

// Add a subscription by writing it into the subscription obj of a stripe customer
exports.addSubscription = functions
	.region('europe-west1')
	.database.ref('/stripe_customers/{userId}/subscriptions/trigger/{pushId}')
	.onCreate(async (snap, context) => {
		const subscriptionData = snap.val();
		console.log('subscriptionData: ', JSON.stringify(subscriptionData));
		// const pushId = context.params.pushId;

		try {
			const snapshot = await admin.database()
				.ref('stripe_customers/' + context.params.userId)
				.once('value');
			// fetch stripe customer id
			// const customer =  snapshot.val().customer_id;
			const customer =  snapshot.val().customerData.id

			const snapshot2 = await admin.database()
				.ref('stripe_customers/' + context.params.userId + '/subscriptions/currentSubscription')
				.once('value');
			// fetch stripe customer id
			const currentSubscription = snapshot2.val();

			if (currentSubscription !== null) {
				// change EXISTING subscription
				const _subscription = await stripe
					.subscriptions
					.update(currentSubscription.id, {
						cancel_at_period_end: false,
						items: [{
							id: currentSubscription.items.data[0].id,
							plan: subscriptionData.plan,
							quantity: subscriptionData.quantity,
						}],
						// apply coupon code to existing subscription
						coupon: subscriptionData.coupon,
						// calculate already consumed subscription with new one
						// create immediately an update invoice
						prorate: true,
					});

				console.log('stripe subscription update: ', JSON.stringify(subscriptionData.plan), JSON.stringify(_subscription));
				await admin.database()
					.ref('stripe_customers/' + context.params.userId + '/subscriptions')
					.update({currentSubscription: _subscription});

				return updateStripeCustomerData(customer, context.params.userId);

			} else {
				// create NEW subscription
				// console.log('customer, token: ', customer, subscriptionData);
				const subscription = await stripe.subscriptions.create({
					customer: customer,
					items: [
						{
							plan: subscriptionData.plan,
							quantity: subscriptionData.quantity,
						},
					],
					// apply coupon code
					coupon: subscriptionData.coupon,
				});

				console.log('stripe subscription: ', JSON.stringify(subscription));
				await admin.database()
					.ref('stripe_customers/' + context.params.userId + '/subscriptions')
					.update({currentSubscription: subscription});

				return updateStripeCustomer(customer, context.params.userId);

			}

		} catch (error) {
			// await snap.ref.set({'error':userFacingMessage(error)},{merge:true});
			console.log('error: ', JSON.stringify(error));
			return null;
			// return reportError(error, {user: context.params.userId});
		}
	});
// [END of addSubscription]

// updates only data and not user role
async function updateStripeCustomerData(stripeCustomerId, userId) {
	//console.log('input', stripeCustomerId, userId);
	try {
		const customerData = await stripe.customers
			.retrieve(stripeCustomerId);

		// console.log('customerData: ', customerData);
		return await admin.database()
			.ref('stripe_customers/' + userId)
			.update({
				customer_id: stripeCustomerId, // legacy
				customerData: customerData,
			});	
	} catch(error) {
		console.log('error: ', error);
		return null;
	}
}

async function updateStripeCustomer(stripeCustomerId, userId) {
	//console.log('input', stripeCustomerId, userId);
	try {
		const customerData = await stripe.customers
			.retrieve(stripeCustomerId);

		// console.log('customerData: ', customerData);
		await admin.database()
			.ref('stripe_customers/' + userId)
			.update({
				customer_id: stripeCustomerId, // legacy
				customerData: customerData,
			});	

		// check subscription, if /subscriptions/total_count > 1
		// set role to 1, otherwise to 0
		const numberOfSubscriptions = customerData.subscriptions.total_count;
		if (numberOfSubscriptions > 0) {
			// create myRooms from backend ???
			console.log('coach role granted: ', userId);
			return admin.database()
				.ref('users/' + userId)
				.update({
					role: 1,
					userActions: "coachRequestGranted",
					updatedOn: Date.now()
				});	
		} else {
			console.log('coach role removed: ', userId);
			return admin.database()
				.ref('users/' + userId)
				.update({
					role: 0,
					userActions: "coachRoleRemoved",
					updatedOn: Date.now()
				});
		}

	} catch(error) {
		console.log('error: ', error);
		return null;
	}
}

async function stripeLog(userId, msgType, msg) {
	console.log('stripeLog: ', userId, msgType, msg);

	return admin.database()
		.ref('stripe_customers/' + userId + '/log')
		.push({
			msgType: msgType, // success | error
			msg: msg, // content
			timestamp: admin.database.ServerValue.TIMESTAMP, // timestamp
		});	

}

// [START syncUserData]
// sync FB user table with authentication user data


/**
 * This database triggered function will check for child nodes that are older than the
 * cut-off time. Each child needs to have a `timestamp` attribute.
 */
// url -> https://europe-west1-doozzoo-dev.cloudfunctions.net/syncUserData?key=3d95702d45dcd4efc19543447a5088397df956a2&limit=10
exports.syncUserData = functions
	.region('europe-west1')
	.https.onRequest(async (req, res) => {
	const key = req.query.key;
	const limit = parseInt(req.query.limit);	

	// Exit if the keys don't match.
	// 3d95702d45dcd4efc19543447a5088397df956a2
	if (!secureCompare(key, functions.config().cron.key)) {
		console.log('The key provided in the request does not match the key set in the environment. Check that', key, 'matches the cron.key attribute in `firebase env:get`');
		res.status(403).send('Security key does not match. Make sure your "key" URL query parameter matches the ' + 'cron.key environment variable.');
		return null;
	}

	let keys = await admin.database()
		.ref('users')
		.orderByChild('updatedOn') // lets work only on the newest entries
		.limitToFirst(limit) // limit it to 200
		.once('value')
		.then(snap => {
			let items = [];
			snap.forEach(val => {
				items.push(val.key); // populate an array with all keys
			});
			return items;
		});	
	
	// using promise pool is mandatory, otherwise firebase will run in timeouts
	const promisePool = new PromisePool(() => deleteOutdatedUser(keys), 4); // function, max concurrent promises
	await promisePool.start();
	// output to the browser
	res.send('User sync finished');

});

/**
 * Deletes one Anonymous user from the list.
 */
function deleteOutdatedUser(keys) {
	if (keys.length > 0) {
	  const key = keys.pop();
	  
	  return admin.auth().getUser(key)
		.then(userRecord => {
			// console.log('user exists: ', key);
			return 'user exists: ', key;
		})
		.catch(async error => {
			// console.log('Error fetching user data:', error.code);
			if (error.code === 'auth/user-not-found') {
				await admin.database().ref(`users/${key}`).remove(); //.set(null);
				console.log('user deleted: ', key);
				return 'user deleted: ', key;
			} else {
				return 'Error fetching user data:', error.code;
			}
		});
	} else {
	  	return null;
	}
  }

// [END syncUserData]

// ### [END all] ### //
