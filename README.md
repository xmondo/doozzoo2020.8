# cmd line shorts

## global versions
### typescript -> 3.9.7 // npx tsc -v
### node.js -> 14.9.0 // node -v

## workaround media fail
https://stackoverflow.com/questions/50513374/firebase-realtime-database-currently-gives-trigger-payload-too-large-error

TRIGGER_PAYLOAD_TOO_LARGE error is part of a new feature Firebase 

You can turn this feature off yourself by making this REST call:
curl -X PUT -d "false"  https://doozzoo-eve/.firebaseio.com/.settings/strictTriggerValidation/.json?auth\=<database secret>
Where <SECRET> is your DB secret
doing the same with setting "true" switches back to before...

# stats
## link
https://medium.com/angular-in-depth/optimize-angular-bundle-size-in-4-steps-4a3b3737bf45

## commands
1. npx ng build --stats-json
2. webpack-bundle-analyzer ./firebase/public/app/stats.json

# serve for external devices
### use local IP
npx ng serve --disableHostCheck=true --host=192.168.1.4 --ssl=true
npx ng serve --disableHostCheck=true --host=192.168.178.108 --ssl=true

## global versions
### typescript -> 3.9.7 // npx tsc -v
### node.js -> 14.9.0 // node -v

# node 
## use old node version with n -> 10.15.3
## command to switch versions:
n use 10.15.3
## dommand to download and install versiom
n <version | lts, current>


## "tone": "^13.4.9",

cd /Users/mypelz/Documents/work/doozzoo/app/firebase-app
firebase use dev
firebase use staging
firebase use prod

ng build --configuration=dev // or --configuration=stage or --prod
ng build --configuration=dev --optimization=true --sourceMap=false --aot
ng build --configuration=stage --optimization=true --sourceMap=false

npm run ng-high-memory -- build --configuration=dev --optimization=true --sourceMap=true --aot
npm run ng-high-memory -- build --configuration=dev --optimization=true --sourceMap=false --aot

npm run ng-high-memory -- build --configuration=stage --optimization=true --sourceMap=true --aot
npm run ng-high-memory -- build --configuration=stage --optimization=true --sourceMap=false --aot

firebase deploy --only hosting:now // or hosting:app
firebase deploy --only hosting:app 

firebase deploy --only functions:doSyncCoachCustomer

// !!! //
get index in cli ->
firebase firestore:indexes
set ->
firebase deploy --only firestore:indexes

ng build --configuration=stage

// export users:
firebase auth:export doozzoo-prod-useraccounts.csv --format=csv // csv | json
// import users (hash obfuscated)
firebase auth:import doozzoo-prod-useraccounts.csv --hash-algo=SCRYPT --hash-key=<hash here from source(!!!)> --salt-separator=Bw== --rounds=8 --mem-cost=14


cd /Users/mypelz/Documents/work/doozzoo/app2019/OtNg6/nowNg6

# npx 
Install with local client
npx -p @angular/cli ng new hello-world-project

# node 
## use old node version with n -> 10.15.3
## command to switch versions:
n use 10.15.3
## dommand to download and install versiom
n <version | lts, current>

then execute e.g. gulp locally...

# backup
## firestore
### backup using gcloud
gcloud firestore export gs://doozzoo-dev-backups/firestore|| gs://[BUCKET_NAME]/[EXPORT_PREFIX]/
gcloud firestore export gs://doozzoo-dev-backups/firestore/2020-05-18/
### restore using gcloud
gcloud firestore import gs://doozzoo-dev-backups/firestore || gs://[BUCKET_NAME]/[EXPORT_PREFIX]/
gcloud firestore import gs://doozzoo-dev-backups/firestore/2020-05-18/

### Prod
### backup using gcloud
gcloud firestore export gs://doozzoo-eve-firestore-backups/2020-05-18/
### restore using gcloud
gcloud firestore import gs://doozzoo-eve-firestore-backups/2020-05-18/

## backup Storage using glcoud cli
### dev
source: doozzoo-eve.appspot.com --> destination: doozzoo-dev-appspot_backup doozzoo-eve-appspot_backup
### command
gsutil cp -r gs://doozzoo-dev.appspot.com gs://doozzoo-dev-appspot_backup

### Prod
source: doozzoo-eve.appspot.com --> destination: doozzoo-eve-appspot_backup
### command
#### singel threaded
gsutil cp -r gs://doozzoo-eve.appspot.com gs://doozzoo-eve-appspot_backup
#### multi threaded
gsutil -m cp -r gs://doozzoo-eve.appspot.com gs://doozzoo-eve-appspot_backup

# installation guide
## firebase
1. go into ./firebase/functions and "npm install"
2. init firebase from ./firebase
3. npm install -g firebase-tools from ./firebase

# documentation
## commands
build -> npm compodoc
serve -> npm compodoc serve
## documentation
https://compodoc.app/guides/usage.html

# serve for external devices
### use local IP
npx ng serve --disableHostCheck=true --host=192.168.1.4 --ssl=true
npx ng serve --disableHostCheck=true --host=192.168.178.32 --ssl=true

## Important!

When building an Angular app with OpenTok you need to make sure to include the zone.js polyfills for rtcpeerconnection and getusermedia otherwise your application will not work in Safari, you will get timeouts subscribing. See [polyfills.ts](src/polyfills.ts) and [#17](/opentok/opentok-web-samples/issues/17) for details.

## Known Limitations

* This sample app does not work in IE 11. To get it to work in IE 11 you will need to turn on the extra polyfills for IE in [polyfills.ts](src/polyfills.ts).

## Demo

You can see a demo of this sample running at [opentok.github.io/opentok-web-samples/Angular-Basic-Video-Chat/](https://opentok.github.io/opentok-web-samples/Angular-Basic-Video-Chat/)

> **Note** The demo is setup so that a new room is generated based on your public IP address. So will only work if you are connecting from 2 browsers on the same network.

## Running the App

### Setting your OpenTok apiKey, sessionId and token

Before you can run this application you need to modify [config.ts](src/config.ts) and include your OpenTok API Key, Session ID and a valid token. For more details on how to get these values see [Token creation
overview](https://tokbox.com/opentok/tutorials/create-token/).

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Option for ssl, served by running -prod: ng e2e --base-url="https://localhost:4200"

### Further help

## EMULATORS


    Check which process is occupying the port sudo lsof -i tcp:<port> 
    Kill the process kill -9 <process id>


## fonts helper
https://google-webfonts-helper.herokuapp.com/fonts/fira-mono?subsets=latin

